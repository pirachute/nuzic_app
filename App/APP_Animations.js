
function APP_blink_error(element){
	//var animationEvent = whichAnimationEvent();
	element.classList.add('animate_red')
	//remove animation when animation ended
	//element.addEventListener((animationEvent,element), RemoveAnimation);
	element.addEventListener('animationend', () => {
		element.classList.remove('animate_red');
	})
}

function APP_blink_good(element){

	element.classList.add('animate_green')

	element.addEventListener('animationend', () => {
		element.classList.remove('animate_green');
	})
}

function APP_blink_play_blue(element){
	element.classList.add('animate_play_blue')
}

function APP_stop_blink_play_blue(element){
	//maybe something more??? XXX
	element.classList.remove("animate_play_blue")
}

function APP_blink_play_green_light(element){
	if(element!=undefined)element.classList.add('animate_play_green_light')
}

function APP_blink_play_green(element){
	if(element!=null)element.classList.add('animate_play_green')
}

function APP_reset_blink_play(){
	var re_data = document.querySelector(".App_RE")
	var blink_list = [...re_data.querySelectorAll(".animate_play_blue")]
	blink_list.forEach(input=>{
		input.classList.remove("animate_play_blue")
	})

	var blink_list_green_light = [...re_data.querySelectorAll(".animate_play_green_light")]
	blink_list_green_light.forEach(input=>{
		input.classList.remove("animate_play_green_light")
	})

	var blink_list_green = [...re_data.querySelectorAll(".animate_play_green")]
	blink_list_green.forEach(input=>{
		input.classList.remove("animate_play_green")
	})
}
/*
function APP_blink_play_input_list(element_list){
	element_list.forEach(element=>{
		APP_blink_play_blue(element)
	})
}

function APP_stop_blink_play_input_list(element_list){
	element_list.forEach(element=>{
		APP_stop_blink_play_blue(element)
	})
}
*/
