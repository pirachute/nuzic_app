window.onclick = function(event) {
	var parent = event.target.parentNode

	if(parent==null){
		APP_close_all_dropdown_segment_contents()
	}else if (!event.target.matches('.App_RE_dropdown_segment_button') && !parent.matches('.App_RE_dropdown_segment_button')
			&& !event.target.matches('.App_RE_voice_menu_button_img')
			&& !event.target.matches('.App_RE_column_selector_segment_menu_button') && !parent.matches('.App_RE_column_selector_segment_menu_button')
			&& !parent.matches('.App_RE_S_show_visualization_options_button') && event.target.closest('.App_RE_C_show_visualization_options_box')==null
			&& event.target.closest('.App_RE_S_show_visualization_options_box')==null
			&& !parent.matches('.App_PMC_time_line_pulse_top') && !parent.matches('.App_RE_C_show_visualization_options_button')
			&& !event.target.matches('.App_PMC_voice_selector_voice_item_menuIcon') && !parent.matches('.App_PMC_voice_selector_voice_item_menuIcon')
			&& !event.target.matches('.App_PMC_dropdown_segment_toggle_selection') && !parent.matches('.App_PMC_dropdown_segment_toggle_selection')
			&& !event.target.matches('.App_Nav_button') && !parent.matches('.App_Nav_button')){
		APP_close_all_dropdown_segment_contents()
	}

	if(event.target.matches('.App_RE_dropdown_segment_button')){
		var dropdown = event.target.closest(".App_RE_dropdown_segment").querySelector(".App_RE_dropdown_segment_content")
		APP_close_all_dropdown_segment_contents(dropdown)
		RE_toggle_dropdown_segment(parent)
	} else if (event.target.matches('.App_RE_voice_menu_button_img')){
		var dropdown = event.target.closest(".App_RE_voice_header_R").querySelector(".App_RE_voice_dropdown_menu")
		APP_close_all_dropdown_segment_contents(dropdown)
		RE_toggle_dropdown_voice(parent)
	} else if (event.target.matches('.App_RE_column_selector_segment_menu_button')){
		var dropdown = event.target.closest(".App_RE_column_selector_segment_menu").querySelector(".App_RE_dropdown_column_selector_content")
		APP_close_all_dropdown_segment_contents(dropdown)
		RE_toggle_dropdown_column_selector_segment(event.target)
	} else if (event.target.matches('.App_PMC_voice_selector_voice_item_menuIcon')){
		var dropdown = event.target.closest(".App_PMC_voice_selector_voice_item").querySelector(".App_PMC_dropdown_voiceMenu")
		APP_close_all_dropdown_segment_contents(dropdown)
		PMC_toggle_dropdown_voice(parent)
	} else if (event.target.matches('.App_PMC_voice_selector_visibility_dropdown_button_img')){
		APP_close_all_dropdown_segment_contents()
		PMC_toggle_line_selector_menu(event.target.parentNode)
	}else if (event.target.matches('.App_PMC_voice_selector_visibility_dropdown_button')){
		APP_close_all_dropdown_segment_contents()
		PMC_toggle_line_selector_menu(event.target)
	} else if (event.target.matches('.APP_PMC_voice_selector_color_picker')){
		APP_close_all_dropdown_segment_contents()
		PMC_color_picker_toggle(parent)
	} else if (event.target.matches('.App_Nav_button')){
		APP_close_all_dropdown_segment_contents()
		TNAV_show_main_menu()
	} 

	if(parent!=null){
		if(parent.matches('.App_RE_dropdown_segment_button')){
			var dropdown = event.target.closest(".App_RE_dropdown_segment").querySelector(".App_RE_dropdown_segment_content")
			APP_close_all_dropdown_segment_contents(dropdown)
			RE_toggle_dropdown_segment(event.target)
		}else if(parent.matches('.App_PMC_voice_selector_voice_item_menuIcon')){
			var dropdown = event.target.closest(".App_PMC_voice_selector_voice_item").querySelector(".App_PMC_dropdown_voiceMenu")
			APP_close_all_dropdown_segment_contents(dropdown)
			PMC_toggle_dropdown_voice(event.target)
		}else if (event.target.matches('.App_PMC_voice_selector_visibility_dropdown_button_img')){
			APP_close_all_dropdown_segment_contents()
			PMC_toggle_line_selector_menu(event.target.parentNode)
		} else if (event.target.matches('.App_PMC_voice_selector_visibility_dropdown_button')){
			APP_close_all_dropdown_segment_contents()
			PMC_toggle_line_selector_menu(event.target)
		} else if(parent.matches('.APP_PMC_voice_selector_color_picker')){
			APP_close_all_dropdown_segment_contents()
			PMC_color_picker_toggle(event.target)
		}else if(parent.matches('.App_RE_S_show_visualization_options_button')){
			var RE_sound_visualization = document.querySelector('.App_RE_S_show_visualization_options_box')
			if(RE_sound_visualization.style.display=='none'){
				APP_close_all_dropdown_segment_contents()
				RE_open_S_lines_visualization(this)
			}else{
				APP_close_all_dropdown_segment_contents()
				//RE_open_C_lines_visualization(this)
			}
		}else if(parent.matches('.App_RE_C_show_visualization_options_button')){
			var RE_time_visualization = document.querySelector('.App_RE_C_show_visualization_options_box')
			if(RE_time_visualization.style.display=='none'){
				APP_close_all_dropdown_segment_contents()
				RE_open_C_lines_visualization(this)
			}else{
				APP_close_all_dropdown_segment_contents()
			}
		} else if (parent.matches('.App_RE_column_selector_segment_menu_button')){
			var dropdown = event.target.closest(".App_RE_column_selector_segment_menu").querySelector(".App_RE_dropdown_column_selector_content")
			APP_close_all_dropdown_segment_contents(dropdown)
			RE_toggle_dropdown_column_selector_segment(parent)
		} else if (parent.matches('.App_Nav_button')){
			//no need , is not a toggle
			//APP_close_all_dropdown_segment_contents()
			//TNAV_show_main_menu()
		}
	}
}

function APP_close_all_dropdown_segment_contents(exception=null){
	var dropdowns = [...document.querySelectorAll(".App_RE_dropdown_segment_content")]
	var PMCdropDowns =[...document.querySelectorAll(".App_PMC_dropdown_voiceMenu")]
	var REdropDowns =[...document.querySelectorAll('.App_RE_voice_dropdown_menu')]
	var REdropDownsColumn =[...document.querySelectorAll('.App_RE_dropdown_column_selector_content')]
	var PMCsegmentdropDown =document.querySelector("#App_PMC_dropdown_segment_content")
	var colorPickerDropDowns =[...document.querySelectorAll(".App_PMC_dropdown_colorPicker")]
	var PMCvoiceSelectDropDowns = [...document.querySelectorAll('.App_PMC_voice_selector_button_visibility_dropdown')]
	//var keyboardDropdown=document.querySelector('.App_BNav_keyboard_optionsSelectKeyboard_container')
	//var RE_sound_visualization = document.querySelector('.App_RE_S_show_visualization_options_box')
	//var RE_time_visualization = document.querySelector('.App_RE_C_show_visualization_options_box')
	dropdowns.forEach(item=>{
		if(item==exception)return
		item.classList.remove('show');
	})
	PMCdropDowns.forEach(item=>{
		if(item==exception)return
		item.classList.remove('show');
	})
	REdropDowns.forEach(item=>{
		if(item==exception)return
		item.classList.remove('show');
	})
	
	REdropDownsColumn.forEach(item=>{
		if(item==exception)return
		item.classList.remove('show');
	})

	PMCvoiceSelectDropDowns.forEach(item=>{
		if(item==exception)return
		item.classList.remove('show');
	})
	colorPickerDropDowns.forEach(item=>{
		if(item==exception)return
		item.classList.remove('show');
	})
	RE_close_S_lines_visualization(this)
	RE_close_C_lines_visualization(this)
	PMCsegmentdropDown.style.display = "none"
	//keyboardDropdown.style.display='none'

	TNAV_hide_main_menu()
}

// function APP_refresh_module_lines(){
// 	console.error("OBSOLETE FUNCTION APP_refresh_module_lines")
// 	var module_button_states = APP_read_module_button_states()

	//find the one that is true
// 	var scale_module_RE = document.querySelector(".App_RE_scale_modules")
// 	var compas_module_RE = document.querySelector(".App_RE_compas_modules")

// 	var scale_module_PMC = document.querySelector(".App_PMC_scale_modules")
// 	var compas_module_PMC = document.querySelector(".App_PMC_compas_modules")

// 	var delta = 0
// 	if(module_button_states.ES_RE==true){
// 		scale_module_RE.style="display: flex;"
// 		delta+=28
// 	}else{
// 		scale_module_RE.style="display: none;"
// 	}

// 	if(module_button_states.CET_RE==true){
// 		compas_module_RE.style="display: flex;"
// 		delta+=28
// 	}else{
// 		compas_module_RE.style="display: none;"
// 	}

// 	if(module_button_states.ES_PMC==true){
// 		scale_module_PMC.style="display: flex;"
// 		delta+=28
// 	}else{
// 		scale_module_PMC.style="display: none;"
// 	}

// 	if(module_button_states.CET_PMC==true){
// 		compas_module_PMC.style="display: flex;"
// 		delta+=28
// 	}else{
// 		compas_module_PMC.style="display: none;"
// 	}
// }

// function APP_read_module_button_states(){
	//get buttons states
// 	console.error("OBSOLETE FUNCTION APP_read_module_button_states")
// 	return {ES_RE: true, CET_RE: true, ES_PMC: true, CET_PMC: true}
// 	var line_selector = document.querySelector(".App_SNav_line_selector")
// 	var scale_inp = line_selector.querySelector(".App_SNav_modules_ES_button")
// 	var compas_inp = line_selector.querySelector(".App_SNav_modules_CET_button")

// 	var module_button_list = [scale_inp,compas_inp]
// 	var index=-1
// 	var module_button_states = []
// 	module_button_list.forEach(button=>{
// 		index++
// 		if(button.classList.contains("App_SNav_modules_button_pressed")){
// 			return module_button_states[index]=true
// 		}else{
// 			return module_button_states[index]=false
// 		}
// 	})

	//module button states for PMC same button XXX
// 	return {
// 		ES_RE:module_button_states[0],
// 		CET_RE:module_button_states[1],
// 		ES_PMC:module_button_states[0],
// 		CET_PMC:module_button_states[1],
// 		}
// }

// function APP_set_module_button_states(module_button_states){
// 	console.error("OBSOLETE FUNCTION APP_set_module_button_states")
// 	if(module_button_states==null)return
// 	var line_selector = document.querySelector(".App_SNav_line_selector")
// 	var scale_inp = line_selector.querySelector(".App_SNav_modules_ES_button")
// 	var compas_inp = line_selector.querySelector(".App_SNav_modules_CET_button")
// 	if(module_button_states.ES_RE ==true){
// 		scale_inp.classList.add("App_SNav_modules_button_pressed")
// 	}else{
// 		scale_inp.classList.remove("App_SNav_modules_button_pressed")
// 	}
// 	if(module_button_states.CET_RE ==true){
// 		compas_inp.classList.add("App_SNav_modules_button_pressed")
// 	}else{
// 		compas_inp.classList.remove("App_SNav_modules_button_pressed")
// 	}
// }

function APP_populate_global_inputs(data){
	outputRE_PPM.value= data.global_variables.PPM
	outputPMC_PPM.value= data.global_variables.PPM
	outputRE_Li.value = data.global_variables.Li
	outputPMC_Li.value= data.global_variables.Li
	outputRE_TET.value=data.global_variables.TET
	outputPMC_TET.value=data.global_variables.TET
	outputRE_Rg.innerText= data.global_variables.TET*7
	outputPMC_Rg.innerText = data.global_variables.TET*7
}

//anonymous event handlers
var anonymous_events_list = [];

//used in dragover segment PMC
function APP_store_anonymous_event(fn, useCaptureMode, event) {
	var e = APP_find_stored_anonymous_event(event, useCaptureMode);
	if (!e) {
		anonymous_events_list.push({fn, useCaptureMode, event});
	}
}

function APP_find_stored_anonymous_event(event, useCaptureMode) {
	return anonymous_events_list.find(el => el.event === event && el.useCaptureMode === el.useCaptureMode);
}

function APP_delete_stored_anonymous_event(element,type){
	var e = APP_find_stored_anonymous_event(type, false);
	if (e) {
		element.removeEventListener(e.event, e.fn, e.useCaptureMode);
		anonymous_events_list.splice(anonymous_events_list.findIndex(el => el === e), 1);
	}else{
		console.error("function not found")
	}
}

//remove_scroll_on_spacebar
//function for chrome
window.onkeydown = function(e) {
	e = e || window.event  //normalize the evebnt for IE
	var target = e.srcElement || e.target  //Get the element that event was triggered on
	var tagName = target.tagName  //get the tag name of the element [could also just compare elements]
	return !(tagName==="BODY" && e.keyCode == 32)  //see if it was body and space
}

function APP_enter_new_value_Li(input){
	APP_stop()
	var new_Li = Math.round(input.value)
	if(new_Li==0){
		input.value=previous_value
		return
	}

	if(input.value==previous_value)return

	var min_Li = DATA_calculate_minimum_Li()
	var rest = new_Li%min_Li
	if (rest==0){
		//Li = new_Li
		DATA_force_Li(new_Li)
	}else {
		//reject value
		input.value=previous_value
		return
	}
}

function APP_enter_new_value_PPM(input){
	APP_stop()
	DATA_change_PPM(parseInt(input.value))
}

function APP_enter_new_value_TET(input){
	//verify value input is an ok option in order to create a equally tone spaced set of notes
	APP_stop()
	var new_TET = Math.round(input.value)
	if(new_TET==0){
		input.value=TET
		return
	}

	if(new_TET==TET){
		return
	}

	var max_TET = 53

	if (new_TET<=max_TET){
		TET = new_TET
	}else {
		//reject value
		TET = max_TET
	}

	if(new_TET!=12){
		BNAV_keyboard_lock_functions_TET_change()
	}else{
		BNAV_keyboard_unlock_functions_TET_change()
	}

	alert("Warning: note values are not automatically updated. Be carefull!")
	editor_scale = false

	var data = DATA_new_composition_data()
	data.global_variables.TET = TET
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(true)

	TNAV_reset_composition_name()
	current_composition_id=""
	if(typeof Reset_current_composition_id === "function"){
		Reset_current_composition_id()
	}
	BNAV_Refresh_Keyboard_On_TET_Change()()	
	
}

function APP_note_to_Hz(note_number,note_TET=null){
	var f_princ = 261.62
	var f_princ_reg = 4
	//LA is the 0
	//var f_princ = 440
	//register TET
	if(note_TET==null){
		console.error("specify TET")
		return
	}
	var freq = Math.pow(2, (note_number-(f_princ_reg*note_TET))/note_TET) * f_princ
	return freq
}

function APP_Hz_to_note(freq,note_TET=null){
	var f_princ = 261.62
	var f_princ_reg = 4
	//LA is the 0
	//var f_princ = 440
	//register TET
	if(note_TET==null){
		console.error("specify TET")
		return
	}
	var note_number = -1 //silence
	if(freq<0){
		//is a Nuzic special object
		note_number = freq
	}else{
		note_number = Math.round((Math.log2(freq/f_princ)+f_princ_reg)*note_TET)
	}

	return note_number
}

function APP_set_composition_name(name){
	if(name==null){
		name=generic_file_name
	}

	current_file_name = name
	var composition_name = document.getElementById('composition_name')
	composition_name.value = name
}

function disable()
{
 document.onkeydown = function (e)
 {
  return false;
 }
}
function enable()
{
 document.onkeydown = function (e)
 {
  return true;
 }
}


function array_division(vector1,vector2){  //XXX
	if(vector1.length!=vector2.length) return
	var index=0
	var result=[]
	while (index<vector1.length){
		result[index]=vector1[index]/vector2[index]
		index++
	}
	return result
}

function array_moltiplication(vector1,vector2){  //XXX
	if(vector1.length!=vector2.length) return
	var index=0
	var result=[]
	while (index<vector1.length){
		result[index]=vector1[index]*vector2[index]
		index++
	}
	return result
}

function array_sum(vector1,vector2){  //XXX
	if(vector1.length!=vector2.length) return
	var index=0
	var result=[]
	while (index<vector1.length){
		result[index]=vector1[index]+vector2[index]
		index++
	}
	return result
}

function array_difference(vector1,vector2){  //XXX
	if(vector1.length!=vector2.length) return
	var index=0
	var result=[]
	while (index<vector1.length){
		result[index]=vector1[index]-vector2[index]
		index++
	}
	return result
}

function isOdd(num) { return num % 2;}

function APP_set_previous_value(element){
	previous_value= element.value
	//last_focused_input = element;

	//select all text
	element.select()
}

// function APP_note_element_Na_to_Hz(element){
// 	var [note_number,isanote,] = RE_read_Sound_Element(element,"Na")
	//if -3 = ligadura
	//id <0 silence or similar
// 	if(isanote){
// 		return APP_note_to_Hz(note_number,TET)
// 	} else {
// 		return note_number
// 	}
// }

function APP_show_hide_minimap_button(button){
	if(button.value==1){
		APP_set_show_hide_minimap(false)
	} else {
		APP_set_show_hide_minimap(true)
	}
	SNAV_select_zoom(document.querySelector('.App_SNav_select_zoom'))
	DATA_save_preferences_file()
}

function APP_set_show_hide_minimap(show){
	var show_button = document.querySelector("#App_show_minimap")
	var show_button2 = document.querySelector("#App_show_minimap2")
	var show_buttonNav = document.getElementById('Nav_switch_minimap')
	if(show){
		if(show_button.value!=1){
			show_button.value=1
			show_button2.value=1
			show_buttonNav.value=1
			show_buttonNav.children[0].src='./Icons/mini_pmc_active.svg'
			show_button.classList.remove('rotated')
			show_button2.classList.remove('rotated')
			/* show_button.children[0].src="./Icons/ojo_mini_pmc_on.svg" */
			var minimap = document.querySelector('.App_MMap_scroll')
			minimap.style.display = 'flex'
			var upperBox = document.querySelector('.App_MMap')
			upperBox.style.height= '98px'
			//upperBox.style.marginTop = '0px'
			//show_button.children[0].src = "./Icons/ojo_mini_pmc_on.svg"
			var sideCanvas = document.getElementsByClassName('App_MMap_sound_line_canvas')
			sideCanvas[0].style.display='flex'
			sideCanvas[1].style.display='flex'
			APP_refresh_minimap()
		}
	}else{
		if(show_button.value!=0){
			show_button.value=0
			show_button2.value=0
			show_buttonNav.value=0
			show_buttonNav.children[0].src='./Icons/mini_pmc.svg'
			show_button.classList.add('rotated')
			show_button2.classList.add('rotated')
			var minimap = document.querySelector('.App_MMap_scroll')
			minimap.style.display = 'none'
			var upperBox = document.querySelector('.App_MMap')
			upperBox.style.height= '14px'
			//upperBox.style.marginTop='14px'
			//show_button.children[0].src = "./Icons/ojo_mini_pmc_off.svg"
			var sideCanvas = document.getElementsByClassName('App_MMap_sound_line_canvas')
			sideCanvas[0].style.display='none'
			sideCanvas[1].style.display='none'
		}
	}
}
