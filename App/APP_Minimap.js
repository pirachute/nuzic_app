function APP_draw_minimap_layer(time_array,freq_array,minimap_id){
	if(minimap_id == null){return}

	var canvas = document.querySelector(`#${minimap_id}`)
	if(canvas==null){
		//console.error("minimap canvas not found")
		return
	}
	//get what is bigger (Li or compass)
	//see if metronome bigger of Li
	var timeSignature = parseInt(ProgressEndLoop.max/PPM*60*18)
	var minimapContainer = document.querySelector('.App_MMap_minimap')
	minimapContainer.style.width=timeSignature
	canvas.height= canvas.offsetHeight
	canvas.width = timeSignature

	var ctx = canvas.getContext('2d')

	//Less TET specific
	var f_last_note = APP_note_to_Hz((TET*7-1),TET)
	var f_first_note = APP_note_to_Hz(0,TET)
	var value_0 = Math.log(f_first_note)/Math.log(f_last_note)

	var percentual_freq_array = freq_array.map(f=>{
		return _frequency_to_percentage(f)
	})

	for(i=0; i<=percentual_freq_array.length;i++){
		ctx.beginPath()
		ctx.moveTo(Math.ceil(time_array[i]*18),Math.round((1-percentual_freq_array[i])*canvas.offsetHeight))
		ctx.lineTo(Math.ceil(time_array[i+1]*18),Math.round((1-percentual_freq_array[i])*canvas.offsetHeight))
		ctx.strokeStyle = '#43433B'
		ctx.stroke()
	}

	function _frequency_to_percentage(frequency){
		var value = Math.log(frequency)/Math.log(f_last_note)
		var linear = (value- value_0)/(1-value_0)
		return linear
	}
}

function APP_draw_cursor_minimap_canvas(time){
	var canvas = document.getElementById('App_MMap_cursor_canvas')
	var timeSignature = parseInt(ProgressEndLoop.max/PPM*60*18)
	canvas.height= canvas.offsetHeight
	canvas.width = timeSignature
	var position = time/PPM*60*18
	var ctx = canvas.getContext('2d')

	ctx.beginPath()
	ctx.moveTo(Math.round(position),canvas.offsetHeight)
	ctx.lineTo(Math.round(position),0)
	ctx.strokeStyle = '#43433B'
	ctx.stroke()
}

function APP_draw_cursor_minimap_canvas_onPLay(time){
	var pulsation = time*PPM/60
	var canvas = document.getElementById('App_MMap_cursor_canvas')
	var timeSignature = parseInt(ProgressEndLoop.max/PPM*60*18)
	canvas.height= canvas.offsetHeight
	canvas.width = timeSignature
	var position = pulsation/PPM*60*18

	var ctx = canvas.getContext('2d')

	ctx.beginPath()
	ctx.moveTo(Math.round(position),canvas.offsetHeight)
	ctx.lineTo(Math.round(position),0)
	ctx.strokeStyle = '#43433B'
	ctx.stroke()
}

function APP_clear_minimaps(){
	var canvasArr = [...document.querySelectorAll('.App_MMap_minimap_subCanvas')]
	canvasArr.forEach(el=>{
		var ctx= el.getContext('2d')
		ctx.clearRect(0, 0, canvas.width, canvas.height)
	})
}

function APP_refresh_minimap(){
	if(document.querySelector("#App_show_minimap").value != 1)return

	var voice_data=DATA_get_current_state_point(false).voice_data//.find(voice=>{return voice.voice_index==voice_index})
	if(voice_data==null){
		console.error("DATA_get_current_state_point failed")
		return
	}
	var idea_length = Li*60/PPM
	voice_data.forEach((voice)=>{
		var time_array = voice.data.midi_data.time
		//add ending time //
		time_array.push(idea_length)
		var freq_array = voice.data.midi_data.note_freq //XXX voice_data.nzc_note exist
		var minimap_id = "MMvoice"+voice.voice_index
		APP_draw_cursor_minimap_canvas(ProgressBar.value)
		APP_draw_minimap_layer(time_array, freq_array, minimap_id)
	})
}

function APP_create_minimap_canvas(voice_index){
	var container = document.querySelector('.App_MMap_minimap')
	var newMap = document.createElement('canvas')
	newMap.classList.add('App_MMap_minimap_subCanvas')
	var id = "MMvoice"+voice_index
	newMap.setAttribute('id',`${id}`)
	container.appendChild(newMap)
}
function APP_create_cursor_minimap_canvas(){
	var container = document.querySelector('.App_MMap_minimap')
	var newMap = document.createElement('canvas')
	//newMap.classList.add('App_MMap_minimap_subCanvas')
	newMap.setAttribute('id',`App_MMap_cursor_canvas`)
	container.appendChild(newMap)
}

function APP_delete_minimap_canvas(voice_index){
	//OBSOLETE XXX
	var id = "MMvoice"+voice_index
	var container = document.querySelector('.App_MMap_minimap')
	var child = container.querySelector(`#${id}`)
	container.removeChild(child)
}

function APP_clear_minimap(){
	var container = document.querySelector('.App_MMap_minimap')
	container.innerHTML = ''
}

function APP_load_minimap(){
	APP_clear_minimap()
	var voice_data=DATA_get_current_state_point(false).voice_data
	APP_create_cursor_minimap_canvas()
	voice_data.forEach(voice =>{
		APP_create_minimap_canvas(voice.voice_index)
	})
}

function APP_check_minimap_scroll(){
	var container = document.querySelector('.App_MMap_scroll')
	var progressBar = document.querySelector('#progress_bar')
	var minimap = document.querySelector('.App_MMap_minimap_subCanvas')
	var playValue = progressBar.value/ProgressEndLoop.max
	/* console.log(progressBar.value)
	console.log(playValue)
	console.log(minimap.width * playValue) */
	container.scrollLeft = minimap.width*playValue
}

/*UNUSED FUNCTIONS XXX
function APP_reset_minimap_scroll(){
	var container = document.querySelector('.App_MMap_scroll')
	container.scrollLeft = 0
}
function APP_reset_minimap_scroll2(){
	var container = document.querySelector('.App_MMap_scroll')
	container.scrollTo(0,0)
}
*/

