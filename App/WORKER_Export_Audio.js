importScripts("lame.all.js")

this.onmessage = function (e) {
	switch (e.data.command) {
		case 'exportAudio':
			WORKER_generate_audio_blob(e.data.data)
			break;
		case 'test':
			console.log("testing")
			break;
	}
}

function WORKER_generate_audio_blob(data){
	// Generate audio blob
	if(data.type=="mp3"){
		//var mp3_blob = _bufferToMp3(renderedBuffer, offlineAudioCtx.length)
		var mp3_blob = _bufferToMp3(data.channels, data.len, data.sampleRate, data.numOfChan)
		this.postMessage(mp3_blob)
	}else{
		//var wave_blob = _bufferToWave(renderedBuffer, offlineAudioCtx.length)
		var wave_blob = _bufferToWave(data.channels, data.len, data.sampleRate , data.numOfChan)
		this.postMessage(wave_blob)
	}

	// Convert AudioBuffer to a Blob using WAVE representation
	function _bufferToWave(channels, len,sampleRate,numOfChan) {
		var length = len * numOfChan * 2 + 44,
		buffer = new ArrayBuffer(length),
		view = new DataView(buffer),
		sample,
		offset = 0,
		pos = 0;

		// write WAVE header
		setUint32(0x46464952)
		setUint32(length - 8)
		setUint32(0x45564157)
		setUint32(0x20746d66)
		setUint32(16)
		setUint16(1)
		setUint16(numOfChan)
		setUint32(sampleRate)
		setUint32(sampleRate * 2 * numOfChan)
		setUint16(numOfChan * 2)
		setUint16(16)
		setUint32(0x61746164)
		setUint32(length - pos - 4)

		while(pos < length) {
			for(let i = 0; i < numOfChan; i++) {
				sample = Math.max(-1, Math.min(1, channels[i][offset]))
				sample = (0.5 + sample < 0 ? sample * 32768 : sample * 32767)|0
				view.setInt16(pos, sample, true)
				pos += 2
			}
			offset++
		}

		// create Blob
		return new Blob([buffer], {type: "audio/wav"})

		function setUint16(data) {
			view.setUint16(pos, data, true)
			pos += 2
		}

		function setUint32(data) {
			view.setUint32(pos, data, true)
			pos += 4
		}
	}

	function _bufferToMp3(channels, len,sampleRate,numOfChan) {
		var length = len * numOfChan * 2 + 44,
		buffer = new ArrayBuffer(length),
		view = new DataView(buffer),
		sample,
		offset = 0,
		pos = 0;

		// write WAVE header
		setUint32(0x46464952)
		setUint32(length - 8)
		setUint32(0x45564157)
		setUint32(0x20746d66)
		setUint32(16)
		setUint16(1)
		setUint16(numOfChan)
		setUint32(sampleRate)
		setUint32(sampleRate * 2 * numOfChan)
		setUint16(numOfChan * 2)
		setUint16(16)
		setUint32(0x61746164)
		setUint32(length - pos - 4)
		while(pos < length) {
			for(let i = 0; i < numOfChan; i++) {
				sample = Math.max(-1, Math.min(1, channels[i][offset]))
				sample = (0.5 + sample < 0 ? sample * 32768 : sample * 32767)|0
				view.setInt16(pos, sample, true)
				pos += 2
			}
			offset++
		}

		// create Blob
		//return new Blob([buffer], {type: "audio/wav"})
		var wav = lamejs.WavHeader.readHeader(new DataView(buffer))

		//stereo
		var samples = new Int16Array(buffer, wav.dataOffset, wav.dataLen / 2)
		let leftData_samples = [];
		let rightData_samples = [];
		for (let i = 0; i < samples.length; i += 2) {
			leftData_samples.push(samples[i]);
			rightData_samples.push(samples[i + 1]);
		}

		var left_samples = new Int16Array(leftData_samples)
		var right_samples = new Int16Array(rightData_samples)
		var mp3_blob = _wavToMp3_stereo(wav.channels, wav.sampleRate, left_samples,right_samples)
		//var mp3_blob_mono = _wavToMp3_mono(wav.channels, wav.sampleRate, samples)
		return mp3_blob

		function _wavToMp3_mono(channels, sampleRate, samples) {
			var buffer = []
			var mp3enc = new lamejs.Mp3Encoder(channels, sampleRate, 128)
			var remaining = samples.length
			var samplesPerFrame = 1152
			for (var i = 0; remaining >= samplesPerFrame; i += samplesPerFrame) {
				var mono = samples.subarray(i, i + samplesPerFrame)
				var mp3buf = mp3enc.encodeBuffer(mono)//mono.buffer???
				if (mp3buf.length > 0) {
					buffer.push(new Int8Array(mp3buf))
				}
				remaining -= samplesPerFrame
			}
			var d = mp3enc.flush()
			if(d.length > 0){
				buffer.push(new Int8Array(d))
			}
			var mp3Blob = new Blob(buffer, {type: 'audio/mp3'})
			var bUrl = window.URL.createObjectURL(mp3Blob);
			 // send the download link to the console
			console.log('mp3 download:', bUrl);
			return mp3Blob
		}

		function _wavToMp3_stereo(channels, sampleRate, left, right) {
			var buffer = [];
			var mp3enc = new lamejs.Mp3Encoder(channels, sampleRate, 128);
			var remaining = left.length;
			var samplesPerFrame = 1152;
			for (var i = 0; remaining >= samplesPerFrame; i += samplesPerFrame) {
				var leftChunk = left.subarray(i, i + samplesPerFrame);
				var rightChunk = right.subarray(i, i + samplesPerFrame);
				var mp3buf = mp3enc.encodeBuffer(leftChunk,rightChunk);
				if (mp3buf.length > 0) {
					buffer.push(mp3buf);//new Int8Array(mp3buf));
				}
				remaining -= samplesPerFrame;
			}
			var d = mp3enc.flush();
			if(d.length > 0){
				buffer.push(new Int8Array(d));
			}

			var mp3Blob = new Blob(buffer, {type: 'audio/mp3'});
			//var bUrl = window.URL.createObjectURL(mp3Blob);

			// send the download link to the console
			//console.log('mp3 download:', bUrl);
			return mp3Blob;
		}

		function setUint16(data) {
			view.setUint16(pos, data, true)
			pos += 2
		}

		function setUint32(data) {
			view.setUint32(pos, data, true)
			pos += 4
		}
	}

}
