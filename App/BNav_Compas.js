function BNAV_change_ET_tab(){
	//not yet implemented idea compases and community tabs
	// var editor_button = document.getElementById('App_BNav_mod_T_editor')
	// var idea_button = document.getElementById('App_BNav_mod_T_idea')
	// var user_button = document.getElementById('App_BNav_mod_T_user')
	// var community_button = document.getElementById('App_BNav_mod_T_global')
	// var editor_content = document.getElementById('App_BNav_mod_T_content_editor')
	// var idea_content
	// var user_content
	// var community_content

	return
/* XXX to be implemented
	editor_content.style["display"] = "none"
	idea_content.style["display"] = "none"
	user_content.style["display"] = "none"
	community_content.style["display"] = "none"

	if(editor_button.checked){
		editor_content.style["display"] = "flex"
	} else if (idea_button.checked){
		idea_content.style["display"] = "flex"
	} else if (user_button.checked){
		user_content.style["display"] = "flex"
	} else if (community_button.checked){
		community_content.style["display"] = "flex"
	}*/
}

function BNAV_refresh_PcLi(){
	var compasLiRep = document.getElementById('EC_idea_length')
	compasLiRep.innerText = Li

	var compas_length = document.getElementById('EC_compas_length')

	//database
	var compas_data = DATA_get_compas_sequence()
	var last_compas= compas_data.pop()
	var sum = last_compas.compas_values[0]+last_compas.compas_values[1]*last_compas.compas_values[2]
	compas_length.innerHTML = sum
}

function BNAV_save_new_compas_sequence(){
	//read database
	var data = DATA_get_current_state_point(true)

	//read new compas sequence
	data.compas=BNAV_read_current_compas_sequence()

	//save new database
	DATA_insert_new_state_point(data)

	var mom = (JSON.parse(JSON.stringify(current_position_PMCRE)))
	DATA_load_state_point_data(false,true)
	//data load corrupt current_position
	current_position_PMCRE=mom

	//redraw/re align c lines
	//DATA_load_state_point_data(false,true) is this  necessary???
	//BNAV_refresh_C_line()
	//BNAV_refresh_PcLi()
	//APP_redraw_Progress_bar_limits()
}

function BNAV_read_current_compas_sequence(){
	//reading input new compasses
	//BNAV_recalculate_compas_position()
	var tabla_compases = document.querySelector(".App_BNav_mod_T_top_sequence")
	var compas_list = [...tabla_compases.querySelectorAll(".App_BNav_mod_T_sequence_item")]

	var compas_values = compas_list.map(item=>{
		//var arrayOfStrings = item.value.split(',')
		return JSON.parse(item.value)
		//return arrayOfStrings.map(Number)
	})
	//current situation
	return compas_values
}

//this function give all the times (in pulse) a compas start
function BNAV_read_time_all_compas(){
	var compas_values = DATA_get_compas_sequence()
	var compas_change = compas_values.map(item=>{
		var change = [item.compas_values[0]]
		for (var i=1 ; i< item.compas_values[2]; i++){
			//change[i]=change[i-1]+parseInt(item.compas_values[1]) //no need , better change value 1 and 2 into int , not strings
			change[i]=change[i-1]+item.compas_values[1]
		}
		return change
	})

	function sortMatrix (matrix) {
		let newArr = [];
		for(let i = 0; i < matrix.length; i++){
			newArr = newArr.concat(matrix[i]);
		}
		return newArr
	}
	compas_change=sortMatrix(compas_change)

	//current situation
	return compas_change
}

//ADD COMPAS
function BNAV_add_new_compas(){
	var container = document.querySelector(".App_BNav_mod_T_top_sequence")
	var radio_ch = container.querySelector(".checked")

	var selected
	if(radio_ch!=null){
		selected = radio_ch
	}else{
		selected = [...container.querySelectorAll(".App_BNav_mod_T_sequence_item")].pop()
	}
	if(selected.value== '{"compas_values":[0,0,0,[0],0]}'){
		selected.value= '{"compas_values":[0,2,1,[0],0]}'
		BNAV_select_compas(selected)
	}else{
		//create new compas
		var add_radio = document.createElement("button")
		//selected.insertAdjacentHTML( 'afterend', add_radio )
		container.insertBefore(add_radio, selected.nextSibling)
		add_radio.innerHTML = selected.cloneNode(true).innerHTML
		add_radio.value =  '{"compas_values":[0,2,1,[0],0]}'
		add_radio.draggable=true
		add_radio.classList.add("App_BNav_mod_T_sequence_item")

		add_radio.addEventListener('click', () => {
			BNAV_select_compas(add_radio)
		})
		//control position all
		BNAV_recalculate_compas_position()
		BNAV_select_compas(add_radio)

		//draggable
		BNAV_make_compas_div_draggable(add_radio,container)
	}
	BNAV_save_new_compas_sequence()
}

//DELETE COMPAS
function BNAV_delete_selected_compas(){
	var tabla_compases = document.querySelector(".App_BNav_mod_T_top_sequence")
	var compas_list = [...tabla_compases.querySelectorAll(".App_BNav_mod_T_sequence_item")]
	var selected = tabla_compases.querySelector(".checked")
	if (compas_list.length==1){
		//if already void dont do a thing
		if(compas_list[0].value == '{"compas_values":[0,0,0,[0],0]}')return
		BNAV_clear_compas_sequence()
	} else if(compas_list.length>1){
		var index = compas_list.indexOf(selected)
		var next_index=1
		if(index!=0){
			next_index=index-1
		}
		var next_selected=compas_list[next_index]
		tabla_compases.removeChild(selected)
		BNAV_select_compas(next_selected)
		BNAV_recalculate_compas_position()
	}
	BNAV_save_new_compas_sequence()
}

//CLEAR COMPAS SEQUENCE
function BNAV_clear_compas_sequence(){
	var tabla_compases = document.querySelector(".App_BNav_mod_T_top_sequence")
	var compas_list = [...tabla_compases.querySelectorAll(".App_BNav_mod_T_sequence_item")]
	for (var i = 1 ; i<compas_list.length; i++){
		tabla_compases.removeChild(compas_list[i])
	}
	compas_list[0].value = '{"compas_values":[0,0,0,[0],0]}'
	BNAV_change_compas_button_values(compas_list[0])
	BNAV_select_compas(compas_list[0])
}

//SELECT COMPAS
function BNAV_select_compas(element){
	//select current compas from compas window
	var all_radio_ch = [...element.parentNode.querySelectorAll(".checked")]

	all_radio_ch.forEach(item =>{
		item.classList.remove('checked')
	})

	element.classList.add('checked')

	BNAV_change_compas_input_values()
	BNAV_create_compas_accent_buttons()
}

function BNAV_select_compas_number(index){
	var tabla_compases = document.querySelector(".App_BNav_mod_T_top_sequence")
	var compas_list = [...tabla_compases.querySelectorAll(".App_BNav_mod_T_sequence_item")]
	var selected = tabla_compases.querySelector(".checked")
	selected.classList.remove('checked')
	compas_list[index].click()
}

//MODIFY COMPAS
//COMPAS REPETITION
function BNAV_selected_compas_rep_changed(element){
	if(element.value == previous_value) return

	var new_rep_compas = DATA_calculate_inRange(parseInt(element.value),1,99)[0]
	element.value=new_rep_compas

	var tabla_compases = document.querySelector(".App_BNav_mod_T_top_sequence")
	var selected = tabla_compases.querySelector(".checked")
	var compas_value = JSON.parse(selected.value)
	compas_value.compas_values[2] = new_rep_compas
	if(compas_value.compas_values[1]==0){
		compas_value.compas_values[1]=2
		var lon_obj = document.querySelector(".App_BNav_mod_T_compas_lg")
		lon_obj.value = 2
	}
	selected.value = JSON.stringify(compas_value)
	BNAV_change_compas_button_values(selected)
	BNAV_create_compas_accent_buttons()
	BNAV_save_new_compas_sequence()
}

//COMPAS Lg
function BNAV_selected_compas_lg_changed(element){
	if(element.value == previous_value) return

	var new_Lg_compas = DATA_calculate_inRange(parseInt(element.value),2,16)[0]
	element.value=new_Lg_compas

	var tabla_compases = document.querySelector(".App_BNav_mod_T_top_sequence")
	var selected = tabla_compases.querySelector(".checked")
	var compas_value = JSON.parse(selected.value)

	compas_value.compas_values[1] = new_Lg_compas

	if(compas_value.compas_values[2]==0){
		compas_value.compas_values[2]=1
		var rep_obj = document.querySelector(".App_BNav_mod_T_compas_repetition")
		rep_obj.value = 1
	}
	//CLEAR ACCENTS
	compas_value.compas_values[3] = [0]
	compas_value.compas_values[4] = 0

	selected.value = JSON.stringify(compas_value)
	BNAV_change_compas_button_values(selected)
	BNAV_create_compas_accent_buttons()
	BNAV_save_new_compas_sequence()
}

//COMPAS ROTATION
function BNAV_selected_compas_iT_rotation_changed(element) {
	if(element.value == previous_value) return
	var rotation_value = parseInt(element.value)
	var tabla_compases = document.querySelector(".App_BNav_mod_T_top_sequence")
	var selected = tabla_compases.querySelector(".checked")
	var compas_selected = JSON.parse(selected.value)

	rotation_value = rotation_value%compas_selected.compas_values[3].length
	element.value=rotation_value
	if(rotation_value == previous_value) return
	compas_selected.compas_values[4]=rotation_value
	selected.value = JSON.stringify(compas_selected)

	BNAV_create_compas_accent_buttons()
	BNAV_save_new_compas_sequence()
}

//MODIFY ACCENTS
function BNAV_select_accents(button){
	var rotation= document.querySelector(".App_BNav_mod_T_compas_rotation")
	rotation.value=0

	if (button.classList.contains('pressed')) {
		button.classList.remove('pressed')
	} else {
		button.classList.add('pressed')
	}

	//read new sequence
	var tabla_compases = document.querySelector(".App_BNav_mod_T_top_sequence")
	var selected = tabla_compases.querySelector(".checked")
	var compas_selected = JSON.parse(selected.value)
	compas_selected.compas_values[4]=0

	var input_compas = document.querySelector(".App_BNav_mod_T_compas_lg")
	var input_rep = document.querySelector(".App_BNav_mod_T_compas_repetition")
	var input_rot = document.querySelector(".App_BNav_mod_T_compas_rotation")


	var accents_checked = [...document.querySelector("#App_BNav_mod_T_accents").querySelectorAll(".pressed")]
	var accents_array = accents_checked.map(item => {
		return parseInt(item.innerHTML)
	})

	if(accents_array.length==0){
		accents_array=[0]
	}

	compas_selected.compas_values[3] = accents_array
	selected.value = JSON.stringify(compas_selected)

	//update iT and grade lines
	BNAV_create_compas_grade_objects(compas_selected)
	BNAV_create_compas_iT_objects(compas_selected)
	BNAV_calculate_compas_grade(compas_selected)

	//update c line too ??? XXX
	BNAV_save_new_compas_sequence()
}

//DATA
function BNAV_compas_number_to_Properties (C_number){
	var compas_type_values = DATA_get_compas_sequence()

	var compas_type_start_C_number = 0
	var compas_type_end_C_number = 0

	var found = false

	var Lc = null
	var starting_point = null

	compas_type_values.find(compas_type=>{
		///true when founded a compas that start at a number > of compas_number
		compas_type_end_C_number=compas_type_start_C_number+compas_type.compas_values[2]

		if(compas_type_end_C_number>C_number ){
			found=true
			Lc=compas_type.compas_values[1]
			starting_point=compas_type.compas_values[0]+(C_number-compas_type_start_C_number)*Lc
		}

		//next compas will start at
		compas_type_start_C_number=compas_type_end_C_number
		return found
	})

	return [starting_point,Lc]
}

//compas window div
//give a list of compas types
function BNAV_read_compas_accents_sequence(){
	var compas_values_array = DATA_get_compas_sequence()
	var Ti = DATA_truncate_number(60/PPM)
	var sequence = [[0,"Accent"]]
	if(compas_values_array.length==1 && compas_values_array[0].compas_values[1]==0){
		//no compases
		//play an accent at the start and pulsacion next
		for (var i=1; i<Li ; i++){
			sequence[i]=[i*Ti,"Tic"]
		}
	}else{
		//compasses
		var index_P = 0
		compas_values_array.forEach(compas =>{
			var accent_sequence=compas.compas_values[3]
			if(compas.compas_values[4]!=0){
				//recalculate rotated sequence
				accent_sequence.push(compas.compas_values[1])
				var iT_sequence = []
				for (let i = 0; i < accent_sequence.length-1; i++) {
					iT_sequence.push(accent_sequence[i+1]-accent_sequence[i])
				}
				//rotate iT sequence
				var rotation_N = compas.compas_values[4]
				while (rotation_N!=0) {
					iT_sequence.unshift(iT_sequence.pop())
					rotation_N--
				}
				accent_sequence=[0]
				var i=0
				iT_sequence.forEach(iT=>{
					accent_sequence.push(accent_sequence[i]+iT)
					i++
				})
				accent_sequence.pop()
			}
			for (var rep = 0; rep<compas.compas_values[2]; rep++){
				for(var pulse =0;pulse<compas.compas_values[1];pulse++){
					if(pulse==0){
						//first accent
						sequence[index_P]=[index_P*Ti,"Accent"]
					}else{
						//if(compas.compas_values[3].includes(pulse)){
						if(accent_sequence.includes(pulse)){
							sequence[index_P]=[index_P*Ti,"Accent_Weak"]
						}else{
							sequence[index_P]=[index_P*Ti,"Tic"]
						}
					}
					index_P++
				}
			}
		})
	}
	return sequence
}

function BNAV_write_compas_sequence(compas_values){
	var tabla_compases = document.querySelector(".App_BNav_mod_T_top_sequence")

	tabla_compases.innerHTML=""
	compas_values.forEach(compas=>{
		//create new compas
		var add_radio = document.createElement("button")
		tabla_compases.appendChild(add_radio)
		add_radio.innerHTML = `
								<div class="App_BNav_mod_T_sequence_inp_box">
									<svg height="28" width="28" viewBox="0 0 28 28">
										<circle cx="14" cy="14" r="13" stroke="black" stroke-width="2" fill="transparent">
										</circle>
									</svg>
									<div class="App_BNav_mod_T_sequence_item_value">
										<p>0</p>
									</div>
									<div class="App_BNav_mod_T_sequence_item_rep_value">
										<p>0</p>
									</div>
								</div>
								`
		add_radio.value =  JSON.stringify(compas)
		add_radio.draggable=true
		add_radio.classList.add("App_BNav_mod_T_sequence_item")

		add_radio.addEventListener('click', () => {
			BNAV_select_compas(add_radio)
		})

		BNAV_make_compas_div_draggable(add_radio,tabla_compases)
	})

	BNAV_refresh_all_compas_button_values()
	BNAV_refresh_PcLi()
	//select first element
	BNAV_select_compas(tabla_compases.querySelector(".App_BNav_mod_T_sequence_item"))
}

function BNAV_make_compas_div_draggable(draggable,container){
	draggable.addEventListener('dragstart', (e) => {
		draggable.classList.add('dragging')
		draggable.style.border="5px solid var(--Nuzic_light)"
		var canvas = document.createElement('canvas')
		var context = canvas. getContext('2d')
		context. clearRect(0, 0, canvas. width, canvas. height)
		e.dataTransfer.setDragImage(canvas, 10, 10)
	})
	draggable.addEventListener('dragend', () => {
		draggable.classList.remove('dragging')
		BNAV_recalculate_compas_position()
		draggable.style.border=""
		BNAV_save_new_compas_sequence()
	})
	draggable.addEventListener('dragover', e => {
		e.preventDefault()
		var after_element = BNAV_get_compas_drag_after_element(container, e.clientX)
		var draggable = document.querySelector('.dragging')
		//var input = draggable.previousSibling
		if (after_element == null){
			//container.appendChild(input)
			container.appendChild(draggable)
			BNAV_recalculate_compas_position()
		} else {
			//container.insertBefore(input, after_element.previousSibling)
			container.insertBefore(draggable, after_element)
		}
	})
	draggable.addEventListener('dragexit', BNAV_recalculate_compas_position())
}

//from RE or PMC select correct compas
function BNAV_show_compas_input(C_button=null){
	//use compass != null == button pressed to focus on correct compass !!!!!!
	APP_stop()

	var selected = -1
	if(C_button==null){
		//select first compas
		selected=0
	}else{		
		selected= JSON.parse(C_button.value).C_values[1]
	}

	//define wich compas type is selected
	var compas_values = DATA_get_compas_sequence()

	var current_max_compas_type=0
	var current_min_compas_type = 0
	var compas_type_index = 0
	compas_values.find(compas_type =>{
		current_max_compas_type+=compas_type.compas_values[2]
		if(selected>=current_min_compas_type && selected<current_max_compas_type)return true
		current_min_compas_type=current_max_compas_type
		compas_type_index++
		return false
	})

	//verify if found something
	if(compas_type_index>=compas_values.length)compas_type_index=0
	var tab = document.getElementById('App_BNav_tab_ET')
	tab.click()
	BNAV_select_compas_number(compas_type_index)
}

//refresh all compas visible values in radio buttons
function BNAV_refresh_all_compas_button_values(){
	var tabla_compases = document.querySelector(".App_BNav_mod_T_top_sequence")
	var compas_list = [...tabla_compases.querySelectorAll(".App_BNav_mod_T_sequence_item")]
	compas_list.forEach(item=>{
		BNAV_change_compas_button_values(item)
	})
}

//the radio values inside the compas sequence
function BNAV_change_compas_button_values(element){
	//var array = element.value.split(',')
	var array = JSON.parse(element.value)
	element.querySelector(".App_BNav_mod_T_sequence_item_value").querySelector("p").innerHTML=array.compas_values[1]
	element.querySelector(".App_BNav_mod_T_sequence_item_rep_value").querySelector("p").innerHTML=array.compas_values[2]
}

function BNAV_change_compas_input_values(){
	var compas_box = document.querySelector("#App_BNav_mod_T")
	var tabla_compases = compas_box.querySelector(".App_BNav_mod_T_top_sequence")
	var element = tabla_compases.querySelector(".checked")
	var arrayOfNumbers
	if(element==null){
		arrayOfNumbers= JSON.parse('{"compas_values":[0,0,0,[0],0]}')
	} else{
		arrayOfNumbers = JSON.parse(element.value)
	}
	//0 = position
	//1 = length
	//2 = repetitions
	//3 = array accents
	//4 = rotation
	var rep_obj = compas_box.querySelector(".App_BNav_mod_T_compas_repetition")
	var lon_obj = compas_box.querySelector(".App_BNav_mod_T_compas_lg")
	var rot_obj = compas_box.querySelector(".App_BNav_mod_T_compas_rotation")

	rep_obj.value = arrayOfNumbers.compas_values[2]
	lon_obj.value = arrayOfNumbers.compas_values[1]
	rot_obj.value = arrayOfNumbers.compas_values[4]
}

function BNAV_recalculate_compas_position(){
	var tabla_compases = document.querySelector(".App_BNav_mod_T_top_sequence")
	var compas_list = [...tabla_compases.querySelectorAll(".App_BNav_mod_T_sequence_item")]
	var position = 0

	compas_list.forEach(item=>{
		var array = JSON.parse(item.value)
		array.compas_values[0]= position
		position += array.compas_values[1]*array.compas_values[2]
		item.value = JSON.stringify(array)
		BNAV_change_compas_button_values(item)
	})
}

function BNAV_get_compas_drag_after_element(container, x) {
	var draggableElements = [...container.querySelectorAll('.App_BNav_mod_T_sequence_item:not(.dragging)')]

	return draggableElements.reduce((closest, child) => {

		var box = child.getBoundingClientRect()
		var offset = x -box.left - box.width / 2
		//console.log(child)

		if(offset < 0 && offset > closest.offset){

			return { offset: offset, element: child }
		} else {

			return closest
		}
	},{offset: Number.NEGATIVE_INFINITY}).element
}

/* Accents */
function BNAV_create_compas_accent_buttons(){
	var accent_container = document.getElementById('App_BNav_mod_T_accents')
	var last_accent = document.createElement('div')
	last_accent.innerHTML = '0'

	//data
	var tabla_compases = document.querySelector(".App_BNav_mod_T_top_sequence")
	var selected = tabla_compases.querySelector(".checked")
	var compas_selected = JSON.parse(selected.value)
	var lg =compas_selected.compas_values[1]

	accent_container.innerHTML = ''

	var rotated_accent_sequence=JSON.parse(JSON.stringify(compas_selected.compas_values[3]))
	var iT_sequence = []

	//recalculate rotated sequence
	rotated_accent_sequence.push(compas_selected.compas_values[1])

	for (let i = 0; i < rotated_accent_sequence.length-1; i++) {
		iT_sequence.push(rotated_accent_sequence[i+1]-rotated_accent_sequence[i])
	}
	//rotate iT sequence
	var rotation_N = compas_selected.compas_values[4]

	//stay sure it is no rotation == null XXX bug thomas
	// if(rotation_N==null){
	// 	rotation_N=0
	// }

	while (rotation_N!=0) {
		iT_sequence.unshift(iT_sequence.pop())
		rotation_N--
	}
	rotated_accent_sequence=[0]
	var i=0
	iT_sequence.forEach(iT=>{
		rotated_accent_sequence.push(rotated_accent_sequence[i]+iT)
		i++
	})
	rotated_accent_sequence.pop()

	//create accents buttons with rotated sequence
	for (i=0;i<lg;i++){
		var button = document.createElement('button')
		button.innerHTML=i
		button.setAttribute('onclick','BNAV_select_accents(this)')
		button.value = i

		if(rotated_accent_sequence.includes(i)) {
			button.classList.add('pressed')
		}

		if(i==0){
			button.classList.add('pressed')
			button.setAttribute('disabled', 'true')
		}

		accent_container.appendChild(button)
	}
	accent_container.appendChild(last_accent)

	BNAV_create_compas_grade_objects(compas_selected,rotated_accent_sequence)
	BNAV_create_compas_iT_objects(compas_selected,iT_sequence)
	BNAV_calculate_compas_grade(compas_selected)
}

function BNAV_calculate_compas_grade(compas_selected){
	var grades_number = document.querySelector('.App_BNav_mod_T_compas_grade')
	//console.log(compas_selected)
	if(compas_selected.compas_values[1]==0){
		grades_number.value=0
	}else{
		grades_number.value = compas_selected.compas_values[3].length
	}
}

function BNAV_create_compas_grade_objects(compas_selected,rotated_accent_sequence=null){
	var grades_container = document.getElementById('App_BNav_mod_T_gT')
	grades_container.innerHTML = ''

	if(rotated_accent_sequence==null){
		var rotated_accent_sequence=JSON.parse(JSON.stringify(compas_selected.compas_values[3]))
		var iT_sequence = []

		//recalculate rotated sequence
		rotated_accent_sequence.push(compas_selected.compas_values[1])

		for (let i = 0; i < rotated_accent_sequence.length-1; i++) {
			iT_sequence.push(rotated_accent_sequence[i+1]-rotated_accent_sequence[i])
		}
		//rotate iT sequence
		var rotation_N = compas_selected.compas_values[4]
		while (rotation_N!=0) {
			iT_sequence.unshift(iT_sequence.pop())
			rotation_N--
		}
		rotated_accent_sequence=[0]
		var i=0
		iT_sequence.forEach(iT=>{
			rotated_accent_sequence.push(rotated_accent_sequence[i]+iT)
			i++
		})
		rotated_accent_sequence.pop()
	}

	var lg = compas_selected.compas_values[1]
	var g_value=0
	for (i=0;i<lg;i++){
		var div = document.createElement('div')
		if(rotated_accent_sequence.includes(i)) {
			div.innerHTML=g_value
			g_value++
		}
		grades_container.appendChild(div)
	}

	var last_grade = document.createElement('div')
	last_grade.innerHTML = '0'
	grades_container.appendChild(last_grade)
}

function BNAV_create_compas_iT_objects(compas_selected,iT_sequence=null){
	var iT_container= document.getElementById('App_BNav_mod_T_iT')
	iT_container.innerHTML = ''
	if(iT_sequence==null){
		var rotated_accent_sequence=JSON.parse(JSON.stringify(compas_selected.compas_values[3]))
		iT_sequence = []

		//recalculate rotated sequence
		rotated_accent_sequence.push(compas_selected.compas_values[1])

		for (let i = 0; i < rotated_accent_sequence.length-1; i++) {
			iT_sequence.push(rotated_accent_sequence[i+1]-rotated_accent_sequence[i])
		}
		//rotate iT sequence
		var rotation_N = compas_selected.compas_values[4]
		while (rotation_N!=0) {
			iT_sequence.unshift(iT_sequence.pop())
			rotation_N--
		}
		rotated_accent_sequence=[0]
		var i=0
		iT_sequence.forEach(iT=>{
			rotated_accent_sequence.push(rotated_accent_sequence[i]+iT)
			i++
		})
		rotated_accent_sequence.pop()
	}

	var lg = compas_selected.compas_values[1]
	if(iT_sequence.length<=1){
		var last_iT = document.createElement('div')
		if(lg==0)return
		last_iT.innerHTML= lg
		divWidth = 28 * lg
		last_iT.style.width=` ${divWidth}px`
		last_iT.classList.add('colored_iT_div')
		
		var background = BNAV_accent_iT_color(lg,compas_selected.compas_values[1])
		//testColor2(lg)
		last_iT.style.backgroundColor = background
		iT_container.appendChild(last_iT)
	}else{
		iT_sequence.forEach(iT=>{
			var div = document.createElement('div')
			div.innerHTML=iT
			//divWidth = 28 * iT
			div.style.width=` ${(28 * iT)}px`
			var background = BNAV_accent_iT_color(iT,compas_selected.compas_values[1])
			div.style.backgroundColor = background
			iT_container.appendChild(div)
		})
	}
}

function BNAV_accent_iT_color(accent_length,compas_length){	
	if(compas_length==0){
		console.error("TET not passed , not possible to calculate color palette")
		return
	}

	//creating array
	var color_base_array = [{color:[231, 111, 104],position:1},
							{color:[242, 138, 173],position:2},
							{color:[255, 187, 51],position:3},
							{color:[124, 214, 179],position:4},
							{color:[123, 180, 205],position:5}
	]

	//modify array base TET
	var D_iS = Math.floor(compas_length/(2*5))
	if(D_iS>0){
		var position = 1
		for (var i = 0 ; i<5 ; i++){
			color_base_array[i].position=position
			position+=D_iS
		}
	}

	//add last item WHITE???
	color_base_array.push({color:[255, 255, 255],position:compas_length})
	var index=-1
	var next_color = color_base_array.find(color=>{
		index++
		return color.position>=accent_length
	})

	if(next_color.position==accent_length){
		//is a base color
		return (background = `rgb(${next_color.color[0]},${next_color.color[1]},${next_color.color[2]})`)
	}else{
		var prev_color=color_base_array[index-1]
		var a = prev_color.color[0]+Math.floor(Math.abs(next_color.color[0]-prev_color.color[0])/(next_color.position-prev_color.position))*(accent_length-prev_color.position)
		var b = prev_color.color[1]+Math.floor(Math.abs(next_color.color[1]-prev_color.color[1])/(next_color.position-prev_color.position))*(accent_length-prev_color.position)
		var c = prev_color.color[2]+Math.floor(Math.abs(next_color.color[2]-prev_color.color[2])/(next_color.position-prev_color.position))*(accent_length-prev_color.position)
		return (background = `rgb(${a},${b},${c})`)
	}
	/* var a = 0
	var b = 0
	var c = 0

	var portland1 = [217,30,30]
	var portland2 = [242,143,56]
	var portland3 = [242,211,56]
	var portland4 = [10,136,186]
	var portland5 = [12,51,131]

	if(par < 5) {
		a = portland1[0]+(par-1)*(portland2[0]-portland1[0])/4
		b = portland1[1]+(par-1)*(portland2[1]-portland1[1])/4
		c = portland1[2]+(par-1)*(portland2[2]-portland1[2])/4
	} else if (par < 9){
		a = portland2[0]+(par-5)*(portland3[0]-portland2[0])/4
		b = portland2[1]+(par-5)*(portland3[1]-portland2[1])/4
		c = portland2[2]+(par-5)*(portland3[2]-portland2[2])/4
	} else if (par < 14){
		a = portland3[0]+(par-9)*(portland4[0]-portland3[0])/4
		b = portland3[1]+(par-9)*(portland4[1]-portland3[1])/4
		c = portland3[2]+(par-9)*(portland4[2]-portland3[2])/4
	} else {
		a = portland4[0]+(par-12)*(portland5[0]-portland4[0])/4
		b = portland4[1]+(par-12)*(portland5[1]-portland4[1])/4
		c = portland4[2]+(par-12)*(portland5[2]-portland4[2])/4
	}
	return (background = `rgb(${a},${b},${c})`) */
}
