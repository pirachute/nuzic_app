//Global Variables

var PPM = 120
var Li = 60
const max_Li=999
var version= "v1.9.17"
var TET = 12
const generic_file_name = "Sin titulo"
var current_file_name = generic_file_name
const extension = ".nzc"
const extension_old = ".pmc"
const outputRE_Li = document.getElementById("RE_Li")
const outputRE_PPM = document.getElementById("RE_PPM")
const outputRE_Rg =document.getElementById('RE_Rg')
const outputRE_TET = document.getElementById("RE_TET")
const outputPMC_Li = document.getElementById("PMC_Li")
const outputPMC_PPM= document.getElementById('PMC_PPM')
const outputPMC_Rg =document.getElementById('PMC_Rg')
const outputPMC_TET =document.getElementById('PMC_TET')
const output_timer_total = document.getElementById("timer_total")
//const totalT = document.getElementById('totalT')
const output_timer_partial_min = document.getElementById('timer_partial_min')
const output_timer_partial_sec = document.getElementById('timer_partial_sec')
//const outputCurrentPls = document.getElementById("currentPls")
const outputLoop = document.getElementById("Nav_loop")
const outputMainMetronome = document.getElementById("Nav_main_metronome")
const outputMainMetronomeVolume = document.getElementById("main_metronome_volume")
const outputMasterVolume = document.getElementById("master_volume")
const tabREbutton = document.getElementById("App_BNav_tab_RE")
const tabPMCbutton = document.getElementById("App_BNav_tab_PMC")
const ProgressBar = document.getElementById('progress_bar')
const ProgressInLoop = document.getElementById('init_loop')
const ProgressEndLoop = document.getElementById('end_loop')
//PMC canvas and svg
const PMC_progress_svg = document.getElementById('App_PMC_progress_svg')
const PMC_progress_line = document.getElementById('App_PMC_progress_line')
const PMC_svg_selection_background = document.getElementById('App_PMC_svg_selection_background')
const PMC_svg_selection_elements = document.getElementById('App_PMC_svg_selection_elements')
const PMC_playing_notes_svg = document.getElementById('App_PMC_playing_notes_svg')
const PMC_mouse_action_svg_1 = document.getElementById('App_PMC_mouse_action_svg_1')
const PMC_mouse_action_svg_2 = document.getElementById('App_PMC_mouse_action_svg_2')
const PMC_mouse_action_time_svg = document.getElementById('App_PMC_mouse_action_time_svg')
const PMC_mouse_action_sound_svg = document.getElementById('App_PMC_mouse_action_sound_svg')
const PMC_selection_time_svg = document.getElementById('App_PMC_selection_time_svg')
const PMC_mouse_action_x = document.getElementById('App_PMC_mouse_action_x')
const PMC_mouse_action_y = document.getElementById('App_PMC_mouse_action_y')
const PMC_mouse_action_y_text = document.getElementById('App_PMC_mouse_action_y_text')

const PMC_mouse_action_drag = document.getElementById('App_PMC_mouse_action_drag')
const PMC_mouse_action_drag_element = document.getElementById('App_PMC_mouse_action_drag_element')

const PMC_main_scrollbar_H = document.querySelector(".App_PMC_main_scrollbar_H")
const PMC_canvas_container = document.querySelector(".App_PMC_canvas_container")
const PMC_svg_compas = document.getElementById('App_PMC_svg_compas')
const PMC_svg_scale = document.getElementById('App_PMC_svg_scale')
const PMC_canvas_dots = document.getElementById('App_PMC_canvas_dots')
const PMC_canvas_dots_container = document.getElementById('App_PMC_canvas_dots_container')
//const PMC_svg_dots = document.getElementById('App_PMC_svg_dots')
//const PMC_svg_dots_pooling = document.getElementById('App_PMC_svg_dots_pooling')
const PMC_svg_segment = document.getElementById('App_PMC_svg_segment')
//const PMC_canvas_all_voices = document.getElementById('App_PMC_canvas_all_voices')
const PMC_svg_all_voices = document.getElementById('App_PMC_svg_all_voices')

var showhelp = false

var flag_DATA_updated={PMC:false,RE:false}

var previous_value=0
var calc_prev_inp 
const ABC_note_list_diesis = ["C","C♯","D","D♯","E","F","F♯","G","G♯","A","A♯","B"]
const ABC_note_list_bemolle = ["C","D♭","D","E♭","E","F","G♭","G","A♭","A","B♭","B"]

var virtualPianoOctave = 3
var virtualPianoNote = 0
var cromaticKb = false
var keyboard_iT=1
var keyboard_iT_lock = false
var keyboard_notes_lock = false
//var last_focused_input
var current_position_PMCRE = null

//init data_array
var state_point_array = new Array(100)
var current_state_point_index = 0
var max_state_point_index = 0
var min_state_point_index = 0

//copy paste
//ID == segment , voice, lineiD
var copied_data =  {"object":null,"id":""}


//ALIGNMENT XXX
var RE_global_time_all_changes // == global_time_all_changes
var RE_global_time_voices_changes
var RE_global_note_voices_changes
var RE_global_time_segment_change // == global time segment_changes

//Graph var
var maincanvas = document.getElementById('maincanvas')

//Mouse Check

//Char
var end_range = "•"
//var no_value = "·"
//var no_value = "٭"
//var no_value = "★"
//var no_value = "☆"
var no_value = "⸰"

//var no_value = "-"
//var no_value_iA = "๏"
//var no_value_iA = "⭐"
//var no_value_iA = "◇"
var no_value_iA = "△"
//var no_value_iA = "∤"
//var no_value_iA = "⍟"
//smaller •
var tie_intervals = "L"
var tie_intervals_m = "l"

//user
var user = null

//Sound and Player variables
var soundsindexP = 0
var soundsindexF = 0
var soundsindexM = 0
var instrumentindex = 0
var firstinstrumentloaded = false

if(document.getElementById("play_button")!=null)document.getElementById("play_button").disabled = true

//PMC
var PMC_zoom_x	= 1
var PMC_zoom_y = 1
var PMC_extra_x_space = 5
var PMC_segment_name_prevent_default = false
//PMC dragging steps counter
var PMC_drag_x_steps =0
var PMC_drag_y_steps =0

//CSS VARIABLES
var r = document.querySelector(':root')
rs = getComputedStyle(r)
var block_string = rs.getPropertyValue('--RE_block')
const nuzic_block = parseInt(block_string,10)
const nuzic_block_half = parseInt(rs.getPropertyValue('--RE_block_half'),10)

const nuzic_dark = rs.getPropertyValue('--Nuzic_dark')
const nuzic_light = rs.getPropertyValue('--Nuzic_light')
const nuzic_white = rs.getPropertyValue('--Nuzic_white')
const nuzic_grey = rs.getPropertyValue('--Nuzic_grey')
const nuzic_blue = rs.getPropertyValue('--Nuzic_blue')
const nuzic_blue_light = rs.getPropertyValue('--Nuzic_blue_light')
const nuzic_red = rs.getPropertyValue('--Nuzic_red')
const nuzic_red_light = rs.getPropertyValue('--Nuzic_red_light')
const nuzic_green = rs.getPropertyValue('--Nuzic_green')
const nuzic_green_light = rs.getPropertyValue('--Nuzic_green_light')
const nuzic_yellow = rs.getPropertyValue('--Nuzic_yellow')
const nuzic_yellow_light = rs.getPropertyValue('--Nuzic_yellow_light')
const nuzic_pink = rs.getPropertyValue('--Nuzic_pink')
const nuzic_pink_light = rs.getPropertyValue('--Nuzic_pink_light')

//ticks por quarter note???
//better to calculate it manually
//Tone.Transport.PPQ=2520

const masterVolume = new Tone.Volume(0).toDestination()
const mainMetronomeVolume = new Tone.Volume(0).connect(masterVolume)

//no multi channel like sampler https://cdn.nuzic.org
var keysPulse
//Buffer //USE ONLY ONE /// use battery
const buffer_keysPulse = new Tone.Buffers({
	"Pulse1" : "https://cdn.nuzic.org/Notes/pulso.wav",
	"Pulse2" : "https://cdn.nuzic.org/Notes/acento.[wav|ogg]",
	"Pulse3" : "https://cdn.nuzic.org/Notes/acento_weak.[wav|ogg]",
	"Pulse4" : "https://cdn.nuzic.org/Notes/pulsacion.wav",
//}, )
}, function(){
	keysPulse=APP_sound_keysPulse().connect(masterVolume)
	APP_ready_pulse_sounds()
})

function APP_sound_keysPulse(){//use only one
	var data = new Tone.Players({
		"Pulse1" : buffer_keysPulse.get("Pulse1"),
		"Pulse2" : buffer_keysPulse.get("Pulse2"),
		"Pulse3" : buffer_keysPulse.get("Pulse3"),
		"Pulse4" : buffer_keysPulse.get("Pulse4"),
	})
	return data
}

var keysMainMetronome
//Buffer
const buffer_keysMainMetronome = new Tone.Buffers({
	"Accent" : "https://cdn.nuzic.org/Notes/MainMetronome/Click_53_high.wav",
	"Accent_Weak" : "https://cdn.nuzic.org/Notes/MainMetronome/Click_51.wav",
	"Tic" : "https://cdn.nuzic.org/Notes/MainMetronome/Click_49.wav",
}, function(){
	keysMainMetronome=APP_sound_keysMainMetronome().connect(mainMetronomeVolume)
	APP_ready_metronome()
})

function APP_sound_keysMainMetronome(){
	var data = new Tone.Players({
		// "Accent" : "https://cdn.nuzic.org/Notes/MainMetronome/Click_53.wav",
		"Accent": buffer_keysMainMetronome.get("Accent"),
		"Accent_Weak": buffer_keysMainMetronome.get("Accent_Weak"),
		"Tic": buffer_keysMainMetronome.get("Tic"),
		// "Accent" : "https://cdn.nuzic.org/Notes/MainMetronome/Click_40.wav",
		// "Accent_Weak" : "https://cdn.nuzic.org/Notes/MainMetronome/Click_40_low.wav",
		// "Tic" : "https://cdn.nuzic.org/Notes/MainMetronome/Click_36.wav",
	})//maybe here return data??
	return data
}

//multimetronome sounds:
var keysFractionedMetronome
//Buffer
const buffer_keysFractionedMetronome = new Tone.Buffers({
	"1" : "https://cdn.nuzic.org/Notes/Percussions/69.wav",//cabasa
	"2" : "https://cdn.nuzic.org/Notes/Percussions/70.wav",//maracas
	"3" : "https://cdn.nuzic.org/Notes/Percussions/54.wav",//tambourine
	"4" : "https://cdn.nuzic.org/Notes/Percussions/41.wav"//closed hi hat
	//"5" : "https://cdn.nuzic.org/Notes/Percussion/69.wav",//one less
}, function(){
	keysFractionedMetronome=APP_sound_keysFractionedMetronome().connect(masterVolume)
	APP_ready_fractioning_metronome()
})

function APP_sound_keysFractionedMetronome(){
	var data = new Tone.Players({
		"1" : buffer_keysFractionedMetronome.get("1"),
		"2" : buffer_keysFractionedMetronome.get("2"),
		"3" : buffer_keysFractionedMetronome.get("3"),
		"4" : buffer_keysFractionedMetronome.get("4"),
		//"5" : buffer_keysFractionedMetronome.get("5"),
	})//maybe here return data??
	return data
}

//INSTRUMENTS

APP_disable_voice_instrument_buttons()
function APP_disable_voice_instrument_buttons(){
	var change_instrument_buttons = [...document.querySelectorAll(".App_RE_voice_instrument")]
	change_instrument_buttons.forEach(item=>{
		item.disabled=true
	})
}

//0
const instrument = []
var instrument_order = [0,5,1,2,3,4]
//var allinstruments = 6  //max index
var allinstruments = instrument_order.length  //max in

var instrument_icon_list = ["./Icons/keyboard.svg",
	"./Icons/drums.svg",
	"./Icons/synth_1.svg",
	"./Icons/synth_2.svg",
	"./Icons/synth_3.svg",
	"./Icons/keyboard_electric.svg"]

var instrument_name_list = ["Piano",
	"Perc.",
	"Synth 1",
	"Synth 2",
	"Synth_3",
	"Piano E"
	]

function APP_sound_instrument(instrument_number){
	switch (instrument_number) {
		case 0:
			return APP_sound_instrument_0()
		break;
		case 1:
			return APP_sound_instrument_1()
		break;
		case 2:
			return APP_sound_instrument_2()
		break;
		case 3:
			return APP_sound_instrument_3()
		break;
		case 4:
			return APP_sound_instrument_4()
		break;
		case 5:
			return APP_sound_instrument_5()
		break;
	}

}

//instruments buffers 0(salamander) 1(drums) 5(electric piano)

//salamander
//tone 14.7.77
const buffer_instrument_0  = new Tone.Buffers({
	urls: {
		"A0": "A0.mp3",
		"C1": "C1.mp3",
		"D#1": "Ds1.mp3",
		"F#1": "Fs1.mp3",
		"A1": "A1.mp3",
		"C2": "C2.mp3",
		"D#2": "Ds2.mp3",
		"F#2": "Fs2.mp3",
		"A2": "A2.mp3",
		"C3": "C3.mp3",
		"D#3": "Ds3.mp3",
		"F#3": "Fs3.mp3",
		"A3": "A3.mp3",
		"C4": "C4.mp3",
		"D#4": "Ds4.mp3",
		"F#4": "Fs4.mp3",
		"A4": "A4.mp3",
		"C5": "C5.mp3",
		"D#5": "Ds5.mp3",
		"F#5": "Fs5.mp3",
		"A5": "A5.mp3",
		"C6": "C6.mp3",
		"D#6": "Ds6.mp3",
		"F#6": "Fs6.mp3",
		"A6": "A6.mp3",
		"C7": "C7.mp3",
		"D#7": "Ds7.mp3",
		"F#7": "Fs7.mp3",
		"A7": "A7.mp3",
		"C8": "C8.mp3"
	},
	baseUrl: "https://cdn.nuzic.org/Notes/Salamander/",
	onload: () => {
		instrument[0] = APP_sound_instrument_0().connect(masterVolume)
		APP_ready_first_instrument()
	}
})

function APP_sound_instrument_0(){
	var data = new Tone.Sampler({
		release: 1
	})
	buffer_instrument_0._buffers.forEach((buffer,entry) => {
		data.add(entry,buffer)
	})
	return data
}

//1
//Percussions
const buffer_instrument_1 = new Tone.Buffers({
	urls: {
			"23":"35.wav",
			"24":"36.wav",
			"25":"37.wav",
			"26":"38.wav",
			"27":"39.wav",
			"28":"40.wav",
			"29":"41.wav",
			"30":"42.wav",
			"31":"43.wav",
			"32":"44.wav",
			"33":"45.wav",
			"34":"46.wav",
			"35":"47.wav",
			"36":"48.wav",
			"37":"49.wav",
			"38":"50.wav",
			"39":"51.wav",
			"40":"52.wav",
			"41":"53.wav",
			"42":"54.wav",
			"43":"55.wav",
			"44":"56.wav",
			"45":"57.wav",
			"46":"58.wav",
			"47":"59.wav",
			"48":"60.wav",
			"49":"61.wav",
			"50":"62.wav",
			"51":"63.wav",
			"52":"64.wav",
			"53":"65.wav",
			"54":"66.wav",
			"55":"67.wav",
			"56":"68.wav",
			"57":"69.wav",
			"58":"70.wav",
			"59":"71.wav",
			"60":"72.wav",
			"61":"73.wav",
			"62":"74.wav",
			"63":"75.wav",
			"64":"76.wav",
			"65":"77.wav",
			"66":"78.wav",
			"67":"79.wav",
			"68":"80.wav",
			"69":"81.wav"
	},
	baseUrl: "https://cdn.nuzic.org/Notes/Percussions/",
	onload: () => {
		instrument[1] = APP_sound_instrument_1().connect(masterVolume)
		APP_ready_instrument()
	}
})

function APP_sound_instrument_1(){
	var data = new Tone.Players({
		release: 1
	})
	buffer_instrument_1._buffers.forEach((buffer,entry) => {
		data.add(entry,buffer)
	})
	return data
}

//2
//AMSynth
function APP_sound_instrument_2(){
	var data = new Tone.PolySynth(Tone.AMSynth, {
		"volume": 20,
		"detune": 0,
		"portamento": 0,
		"harmonicity": 0,
		"oscillator": {
			"partialCount": 3,
			"partials": [
				0.586181640625,
				0.012345679012345684,
				0.0018838011188271615
			],
			"phase": 5,
			"type": "custom"
		},
		"envelope": {
			"attack": 0.1,
			"attackCurve": "linear",
			"decay": 3,
			"decayCurve": "exponential",
			"release": 0.3,
			"releaseCurve": "exponential",
			"sustain": 0.2
		},
		"modulation": {
			"partialCount": 2,
			"partials": [
				0.19753086419753094,
				0.586181640625
			],
			"phase": 0,
			"type": "custom"
		},
		"modulationEnvelope": {
			"attack": 0.1,
			"attackCurve": "linear",
			"decay": 0.01,
			"decayCurve": "exponential",
			"release": 0.5,
			"releaseCurve": "exponential",
			"sustain": 1
		}
	})
	return data
}

instrument[2] = APP_sound_instrument_2().connect(masterVolume)
APP_ready_instrument()

//3
//FMSynth
function APP_sound_instrument_3(){
	var data = new Tone.PolySynth(Tone.FMSynth, {
		"volume": 0,
		"detune": 0,
		"portamento": 0,
		"harmonicity": 3,
		"oscillator": {
			"partialCount": 10,
			"partials": [
				1,
				1,
				0.7060667438271603,
				0.0625,
				0.152587890625,
				0.044129171489197545,
				0.0007716049382716042,
				0.030140817901234556,
				0.00390625,
				0.000244140625
			],
			"phase": 0,
			"type": "custom"
		},
		"envelope": {
			"attack": 0.01,
			"attackCurve": "linear",
			"decay": 0.2,
			"decayCurve": "exponential",
			"release": 0.5,
			"releaseCurve": "exponential",
			"sustain": 1
		},
		"modulation": {
			"partialCount": 0,
			"partials": [],
			"phase": 0,
			"type": "square"
		},
		"modulationEnvelope": {
			"attack": 0.5,
			"attackCurve": "linear",
			"decay": 3,
			"decayCurve": "exponential",
			"release": 0.5,
			"releaseCurve": "exponential",
			"sustain": 0
		},
		"modulationIndex": 12.22,
	})
	return data
}

instrument[3] = APP_sound_instrument_3().connect(masterVolume)
APP_ready_instrument()

//4
//Synth
function APP_sound_instrument_4(){
	var data = new Tone.PolySynth(Tone.Synth, {
		"volume": 0,
		"detune": 0,
		"portamento": 0.05,
		"envelope": {
			"attack": 0.6,
			"attackCurve": "exponential",
			"decay": 2,
			"decayCurve": "exponential",
			"release": 0.1,
			"releaseCurve": "exponential",
			"sustain": 0.5
		},
		"oscillator": {
			"partialCount": 4,
			"partials": [
				1,
				0.31640625,
				0.11578896604938264,
				0.012345679012345684
			],
			"phase": 2,
			"type": "amcustom",
			"harmonicity": 1.5,
			"modulationType": "sine"
		}
	})
	return data
}

instrument[4] = APP_sound_instrument_4().connect(masterVolume)
APP_ready_instrument()

const buffer_instrument_5 = new Tone.Buffers({
	urls: {
		"24":"midi_24.mp3",
		"28":"midi_28.mp3",
		"31":"midi_31.mp3",
		"35":"midi_35.mp3",
		"36":"midi_36.mp3",
		"40":"midi_40.mp3",
		"43":"midi_43.mp3",
		"47":"midi_47.mp3",
		"48":"midi_48.mp3",
		"52":"midi_52.mp3",
		"55":"midi_55.mp3",
		"59":"midi_59.mp3",
		"60":"midi_60.mp3",
		"62":"midi_62.mp3",
		"65":"midi_65.mp3",
		"69":"midi_69.mp3",
		"72":"midi_72.mp3",
		"76":"midi_76.mp3",
		"79":"midi_79.mp3",
		"83":"midi_83.mp3",
		"88":"midi_88.mp3",
		"91":"midi_91.mp3"
		// "12":"midi_24.mp3",
		// "16":"midi_28.mp3",
		// "19":"midi_31.mp3",
		// "23":"midi_35.mp3",
		// "24":"midi_36.mp3",
		// "28":"midi_40.mp3",
		// "31":"midi_43.mp3",
		// "35":"midi_47.mp3",
		// "36":"midi_48.mp3",
		// "40":"midi_52.mp3",
		// "43":"midi_55.mp3",
		// "47":"midi_59.mp3",
		// "48":"midi_60.mp3",
		// "50":"midi_62.mp3",
		// "53":"midi_65.mp3",
		// "57":"midi_69.mp3",
		// "60":"midi_72.mp3",
		// "64":"midi_76.mp3",
		// "67":"midi_79.mp3",
		// "71":"midi_83.mp3",
		// "76":"midi_88.mp3",
		// "79":"midi_91.mp3"
	},
	baseUrl: "https://cdn.nuzic.org/Notes/Rhodes/",
	onload: () => {
		instrument[5] = APP_sound_instrument_5().connect(masterVolume)
		APP_ready_instrument()
	}
})

function APP_sound_instrument_5(){
	var data = new Tone.Sampler({
		release: 1
	})
	buffer_instrument_5._buffers.forEach((buffer,entry) => {
		data.add(entry,buffer)
	})
	return data
}

var part = new Tone.Part()
var stopper = new Tone.Part()

function APP_ready_pulse_sounds(){
	soundsindexP++;
	//players
	console.info(`all sounds Pulse have been loaded`)
	if(soundsindexP==1 && soundsindexM==1 && soundsindexF==1 && firstinstrumentloaded){
		//activate play button
		var playbutton = document.getElementById("playbutton")
		var playbuttonlabel = document.getElementById("playbuttonlabel")
		playbutton.disabled = false
		playbuttonlabel.innerHTML = "Play"
	}
}

function APP_ready_fractioning_metronome(){
	soundsindexF++;
	//players
	console.info(`all sounds Frac Metro have been loaded`)
	if(soundsindexP==1 && soundsindexM==1 && soundsindexF==1 && firstinstrumentloaded){
		//activate play button
		var playbutton = document.getElementById("playbutton")
		var playbuttonlabel = document.getElementById("playbuttonlabel")
		playbutton.disabled = false
		//playbuttonlabel.innerHTML = '<img class="RE_icon" src="./Icons/play.svg">'
	}
}

function APP_ready_metronome(){
	//main metronome sound
	soundsindexM++;
	//players
	console.info(`all sounds Main Metro have been loaded`)
	if(soundsindexP==1 && soundsindexM==1 && soundsindexF==1 && firstinstrumentloaded){
		//activate play button
		var playbutton = document.getElementById("playbutton")
		var playbuttonlabel = document.getElementById("playbuttonlabel")
		playbutton.disabled = false
		//playbuttonlabel.innerHTML = '<img class="RE_icon" src="./Icons/play.svg">'
	}
}

function APP_ready_instrument(){
	//console.log("ready instrument "+instrumentindex)
	instrumentindex++;
	//players
	if(instrumentindex==allinstruments){
		console.log(`all note have been loaded`)
		//abilitate all RE "change instruments buttons"
		var change_instrument_buttons = [...document.querySelector(".App_RE_voice_matrix").querySelectorAll(".App_RE_voice_instrument")]
		change_instrument_buttons.forEach(item=>{
			item.disabled=false
		})

		//abilitate PMC "change instrument button"
		document.querySelector(".App_PMC").querySelector(".App_PMC_selected_voice_instrument").disabled=false
	}

	if(APP_verify_sound_ready()){
		//activate play button
		var playbutton = document.getElementById("play_button")
		if(playbutton!=null){
			var playbuttonlabel = document.querySelector(".App_Nav_play_button")
			playbutton.disabled = false
			playbuttonlabel.innerHTML = '<img class="RE_icon" src="./Icons/main_play.svg">'
		}
	}
}

function APP_ready_first_instrument(){
	firstinstrumentloaded = true
	//players
	console.info("Base instrument loaded")
	// if(soundsindexP==1 && soundsindexM==1 && soundsindexF==1 && firstinstrumentloaded){
		//activate play button only on first instrument???
	// 	var playbutton = document.getElementById("play_button")
	// 	if(playbutton!=null){
	// 		var playbuttonlabel = document.querySelector(".App_Nav_play_button")
	// 		playbutton.disabled = false
	// 		playbuttonlabel.innerHTML = '<img class="RE_icon" src="./Icons/main_play.svg">'
	// 	}

		//download other instruments
	// 	APP_download_all_instruments()
	// }
	APP_ready_instrument()
}

function APP_verify_sound_ready(){
	if(soundsindexP==1 && soundsindexM==1 && soundsindexF==1 && firstinstrumentloaded && instrumentindex==allinstruments){
		return true
	}else{
		return false
	}
}


//User and global variables SCALE

// tet,name,iS,module,tags
var global_scale_list = []
var user_scale_list = []

// scale_list =  tet,name,iS,letter,module,tags   ; TET_list = tet used
var idea_scale_list = {scale_list: [],
		TET_list : [TET]
	}

var editor_scale = false

//bookmarks
var global_scale_list_bookmarks = []


// function Disconnect_instruments(){
// 	instrument.forEach(inst=>{
		//inst.disconnect()
		//inst.unsync()
// 	})
// }

function APP_connect_first_instrument(){
 	//instrument.forEach(inst=>{
		//inst.toDestination()
		//console.log(inst)
		//inst.sync()
// 	})
	//instrument[0].toDestination()
	instrument[0].connect(masterVolume)
	//if(instrument.length>=6)instrument[5].connect(masterVolume)
	if(instrument[5]==null)return
	instrument[5].connect(masterVolume)
}


//instead of disconnecting try to release or stop all
function APP_stop_all_notes(){
	instrument[0].releaseAll() //sampler -> XXX not working as expected

	//destroy and recreate instrument   XXX NOT GOOD, SLOW AND BAD!
	//instrument[0].dispose() //destroy the instrument
	//recreate the instrument

	//only working solution (bad XXX there is still a ghost)
	instrument[0].disconnect()
	//instrument[0].toDestination()//need to reconnect when replay


	instrument[1].stopAll()		//player

	instrument[2].sync()
	instrument[2].releaseAll()	//polysinth
	instrument[2].unsync()
	instrument[3].sync()
	instrument[3].releaseAll()	//polysinth
	instrument[3].unsync()
	instrument[4].sync()
	instrument[4].releaseAll()	//polysinth
	instrument[4].unsync()
	//console.error("muting all notes")
	//if(instrument.length>=6)instrument[5].releaseAll()
	instrument[5].releaseAll() //sampler -> XXX not working as expected
	//only working solution (bad XXX there is still a ghost)
	instrument[5].disconnect()
}

