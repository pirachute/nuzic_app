function on_MIDI_Success(midiAccess) {
	console.log("MIDI ready")
	console.error("OBSOLETE FUNCTION , ADD TO DATABASEEEEEEE")
	var MIDI_info_label = document.getElementById("MIDI_info_label")
	MIDI_info_label.innerHTML = "No MIDI connected"

	Array.from(midiAccess.inputs).forEach(input => {
		//console.log(input)
		//var myID="D1DE5D0FCE3B68C586476520A2E3295BB508E7FB09C6C131E272B6F8616E5523"
		//XXX depending on what is connected
		var MIDI_info_label = document.getElementById("MIDI_info_label")
		MIDI_info_label.innerHTML = input[1].manufacturer+" "+input[1].name
		input[1].onmidimessage = (msg) => {
			// console.log(input)
			// console.log(msg)

			//var instrument_index=0
			//var nzc_note = msg.data[1]-TET
			var nzc_note = msg.data[1]-12
			//var freq_note = APP_note_to_Hz(nzc_note,TET)

			//button play pause stop
			if(msg.data[0]==191 && msg.data[1]>106){
				//play
				var playbutton = document.getElementById("playbutton")
				playbitton.click()
				return
			}
			if(msg.data[0]==191 && msg.data[1]>105){
				//Stop
				APP_stop()
				return
			}


			//verify is a note
			if(msg.data[0]<144 || msg.data[0]>159)return //16 channels

			var focused_elem = document.activeElement
			if(msg.data[2]!=0){
				//keydown
				//console.log("midi note "+msg.data[1]+"  nuzic note "+nzc_note)

				//add a note to the current voice segment
				Add_MIDI_note_to_RE(focused_elem,nzc_note,true,1,msg.data[2]/128)
			}else{
				//key up
				Add_MIDI_note_to_RE(focused_elem,nzc_note,false)
			}
			//console.log(msg)
			//Play_this_note(iA_elements.down_Na,"Na")

		}//*/
	})
}

function on_MIDI_Failure(msg) {
	console.log("Failed to get MIDI access - " + msg)
	var MIDI_info_label = document.getElementById("MIDI_info_label")
	MIDI_info_label.innerHTML = "Failed to get MIDI access - " + msg
}

if (navigator.requestMIDIAccess) {
	navigator.requestMIDIAccess().then( on_MIDI_Success, on_MIDI_Failure)
} else {
	//alert("No MIDI support in your browser (YET).\nTry using Chrome")
	var MIDI_info_label = document.getElementById("MIDI_info_label")
	MIDI_info_label.innerHTML = "No MIDI support in your browser (YET).\n Try using Chrome\n Firefox Nightly webMIDI in development"
}

// Computer keys
let key_pressed = {}
function Switch_keyboard_as_MIDI_controller(){
	var button = document.getElementById("compKeyBttn")
	var use_keyboard_as_MIDI_controller=!(button.value=="true")
	var kbDiv = document.querySelector('.App_BNav_keyboard_container_selected_keys')
	
	button.value=use_keyboard_as_MIDI_controller
	if(use_keyboard_as_MIDI_controller){
		button.innerHTML=`<img src="./Icons/kb-teclas-on.svg">`
		document.addEventListener("keydown", on_computer_keyboard_MIDI_Success)
		document.addEventListener("keyup", on_computer_keyboard_MIDI_Success)
		kbDiv.style.background = nuzic_pink
		button.setAttribute('data-value','1')
		BNAV_keyboard_keys_visualFeedback()
	}else{
		button.innerHTML=`<img src="./Icons/kb-teclas.svg">`
		document.removeEventListener("keydown", on_computer_keyboard_MIDI_Success)
		document.removeEventListener("keyup", on_computer_keyboard_MIDI_Success)
		kbDiv.style.background = nuzic_light
		BNAV_keyboard_clear_keys_visualFeedback()
		button.setAttribute('data-value','0')
	}
}


function on_computer_keyboard_MIDI_Success(event) {
	//when a keyboard is pressed act like a virtual MIDI keiboard
	//stop propagation
	//RE_disableEvent(event) //after

	//if (event.repeat)return
	if (key_pressed[event.which] && event.type=="keydown")return
	//console.log(event)
	var nzc_note=null
	if(cromaticKb){
		if(virtualPianoOctave!=6){
		if(virtualPianoNote%2==0){
			switch (event.key) {
				case "a":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+0
				break
				case "w":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+1
				break
				case "s":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+2
				break
				case "e":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+3
				break
				case "d":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+4
				break
				case "r":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+5
				break
				case "f":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+6
				break
				case "t":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+7
				break
				case "g":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+8
				break
				case "y":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+9
				break
				case "h":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+10
				break
				case "u":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+11
				break				
				case "j":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+12
				break				
				case ".":
					if(event.type=="keydown")BNAV_increase_virtual_piano_octave()
				break
				case ",":
					if(event.type=="keydown")BNAV_decrease_virtual_piano_octave()
				break
				case ">":
					if(event.type=="keydown")BNAV_keyboard_increase_note()
				break
				case "<":
					if(event.type=="keydown")BNAV_keyboard_decrease_note()
				break
				
				case "z":
					nzc_note = -1
					
				break
				case "x":
					nzc_note = -2
					
				break
				case "c":
					nzc_note = -3
					break
				case "1":
					//change iT
					if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[1])
				break
				case "2":
					//change iT
					if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[2])
				break
				case "3":
					//change iT
					if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[3])
					break
				case "4":
					//change iT
					if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[4])
					break
				case "5":
					//change iT
					if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[5])
					break
				case "6":
					//change iT
					if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[6])
					break
				case "7":
					//change iT
					if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[7])
					break
				case "8":
					//change iT
					if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[8])
					break
				case "9":
					//change iT
					if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[9])
					break
				case "0":
					//change iT
					if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[10])
					break
				default:
					
					return
				}
		}else{
			switch (event.key) {
				case "q":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+0
					break
				case "a":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+1
				break
				case "w":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+2
				break
				case "s":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+3
				break
				case "e":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+4
				break
				case "d":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+5
				break
				case "r":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+6
				break
				case "f":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+7
				break
				case "t":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+8
				break
				case "g":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+9
				break
				case "y":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+10
				break
				case "h":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+11
				break
				case "u":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+12
				break
				
				case ".":
					if(event.type=="keydown")BNAV_increase_virtual_piano_octave()
				break
				case ",":
					if(event.type=="keydown")BNAV_decrease_virtual_piano_octave()
				break
				case ">":
					if(event.type=="keydown")BNAV_keyboard_increase_note()
				break
				case "<":
					if(event.type=="keydown")BNAV_keyboard_decrease_note()
				break
				case "z":
					nzc_note = -1
					
				break
				case "x":
					nzc_note = -2
					
				break
				case "c":
					nzc_note = -3
					break
				case "1":
					//change iT
					if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[1])
				break
				case "2":
					//change iT
					if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[2])
				break
				case "3":
					//change iT
					if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[3])
					break
				case "4":
					//change iT
					if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[4])
					break
				case "5":
					//change iT
					if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[5])
					break
				case "6":
					//change iT
					if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[6])
					break
				case "7":
					//change iT
					if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[7])
					break
				case "8":
					//change iT
					if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[8])
					break
				case "9":
					//change iT
					if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[9])
					break
				case "0":
					//change iT
					if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[10])
					break
				default:
					
					return
				}
			}
		}else{
			switch (event.key) {
				case "a":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+0
				break
				case "w":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+1
				break
				case "s":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+2
				break
				case "e":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+3
				break
				case "d":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+4
				break
				case "r":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+5
				break
				case "f":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+6
				break
				case "t":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+7
				break
				case "g":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+8
				break
				case "y":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+9
				break
				case "h":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+10
				break
				case "u":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+11
				break				
				case ".":
					if(event.type=="keydown")BNAV_increase_virtual_piano_octave()
				break
				case ",":
					if(event.type=="keydown")BNAV_decrease_virtual_piano_octave()
				break
				case ">":
					if(event.type=="keydown")BNAV_keyboard_increase_note()
				break
				case "<":
					if(event.type=="keydown")BNAV_keyboard_decrease_note()
				break
				case "z":
					nzc_note = -1
					
				break
				case "x":
					nzc_note = -2
					
				break
				case "c":
					nzc_note = -3
					break
					case "1":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[1])
					break
					case "2":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[2])
					break
					case "3":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[3])
						break
					case "4":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[4])
						break
					case "5":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[5])
						break
					case "6":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[6])
						break
					case "7":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[7])
						break
					case "8":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[8])
						break
					case "9":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[9])
						break
					case "0":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[10])
						break
				default:
					
					return
				}
		}
	} else {
		if(virtualPianoOctave!=6){

		if(virtualPianoNote==0){
			switch (event.key) {
				case "a":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+0
				break
				case "w":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+1
				break
				case "s":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+2
				break
				case "e":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+3
				break
				case "d":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+4
				break
				case "f":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+5
				break
				case "t":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+6
				break
				case "g":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+7
				break
				case "y":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+8
				break
				case "h":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+9
				break
				case "u":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+10
				break
				case "j":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+11
				break
				case "k":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+12
				break
				
				case ".":
					if(event.type=="keydown")BNAV_increase_virtual_piano_octave()
				break
				case ",":
					if(event.type=="keydown")BNAV_decrease_virtual_piano_octave()
				break
				case ">":
					if(event.type=="keydown")BNAV_keyboard_increase_note()
				break
				case "<":
					if(event.type=="keydown")BNAV_keyboard_decrease_note()
				break
				case "z":
					nzc_note = -1
					
				break
				case "x":
					nzc_note = -2
					
				break
				case "c":
					nzc_note = -3
					break
					case "1":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[1])
					break
					case "2":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[2])
					break
					case "3":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[3])
						break
					case "4":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[4])
						break
					case "5":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[5])
						break
					case "6":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[6])
						break
					case "7":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[7])
						break
					case "8":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[8])
						break
					case "9":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[9])
						break
					case "0":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[10])
						break
				default:
					
					return
				}
		}else if(virtualPianoNote==1){
			switch (event.key) {
				case "q":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+0
				break
				case "a":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+1
				break
				case "w":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+2
				break
				case "s":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+3
				break
				case "d":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+4
				break
				case "r":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+5
				break
				case "f":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+6
				break
				case "t":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+7
				break
				case "g":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+8
				break
				case "y":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+9
				break
				case "h":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+10
				break
				case "j":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+11
				break
				case "i":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+12
				break
				case ".":
					if(event.type=="keydown")BNAV_increase_virtual_piano_octave()
				break
				case ",":
					if(event.type=="keydown")BNAV_decrease_virtual_piano_octave()
				break
				case ">":
					if(event.type=="keydown")BNAV_keyboard_increase_note()
				break
				case "<":
					if(event.type=="keydown")BNAV_keyboard_decrease_note()
				break
				case "z":
					nzc_note = -1
					
				break
				case "x":
					nzc_note = -2
					
				break
				case "c":
					nzc_note = -3
					break
					case "1":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[1])
					break
					case "2":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[2])
					break
					case "3":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[3])
						break
					case "4":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[4])
						break
					case "5":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[5])
						break
					case "6":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[6])
						break
					case "7":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[7])
						break
					case "8":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[8])
						break
					case "9":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[9])
						break
					case "0":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[10])
						break
				default:
					
					return
				}
		} else if(virtualPianoNote==2){
			switch (event.key) {
				case "a":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+0
				break
				case "w":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+1
				break
				case "s":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+2
				break
				case "d":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+3
				break
				case "r":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+4
				break
				case "f":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+5
				break
				case "t":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+6
				break
				case "g":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+7
				break
				case "y":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+8
				break
				case "h":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+9
				break
				case "j":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+10
				break
				case "i":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+11
				break
				case "k":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+12
				break
				case ".":
					if(event.type=="keydown")BNAV_increase_virtual_piano_octave()
				break
				case ",":
					if(event.type=="keydown")BNAV_decrease_virtual_piano_octave()
				break
				case ">":
					if(event.type=="keydown")BNAV_keyboard_increase_note()
				break
				case "<":
					if(event.type=="keydown")BNAV_keyboard_decrease_note()
				break
				case "z":
					nzc_note = -1
					
				break
				case "x":
					nzc_note = -2
					
				break
				case "c":
					nzc_note = -3
					break
					case "1":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[1])
					break
					case "2":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[2])
					break
					case "3":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[3])
						break
					case "4":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[4])
						break
					case "5":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[5])
						break
					case "6":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[6])
						break
					case "7":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[7])
						break
					case "8":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[8])
						break
					case "9":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[9])
						break
					case "0":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[10])
						break
				default:
					
					return
				}
		}else if(virtualPianoNote==3){
			switch (event.key) {
				case "q":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+0
				break
				case "a":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+1
				break
				case "s":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+2
				break
				case "e":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+3
				break
				case "d":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+4
				break
				case "r":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+5
				break
				case "f":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+6
				break
				case "t":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+7
				break
				case "g":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+8
				break
				case "h":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+9
				break
				case "u":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+10
				break
				case "j":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+11
				break
				case "i":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+12
				break
				case ".":
					if(event.type=="keydown")BNAV_increase_virtual_piano_octave()
				break
				case ",":
					if(event.type=="keydown")BNAV_decrease_virtual_piano_octave()
				break
				case ">":
					if(event.type=="keydown")BNAV_keyboard_increase_note()
				break
				case "<":
					if(event.type=="keydown")BNAV_keyboard_decrease_note()
				break
				case "z":
					nzc_note = -1
					
				break
				case "x":
					nzc_note = -2
					
				break
				case "c":
					nzc_note = -3
					break
					case "1":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[1])
					break
					case "2":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[2])
					break
					case "3":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[3])
						break
					case "4":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[4])
						break
					case "5":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[5])
						break
					case "6":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[6])
						break
					case "7":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[7])
						break
					case "8":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[8])
						break
					case "9":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[9])
						break
					case "0":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[10])
						break
				default:
					
					return
				}
		}else if(virtualPianoNote==4){
			switch (event.key) {
				case "a":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+0
				break
				case "s":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+1
				break
				case "e":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+2
				break
				case "d":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+3
				break
				case "r":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+4
				break
				case "f":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+5
				break
				case "t":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+6
				break
				case "g":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+7
				break
				case "h":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+8
				break
				case "u":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+9
				break
				case "j":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+10
				break
				case "i":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+11
				break
				case "k":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+12
				break
				case ".":
					if(event.type=="keydown")BNAV_increase_virtual_piano_octave()
				break
				case ",":
					if(event.type=="keydown")BNAV_decrease_virtual_piano_octave()
				break
				case ">":
					if(event.type=="keydown")BNAV_keyboard_increase_note()
				break
				case "<":
					if(event.type=="keydown")BNAV_keyboard_decrease_note()
				break
				case "z":
					nzc_note = -1
					
				break
				case "x":
					nzc_note = -2
					
				break
				case "c":
					nzc_note = -3
					break
					case "1":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[1])
					break
					case "2":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[2])
					break
					case "3":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[3])
						break
					case "4":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[4])
						break
					case "5":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[5])
						break
					case "6":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[6])
						break
					case "7":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[7])
						break
					case "8":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[8])
						break
					case "9":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[9])
						break
					case "0":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[10])
						break
				default:
					
					return
				}
		}else if(virtualPianoNote==5){
			switch (event.key) {
				case "a":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+0
				break
				case "w":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+1
				break
				case "s":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+2
				break
				case "e":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+3
				break
				case "d":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+4
				break
				case "r":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+5
				break
				case "f":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+6
				break
				case "g":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+7
				break
				case "y":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+8
				break
				case "h":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+9
				break
				case "u":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+10
				break
				case "j":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+11
				break
				case "k":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+12
				break
				case ".":
					if(event.type=="keydown")BNAV_increase_virtual_piano_octave()
				break
				case ",":
					if(event.type=="keydown")BNAV_decrease_virtual_piano_octave()
				break
				case ">":
					if(event.type=="keydown")BNAV_keyboard_increase_note()
				break
				case "<":
					if(event.type=="keydown")BNAV_keyboard_decrease_note()
				break
				case "z":
					nzc_note = -1
					
				break
				case "x":
					nzc_note = -2
					
				break
				case "c":
					nzc_note = -3
					break
					case "1":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[1])
					break
					case "2":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[2])
					break
					case "3":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[3])
						break
					case "4":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[4])
						break
					case "5":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[5])
						break
					case "6":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[6])
						break
					case "7":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[7])
						break
					case "8":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[8])
						break
					case "9":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[9])
						break
					case "0":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[10])
						break
				default:
					
					return
				}
		}else if(virtualPianoNote==6){
			switch (event.key) {
				case "q":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+0
				break
				case "a":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+1
				break
				case "w":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+2
				break
				case "s":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+3
				break
				case "e":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+4
				break
				case "d":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+5
				break
				case "f":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+6
				break
				case "t":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+7
				break
				case "g":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+8
				break
				case "y":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+9
				break
				case "h":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+10
				break
				case "j":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+11
				break
				case "i":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+12
				break
				case ".":
					if(event.type=="keydown")BNAV_increase_virtual_piano_octave()
				break
				case ",":
					if(event.type=="keydown")BNAV_decrease_virtual_piano_octave()
				break
				case ">":
					if(event.type=="keydown")BNAV_keyboard_increase_note()
				break
				case "<":
					if(event.type=="keydown")BNAV_keyboard_decrease_note()
				break
				case "z":
					nzc_note = -1
					
				break
				case "x":
					nzc_note = -2
					
				break
				case "c":
					nzc_note = -3
					break
					case "1":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[1])
					break
					case "2":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[2])
					break
					case "3":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[3])
						break
					case "4":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[4])
						break
					case "5":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[5])
						break
					case "6":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[6])
						break
					case "7":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[7])
						break
					case "8":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[8])
						break
					case "9":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[9])
						break
					case "0":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[10])
						break
				default:
					
					return
				}
		}else if(virtualPianoNote==7){
			switch (event.key) {
				case "a":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+0
				break
				case "w":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+1
				break
				case "s":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+2
				break
				case "e":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+3
				break
				case "d":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+4
				break
				case "f":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+5
				break
				case "t":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+6
				break
				case "g":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+7
				break
				case "y":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+8
				break
				case "h":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+9
				break
				case "j":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+10
				break
				case "i":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+11
				break
				case "k":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+12
				break
				case ".":
					if(event.type=="keydown")BNAV_increase_virtual_piano_octave()
				break
				case ",":
					if(event.type=="keydown")BNAV_decrease_virtual_piano_octave()
				break
				case ">":
					if(event.type=="keydown")BNAV_keyboard_increase_note()
				break
				case "<":
					if(event.type=="keydown")BNAV_keyboard_decrease_note()
				break
				case "z":
					nzc_note = -1
					
				break
				case "x":
					nzc_note = -2
					
				break
				case "c":
					nzc_note = -3
					break
					case "1":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[1])
					break
					case "2":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[2])
					break
					case "3":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[3])
						break
					case "4":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[4])
						break
					case "5":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[5])
						break
					case "6":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[6])
						break
					case "7":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[7])
						break
					case "8":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[8])
						break
					case "9":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[9])
						break
					case "0":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[10])
						break
				default:
					
					return
				}
		}else if(virtualPianoNote==8){
			switch (event.key) {
				case "q":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+0
				break
				case "a":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+1
				break
				case "w":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+2
				break
				case "s":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+3
				break
				case "d":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+4
				break
				case "r":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+5
				break
				case "f":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+6
				break
				case "t":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+7
				break
				case "g":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+8
				break
				case "h":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+9
				break
				case "u":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+10
				break
				case "j":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+11
				break
				case "i":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+12
				break
				case ".":
					if(event.type=="keydown")BNAV_increase_virtual_piano_octave()
				break
				case ",":
					if(event.type=="keydown")BNAV_decrease_virtual_piano_octave()
				break
				case ">":
					if(event.type=="keydown")BNAV_keyboard_increase_note()
				break
				case "<":
					if(event.type=="keydown")BNAV_keyboard_decrease_note()
				break
				case "z":
					nzc_note = -1
					
				break
				case "x":
					nzc_note = -2
					
				break
				case "c":
					nzc_note = -3
					break
					case "1":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[1])
					break
					case "2":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[2])
					break
					case "3":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[3])
						break
					case "4":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[4])
						break
					case "5":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[5])
						break
					case "6":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[6])
						break
					case "7":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[7])
						break
					case "8":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[8])
						break
					case "9":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[9])
						break
					case "0":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[10])
						break
				default:
					
					return
				}
		}else if(virtualPianoNote==9){
			switch (event.key) {
				case "a":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+0
				break
				case "w":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+1
				break
				case "s":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+2
				break
				case "d":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+3
				break
				case "r":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+4
				break
				case "f":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+5
				break
				case "t":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+6
				break
				case "g":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+7
				break
				case "h":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+8
				break
				case "u":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+9
				break
				case "j":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+10
				break
				case "i":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+11
				break
				case "k":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+12
				break
				case ".":
					if(event.type=="keydown")BNAV_increase_virtual_piano_octave()
				break
				case ",":
					if(event.type=="keydown")BNAV_decrease_virtual_piano_octave()
				break
				case ">":
					if(event.type=="keydown")BNAV_keyboard_increase_note()
				break
				case "<":
					if(event.type=="keydown")BNAV_keyboard_decrease_note()
				break
				case "z":
					nzc_note = -1
					
				break
				case "x":
					nzc_note = -2
					
				break
				case "c":
					nzc_note = -3
					break
					case "1":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[1])
					break
					case "2":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[2])
					break
					case "3":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[3])
						break
					case "4":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[4])
						break
					case "5":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[5])
						break
					case "6":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[6])
						break
					case "7":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[7])
						break
					case "8":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[8])
						break
					case "9":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[9])
						break
					case "0":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[10])
						break
				default:
					
					return
				}
		}else if(virtualPianoNote==10){
			switch (event.key) {
				case "q":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+0
				break
				case "a":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+1
				break
				case "s":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+2
				break
				case "e":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+3
				break
				case "d":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+4
				break
				case "r":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+5
				break
				case "f":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+6
				break
				case "g":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+7
				break
				case "y":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+8
				break
				case "h":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+9
				break
				case "u":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+10
				break
				case "j":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+11
				break
				case "i":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+12
				break
				case ".":
					if(event.type=="keydown")BNAV_increase_virtual_piano_octave()
				break
				case ",":
					if(event.type=="keydown")BNAV_decrease_virtual_piano_octave()
				break
				case ">":
					if(event.type=="keydown")BNAV_keyboard_increase_note()
				break
				case "<":
					if(event.type=="keydown")BNAV_keyboard_decrease_note()
				break
				case "z":
					nzc_note = -1
					
				break
				case "x":
					nzc_note = -2
					
				break
				case "c":
					nzc_note = -3
					break
					case "1":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[1])
					break
					case "2":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[2])
					break
					case "3":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[3])
						break
					case "4":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[4])
						break
					case "5":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[5])
						break
					case "6":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[6])
						break
					case "7":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[7])
						break
					case "8":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[8])
						break
					case "9":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[9])
						break
					case "0":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[10])
						break
				default:
					
					return
				}
		}else if(virtualPianoNote==11){
			switch (event.key) {
				case "a":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+0
				break
				case "s":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+1
				break
				case "e":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+2
				break
				case "d":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+3
				break
				case "r":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+4
				break
				case "f":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+5
				break
				case "g":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+6
				break
				case "y":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+7
				break
				case "h":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+8
				break
				case "u":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+9
				break
				case "j":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+10
				break
				case "i":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+11
				break
				case "k":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+12
				break
				case ".":
					if(event.type=="keydown")BNAV_increase_virtual_piano_octave()
				break
				case ",":
					if(event.type=="keydown")BNAV_decrease_virtual_piano_octave()
				break
				case ">":
					if(event.type=="keydown")BNAV_keyboard_increase_note()
				break
				case "<":
					if(event.type=="keydown")BNAV_keyboard_decrease_note()
				break
				case "z":
					nzc_note = -1					
				break
				case "x":
					nzc_note = -2					
				break
				case "c":
					nzc_note = -3
					break
					case "1":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[1])
					break
					case "2":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[2])
					break
					case "3":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[3])
						break
					case "4":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[4])
						break
					case "5":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[5])
						break
					case "6":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[6])
						break
					case "7":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[7])
						break
					case "8":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[8])
						break
					case "9":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[9])
						break
					case "0":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[10])
						break
				default:
					
					return
				}
			}
		}else{
			switch (event.key) {
				case "a":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+0
				break
				case "w":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+1
				break
				case "s":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+2
				break
				case "e":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+3
				break
				case "d":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+4
				break
				case "r":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+5
				break
				case "f":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+6
				break
				case "t":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+7
				break
				case "g":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+8
				break
				case "y":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+9
				break
				case "h":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+10
				break
				case "u":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+11
				break				
				case "j":
					nzc_note = 12*virtualPianoOctave+virtualPianoNote+12
				break				
				case ".":
					if(event.type=="keydown")BNAV_increase_virtual_piano_octave()
				break
				case ",":
					if(event.type=="keydown")BNAV_decrease_virtual_piano_octave()
				break
				case ">":
					if(event.type=="keydown")BNAV_keyboard_increase_note()
				break
				case "<":
					if(event.type=="keydown")BNAV_keyboard_decrease_note()
				break
				case "z":
					nzc_note = -1
					
				break
				case "x":
					nzc_note = -2
					
				break
				case "c":
					nzc_note = -3
					break
					case "1":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[1])
					break
					case "2":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[2])
					break
					case "3":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[3])
						break
					case "4":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[4])
						break
					case "5":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[5])
						break
					case "6":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[6])
						break
					case "7":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[7])
						break
					case "8":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[8])
						break
					case "9":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[9])
						break
					case "0":
						//change iT
						if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[10])
						break
				default:
					
					return
				}
		}
		
	}
	//console.log(nzc_note)
	RE_disableEvent(event)

	if(nzc_note!=null){
		var instrument_index=0
		//var freq_note = APP_note_to_Hz(nzc_note,TET)
		//add a note to the current voice segment
		var focused_elem = document.activeElement
		if(event.type=="keydown"){
			// add key to store
			key_pressed = { ...key_pressed, [event.which]: true }
			if(nzc_note>=0){

			
			BNAV_play_note(nzc_note, true, 2)	
			}
			
			//BNAV_virtual_keyboard_key_down(nzc_note)
			//play note inside Add_midi_note_to_RE
			var iT = 1
			//Add_MIDI_note_to_RE(focused_elem,nzc_note,true,iT,0.5)
			//var now = Tone.immediate()
			//instrument[instrument_index].triggerAttack(freq_note, now, 0.5)
		}else{
			if(nzc_note>=0){
			BNAV_play_note(nzc_note, false, 2)

			}
			
			//BNAV_virtual_keyboard_key_up(nzc_note)
			//var now = Tone.now()
			//instrument[instrument_index].triggerRelease(freq_note, now)
			//Add_MIDI_note_to_RE(focused_elem,nzc_note,false)
			//console.gitlog(typeof(nzc_note))
			BNAV_try_insert_note_duration(nzc_note,keyboard_iT)
			const { [event.which]: id, ...rest } = key_pressed
			// remove key from store
			key_pressed = rest
		}
	}
}

function Add_MIDI_note_to_RE(focused_elem, nzc_note , down , iT, volume) {
console.error("OBSOLETE Add_MIDI_note_to_RE use database" )
	var freq_note = APP_note_to_Hz(nzc_note,TET)
	var quantization = false
	var instrument_index = 0
	if(quantization){
		//XXX logic for register note during play
	}else{
		//add note to RE directly
		//if (focused_elem.classList.contains("RE_inp_box") && (focused_elem.classList.contains("RE_note_cell")||focused_elem.classList.contains("RE_time_cell"))){
		if (focused_elem.classList.contains("App_RE_note_cell")||focused_elem.classList.contains("App_RE_time_cell")){
			//console.log("is a inp_box")
			var segment = focused_elem.closest(".App_RE_segment")
			if(segment!=null ){
				var voice = segment.closest(".App_RE_voice")
				//change instrument
				instrument_index = RE_which_voice_instrument(voice)
				if(down){
					var [elementIndex,elm_list]=RE_element_index(focused_elem,".App_RE_inp_box")
					//console.log(list)
					var input_N_list = [...segment.querySelector(".App_RE_Na").querySelectorAll(".App_RE_inp_box")]
					var Na_element = input_N_list[elementIndex]
					//the note define a new iT
					//find current iT
					var input_iT_list = [...segment.querySelector(".App_RE_iT").querySelectorAll(".App_RE_inp_box")]
					//var input_P_list = [...segment.querySelector(".App_RE_Ts").querySelectorAll(".App_RE_inp_box")]

					if(elementIndex+1>=input_iT_list.length){
						//console.log("outside")
						return
					}
					var element_iT = input_iT_list[elementIndex+1]
					//XXX look for input it >1
					if(element_iT.value==""){
						//find correct iT
						var element_iT = _find_prev_element(element_iT)
					}
					var iT_value = Math.floor(element_iT.value)


					// if(iT_value==1){
						//no space for new note
						//go to next range
					// 	console.log("go to next range")
					// 	var [Na_element_bis,,] = RE_find_next_filled_element(Na_element,".App_RE_inp_box")
					// 	console.log(Na_element_bis)
					// }

					//write note in position
					//(Na_element.focus() //if Na line is not active doesn't work
					APP_set_previous_value(Na_element) //trigger con focus in
					Na_element.value=nzc_note

					//deactivate mom sound note
					var reset_play_new_note=false
					if(SNAV_read_play_new_note()){
						SNAV_switch_play_new_note()
						reset_play_new_note=true
					}
					var reset_note_on_click=false
					if(SNAV_read_note_on_click()){
						SNAV_switch_note_on_click()
						reset_note_on_click=true
					}

					RE_enter_new_value_S(Na_element,"Na")  //trigger con focus out
					//Na_element.blur() //if Na line is not active doesn't work

					APP_set_previous_value(Na_element)//avoid run RE_enter_new_value_S a second time AND generate errors!!!

					var [new_elementIndex,list]=RE_element_index(focused_elem,".App_RE_inp_box")
					var next_element = null
					if(iT_value<=2){
						//focus on next filled element
						var [next_element,,] = RE_find_next_filled_element(focused_elem,".App_RE_inp_box")
						//console.log(next_element)
						//console.log('raro')
					}else{
						next_element = list[new_elementIndex+1]
						//console.log('else')
						//console.log(next_element)
					}
					// RE_focus_next_active_iA_element(element,direction) //does not jump segment

					//XXX next focus all cases (end segment or range or it=1?
					//console.log(next_element)
					//but if is end of segment focus on next segment
					if(next_element!=null){
						//console.log('??')
						//console.log(next_element)
						next_element.focus()
						//console.log(document.activeElement)
					}else{
						//console.log('!!')
						//try to find next segment
						//console.log("search next segment")
						RE_focus_next_index(elm_list[elm_list.length-1],1)//????? XXX
						//console.log(document.activeElement)
					}

					if(reset_play_new_note){
						SNAV_switch_play_new_note()
					}
					if(reset_note_on_click){
						SNAV_switch_note_on_click()
					}

					
				}else{
					//nothing to do
				}
			}
		}
		//play or stop
		if(down){
			//play note
			var now = Tone.immediate()
			//if not drum XXX
			if(instrument_index!=1){
				instrument[instrument_index].triggerAttack(freq_note, now, volume)
			}else{
				if(nzc_note>=23 && nzc_note<= 69)instrument[instrument_index].player(nzc_note).start(now, 0, 1, 0, volume)
				//keysFractionedMetronome.player(value.note).start(time, 0, value.durationHz, 0, vel)
			}
			
			BNAV_virtual_keyboard_key_down(nzc_note)
		}else{
			var now = Tone.now()
			//if not drum XXX
			if(instrument_index!=1){
				instrument[instrument_index].triggerRelease(freq_note, now)
			}//else if(instrument_index>1){
			//	instrument[instrument_index].triggerRelease(now)
			//}
			BNAV_virtual_keyboard_key_up(nzc_note)
		}
	}
	//console.log(document.activeElement)


	//defferent, it doesn t see Silence , e L and and range
	function _find_prev_element(element, string="input"){  //XXX  //XXX XXX XXX XXX XXX XXX
		console.error("OBSOLETE FUNCTION _find_prev_element used on RE midi , USE DATABASE")      //               XXX
		var parent = element.parentNode
		var input_list = [...parent.querySelectorAll(string)]
		var elementIndex = input_list.indexOf(element)
		var index_previous = -1

		for (var i=elementIndex-1; i>=0;i--){
			var value = input_list[i].value
			if(value!="" && value!="e"&& value!="s" && value!=tie_intervals && value!=end_range){
				//element not void
				index_previous=i
				i=-1
			}
		}

		if (index_previous>=0){
			var distance = elementIndex-index_previous
			return [input_list[index_previous], index_previous, distance]
		}else{
			return [null, -1,-1]
		}
	}
}


