function APP_sequencer_for_WEB(current_data){
	//in freq
	hz=current_data.global_variables.PPM/60
	pulsationFrequency=hz+"hz"

	//change to database
	//a copy of database
	//var current_data = DATA_get_current_state_point(false)

	var voice_solo = current_data.voice_data.filter(voice=>{
		return voice.solo==true
	})

	var voice_list_unmuted_time_data = []

	if(voice_solo!=null&&voice_solo.length!=0){
		//there is a solo
		voice_solo.forEach(element=>{
			if(element.volume!="0"){
				voice_list_unmuted_time_data.push(element)
			}
		})

	}else{
		//find other not muted voices
		voice_list_unmuted_time_data = current_data.voice_data.filter(voice=>{
			return voice.volume!="0"
		})
	}

	var compas_values_array = current_data.compas
	var Ti = 60/current_data.global_variables.PPM
	var compas_sequence = [[0,"Accent"]]
	if(compas_values_array.length==1 && compas_values_array[0].compas_values[1]==0){
		//no compases
		//play an accent at the start and pulsacion next
		for (var i=1; i<Li ; i++){
			compas_sequence[i]=[i*Ti,"Tic"]
		}
	}else{
		//compasses
		var index_P = 0
		compas_values_array.forEach(compas =>{
			var accent_sequence=compas.compas_values[3]
			if(compas.compas_values[4]!=0){
				//recalculate rotated sequence
				accent_sequence.push(compas.compas_values[1])
				var iT_sequence = []
				for (let i = 0; i < accent_sequence.length-1; i++) {
					iT_sequence.push(accent_sequence[i+1]-accent_sequence[i])
				}
				//rotate iT sequence
				var rotation_N = compas.compas_values[4]
				while (rotation_N!=0) {
					iT_sequence.unshift(iT_sequence.pop())
					rotation_N--
				}
				accent_sequence=[0]
				var i=0
				iT_sequence.forEach(iT=>{
					accent_sequence.push(accent_sequence[i]+iT)
					i++
				})
				accent_sequence.pop()
			}
			for (var rep = 0; rep<compas.compas_values[2]; rep++){
				for(var pulse =0;pulse<compas.compas_values[1];pulse++){
					if(pulse==0){
						//first accent
						compas_sequence[index_P]=[index_P*Ti,"Accent"]
					}else{
						//if(compas.compas_values[3].includes(pulse)){
						if(accent_sequence.includes(pulse)){
							compas_sequence[index_P]=[index_P*Ti,"Accent_Weak"]
						}else{
							compas_sequence[index_P]=[index_P*Ti,"Tic"]
						}
					}
					index_P++
				}
			}
		})
	}

	var now =0
	var start =0
	var end = current_data.global_variables.Li

	stopper.dispose()
	stopper = new Tone.Part(function(time, value){
		if(value.end){
			//stop the reproduction
			APP_stop_for_WEB()
			now=0
			start=0
		}else{
		}
	})


	part.dispose()
	part = new Tone.Part(function(time, value){
		//sounds
		var vel = Math.random() * 0.5 + 0.5;
		//suona la nota nel gruppo di note keys al pulso "time" calcolato mediante una frequenza di pulsazione
		if(value.note>0 && value.note<=5){
			//fractioned metronome
			//volume
			var vol = -1
			//adjust volume for Claves
			if (value.note == 1){
				vol += 2
			}
			//adjust volume for Wood-Block-Med
			if (value.note == 4){
				vol += 5
			}
			keysFractionedMetronome.player(value.note).volume.value = vol;
			//console.log(vol)
			keysFractionedMetronome.player(value.note).start(time, 0, value.durationHz, 0, vel)
		}else{
			//main metronome (compass, accents) 0.1 is duration
			//vel is due to main volume and value.volume
			if(value.note=="Accent"){
				keysMainMetronome.player("Accent").start(time, 0, 0.1, 0, 1)
				return
			}
			if(value.note=="Accent_Weak"){
				keysMainMetronome.player("Accent_Weak").start(time, 0, 0.1, 0, 1)
				return
			}
			if(value.note=="Tic"){
				keysMainMetronome.player("Tic").start(time, 0, 0.1, 0, -1)
				return
			}

			if(value.note==-2){
				//pulse
				//with players
				if(keysPulse.player(value.inst).state=="stopped"){
					keysPulse.player(value.inst).start(time, 0, value.duration, 0, 1)
				}else{
					//time + little delay in order to not give sound pulse error
					keysPulse.player(value.inst).start(time+0.001, 0, value.duration, 0, 1)
				}
			}else{
				//NOTES INSTRUMENTS
				//if instrument sync not good
				if(value.inst!=1){
					//console.log(noteAbs)
					//console.log(value.note)
					instrument[value.inst].triggerAttackRelease(value.note, value.duration, time, value.volume/8)
				}else{
					let nzc_note = value.note
					if(nzc_note>=23 && nzc_note<= 69){
						var volume = WEB_volume_value_to_dB(value.volume/3)
						//console.log(volume)
						instrument[value.inst].player(nzc_note).volume.value = volume -12//dB adjust
						instrument[value.inst].player(nzc_note).start(time, 0, value.duration, 0, 1)
					}
				}
			}
		}
	})

	//ADDING SEQUENCES OF SOUNDS LOOP BETTER IN TRANSPORT
	Tone.Transport.timeSignature = end

	//ROZZO
	var now_seconds = now*(60/current_data.global_variables.PPM)
	var start_seconds = start*(60/current_data.global_variables.PPM)
	var end_seconds = end*(60/current_data.global_variables.PPM)

	var time_to_stop = 1 * end * (60/current_data.global_variables.PPM) -0.01
	stopper.add({time: time_to_stop, end: true})

	//CLASSIC METRONOME NEED LOVE
	//var main_metro_vol = 0
	//dB
	//keysMainMetronome.volume.value=-80
	var MainMetronome=false
	if(MainMetronome){
		//main_metro_vol = 1
		//keysMainMetronome.volume.value=0
		keysMainMetronome.mute=false
		//see if Li bigger than main metronome
		compas_sequence.forEach(item=>{
			//part.add("0:"+i,noteNames[1])
			var pulseN = Math.round(item[0]*(current_data.global_variables.PPM/60))
			//console.log(item[0])
			part.add({ time: item[0], note: item[1], durationHz: 10, pulse: pulseN})
			// part.add({ time: item[0], note: item[1], durationHz: 10 , volume: main_metro_vol})
		})
	}else{
		keysMainMetronome.mute=true
	}

	//METRONOME VOICE (FRACTIONING)
	voice_list_unmuted_time_data.forEach(voice_data=>{
		//check of i want to hear a metronome for this voice

		//from database
		var metro_sound = voice_data.metro

		//console.log(metro_sound)
		if(metro_sound!=0){
			//Metro sounds
			var metro_fractioning= DATA_list_voice_data_metro_fractioning(voice_data)
			metro_fractioning.forEach(change_fraction_data=>{
				for (var i =0; i<change_fraction_data.rep; i++){
					var time = change_fraction_data.time + i/change_fraction_data.freq
					var freq = change_fraction_data.freq+"hz"
					//part.add(time,metro_sound, freq)
					//console.log("time : "+ time)
					part.add({ time: time, note: metro_sound, durationHz: freq})
				}
			})
		}
	})

	//NOTES VOICE
	voice_list_unmuted_time_data.forEach(voice_data=>{
		var midi_data = (JSON.parse(JSON.stringify(voice_data.data.midi_data)))
		var index_position = 0

		//changing instruments
		var voice_instrument = voice_data.instrument

		var nzc_note = midi_data.note_freq.map(freq=>{return APP_Hz_to_note_WEB(freq,current_data.global_variables.TET)})//.reverse()
		var freq_note = midi_data.note_freq//.reverse()
		var note_list = []//array nuzic_note and frequence
		for (var i = 0 ; i<nzc_note.length;i++){
			note_list.push({"nzc" : nzc_note[i], "freq" : freq_note[i]})
		}

		var time_list = midi_data.time//.reverse()
		//duration iT_freq
		var duration_list = midi_data.duration//.reverse()

		var Dduration = 0

		note_list.forEach((note,index)=>{
			if(note.nzc<0){
				if(note.nzc==-3){
					//ligadura
					Dduration+=(1/duration_list[index])
					// XXX
					/// animation ligadura
				}else{
					//silence or element
					Dduration=0

					//animation silence and element
					var duration = 1/duration_list[index]

					//element
					//XXX attention if i call same sound twice XXX error same time XXX
					if(note.nzc==-2){
						part.add({ time: time_list[index], note: note.nzc, duration: 0.3, inst: "Pulse1"})
					}
				}
			}else{
				//it is a note
				var duration = 1/duration_list[index]
				if(Dduration!=0){
					duration += Dduration
					//reset
					Dduration=0
				}

				if(time_list[index]<end_seconds){
					var start_note_time = time_list[index]
					var end_note_time = start_note_time+duration
					//control loop start/end
					if(end_note_time>start_seconds && start_note_time<end_seconds){
						//note must be played
						//control loop start
						if(start_note_time<start_seconds){
							start_note_time= start_seconds
							duration = end_note_time-start_note_time
						}

						//control loop end
						if(end_note_time>end_seconds){
							duration= end_seconds-start_note_time
						}

						//if instrument sync directly with transport
						//instrument[voice_instrument].triggerAttackRelease(note, duration, start_note_time, voice_data.volume/10)
						//if trigger with a part
						if(voice_instrument==1){
							//drum
							part.add({ time: start_note_time, note: note.nzc, duration: duration, inst: voice_instrument, volume: voice_data.volume})
						}else{
							part.add({ time: start_note_time, note: note.freq, duration: duration, inst: voice_instrument, volume: voice_data.volume})
						}

						if(isNaN(note.nzc)){
							console.error("note is NaN")
						}
					}
				}
			}
		})
	})
}

function APP_play_for_WEB(current_data){
	APP_stop_for_WEB()
	var now =0
	var start =0
	var end = current_data.global_variables.Li
	var buffer = 0.3

	//not using bar to define transport position
	var now_seconds = Tone.Transport.seconds
	var start_seconds = start*(60/current_data.global_variables.PPM)
	var end_seconds = end*(60/current_data.global_variables.PPM)

	//chrome and safari need action to start context
	if(Tone.context.state!=='running'){
		Tone.context.resume()
		Tone.start()
	}

	Tone.Transport.bpm.value = current_data.global_variables.PPM

	Tone.Transport.loop=false

	if(Tone.Transport.state=="stopped"){
		//from the start
		APP_sequencer_for_WEB(current_data)
		//connect instrument if disconnected
		APP_connect_first_instrument()

		//countin
		keysMainMetronome.mute = false
		var j = current_data.global_variables.count_in
		for (var i=0; i<j; i++){
			var countIn_time = i *1000 * 60 / current_data.global_variables.PPM
			setTimeout(function () {
				var now = Tone.now()
				keysMainMetronome.player("Accent_Weak").start(now, 0, 0.1, 0, 1)
			}, countIn_time)
		}
		//keysMainMetronome.mute = true

		var countIn_end = j * 1000 * 60 / current_data.global_variables.PPM - buffer * 1000//buffering
		setTimeout(function () {
			part.start(0)
			stopper.start(0)
			if(now_seconds==0){
				Tone.Transport.seconds = now_seconds-buffer
			}else{
				//smaller buffer
				Tone.Transport.seconds = now_seconds-0.05
			}

			Tone.Transport.start()
		}, countIn_end)
	}else {
		//NOTHING TO DO
	}
}

function APP_stop_for_WEB(){
	if(Tone.Transport.state!="stopped"){
		//need to stop tone event beeng played
		APP_stop_all_notes()
		Play_Compo_from_widget_END()
		Tone.Transport.stop()
		part.stop(Tone.now())
		stopper.start(0)
	}
}

function APP_Hz_to_note_WEB(freq,note_TET=null){
	var f_princ = 261.62
	var f_princ_reg = 4
	//LA is the 0
	//var f_princ = 440
	//register TET
	if(note_TET==null){
		console.error("specify TET")
		return
	}
	var note_number = -1 //silence
	if(freq<0){
		//is a Nuzic special object
		note_number = freq
	}else{
		note_number = Math.round((Math.log2(freq/f_princ)+f_princ_reg)*note_TET)
	}
	return note_number
}

function WEB_volume_value_to_dB(value){
	//transform volume value 0-1 to decibel value -20 a 0
	var dB = -20 + value * 20
	//console.log(dB)
	return dB
}

function WEB_frequency_to_percentage(frequency){
	let f_princ = 261.62
	let f_princ_reg = 4
	let f_first_note = f_princ/Math.pow(2,f_princ_reg)
	let f_last_note = Math.pow(2, (83-(f_princ_reg*12))/12) * f_princ
	let value_0 = Math.log(f_first_note)/Math.log(f_last_note)
	// var f_princ = 261.62
	// var f_princ_reg = 4
	// var f_first_note = f_princ/Math.pow(2,f_princ_reg)
	// var f_last_note = Math.pow(2, (83-(f_princ_reg*12))/12) * f_princ
	var value = Math.log(frequency)/Math.log(f_last_note)
	//var value_0 = Math.log(f_first_note)/Math.log(f_last_note)

	var linear = (value- value_0)/(1-value_0)
	return linear
}

function App_minimap_get_jpg_for_web_component(){
	console.error("obsolete App function")
	WEB_minimap_get_jpg_for_web_component()
}

function WEB_minimap_get_jpg_for_web_component(){
	//var canvas = document.getElementById("minimapWebCanvas")
	var canvas = document.createElement('canvas')
	var timeData = []
	var freqData = []
	var timeSig = 0

	var datatest = DATA_get_current_state_point(false)
	console.log(datatest)
	timeSig=datatest.global_variables.Li/datatest.global_variables.PPM*60*18
	//refresh tab global scale if there is a new database
	datatest.voice_data.forEach(voice => {
		// voice.data.midi_data
		voice.data.midi_data.time.push(60*datatest.global_variables.Li/datatest.global_variables.PPM)
		timeData.push(voice.data.midi_data.time)
		//console.log(voice.data.midi_data.note_freq)
		freqData.push(voice.data.midi_data.note_freq.map(f=>{
		return WEB_frequency_to_percentage(f)
		}))
	})

	canvas.height= 84
	canvas.width = timeSig

	var ctx = canvas.getContext('2d')
	for(i=0; i<freqData.length;i++){
		for(j=0;j<=freqData[i].length;j++){
			ctx.beginPath()
			ctx.moveTo(Math.ceil(timeData[i][j]*18),Math.round((1-freqData[i][j])*canvas.height))
			ctx.lineTo(Math.ceil(timeData[i][j+1]*18),Math.round((1-freqData[i][j])*canvas.height))
			ctx.strokeStyle = '#43433B'
			ctx.stroke()
		}
	}
	var img = canvas.toDataURL('image/png')
	return img
}
