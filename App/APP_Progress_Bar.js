function APP_set_progressBar_eventListeners(){
	var etiqueta = document.querySelector('#progress_label');
	var etiqueta_inloop = document.querySelector('#inloop_label');
	var etiqueta_endloop = document.querySelector('#endloop_label');
	var canvas = document.querySelector('#progress_bar_canvas')
	//XXX cuidado , por ejemplo en la ultima funcion creas otra var canvas XXX
	var upperBox = document.querySelector('.App_MMap')

	/*  Event Listeners */
	ProgressBar.addEventListener('input', (event) => {
		APP_stop()
		var P_abs = Number(ProgressBar.value)
		RE_highlight_columns_at_pulse(P_abs)
		if(tabREbutton.checked){
			RE_highlight_columns_at_pulse(P_abs)
		}else{
			//PMC_position??? XXX check if current voice is selected???
			PMC_draw_progress_line_time(time)
		}
		APP_draw_cursor_minimap_canvas(P_abs)
		var uBox = parseInt(window.getComputedStyle(upperBox, null).getPropertyValue('width'));
		var w =  parseInt(window.getComputedStyle(canvas, null).getPropertyValue('width'));
		var etiquetaLeft = (uBox-w)/2-10.5
		var pxls = w/ProgressEndLoop.max;
		var correction = 0

		etiqueta.style.display = 'block'
		etiqueta.innerHTML = ProgressBar.value
		etiqueta.style.left = etiquetaLeft + correction + ((ProgressBar.value * pxls)) + 'px';

		//var time= Number(ProgressBar.value)*60/PPM
		var time=DATA_calculate_exact_time(P_abs,0,0,1,1,1,1)
		if(!tabREbutton.checked){
			PMC_draw_progress_line_time(time)
		}
		Tone.Transport.seconds = time
	})

	ProgressBar,addEventListener('mouseup',() => {
		etiqueta.style.display = 'none'
	})

	ProgressBar.addEventListener('change', (event) => {
		//etiqueta.style.display = 'none'
		if(parseInt(ProgressBar.value) < parseInt(ProgressInLoop.value)){
			ProgressInLoop.value = ProgressBar.value
			APP_draw_progress_canvas()
			//APP_draw_cursor_minimap_canvas(ProgressBar.value)
		}
		if(parseInt(ProgressBar.value) > parseInt(ProgressEndLoop.value)){
			ProgressEndLoop.value = ProgressBar.value
			ProgressBar.value = ProgressInLoop.value
			APP_draw_progress_canvas()
			//APP_draw_cursor_minimap_canvas(ProgressBar.value)
		}
	})

	ProgressInLoop.addEventListener('input', (event) => {
		APP_stop()
		var uBox = parseInt(window.getComputedStyle(upperBox, null).getPropertyValue('width'));
		var w =  parseInt(window.getComputedStyle(canvas, null).getPropertyValue('width'));
		var etiquetaLeft = (uBox-w)/2-10.5
		var pxls = w /ProgressEndLoop.max;
		etiqueta_inloop.style.display = 'block'
		etiqueta_inloop.innerHTML = ProgressInLoop.value
		etiqueta_inloop.style.left = etiquetaLeft + ((ProgressInLoop.value * pxls)) + 'px';
	})

	ProgressInLoop,addEventListener('mouseup',() => {
		etiqueta_inloop.style.display = 'none'
	})

	ProgressInLoop.addEventListener('change', (event) => {
		if(parseInt(ProgressInLoop.value) >= parseInt(ProgressEndLoop.value)){
			if(ProgressInLoop.value == Li){
				//ProgressInLoop.value = parseFloat(ProgressEndLoop.value)-1
				ProgressInLoop.value= Li-1
				ProgressEndLoop.value = Li
				// ProgressBar.value = Li-1
			} else {
				ProgressEndLoop.value = parseFloat(ProgressInLoop.value)+1
			}
		}
		if(parseInt(ProgressInLoop.value) > parseInt(ProgressBar.value )){
			ProgressBar.value =  ProgressInLoop.value
			APP_check_minimap_scroll()
			APP_draw_cursor_minimap_canvas(ProgressBar.value)
		}
		APP_draw_progress_canvas()
	})


	ProgressEndLoop.addEventListener('input', (event) => {
		APP_stop()
		var uBox = parseInt(window.getComputedStyle(upperBox, null).getPropertyValue('width'));
		var w =  parseInt(window.getComputedStyle(canvas, null).getPropertyValue('width'));
		var etiquetaLeft = (uBox-w)/2-10.5
		var pxls = w /ProgressEndLoop.max;
		etiqueta_endloop.style.display = 'block'
		etiqueta_endloop.innerHTML = ProgressEndLoop.value
		etiqueta_endloop.style.left = etiquetaLeft + ((ProgressEndLoop.value * pxls)) + 'px';
	})

	ProgressEndLoop,addEventListener('mouseup',() => {
		etiqueta_endloop.style.display = 'none'
	})

	ProgressEndLoop.addEventListener('change', (event) => {
		if(parseInt(ProgressEndLoop.value) <= parseInt(ProgressInLoop.value)){
			if(ProgressEndLoop.value == 0){
				// ProgressEndLoop.value = parseFloat(ProgressInLoop.value)+1
				ProgressInLoop.value = 0
				ProgressEndLoop.value = 1
				// ProgressBar.value = 0
			} else {
				ProgressInLoop.value = parseFloat(ProgressEndLoop.value)-1
			}
		}
		if(parseInt(ProgressEndLoop.value) < parseInt(ProgressBar.value )){
			ProgressBar.value = ProgressInLoop.value
			APP_check_minimap_scroll()
			APP_draw_cursor_minimap_canvas(ProgressBar.value)
		}
		APP_draw_progress_canvas()
	})
}

APP_set_progressBar_eventListeners()

function APP_return_progress_bar_values(){
	return [parseInt(ProgressBar.value),parseInt(ProgressInLoop.value),parseInt(ProgressEndLoop.value),parseInt(ProgressEndLoop.max)]
}

function APP_move_progress_bar(position1, position2=null, position3=null){
	//current position play
	if(position1!=null){
		ProgressBar.value = position1
		APP_check_minimap_scroll()
	}

	//move starting point
	if(position2!=null){
		//at position
		ProgressInLoop.value=position2
	}
	//move ending point
	if(position3!=null){
		if(position3=="max"){
			//at the end
			ProgressEndLoop.value = ProgressEndLoop.max
		}else{
			//at position
			ProgressEndLoop.value = position3
		}
	}
}

function APP_redraw_Progress_bar_limits(reset=false,endloop_at_Li=false){
	var max_progress=Li
	var last_compas = DATA_get_compas_sequence().pop()
	var max_compas=last_compas.compas_values[0]+last_compas.compas_values[1]*last_compas.compas_values[2]
	if(max_compas>Li)max_progress=max_compas
	var current_values=APP_return_progress_bar_values()
	ProgressBar.max = max_progress
	ProgressInLoop.max=max_progress
	ProgressEndLoop.max =max_progress
	//reset positions
	if(reset==true){
		APP_move_progress_bar(0,0,max_progress)
	}else{
		//try to not reset values
		var a=0
		var b=0
		var c=max_progress

		if(current_values[1]<max_progress) b=current_values[1]

		if(current_values[0]<max_progress){
			a=current_values[0]
		} else {
			a=b
		}
		if(current_values[2]<max_progress) c=current_values[2]

		if(endloop_at_Li){
			//endLoop was at the end and i want it there
		 	c=max_progress
		 }

		// APP_move_progress_bar(0,0,max_progress)
		APP_move_progress_bar(a,b,c)
	}

	APP_draw_progress_canvas()
}

function APP_draw_progress_canvas(){
	var box = document.querySelector('.App_MMap_progress_bar_canvas_container')
	var canvas = box.querySelector('canvas')
	canvas.setAttribute('width', window.getComputedStyle(canvas, null).getPropertyValue("width"))
	canvas.setAttribute('height', window.getComputedStyle(canvas, null).getPropertyValue("height"))

	var width = canvas.width
	var height = canvas.height

	var context = canvas.getContext('2d')
	var progress_number = 0
	var n = 1

	//get what is bigger (Li or compass)
	//see if metronome bigger of Li
	var timeSignature = ProgressEndLoop.max
	//console.log(timeSignature)

	if (timeSignature > 600){
		n= 100
	}else if (timeSignature > 200){
		n=50
	}else if (timeSignature > 160){
		n=20
	} else if (timeSignature > 81){
		n=10
	} else if (timeSignature > 20){
		n=5
	} else  if (timeSignature > 10){
		n = 2
	}

	var DtimeSignature = timeSignature/n

	var cuts = Math.round(width/DtimeSignature)   //XXX not a good way , the round error build up and fk up the alignment

	context.clearRect(0, 0, canvas.width, canvas.height)

	context.beginPath()
	context.fillStyle = "#FFBB33"
	

	//context.fillRect(ProgressInLoop.value*cuts, 0, (ProgressEndLoop.value-ProgressInLoop.value)*cuts, height)		//XXX changing in something better
	//XXX MUST take account of thumbs width!!!! XXX
	//better BUT.....
	context.fillRect((ProgressInLoop.value/timeSignature)*width, 0, ((ProgressEndLoop.value-ProgressInLoop.value)/timeSignature)*width, height) //no round here (meaningless)
	// first line

	context.stroke()
	/* context.lineWidth = 2;
	context.beginPath()
	context.moveTo(1,0)
	context.lineTo(1,height)
	
	context.stroke() */
	/* context.beginPath()
	context.lineWidth = 1;
	context.strokeStyle="black"
	context.font = "11px Krub";
	
	context.strokeText(`${progress_number*n}`, 12, height-4) */
	var initLoop= document.getElementById('init_loop')
	var endLoop= document.getElementById('end_loop')
	for (i = cuts; i < width; i += cuts){
		//lines
		
		context.beginPath()
		
		context.lineWidth = 3;
		
		context.moveTo(i, 0);
		context.lineTo(i, height);
		/* console.log(i)
		console.log(box.offsetWidth)
		console.log(endLoop.max) */
		/* console.log(i/box.offsetWidth*endLoop.max)
		console.log(initLoop.value)
		console.log(endLoop.value) */
		if(i/box.offsetWidth*endLoop.max> initLoop.value && i/box.offsetWidth*endLoop.max < endLoop.value){
			context.strokeStyle="white"
		} else {
			context.strokeStyle="#EBC150"
		}
		
		context.stroke();
		
		//text
		context.beginPath()
		context.font = "11px Krub";
		context.lineWidth = 1;
		context.strokeStyle="black"
		progress_number ++
		context.strokeText(`${progress_number*n}`, i+10, height-4)
		
		
	}

	//last line
	/* context.beginPath()
	context.lineWidth = 2;
	context.moveTo(width-1, 0);
	context.lineTo(width-1, height);
	context.strokeStyle="black"
	context.stroke(); */
}

window.addEventListener('resize', () => {
	APP_draw_progress_canvas()
})

function APP_sraw_sideCanvas_minimap(){
	var canvas = [...document.getElementsByClassName('App_MMap_sound_line_canvas')]

	canvas[0].setAttribute('width', window.getComputedStyle(canvas[0], null).getPropertyValue("width"))
	canvas[0].setAttribute('height', window.getComputedStyle(canvas[0], null).getPropertyValue("height"))
	canvas[1].setAttribute('width', window.getComputedStyle(canvas[1], null).getPropertyValue("width"))
	canvas[1].setAttribute('height', window.getComputedStyle(canvas[1], null).getPropertyValue("height"))
	var width = canvas[0].width
	var height = canvas[0].height
	// console.log(canvas)
	// console.log(width)
	// console.log(height)
	canvas.forEach(function (element){
		// console.log(element)
		// console.log('entro')
		var context = element.getContext('2d')
		/* context.beginPath()
		context.moveTo(3,55)
		context.lineTo(11, 55 )
		context.stroke()
		context.beginPath()
		context.lineWidth = 2;
			context.strokeStyle= 'white';
		context.moveTo(3,77)
		context.lineTo(11, 77 )
		context.stroke() */
		for(i=1;i<=80;i++){
			var context = element.getContext('2d')
			context.beginPath()
			context.lineWidth = 1;
			context.strokeStyle= 'white';
			context.moveTo(3, 0+i*5);
			context.lineTo(11, 0+i*5);
			context.stroke()
		}
	})
}

APP_sraw_sideCanvas_minimap()
