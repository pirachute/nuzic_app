function RE_write_all_iA_lines(data,iA_n_boxes){
	//different from voice lines because it re translate all...
	var RE_box = document.querySelector(".App_RE")
	var voice_matrix = RE_box.querySelector(".App_RE_voice_matrix")
	var voice_list = [...voice_matrix.querySelectorAll(".App_RE_voice")]
	var visible_voice_list = voice_list.filter((item)=>{return RE_voice_is_visible(item)})

	var iA_button_states={"iAa":data.global_variables.RE.iAa,"iAm":data.global_variables.RE.iAm,"iAgm":data.global_variables.RE.iAgm}

	var type="none"
	Object.keys(iA_button_states).forEach(id=>{
		if(iA_button_states[id]==true){
			type=id
		}
	})

	if(type=="none")return

	//modify every visible iA
	var visible_iA_line = [...voice_matrix.querySelectorAll(".App_RE_iA:not(.hidden)")]

	visible_iA_line.forEach((iA_line)=>{
		var inner_string = ""

		//add spacer
		inner_string=`
			<div class="App_RE_iA_spacer">
				<svg data-name="Trazado 10" height="28" width="28" viewBox="0 0 28 28" preserveAspectRatio="none">
					<line x1="14" y1="0" x2="14" y2="28" style="stroke-width:2"></line>
				</svg>
			</div>
			`
		//add inp boxes
		for (let i = 0; i < iA_n_boxes; i++) {
			// inner_string+=`
			// 				<input class="App_RE_inp_box App_RE_iA_cell" ondragstart="RE_disableEvent(event)" onkeypress="OnlyIntegers_iAa(event,this)" onkeydown="RE_move_focus_iA(event,this)" onfocusin="Set_previous_value(this);RE_play_input_on_click(this,'iA');Replace_inp_box_superscript(this)" onfocusout="Enter_new_value_iA(this,'XXX')" ondblclick="RE_dbclick_inp_box(this)" oninput="Resize_inp_box(this)" placeholder=" " maxlength="4" disabled="true">
			// 				`
			inner_string+=`
							<input class="App_RE_inp_box App_RE_iA_cell" ondragstart="RE_disableEvent(event)" placeholder=" " maxlength="4" disabled="true">
							`
		}

		// add spacer
		inner_string+=`
			<div class="App_RE_iA_spacer">
				<svg data-name="Trazado 10" height="28" width="28" viewBox="0 0 28 28" preserveAspectRatio="none">
					<line x1="14" y1="0" x2="14" y2="28" style="stroke-width:2"></line>
				</svg>
			</div>
			<div class="App_RE_segment_spacer_end">
			</div>
			`
		iA_line.innerHTML=inner_string
		//Actualize iA values
		//var voice_obj = iA_line.closest(".App_RE_voices_between").previousSibling
		var prev_voice_obj= RE_find_voice_obj_from_iA_line(iA_line,true)
		var next_voice_obj= RE_find_voice_obj_from_iA_line(iA_line,false)
		var pos_voice_up = voice_list.indexOf(prev_voice_obj)
		var pos_voice_down = voice_list.indexOf(next_voice_obj)
		//var pos_voice_down = pos_voice_up+1
		var data_up = data.voice_data[pos_voice_up]
		var data_down= data.voice_data[pos_voice_down]
		RE_write_iA_list(iA_line, type ,data_up , data_down)
	})
}

function RE_write_iA_list(iA_line, type,data_up,data_down){
	//Actualize iA values
	var iA_input_boxes = [...iA_line.querySelectorAll(".App_RE_inp_box")]
	var time_list_voice_up = RE_global_time_voices_changes.find(time=>{return time.voice_index==data_up.voice_index}).time
	var time_list_voice_down = RE_global_time_voices_changes.find(time=>{return time.voice_index==data_down.voice_index}).time

	//use RE_global_time_segment_change for allignment

	//make a list of all note up and down
	//var N_voice_up_list = RE_global_note_voices_changes.find(note=>{return note.voice_index==data_up.voice_index}).note//we can use scale data...
	//var N_voice_down_list = RE_global_note_voices_changes.find(note=>{return note.voice_index==data_down.voice_index}).note
	var Na_up_list =  []
	var Na_down_list = []
	var diesis_up_list=[]
	var diesis_down_list=[]
	var scale_data_up_list = []
	var scale_data_down_list = []
	var time_segment_change_up = []
	var time_segment_change_down = []

	//prepare a list of scale note up scale note down
	var scale_sequence=DATA_get_scale_sequence()
	var all_scale_length_value = scale_sequence.reduce((p,scale)=>{return parseInt(scale.duration)+p},0)

	var N_NP_up = data_up.neopulse.N
	var N_NP_down = data_down.neopulse.N
	var D_NP_up = data_up.neopulse.D
	var D_NP_down = data_down.neopulse.D

	//scale_data_up_list
	var Psg_segment_start_up=0
	var voice_up_outside=false
	var index_end_segment_up = 0
	data_up.data.segment_data.forEach(segment=>{
		var last_index=segment.time.length-1
		segment.time.forEach((time,index)=>{
			if(index!=last_index){
				Na_up_list.push(segment.note[index])
				diesis_up_list.push(segment.diesis[index])
				if(voice_up_outside || type!="iAgm"){
					scale_data_up_list.push(null)
					return
				}
				//find fraction
				var fraction = segment.fraction.find(fraction=>{return fraction.stop>time.P})
				//if (typeof(fraction) == "undefined")fraction = null //no need , end seg not processed
				var frac_p = fraction.N/fraction.D
				var Pa_equivalent = (Psg_segment_start_up+time.P) * N_NP_up/D_NP_up + time.F* frac_p * N_NP_up/D_NP_up
				if(Pa_equivalent <all_scale_length_value && all_scale_length_value!=0){
					//element inside a scale module
					var scale_end_p=0
					current_scale = scale_sequence.find(scale=>{
						scale_end_p += parseInt(scale.duration)
						return scale_end_p > Pa_equivalent
					})
					scale_data_up_list.push(current_scale)
				}else {
					//outside range scale
					scale_data_up_list.push(null)
					voice_up_outside=true
				}
			}else{
				index_end_segment_up+= last_index
				time_segment_change_up.push(time_list_voice_up[index_end_segment_up])
			}
		})
		Psg_segment_start_up+=segment.time.slice(-1)[0].P
	})

	//scale_data_down_list
	var Psg_segment_start_down=0
	var voice_down_outside=false
	var index_end_segment_down = 0
	data_down.data.segment_data.forEach(segment=>{
		var last_index=segment.time.length-1
		segment.time.forEach((time,index)=>{
			if(index!=last_index){
				diesis_down_list.push(segment.diesis[index])
				Na_down_list.push(segment.note[index])
				if(voice_down_outside || type!="iAgm"){
					scale_data_down_list.push(null)
					return
				}
				var fraction = segment.fraction.find(fraction=>{return fraction.stop>time.P})
				//if (typeof(fraction) == "undefined")fraction = null //no need , end seg not processed
				var frac_p = fraction.N/fraction.D
				var Pa_equivalent = (Psg_segment_start_down+time.P) * N_NP_down/D_NP_down + time.F* frac_p * N_NP_down/D_NP_down
				if(Pa_equivalent <all_scale_length_value && all_scale_length_value!=0){
					//element inside a scale module
					var scale_end_p=0
					current_scale = scale_sequence.find(scale=>{
						scale_end_p += parseInt(scale.duration)
						return scale_end_p > Pa_equivalent
					})
					scale_data_down_list.push(current_scale)
				}else {
					//outside range scale
					scale_data_down_list.push(null)
					voice_down_outside=true
				}
			}else{
				index_end_segment_down+= last_index
				time_segment_change_down.push(time_list_voice_down[index_end_segment_down])
			}
		})
		Psg_segment_start_down+=segment.time.slice(-1)[0].P
	})

	//var abs_position_all=0
	var abs_position_up=0
	var abs_position_down=0
	var prev_Na_up = null
	var prev_Na_down = null
	var prev_diesis_up = null
	var prev_diesis_down = null
	var prev_scale_up=null
	var prev_scale_down=null

	var time_segment_change= JSON.parse(JSON.stringify(RE_global_time_segment_change))
	//no need first or last element (0 and Li)
	var end_time = time_segment_change.slice(-1)[0]
	time_segment_change.shift()
	var abs_position_seg_change=0
	var time_all_changes = RE_global_time_all_changes.filter(time =>time<=end_time)

	var iA_object_list= []
	time_all_changes.forEach((time,index)=>{
		var pos_up=abs_position_up
		var pos_down=abs_position_down
		if(time==time_segment_change[abs_position_seg_change]){
			abs_position_seg_change++;

			//insert 2 values
			var seg_change_down = false
			var seg_change_up = false
			var Na_up = null
			var Na_down = null
			var diesis_up = null
			var diesis_down = null
			var scale_up=null
			var scale_down=null

			if(time==time_list_voice_up[abs_position_up]){
				Na_up =Na_up_list[abs_position_up]
				diesis_up = diesis_up_list[abs_position_up]
				scale_up = scale_data_up_list[abs_position_up]
				abs_position_up++
				seg_change_up=time_segment_change_up.some(item=>{return item==time})
			}

			if(seg_change_up){
				if(Na_up==-3){
					//error
					prev_Na_up=null
					prev_diesis_up=null
					prev_scale_up=null
				}else{
					prev_Na_up=Na_up
					prev_diesis_up=diesis_up
					prev_scale_up=scale_up
				}
			}

			if(time==time_list_voice_down[abs_position_down]){
				Na_down =Na_down_list[abs_position_down]
				diesis_down = diesis_down_list[abs_position_down]
				scale_down = scale_data_down_list[abs_position_down]
				abs_position_down++
				seg_change_down=time_segment_change_down.some(item=>{return item==time})
			}

			if(seg_change_down){
				if(Na_down==-3){
					//error
					prev_Na_down=null
					prev_diesis_down=null
					prev_scale_down=null
				}else{
					prev_Na_down=Na_down
					prev_diesis_down=diesis_down
					prev_scale_down=scale_down
				}
			}

			if(seg_change_down && seg_change_up){
				iA_object_list.push({void:"last"})
				//not possible Na=null
				iA_object_list.push({void:false,disabled:false,iA_data:{Na_up:Na_up, Na_down:Na_down,abs_position_up:pos_up,abs_position_down:pos_down,scale_up:scale_up,scale_down:scale_down}})
				return
			}

			if(seg_change_down){
				iA_object_list.push({void:"last"})
				if(Na_up==null){
					Na_up=prev_Na_up
					if(Na_up==null){
						console.error("Error iA: prev Na not found")
						iA_object_list.push({void:"error"})
						return
					}
					diesis_up=prev_diesis_up
					scale_up=prev_scale_up
					iA_object_list.push({void:false,disabled:true,iA_data:{Na_up:Na_up, Na_down:Na_down,abs_position_up:pos_up,abs_position_down:pos_down,scale_up:scale_up,scale_down:scale_down}})
					return
				}else{
					if(Na_up==-3){
						Na_up=prev_Na_up
						diesis_up=prev_diesis_up
						scale_up=prev_scale_up
					}else{
						prev_Na_up=Na_up
						prev_diesis_up=diesis_up
						prev_scale_up=scale_up
					}

					if(Na_up==null){
						console.error("Error iA: prev Na not found")
						iA_object_list.push({void:"error"})
						return
					}
					iA_object_list.push({void:false,disabled:false,iA_data:{Na_up:Na_up, Na_down:Na_down,abs_position_up:pos_up,abs_position_down:pos_down,scale_up:scale_up,scale_down:scale_down}})
					return
				}
			}

			if(seg_change_up){
				iA_object_list.push({void:"last"})
				if(Na_down==null){
					Na_down=prev_Na_down
					if(Na_down==null){
						console.error("Error iA: prev Na not found")
						iA_object_list.push({void:"error"})
						return
					}
					diesis_down=prev_diesis_down
					scale_down=prev_scale_down
					iA_object_list.push({void:false,disabled:false,iA_data:{Na_up:Na_up, Na_down:Na_down,abs_position_up:pos_up,abs_position_down:pos_down,scale_up:scale_up,scale_down:scale_down}})
					return
				}else{
					if(Na_down==-3){
						Na_down=prev_Na_down
						diesis_down=prev_diesis_down
						scale_down=prev_scale_down
					}else{
						prev_Na_down=Na_down
						prev_diesis_down=diesis_down
						prev_scale_down=scale_down
					}
					if(Na_down==null){
						console.error("Error iA: prev Na not found")
						iA_object_list.push({void:"error"})
						return
					}
					iA_object_list.push({void:false,disabled:false,iA_data:{Na_up:Na_up, Na_down:Na_down,abs_position_up:pos_up,abs_position_down:pos_down,scale_up:scale_up,scale_down:scale_down}})
					return
				}
			}

			//case segment change elsewere
			if(Na_up==null && Na_down==null){
				//no value
				iA_object_list.push({void:true})
				iA_object_list.push({void:true})
				return
			}
			iA_object_list.push({void:true})
			if(Na_down==null){
				//deactivated
				Na_down=prev_Na_down
				if(Na_down==null){
					console.error("Error iA: prev Na not found")
					iA_object_list.push({void:"error"})
					return
				}
				diesis_down=prev_diesis_down
				scale_down=prev_scale_down
				iA_object_list.push({void:false,disabled:false,iA_data:{Na_up:Na_up, Na_down:Na_down,abs_position_up:pos_up,abs_position_down:pos_down,scale_up:scale_up,scale_down:scale_down}})
				return
			}
			var is_disabled=false
			if(Na_up==null){
				Na_up=prev_Na_up
				diesis_up=prev_diesis_up
				scale_up=prev_scale_up
				is_disabled=true
			}

			if(Na_up==null){
				console.error("iA calc : previous value Na UP null ")
				iA_object_list.push({void:"error"})
				return
			}
			iA_object_list.push({void:false,disabled:is_disabled,iA_data:{Na_up:Na_up, Na_down:Na_down,abs_position_up:pos_up,abs_position_down:pos_down,scale_up:scale_up,scale_down:scale_down}})
		}else{
			//insert 1 value
			var Na_up = null
			var Na_down = null
			var diesis_up = null
			var diesis_down = null
			var scale_up=null
			var scale_down=null
			if(time==time_list_voice_up[abs_position_up]){
				Na_up =Na_up_list[abs_position_up]
				diesis_up = diesis_up_list[abs_position_up]
				scale_up = scale_data_up_list[abs_position_up]
				if(Na_up==-3){
					Na_up=prev_Na_up
					diesis_up=prev_diesis_up
					scale_up=prev_scale_up
				}else{
					prev_Na_up=Na_up
					prev_diesis_up=diesis_up
					prev_scale_up=scale_up
				}
				abs_position_up++
			}

			if(time==time_list_voice_down[abs_position_down]){
				Na_down =Na_down_list[abs_position_down]
				diesis_down = diesis_down_list[abs_position_down]
				scale_down = scale_data_down_list[abs_position_down]
				if(Na_down==-3){
					Na_down=prev_Na_down
					diesis_down=prev_diesis_down
					scale_down=prev_scale_down
				}else{
					prev_Na_down=Na_down
					prev_diesis_down=diesis_down
					prev_scale_down=scale_down
				}
				abs_position_down++
			}

			if(Na_up==null && Na_down==null){
				//no value
				iA_object_list.push({void:true})
				return
			}

			if(Na_down==null){
				//deactivated
				Na_down=prev_Na_down
				if(Na_down==null){
					console.error("Error iA: prev Na not found")
					iA_object_list.push({void:"error"})
					return
				}
				diesis_down=prev_diesis_down
				scale_down=prev_scale_down
				iA_object_list.push({void:false,disabled:false,iA_data:{Na_up:Na_up, Na_down:Na_down,abs_position_up:pos_up,abs_position_down:pos_down,scale_up:scale_up,scale_down:scale_down}})
				return
			}
			var is_disabled=false
			if(Na_up==null){
				Na_up=prev_Na_up
				diesis_up=prev_diesis_up
				scale_up=prev_scale_up
				is_disabled=true
			}

			if(Na_up==null){
				console.error("iA calc : previous value Na UP null ")
				iA_object_list.push({void:"error"})
				return
			}
			iA_object_list.push({void:false,disabled:is_disabled,iA_data:{Na_up:Na_up, Na_down:Na_down,abs_position_up:pos_up,abs_position_down:pos_down,scale_up:scale_up,scale_down:scale_down}})
		}
	})
	//last one is undefined??
	iA_object_list.pop()

	//define scale DATA
	iA_object_list.forEach((item,index)=>{
		if(!item.disabled && !item.void){
			//add voice index and scale if necessary
			//item.iA_data.push({voice_index_up:data_up.voice_index})
			item.iA_data.voice_index_up=data_up.voice_index
			item.iA_data.voice_index_down=data_down.voice_index
		}
	})

	iA_input_boxes.forEach((item,index)=>{
		if(isOdd(index)==0){
			var true_index=index/2
			var current_obj = iA_object_list[true_index]
			if(current_obj==null){
				console.error("ERROR iA : array data wrong size")
				RE_write_iA_element(item, null,"error",true,null)
				return
			}

			if(current_obj.void==true)return
			if(current_obj.void=="error"){
				RE_write_iA_element(item, null,"error",true,null)
				return
			}

			if(current_obj.void=="last"){
				RE_write_iA_element(item, null,"last",true,null)
				return
			}

			if(current_obj.void==false){
				if(current_obj.iA_data.Na_up ==-1 || current_obj.iA_data.Na_up ==-2 || current_obj.iA_data.Na_down ==-1 || current_obj.iA_data.Na_down ==-2){
					RE_write_iA_element(item, "no_value",type,current_obj.disabled,current_obj.iA_data)
					return
				}
				RE_write_iA_element(item, current_obj.iA_data.Na_up-current_obj.iA_data.Na_down,type,current_obj.disabled,current_obj.iA_data)
				return
			}
		}
	})
/*

						if(type=="iAgm"){
							//prepare a list of scale note up scale note down
							current_scale_data={Na_up:Na_up,Na_down:Na_down,
								diesis_up:diesis_up_list[index_Na_up],diesis_down:diesis_down_list[index_Na_down],
								scale_up: scale_data_up_list[index_Na_up],scale_down:scale_data_down_list[index_Na_down]}
						}
	*/
}

function RE_write_iA_element(element,delta,type,disabled,iA_data){
	//item disabled if not possible indipendent change
	//give correct in function and callback
	//if(isNaN(delta) && delta!=no_value && delta!=end_range)console.error("hello "+delta)
	var copy_type=type
	if(delta=="no_value")type=delta

	element.setAttribute("onfocusin","APP_set_previous_value(this);APP_play_input_on_click(this,'iA');RE_replace_inp_box_superscript(this)")
	element.setAttribute("ondblclick","RE_dbclick_inp_box(this)")
	element.setAttribute("oninput","RE_resize_inp_box(this)")
	switch (type) {
	case "iAa":
		element.setAttribute("onkeypress","APP_onlyIntegers_iAa(event,this)")
		element.setAttribute("onfocusout","RE_enter_new_value_iA(this,'iAa')")
		element.setAttribute("onkeydown","RE_move_focus_iA(event,this)")
		var rg_length = 3
		if(TET*7>99)rg_length=4
		element.setAttribute("maxlength",rg_length)
		element.value = delta
	break;
	case "iAm":
		element.setAttribute("onkeypress","APP_onlyIntegers_iAm(event,this)")
		element.setAttribute("onfocusout","RE_enter_new_value_iA(this,'iAm')")
		element.setAttribute("onkeydown","RE_move_focus_iA(event,this)")
		element.setAttribute("maxlength","5")
		// if(delta==no_value){
		// 	element.value = delta
		// 	break
		// }
		var mod = TET
		var reg = Math.floor(delta/mod)
		var resto = delta%mod
		var neg = false
		var stringiA = ""
		// distance modular
		//verify if is negative
		if(delta<0){
			neg = true
			resto = Math.abs(resto)
			if(resto!=0){
				reg= Math.abs(reg)-1
			}else{
				reg= Math.abs(reg)
			}
		}
		if(neg){
			stringiA="-"+resto//+"r"+reg
		}else{
			stringiA=resto//+"r"+reg
		}
		if(reg!=0)stringiA=stringiA+"r"+reg
		element.value = stringiA
	break;
	case "iAgm":
		element.setAttribute("onkeypress","APP_onlyIntegers_iAgm(event,this)")
		element.setAttribute("onfocusout","RE_enter_new_value_iA(this,'iAgm')")
		element.setAttribute("onkeydown","RE_move_focus_iA(event,this)")
		element.setAttribute("maxlength","8")

		//console.log(iA_data)
		//XXX disabled
		disabled=true
		if(iA_data==null){
			//console.error("ERROR SCALE DATA NULL PROBABLY A NOT NOTE")
			element.value = "Err"
			disabled=true
			break
		}else{
			if(iA_data.scale_up==null){
				//console.error("ERROR SCALE DATA UP NULL outside scale range")
				element.value = no_value
				disabled=true
			}else{
				if(iA_data.scale_down==null){
					//console.error("ERROR SCALE DATA DOWN NULL outside scale range")
					element.value = no_value
					disabled=true
				}else{
					if(delta==no_value){
						element.value = delta
						break
					}
					if(delta==end_range){
						element.value = delta
						break
					}

					//calculate iA
					//if scale down != up ...
					if(iA_data.scale_down.acronym==iA_data.scale_up.acronym && iA_data.scale_down.rotation==iA_data.scale_up.rotation && iA_data.scale_down.starting_note==iA_data.scale_up.starting_note){
						//calculate resto in grades
						element.value = BNAV_calculate_scale_interval_string(iA_data.Na_down,iA_data.diesis_down,iA_data.scale_down,iA_data.Na_up,iA_data.diesis_up,iA_data.scale_up)
					}else{
						//when scale doesn't match
						//element.value = no_value
						element.value = no_value_iA
						//element.value = "PLOP"
						disabled=true
						// console.log(element)
						// console.log(iA_data.scale_up)
						// console.log(iA_data.scale_down)
						//console.error("OBSOLETE?")
						//var [grade_up,delta_up,reg_up]=BNAV_absolute_note_to_scale(iA_data.Na_up,true,iA_data.scale_up)//XXX true??
						//var [grade_down,delta_down,reg_down]=BNAV_absolute_note_to_scale(iA_data.Na_down,true,iA_data.scale_down)//XXX true??
					}
				}
			}
		}
	break;
	case "no_value":
		element.value = no_value

		if(copy_type=="iAa"){
			element.setAttribute("onkeypress","APP_onlyIntegers_iAa(event,this)")
			element.setAttribute("onfocusout","RE_enter_new_value_iA(this,'iAa')")
			var rg_length = 3
			if(TET*7>99)rg_length=4
			element.setAttribute("maxlength",rg_length)
		}
		if(copy_type=="iAm"){
			element.setAttribute("onkeypress","APP_onlyIntegers_iAa(event,this)")
			element.setAttribute("onfocusout","RE_enter_new_value_iA(this,'iAm')")
			element.setAttribute("maxlength","5")
		}

		if(copy_type=="iAgm"){
			//disabled XXX
			disabled=true
			element.setAttribute("onkeypress","APP_onlyIntegers_iAgm(event,this)")
			element.setAttribute("onfocusout","RE_enter_new_value_iA(this,'iAgm')")
			element.setAttribute("onkeydown","RE_move_focus_iA(event,this)")
			element.setAttribute("maxlength","8")
		}
		element.setAttribute("onkeydown","RE_move_focus_iA(event,this)")
		//no need
	break;
	case "void":
		//element.value = delta
		//no need
	break;
	case "last":
		element.value = end_range
		element.classList.add("App_RE_segment_last_cell")
	break;
	case "error":
		element.value = "x"
	break;
	}
	element.disabled = disabled

	//info data
	if(!disabled){
		//var up_info = JSON.stringify({abs_position:abs_position_up,voice_index:data_up.voice_index,nzc_note:Na_up,diesis:diesis_up_list[index_Na_up]})
		var up_info = JSON.stringify({abs_position:iA_data.abs_position_up,voice_index:iA_data.voice_index_up,nzc_note:iA_data.Na_up,diesis:iA_data.diesis_up})
		//var down_info = JSON.stringify({abs_position:abs_position_down,voice_index:data_down.voice_index,nzc_note:Na_down,diesis:diesis_down_list[index_Na_down]})
		var down_info = JSON.stringify({abs_position:iA_data.abs_position_down,voice_index:iA_data.voice_index_down,nzc_note:iA_data.Na_down,diesis:iA_data.diesis_down})
		element.setAttribute("up_info",up_info)
		element.setAttribute("down_info",down_info)
	}
	RE_resize_inp_box(element)
}

function RE_enter_new_value_iA(element,type){

	var string = element.value
	if(string==previous_value)return
	if(type=="iAgm" && string==RE_superscript_to_string(previous_value)) {
		//need to reset sup text
		element.value=previous_value
		return
	}

	// if(previous_value==no_value_iA) {
		//no need of continuing calculations
		// scale were different and is not possible to vhange that
		//not really needed this if
	// 	element.value=previous_value
	// 	return
	// }
	APP_stop()

	if (string==tie_intervals_m){
		//tie 2 intervals
		string=tie_intervals
		element.value=tie_intervals
	}

	//find info list ???
	var info_list=RE_find_voice_elements_info_from_iA_element(element)
	var position=0
	var next=0

	//find position
	var [elementIndex,input_iA_list]=RE_element_index(element,".App_RE_inp_box:not(:disabled)")
	input_iA_list.find((inp,index)=>{
		if (index==elementIndex)return true
		position++
	})

	//modify existing Note
	if(string==""){
		DATA_delete_object_index(info_list.up.voice_index,null,info_list.up.abs_position)
		var next=0
		RE_focus_on_object_index_iA(info_list.down.voice_index,position,next)
		return
	}

	if (string=="s"){
		//silence
		var success = DATA_modify_object_index_note(info_list.up.voice_index,null, info_list.up.abs_position, -1)
		if(!success){
			element.value=previous_value
			APP_blink_error(element)
			return
		}
		var next=1
		RE_focus_on_object_index_iA(info_list.down.voice_index,position,next)
		return
	}

	if(string==no_value || string == "e") {
		//enter e element in upper voice
		var success = DATA_modify_object_index_note(info_list.up.voice_index,null, info_list.up.abs_position, -2)
		if(!success){
			element.value=previous_value
			APP_blink_error(element)
			return
		}
		var next=1
		RE_focus_on_object_index_iA(info_list.down.voice_index,position,next)
		return
	}

	if (string==tie_intervals){
		//enter e element in upper voice
		var success = DATA_modify_object_index_note(info_list.up.voice_index,null, info_list.up.abs_position, -3)
		if(!success){
			element.value=previous_value
			APP_blink_error(element)
			return
		}
		var next=1
		RE_focus_on_object_index_iA(info_list.down.voice_index,position,next)
		return
	}

	//translate element.value to N element absolute (difference bw 2 notes)
	var delta_note = null
	var diesis=null

	//case it is a note
	switch(type) {
	case "iAa":
		// Abs
		delta_note = Math.floor(string)

		if(isNaN(delta_note)){
			delta_note = null
			break
		}
		break;
	case "iAm":
		//verify if is negative
		var i= 1
		if (string[0]=="-"){
			i=-1
		}
		//verify if has module
		var split = element.value.split('r')
		var resto = Math.floor(split[0])
		// code for different ranges and microtones XXX
		var reg=0
		if(split[1]>=0 && split[1]!=""){
			reg=Math.floor(split[1])
		}
		if(!isNaN(resto)){
			delta_note = resto + reg*TET*i
		}else{
			delta_note = null
		}
		break;
	case "iAgm":
		// grade mod
		//find correct scale (it is the same up and down)
		var current_scale = info_list.up.info.scale

		if(current_scale==null){
			note_number = null
			break
		}

		//verify if is negative
		var i= 1
		if (string[0]=="-"){
			i=-1
			string = string.substring(1)
		}

		var split = string.split('r')
		var reg=0
		if(string.includes("r")){
			if(split[1]!=""){
				reg=parseInt(split[1])
			}
		}

		//calculating grade and dieresis
		var g = 0
		var delta = 0

		if(split[0].includes("+")){
			//positive dieresis
			var split2 = split[0].split('+')
			g= Math.floor(split2[0])
			if(split2[1]==""){
				delta=1
			}else{
				delta=Math.floor(split2[1])
			}
		}else if(split[0].includes("-")){
			//negatives
			var split2 = split[0].split('-')
			g= Math.floor(split2[0])
			if(split2[1]==""){
				delta=-1
			}else{
				delta=Math.floor(split2[1])*(-1)
			}
		}else{
			g= Math.floor(split[0])
		}

		//console.log("going "+i+ "  of grade "+g+" and diesis "+delta+" and reg "+reg)
		//changing this values in note_number

		//find previous note
		var previous_Na=info_list.down.nzc_note

		if(!isNaN(i) && !isNaN(g) && !isNaN(delta) && !isNaN(reg)){
			//grade , delta , register
			var grade = g*i
			//console.log("going grade "+grade+" and diesis "+delta+" and reg "+reg)

			//if(JSON.stringify(current_scale)==JSON.stringify(previous_scale)){//already know this
			//same scale

			var [prev_grade,prev_delta,prev_rev]=BNAV_absolute_note_to_scale(previous_Na,info_list.down.diesis,info_list.down.info.scale)

			var next_grade = prev_grade+grade
			var next_delta = prev_delta+delta
			var next_reg = prev_rev+reg

			var current_Na = BNAV_scale_note_to_absolute(next_grade,next_delta,next_reg,current_scale)

			////contorl neg
			if(current_Na<0){
				console.log("going grade "+next_grade+" and diesis "+next_delta+" and reg "+next_reg)
				console.error("first element of a new scale but negative : Na "+current_Na)
				delta_note = null
				break
			}

			delta_note=current_Na-previous_Na

			//DIESISSSSS STESSOOOOOOO
			if(next_delta>0)diesis=true
			if(next_delta<0)diesis=false
			//no change if delta = 0
			break
			/*
			}else{
				//scale changed INPUT INSERTED IS NEW PULSE
				console.log("scale changed")
				if(grade<0 || reg<0){ //XXX non é detto   RICORDA LE ROTAZIONIIIII XXX
					note_number = null
					console.log("going grade "+grade+" and diesis "+delta+" and reg "+reg)
					console.error("first number of a new scale cant be neg")
					break
				}

				var current_Na = BNAV_scale_note_to_absolute(grade,delta,reg,current_scale)

				if(current_Na<0){
					console.log("going grade "+grade+" and diesis "+delta+" and reg "+reg)
					console.error("first element of a new scale but negative : Na "+current_Na)
					note_number = null
					break
				}
				note_number= current_Na-previous_Na
				if(next_delta>0)diesis=true
				if(next_delta<0)diesis=false
				//no change if delta = 0
				break
			}*/
		}else{
			delta_note = null
		}
		break;
	default:
		console.error("Error reading case N writing roules")
		delta_note=null
	}

	if(delta_note==null){
		console.error("i don't understand what you wrote")
		element.value=previous_value
		APP_blink_error(element)
		return
	}

	var new_upper_note=0
	if (info_list.down.nzc_note<0){
		if(delta_note<0){
			element.value=previous_value
			APP_blink_error(element)
			return
		}
		new_upper_note=delta_note
	}else{
		new_upper_note=info_list.down.nzc_note+delta_note
		if(new_upper_note<0)new_upper_note=0
	}

	var success = DATA_modify_object_index_note(info_list.up.voice_index,null, info_list.up.abs_position, new_upper_note,diesis)
	if(!success){
		element.value=previous_value
		APP_blink_error(element)
	}
	var next=1
	RE_focus_on_object_index_iA(info_list.down.voice_index,position,next)
}

function RE_find_voice_obj_from_iA_line(iA_line,up){
	var iA_input_boxes = [...iA_line.querySelectorAll(".App_RE_inp_box")]
	//find index
	var visible_iA_line = [...document.querySelectorAll(".App_RE_iA:not(.hidden)")]
	var iA_index = visible_iA_line.indexOf(iA_line)

	var voice_list = [...document.querySelectorAll(".App_RE_voice")]
	var visible_voice_list = voice_list.filter((item)=>{return RE_voice_is_visible(item)})

	//find voice index and voice index+1
	if(up){
		return voice_up = visible_voice_list[iA_index]
	}else{
		return voice_down = visible_voice_list[iA_index+1]
	}
}

function RE_find_voice_elements_info_from_iA_element(element){
	//find up and down voice elements corresponding to an iA element
	var down_info=JSON.parse(element.getAttribute("down_info"))
	var up_info=JSON.parse(element.getAttribute("up_info"))

	//use
	var data_up = DATA_get_object_index_info(up_info.voice_index,null,up_info.abs_position,0,true,false,true)
	var data_down = DATA_get_object_index_info(down_info.voice_index,null,down_info.abs_position,0,true,false,true)
	//ATT!!! L is not acceptable (sometime??) add information

	return {	up:{voice_index: up_info.voice_index,abs_position:up_info.abs_position,nzc_note:up_info.nzc_note,info:data_up},
				down:{voice_index: down_info.voice_index,abs_position:down_info.abs_position,nzc_note:down_info.nzc_note,info:data_down}}
}

function Find_voice_segment_up_from_iA_element(element){//XXX
	//find up voice and segment corresponding to an iA element
	//finding voice

	console.error("OBSOLETE FUNCTION Find_voice_segment_up_from_iA_element")
	var iA_list = element.closest(".App_RE_iA")
	var iA_input_boxes = [...iA_list.querySelectorAll(".App_RE_inp_box")]
	//find index
	var visible_iA_list = [...document.querySelectorAll(".App_RE_iA:not(.hidden)")]
	var iA_index = visible_iA_list.indexOf(iA_list)

	var voice_list = [...document.querySelectorAll(".App_RE_voice")]
	var visible_voice_list = voice_list.filter((item)=>{return RE_voice_is_visible(item)})

	//find voice index and voice
	var voice_up = visible_voice_list[iA_index]
	var N_line = [...voice_up.querySelectorAll(".App_RE_Na")]
	var [elementIndex,element_list]=RE_element_index(element,".App_RE_inp_box")
	var segment_index = -1
	var segment_ending_element_index = -1
	N_line.find((line)=>{
		var inputs = [...line.querySelectorAll("input")]
		var segment_starting_element_index = segment_ending_element_index+1
		segment_ending_element_index = segment_ending_element_index + inputs.length +1
		segment_index++
		if(segment_starting_element_index<=elementIndex && elementIndex<=segment_ending_element_index)return true
		return false
	})

	var segment_list = [...voice_up.querySelectorAll(".App_RE_segment")]
	return [voice_up, segment_list[segment_index]]
}

function RE_play_iA(element){
	var down_info=JSON.parse(element.getAttribute("down_info"))
	var up_info=JSON.parse(element.getAttribute("up_info"))
	//no need
	//var info_list=RE_find_voice_elements_info_from_iA_element(element)
	//find instrument
	var instrument_index_down= DATA_which_voice_instrument(down_info.voice_index)
	var instrument_index_up= DATA_which_voice_instrument(up_info.voice_index)
	//play note
	APP_play_this_note_number(down_info.nzc_note,instrument_index_down)
	APP_play_this_note_number(up_info.nzc_note,instrument_index_up)
}
