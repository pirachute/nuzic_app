function PMC_press_vsb_N(button){
	if(button.classList.contains("pressed")){
		button.classList.remove("pressed")
	} else {
		button.classList.add("pressed")
	}
	PMC_redraw_selected_voice()
}

function PMC_press_vsb_Nabc(button){
	if(button.classList.contains("pressed")){
		button.classList.remove("pressed")
	} else {
		button.classList.add("pressed")
	}
	PMC_redraw_Nabc()
}

function PMC_set_vsb_N(value){
	//change in side panel
	var container = document.querySelector(".App_PMC_vsb_N_type")
	var buttons = [...container.querySelectorAll(".App_PMC_vsb_N_type_button")]
	var button_Nabc = container.querySelector(".App_PMC_vsb_Nabc_type_button")
	buttons.forEach(el=>{
		if(el.classList.contains("pressed")){
			el.classList.remove("pressed")
		}
	})
	buttons.find(bttn=>{
		if(bttn.value==value.type){
			bttn.classList.add("pressed")
			return true
		}
	})

	//change in voice selector
	var button = document.querySelector("#App_PMC_vsb_N")
	if(value.type=="Na")button.innerHTML=value.type
	if(value.type=="Nm")button.innerHTML="Nm"
	if(value.type=="Ngm")button.innerHTML="N°"

	if(value.show && !button.classList.contains("pressed"))button.classList.add("pressed")
	if(!value.show && button.classList.contains("pressed"))button.classList.remove("pressed")
}

function PMC_set_vsb_Nabc(value_abc){
	//change in side panel
	var container = document.querySelector(".App_PMC_voice_selector_display_list")
	var button_Nabc = container.querySelector(".App_PMC_vsb_Nabc_type_button")

	if(value_abc){
		button_Nabc.classList.add("pressed")
	} else {
		button_Nabc.classList.remove("pressed")
	}
}


function PMC_read_vsb_N(){
	var container = document.querySelector(".App_PMC_vsb_N_type")
	var button = container.querySelector(".App_PMC_vsb_N_type_button.pressed")

	var type = button.value
	var show = document.querySelector("#App_PMC_vsb_N").classList.contains("pressed")
	return {show,type}
}

function PMC_read_vsb_Nabc(){
	var container = document.querySelector(".App_PMC_voice_selector_display_list")
	var button = container.querySelector(".App_PMC_vsb_Nabc_type_button")

	var type = false
	if(button.classList.contains("pressed"))type=true
	return type
}

function PMC_press_vsb_iN(button){
	if(button.classList.contains("pressed")){
		button.classList.remove("pressed")
	} else {
		button.classList.add("pressed")
	}
	PMC_redraw_selected_voice()
}

function PMC_set_vsb_iN(value){
	//change in side panel too
	var container = document.querySelector(".App_PMC_vsb_iN_type")
	var buttons = [...container.querySelectorAll(".App_PMC_vsb_iN_type_button")]
	buttons.forEach(el=>{
		if(el.classList.contains("pressed")){
			el.classList.remove("pressed")
		}
	})
	buttons.find(bttn=>{
		if(bttn.value==value.type){
			bttn.classList.add("pressed")
			return true
		}
	})
	//change in voice selector
	var button = document.querySelector("#App_PMC_vsb_iN")
	if(value.type=="iNa")button.innerHTML="iSa"
	if(value.type=="iNm")button.innerHTML="iSm"
	if(value.type=="iNgm")button.innerHTML="iS°"

	if(value.show && !button.classList.contains("pressed"))button.classList.add("pressed")
	if(!value.show && button.classList.contains("pressed"))button.classList.remove("pressed")
}

function PMC_read_vsb_iN(){
	var container = document.querySelector(".App_PMC_vsb_iN_type")
	var button = container.querySelector(".App_PMC_vsb_iN_type_button.pressed")

	var type = button.value
	var show = document.querySelector("#App_PMC_vsb_iN").classList.contains("pressed")
	return {show,type}
}

function PMC_press_vsb_T(button){
	if(button.classList.contains("pressed")){
		button.classList.remove("pressed")
	} else {
		button.classList.add("pressed")
	}
	PMC_redraw_selected_voice()
}

function PMC_set_vsb_T(value){
	//change in side panel
	var container = document.querySelector(".App_PMC_vsb_T_type")
	var buttons = [...container.querySelectorAll(".App_PMC_vsb_T_type_button")]
	buttons.forEach(el=>{
		if(el.classList.contains("pressed")){
			el.classList.remove("pressed")
		}
	})
	buttons.find(bttn=>{
		if(bttn.value==value.type){
			bttn.classList.add("pressed")
			return true
		}
	})
	//change in voice selector
	var button = document.querySelector("#App_PMC_vsb_T")
	if(value.type=="Ta")button.innerHTML="Pa"
	if(value.type=="Tm")button.innerHTML="Pm"
	if(value.type=="Ts")button.innerHTML="Psg"

	if(value.show && !button.classList.contains("pressed"))button.classList.add("pressed")
	if(!value.show && button.classList.contains("pressed"))button.classList.remove("pressed")
}

function PMC_read_vsb_T(){
	var container = document.querySelector(".App_PMC_vsb_T_type")
	var button = container.querySelector(".App_PMC_vsb_T_type_button.pressed")
	var type = button.value
	var show = document.querySelector("#App_PMC_vsb_T").classList.contains("pressed")
	return {show,type}
}

function PMC_press_vsb_iT(button){
	if(button.classList.contains("pressed")){
		button.classList.remove("pressed")
	} else {
		button.classList.add("pressed")
	}
	PMC_redraw_selected_voice()
}

function PMC_set_vsb_iT(value){
	var button = document.querySelector("#App_PMC_vsb_iT")
	//ONLY OPTION
	if(value.show && !button.classList.contains("pressed"))button.classList.add("pressed")
	if(!value.show && button.classList.contains("pressed"))button.classList.remove("pressed")
}

function PMC_read_vsb_iT(){
	var type = "iT" //only option
	var show = document.querySelector("#App_PMC_vsb_iT").classList.contains("pressed")
	return {show,type}
}

function PMC_press_vsb_RE(button){//XXX
	if(button.classList.contains("pressed")){
		button.classList.remove("pressed")
	} else {
		button.classList.add("pressed")
	}
	//XXX show or hide
}

function PMC_set_vsb_RE(value){
	var button = document.querySelector("#App_PMC_vsb_RE")
	if(value && !button.classList.contains("pressed"))button.classList.add("pressed")
	if(!value && button.classList.contains("pressed"))button.classList.remove("pressed")
	//XXX show or hide
}

function PMC_read_vsb_RE(){
	var show = document.querySelector("#App_PMC_vsb_RE").classList.contains("pressed")
	return show
}

function PMC_select_type_N_button(bttn){
	var button = document.querySelector("#App_PMC_vsb_N")
	var value={"show":button.classList.contains("pressed"),"type":bttn.value}
	PMC_set_vsb_N(value)

	PMC_write_sound_line_values()
	PMC_redraw_selected_voice()
	PMC_redraw_selected_voice_sequence()
}

function PMC_select_type_iN_button(bttn){
	var button = document.querySelector("#App_PMC_vsb_N")
	var value={"show":button.classList.contains("pressed"),"type":bttn.value}
	PMC_set_vsb_iN(value)

	PMC_redraw_selected_voice()
}

function PMC_select_type_T_button(bttn){
	var button = document.querySelector("#App_PMC_vsb_T")
	var value={"show":button.classList.contains("pressed"),"type":bttn.value}
	PMC_set_vsb_T(value)

	PMC_redraw_time_line()
	PMC_redraw_selected_voice()
}

function PMC_select_type_iT_button(button){
	if(button.classList.contains("pressed")){
		//remove
		//button.classList.remove("pressed")
	}else{
		//add
		button.classList.add("pressed")
	}
	//nothing to do , only 1 option
}

/* function Press_metronome_PMC_vsb(button){
	if(button.classList.contains("pressed")){
		button.classList.remove("pressed")
	} else {
		button.classList.add("pressed")
	}
}
function Press_mute_PMC_vsb(button){
	if(button.classList.contains("pressed")){
		button.classList.remove("pressed")
	} else {
		button.classList.add("pressed")
	}
}
function Press_solo_PMC_vsb(button){
	if(button.classList.contains("pressed")){
		button.classList.remove("pressed")
	} else {
		button.classList.add("pressed")
	}
} */

function PMC_populate_voice_list(){
	var data = DATA_get_current_state_point(false)
	var voice_list = document.querySelector(".App_PMC_voice_selector_voice_list")
	voice_list.innerHTML= ""

	data.voice_data.forEach((element, index) =>{
		var voice_item = document.createElement("div")
		voice_item.classList.add("App_PMC_voice_selector_voice_item")
		voice_item.setAttribute("value", element.voice_index)
		voice_item.setAttribute("draggable", true)
		if(element.selected){
			voice_item.classList.add("checked")
		}
		voice_item.onclick= function(){
			PMC_select_voice_button(voice_item)
		}

		var item_title = document.createElement("input")
		item_title.classList.add("App_PMC_voice_selector_voice_item_name")
		item_title.value= element.name
		//item_title.setAttribute("readonly",true)
		item_title.setAttribute("maxlength",8)

		item_title.addEventListener("change", function(){
			//console.log("hello");
			PMC_change_voice_name(this)
		})


		var item_content = document.createElement("div")
		item_content.classList.add("App_PMC_voice_selector_voice_item_content")

		voice_list.appendChild(voice_item)
		voice_item.appendChild(item_title)
		voice_item.appendChild(item_content)

		var colorPicker=document.createElement("button")
		colorPicker.classList.add("APP_PMC_voice_selector_color_picker")
		colorPicker.value =element.color
		//XXX no distinction selected or not
		//if(element.selected){
			colorPicker.style.backgroundColor = eval(element.color)
		//}else {
			//colorPicker.style.backgroundColor = eval(element.color+"_light")
		//}

		colorPicker.onclick=function(){
			PMC_color_picker_toggle(colorPicker)
		}
		item_content.appendChild(colorPicker)

		var colorPickerContent = document.createElement("div")
		colorPickerContent.classList.add("App_PMC_dropdown_colorPicker")
		colorPickerContent.innerHTML =`<button onmouseenter="Hover_area_enter(this)" onmouseleave="Hover_area_exit(this)" onclick="PMC_color_picker_selectColor(this)"
		value="nuzic_blue"></button>
	<button onmouseenter="Hover_area_enter(this)" onmouseleave="Hover_area_exit(this)" onclick="PMC_color_picker_selectColor(this)"
		value="nuzic_green"></button>
	<button onmouseenter="Hover_area_enter(this)" onmouseleave="Hover_area_exit(this)" onclick="PMC_color_picker_selectColor(this)"
		value="nuzic_yellow"></button>
	<button onmouseenter="Hover_area_enter(this)" onmouseleave="Hover_area_exit(this)" onclick="PMC_color_picker_selectColor(this)"
		value="nuzic_pink"></button>
	<button onmouseenter="Hover_area_enter(this)" onmouseleave="Hover_area_exit(this)" onclick="PMC_color_picker_selectColor(this)"
		value="nuzic_red"></button>`

		item_content.appendChild(colorPickerContent)

		var voiceMuteSolo_icon = document.createElement("button")
		voiceMuteSolo_icon.classList.add("App_PMC_voice_selector_voice_item_voiceMuteSoloIcon")

		voiceMuteSolo_icon.onclick=function(){
			PMC_voice_volMuteSolo_Loop(voiceMuteSolo_icon)
		}
		//voiceMuteSolo_icon.innerHTML= "<img src="./Icons/audio-volume-high-symbolic-w.svg">"
		if(!element.solo && element.volume!=0){
			voiceMuteSolo_icon.value=0
			voiceMuteSolo_icon.innerHTML='<img src="./Icons/audio-volume-high-symbolic-w.svg">'
		} else if(!element.solo && element.volume==0){
			voiceMuteSolo_icon.value=1
			voiceMuteSolo_icon.classList.add("App_PMC_vsb_mute")
			voiceMuteSolo_icon.innerHTML='M'
		} else if(element.solo){
			voiceMuteSolo_icon.value=2
			voiceMuteSolo_icon.classList.add("App_PMC_vsb_solo")
			voiceMuteSolo_icon.innerHTML='S'
		}

		item_content.appendChild(voiceMuteSolo_icon)


		var eye_icon = document.createElement("button")
		eye_icon.classList.add("App_PMC_voice_selector_voice_item_eyeIcon")
		eye_icon.onclick=function(){
			PMC_switch_voice_visibility(voiceMuteSolo_icon)
		}
		if(element.PMC_visible){
			eye_icon.innerHTML= '<img src="./Icons/side_ojo.svg">'
			eye_icon.classList.add("checked")
		}else{
			eye_icon.innerHTML= '<img src="./Icons/side_ojo_hover.svg">'
			eye_icon.classList.remove("checked")
		}
		item_content.appendChild(eye_icon)

		var menuButton = document.createElement("button")
		menuButton.classList.add("App_PMC_voice_selector_voice_item_menuIcon")
		menuButton.innerHTML='<img src="./Icons/segment_menu.svg">'
		//menuButton.onclick=function(){
			//PMC_open_voice_menu(menuButton)
		//}
		item_content.appendChild(menuButton)

		var menuButtonContent = document.createElement("div")
		menuButtonContent.classList.add("App_PMC_dropdown_voiceMenu")
		menuButtonContent.innerHTML=`<a onmouseenter="Hover_area_enter(this)" onmouseleave="Hover_area_exit(this)"
		onclick="PMC_add_voice_button(this)"><img class="App_RE_icon" src="./Icons/op_menu_add.svg"></a>
		<!--a onmouseenter="Hover_area_enter(this)" onmouseleave="Hover_area_exit(this)"
		onclick="PMC_clone_voice_button(this)">Clone</a-->
		<a onmouseenter="Hover_area_enter(this)" onmouseleave="Hover_area_exit(this)"
		onclick="PMC_clear_voice_button(this)"><img class="App_RE_icon" src="./Icons/op_menu_clear.svg"></a>
		<a onmouseenter="Hover_area_enter(this)" onmouseleave="Hover_area_exit(this)"
		onclick="PMC_delete_voice_button(this)"><img class="App_RE_icon" src="./Icons/op_menu_delete.svg"></a>
		<a onmouseenter="Hover_area_enter(this)" onmouseleave="Hover_area_exit(this)"
		onclick="PMC_repeat_voice_button(this,event)"><img class="App_RE_icon" src="./Icons/op_menu_repeat_up.svg"></a>
		<a onmouseenter="Hover_area_enter(this)" onmouseleave="Hover_area_exit(this)"
		onclick="PMC_operations_voice_button(this)"><img class="App_RE_icon" src="./Icons/op_menu_operations.svg"></a>
		<a ></a>
		`
		item_content.appendChild(menuButtonContent)
	})

	PMC_refresh_draggable_voices()
}

function PMC_voice_volMuteSolo_Loop(button){
	//find voice_index
	var voice_index= parseInt(button.closest(".App_PMC_voice_selector_voice_item").getAttribute("value"))
	var data = DATA_get_current_state_point(true)
	var voice_data= data.voice_data.find(voice=>{
		return voice.voice_index==voice_index
	})

	if(button.value==0){
		// button.classList.add("App_PMC_vsb_mute")
		// button.innerHTML='M'
		// button.value=1
		voice_data.volume=0
	} else if(button.value==1){
		// button.classList.remove("App_PMC_vsb_mute")
		// button.classList.add("App_PMC_vsb_solo")
		// button.innerHTML='S'
		// button.value=2
		//remove eventually other solos
		/* data.voice_data.forEach(voice=>{
			voice.solo=false
		}) */
		voice_data.volume=3
		voice_data.solo=true
	} else if(button.value==2){
		// button.classList.remove("App_PMC_vsb_solo")
		// button.innerHTML='<img src="./Icons/audio-volume-high-symbolic-w.svg">'
		// button.value=0
		voice_data.solo=false
	}
	
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function PMC_switch_voice_visibility(button){
	var voice_index= parseInt(button.closest(".App_PMC_voice_selector_voice_item").getAttribute("value"))
	var data = DATA_get_current_state_point(true)
	var voice_data= data.voice_data.find(voice=>{
		return voice.voice_index==voice_index
	})
	voice_data.PMC_visible= !voice_data.PMC_visible
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
	return
}

function PMC_change_voice_name(element){
	element.blur()

	var data = DATA_get_current_state_point(true)

	var selected_voice_data= data.voice_data.find(item=>{
		return item.selected==true
	})
	selected_voice_data.name = element.value

	DATA_insert_new_state_point(data)
}

function PMC_toggle_line_selector_menu(button){
	var parent = button.parentNode
	if(parent.querySelector(".App_PMC_voice_selector_button_visibility_dropdown").classList.contains("show")){
		parent.querySelector(".App_PMC_voice_selector_button_visibility_dropdown").classList.remove("show")
	}else{
		parent.querySelector(".App_PMC_voice_selector_button_visibility_dropdown").classList.add("show")
	}
}

function PMC_color_picker_toggle(button){
	var parent = button.closest(".App_PMC_voice_selector_voice_item")
	parent.querySelector(".App_PMC_dropdown_colorPicker").classList.toggle("show")
}

function PMC_color_picker_selectColor(button){
	var parent = button.closest(".App_PMC_voice_selector_voice_item_content")
	var colorSelected = parent.querySelector(".APP_PMC_voice_selector_color_picker")
	if(colorSelected.value != button.value){
		//color has changed
		colorSelected.value= button.value
		colorSelected.style.backgroundColor = eval(button.value+"_light")

		var data = DATA_get_current_state_point(true)

		var selected_voice_data= data.voice_data.find(item=>{
			return item.selected==true
		})
		selected_voice_data.color = colorSelected.value

		DATA_insert_new_state_point(data)
		DATA_load_state_point_data(false,true)
	}
}

function PMC_voice_is_selected(voice_index){
	var voice_list = PMC_list_voices()
	var voice = voice_list.find(voice=>{
		var index = Number(voice.getAttribute("value"))
		return voice_index==index
	})

	var selected = false
	if(voice!=null){
		selected = voice.classList.contains("checked")
	}
	return selected
}

function PMC_voice_is_visible(voice_index){
	var voice_list = PMC_list_voices()
	var voice = voice_list.find(voice=>{
		var index = Number(voice.getAttribute("value"))
		return voice_index==index
	})

	var PMC_visible = true
	if(voice!=null){
		//color = voice.querySelector(".APP_PMC_voice_selector_color_picker").value
		var button=voice.querySelector(".App_PMC_voice_selector_voice_item_eyeIcon")
		PMC_visible=button.classList.contains("checked")
	}
	return PMC_visible
}

function PMC_read_voice_color(voice_index){
	var voice_list = PMC_list_voices()

	var voice = voice_list.find(voice=>{
		var index = Number(voice.getAttribute("value"))
		return voice_index==index
	})

	var color = "nuzic_blue"
	if(voice!=null){
		color = voice.querySelector(".APP_PMC_voice_selector_color_picker").value
	}else{
		//console.error("voice "+voice_index+" not found")//maybe created new voice and not yet actualized the list
	}
	return color
}

function PMC_list_voices(){
	var container = document.querySelector(".App_PMC_voice_selector_voice_list")
	return [...container.querySelectorAll(".App_PMC_voice_selector_voice_item")]
}

//Drag and drop
function PMC_refresh_draggable_voices(){
	var voice_container = document.querySelector(".App_PMC_voice_selector_voice_list")
	var draggables_voices = [...voice_container.querySelectorAll(".App_PMC_voice_selector_voice_item")]
	draggables_voices.forEach(draggable => {
		draggable.addEventListener("dragstart", (e) => {
			draggable.classList.add("dragging_voice")
			draggable.style.border="2px solid var(--Nuzic_light)"
			var canvas = document.createElement("canvas")
			var context = canvas. getContext("2d")
			context. clearRect(0, 0, canvas. width, canvas. height)
			e.dataTransfer.setDragImage(canvas, 10, 10)
		})
		draggable.addEventListener("dragend", () => {
			draggable.classList.remove("dragging_voice")
			var voice_container = document.querySelector(".App_PMC_voice_selector_voice_list")
			var draggables_voices = [...voice_container.querySelectorAll(".App_PMC_voice_selector_voice_item")]
			var new_voice_index_order = draggables_voices.map(div=>{return parseInt(div.getAttribute("value"))})
			DATA_rearrange_voices(new_voice_index_order)
		})
		draggable.addEventListener("dragover", e => {
			e.preventDefault()
			var after_element = PMC_get_drag_after_voice(voice_container, e.clientY)
			var draggable = voice_container.querySelector(".dragging_voice")
			if (after_element == null){
				voice_container.appendChild(draggable)
			} else {
				voice_container.insertBefore(draggable, after_element)
			}
		})
	})
}

//Voices find drag
function PMC_get_drag_after_voice(container, y) {
	var draggableElements = [...container.querySelectorAll(".App_PMC_voice_selector_voice_item:not(.dragging_voice)")]
	return draggableElements.reduce((closest, child) => {
		var box = child.getBoundingClientRect()

		var offset = y - box.top - box.height / 2
		if (offset < 0 && offset > closest.offset) {
			return { offset: offset, element: child }
		} else {
			return closest
		}
	}, { offset: Number.NEGATIVE_INFINITY }).element
}



