function RE_resize_voice_headers(){
	//resize all voice veaders based on voice displayed list
	var voice_matrix = document.querySelector(".App_RE_voice_matrix")
	var voice_list = [...voice_matrix.querySelectorAll(".App_RE_voice")]
	voice_list.forEach(voice=>{
		RE_resize_voice_header_vertical(voice)
		//var voice_header = voice.querySelector(".App_RE_voice_header")
		if(voice.classList.contains("minimized")){
			voice.classList.add("minimized")
			RE_minimize_voice_header(voice,true)
		}else{
			voice.classList.remove("minimized")
			RE_minimize_voice_header(voice,false)
		}
	})
}

function RE_count_voice_lines_displayed(voice){
	//var voice_data = voice.querySelector(".App_RE_voice_data")
	if(voice.classList.contains("minimized")) return 0
	var labels_container = voice.querySelector(".App_RE_voice_labels")
	var label_list = [...labels_container.querySelectorAll(".App_RE_label")]
	var visible = label_list.filter(label=>{return label.style.display!="none"})
	return visible.length
}

function RE_resize_voice_header_vertical(voice){
	var lines_needed = RE_count_voice_lines_displayed(voice) +2
	var header = voice.querySelector(".App_RE_voice_header")
	var extraDiv = [...header.querySelectorAll(".App_RE_voice_header_extra")]
	var centerColumn = header.querySelector(".App_RE_voice_header_C")
	var instrumentDiv = header.querySelector(".App_RE_voice_instrument")
	if(lines_needed<6){
		extraDiv.forEach(extra=>{extra.style.height= "0px"})
		if(lines_needed<5){
			centerColumn.style.paddingTop= "0px"
			instrumentDiv.style.height= "var(--RE_block2)"
		}else{
			centerColumn.style.paddingTop= "var(--RE_block)"
			instrumentDiv.style.height= "var(--RE_block3)"
		}
	}else{
		var string = "calc(var(--RE_block) * "+(lines_needed-5)+")"
		//extraDiv.style.height=`${(lines_needed-5)*28}px`
		extraDiv.forEach(extra=>{extra.style.height= string})
		centerColumn.style.paddingTop= "var(--RE_block)"
		instrumentDiv.style.height= "var(--RE_block3)"
	}
}

function RE_minimized_voice_header_button(button){
	var voice_obj = button.closest(".App_RE_voice")

	//maximize
	//make sure voice is visible
	var visible = RE_voice_is_visible(voice_obj)

	if(!visible){
		var [,voice_index] = RE_read_voice_id(voice_obj)
		DATA_show_hide_voice_RE(voice_index,true)
	}
}

function RE_minimize_voice_header(voice, minimize){
	var voice_header = voice.querySelector(".App_RE_voice_header")
	var lines_needed = RE_count_voice_lines_displayed(voice) + 2

	var vis_button = voice.querySelector(".App_RE_voice_visible")

	if(minimize){
		var voice_header_central= voice_header.querySelector(".App_RE_voice_header_C")
		var central_button_list= [...voice_header_central.querySelectorAll("button, input")]
		central_button_list.forEach(button=> {
			button.style="display:block"
		})

		if(lines_needed<=3){
			central_button_list[0].style="display:none"
		}
		if(lines_needed==2){
			central_button_list[3].style="display:none"
		}

		voice_header.querySelector(".App_RE_voice_header_L").style="display:none"
		//show R_minimized_button
		voice_header.querySelector(".App_RE_voice_header_R_minimized_button").style="display:block"
		var [name,id]=RE_read_voice_id(voice)
		voice_header.querySelector(".App_RE_voice_header_R_minimized_button").querySelector("p").innerHTML=name
		//hide others
		voice_header.querySelector(".App_RE_voice_menu_button").style="display:none"
		vis_button.style="display:none"
	}else{
		var central_button_list = [...voice_header.querySelector(".App_RE_voice_header_C").querySelectorAll("button ,input")]
		central_button_list.forEach(button=> {
			button.style="display:block"
		})
		voice_header.querySelector(".App_RE_voice_header_L").style="display:block"
		//hide R_minimized_button
		voice_header.querySelector(".App_RE_voice_header_R_minimized_button").style="display:none"
		//show others
		voice_header.querySelector(".App_RE_voice_menu_button").style="display:block"
		vis_button.style="display:block"
	}
}

function RE_show_hide_voice_button(button){
	var voice_obj = button.closest(".App_RE_voice")
	var [,voice_index] = RE_read_voice_id(voice_obj)
	var show
	if(button.classList.contains("show")){
		show=false
	}else{
		show=true
	}
	DATA_show_hide_voice_RE(voice_index,show)
}

function RE_show_hide_voice(voice,show){
	var show_button = voice.querySelector(".App_RE_voice_visible")

	var voice_labels_container = voice.querySelector(".App_RE_voice_labels")
	var labels = [...voice_labels_container.querySelectorAll("label")]

	var white_space_voice_end = voice.querySelector(".App_RE_white_space_voice_end")

	//iA line
	var voice_matrix = document.querySelector('.App_RE_voice_matrix')
	var [index, ] = RE_element_index(voice,'.App_RE_voice')
	var line_between_voices_list = [...voice_matrix.querySelectorAll('.App_RE_voices_between')]

	if (show){
		if (!show_button.classList.contains("show")){
			show_button.classList.add("show")
			show_button.innerHTML = "-"
			white_space_voice_end.classList.add("hidden")
			labels.forEach((item)=>{item.classList.remove("hidden")})
		}
	} else {
		if (show_button.classList.contains("show")){
			show_button.classList.remove("show")
			white_space_voice_end.classList.remove("hidden")
			labels.forEach((item)=>{item.classList.add("hidden")})
			//close head
			voice.classList.add("minimized")
			RE_minimize_voice_header(voice,true)
			RE_resize_voice_header_vertical(voice)
		}
	}
}

function RE_voice_is_visible(voice){
	var toggle = voice.querySelector(".App_RE_voice_visible")
	var class_string= toggle.className
	if (class_string.includes("show")){
		return true
	} else {
		return false
	}
}

function RE_block_unblock_voice(button){
	var class_string= button.className
	var RE_voice_block_icons = [...button.querySelectorAll(".App_RE_icon")]

	if (class_string.includes("pressed")){
		button.classList.remove("pressed")
		RE_voice_block_icons[0].style["display"] = 'none'
		RE_voice_block_icons[1].style["display"] = 'flex'

		var data = DATA_get_current_state_point(true)
		//Selected voice
		var voice_obj = button.closest(".App_RE_voice")
		voice_obj.classList.remove("blocked")
		var [,voice_index] = RE_read_voice_id(voice_obj)
		var selected_voice_data= data.voice_data.find(item=>{
			return item.voice_index==voice_index
		})
		selected_voice_data.blocked = false
		//var PMC_reload = flag_DATA_updated.PMC
		DATA_insert_new_state_point(data)
		//no need to load
		//flag_DATA_updated={PMC:PMC_reload,RE:true}
		flag_DATA_updated.RE=true
		RE_redraw_column_selection(data)
	} else {
		//control if voice is blockable

		//find all blocked voices
		var data = DATA_get_current_state_point(true)
		//Selected voice
		var voice_obj = button.closest(".App_RE_voice")
		var voice_index = RE_read_voice_id(voice_obj)[1]
		var current_voice_data= data.voice_data.find(item=>{
			return item.voice_index==voice_index
		})
		var blocked_voice_list = data.voice_data.filter(item=>{
			return item.blocked==true
		})
		//verify if voice is blockable
		var is_blockable = false
		if(blocked_voice_list.length==0 ) {
			is_blockable=true
		}else{
			//verify long and number every segment is the same
			var N_NP_this_voice= current_voice_data.neopulse.N
			var D_NP_this_voice= current_voice_data.neopulse.D

			var segment_data_list = current_voice_data.data.segment_data
			var segment_L_base_list = segment_data_list.map(item=>{
				var last_time = item.time.slice(-1)[0]
				return Math.round(last_time.P*N_NP_this_voice/D_NP_this_voice)
			})

			//create give a list of segment Longitud of a blocked voices and translate to 1/1 //XXX  TS
			var first_blocked_voice_data= blocked_voice_list[0]
			var N_NP= first_blocked_voice_data.neopulse.N
			var D_NP= first_blocked_voice_data.neopulse.D

			var first_blocked_segment_data_list = first_blocked_voice_data.data.segment_data
			var first_blocked_segment_L_base_list = first_blocked_segment_data_list.map(item=>{
				var last_time = item.time.slice(-1)[0]
				return Math.round(last_time.P*N_NP/D_NP)
			})

			if(segment_L_base_list.length==first_blocked_segment_L_base_list.length){
				var not_equal = segment_L_base_list.some((segment_L_base,index)=>{
					return segment_L_base!=first_blocked_segment_L_base_list[index]
				})
				if(!not_equal)is_blockable=true
			}
		}
		if(is_blockable){
			button.classList.add("pressed")
			voice_obj.classList.add("blocked")
			RE_voice_block_icons[0].style["display"] = 'flex'
			RE_voice_block_icons[1].style["display"] = 'none'

			current_voice_data.blocked = true

			//verify if segment names are the same
			var segment_name_changed=false
			if(blocked_voice_list.length!=0 ) {
				var first_blocked_voice_data= blocked_voice_list[0]
				first_blocked_voice_data.data.segment_data.forEach((segment,index)=>{
					if(segment.segment_name!=current_voice_data.data.segment_data[index].segment_name){
						current_voice_data.data.segment_data[index].segment_name=segment.segment_name
						segment_name_changed=true
					}
				})
			}

			//warning segment name changed
			if(segment_name_changed){
				console.warn("BLOCK VOICE CAUSED SEGMENT NAME CHANGES")
				DATA_insert_new_state_point(data)
				DATA_load_state_point_data(false,true)
			}else{
				//var PMC_reload = flag_DATA_updated.PMC
				DATA_insert_new_state_point(data)
				//no need to load
				//flag_DATA_updated={PMC:PMC_reload,RE:true}
				flag_DATA_updated.RE=true
				RE_redraw_column_selection(data)
			}
		}
	}
}

function RE_voice_is_blocked(voice){
	if(voice==null)return false
	var button = voice.querySelector(".App_RE_voice_block")
	var class_string= button.className
	if (class_string.includes("pressed")){
		return true
	}else{
		return false
	}
}

function RE_change_voice_instrument(button){
	APP_stop()

	//better using array
	var current_index = instrument_order.indexOf(Number(button.value))
	if(current_index==-1)console.error("instrument not found")
	var next_index=current_index+1
	if(next_index>=allinstruments)next_index=0

	button.value=instrument_order[next_index]
	button.querySelector('.App_RE_icon').src = instrument_icon_list[instrument_order[next_index]]

	var data = DATA_get_current_state_point(true)
	//Selected voice
	var voice_obj = button.closest(".App_RE_voice")
	var [,voice_index] = RE_read_voice_id(voice_obj)
	var selected_voice_data= data.voice_data.find(item=>{
		return item.voice_index==voice_index
	})
	selected_voice_data.instrument = instrument_order[next_index]

	DATA_insert_new_state_point(data)
	flag_DATA_updated.RE=true
}

//old define voice instrument
function RE_set_voice_instrument(voice,instrument_number=0){
	APP_stop()
	var instrument_button = voice.querySelector(".App_RE_voice_instrument")
	if(instrument_number>=allinstruments)instrument_number=0

	instrument_button.value=instrument_number
	instrument_button.querySelector('.App_RE_icon').src = instrument_icon_list[instrument_number]
}

function RE_which_voice_instrument(voice){  //XXX
	//console.error("OBSOLETE FUNCTION, use database DATA_which_voice_instrument(voice_index)")
	var voice_button = voice.querySelector(".App_RE_voice_instrument")
	return parseInt(voice_button.value)
}

function RE_change_metro_sound(button){
	APP_stop()
	var current_value = parseInt(button.value)
	var available_metro_sounds = DATA_list_available_metro_sounds()
	//var max_value = 5
	var max_value = available_metro_sounds-length-1
	current_value = available_metro_sounds.find(item=>{return item>current_value})
	//null if current = 5 (last one)
	if(current_value>max_value || current_value==null)current_value=0

	button.value=current_value
	if(current_value==0){
		button.innerHTML="M"
		button.classList.add('App_RE_text_strikethrough')
	}else{
		button.classList.remove('App_RE_text_strikethrough')
		button.innerHTML="M"+current_value
	}

	var data = DATA_get_current_state_point(true)
	//Selected voice
	var voice_obj = button.closest(".App_RE_voice")
	var [,voice_index] = RE_read_voice_id(voice_obj)
	var selected_voice_data= data.voice_data.find(item=>{
		return item.voice_index==voice_index
	})

	selected_voice_data.metro=current_value

	DATA_insert_new_state_point(data)
	flag_DATA_updated.RE=true
}

function RE_set_metro_sound(voice, metronome_number=0){
	APP_stop()
	var button = voice.querySelector(".App_RE_metro_sound")
	var available_metro_sounds = DATA_list_available_metro_sounds()
	//var max_value = 5
	var max_value = available_metro_sounds-length-1
	if(metronome_number>max_value)metronome_number=0

	button.value=metronome_number
	if(metronome_number==0){
		button.innerHTML="M"
		button.classList.add('App_RE_text_strikethrough')
	}else{
		button.classList.remove('App_RE_text_strikethrough')
		button.innerHTML="M"+metronome_number
	}
}

function RE_which_metro_sound(voice){
	var metro_button = voice.querySelector(".App_RE_metro_sound")
	return parseInt(metro_button.value)
}

function RE_change_voice_name(element){
	element.blur()
	var data = DATA_get_current_state_point(true)
	//Selected voice
	var voice_obj = element.closest(".App_RE_voice")
	var [,voice_index] = RE_read_voice_id(voice_obj)
	var selected_voice_data= data.voice_data.find(item=>{
		return item.voice_index==voice_index
	})

	selected_voice_data.name = element.value

	var PMC_reload = flag_DATA_updated.PMC
	DATA_insert_new_state_point(data)
	if(PMC_reload)PMC_populate_voice_list()
	//no need to load
	flag_DATA_updated={PMC:PMC_reload,RE:true}
}

function RE_set_voice_volume(voice, value){
	APP_stop()
	var volume_button = voice.querySelector(".App_RE_voice_volume")

	let img0 = "./Icons/audio-volume-muted-symbolic.svg"
	let img1 = "./Icons/audio-volume-min-symbolic.svg"
	let img2 = "./Icons/audio-volume-med-symbolic.svg"
	let img3 = "./Icons/audio-volume-high-symbolic.svg"
	let max_value = 3
	if(value>max_value)value=3
	var old_volume = volume_button.value
	volume_button.value = value

	if (value == 0){
		volume_button.querySelector('.App_RE_icon').src = img0
	} else if (value == 1) {
		volume_button.querySelector('.App_RE_icon').src = img1
	} else if (value == 2){
		volume_button.querySelector('.App_RE_icon').src = img2
	} else {
		volume_button.querySelector('.App_RE_icon').src = img3
	}
	//set mute button
	if (value == 0){
		RE_set_mute_voice(voice,true,old_volume)
	}else{
		RE_set_mute_voice(voice,false,old_volume)
	}
}

function RE_change_voice_volume(button){
	var volume= Number(button.value)+1
	let max_value = 3
	var voice = button.closest(".App_RE_voice")
	if(volume>max_value)volume=0

	RE_set_voice_volume(voice, volume)

	var data = DATA_get_current_state_point(true)
	//Selected voice
	var voice_obj = button.closest(".App_RE_voice")
	var [,voice_index] = RE_read_voice_id(voice_obj)
	var selected_voice_data= data.voice_data.find(item=>{
		return item.voice_index==voice_index
	})

	selected_voice_data.volume = volume

	//var PMC_reload = flag_DATA_updated.PMC
	DATA_insert_new_state_point(data)
	//if(PMC_reload){PMC_populate_voice_list()
	//no need to load
	//flag_DATA_updated={PMC:PMC_reload,RE:true}
	flag_DATA_updated.RE=true
}

function RE_read_voice_volume(voice){
	var button = voice.querySelector(".App_RE_voice_volume")
	var class_string= button.className
	return parseInt(button.value)
}


//return true if voice is muted
function RE_mute_unmute_voice(button) {
	var data = DATA_get_current_state_point(true)
	//Selected voice
	var voice_obj = button.closest(".App_RE_voice")
	var [,voice_index] = RE_read_voice_id(voice_obj)
	var selected_voice_data= data.voice_data.find(item=>{
		return item.voice_index==voice_index
	})

	var voice = button.closest(".App_RE_voice")
	if(button.classList.contains("pressed")){
		RE_set_voice_volume(voice, button.value)
		selected_voice_data.volume = RE_read_voice_volume(voice)
		//console.log(selected_voice_data.volume)
	}else{
		RE_set_voice_volume(voice, 0)
		selected_voice_data.volume = 0
	}

	//var PMC_reload = flag_DATA_updated.PMC
	DATA_insert_new_state_point(data)
	//if(PMC_reload)PMC_populate_voice_list()
	//no need to load
	//flag_DATA_updated={PMC:PMC_reload,RE:true}
	flag_DATA_updated.RE=true
}

function RE_set_mute_voice(voice,muted,old_volume){
	var button = voice.querySelector(".App_RE_voice_mute")
	if(muted){
		button.classList.add("pressed")
	}else{
		button.classList.remove("pressed")
	}
	button.value=old_volume
}

function RE_change_solo_voice(button){
	APP_stop()
	var voice = button.closest(".App_RE_voice")
	var data = DATA_get_current_state_point(true)
	//Selected voice
	var voice_obj = button.closest(".App_RE_voice")
	var [,voice_index] = RE_read_voice_id(voice_obj)
	var selected_voice_data= data.voice_data.find(item=>{
		return item.voice_index==voice_index
	})

	if(voice.className.includes("App_RE_solo")){
		voice.classList.remove("App_RE_solo")
		selected_voice_data.solo=false
	}
	else{
		voice.classList.add("App_RE_solo")
		selected_voice_data.solo=true
	}

	//var PMC_reload = flag_DATA_updated.PMC
	DATA_insert_new_state_point(data)
	//if(PMC_reload)PMC_populate_voice_list()
	//no need to load
	//flag_DATA_updated={PMC:PMC_reload,RE:true}
	flag_DATA_updated.RE=true
}

function RE_set_solo_voice(voice,solo){
	if(solo){
		//voice is solo
		if(!voice.className.includes("App_RE_solo")){
			voice.classList.add("App_RE_solo")
		}
	}else{
		//voice is not solo
		if(voice.className.includes("App_RE_solo")){
			voice.classList.remove("App_RE_solo")
		}
	}
}

function RE_enter_new_NP(element){
	if(element.value == previous_value) return
	var voice_obj = element.closest(".App_RE_voice")
	var [N,D]= RE_Calculate_NP(voice_obj)
	if(N==0 || D==0 || isNaN(D)){
		console.error("Enter correct NP fraction please...")

		APP_blink_error(element)
		element.value = previous_value
		return
	}
	//calculating if every segment existing is multiple of N
	var NP_ok=false
	var data = DATA_get_current_state_point(true)
	var segment_Ls_list = []
	data.voice_data.forEach(voice=>{
		voice.data.segment_data.forEach(segment=>{
			segment_Ls_list.push(segment.time.slice(-1)[0].P*voice.neopulse.N/voice.neopulse.D)
		})
	})
	//console.log(segment_Ls_list)
	NP_ok=!segment_Ls_list.find(Ls=>{return Ls%N!=0})
	if(!NP_ok){
		element.value = previous_value
		APP_blink_error(element)
		console.error("at least one segment Ls in the composition not divisible by N neopulse")
		return
	}

	APP_stop()
	//Selected voice
	var [,voice_index] = RE_read_voice_id(voice_obj)
	var selected_voice_data= data.voice_data.find(item=>{
		return item.voice_index==voice_index
	})

	//changing L segments to new one and clearing them
	var N_old = selected_voice_data.neopulse.N
	var D_old = selected_voice_data.neopulse.D

	selected_voice_data.data.segment_data.forEach(segment=>{
		let Pend = (Math.round(segment.time.slice(-1)[0].P*N_old/D_old))*(D/N)
		segment.time = [{"P":0,"F":0},{"P": Pend,"F":0}]
		segment.fraction = [{"N": 1,"D": 1,"start": 0,"stop":Pend}]
		segment.note = [-1,-4]
		segment.diesis = [true,true]
	})
	selected_voice_data.neopulse={"N":N,"D":D}
	//recalc midi
	selected_voice_data.data.midi_data=DATA_segment_data_to_midi_data(selected_voice_data.data.segment_data,selected_voice_data.neopulse)

	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function RE_change_voice_pulse_sound(button){  //XXX NOT IMPLEMENTED BUTTON YET
	APP_stop()
	let img0 = "./Icons/relieved.svg"
	let img1 = "./Icons/confused.svg"
	let img2 = "./Icons/mindblow.svg"
	let img3 = "./Icons/sleepy.svg"
	let img4 = "./Icons/kiss.svg"
	let max_value = 4
	if (button.value == 0) {
		button.querySelector('.App_RE_icon').src = img1

	} else if (button.value == 1){
		button.querySelector('.App_RE_icon').src = img2

	} else if (button.value == 2){
		button.querySelector('.App_RE_icon').src = img3

	} else if (button.value == 3){
		button.querySelector('.App_RE_icon').src = img4

	} else {
		button.querySelector('.App_RE_icon').src = img0

	}
	button.value++
	if(button.value>max_value)button.value=0

	//DATA_save XXX
}
