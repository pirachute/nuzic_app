var _listeners = []

EventTarget.prototype.addEventListenerBase = EventTarget.prototype.addEventListener;
EventTarget.prototype.addEventListener = function(type, listener)
{
	_listeners.push({target: this, type: type, listener: listener});
	this.addEventListenerBase(type, listener);
};

EventTarget.prototype.removeEventListeners = function(targetType)
{
	for(var index = 0; index != _listeners.length; index++)
	{
		var item = _listeners[index];

		var target = item.target;
		var type = item.type;
		var listener = item.listener;

		if(target == this && type == targetType)
		{
			this.removeEventListener(type, listener);
		}
	}
}

//Segments
//var RE_Dragndrop = {
//	RE_refresh_draggable_segments: function(){
//	},
//	RE_refresh_draggable_voices: function(){
//	}
//}
function RE_refresh_draggable_segments(){
		var voice_matrix = document.querySelector('.App_RE_voice_matrix')
		var draggables_segments = [...voice_matrix.querySelectorAll('.App_RE_segment')]
		var containers_matrix = [...voice_matrix.querySelectorAll('.App_RE_voice_data')]
		var voice_list = [...voice_matrix.querySelectorAll(".App_RE_voice")]

		//clear all draggable event listeners (dragstart, dragend, dragover)

		draggables_segments.forEach(draggable => {
			draggable.removeEventListeners('dragstart')
			draggable.removeEventListeners('dragend')
		})
		containers_matrix.forEach(container => {
			container.removeEventListeners('dragover')
		})

		//add event listener
		draggables_segments.forEach(draggable => {
			draggable.addEventListener('dragstart', e => {
				draggable.classList.add('dragging_segment')

				var canvas = document.createElement('canvas')
				var context = canvas. getContext('2d')
				context. clearRect(0, 0, canvas. width, canvas. height)
				e.dataTransfer.setDragImage(canvas, 10, 10)

				e.stopPropagation()
			})

			draggable.addEventListener('dragend', e => {
				e.stopPropagation()
				draggable.classList.remove('dragging_segment')
				var segment_container = draggable.closest(".App_RE_voice_data")
				var draggables_segments = [...segment_container.querySelectorAll(".App_RE_segment")]
				//var new_voice_index_order = draggables_voices.map(div=>{return parseInt(div.getAttribute('value'))})
				var new_segment_index_order = draggables_segments.map(segment=>{
					var segment_index= parseInt(segment.querySelector(".App_RE_segment_number").innerHTML)
					return segment_index
				})
				//console.log(new_segment_index_order.some((value,index)=>{return value!=index}))
				if(new_segment_index_order.some((value,index)=>{return value!=index})){
					var voice_index = RE_read_voice_id(segment_container.closest(".App_RE_voice"))[1]
					DATA_rearrange_segments(voice_index,new_segment_index_order)
				}
			})
		})
		containers_matrix.forEach(container => {
			container.addEventListener('dragover', e => {
				e.preventDefault()
				e.stopPropagation()
				var afterElement = RE_get_drag_after_segment(container, e.clientX)

				var draggable = document.querySelector('.dragging_segment')
				if(draggable==null)return
				var draggable_voice = draggable.closest(".App_RE_voice")

				var [index, ] = RE_element_index(draggable,'.App_RE_segment')
				var spacer_list = [...draggable.parentNode.querySelectorAll('.App_RE_segment_spacer')]
				var indexAE=-1
				//PREVENT DRAG TO OTHER VOICE
				if(container===draggable.parentNode){
					if (afterElement == null) {
						//Last element
						//not here , there is segment spacer end
						//container.appendChild(draggable)
						//container.appendChild(spacer_list[index])
						var end = container.querySelector(".App_RE_segment_spacer_end")
						container.insertBefore(draggable,end)
						container.insertBefore(spacer_list[index],end)
					} else {
						[indexAE, ] = RE_element_index(afterElement,'.App_RE_segment')
						container.insertBefore(draggable, afterElement)
						container.insertBefore(spacer_list[index],afterElement)
					}

					if(RE_voice_is_blocked(draggable_voice)){
						//find all voices with same polipulse fractioning and blocked and move the corresponding segment too
						var sync_voice_list = voice_list.filter(voice => {
							//drag if polipulse blocked!
							return RE_voice_is_blocked(voice) && voice!=draggable_voice
						})
						//move synched segments [index ]in place
						sync_voice_list.forEach((voice)=>{
							var sync_segment_list=[...voice.querySelectorAll(".App_RE_segment")]
							var sync_container = sync_segment_list[index].parentNode
							var sync_spacer_list=[...sync_container.querySelectorAll(".App_RE_segment_spacer")]
							//moving sync_segment_list[index] in position indexAE
							if(indexAE!=-1){
								sync_container.insertBefore(sync_segment_list[index], sync_segment_list[indexAE])
								sync_container.insertBefore(sync_spacer_list[index],sync_segment_list[indexAE])
							}else{
								//append at the end
								//not here , there is segment spacer end
								//sync_container.appendChild(sync_segment_list[index])
								//sync_container.appendChild(sync_spacer_list[index])
								var end = sync_container.querySelector(".App_RE_segment_spacer_end")
								sync_container.insertBefore(sync_segment_list[index],end)
								sync_container.insertBefore(sync_spacer_list[index],end)
							}
						})
					}

					APP_stop()
				}
			})
		})
}

function RE_refresh_draggable_voices(){
		var voice_matrix = document.querySelector('.App_RE_voice_matrix')
		var draggables_voices = [...voice_matrix.querySelectorAll('.App_RE_voice')]

		draggables_voices.forEach(draggable => {
			draggable.addEventListener('dragstart', (e) => {
				draggable.classList.add('dragging_voice')
				//console.log(draggable)
				var canvas = document.createElement('canvas')
				var context = canvas. getContext('2d')
				context. clearRect(0, 0, canvas. width, canvas. height)
				e.dataTransfer.setDragImage(canvas, 10, 10)
			})
			draggable.addEventListener('dragend', () => {
				draggable.classList.remove('dragging_voice')
				var voice_container = document.querySelector('.App_RE_voice_matrix')
				var draggables_voices = [...voice_container.querySelectorAll('.App_RE_voice')]
				//var new_voice_index_order = draggables_voices.map(div=>{return parseInt(div.getAttribute('value'))})
				var new_voice_index_order = draggables_voices.map(voice_obj=>{return RE_read_voice_id(voice_obj)[1]})
				DATA_rearrange_voices(new_voice_index_order)
			})
		})

		voice_matrix.addEventListener('dragover', e => {
				e.preventDefault()
				//var afterElement = RE_get_drag_after_voice(container, e.clientY)
				var afterElement = RE_get_drag_after_voice(voice_matrix, e.clientY)
				var draggable = document.querySelector('.dragging_voice')
				if(draggable==null)return
				var [index, ] = RE_element_index(draggable,'.App_RE_voice')
				var iA_list = [...voice_matrix.querySelectorAll('.App_RE_voices_between')]
				if (afterElement == null) {
					//if compasses is not in the container
					//append before Compasses etc
					var final_child = voice_matrix.querySelector(".RE_modules")

					voice_matrix.insertBefore(iA_list[index],final_child)
					voice_matrix.insertBefore(draggable, final_child)
					//reset scrollbar horizontal Obsolete
					//Reset_main_horizontal_scrollbar()
				} else {
					voice_matrix.insertBefore(draggable, afterElement)
					voice_matrix.insertBefore(iA_list[index],afterElement)
				}
				//REFRESH IA visibility
				RE_check_iA_line_visibility()
			})

}

//PROBABLY NOT NEEDED XXX
//RE_Dragndrop.RE_refresh_draggable_segments()
//RE_Dragndrop.RE_refresh_draggable_voices()

function RE_check_iA_line_visibility(){
	var voice_matrix = document.querySelector(".App_RE_voice_matrix")
	var line_between_voices_list = [...voice_matrix.querySelectorAll(".App_RE_voices_between")]
	var voice_list = [...voice_matrix.querySelectorAll(".App_RE_voice")]
	var first_line = line_between_voices_list.shift()
	first_line.querySelector(".App_RE_label").classList.add('hidden')
	first_line.querySelector(".App_RE_iA").classList.add('hidden')
	var first_iA_shown = true
	if(RE_voice_is_visible(voice_list[0]))first_iA_shown=false
	var index=1
	line_between_voices_list .forEach((item) =>{
		if(RE_voice_is_visible(voice_list[index])){
			if(first_iA_shown){
				//first voice shown has iA still hidden
				item.querySelector(".App_RE_label").classList.add('hidden')
				item.querySelector(".App_RE_iA").classList.add('hidden')
				first_iA_shown=false
			}else{
				//
				item.querySelector(".App_RE_label").classList.remove('hidden')
				item.querySelector(".App_RE_iA").classList.remove('hidden')
			}
		}else{
			//hidden
			item.querySelector(".App_RE_label").classList.add('hidden')
			item.querySelector(".App_RE_iA").classList.add('hidden')
		}
		index++
	})
}


function RE_get_drag_after_segment(container, x) {
	//list of draggable segments - segment that i'm currently dragging
	var draggableElements = [...container.querySelectorAll('.App_RE_segment:not(.dragging_segment)')]

	return draggableElements.reduce((closest, child) => {
		var box = child.getBoundingClientRect()
		var offset = x - box.left - box.width / 2
		if (offset < 0 && offset > closest.offset) {
			return { offset: offset, element: child }
		} else {
			return closest
		}
	}, { offset: Number.NEGATIVE_INFINITY }).element
}

//Voices

function RE_get_drag_after_voice(container, y) {
	var draggableElements = [...container.querySelectorAll('.App_RE_voice:not(.dragging_voice)')]
	return draggableElements.reduce((closest, child) => {
		var box = child.getBoundingClientRect()
		var offset = y - box.top - box.height / 2
		if (offset < 0 && offset > closest.offset) {
			return { offset: offset, element: child }
		} else {
			return closest
		}
	}, { offset: Number.NEGATIVE_INFINITY }).element
}

//document.addEventListener('keyup', function(e){console.log(_listeners)});
