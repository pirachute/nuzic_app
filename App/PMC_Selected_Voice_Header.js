function PMC_load_selected_voice_header(voice_data){
	var container = document.getElementById("App_PMC_selected_voice_header")
	var instrument_button = container.querySelector(".App_PMC_selected_voice_instrument")
	instrument_button.value=voice_data.instrument
	//instrument_button.querySelector('.App_RE_icon').src = instrument_icon_list[instrument_order[next_index]]
	instrument_button.innerHTML = instrument_name_list[voice_data.instrument]

	var volume_button = container.querySelector(".App_PMC_selected_voice_volume")
	volume_button.value= voice_data.volume
	let img0 = "./Icons/audio-volume-muted-symbolic.svg"
	let img1 = "./Icons/audio-volume-min-symbolic.svg"
	let img2 = "./Icons/audio-volume-med-symbolic.svg"
	let img3 = "./Icons/audio-volume-high-symbolic.svg"
	if (voice_data.volume == 0){
		volume_button.querySelector('.App_PMC_icon').src = img0
	} else if (voice_data.volume == 1) {
		volume_button.querySelector('.App_PMC_icon').src = img1
	} else if (voice_data.volume == 2){
		volume_button.querySelector('.App_PMC_icon').src = img2
	} else {
		volume_button.querySelector('.App_PMC_icon').src = img3
	}

	var block_button = container.querySelector(".App_PMC_selected_voice_block")
	var block_icons = [...block_button.querySelectorAll(".App_PMC_icon")]
	if(voice_data.blocked){
		//block_button.classList.add("pressed")
		block_icons[0].style["display"] = 'flex'
		block_icons[1].style["display"] = 'none'
	}else{
		//block_button.classList.remove("pressed")
		block_icons[0].style["display"] = 'none'
		block_icons[1].style["display"] = 'flex'
	}

	var metro_button = container.querySelector(".App_PMC_selected_voice_metro_sound")
	metro_button.value=voice_data.metro
	if(voice_data.metro==0){
		metro_button.innerHTML="M"
		metro_button.classList.add('App_PMC_text_strikethrough')
	}else{
		metro_button.classList.remove('App_PMC_text_strikethrough')
		metro_button.innerHTML="M"+voice_data.metro
	}
	//App_PMC_text_strikethrough
	var neopulse_input = container.querySelector(".App_PMC_selected_voice_neopulse_inp_box")
	neopulse_input.value=voice_data.neopulse.N+"/"+voice_data.neopulse.D
}


function PMC_change_selected_voice_instrument(button){
	APP_stop()
	var current_index = instrument_order.indexOf(Number(button.value))
	if(current_index==-1)console.error("instrument not found")
	var next_index=current_index+1
	if(next_index>=allinstruments)next_index=0

	button.value=instrument_order[next_index]
	button.innerHTML = instrument_name_list[instrument_order[next_index]]

	var data = DATA_get_current_state_point(true)
	//Selected voice
	var selected_voice_data= data.voice_data.find(item=>{
		return item.selected
	})
	selected_voice_data.instrument = instrument_order[next_index]

	DATA_insert_new_state_point(data)
	//PMC_load_selected_voice_header(selected_voice_data)
	flag_DATA_updated.PMC=true
}


function PMC_change_selected_voice_volume(button){
	APP_stop()
	var volume= Number(button.value)+1
	let max_value = 3
	if(volume>max_value)volume=0

	var data = DATA_get_current_state_point(true)
	//Selected voice
	var selected_voice_data= data.voice_data.find(item=>{
		return item.selected
	})
	selected_voice_data.volume = volume
	DATA_insert_new_state_point(data)

	//no need to rec all
	PMC_load_selected_voice_header(selected_voice_data)
	PMC_populate_voice_list()
	flag_DATA_updated.PMC=true
}

function PMC_block_unblock_selected_voice(button){
	var data = DATA_get_current_state_point(true)
	//Selected voice
	var selected_voice_data= data.voice_data.find(item=>{
		return item.selected
	})
	if(selected_voice_data.blocked){
		selected_voice_data.blocked = false
		DATA_insert_new_state_point(data)
		PMC_load_selected_voice_header(selected_voice_data)
		//no need to load
		flag_DATA_updated.PMC=true
	}else{
		//control if blockable
		var blocked_voice_list = data.voice_data.filter(item=>{
			return item.blocked==true
		})
		//verify if voice is blockable
		var is_blockable = false
		if(blocked_voice_list.length==0 ) {
			is_blockable=true
		}else{
			//verify long and number every segment is the same
			var N_NP_this_voice= selected_voice_data.neopulse.N
			var D_NP_this_voice= selected_voice_data.neopulse.D

			var segment_data_list = selected_voice_data.data.segment_data
			var segment_L_base_list = segment_data_list.map(item=>{
				var last_time = item.time.slice(-1)[0]
				return Math.round(last_time.P*N_NP_this_voice/D_NP_this_voice)
			})

			//create give a list of segment Longitud of a blocked voices and translate to 1/1 //XXX  TS
			var first_blocked_voice_data= blocked_voice_list[0]
			var N_NP= first_blocked_voice_data.neopulse.N
			var D_NP= first_blocked_voice_data.neopulse.D

			var first_blocked_segment_data_list = first_blocked_voice_data.data.segment_data
			var first_blocked_segment_L_base_list = first_blocked_segment_data_list.map(item=>{
				var last_time = item.time.slice(-1)[0]
				return Math.round(last_time.P*N_NP/D_NP)
			})

			if(segment_L_base_list.length==first_blocked_segment_L_base_list.length){
				var not_equal = segment_L_base_list.some((segment_L_base,index)=>{
					return segment_L_base!=first_blocked_segment_L_base_list[index]
				})
				if(!not_equal)is_blockable=true
			}
		}
		if(is_blockable){
			selected_voice_data.blocked = true

			//verify if segment names are the same
			var segment_name_changed=false
			if(blocked_voice_list.length!=0 ) {
				var first_blocked_voice_data= blocked_voice_list[0]
				first_blocked_voice_data.data.segment_data.forEach((segment,index)=>{
					if(segment.segment_name!=selected_voice_data.data.segment_data[index].segment_name){
						selected_voice_data.data.segment_data[index].segment_name=segment.segment_name
						segment_name_changed=true
					}
				})
			}

			//warning segment name changed
			if(segment_name_changed){
				console.warn("BLOCK VOICE CAUSED SEGMENT NAME CHANGES")
				DATA_insert_new_state_point(data)
				DATA_load_state_point_data(false,true)
			}else{
				//var PMC_reload = flag_DATA_updated.PMC
				DATA_insert_new_state_point(data)
				//no need to load
				PMC_load_selected_voice_header(selected_voice_data)
				//no need to load
				flag_DATA_updated.PMC=true
			}
		}else{
			return
		}
	}
}

function PMC_change_metro_sound_selected_voice(button){
	APP_stop()
	var current_value = parseInt(button.value)
	var available_metro_sounds = DATA_list_available_metro_sounds()
	//var max_value = 5
	var max_value = available_metro_sounds-length-1
	current_value = available_metro_sounds.find(item=>{return item>current_value})
	//null if current = 5 (last one)
	if(current_value>max_value || current_value==null)current_value=0

	var data = DATA_get_current_state_point(true)
	//Selected voice
	var selected_voice_data= data.voice_data.find(item=>{
		return item.selected
	})

	selected_voice_data.metro=current_value

	DATA_insert_new_state_point(data)
	PMC_load_selected_voice_header(selected_voice_data)
	flag_DATA_updated.PMC=true
}

function PMC_selected_voice_enter_new_NP(element){
	if(element.value == previous_value) return

	var NP_voice = element.value
	var split = NP_voice.split('/')
	var N = Math.floor(split[0])
	var D = Math.floor(split[1])

	if(N==0 || D==0 || isNaN(D)){
		console.error("Enter correct NP fraction please...")
		APP_blink_error(element)
		element.value = previous_value
		return
	}

	//calculating if every segment existing is multiple of N
	var NP_ok=false
	var data = DATA_get_current_state_point(true)
	var segment_Ls_list = []
	data.voice_data.forEach(voice=>{
		voice.data.segment_data.forEach(segment=>{
			segment_Ls_list.push(segment.time.slice(-1)[0].P*voice.neopulse.N/voice.neopulse.D)
		})
	})
	//console.log(segment_Ls_list)
	NP_ok=!segment_Ls_list.find(Ls=>{return Ls%N!=0})
	if(!NP_ok){
		element.value = previous_value
		APP_blink_error(element)
		return
	}

	APP_stop()
	//Selected voice
	var selected_voice_data= data.voice_data.find(item=>{
		return item.selected
	})

	//changing L segments to new one and clearing them
	var N_old = selected_voice_data.neopulse.N
	var D_old = selected_voice_data.neopulse.D

	selected_voice_data.data.segment_data.forEach(segment=>{
		let Pend = (Math.round(segment.time.slice(-1)[0].P*N_old/D_old))*(D/N)
		segment.time = [{"P":0,"F":0},{"P": Pend,"F":0}]
		segment.fraction = [{"N": 1,"D": 1,"start": 0,"stop":Pend}]
		segment.note = [-1,-4]
		segment.diesis = [true,true]
	})
	selected_voice_data.neopulse={"N":N,"D":D}
	//recalc midi
	selected_voice_data.data.midi_data=DATA_segment_data_to_midi_data(selected_voice_data.data.segment_data,selected_voice_data.neopulse)

	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}
