function RE_reset_selection(){
	////single segment
	var single_segment= document.getElementById("App_RE_segment_selected_svg")
	if(single_segment!=null){
		//div to be eliminated
		var div = single_segment.parentNode
		var segment_obj = div.parentNode
		segment_obj.removeChild(div)
	}
	//multiple segments
	var voice_matrix= document.querySelector(".App_RE_voice_matrix")
	var segment_list=[...voice_matrix.querySelectorAll(".App_RE_segment")]
	segment_list.forEach(segment=>{
		segment.classList.remove("selected")
	})

	//voice
	var container= document.querySelector(".App_RE_voice_matrix")
	var voice_list = [...container.querySelectorAll(".App_RE_voice.selected")]
	voice_list.forEach(voice=>{
		voice.classList.remove("selected")
	})

	//columns
	var container= document.getElementById("App_RE_column_selector_container")
	var column_list = [...container.querySelectorAll(".selected")]
	column_list.forEach(segment=>{
		segment.classList.remove("selected")
	})

	//clear svg
	var div='<svg id="App_RE_column_selector_svg"></svg>'
	var selection_svg = document.getElementById("App_RE_column_selector_svg")
	selection_svg.outerHTML=div
	var div_top='<svg id="App_RE_column_selector_top_svg"></svg>'
	var selection_top_svg = document.getElementById("App_RE_column_selector_top_svg")
	selection_top_svg.outerHTML=div_top
}

/////*SEGMENT*////////

function RE_toggle_selection_segment(segment_child){
	var segment_obj = segment_child.closest(".App_RE_segment")
	if(segment_obj.classList.contains("selected")){
		RE_reset_selection()
	}else{
		RE_reset_selection()
		RE_select_segment(segment_obj)
	}
}

function RE_toggle_dropdown_segment(button){
	var parent = button.closest(".App_RE_dropdown_segment")
	parent.querySelector(".App_RE_dropdown_segment_content").classList.toggle("show")
	var segment_obj = button.closest(".App_RE_segment")
	if(segment_obj.classList.contains("selected")!=parent.querySelector(".App_RE_dropdown_segment_content").classList.contains("show"))RE_toggle_selection_segment(button)
}

function RE_select_segment(segment_obj){
	segment_obj.classList.add("selected")
	//segment_obj.classList.add("selected_green")
	//add a svg
	var svg=`<svg id="App_RE_segment_selected_svg">
					<rect x="0" y="0"  width="100%" height="100%" fill="var(--Nuzic_green_light)" />
					<line x1="0" y1="0" x2="0" y2="100%" stroke="var(--Nuzic_green)" stroke-width="${nuzic_block+2}"></line>
					<line x1="100%" y1="0" x2="100%" y2="100%" stroke="var(--Nuzic_green)" stroke-width="${nuzic_block+2}"></line>
					<line x1="0" y1="0" x2="100%" y2="0" stroke="var(--Nuzic_green)" stroke-width="6"></line>
					<line x1="0" y1="100%" x2="100%" y2="100%" stroke="var(--Nuzic_green)" stroke-width="6"></line>
			</svg>`

	//	segment_obj.innerHTML+=div
	var div=document.createElement("div")
	//div.outerHTML=svg
	div.innerHTML=svg
	segment_obj.appendChild(div)
}

//clear segment button
function RE_clear_segment_button(button){
	APP_stop()

	var voice = button.closest(".App_RE_voice")
	// define index
	var segment = button.closest(".App_RE_segment")
	var [segment_index,] = RE_element_index(segment, ".App_RE_segment")
	DATA_clear_segment(RE_read_voice_id(voice)[1],segment_index)
}

//copy segment data
function RE_copy_segment_button(button){
	var voice = button.closest(".App_RE_voice")
	// define index
	var segment = button.closest(".App_RE_segment")
	var [segment_index,] = RE_element_index(segment, ".App_RE_segment")
	DATA_copy_segment(RE_read_voice_id(voice)[1],segment_index)
}

//paste segment data
function RE_paste_segment_button(button){
	var voice = button.closest(".App_RE_voice")
	// define index
	var segment = button.closest(".App_RE_segment")
	var [segment_index,] = RE_element_index(segment, ".App_RE_segment")
	DATA_paste_segment(RE_read_voice_id(voice)[1],segment_index)
}

function RE_change_segment_name_button(button){
	//focus on segment name
	var menu_div = button.closest(".App_RE_dropdown_segment_content")
	//menu_div.style.display = "none"
	menu_div.classList.remove("show")

	var container = button.closest(".App_RE_segment_draggable")
	//var segment_index= parseInt(container.querySelector(".App_RE_segment_number").innerHTML)

	var segment_name = container.querySelector(".App_RE_segment_name")
	if(segment_name.classList.contains("hidden")){
		//if hidden
		segment_name.classList.remove("hidden")
		segment_name.classList.add("washidden")
	}

	segment_name.disabled=false
	segment_name.focus()
	segment_name.select()
}

function RE_repeat_segment_button(button,e){
	e.stopPropagation()
	APP_close_all_dropdown_segment_contents()
	var voice_obj = button.closest(".App_RE_voice")
	var [,voice_index] = RE_read_voice_id(voice_obj)
	var voice_obj_list = [...voice_obj.parentNode.querySelectorAll(".App_RE_voice")]
	var voice_number= voice_obj_list.length - 1 -voice_obj_list.indexOf(voice_obj)
	var segment_obj= button.closest(".App_RE_segment")
	var segment_obj_list=[...segment_obj.parentNode.querySelectorAll(".App_RE_segment")]
	var segment_index= segment_obj_list.indexOf(segment_obj)
	//RE_select_XXXXXXXXXXXXX(column_number)
	APP_show_operation_dialog_box({target:'segment',working_space:'RE',voice_number:voice_number,voice_index:voice_index,segment_index:segment_index,type:'repetitions'})
}

function RE_operations_segment_button(button,e){
	e.stopPropagation()
	APP_close_all_dropdown_segment_contents()
	//Selected voice
	var voice_obj = button.closest(".App_RE_voice")
	var [,voice_index] = RE_read_voice_id(voice_obj)
	var voice_obj_list = [...voice_obj.parentNode.querySelectorAll(".App_RE_voice")]
	var voice_number= voice_obj_list.length - 1 -voice_obj_list.indexOf(voice_obj)
	var segment_obj= button.closest(".App_RE_segment")
	var segment_obj_list=[...segment_obj.parentNode.querySelectorAll(".App_RE_segment")]
	var segment_index= segment_obj_list.indexOf(segment_obj)
	//RE_select_XXXXXXXXXX(column_number)
	APP_show_operation_dialog_box({target:'segment',working_space:'RE',voice_number:voice_number,voice_index:voice_index,segment_index:segment_index,type:'operations'})
}

/////*VOICE*////////

function RE_toggle_selection_voice(voice_child){
	var voice_obj = voice_child.closest(".App_RE_voice")
	//find if minimized
	//App_RE_voice_header minimized
	//App_RE_voice_data hidden
	// var voice_data_obj = voice_obj.querySelector(".App_RE_voice_data")
	// if(voice_data_obj.classList.contains("hidden"))return
	if(voice_obj.classList.contains("minimized"))return

	if(voice_obj.classList.contains("selected")){
		RE_reset_selection()
	}else{
		RE_reset_selection()
		RE_select_voice(voice_obj)
	}
}

function RE_toggle_dropdown_voice(button){
	var parent = button.closest(".App_RE_voice_header_R")
	parent.querySelector(".App_RE_voice_dropdown_menu ").classList.toggle("show")
	var voice_obj = button.closest(".App_RE_voice")
	if(voice_obj.classList.contains("selected")!=parent.querySelector(".App_RE_voice_dropdown_menu ").classList.contains("show"))RE_toggle_selection_voice(button)
}

function RE_select_voice(voice_obj){
	voice_obj.classList.add("selected")
}

//voices management
/*/OBSOLETE - use EDITION REPETITIONS
function RE_clone_voice_button(button){
	// define segment object
	APP_stop()
	var voice_object = button.closest('.App_RE_voice')
	var voice_index=RE_read_voice_id(voice_object)[1]

	DATA_clone_voice(voice_index)
}*/

function RE_add_voice_button(button){
	// define segment object
	APP_stop()
	var voice_object = button.closest('.App_RE_voice')
	var voice_index=RE_read_voice_id(voice_object)[1]

	DATA_add_voice(voice_index)
}

function RE_clear_voice_button(button){
	// define segment object
	APP_stop()
	var voice_object = button.closest('.App_RE_voice')
	var voice_index=RE_read_voice_id(voice_object)[1]

	DATA_clear_voice(voice_index)
}

function RE_delete_voice_button(button){
	//if it is not the only voice
	APP_stop()
	var voice = button.closest(".App_RE_voice")
	var voice_index=RE_read_voice_id(voice)[1]
	console.log(voice_index)
	DATA_delete_voice(voice_index)
}

function RE_repeat_voice_button(button,e){
	e.stopPropagation()
	APP_close_all_dropdown_segment_contents()
	var voice_obj = button.closest(".App_RE_voice")
	var [,voice_index] = RE_read_voice_id(voice_obj)
	var voice_obj_list = [...voice_obj.parentNode.querySelectorAll(".App_RE_voice")]
	var voice_number= voice_obj_list.length - 1 -voice_obj_list.indexOf(voice_obj)
	//RE_select_XXXXXXX(column_number)
	APP_show_operation_dialog_box({target:'voice',working_space:'RE',voice_number:voice_number,voice_index:voice_index,type:'repetitions'})
}

function RE_operations_voice_button(button,e){
	e.stopPropagation()
	APP_close_all_dropdown_segment_contents()
	//Selected voice
	var voice_obj = button.closest(".App_RE_voice")
	var [,voice_index] = RE_read_voice_id(voice_obj)
	var voice_obj_list = [...voice_obj.parentNode.querySelectorAll(".App_RE_voice")]
	var voice_number= voice_obj_list.length - 1 -voice_obj_list.indexOf(voice_obj)
	APP_show_operation_dialog_box({target:'voice',working_space:'RE',voice_number:voice_number,voice_index:voice_index,type:'operations'})
}

/////*COLUMN*////////

function RE_toggle_selection_segment_column(column_number){
	var container= document.getElementById("App_RE_column_selector_container")
	var column_list = [...container.querySelectorAll(".App_RE_column_selector_segment")]
	if(column_list[column_number]==null)return
	if(column_list[column_number].classList.contains("selected")){
		RE_reset_selection()
	}else{
		RE_reset_selection()
		RE_select_segment_column(column_number)
	}
}

function RE_toggle_dropdown_column_selector_segment(button){
	var parent = button.closest(".App_RE_column_selector_segment_menu")
	parent.querySelector(".App_RE_dropdown_column_selector_content").classList.toggle("show")
	var container= document.getElementById("App_RE_column_selector_container")
	var column_obj_list = [...container.querySelectorAll(".App_RE_column_selector_segment")]
	var current_column_obj = parent.closest(".App_RE_column_selector_segment")
	var current_column_number = column_obj_list.indexOf(current_column_obj)
	if(current_column_obj.classList.contains("selected")!=parent.querySelector(".App_RE_dropdown_column_selector_content").classList.contains("show"))RE_toggle_selection_segment_column(current_column_number)
}

function RE_select_segment_column(column_number){
	//header
	var data = DATA_get_current_state_point(false)
	var blocked_voice=data.voice_data.find(voice=>{return voice.blocked==true})

	if(typeof(blocked_voice) != "undefined"){
		var container= document.getElementById("App_RE_column_selector_container")
		var column_list = [...container.querySelectorAll(".App_RE_column_selector_segment")]
		column_list[column_number].classList.add("selected")

		//svg
		var selection_svg = document.getElementById("App_RE_column_selector_svg")
		var selection_top_svg = document.getElementById("App_RE_column_selector_top_svg")

		//calculating svg line position and width
		var segment_list = blocked_voice.data.segment_data
		var N_NP = blocked_voice.neopulse.N
		var D_NP = blocked_voice.neopulse.D

		var Psg_=0

		var current_seg_col_n=0
		var current_seg_col_start=1
		segment_list.find((segment,indexS)=>{
			var Psg_segment_start = Psg_
			var Psg_segment_end = Psg_segment_start + segment.time.slice(-1)[0].P
			Psg_=Psg_segment_end

			//calculate times
			var time_segment_start= DATA_calculate_exact_time(Psg_segment_start,0,0,1,1,N_NP,D_NP)
			var time_segment_end= DATA_calculate_exact_time(Psg_segment_end,0,0,1,1,N_NP,D_NP)

			var index_col_start = RE_find_time_column_index(time_segment_start)
			var index_col_end = RE_find_time_column_index(time_segment_end)
			var n_column = (index_col_end-index_col_start)-1
			if(indexS==column_number){
				current_seg_col_n=n_column
			}else{
				current_seg_col_start+=1+n_column
			}
			return indexS==column_number
		})

		var line_pos_1=current_seg_col_start*nuzic_block-nuzic_block_half/2
		var line_pos_2=current_seg_col_start*nuzic_block+current_seg_col_n*nuzic_block/2
		var line_spe_2=current_seg_col_n*nuzic_block
		var line_pos_3=current_seg_col_start*nuzic_block+current_seg_col_n*nuzic_block+nuzic_block_half/2
		var svg_width = RE_global_time_all_changes.length * 2 * nuzic_block + segment_list.length * nuzic_block * 2 + nuzic_block
		var div=`<svg id="App_RE_column_selector_svg" style="min-width: ${svg_width}px;">
					<line x1="${line_pos_1}" y1="0" x2="${line_pos_1}" y2="2366" stroke="var(--Nuzic_green)" stroke-width="${nuzic_block_half}"></line>
					<line x1="${line_pos_2}" y1="0" x2="${line_pos_2}" y2="2366" stroke="var(--Nuzic_green_light)" stroke-width="${line_spe_2}"></line>
					<line x1="${line_pos_3}" y1="0" x2="${line_pos_3}" y2="2366" stroke="var(--Nuzic_green)" stroke-width="${nuzic_block_half}"></line>
			</svg>`
		selection_svg.outerHTML=div

		//top box
		var line_pos_1_top=current_seg_col_start*nuzic_block-1
		var line_pos_3_top=current_seg_col_start*nuzic_block+current_seg_col_n*nuzic_block+1
		var div_top=`<svg id="App_RE_column_selector_top_svg" style="min-width: ${svg_width}px;">
					<line x1="${line_pos_1-7}" y1="22" x2="${line_pos_3+7}" y2="22" stroke="var(--Nuzic_green)" stroke-width="2"></line>
					<line x1="${line_pos_1_top}" y1="22" x2="${line_pos_3_top}" y2="22" stroke="var(--Nuzic_green_light)" stroke-width="3"></line>
					<line x1="${line_pos_1_top}" y1="0" x2="${line_pos_1_top}" y2="2366" stroke="var(--Nuzic_green)" stroke-width="3"></line>
					<line x1="${line_pos_3_top}" y1="0" x2="${line_pos_3_top}" y2="2366" stroke="var(--Nuzic_green)" stroke-width="3"></line>
			</svg>`
		selection_top_svg.outerHTML=div_top

		//voices data
		data.voice_data.forEach(voice=>{
			if(voice.blocked==true){
				var voice_RE_obj=RE_find_voice_obj_by_index(voice.voice_index)
				var segment_list = [...voice_RE_obj.querySelector(".App_RE_voice_data").querySelectorAll(".App_RE_segment")]
				segment_list[column_number].classList.add("selected")
			}
		})
	}
}

function RE_redraw_column_selection(data){
	var blocked_voice=data.voice_data.find(voice=>{return voice.blocked==true})
	var column_selector_container = document.getElementById("App_RE_column_selector_container")
	if(typeof(blocked_voice) != "undefined"){
		column_selector_container.style.display = 'flex'
		//draw segments headers

		var segment_list = blocked_voice.data.segment_data
		//svg
		var svg_width = RE_global_time_all_changes.length * 2 * nuzic_block + segment_list.length * nuzic_block * 2 +nuzic_block
		var div=`<svg id="App_RE_column_selector_svg" style="min-width: ${svg_width}px;"></svg><svg id="App_RE_column_selector_top_svg" style="min-width: ${svg_width}px;"></svg>`
		var N_NP = blocked_voice.neopulse.N
		var D_NP = blocked_voice.neopulse.D

		var Psg_=0
		var n_seg=segment_list.length
		segment_list.forEach((segment,indexS)=>{
			var Psg_segment_start = Psg_
			var Psg_segment_end = Psg_segment_start + segment.time.slice(-1)[0].P
			Psg_=Psg_segment_end

			//calculate times
			var time_segment_start= DATA_calculate_exact_time(Psg_segment_start,0,0,1,1,N_NP,D_NP)
			var time_segment_end= DATA_calculate_exact_time(Psg_segment_end,0,0,1,1,N_NP,D_NP)

			var index_col_start = RE_find_time_column_index(time_segment_start)
			var index_col_end = RE_find_time_column_index(time_segment_end)
			var n_column = (index_col_end-index_col_start)-1

			//white space
			div+='<div class="App_RE_column_selector_spacer"></div>'
			//segment div
			div+=`<div class="App_RE_column_selector_segment" style="width: calc(var(--RE_block)* ${n_column}); min-width: calc(var(--RE_block)* ${n_column});" onclick="RE_toggle_selection_segment_column(${indexS})">`
			//menu container
			div+='<div class="App_RE_column_selector_segment_menu">'
			//menu button
			//div+='<div class="App_RE_column_selector_segment_menu_button" onclick="RE_toggle_dropdown_column_selector_segment(this)"><img class="App_RE_column_selector_icon" src="./Icons/menu.svg"></div>'
			//function in APP_General window
			div+='<div class="App_RE_column_selector_segment_menu_button" ><img class="App_RE_column_selector_icon" src="./Icons/menu.svg"></div>'
			//menu content
			div+=`<div class="App_RE_dropdown_column_selector_content">
					<a onmouseenter="Hover_area_enter(this)" onmouseleave="Hover_area_exit(this)" data-value="NameColSegHover" onclick="RE_change_column_segment_name_button(this)"><img class="App_RE_icon" src="./Icons/op_menu_rename.svg"></a>
					<a onmouseenter="Hover_area_enter(this)" onmouseleave="Hover_area_exit(this)" data-value="ClearColSegHover" onclick="RE_clear_column_segment_button(this)"><img class="App_RE_icon" src="./Icons/op_menu_clear.svg"></a>
					<a onmouseenter="Hover_area_enter(this)" onmouseleave="Hover_area_exit(this)" data-value="PasteColSegHover" onclick="RE_repeat_column_segment_button(this,event)"><img class="App_RE_icon" src="./Icons/op_menu_repeat_right.svg"></a>
					<a onmouseenter="Hover_area_enter(this)" onmouseleave="Hover_area_exit(this)" data-value="CopyColSegHover" onclick="RE_copy_column_segment_button(this)"><img class="App_RE_icon" src="./Icons/op_menu_copy.svg"></a>
					<a onmouseenter="Hover_area_enter(this)" onmouseleave="Hover_area_exit(this)" data-value="PasteColSegHover" onclick="RE_paste_column_segment_button(this)"><img class="App_RE_icon" src="./Icons/op_menu_paste.svg"></a>
					<a onmouseenter="Hover_area_enter(this)" onmouseleave="Hover_area_exit(this)" data-value="OperColSegHover" onclick="RE_operations_column_segment_button(this,event)"><img class="App_RE_icon" src="./Icons/op_menu_operations.svg"></a>
				</div>`
			//close menu container
			div+='</div>'
			//segment number
			div+=`<div class="App_RE_column_selector_segment_number">${indexS}</div>`
			//segment name if
			if(n_column>5 && segment.segment_name!=""){
				div+=`<input class="App_RE_column_selector_segment_name" onchange="RE_change_column_segment_name(this)" onfocusout="RE_hide_column_segment_name(this)" maxlength="8" placeholder="" value="${segment.segment_name}"></input>`
			}else{
				div+=`<input class="App_RE_column_selector_segment_name hidden" onchange="RE_change_column_segment_name(this)" onfocusout="RE_hide_column_segment_name(this)" maxlength="8" placeholder="" value="${segment.segment_name}"></input>`
			}
			//delete segment column
			if(n_seg>1)div+=`<div class="App_RE_column_selector_segment_delete" onclick="RE_delete_column_segment_button(this)"><img class="App_RE_column_selector_icon" src="./Icons/delete.svg"></div>`

			//close segment
			div+='</div>'
		})
		//final space
		var final_seg_time = DATA_calculate_exact_time(Li,0,0,1,1,1,1)
		var final_time = RE_global_time_all_changes[RE_global_time_all_changes.length-1]
		var extra = 0
		if(final_seg_time!=final_time)extra=1
		var n = (RE_global_time_all_changes.length - 1 -RE_global_time_all_changes.indexOf(RE_global_time_segment_change.slice(-1)[0]))*2
		if(n<0) n=0
		//give 1 more bc S and T line end with a div that cover the boxes
		n = extra==1? n+1:n
		var final_space = n+1
		div+=`<div style="min-width: calc(var(--RE_block) * ${final_space} + var(--RE_scrollbar_th))"></div>`

		column_selector_container.innerHTML=div
	}else{
		column_selector_container.style.display = 'none'
	}
}

function RE_column_segment_to_segment_number(column_div){
	var container= column_div.closest("#App_RE_column_selector_container")
	if(!column_div.matches('.App_RE_column_selector_segment')){
		column_div=column_div.closest(".App_RE_column_selector_segment")
	}
	var column_list = [...container.querySelectorAll(".App_RE_column_selector_segment")]
	return column_list.indexOf(column_div)
}

function RE_change_column_segment_name_button(button){
	//focus on segment name
	var menu_div = button.closest(".App_RE_dropdown_column_selector_content")
	menu_div.classList.remove("show")

	var container = button.closest(".App_RE_column_selector_segment")
	//var segment_index= parseInt(container.querySelector(".App_RE_segment_number").innerHTML)

	var segment_name = container.querySelector(".App_RE_column_selector_segment_name")
	if(segment_name.classList.contains("hidden")){
		//if hidden
		segment_name.classList.remove("hidden")
		segment_name.classList.add("washidden")
	}

	segment_name.disabled=false
	segment_name.focus()
	segment_name.select()
}

function RE_change_column_segment_name(element){
	element.blur()
	APP_stop()
	var data = DATA_get_current_state_point(true)

	//find the segment index
	var segment_index=RE_column_segment_to_segment_number(element)

	//change name all blocked voices
	data.voice_data.forEach(item=>{
		if(item.blocked)item.data.segment_data[segment_index].segment_name = element.value
	})

	// if(element.value==""){
	// 	element.classList.add("hidden")
	// }else{
	// 	element.classList.remove("hidden")
	// }

	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function RE_hide_column_segment_name(input) {
	if(input.classList.contains("washidden")){
		input.classList.add("hidden")
		input.classList.remove("washidden")
	}
}

function RE_copy_column_segment_button(button){
	//find the segment index
	var segment_index=RE_column_segment_to_segment_number(button)
	DATA_copy_column_segment(segment_index)
}

//paste segment data
function RE_paste_column_segment_button(button){
	//find the segment index
	var segment_index=RE_column_segment_to_segment_number(button)
	DATA_paste_column_segment(segment_index)
}

function RE_delete_column_segment_button(button){
	APP_stop()
	//find the segment index
	var segment_index=RE_column_segment_to_segment_number(button)
	DATA_delete_column_segment(segment_index)
}

function RE_clear_column_segment_button(button){
	APP_stop()
	//find the segment index
	var segment_index=RE_column_segment_to_segment_number(button)
	DATA_clear_column_segment(segment_index)
}

function RE_repeat_column_segment_button(button,e){
	e.stopPropagation()
	APP_close_all_dropdown_segment_contents()
	var column_number = RE_column_segment_to_segment_number(button)
	//RE_select_segment_column(column_number)
	APP_show_operation_dialog_box({target:'column',working_space:'RE',column:column_number,type:'repetitions'})
}

function RE_operations_column_segment_button(button,e){
	e.stopPropagation()
	APP_close_all_dropdown_segment_contents()
	var column_number = RE_column_segment_to_segment_number(button)
	//RE_select_segment_column(column_number)
	APP_show_operation_dialog_box({target:'column',working_space:'RE',column:column_number,type:'operations'})
	//RE_toggle_dropdown_column_selector_segment(column_div)//no need
}







