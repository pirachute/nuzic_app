function RE_show_hide_voices_line_button(button){
	var line_button_list = RE_list_all_option_lines_buttons()

	var pressed_buttons = Object.keys(line_button_list).filter(id=>{
		var b = line_button_list[id]
		if(b.classList.contains("pressed") && id!="Tf"){
			return true
		}
	})

	//change button state
	if(button.classList.contains("pressed")){
		//remove BUT leave at least 1
		if(button.classList.contains("App_RE_line_button_T_f")){
			button.classList.remove("pressed")
		}else{
			var line_needed = 1

			if(pressed_buttons.indexOf("iAa")!=-1)line_needed++
			if(pressed_buttons.indexOf("iAm")!=-1)line_needed++
			if(pressed_buttons.indexOf("iAgm")!=-1)line_needed++

			if (pressed_buttons.length > line_needed) {
				button.classList.remove("pressed")
			}else{
				return
			}
		}
	}else{
		//add
		button.classList.add("pressed")
	}

	DATA_save_preferences_file()

	flag_DATA_updated.RE=false
	DATA_smart_redraw()
}


function RE_show_hide_iA_line_button(button){
	var buttons = [...document.querySelectorAll('.App_button_sound_iA')]
	//change button state
	if(button.classList.contains("pressed")){
		//NEVER SHOW IA LINES
		button.classList.remove("pressed")
	}else{
		//add
		//reset other buttons
		buttons.forEach(item => {
			item.classList.remove("pressed")
		})
		button.classList.add("pressed")
	}

	//DATA_insert_new_state_point(data)
	DATA_save_preferences_file()

	flag_DATA_updated.RE=false
	DATA_smart_redraw()
}

function RE_list_all_option_lines_buttons(){
	var line_selector_sound = document.querySelector(".App_RE_S_show_visualization_options_box")
	var line_selector_time = document.querySelector(".App_RE_C_show_visualization_options_box")

	//SOUND
	var Na_line_l = line_selector_sound.querySelector(".App_RE_Na_l")
	var Nm_line_l = line_selector_sound.querySelector(".App_RE_Nm_l")
	var Ngm_line_l = line_selector_sound.querySelector(".App_RE_Ngm_l")
	var Nabc_line_l = line_selector_sound.querySelector(".App_RE_Nabc_l")
	var iNa_line_l = line_selector_sound.querySelector(".App_RE_iNa_l")
	var iNm_line_l = line_selector_sound.querySelector(".App_RE_iNm_l")
	var iNgm_line_l = line_selector_sound.querySelector(".App_RE_iNgm_l")
	//iA
	var iAa_inp = line_selector_sound.querySelector(".App_RE_iAa_l")
	var iAm_inp = line_selector_sound.querySelector(".App_RE_iAm_l")
	var iAgm_inp = line_selector_sound.querySelector(".App_RE_iAgm_l")
	//TIME
	var Ta_line_l = line_selector_time.querySelector(".App_RE_Ta_l")
	var Tm_line_l = line_selector_time.querySelector(".App_RE_Tm_l")
	var Ts_line_l = line_selector_time.querySelector(".App_RE_Ts_l")
	var iT_line_l = line_selector_time.querySelector(".App_RE_iT_l")
	var Tf_line_l = line_selector_time.querySelector(".App_RE_Tf_l")
	return {
		Na:Na_line_l,
		Nm:Nm_line_l,
		Ngm:Ngm_line_l,
		Nabc:Nabc_line_l,
		iNa:iNa_line_l,
		iNm:iNm_line_l,
		iNgm:iNgm_line_l,
		iAa:iAa_inp,
		iAm:iAm_inp,
		iAgm:iAgm_inp,
		Ta:Ta_line_l,
		Tm:Tm_line_l,
		Ts:Ts_line_l,
		iT:iT_line_l,
		Tf:Tf_line_l}
}

