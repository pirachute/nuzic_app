
function APP_onlyReturn(evt,element){  //XXX
	if (evt.keyCode == 13) {
		//exit focus
		element.blur()
	}
}

function APP_onlyLetters(evt,element){  //XXX
	var ch = String.fromCharCode(event.which);
	if(!(/[a-zA-Z]/.test(ch))){
		evt.preventDefault();
	}

	if (evt.keyCode == 13) {
		//exit focus
		element.blur()
	}
}

function APP_onlyText(evt,element){  //XXX
	var ch = String.fromCharCode(event.which);
	if(!(/^[a-zA-Z0-9!@#$%\^&*)(+=._-]*$/.test(ch) || /[\s]/.test(ch))){
		evt.preventDefault();
	}

	if (evt.keyCode == 13) {
		//exit focus
		element.blur()
	}
}

function APP_onlyIntegersNo0(evt){  //XXX
	var ch = String.fromCharCode(event.which);
	if(!(/[1-9]/.test(ch))){
		evt.preventDefault();
	}
}

function APP_onlyIntegers(evt,element){
	var ch = String.fromCharCode(event.which);
	//if - must be alone or first
	var isSelected = RE_input_box_is_selected(element)
	var prev_char = RE_input_box_prev_char(element)
	if(!(/[0-9]/.test(ch) || (/[-]/.test(ch) && !element.value.includes("-") && (isSelected || prev_char=="")))){
		evt.preventDefault();
	}

	if (evt.keyCode == 13) {
		//exit focus
		element.blur()
	}
}

function APP_onlyIntegers_pos(evt,element){
	var ch = String.fromCharCode(event.which);
	if(!(/[0-9]/.test(ch))){
		evt.preventDefault();
	}

	if (evt.keyCode == 13) {
		//exit focus
		element.blur()
	}
}

function APP_operations_int_mod(evt,element){
	//verify inp box is an integer or a modular
	var ch = String.fromCharCode(event.which);
	if(!(/[0-9]/.test(ch) || /[r]/.test(ch) || /[-]/.test(ch))){
		evt.preventDefault();
	}

	var isSelected = RE_input_box_is_selected(element)
	var isEmpty = !!(element.value=="")
	var prev_char = RE_input_box_prev_char(element)
	var prev_str = RE_input_box_prev_str(element)

	let stringOk = false

	//if - must be alone or first
	if(/[-]/.test(ch) && !element.value.includes("-") && (isSelected || prev_char=="")){
		stringOk = true
	}

	//if r must be after a number of lesser than TET (positive or negative)
	if((/[r]/.test(ch) 	&& !(isSelected || isEmpty)
						&& !element.value.includes("r") && parseInt(prev_str)<TET && parseInt(prev_str)>-TET
						&& prev_char!="")
		){
		stringOk = true
	}

	//if number is
	// first or second value minor of max note number OR
	var max_note = TET*7 -1
	// numb
	if( /[0-9]/.test(ch) && (	(isSelected || isEmpty) ||
								!element.value.includes("r") ||
								element.value=="-"
							)
		){
		//control if < TET
		let string_value = parseInt(prev_str+ch+RE_input_box_next_str(element))
		if((string_value<max_note && string_value>-max_note) || isSelected){
			stringOk = true
		}
	}

	//number after a r
	if( /[0-9]/.test(ch) && (element.value.slice(-1)=="r") && ch<7){
		stringOk = true
	}

	if (evt.keyCode == 13) {
		//exit focus
		element.blur()
	}

	if(!stringOk){
		evt.preventDefault()
	}
}

function APP_operations_int_mod_pos(evt,element){
	//verify inp box is positive integer or positive modular
	var ch = String.fromCharCode(event.which);
	if(!(/[0-9]/.test(ch) || /[r]/.test(ch))){
		evt.preventDefault();
	}

	var isSelected = RE_input_box_is_selected(element)
	var isEmpty = !!(element.value=="")
	var prev_char = RE_input_box_prev_char(element)
	var prev_str = RE_input_box_prev_str(element)

	let stringOk = false

	//if r must be after a number of 1 or 2 char
	if((/[r]/.test(ch) 	&& !(isSelected || isEmpty)
						&& !element.value.includes("r") && parseInt(prev_str)<TET
						&& prev_char!="")
		){
		stringOk = true
	}

	//if number is
	var max_note = TET*7 -1
	//if number is

	// first or second value of another number OR
	if( /[0-9]/.test(ch) && (	(isSelected || isEmpty) ||
								!isNaN(prev_str)
							)
		){
		//control if < TET
		let string = parseInt(prev_str+ch)
		if(string<TET && string>-TET){
		//if((string<TET && string>-TET) || isSelected){
			stringOk = true
		}
	}

	// first or second value minor of max note number OR
	if( /[0-9]/.test(ch) && (	(isSelected || isEmpty) ||
								//Obsolete : later control of TET magnitude
								//(!isNaN(element.value) && element.value.length<2) ||
								!isNaN(element.value)
							)
		){
		//control if < TET
		let string_value = parseInt(element.value+ch)
		if((string_value<max_note) || isSelected){
			stringOk = true
		}
	}

	//number after a r
	if( /[0-9]/.test(ch) && (prev_char=="r" && element.value.slice(-1)=="r") && ch<7){
		stringOk = true
	}

	if (evt.keyCode == 13) {
		//exit focus
		element.blur()
	}

	if(!stringOk){
		evt.preventDefault()
	}
}

function APP_onlyIntegers_enter_fraction(evt,element){  //XXX
	var ch = String.fromCharCode(event.which);
	if(!((/[0-9]/.test(ch)) || (/[/]/.test(ch)))){
		evt.preventDefault();
	}
}


function APP_onlyIntegers_enter_NP(evt,element){
	var ch = String.fromCharCode(event.which);
	if(!((/[0-9]/.test(ch)) || (/[/]/.test(ch)))){
		evt.preventDefault();
	}

	if (evt.keyCode == 13) {
		//exit focus
		element.blur()
	}
}


//Sound line absolute
function APP_onlyIntegers_Na_line(evt,element){
	var ch = String.fromCharCode(event.which)
	if(!((/[0-9]/.test(ch)) || (/[s]/.test(ch)) || (/[l]/.test(ch)) || (/[e]/.test(ch)))){
		evt.preventDefault();
	}

	var isSelected = RE_input_box_is_selected(element)
	var isEmpty = !!(element.value=="")

	//if s must be alone
	let stringOk = false
	if((/[s]/.test(ch)) && (isSelected || isEmpty)){
		//console.log("rad")
		stringOk = true
	}

	//if e must be alone
	if((/[e]/.test(ch)) && (isSelected || isEmpty)){
		stringOk = true
	}
	//if l must be alone
	if((/[l]/.test(ch)) && (isSelected || isEmpty)){
		stringOk = true
	}

	//if number
	if( /[0-9]/.test(ch) && (
								(isSelected || isEmpty) ||
								!isNaN(element.value)
							)
		){
		//control if < TET*7-1
		let string = parseInt(RE_input_box_prev_str(element)+ch+RE_input_box_next_str(element))//attenction when defined fractioning
		if(isSelected || string<TET*7){
			stringOk = true
		}
	}

	if(!stringOk){
		evt.preventDefault()
	}
}

function APP_onlyIntegers_Nm_line(evt,element){
	//element.value=previous_value
	var ch = String.fromCharCode(event.which)
	if(!((/[0-9]/.test(ch)) || (/[s]/.test(ch)) || (/[r]/.test(ch)) || (/[l]/.test(ch)) || (/[e]/.test(ch)))){
		evt.preventDefault();
	}

	var isSelected = RE_input_box_is_selected(element)
	var isEmpty = !!(element.value=="")

	//if s must be alone
	let stringOk = false
	if((/[s]/.test(ch) && (isSelected || isEmpty))){
		//console.log("rad")
		stringOk = true
	}
	//if e must be alone
	if((/[e]/.test(ch) && (isSelected || isEmpty))){
		stringOk = true
	}
	//if l must be alone
	if((/[l]/.test(ch) && (isSelected || isEmpty))){
		stringOk = true
	}

	//if r must be after a number of 1 or 2 char
	var prev_str = RE_input_box_prev_str(element)
	var prev_char = RE_input_box_prev_char(element)
	if((/[r]/.test(ch) 	&& !(isSelected || isEmpty)
						&& !element.value.includes("r") && parseInt(prev_str)<TET
						&& prev_char!="")
		){
		stringOk = true
	}

	//if number is

	// first or second value of another number OR
	if( /[0-9]/.test(ch) && (	(isSelected || isEmpty) ||
								!isNaN(prev_str)
							)
		){
		//control if < TET
		let string = parseInt(prev_str+ch)
		if(string<TET && string>-TET){
		//if((string<TET && string>-TET) || isSelected){
			stringOk = true
		}
	}
	//number after a r
	var prev_char = RE_input_box_prev_char(element)
	if( /[0-9]/.test(ch) && (prev_char=="r" && element.value.slice(-1)=="r") && ch<7){
		stringOk = true
	}

	if(!stringOk){
		evt.preventDefault()
	}
}

function APP_onlyIntegers_Ngm_line(evt,element){
	//max 7 char   11+11r0
	var ch = String.fromCharCode(event.which)

	var segment_obj = element.closest(".App_RE_segment")
	var voice_obj = element.closest(".App_RE_voice")
	var voice_index = RE_read_voice_id(voice_obj)[1]
	var segment_list= [...voice_obj.querySelectorAll(".App_RE_segment")]
	var segment_index = segment_list.indexOf(segment_obj)
	var [elementIndex,input_list]=RE_element_index(element,".App_RE_inp_box")
	var position = 0
	input_list.find((inp,index)=>{
	if (index==elementIndex)return true
		if(inp.value!="")position++
	})

	var iT=0
	var scale_position=position
	if(previous_value==""){
		iT=1
		scale_position--
	}
	var current_scale = DATA_get_object_index_info(voice_index,segment_index,scale_position,iT,false,false,true).scale
	if(current_scale==null){
		evt.preventDefault()
		return
	}
	//find previous note
	var previous_element_position = DATA_get_object_prev_note_index(voice_index,segment_index,position)
	var previous_scale = null
	//var previous_Na=0
	if(previous_element_position!= null){
		var prev_element_info = DATA_get_object_index_info(voice_index,segment_index,previous_element_position,0,false,false,true)
		//var [,,c]=BNAV_absolute_note_to_scale(prev_element_info.note,true,prev_element_info.scale)
		//reg= c
		//previous_Na = prev_element_info.note
		previous_scale = prev_element_info.scale
	}
	var is_special= false
	if(previous_scale!=null){
		if(previous_scale.acronym!=current_scale.acronym || previous_scale.rotation!=current_scale.rotation || previous_scale.starting_note!=current_scale.starting_note){
			// different scales
			is_special=true
		}
	}else{
		//first element in segment
		is_special=true
	}

	if(is_special){
		//first possible note???
		if(!element.classList.contains("App_RE_note_bordered_cell")){
			element.classList.add("App_RE_note_bordered_cell")
			element.addEventListener("focusout",()=>{
				element.classList.remove("App_RE_note_bordered_cell")
			})
		}
	}


	if(!((/[0-9]/.test(ch)) || (/[r]/.test(ch)) || (/[+]/.test(ch)) || (/[-]/.test(ch)) || (/[s]/.test(ch)) || (/[l]/.test(ch)) || (/[e]/.test(ch)))){
		evt.preventDefault();
	}

	var isSelected = RE_input_box_is_selected(element)
	var isEmpty = !!(element.value=="")

	//if s must be alone
	let stringOk = false
	if((/[s]/.test(ch) && (isSelected || isEmpty))){
		//console.log("rad")
		stringOk = true
	}
	//if e must be alone
	if((/[e]/.test(ch) && (isSelected || isEmpty))){
		stringOk = true
	}
	//if l must be alone
	if((/[l]/.test(ch) && (isSelected || isEmpty))){
		stringOk = true
	}

	var caret_position = evt.target.selectionStart
	var before = element.value.slice(0,caret_position)
	caret_position = evt.target.selectionEnd
	var after = element.value.slice(caret_position,element.value.length)

	//if + or - must be after something that start with a number
	if((/[+]/.test(ch) || /[-]/.test(ch))	&& !(isSelected || isEmpty)){
		//must not exist a + or - or r before position
		if(!isNaN(element.value[0]) && !before.includes("r")){
			if(!before.includes("-") && !after.includes("-") && !before.includes("+") && !after.includes("+")){
				stringOk = true
			}
		}
	}

	//if - can be after a r
	if(/[-]/.test(ch)	&& !(isSelected || isEmpty)){
		//must not exist a + or - or r before position
		if(before[before.length-1]=="r"){
			stringOk = true
		}
	}

	//if r must be after something that start with a number
	if(/[r]/.test(ch) 	&& !(isSelected || isEmpty)){
		//MUST exist at least a number no + or + after and no other r
		if(!isNaN(element.value[0]) && !after.includes("-") && !after.includes("+") && !before.includes("r") && !after.includes("r")){
			stringOk = true
		}
	}

	//if number is
	// first or second value of another number OR
	// number at start (2 char)

	if(/[0-9]/.test(ch)){
		if(isSelected || element.value==""){

		}
		if(after==""){
			if(before.length<=1){
				stringOk = true
			}else{
				if(before.includes("+")){
					var split = before.split('+')
					if(split[1].length<2){
						stringOk = true
					}
				}
				if(before.includes("-")){
					var split = before.split('-')
					if(split[1].length<2){
						stringOk = true
					}
				}
			}
		}else{
			//max 2 int before a + or -
			if(after.includes("+")){
				var split = after.split('+')
				if(split[0].length+before.length<2){
					stringOk = true
				}
			}
			if(after.includes("-")){
				var split = after.split('-')
				if(split[0].length+before.length<2){
					stringOk = true
				}
			}

			//max 2 int before a r
			if(after.includes("r") && !after.includes("+") && !after.includes("-")){
				var split = after.split('r')
				var b = split[0]
				var a=""
				if(before.includes("+")){
					split = before.split('+')
					a=split[1]
				}else if (before.includes("+")) {
				 	split = before.split('-')
					a=split[1]
				}else{
				 	a=before
				}

				if(b.length+a.length<2){
					console.log('?=?')
					stringOk = true
				}
			}
		}
	}
	//number after a r
	if( /[0-7]/.test(ch) && before.includes("r") && before[before.length-1]=="r" && after.length==0){
		stringOk = true
	}

	if(!stringOk){
		evt.preventDefault()
	}
}

function APP_onlyIntegers_Nabc_line(evt,element){
	evt.preventDefault()
}

function APP_onlyIntegers_iNa_line(evt,element){
	var ch = String.fromCharCode(event.which)

	var segment_obj = element.closest(".App_RE_segment")
	var voice_obj = element.closest(".App_RE_voice")
	var voice_index = RE_read_voice_id(voice_obj)[1]
	var segment_list= [...voice_obj.querySelectorAll(".App_RE_segment")]
	var segment_index = segment_list.indexOf(segment_obj)
	var [elementIndex,input_list]=RE_element_index(element,".App_RE_inp_box")
	var position = 0
	input_list.find((inp,index)=>{
	if (index==elementIndex)return true
		if(inp.value!="")position++
	})

	//find previous note
	var previous_element_position = DATA_get_object_prev_note_index(voice_index,segment_index,position)

	if(previous_element_position== null){
		//first possible note???
		if(!element.classList.contains("App_RE_note_bordered_cell")){
			element.classList.add("App_RE_note_bordered_cell")
			element.addEventListener("focusout",()=>{
				element.classList.remove("App_RE_note_bordered_cell")
			})
		}
	}

	if(!((/[0-9]/.test(ch)) || (/[s]/.test(ch)) || (/[l]/.test(ch)) || (/[-]/.test(ch)) || (/[e]/.test(ch)))){
		evt.preventDefault();
	}

	var isSelected = RE_input_box_is_selected(element)
	var isEmpty = !!(element.value=="")
	var prev_char = RE_input_box_prev_char(element)

	let stringOk = false
	//if - must be alone or first
	if(/[-]/.test(ch) && !element.value.includes("-") && (isSelected || prev_char=="")){
		stringOk = true
	}

	//if s must be alone
	if(/[s]/.test(ch) && (isSelected || isEmpty)){
		//console.log("rad")
		stringOk = true
	}
	//if e must be alone
	if(/[e]/.test(ch) && (isSelected || isEmpty)){
		stringOk = true
	}
	//if l must be alone
	if(/[l]/.test(ch) && (isSelected || isEmpty)){
		stringOk = true
	}

	if( /[0-9]/.test(ch)){
		let string = parseInt(RE_input_box_prev_str(element)+ch+RE_input_box_next_str(element))
		if(string<TET*7 && string>-TET*7){
			stringOk = true
		}
	}

	if(!stringOk){
		evt.preventDefault()
	}
}

//fractioning ????
function APP_onlyIntegers_iNm_line(evt,element){
	var ch = String.fromCharCode(event.which)

	var segment_obj = element.closest(".App_RE_segment")
	var voice_obj = element.closest(".App_RE_voice")
	var voice_index = RE_read_voice_id(voice_obj)[1]
	var segment_list= [...voice_obj.querySelectorAll(".App_RE_segment")]
	var segment_index = segment_list.indexOf(segment_obj)
	var [elementIndex,input_list]=RE_element_index(element,".App_RE_inp_box")
	var position = 0
	input_list.find((inp,index)=>{
	if (index==elementIndex)return true
		if(inp.value!="")position++
	})

	//find previous note
	var previous_element_position = DATA_get_object_prev_note_index(voice_index,segment_index,position)

	if(previous_element_position== null){
		//first possible note???
		if(!element.classList.contains("App_RE_note_bordered_cell")){
			element.classList.add("App_RE_note_bordered_cell")
			element.addEventListener("focusout",()=>{
				element.classList.remove("App_RE_note_bordered_cell")
			})
		}
	}

	if(!((/[0-9]/.test(ch)) || (/[-]/.test(ch)) || (/[s]/.test(ch)) || (/[r]/.test(ch)) || (/[l]/.test(ch)) || (/[e]/.test(ch)))){
		evt.preventDefault();
	}

	var isSelected = RE_input_box_is_selected(element)
	var isEmpty = !!(element.value=="")
	var prev_char = RE_input_box_prev_char(element)
	let stringOk = false
	//if - must be alone or first
	if(/[-]/.test(ch) && !element.value.includes("-") && (isSelected || prev_char=="")){
		stringOk = true
	}
	//if s must be alone
	if(/[s]/.test(ch) && (isSelected || isEmpty)){
		//console.log("rad")
		stringOk = true
	}
	//if e must be alone
	if(/[e]/.test(ch) && (isSelected || isEmpty)){
		stringOk = true
	}
	//if l must be alone
	if(/[l]/.test(ch) && (isSelected || isEmpty)){
		stringOk = true
	}
	//if r must be after a number of 1 or 2 char
	if(/[r]/.test(ch) && !(isSelected || isEmpty)
					&& !element.value.includes("r")
					&& prev_char!=""
		){
		stringOk = true
	}
	//if number is
	var prev_str = RE_input_box_prev_str(element)
	// first or second value of another number
	if( /[0-9]/.test(ch) && (	(isSelected || isEmpty) ||
								!isNaN(prev_str) ||
								(prev_char=="-")
							)
		){
		//control if < TET
		let string = parseInt(prev_str+ch)
		if(string<TET && string>-TET){
			stringOk = true
		}
	}
	//number after a r
	if( /[0-9]/.test(ch) && (prev_char=="r" && element.value.slice(-1)=="r") && ch<7){
		stringOk = true
	}

	if(!stringOk){
		evt.preventDefault()
	}
}

function APP_onlyIntegers_iNgm_line(evt,element){
	var ch = String.fromCharCode(event.which);

	var isSelected = RE_input_box_is_selected(element)
	var isEmpty = !!(element.value=="")
	var prev_char = RE_input_box_prev_char(element)
	//var last_char = element.value[element.value.length-1]

	//verify if is a new scale or not
	//using database in order to find prev element note/scale/etc...
	var segment_obj = element.closest(".App_RE_segment")
	var voice_obj = element.closest(".App_RE_voice")
	var voice_index = RE_read_voice_id(voice_obj)[1]
	var segment_list= [...voice_obj.querySelectorAll(".App_RE_segment")]
	var segment_index = segment_list.indexOf(segment_obj)
	var [elementIndex,input_list]=RE_element_index(element,".App_RE_inp_box")
	var position = 0
	input_list.find((inp,index)=>{
	if (index==elementIndex)return true
		if(inp.value!="")position++
	})

	var iT=0
	var scale_position=position
	if(previous_value==""){
		iT=1
		scale_position--
	}
	var current_scale = DATA_get_object_index_info(voice_index,segment_index,scale_position,iT,false,false,true).scale
	if(current_scale==null){
		evt.preventDefault()
		return
	}
	//find previous note
	var previous_element_position = DATA_get_object_prev_note_index(voice_index,segment_index,position)
	var previous_scale = null
	//var previous_Na=0
	if(previous_element_position!= null){
		var prev_element_info = DATA_get_object_index_info(voice_index,segment_index,previous_element_position,0,false,false,true)
		//var [,,c]=BNAV_absolute_note_to_scale(prev_element_info.note,true,prev_element_info.scale)
		//reg= c
		//previous_Na = prev_element_info.note
		previous_scale = prev_element_info.scale
	}
	var write_iN = true
	if(previous_scale!=null){
		if(previous_scale.acronym!=current_scale.acronym || previous_scale.rotation!=current_scale.rotation || previous_scale.starting_note!=current_scale.starting_note){
			// different scales
			write_iN=false
		}
	}else{
		//first element in segment
		write_iN=false
	}

	if(write_iN){
		//calculate note interval and make a traduction in current scale //PREV OR CURRENT SCALE??? XXX XXX
		//input_list[column_number].value = BNAV_calculate_scale_interval_string(prev_note,prev_diesis,current_scale,current_note,current_diesis,current_scale)
		if(!((/[0-9]/.test(ch)) || (/[-]/.test(ch)) || (/[s]/.test(ch)) || (/[l]/.test(ch)) || (/[e]/.test(ch)))){
			evt.preventDefault()
		}

		let stringOk = false

		//if - must be alone or first and only one
		//if(/[-]/.test(ch) && (isSelected || isEmpty || prev_char=="")){
		if(/[-]/.test(ch) && !element.value.includes("-") && (isSelected || prev_char=="")){
			stringOk = true
		}

		//if s must be alone
		if((/[s]/.test(ch) && (isSelected || isEmpty))){
			//console.log("rad")
			stringOk = true
		}

		//if e must be alone
		if((/[e]/.test(ch) && (isSelected || isEmpty))){
			stringOk = true
		}

		//if l must be alone
		if((/[l]/.test(ch) && (isSelected || isEmpty))){
			stringOk = true
		}


		// first or second value of another number
		if( /[0-9]/.test(ch) && (	isSelected || isEmpty ||
									(element.value.includes("-") && prev_char!="") || !element.value.includes("-")
								)
			){

			//if number is max X values depending of TET

			let string = parseInt(RE_input_box_prev_str(element)+ch+RE_input_box_next_str(element))
			if(string<TET*7 && string>-TET*7){
				stringOk = true
			}
			//stringOk = true
		}

		if(!stringOk){
			evt.preventDefault()
		}

	}else{
		//write note in current scale

		//add bordered cell and thake it out after
		//NOT NEEDED ; RE-DONE BY APP_onlyIntegers_Ngm_line
		// if(!element.classList.contains("App_RE_note_bordered_cell")){
		// 	element.classList.add("App_RE_note_bordered_cell")
		// 	element.addEventListener("focusout",()=>{
		// 		element.classList.remove("App_RE_note_bordered_cell")
		// 	})
		// }

		//apply Ngm roules
		APP_onlyIntegers_Ngm_line(evt,element)
	}
}

//iA lines
function APP_onlyIntegers_iAa(evt,element){//XXX
	var ch = String.fromCharCode(event.which)
	if(!((/[0-9]/.test(ch)) || (/[s]/.test(ch)) || (/[l]/.test(ch)) || (/[-]/.test(ch)) || (/[e]/.test(ch)))){
		evt.preventDefault();
	}

	var isSelected = RE_input_box_is_selected(element)
	var isEmpty = !!(element.value=="")

	//if - must be alone
	let stringOk = false
	if(/[-]/.test(ch) && (isSelected || isEmpty)){
		//console.log("rad")
		stringOk = true
	}
	//if s must be alone
	if(/[s]/.test(ch) && (isSelected || isEmpty)){
		//console.log("rad")
		stringOk = true
	}
	//if e must be alone
	if(/[e]/.test(ch) && (isSelected || isEmpty)){
		stringOk = true
	}
	//if l must be alone
	if(/[l]/.test(ch) && (isSelected || isEmpty)){
		stringOk = true
	}

	//if number, only numbers or minus sign
	//length depends on TET >= 15 == 4 char (-100)
	let max_length = 3
	if(TET>=15)max_length++
	let prev_value= 10*(max_length-2)
	if( /[0-9]/.test(ch) && (
		(isSelected || element.value=="") ||
		(!isNaN(element.value) && element.value<prev_value && element.value>-prev_value) ||
		(element.value=="-")
		)
	){
		stringOk = true
	}

	if(!stringOk){
		evt.preventDefault()
	}
}

function APP_onlyIntegers_iAm(evt,element){//XXX
	var ch = String.fromCharCode(event.which);
	if(!((/[0-9]/.test(ch)) || (/[-]/.test(ch)) || (/[s]/.test(ch)) || (/[r]/.test(ch)) || (/[l]/.test(ch)) || (/[e]/.test(ch)))){
		evt.preventDefault();
	}

	var isSelected = RE_input_box_is_selected(element)
	var isEmpty = !!(element.value=="")

	//if - must be alone
	let stringOk = false
	if(/[-]/.test(ch) && (isSelected || isEmpty)){
		//console.log("rad")
		stringOk = true
	}
	//if s must be alone
	if((/[s]/.test(ch) && (isSelected || isEmpty))){
		//console.log("rad")
		stringOk = true
	}
	//if e must be alone
	if((/[e]/.test(ch) && (isSelected || isEmpty))){
		stringOk = true
	}
	//if l must be alone
	if((/[l]/.test(ch) && (isSelected || isEmpty))){
		stringOk = true
	}
	//if r must be after a number of 1 or 2 char
	if((/[r]/.test(ch) && !(
								isSelected || isEmpty)
								&& !isNaN(element.value)
								&& element.value<100
								&& element.value>-100
							)
		){
		stringOk = true
	}

	//if number is
	// first or second value of another number
	if( /[0-9]/.test(ch) && (	(isSelected || isEmpty) ||
								//Obsolete : later control of TET magnitude
								//(!isNaN(element.value) && element.value<10 && element.value>-10) ||
								!isNaN(element.value) ||
								//(element.value.slice(-1)=="r") ||
								(element.value=="-")
							)
		){
		//control if < TET
		let string = parseInt(element.value+ch)//XXX prev + ch + next
		if((string<TET && string>-TET) || isSelected){
			stringOk = true
		}
	}

	//number after a r
	if( /[0-9]/.test(ch) && (element.value.slice(-1)=="r") && ch<7){
		stringOk = true
	}

	if(!stringOk){
		evt.preventDefault()
	}
}

function APP_onlyIntegers_iAgm(evt,element){
	var ch = String.fromCharCode(event.which);
	var isSelected = RE_input_box_is_selected(element)
	var isEmpty = !!(element.value=="")
	var prev_char = RE_input_box_prev_char(element)
	//NOT THIS APP_onlyIntegers_iNgm_line(evt,element)
	if(!((/[0-9]/.test(ch)) || (/[-]/.test(ch)) || (/[s]/.test(ch)) || (/[l]/.test(ch)) || (/[e]/.test(ch)))){
		evt.preventDefault()
	}

	let stringOk = false

	//if - must be alone or first and only one
	//if(/[-]/.test(ch) && (isSelected || isEmpty || prev_char=="")){
	if(/[-]/.test(ch) && !element.value.includes("-") && (isSelected || prev_char=="")){
		stringOk = true
	}

	//if s must be alone
	if((/[s]/.test(ch) && (isSelected || isEmpty))){
		//console.log("rad")
		stringOk = true
	}

	//if e must be alone
	if((/[e]/.test(ch) && (isSelected || isEmpty))){
		stringOk = true
	}

	//if l must be alone
	if((/[l]/.test(ch) && (isSelected || isEmpty))){
		stringOk = true
	}

	//if number is max 3 values
	// first or second value of another number
	if( /[0-9]/.test(ch) && (	isSelected || isEmpty ||
								(element.value.includes("-") && prev_char!="")
							)
		){
		stringOk = true
	}

	if(!stringOk){
		evt.preventDefault()
	}
}

function APP_onlyIntegers_denied(evt,element){
	//not editable
	evt.preventDefault()
}

//Time line
function APP_onlyIntegers_Ta_line(evt,element){
	APP_onlyIntegers_denied(evt,element)
	//XXX not yet editable
}


//segment line
function APP_onlyIntegers_Ts_line(evt,element){
	var ch = String.fromCharCode(event.which)
	var isRange
	if(previous_value==""){
		isRange = false
	}else{
		isRange = RE_element_is_fraction_range(element)
	}

	if(isRange){
		if(!(/[0-9]/.test(ch))){
			evt.preventDefault()
		}
	}else {
		if(!((/[0-9]/.test(ch)) || (/[.]/.test(ch)))){
			evt.preventDefault()
		}
	}

	var isSelected = RE_input_box_is_selected(element)

	//must be only ONE "."
	if((/[.]/.test(ch) && (!isSelected && element.value.includes(".")))){
		evt.preventDefault()
	}
}

//modular line (compas)
function APP_onlyIntegers_Tm_line(evt,element){
	var ch = String.fromCharCode(event.which)
	var isRange
	if(previous_value==""){
		isRange = false
	}else{
		isRange = RE_element_is_fraction_range(element)
	}
	var isSelected = RE_input_box_is_selected(element)
	var isEmpty = !!(element.value=="")

	if(isRange){
		if(!((/[0-9]/.test(ch)) || (/[c]/.test(ch)) )){
			evt.preventDefault();
		}
	}else {
		if(!((/[0-9]/.test(ch)) || (/[.]/.test(ch)) || (/[c]/.test(ch)) )){
			evt.preventDefault();
		}
	}

	let stringOk = false
	//must be only ONE "."
	if(/[.]/.test(ch) && (isSelected || (!element.value.includes(".") && !element.value.includes("c")))){
		stringOk = true
	}

	//must be only ONE "c"
	if(/[c]/.test(ch) && (isSelected || !element.value.includes("c"))){
		stringOk = true
	}

	// first value of another number
	if( /[0-9]/.test(ch) && (	(isSelected || isEmpty) ||
								(
									!isNaN(element.value) &&
									!element.value.includes(".")
								)
							)
		){
		//control if < 16 (max compas number)
		let string = parseInt(element.value+ch)
		if((string<16) || isSelected){
			stringOk = true
		}
	}

	//number after a "."
	if( /[0-9]/.test(ch) && (element.value.slice(-1)==".") && !isSelected){
		stringOk = true
	}

	//number after a c
	if( /[0-9]/.test(ch) && element.value.includes("c")){
		stringOk = true
	}

	if(!stringOk){
		evt.preventDefault()
	}
}

// function APP_onlyIntegers_Tga_line(evt,element){
	//code here XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXx
// 	element.value=previous_value
// }

// function APP_onlyIntegers_Tgm_line(evt,element){
	//code here XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXx
// 	element.value=previous_value
// }

//intervals
function APP_onlyIntegers_iT_line(evt,element){
	var ch = String.fromCharCode(event.which);
	if(!((/[0-9]/.test(ch)))){
		evt.preventDefault();
	}
}


function APP_onlyIntegers_Cvalue(evt,element){
	var ch = String.fromCharCode(event.which);
	if(!(/[0-9]/.test(ch))){
		evt.preventDefault();
	}

	if (evt.keyCode == 13) {
		//exit focus
		element.blur()
	}
}

function APP_onlyIntegers_iTKb(evt, element){
	var char = String.fromCharCode(event.which);
	var regex = /^([0-9]|[1-9][0-9])$/;
	console.log(regex.test(char))
	if(!regex.test(char)){
		evt.preventDefault()
	}
}

function RE_input_box_is_selected(input){
	//true if all text of inp bos is selected
	//if value=="" true ( equal to all selected), starting typing equals to trart a new string
	if (typeof input.selectionStart == "number") {
		return input.selectionStart == 0 && input.selectionEnd == input.value.length;
	}// else if (typeof document.selection != "undefined") {
	//	input.focus();
	//	return document.selection.createRange().text == input.value;
	//}
}

function RE_input_box_prev_char(input){
	if (typeof input.selectionStart == "number") {
		//return input.selectionStart
		if(input.selectionStart==0)return ""
		return input.value[input.selectionStart-1]
	}
}

function RE_input_box_prev_str(input){
	if (typeof input.selectionStart == "number") {
		//return input.selectionStart
		if(input.selectionStart==0)return ""
		return input.value.slice(0,input.selectionStart)
	}
}

function RE_input_box_next_str(input){
	if (typeof input.selectionEnd == "number") {
		//return input.selectionStart
		if(input.selectionEnd==input.value.length)return ""
		return input.value.slice(input.selectionEnd,input.value.length)
	}
}
