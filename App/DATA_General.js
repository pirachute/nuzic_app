//data manipulation functions
function DATA_change_PPM(value){
	var data = DATA_get_current_state_point(true)

	var provv=DATA_calculate_inRange(value,30,240)[0]
	data.global_variables.PPM=provv
	PPM=provv
	data.voice_data.forEach(voice=>{
		voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
	})

	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_calculate_inRange(x, value1, value2) {
	//ritorna vero o falso se fuori dal limite
	//return ((x-value1)*(x-value2) <= 0);
	//ritorna un valore entro i limiti
	return x < value1 ? [value1,false] : (x > value2 ? [value2,false] : [x,true]);
}

function DATA_calculate_minimum_Li(){
	var data = DATA_get_current_state_point(false)

	var neopulse_list = data.voice_data.map(voice=>{
		return voice.neopulse
	})

	var mCM = neopulse_list.reduce((prop,item)=>{

		return Math.abs((prop * item.N) / gcd_two_numbers(prop, item.N))
	},1)
	return mCM

	function gcd_two_numbers(x, y) {
		x = Math.abs(x)
		y = Math.abs(y)
		while(y) {
			var t = y
			y = x % y
			x = t
		}
		return x
	}
}

function DATA_force_Li(new_Li_absolute){
	var current_progress_bar_values=APP_return_progress_bar_values()
	var end_loop_at_Li = false
	if(current_progress_bar_values[2]==Li)end_loop_at_Li=true

	var data = DATA_get_current_state_point(true)
	//verify if last segment blocked voices can be extended
	var extend_blocked=DATA_verify_blocked_voices_extend_last_segment(data)
	data.voice_data.forEach(voice=>{
		DATA_calculate_force_voice_Li(voice,new_Li_absolute,extend_blocked)
	})

	data.global_variables.Li=new_Li_absolute

	//actualize progress bar
	APP_redraw_Progress_bar_limits(false,end_loop_at_Li)
	//console.log(data.voice_data[1].data)
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

//VOICES
/*/OBSOLETE - use EDITION REPETITIONS
function DATA_clone_voice(voice_index){
	var data = DATA_get_current_state_point(true)

	//remove solo
	data.voice_data.forEach(voice=>{
		voice.solo=false
	})
	//duplicate current voice
	//console.log(data)
	var voice_position = -1
	var voice_data = JSON.parse(JSON.stringify(data.voice_data.find(item=>{
		voice_position++
		return item.voice_index==voice_index
	})))
	
	//console.log(voice_data)
	if(voice_data==null)return
	//console.log(voice_data)
	//var max_index = data.voice_data.forEach

	var voice_index_list = data.voice_data.map(item=>{
		var number = Number(item.voice_index)
		return number
	})

	var new_voice_index = Math.max(...voice_index_list)+1
	//var new_voice_index = max_voice_index+1

	//console.log(voice_position)
	voice_data.solo= false
	voice_data.selected=false
	voice_data.voice_index = new_voice_index
	var string = voice_data.name+"#"

	voice_data.name= string.slice(0,8)
	data.voice_data.splice(voice_position,0,voice_data)
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}*/

function DATA_add_voice(voice_index){
	var data = DATA_get_current_state_point(true)
	//remove solo
	data.voice_data.forEach(voice=>{
		voice.solo=false
	})
	
	var voice_position = -1
	var voice_data = JSON.parse(JSON.stringify(data.voice_data.find(item=>{
		voice_position++
		return item.voice_index==voice_index
	})))

	if(voice_data==null)return

	voice_data.data.segment_data.forEach(segment=>{
		var frac = segment.fraction.pop()
		frac.N=1
		frac.D=1
		frac.start=0
		segment.fraction=[]
		segment.fraction[0]=frac
		//reset note
		segment.note=[]
		segment.note[0]=-1
		segment.note[1]=-4
		//reset diesis
		segment.diesis=[]
		segment.diesis[0]=true
		segment.diesis[1]=true
		//reset time
		segment.time=[]
		segment.time[0]={"P":0,"F":0}
		segment.time[1]={"P":frac.stop,"F":0}
	})

	var voice_index_list = data.voice_data.map(item=>{
		var number = Number(item.voice_index)
		return number
	})

	var new_voice_index = Math.max(...voice_index_list)+1
	voice_data.solo= false
	voice_data.selected=false
	voice_data.voice_index = new_voice_index

	var string = `Voz ${new_voice_index+1}`
	voice_data.name= string.slice(0,8)

	voice_data.data.midi_data=DATA_segment_data_to_midi_data(voice_data.data.segment_data,voice_data.neopulse)
	data.voice_data.splice(voice_position,0,voice_data)
	
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_clear_voice(voice_index){
	var data = DATA_get_current_state_point(true)
	//duplicate current voice

	var voice_data = data.voice_data.find(item=>{
		return item.voice_index==voice_index
	})

	voice_data.data.segment_data.forEach(segment=>{
		var frac = segment.fraction.pop()
		frac.N=1
		frac.D=1
		frac.start=0
		segment.fraction=[]
		segment.fraction[0]=frac
		//reset note
		segment.note=[]
		segment.note[0]=-1
		segment.note[1]=-4
		//reset diesis
		segment.diesis=[]
		segment.diesis[0]=true
		segment.diesis[1]=true
		//reset time
		segment.time=[]
		segment.time[0]={"P":0,"F":0}
		segment.time[1]={"P":frac.stop,"F":0}
	})

	//data.midi_data
	voice_data.data.midi_data=DATA_segment_data_to_midi_data(voice_data.data.segment_data,voice_data.neopulse)

	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_delete_voice(voice_index){
	var data = DATA_get_current_state_point(true)
	//duplicate current voice

	if(data.voice_data.length>1){
		//more that 1 voice exist
		//proceed to eliminate voice
		var voice_position = -1
		data.voice_data.find(item=>{
			voice_position++
			return item.voice_index==voice_index
		})
		var was_selected=data.voice_data[voice_position].selected
		data.voice_data.splice(voice_position,1)
		//select previous voice if no other voice is selected
		//if(!data.voice_data.some(voice=>{return voice.selected})){
		if(was_selected){
			voice_position--
			if(voice_position<0)voice_position=0
			data.voice_data[voice_position].selected=true
		}

		DATA_insert_new_state_point(data)
		DATA_load_state_point_data(false,true)
	}
}

//input voice_data , new_Li 1/1
function DATA_calculate_force_voice_Li(voice,new_Li_absolute,extend_last_segment_blocked="false"){
	//!!! this function modify voice_data !!!

	//need to be done Before
	//calculate minimum voice length (pulse absolute)
	//DATA_calculate_minimum_Li

	var new_Li = new_Li_absolute * voice.neopulse.D/voice.neopulse.N
	var voice_Li = 0 //att is voice Li EXCLUDING segments that will be cutted
	var index_cut = 0
	var index = 0

	voice.data.segment_data.find(segment=>{
		var Ls = segment.time.slice(-1)[0].P
		voice_Li+=Ls
		if(voice_Li>=new_Li){
			index_cut=index
			return true
		}
		index++
	})
	var d_Li = new_Li - voice_Li
	var n_seg = voice.data.segment_data.length-1

	if(voice_Li<new_Li){
		//modify last segment if void OR create a new one
		var last_segment = voice.data.segment_data[voice.data.segment_data.length-1]
		//if voice is blocked need to add a new segment always
		//if(!voice.blocked && last_segment.note.length==2 && last_segment.note[0]==-1 && last_segment.fraction.length==1 && last_segment.fraction[0].N==1 && last_segment.fraction[0].D==1){

		//if voice_blocked it can be extended if previously checked
		if(!(voice.blocked && !extend_last_segment_blocked) && last_segment.note.length==2 && last_segment.note[0]==-1 && last_segment.fraction.length==1 && last_segment.fraction[0].N==1 && last_segment.fraction[0].D==1){
			var end_pulse=last_segment.time[1].P+d_Li
			voice.data.segment_data[voice.data.segment_data.length-1].time[1].P=end_pulse
			voice.data.segment_data[voice.data.segment_data.length-1].fraction[0].stop=end_pulse
			//diesis doesnt change
		}else{
			var fraction=[]
			var new_Ls=d_Li
			fraction[0] = {"N":1,"D":1,"start":0,"stop":new_Ls}
			var new_segment_data={"time":[{"P":0,"F":0},{"P":new_Ls,"F":0}],"fraction":fraction,"note":[-1,-4],"diesis":[true,true],"segment_name":""}
			voice.data.segment_data.splice(voice.data.segment_data.length,0,new_segment_data)
		}
	}

	if(voice_Li>=new_Li){
		//modify or eliminate last segment(s)
		voice.data.segment_data.splice(index_cut+1,n_seg-index_cut+1)
		if(d_Li==0){
			//cut from next segment
			//voice.data.segment_data.splice(index_cut+1,n_seg-index_cut+1)
			//console.error("hello")
		}else{
			//shorten index_cut segment
			var new_Ls = voice.data.segment_data[index_cut].time.slice(-1)[0].P+d_Li//d_Li negativo
			DATA_calculate_force_segment_data_Ls(voice.data.segment_data[index_cut],new_Ls)
		}
	}
	voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
}

function DATA_verify_blocked_voices_extend_last_segment(data){
	var extend_blocked = true
	data.voice_data.forEach(voice=>{
		if(voice.blocked){
			var last_segment = voice.data.segment_data[voice.data.segment_data.length-1]
			if(last_segment.note.length==2 && last_segment.note[0]==-1 && last_segment.fraction.length==1 && last_segment.fraction[0].N==1 && last_segment.fraction[0].D==1){
				//no problem
			}else{
				extend_blocked=false
			}
		}
	})
	return extend_blocked
}

function DATA_rearrange_voices(new_voice_index_order){
	//new_voice_index_order == array of voice_index in the new order
	var data = DATA_get_current_state_point(true)

	//verify that new_voices_order contain every voice_index of data voices XXX
	var voice_index_array = data.voice_data.map(voice=>{
		return voice.voice_index
	})
	//console.log(voice_index_array)
	if(new_voice_index_order.length!=voice_index_array.length){
		console.error("New voice order not compatible with current voice list")
		return
	}

	var changed = voice_index_array.some((value,index)=>{
		return value!=new_voice_index_order[index]
	})

	if(!changed){
		//console.error("No cheanges")
		return
	}

	var proceed = !voice_index_array.some(index=>{
		return !new_voice_index_order.some(new_index=>{return new_index==index})
	})

	if(!proceed){
		console.error("New voice order not compatible with current voice list")
		return
	}

	// reorder
	var new_voice_data = new_voice_index_order.map(voice_index=>{
		return data.voice_data.find(voice=>{return voice.voice_index==voice_index})
	})

	data.voice_data=new_voice_data
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_which_voice_instrument(voice_index){
	var data = DATA_get_current_state_point(false)

	var current_voice_data = data.voice_data.find(item=>{
		return item.voice_index==voice_index
	})

	return current_voice_data.instrument
}

function DATA_show_hide_voice_RE(voice_index,show){

	var data = DATA_get_current_state_point(true)
	//Selected voice

	var selected_voice_data= data.voice_data.find(item=>{
		return item.voice_index==voice_index
	})

	selected_voice_data.RE_visible = show

	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_list_available_metro_sounds(){
	var all_metro_sounds=[0,1,2,3,4]
	//not used  1,2,3,4,5

	// var voice_matrix = document.querySelector(".App_RE_voice_matrix")
	// var voice_list = [...voice_matrix.querySelectorAll(".App_RE_voice")]
	// var used = voice_list.map(item => {return RE_which_metro_sound(item)})

	var data = DATA_get_current_state_point(true)
	var used = data.voice_data.map(voice => {return voice.metro})

	used.sort(function(a, b){return a-b})
	//make sure no duplicates (zeros)
	var unique = [...new Set(used)]

	var available_metro_sounds = all_metro_sounds.filter(item=>{
		var found = unique.some(usedItem=>{
			return usedItem == item
		})
		return !found
	})
	//add 0
	available_metro_sounds.unshift(0)
	var available_metro_sounds = [...new Set(available_metro_sounds)]
	return available_metro_sounds
}

//SEGMENT COLUMN
function DATA_clear_column_segment(segment_index){
	var data = DATA_get_current_state_point(true)

	//change segment all blocked voices
	data.voice_data.forEach(voice=>{
		if(voice.blocked){
			var segment_data = voice.data.segment_data[segment_index]
			//reset fractioning
			var frac = segment_data.fraction.pop()
			frac.N=1
			frac.D=1
			frac.start=0
			segment_data.fraction=[]
			segment_data.fraction[0]=frac
			//reset note
			segment_data.note=[]
			segment_data.note[0]=-1
			segment_data.note[1]=-4
			//reset diesis
			segment_data.diesis=[]
			segment_data.diesis[0]=true
			segment_data.diesis[1]=true
			//reset time
			segment_data.time=[]
			segment_data.time[0]={"P":0,"F":0}
			segment_data.time[1]={"P":frac.stop,"F":0}
			//reset name
			//segment_data.segment_name="" //other blocked segments too??? NO!

			//midi data
			voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
		}
	})

	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_delete_column_segment(segment_index){
	APP_stop()
	var data = DATA_get_current_state_point(true)

	//change segment all blocked voices
	var first_blocked_voice = data.voice_data.find(item=>{
		return item.blocked
	})
	//verify there is more than 1 segment
	if(first_blocked_voice.data.segment_data.length==1)return

	var Ls_absolute= first_blocked_voice.data.segment_data[segment_index].time.slice(-1)[0].P*first_blocked_voice.neopulse.N/first_blocked_voice.neopulse.D

	//recalculate Li
	var new_Li_absolute = data.global_variables.Li-Ls_absolute
	data.global_variables.Li=new_Li_absolute

	data.voice_data.forEach(item=>{
		if(item.blocked){
			//remove same segment from blocked voices
			item.data.segment_data.splice(segment_index,1)
			//recalculate midi
			item.data.midi_data=DATA_segment_data_to_midi_data(item.data.segment_data,item.neopulse)
		}else{
			//cut other voices
			DATA_calculate_force_voice_Li(item,new_Li_absolute,false)
		}
	})

	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_copy_column_segment(segment_index){
	var data = DATA_get_current_state_point(true)

	var segment_data_list = []
	data.voice_data.forEach(voice=>{
		if(voice.blocked){
			segment_data_list.push({voice_index:voice.voice_index,neopulse:voice.neopulse, segment_data: voice.data.segment_data[segment_index]})
		}
	})

	copied_data = {"id":"column", "data":segment_data_list}
	console.log(copied_data)
}

function DATA_paste_column_segment(segment_index){
	if(copied_data.id!="column"){
		return
	}

	var data = DATA_get_current_state_point(true)

	//verify if copied data is compatible (same list of blocked voices and in correct order)
	var blocked_voices_list= data.voice_data.filter(item=>{
		return item.blocked
	})

	var compatible=true
	if (blocked_voices_list.length==copied_data.data.length){
		for (let i = 0; i < blocked_voices_list.length; i++) {
			if(blocked_voices_list[i].voice_index!=copied_data.data[i].voice_index)compatible=false
			if(blocked_voices_list[i].neopulse.N!=copied_data.data[i].neopulse.N)compatible=false
			if(blocked_voices_list[i].neopulse.D!=copied_data.data[i].neopulse.D)compatible=false
		}
	}else{
		compatible=false
	}

	if(!compatible){
		console.error("COPIED DATA NON COMPATIBLE")
		return
	}

	var new_Li_absolute=0
	data.voice_data.forEach(voice=>{
		if(voice.blocked && compatible){
			var segment_copied_data = copied_data.data.find(data=>{return data.voice_index==voice.voice_index}).segment_data
			//var current_segment_data=voice.data.segment_data[segment_index]

			//verify if is possible...
			//if segment Ls> and not void
			var current_ls = voice.data.segment_data[segment_index].time.slice(-1)[0].P
			var copied_ls = segment_copied_data.time.slice(-1)[0].P

			if(current_ls==copied_ls){
				//segments are same length, proceed to copy data
				voice.data.segment_data[segment_index]=segment_copied_data
				//recalculate midi time and notes
				voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
			}else{
				//different
				if(current_ls>copied_ls){
					//segment bigger
					//this function will create a segment that start with the copied one and continue with Fr 1/1 to the end of current segment
					var last_index = segment_copied_data.time.length-1

					//copy data
					voice.data.segment_data[segment_index]=JSON.parse(JSON.stringify(segment_copied_data))

					//add silence last-1 item
					voice.data.segment_data[segment_index].note.splice(last_index,0,-1)
					voice.data.segment_data[segment_index].diesis.splice(last_index,0,true)
					//add last pulse
					voice.data.segment_data[segment_index].time.push({"P":current_ls,"F":0})


					//add/change fractioning
					var last_fraction = segment_copied_data.fraction.slice(-1)[0]
					if(last_fraction.N==1 && last_fraction.D==1){
						//modify last fraction interval
						voice.data.segment_data[segment_index].fraction[segment_copied_data.fraction.length-1].stop=current_ls
					}else{
						//add new fraction interval
						voice.data.segment_data[segment_index].fraction.push({"N":1,"D":1,"start":copied_ls,"stop":current_ls})
					}

					//recalculate midi time and notes
					voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
				}else{
					//short
					//make the segment bigger
					var min_delta_Ls_abs = DATA_calculate_minimum_Li()
					var delta_abs = (copied_ls-current_ls)*voice.neopulse.N/voice.neopulse.D
					var resto_abs = delta_abs%min_delta_Ls_abs
					//check compatibility with NP
					if(resto_abs==0){
						//i can change the Li of the segment with the copied one
					}else{
						//need to add some more pulses
						//in this case it is a BAD THING : theorically impossible
						console.error("ERROR copy segment column, mismatching Ls")
						compatible=false
						return
					}
					//change Li
					var newLi=data.global_variables.Li+delta_abs

					if(new_Li_absolute!=0 && new_Li_absolute!=newLi){
						//maybe overkill
						console.error("ERROR copy segment column, mismatching Li")
						compatible=false
						return
					}
					new_Li_absolute=newLi

					if(newLi>max_Li){
						console.error("ERROR copy segment column, limit Li reached")
						compatible=false
						return
					}

					//copy segment
					voice.data.segment_data[segment_index]=segment_copied_data
					voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
				}
			}
		}
	})

	if(!compatible){
		console.error("COPIED DATA NON COMPATIBLE: Li reached")
		return
	}

	//change Li
	if(new_Li_absolute!=0){
		data.global_variables.Li=new_Li_absolute
		//change unblocked voices
		data.voice_data.forEach(voice=>{
				if(!voice.blocked){
					DATA_calculate_force_voice_Li(voice,new_Li_absolute,false)
				}
		})
	}

	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

//SEGMENTS
function DATA_add_segment(voice_index,segment_index){
	var data = DATA_get_current_state_point(true)

	var current_voice_data = data.voice_data.find(item=>{
		return item.voice_index==voice_index
	})
	// console.log("implement this "+voice_index+" "+segment_index)
	//console.log(current_voice_data)
	if(current_voice_data==null){
		console.error("Error adding segment, voice index not found")
		return
	}

	if(current_voice_data.data.segment_data.length<segment_index || segment_index<0){
		console.error("Error adding segment, segment index not found")
		return
	}
	//calculate minimum segment length (pulse absolute)
	var new_Ls_absolute=DATA_calculate_minimum_Li()

	//try to match with previous segment Li
	if(segment_index>0){
		var last_segment_Ls_absolte = current_voice_data.data.segment_data[segment_index-1].time.slice(-1)[0].P *current_voice_data.neopulse.N/current_voice_data.neopulse.D
		if(last_segment_Ls_absolte%new_Ls_absolute==0){
			//it is possible to use this value
			//console.log(last_segment_Ls_absoltue)
			new_Ls_absolute=last_segment_Ls_absolte
		}
	}

	var new_Li_absolute=Li+new_Ls_absolute

	//max length idea
	if(new_Li_absolute>max_Li){
		console.error("ERROR: max Li reached")
		return
	}

	if(current_voice_data.blocked){
		//voice is blocked
		data.voice_data.forEach(voice=>{
			var new_Ls=new_Ls_absolute*voice.neopulse.D/voice.neopulse.N
			var fraction=[]
			fraction[0] = {"N":1,"D":1,"start":0,"stop":new_Ls}
			var new_segment_data={"time":[{"P":0,"F":0},{"P":new_Ls,"F":0}],"fraction":fraction,"note":[-1,-4],diesis:[true,true],"segment_name":""}
			if(voice.blocked){
				//add segment in position
				voice.data.segment_data.splice(segment_index,0,new_segment_data)
				//midi
				voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
			}else{
				//add-extend segment in end
				DATA_calculate_force_voice_Li(voice,new_Li_absolute,false)
			}
			//console.log(voice.data.segment_data)
		})
	}else{
		//verify if last segment blocked voices can be extended
		var extend_blocked=DATA_verify_blocked_voices_extend_last_segment(data)

		data.voice_data.forEach(voice=>{
			var new_Ls=(new_Ls_absolute*voice.neopulse.D/voice.neopulse.N)
			var fraction=[]
			fraction[0] = {"N":1,"D":1,"start":0,"stop":new_Ls}
			var new_segment_data={"time":[{"P":0,"F":0},{"P":new_Ls,"F":0}],"fraction":fraction,"note":[-1,-4],diesis:[true,true],"segment_name":""}
			if(voice.voice_index==voice_index){
				//add segment in position current voice
				voice.data.segment_data.splice(segment_index,0,new_segment_data)
				//midi
				//console.log(voice.data.segment_data)
				voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
			}else{
				//add-extend segment in end
				DATA_calculate_force_voice_Li(voice,new_Li_absolute,extend_blocked)
				//add-extend segment in end
				//if voice is blocked need to add a new segment always XXX
				// var last_segment = voice.data.segment_data[voice.data.segment_data.length-1]
				// if(!voice.blocked && last_segment.note.length==2 && last_segment.note[0]==-2 && last_segment.fraction.length==1 && last_segment.fraction[0].N==1 && last_segment.fraction[0].D==1){
				// 	var end_pulse=(last_segment.time[1].P+(new_Ls_absolute*voice.neopulse.D/voice.neopulse.N))
				// 	voice.data.segment_data[voice.data.segment_data.length-1].time[1]={"P":end_pulse,"F":0}
				// 	voice.data.segment_data[voice.data.segment_data.length-1].fraction[0].stop=end_pulse
				// }else{
				// 	voice.data.segment_data.splice(voice.data.segment_data.length,0,new_segment_data)
				// }
			}

		})
	}
	//recalculate Li
	data.global_variables.Li=new_Li_absolute
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_delete_segment(voice_index,segment_index){
	APP_stop()
	var data = DATA_get_current_state_point(true)

	var voice = data.voice_data.find(item=>{
		return item.voice_index==voice_index
	})
	//verify there is more than 1 segment
	if(voice.data.segment_data.length==1)return

	var Ls_absolute= voice.data.segment_data[segment_index].time.slice(-1)[0].P*voice.neopulse.N/voice.neopulse.D

	//recalculate Li
	var new_Li_absolute = data.global_variables.Li-Ls_absolute
	data.global_variables.Li=new_Li_absolute

	if(voice.blocked){
		data.voice_data.forEach(item=>{
			if(item.blocked){
				//remove same segment from blocked voices
				item.data.segment_data.splice(segment_index,1)
				//recalculate midi
				item.data.midi_data=DATA_segment_data_to_midi_data(item.data.segment_data,item.neopulse)
			}else{
				//cut other voices
				DATA_calculate_force_voice_Li(item,new_Li_absolute,false)
			}
		})
	}else{
		data.voice_data.forEach(item=>{
			if(item.voice_index==voice_index){
				//remove segment
				item.data.segment_data.splice(segment_index,1)
				//recalculate midi
				item.data.midi_data=DATA_segment_data_to_midi_data(item.data.segment_data,item.neopulse)
			}else{
				//cut other voices
				DATA_calculate_force_voice_Li(item,new_Li_absolute,false)
			}
		})
	}


	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_rearrange_segments(voice_index,new_segment_index_order){
	var data = DATA_get_current_state_point(true)

	var current_voice_data = data.voice_data.find(item=>{
		return item.voice_index==voice_index
	})

	var mod_voice_list=[]
	if(current_voice_data.blocked){
		//find list all blocked voices
		mod_voice_list=data.voice_data.filter(item=>{
			return item.blocked
		})
	}else{
		mod_voice_list[0]=current_voice_data
	}

	mod_voice_list.forEach(voice=>{
		var new_segment_data = []
		new_segment_index_order.forEach(segment_index=>{
			new_segment_data.push(voice.data.segment_data[segment_index])
		})

		voice.data.segment_data=new_segment_data
		voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
	})

	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_calculate_force_segment_data_Ls(segment,new_Ls){
	//verify cut can be done??
	//need to be done Before
	//calculate minimum segment length (pulse absolute)
	//var delta_Ls_absolute=DATA_calculate_minimum_Li()
	var old_Ls = segment.time.slice(-1)[0].P

	//new_Ls is NOT absolute

	if(new_Ls>old_Ls){
		//expand segment
		//verify fractioning
		var last_fraction = segment.fraction.slice(-1)[0]
		var delta = new_Ls-old_Ls
		var resto = delta%last_fraction.N

		if(resto==0){
			//extending fraction
			last_fraction.stop=new_Ls
			//mod last pulse
			segment.time[segment.time.length-1].P=new_Ls
		}else{
			//creating a new fractioning range 1/1+ "s" XXX
			var inter_position = new_Ls-resto
			segment.fraction[segment.fraction.length-1].stop=inter_position
			var new_fraction = {"N":1,"D":1,"start":inter_position,"stop":new_Ls}
			segment.fraction.push(new_fraction)
			//add pulse and mod last
			segment.time[segment.time.length-1].P=inter_position
			segment.time.push({"P":new_Ls,"F":0})
			segment.note[segment.note.length-1]=-1
			segment.note.push(-4)
			segment.diesis.push(true)
		}
	}

	if(new_Ls<old_Ls){
		//trim segment
		//determine fractioning roules
		//what is the last fraction involved
		var index_last_fraction_range=0
		//var position=0
		segment.fraction.find(fraction=>{
			if(fraction.stop>=new_Ls)return true
			index_last_fraction_range++
		})
		//eliminate the others
		segment.fraction.splice(index_last_fraction_range+1,segment.fraction.length-index_last_fraction_range-1)
		//eventually modificate/duplicate the last fractioning range
		var last_fraction = segment.fraction[index_last_fraction_range]

		var start_pulse = last_fraction.start
		var stop_pulse = last_fraction.stop
		var new_range = new_Ls-start_pulse
		var resto = new_range%last_fraction.N
		if(resto==0){
			//no need of new fractioning range
			//modify current fractioning range
			segment.fraction[index_last_fraction_range].stop=new_Ls
			//modify notes and pulses
			var index_cut=0 //simple bc resto ==0
			segment.time.find(time=>{
				if( time.P>=new_Ls)return true
				index_cut++
			})
			segment.time.splice(index_cut,segment.time.length-index_cut)
			segment.note.splice(index_cut,segment.note.length-index_cut)
			segment.diesis.splice(index_cut,segment.diesis.length-index_cut)
			segment.time.push({"P":new_Ls,"F":0})
			segment.note.push(-4)
			segment.diesis.push(true)
		}else{
			//modify current fractioning range
			var start_last_fraction_range=0
			if(new_range==resto){
				//old fractioning range is lost
				segment.fraction[index_last_fraction_range].N=1
				segment.fraction[index_last_fraction_range].D=1
				segment.fraction[index_last_fraction_range].stop=new_Ls
				start_last_fraction_range=start_pulse
			}else{
				segment.fraction[index_last_fraction_range].stop=(new_Ls-resto)
				//creating new fractioning range
				segment.fraction.push({"N":1,"D":1,"start":(new_Ls-resto),"stop":new_Ls})
				start_last_fraction_range=new_Ls-resto
			}

			//modify notes and pulses
			//clear all note and pulses of entire LAST fractioning range

			var index_cut=0 //OF STARTING LAST RANGE FORCEd 1/1
			segment.time.find(time=>{
				if(time.P>=start_last_fraction_range)return true
				index_cut++
			})
			segment.time.splice(index_cut,segment.time.length-index_cut)
			segment.note.splice(index_cut,segment.note.length-index_cut)
			segment.diesis.splice(index_cut,segment.note.length-index_cut)
			segment.time.push({"P":start_last_fraction_range,"F":0})
			segment.note.push(-1)
			segment.diesis.push(true)
			segment.time.push({"P":new_Ls,"F":0})
			segment.diesis.push(true)
		}
	}
}

function DATA_change_Ls_segment(voice_index,segment_index,new_Ls){
	//new_Ls is relative to the voice (no absolute)
	var data = DATA_get_current_state_point(true)
	data = DATA_calculate_change_Ls_segment(data,voice_index,segment_index,new_Ls)
	if(data==false)return false
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
	return true
}

function DATA_calculate_change_Ls_segment(data,voice_index,segment_index,new_Ls){
	var voice_data_mod = data.voice_data.find(voice=>{return voice.voice_index==voice_index})
	//calc min Ls
	var delta_Ls_absolute=DATA_calculate_minimum_Li()
	var delta_Ls = delta_Ls_absolute*voice_data_mod.neopulse.D/voice_data_mod.neopulse.N

	if((new_Ls%delta_Ls)!=0)return false//new Ls not compatible with this voice segment, redraw correct segment(s)
	var new_Ls_absolute = new_Ls*voice_data_mod.neopulse.N/voice_data_mod.neopulse.D
	var new_Li_absolute = Li+(new_Ls-voice_data_mod.data.segment_data[segment_index].time.slice(-1)[0].P)*voice_data_mod.neopulse.N/voice_data_mod.neopulse.D

	if(new_Li_absolute>max_Li){
		console.error("ERROR: max Li reached")
		return data
	}

	data.global_variables.Li = new_Li_absolute
	//recalculate data segment
	if(voice_data_mod.blocked){
		data.voice_data.forEach(voice=>{
			if(voice.blocked){
				//change segment
				DATA_calculate_force_segment_data_Ls(voice.data.segment_data[segment_index],new_Ls_absolute*voice.neopulse.D/voice.neopulse.N)
				//console.error(voice.data.segment_data)
				voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
			}else{
				//change Li
				DATA_calculate_force_voice_Li(voice,new_Li_absolute,false)
			}
		})
	}else{
		var extend_blocked=DATA_verify_blocked_voices_extend_last_segment(data)

		data.voice_data.forEach(voice=>{
			if(voice.voice_index==voice_index){
				//change segment
				DATA_calculate_force_segment_data_Ls(voice.data.segment_data[segment_index],new_Ls_absolute*voice.neopulse.D/voice.neopulse.N)
				voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
			}else{
				//change Li
				DATA_calculate_force_voice_Li(voice,new_Li_absolute,extend_blocked)
			}
		})
	}
	return data
}

function DATA_merge_segment(voice_index,segment_index){
	var data = DATA_get_current_state_point(true)
	var current_voice_data = data.voice_data.find(item=>{
		return item.voice_index==voice_index
	})
	// console.log("implement this "+voice_index+" "+segment_index)
	var voice_list=[]
	if(current_voice_data.blocked){
		//merge everything is connected
		data.voice_data.forEach(item=>{
			if(item.blocked)voice_list.push(item)
		})
	}else{
		voice_list.push(current_voice_data)
	}

	voice_list.forEach(voice=>{
		var seg1=voice.data.segment_data[segment_index]
		var seg2=voice.data.segment_data[segment_index+1]

		//merge
		seg1.note.pop()
		seg1.diesis.pop()
		seg1.note=seg1.note.concat(seg2.note)
		seg1.diesis=seg1.diesis.concat(seg2.diesis)
		//recalculate ranges
		var fraction_index=seg1.fraction.length
		var prev_fraction_end=seg1.time[seg1.time.length-1].P
		seg1.fraction=seg1.fraction.concat(seg2.fraction)
		for(var i=fraction_index; i<seg1.fraction.length;i++){
			var range=seg1.fraction[i].stop-seg1.fraction[i].start
			seg1.fraction[i].start=prev_fraction_end
			prev_fraction_end+=range
			seg1.fraction[i].stop=prev_fraction_end
		}
		//if contact fraction are == merge!
		if(seg1.fraction[fraction_index-1].N==seg1.fraction[fraction_index].N && seg1.fraction[fraction_index-1].D==seg1.fraction[fraction_index].D){
			//merge
			seg1.fraction[fraction_index-1].stop=seg1.fraction[fraction_index].stop
			seg1.fraction.splice(fraction_index,1)
		}

		//time need to recalc
		var time_index=seg1.time.length
		var prev_segment_end=seg1.time[seg1.time.length-1].P
		seg2.time.shift()
		seg1.time=seg1.time.concat(seg2.time)

		for(var i=time_index; i<seg1.time.length;i++){
			//console.log(seg1.time[i])
			seg1.time[i].P+=prev_segment_end
		}
		//eliminate second
		voice.data.segment_data.splice(segment_index+1,1)
		//midi
		voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
	})

	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

//function find and clear data inside a segment and redraw
function DATA_clear_segment(voice_index,segment_index){
	var data = DATA_get_current_state_point(true)

	var voice = data.voice_data.find(item=>{
		return item.voice_index==voice_index
	})

	var segment_data = voice.data.segment_data[segment_index]
	//reset fractioning
	var frac = segment_data.fraction.pop()
	frac.N=1
	frac.D=1
	frac.start=0
	segment_data.fraction=[]
	segment_data.fraction[0]=frac
	//reset note
	segment_data.note=[]
	segment_data.note[0]=-1
	segment_data.note[1]=-4
	//reset diesis
	segment_data.diesis=[]
	segment_data.diesis[0]=true
	segment_data.diesis[1]=true
	//reset time
	segment_data.time=[]
	segment_data.time[0]={"P":0,"F":0}
	segment_data.time[1]={"P":frac.stop,"F":0}
	//reset name
	//segment_data.segment_name="" //other blocked segments too??? NO!

	//midi data
	voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)

	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_break_segment(voice_index,segment_index, pulse){
	var data = DATA_get_current_state_point(true)
	//XXX control again if brEAKABLE???
	var voice_data = data.voice_data.find(item=>{
		return item.voice_index==voice_index
	})
	//calculating absolute pulse
	var abs_pulse= pulse * voice_data.neopulse.N/voice_data.neopulse.D

	var voices_data_list=[]
	if(voice_data.blocked){
		//find all voice datas
		data.voice_data.forEach(item=>{
			if(item.blocked)voices_data_list.push(item)
		})
	}else{
		voices_data_list[0]=voice_data
	}

	voices_data_list.forEach(voice=>{
		//create a copy of degment data and inserting it in next position
		var position = (abs_pulse*voice.neopulse.D/voice.neopulse.N)
		var position_index = voice.data.segment_data[segment_index].time.indexOf(voice.data.segment_data[segment_index].time.find(time=>{return time.P==position && time.F==0}))
		if(position_index==-1){
			//need to add an event in that position
			voice.data.segment_data[segment_index].time.find(time=>{
				var pulse =  time.P
				position_index++
				if(pulse>=position){//>= bc it didnt found it and it can be a fraction up
					voice.data.segment_data[segment_index].time.splice(position_index,0,{"P":position,"F":0})
					voice.data.segment_data[segment_index].note.splice(position_index,0,-1)
					voice.data.segment_data[segment_index].diesis.splice(position_index,0,true)
					return true
				}
			})
		}
		var new_segment = JSON.parse(JSON.stringify(voice.data.segment_data[segment_index]))
		//cut segment at position

		//first segment
		var eliminate_s1 = voice.data.segment_data[segment_index].time.length-1-position_index //XXX if posiiton index = -1 ???
		voice.data.segment_data[segment_index].time.splice(position_index+1,eliminate_s1)
		voice.data.segment_data[segment_index].note.splice(position_index+1,eliminate_s1)
		voice.data.segment_data[segment_index].diesis.splice(position_index+1,eliminate_s1)
		voice.data.segment_data[segment_index].note[position_index]=-4
		voice.data.segment_data[segment_index].diesis[position_index]=true
		//fractioning
		var fraction_index=-1
		voice.data.segment_data[segment_index].fraction.find(fraction=>{
			fraction_index++
			return fraction.stop>=position
		})
		voice.data.segment_data[segment_index].fraction.splice(fraction_index+1,voice.data.segment_data[segment_index].fraction.length-1-fraction_index)
		//verify last is == Ls
		voice.data.segment_data[segment_index].fraction[voice.data.segment_data[segment_index].fraction.length-1].stop=position

		//second segment
		new_segment.time.splice(0,position_index)
		new_segment.note.splice(0,position_index)
		new_segment.diesis.splice(0,position_index)
		//verify first note is not a L
		if(new_segment.note[0]==-3)new_segment.note[0]=-1
		//recalculate pulses
		var s2_time=[]
		new_segment.time.forEach(time=>{
			s2_time.push({"P":time.P-position,"F":time.F})
		})
		new_segment.time=s2_time
		//fractioning
		fraction_index=-1
		new_segment.fraction.find(fraction=>{
			fraction_index++
			return fraction.stop>position
		})
		new_segment.fraction.splice(0,fraction_index)
		new_segment.fraction[0].start=position
		//recalculate fraction ranges
		var current_p=0
		for(var i=0; i<new_segment.fraction.length; i++){
			new_segment.fraction[i].start=new_segment.fraction[i].start-position
			new_segment.fraction[i].stop=new_segment.fraction[i].stop-position
		}
		voice.data.segment_data.splice(segment_index+1,0,new_segment)
		voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
		//console.log(voice.neopulse)
	})

	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_calculate_segment_break_in_object_index(voice_index,segment_index,abs_position){
	//already checked if it is a non fractioned pulse
	var data = DATA_get_current_state_point(false)

	var voice_data = data.voice_data.find(voice=>{
		return voice.voice_index==voice_index
	})

	var position = abs_position
	var all_segment_data= voice_data.data.segment_data
	if(segment_index==null){
		//ABSOLUTE position
		//need to calculate segment number and relative position
		segment_index=0
		var prev_segments_n_obj = 0
		all_segment_data.find(segment=>{
			var n_obj = segment.time.length -1
			if(prev_segments_n_obj+n_obj>abs_position){
				return true
			}else{
				//next segment
				prev_segments_n_obj+=n_obj //last element of a segment is .
				segment_index++
				return false
			}
		})
		position = abs_position - prev_segments_n_obj
	}

	var current_segment_data=all_segment_data[segment_index]

	if(current_segment_data.time[position].F!=0)return false

	var pulse = current_segment_data.time[position].P

	var pulse_abs=pulse*voice_data.neopulse.N/voice_data.neopulse.D

	//verify if divisible by min Ls
	var min_Ls_abs = DATA_calculate_minimum_Li()
	if(pulse_abs%min_Ls_abs!=0)return false

	if(voice_data.blocked){
		//if voice blocked verify fraccioning
		//find every fraction range
		var not_breakable = data.voice_data.some(voice=>{
			if(voice.blocked){
				//find current segment/fractioning range
				var current_pulse= pulse_abs*voice.neopulse.D/voice.neopulse.N
				var current_fraction=voice.data.segment_data[segment_index].fraction.find(fraction=>{return fraction.start<=current_pulse && fraction.stop>current_pulse})
				//console.log(current_fraction)
				//console.log(current_pulse)
				//verify if current pulse exist in every fractioning range
				var delta = current_pulse-current_fraction.start
				if(delta%current_fraction.N!=0){
					//not breakable
					//stop the search!!!
					return true
				}
			}
		})
		if(not_breakable){
			return false
		}else{
			return true
		}
	}else{
		//if voice unblocked ok
		return true
	}
	return false
}

function DATA_copy_segment(voice_index,segment_index){
	//verify if voice is blocked
	var data = DATA_get_current_state_point(true)

	var voice = data.voice_data.find(item=>{
		return item.voice_index==voice_index
	})

	//selected segment
	var segment_data=voice.data.segment_data[segment_index]
	copied_data = {"id":"segment", "data":segment_data}
}

function DATA_paste_segment(voice_index,segment_index){
	if(copied_data.id!="segment"){
		return
	}

	var data = DATA_get_current_state_point(true)

	var voice = data.voice_data.find(item=>{
		return item.voice_index==voice_index
	})

	//selected segment
	var current_segment_data=voice.data.segment_data[segment_index]

	//verify if is possible...
	//if segment Ls> and not void
	var current_ls = voice.data.segment_data[segment_index].time.slice(-1)[0].P
	var copied_ls = copied_data.data.time.slice(-1)[0].P

	if(current_ls==copied_ls){
		//segments are same length, proceed to copy data
		voice.data.segment_data[segment_index]=copied_data.data
		//recalculate midi time and notes
		voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
	}else{
		//different
		if(current_ls>copied_ls){
			//segment bigger
			//this function will create a segment that start with the copied one and continue with Fr 1/1 to the end of current segment
			var last_index = copied_data.data.time.length-1

			//copy data
			voice.data.segment_data[segment_index]=JSON.parse(JSON.stringify(copied_data.data))

			//add silence last-1 item
			voice.data.segment_data[segment_index].note.splice(last_index,0,-1)
			voice.data.segment_data[segment_index].diesis.splice(last_index,0,true)
			//add last pulse
			voice.data.segment_data[segment_index].time.push({"P":current_ls,"F":0})


			//add/change fractioning
			var last_fraction = copied_data.data.fraction.slice(-1)[0]
			if(last_fraction.N==1 && last_fraction.D==1){
				//modify last fraction interval
				voice.data.segment_data[segment_index].fraction[copied_data.data.fraction.length-1].stop=current_ls
			}else{
				//add new fraction interval
				voice.data.segment_data[segment_index].fraction.push({"N":1,"D":1,"start":copied_ls,"stop":current_ls})
			}

			//recalculate midi time and notes
			voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
		}else{
			//short
			//make the segment bigger
			var min_delta_Ls_abs = DATA_calculate_minimum_Li()
			var delta_abs = (copied_ls-current_ls)*voice.neopulse.N/voice.neopulse.D
			var resto_abs = delta_abs%min_delta_Ls_abs
			//check compatibility with NP
			var wanted_Ls_abs = 0
			if(resto_abs==0){
				//i can change the Li of the segment with the copied one
				wanted_Ls_abs= copied_ls*voice.neopulse.N/voice.neopulse.D
			}else{
				//need to add some more pulses
				wanted_Ls_abs= copied_ls*voice.neopulse.N/voice.neopulse.D + (min_delta_Ls_abs-resto_abs)
				delta_abs=wanted_Ls_abs-(current_ls*voice.neopulse.N/voice.neopulse.D)
			}
			//change Li
			data.global_variables.Li+=delta_abs

			if(data.global_variables.Li>max_Li){
				console.error("ERROR copy segment, limit Li reached")
				return
			}

			//check segment blocked
			if(voice.blocked){
				data.voice_data.forEach(item=>{
					if(item.blocked){
						//change Ls all blocked voices
						var end_pulse=wanted_Ls_abs*item.neopulse.D/item.neopulse.N
						item.data.segment_data[segment_index].time.push({"P":end_pulse,"F":0})
						item.data.segment_data[segment_index].note.splice(item.data.segment_data[segment_index].note.length-1,0,-1)
						item.data.segment_data[segment_index].diesis.splice(item.data.segment_data[segment_index].diesis.length-1,0,true)
						//if contact fraction are == merge!
						var fraction_list = item.data.segment_data[segment_index].fraction
						if(fraction_list[fraction_list.length-1].N==1 && fraction_list[fraction_list.length-1].D==1){
							//merge-expand range
							fraction_list[fraction_list.length-1].stop=end_pulse
						}else{
							//add
							fraction_list[fraction_list.length]={"N":1,"D":1,"start":fraction_list[fraction_list.length-1].stop,"stop":end_pulse}
						}
					}else{
						//add-extend segment at the end
						var last_segment = item.data.segment_data[item.data.segment_data.length-1]
						if(last_segment.note.length==2 && last_segment.note[0]==-2 && last_segment.fraction.length==1 && last_segment.fraction[0].N==1 && last_segment.fraction[0].D==1){
							var end_pulse=last_segment.time[1].P+delta_abs*item.neopulse.D/item.neopulse.N
							item.data.segment_data[item.data.segment_data.length-1].time[1].P=end_pulse
							item.data.segment_data[item.data.segment_data.length-1].fraction[0].stop=end_pulse
						}else{
							var end_pulse=delta_abs*item.neopulse.D/item.neopulse.N
							var f = []
							f[0]={"N":1,"D":1,"start":0,"stop":end_pulse}
							var new_segment={"time": [{"P":0,"F":0},{"P":end_pulse,"F":0}], "note":[-2,-4],"diesis":[true,true],"fraction":f,"segment_name":""}
							item.data.segment_data[item.data.segment_data.length]=new_segment
						}
					}
				})
				//copy segment
				current_ls = voice.data.segment_data[segment_index].time.slice(-1)[0].P
				if(current_ls==copied_ls){
					voice.data.segment_data[segment_index]=copied_data.data
				}else{
					//enter here if segment copied from other voice with a different neopulse not compatible with current ((current_ls=copied Ls+extra_compatibility_pulses)> copied LS)
					var old_last_pulse_index = copied_data.data.time.length-1
					//copy data
					voice.data.segment_data[segment_index]=JSON.parse(JSON.stringify(copied_data.data))

					//add silence last-1 item
					voice.data.segment_data[segment_index].note.splice(old_last_pulse_index,0,-1)
					voice.data.segment_data[segment_index].diesis.splice(old_last_pulse_index,0,true)
					//add last pulse
					voice.data.segment_data[segment_index].time.push({"P":current_ls,"F":0})
					//add/change fractioning
					var last_fraction = copied_data.data.fraction.slice(-1)[0]
					if(last_fraction.N==1 && last_fraction.D==1){
						//modify last fraction interval
						voice.data.segment_data[segment_index].fraction[copied_data.data.fraction.length-1].stop=current_ls
					}else{
						//add new fraction interval
						voice.data.segment_data[segment_index].fraction.push({"N":1,"D":1,"start":copied_ls,"stop":copied_ls})
					}
				}

				data.voice_data.forEach(item=>{
					//midi data
					item.data.midi_data=DATA_segment_data_to_midi_data(item.data.segment_data,item.neopulse)
				})
			}else{
				//copy segment after changing its length
				//change Ls
				var end_pulse=wanted_Ls_abs*voice.neopulse.D/voice.neopulse.N
				if(end_pulse==copied_ls){
					//can copy exactly
					voice.data.segment_data[segment_index]=copied_data.data
				}else{
					//need to add a silence and possibly a fraction range
					var old_last_pulse_index = copied_data.data.time.length-1
					//copy data
					voice.data.segment_data[segment_index]=JSON.parse(JSON.stringify(copied_data.data))
					//add silence last-1 item
					voice.data.segment_data[segment_index].note.splice(old_last_pulse_index,0,-1)
					voice.data.segment_data[segment_index].diesis.splice(old_last_pulse_index,0,true)
					//add last pulse
					voice.data.segment_data[segment_index].time.push({"P":end_pulse,"F":0})
					//add/change fractioning
					var last_fraction = copied_data.data.fraction.slice(-1)[0]
					if(last_fraction.N==1 && last_fraction.D==1){
						//modify last fraction interval
						voice.data.segment_data[segment_index].fraction[copied_data.data.fraction.length-1].stop=end_pulse
					}else{
						//add new fraction interval
						voice.data.segment_data[segment_index].fraction.push({"N":1,"D":1,"start":copied_ls,"stop":copied_ls})
					}
				}

				//change other voices
				data.voice_data.forEach(item=>{
					if(item.voice_index!=voice_index){
						//XXX USE FUNCTION CALCULATE VOICE LI???
						//DATA_calculate_force_voice_Li(voice,new_Li_absolute,false)
						//add-extend segment at the end
						var last_segment = item.data.segment_data[item.data.segment_data.length-1]
						if(last_segment.note.length==2 && last_segment.note[0]==-2 && last_segment.fraction.length==1 && last_segment.fraction[0].N==1 && last_segment.fraction[0].D==1){
							var end_pulse=last_segment.time[1].P+delta_abs*item.neopulse.D/item.neopulse.N
							item.data.segment_data[item.data.segment_data.length-1].time[1].P=end_pulse
							item.data.segment_data[item.data.segment_data.length-1].fraction[0].stop=end_pulse
						}else{
							var end_pulse=delta_abs*item.neopulse.D/item.neopulse.N
							var f = []
							f[0]={"N":1,"D":1,"start":0,"stop":end_pulse}
							var new_segment={"time": [{"P":0,"F":0},{"P":end_pulse,"F":0}], "note":[-2,-4],"diesis":[true,true],"fraction":f,"segment_name":""}
							item.data.segment_data[item.data.segment_data.length]=new_segment
						}
					}
					item.data.midi_data=DATA_segment_data_to_midi_data(item.data.segment_data,item.neopulse)
				})
			}
		}
	}

	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_segment_data_to_midi_data(segment_data_in,neopulse,count_in=0,current_PPM=PPM){
	var D_NP = neopulse.D
	var N_NP = neopulse.N
	var segment_data = JSON.parse(JSON.stringify(segment_data_in))//FK COPY!!!
	var time = []
	var frequency = []
	var note = []

	var nzc_note_list=[]
	var nzc_time_list = []
	var nzc_diesis_list = []
	var nzc_frac_list = []

	segment_data.forEach(segment=>{
		segment.note.pop()
		nzc_note_list = nzc_note_list.concat(segment.note)
		//create a list of times
		nzc_time_list = nzc_time_list.concat(segment.time)
		//create a list of fractioning ranges
		nzc_frac_list = nzc_frac_list.concat(segment.fraction)
		nzc_diesis_list = nzc_diesis_list.concat(segment.diesis)
	})

	//convert array time in pulse/fraction data
	var last_pulse = 0
	var segment_start = 0
	var voice_time_PF_list = [[0,0]]

	nzc_time_list.forEach(value=> {
		if(value==="") {
			console.error("error database, pulse string conversion")
			alert("error database, pulse string conversion")
			voice_time_PF_list.push( [null,null])
		}

		var pulse = value.P
		var fraction = value.F

		if(pulse == 0 && fraction==0) {
			//start of a segment
			segment_start=last_pulse
			pulse=last_pulse
		}else{
			pulse +=segment_start

			voice_time_PF_list.push([pulse,fraction])
		}
		//console.log(value +" = " +pulse+"  "+fraction)
		last_pulse = pulse
		//console.log(last_pulse+"   "+segment_start)
	})

	//convert array fraction ranges
	last_pulse = 0
	var voice_fraction_range_list = []
	nzc_frac_list.forEach(fraction=>{
		var delta = fraction.stop-fraction.start
		var start = last_pulse
		var end = start+delta
		last_pulse = end
		voice_fraction_range_list.push([[fraction.N,fraction.D],start,end,delta])
	})

	//calculate time
	var time = []
	voice_time_PF_list.forEach(item=>{
		//console.log(time_P)
		//better truncate
		if (item[1]!=0){
			var fraction_data = voice_fraction_range_list.find((fraction)=>{
				//console.log(fraction)
				return fraction[2]>item[0]
			})
			var time_sec = DATA_calculate_exact_time(count_in,item[0],item[1],fraction_data[0][0],fraction_data[0][1],N_NP,D_NP,current_PPM)
			time.push(time_sec)
		}else{
			var time_sec = DATA_calculate_exact_time(count_in,item[0],0,1,1,N_NP,D_NP,current_PPM)
			time.push(time_sec)
		}
	})
	//recalculate the duration or iTs
	var index=1
	var max_index = time.length
	var ranges_event_list = time.map(item=>{
		if(index<=max_index){
			var range= time[index] - item
			index++
			return range
		}
	})

	ranges_event_list.pop()

	//Define duration frequency : better way
	frequency = ranges_event_list.map(item=>{
		return DATA_truncate_number(1/item)
	})

	//NOTE
	nzc_note_list.forEach(nzc_note=>{
		if (nzc_note <0){
			//silence
			note.push(nzc_note)
		} else {
			note.push(APP_note_to_Hz(nzc_note,TET))
		}
	})
	time.pop()//no end in midi_data only start and duration
	return {"time":time, "duration":frequency, "note_freq":note}
}

function DATA_calculate_exact_time(Psg_segment_start,Ps,Fs,N,D,N_NP,D_NP,current_PPM=PPM){//XXX
	var A = 60*(Psg_segment_start+Ps)*N_NP*D
	var B = 60*Fs*N_NP*N
	var value = (A+B)/(D_NP*D*current_PPM)
	return DATA_truncate_number(value)
}

function DATA_truncate_number(number){
	//var Number((number).toFixed(5))
	//XXX BETTER TRUNCATE FUNCTION
	return Math.trunc(number*Math.pow(10, 5))/Math.pow(10, 5) //66.66666 give error
}

//FRACTIONS
function DATA_modify_fraction_value(voice_index,segment_index,abs_position,fractComplex,fractSimple,fraction_number,voice_data){
	//return false if fraction value is not possible
	//return new voice data if ok
	var data = DATA_get_current_state_point(true)
	var position = abs_position
	var voice_data_mod = data.voice_data.find(item=>{
		return item.voice_index==voice_index
	})
	var all_segment_data= voice_data_mod.data.segment_data
	if(segment_index==null){
		//absolute position
		//need to calculate segment number and relative position
		segment_index=0
		var prev_segments_n_obj = 0
		all_segment_data.find(segment=>{
			var n_obj = segment.fraction.length
			if(prev_segments_n_obj+n_obj>abs_position){
				return true
			}else{
				//next segment
				prev_segments_n_obj+=n_obj //last element of a segment is .
				segment_index++
				return false
			}
		})
		position = abs_position - prev_segments_n_obj
	}

	var current_segment_data=all_segment_data[segment_index]

	//console.log(segment_index+"   "+fraction_index)
	var current_fraction = current_segment_data.fraction[position]

	if(isNaN(fractComplex) || isNaN(fractSimple) || fractSimple == 0 || fractComplex == 0)return false

	var range = current_fraction.stop-current_fraction.start
	var resto =  range%fractComplex

	if(resto==0){
		//all ok rewrite value in database
		//Other Cases
		var old_fractComplex = current_fraction.N
		var old_fractSimple = current_fraction.D
		current_fraction.N=fractComplex
		current_fraction.D=fractSimple
		//modify database pulses/note
		var tot_iT = range*fractSimple/fractComplex
		var start_range_index = current_segment_data.time.indexOf(current_segment_data.time.find(time=>{return time.P==current_fraction.start && time.F==0}))
		var end_range_index = current_segment_data.time.indexOf(current_segment_data.time.find(time=>{return time.P==current_fraction.stop && time.F==0}))

		//case range is empty
		if(start_range_index==end_range_index-1 && (current_segment_data.note[start_range_index]==-1 || current_segment_data.note[start_range_index]==-2)){
			voice_data_mod.data.midi_data=DATA_segment_data_to_midi_data(all_segment_data,voice_data_mod.neopulse)
			DATA_insert_new_state_point(data)
			DATA_load_state_point_data(false,true)
			return true
		}

		//calculate old iTs
		var iT_list = []
		var note_insert = []
		var prev_pulse=0
		var prev_fraction=0
		for (let i = start_range_index; i <= end_range_index ; i++) {
			var pulse = current_segment_data.time[i].P
			var fraction = current_segment_data.time[i].F

			//calculate the dT value to be written
			var value = (pulse - prev_pulse)  * old_fractSimple / old_fractComplex + (fraction - prev_fraction)
			iT_list.push(value)
			note_insert.push(current_segment_data.note[i])
			prev_pulse=pulse
			prev_fraction=fraction
		}
		//eliminate first iT
		iT_list.shift()
		//last element is "not part" or fractioning range
		//it is part of following range/segment/end of voice
		note_insert.pop()

		//adjust it list to new fractioning range
		var sum = iT_list.reduce((prop,item)=>{return item+prop},0)

		if(tot_iT>sum){
			//add a last it
			iT_list.push(tot_iT-sum)
			//add a last e
			note_insert.push(-2)
		}else if(tot_iT<sum){
			//take out its
			var position = 0
			var new_iT_list = []
			iT_list.forEach(iT=>{
				if(tot_iT>position){
					var partial_iT=tot_iT-position
					position+=iT
					//console.log("evaluating "+iT+"  "+partial_iT)
					if(iT<=partial_iT){
						new_iT_list.push(iT)
					}else{
						new_iT_list.push(partial_iT)
					}
				}else{
					//outside
				}
			})
			iT_list = new_iT_list
			//eliminate extra notes
			note_insert.splice(iT_list.length,(note_insert.length-iT_list.length))
		}

		//recalculate pulse time with new fractioning and iT_list XXX
		prev_pulse = current_fraction.start
		prev_fraction = 0
		var time_insert = [{"P":current_fraction.start,"F":0}]
		var diesis_insert = [true]
		iT_list.forEach(iT=>{
			var d_pulse = (Math.floor((prev_fraction+iT)/fractSimple))*fractComplex
			prev_pulse+=d_pulse
			prev_fraction = (prev_fraction+iT)%fractSimple
			time_insert.push({"P":prev_pulse,"F":prev_fraction})
			diesis_insert.push(true)
		})
		//last element is "not part" or fractioning range
		//it is part of following range/segment/end of voice
		time_insert.pop()
		diesis_insert.pop()
		//Change original data
		current_segment_data.time.splice(start_range_index,end_range_index-start_range_index,...time_insert)
		current_segment_data.note.splice(start_range_index,end_range_index-start_range_index,...note_insert)
		current_segment_data.diesis.splice(start_range_index,end_range_index-start_range_index,...diesis_insert)
		//recalculate midi_data
		voice_data_mod.data.midi_data=DATA_segment_data_to_midi_data(all_segment_data,voice_data_mod.neopulse)
		DATA_insert_new_state_point(data)
		DATA_load_state_point_data(false,true)
		return true
	}else {
		//show error and restore old value
		return false
	}
}

function DATA_break_fraction_range(voice_index,segment_index,current_fraction_index,new_fraction_list){
	//new_fractions are the substitutes of fraction[current_fraction_index]
	var data = DATA_get_current_state_point(true)
	var index_voice_to_change = -1
	data.voice_data.some(voice=>{
		index_voice_to_change++
		return voice.voice_index==voice_index
	})
	//verify fraction(s)
	var fraction_verificated = true
	var current_fraction = data.voice_data[index_voice_to_change].data.segment_data[segment_index].fraction[current_fraction_index]
	var frac_N = current_fraction.N
	var frac_D = current_fraction.D
	if(current_fraction.start!=new_fraction_list[0].start || current_fraction.stop!=new_fraction_list.slice(-1)[0].stop)fraction_verificated=false
	for (let i = 0; i < new_fraction_list.length-1; i++) {
		if(new_fraction_list[i].stop!=new_fraction_list[i+1].start)fraction_verificated=false
	}
	for (let i = 0; i < new_fraction_list.length; i++) {
		if(new_fraction_list[i].D!=frac_D)fraction_verificated=false
		if(new_fraction_list[i].N!=frac_N)fraction_verificated=false
	}

	if(!fraction_verificated){
		console.error("ERROR BREAKING FRACTIONS")
		console.log(current_fraction)
		console.log(new_fraction_list)
		return
	}

	data.voice_data[index_voice_to_change].data.segment_data[segment_index].fraction.splice(current_fraction_index,1,...new_fraction_list)
	//add pulse(s) in time if doesn t exist

	var recalc_midi = false

	for (let i = 0; i < new_fraction_list.length-1; i++) {
		var new_pulse = new_fraction_list[i].stop
		var found = data.voice_data[index_voice_to_change].data.segment_data[segment_index].time.some(time=>{return time.P==new_pulse && time.F==0})

		if(!found){
			//insert
			var index = data.voice_data[index_voice_to_change].data.segment_data[segment_index].time.indexOf(data.voice_data[index_voice_to_change].data.segment_data[segment_index].time.find(time=>{return time.P>=new_pulse}))
			data.voice_data[index_voice_to_change].data.segment_data[segment_index].time.splice(index,0,{"P":new_pulse,"F":0})
			data.voice_data[index_voice_to_change].data.segment_data[segment_index].note.splice(index,0,-2)
			data.voice_data[index_voice_to_change].data.segment_data[segment_index].diesis.splice(index,0,true)
			recalc_midi=true
		}
	}

	if(recalc_midi){
		data.voice_data[index_voice_to_change].data.midi_data=DATA_segment_data_to_midi_data(data.voice_data[index_voice_to_change].data.segment_data,data.voice_data[index_voice_to_change].neopulse)
	}

	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

//verify if is fractioned pulse first and then if is all ok breaking here,
//return true original fraction index, array new fraction ranges
function DATA_calculate_fraction_break_in_object_index(voice_index,segment_index,abs_position,is_A_T){
	//calculate if possible breaking fraction in position
	var data = DATA_get_current_state_point(true)
	var voice_data_mod = data.voice_data.find(item=>{
		return item.voice_index==voice_index
	})

	var position = abs_position
	var all_segment_data= voice_data_mod.data.segment_data
	if(segment_index==null){
		//ABSOLUTE position
		//need to calculate segment number and relative position
		segment_index=0
		var prev_segments_n_obj = 0
		all_segment_data.find(segment=>{
			var n_obj = segment.time.length -1
			if(prev_segments_n_obj+n_obj>abs_position){
				return true
			}else{
				//next segment
				prev_segments_n_obj+=n_obj //last element of a segment is .
				segment_index++
				return false
			}
		})
		position = abs_position - prev_segments_n_obj
	}

	var current_segment_data=all_segment_data[segment_index]

	if(is_A_T){
		//cutting a pulse
		//ok if F=0 and not a fraction range
		var time = current_segment_data.time[position]
		if(time.F!=0)return [false,]
		var fraction_range_index=-1
		var current_fraction_range = current_segment_data.fraction.find(range=>{
			fraction_range_index++
			return range.stop>time.P
		})

		if(current_fraction_range==null)return [false,]
		if(current_fraction_range.start==time.P)return [false,]

		var new_fraction_range_1 = {N:current_fraction_range.N,D:current_fraction_range.D,start:current_fraction_range.start,stop:time.P}
		var new_fraction_range_2 = {N:current_fraction_range.N,D:current_fraction_range.D,start:time.P,stop:current_fraction_range.stop}
		return [true,fraction_range_index,[new_fraction_range_1,new_fraction_range_2]]
	}else{
		//cutting a iT
		var timeA = current_segment_data.time[position]
		var timeB = current_segment_data.time[position+1]
		if(timeA.F!=0 || timeB.F!=0)return [false,]
		var fraction_range_index=-1
		var current_fraction_range = current_segment_data.fraction.find(range=>{
			fraction_range_index++
			return range.stop>timeA.P
		})
		if(current_fraction_range==null)return [false,]
		//if is entire interval
		if(current_fraction_range.start==timeA.P && current_fraction_range.stop==timeB.P)return [false,]

		//3 cases
		if(current_fraction_range.start==timeA.P){
			var new_fraction_range_1 = {N:current_fraction_range.N,D:current_fraction_range.D,start:current_fraction_range.start,stop:timeB.P}
			var new_fraction_range_2 = {N:current_fraction_range.N,D:current_fraction_range.D,start:timeB.P,stop:current_fraction_range.stop}
			return [true,fraction_range_index,[new_fraction_range_1,new_fraction_range_2]]
		}else if(current_fraction_range.stop==timeB.P){
			var new_fraction_range_1 = {N:current_fraction_range.N,D:current_fraction_range.D,start:current_fraction_range.start,stop:timeA.P}
			var new_fraction_range_2 = {N:current_fraction_range.N,D:current_fraction_range.D,start:timeA.P,stop:current_fraction_range.stop}
			return [true,fraction_range_index,[new_fraction_range_1,new_fraction_range_2]]
		}else{
			var new_fraction_range_1 = {N:current_fraction_range.N,D:current_fraction_range.D,start:current_fraction_range.start,stop:timeA.P}
			var new_fraction_range_2 = {N:current_fraction_range.N,D:current_fraction_range.D,start:timeA.P,stop:timeB.P}
			var new_fraction_range_3 = {N:current_fraction_range.N,D:current_fraction_range.D,start:timeB.P,stop:current_fraction_range.stop}
			return [true,fraction_range_index,[new_fraction_range_1,new_fraction_range_2,new_fraction_range_3]]
		}
	}
}

function DATA_join_fraction_ranges(voice_index,segment_index,first_fraction_index){
	var data = DATA_get_current_state_point(true)

	var index_voice_to_change = -1
	data.voice_data.some(voice=>{
		index_voice_to_change++
		return voice.voice_index==voice_index
	})

	var current_segment_data=data.voice_data[index_voice_to_change].data.segment_data[segment_index]

	//control if fract 1 compatible with new range, if not =1/1
	var start1=current_segment_data.fraction[first_fraction_index].start
	var stop1=current_segment_data.fraction[first_fraction_index].stop
	var start2=current_segment_data.fraction[first_fraction_index+1].start
	var stop2=current_segment_data.fraction[first_fraction_index+1].stop
	var N1 = current_segment_data.fraction[first_fraction_index].N
	var D1 = current_segment_data.fraction[first_fraction_index].D
	var N2 = current_segment_data.fraction[first_fraction_index+1].N
	var D2 = current_segment_data.fraction[first_fraction_index+1].D

	var new_start = start1
	var new_stop = stop2
	var new_range = new_stop-new_start
	var new_N = N1
	var new_D = D1
	var recalculate_range1_time = false
	var recalculate_range2_time = false

	if(new_range%new_N==0){
		//we can use the first fractioning
		if(N1==N2 && D1==D2){
			//mantaining all the pulses and notes
			//nothing to do
		}else{
			recalculate_range2_time=true
			//on second range
		}
	}else{
		//reset fractioning
		new_N = 1
		new_D = 1
		recalculate_range1_time=true
		recalculate_range2_time=true
		//on all the range
	}

	var iT_list = []
	var note_insert_list = []
	var diesis_insert_list = []
	var recalculate_start_range_index = null
	var recalculate_stop_range_index = null
	var recalculate_start=null
	var recalculate_stop=null

	if(recalculate_range2_time){
		//RECALCULATE USING IT
		//calculate old iTs
		var prev_pulse=0
		var prev_fraction=0

		//var tot_iT = range*fractSimple/fractComplex
		var start_range2_index = current_segment_data.time.indexOf(current_segment_data.time.find(time=>{return time.P==start2 && time.F==0}))
		var stop_range2_index = current_segment_data.time.indexOf(current_segment_data.time.find(time=>{return time.P==stop2 && time.F==0}))
		recalculate_start_range_index = start_range2_index
		recalculate_stop_range_index = stop_range2_index
		recalculate_start=start2
		recalculate_stop=stop2

		for (let i = start_range2_index; i <= stop_range2_index ; i++) {
			var pulse = current_segment_data.time[i].P
			var fraction = current_segment_data.time[i].F

			//calculate the dT value to be written
			var value = (pulse - prev_pulse) * D2 / N2 + (fraction - prev_fraction)
			iT_list.push(value)
			note_insert_list.push(current_segment_data.note[i])
			diesis_insert_list.push(current_segment_data.diesis[i])
			prev_pulse=pulse
			prev_fraction=fraction
		}
		//eliminate first iT
		iT_list.shift()
		//last element is "not part" or fractioning range
		//it is part of following range/segment/end of voice
		note_insert_list.pop()
		diesis_insert_list.pop()
	}

	if(recalculate_range1_time){
		var prev_pulse=0
		var prev_fraction=0

		var start_range1_index = current_segment_data.time.indexOf(current_segment_data.time.find(time=>{return time.P==start1 && time.F==0}))
		var stop_range1_index = current_segment_data.time.indexOf(current_segment_data.time.find(time=>{return time.P==stop1 && time.F==0}))
		recalculate_start_range_index = start_range1_index
		recalculate_start=start1

		var iT_list_1 = []
		var note_insert_list_1 = []
		var diesis_insert_list_1 = []

		for (let i = start_range1_index; i <= stop_range1_index ; i++) {
			var pulse = current_segment_data.time[i].P
			var fraction = current_segment_data.time[i].F

			//calculate the dT value to be written
			var value = (pulse - prev_pulse) * D1 / N1 + (fraction - prev_fraction)
			iT_list_1.push(value)
			note_insert_list_1.push(current_segment_data.note[i])
			diesis_insert_list_1.push(current_segment_data.diesis[i])
			prev_pulse=pulse
			prev_fraction=fraction
		}
		//eliminate first iT
		iT_list_1.shift()
		//last element is "not part" or fractioning range
		//it is part of following range/segment/end of voice
		note_insert_list_1.pop()
		diesis_insert_list_1.pop()

		//concat 2 lists
		iT_list=iT_list_1.concat(iT_list)
		note_insert_list=note_insert_list_1.concat(note_insert_list)
		diesis_insert_list=diesis_insert_list_1.concat(diesis_insert_list)
	}

	if(recalculate_range1_time || recalculate_range2_time){
		//adjust it list to new fractioning range
		var sum = iT_list.reduce((prop,item)=>{return item+prop},0)
		var range = recalculate_stop-recalculate_start
		var tot_iT = range*new_D/new_N

		if(tot_iT>sum){
			//add a last it
			iT_list.push(tot_iT-sum)
			//add a last e
			note_insert_list.push(-2)
			diesis_insert_list.push(true)
		}else if(tot_iT<sum){
			//take out its
			var position = 0
			var new_iT_list = []
			iT_list.forEach(iT=>{
				if(tot_iT>position){
					var partial_iT=tot_iT-position
					position+=iT
					//console.log("evaluating "+iT+"  "+partial_iT)
					if(iT<=partial_iT){
						new_iT_list.push(iT)
					}else{
						new_iT_list.push(partial_iT)
					}
				}else{
					//outside
				}
			})
			iT_list = new_iT_list
			//eliminate extra notes
			note_insert_list.splice(iT_list.length,(note_insert_list.length-iT_list.length))
			diesis_insert_list.splice(iT_list.length,(diesis_insert_list.length-iT_list.length))
		}


		//recalculate pulse time with new fractioning and iT_list
		prev_pulse = recalculate_start
		prev_fraction = 0
		var time_insert_list = [{"P":recalculate_start,"F":0}]
		iT_list.forEach(iT=>{
			var d_pulse = (Math.floor((prev_fraction+iT)/new_D))*new_N
			prev_pulse+=d_pulse
			prev_fraction = (prev_fraction+iT)%new_D
			time_insert_list.push({"P":prev_pulse,"F":prev_fraction})
		})
		//last element is "not part" or fractioning range
		//it is part of following range/segment/end of voice
		time_insert_list.pop()
		//Change original data
		current_segment_data.time.splice(recalculate_start_range_index,recalculate_stop_range_index-recalculate_start_range_index,...time_insert_list)
		current_segment_data.note.splice(recalculate_start_range_index,recalculate_stop_range_index-recalculate_start_range_index,...note_insert_list)
		current_segment_data.diesis.splice(recalculate_start_range_index,recalculate_stop_range_index-recalculate_start_range_index,...diesis_insert_list)
	}

	//change first range
	current_segment_data.fraction[first_fraction_index].N=new_N
	current_segment_data.fraction[first_fraction_index].D=new_D
	current_segment_data.fraction[first_fraction_index].stop=new_stop
	//delete second
	current_segment_data.fraction.splice(first_fraction_index+1,1)

	//overwrite midi_data
	data.voice_data[index_voice_to_change].data.midi_data=DATA_segment_data_to_midi_data(data.voice_data[index_voice_to_change].data.segment_data,data.voice_data[index_voice_to_change].neopulse)
	//console.log(data)
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

//ELEMENTS
function DATA_get_object_prev_note_index(voice_index,segment_index,abs_position){
	var data = DATA_get_current_state_point(true)
	var voice_data_mod = data.voice_data.find(item=>{
		return item.voice_index==voice_index
	})
	var all_segment_data= voice_data_mod.data.segment_data
	var current_segment_data=all_segment_data[segment_index]

	var position = abs_position
	if(segment_index==null){
		//ABSOLUTE position
		//need to calculate segment number and relative position
		segment_index=0
		var prev_segments_n_obj = 0
		all_segment_data.find(segment=>{
			var n_obj = segment.time.length -1
			if(prev_segments_n_obj+n_obj>abs_position){
				return true
			}else{
				//next segment
				prev_segments_n_obj+=n_obj //last element of a segment is .
				segment_index++
				return false
			}
		})
		position = abs_position - prev_segments_n_obj
	}

	return _find_prev_obj_position(position-1)

	function _find_prev_obj_position(position){
		if(position<0)return null
		var note = current_segment_data.note[position]
		if (note<0){
			return _find_prev_obj_position(position-1)
		}else{
			return position
		}
	}
}



//function return time, diesis, noteNa, N,D,N_NP,D_NP,isFrange(optional),compas(optional),scale(optional)
function DATA_get_object_index_info(voice_index,segment_index,abs_position,iT=0,getRange=false,getCompass=false,getScale=false){
	var data = DATA_get_current_state_point(true)
	var voice_data_mod = data.voice_data.find(item=>{
		return item.voice_index==voice_index
	})
	var all_segment_data= voice_data_mod.data.segment_data

	var position = abs_position
	if(segment_index==null){
		//ABSOLUTE position
		//need to calculate segment number and relative position
		segment_index=0
		var prev_segments_n_obj = 0
		all_segment_data.find(segment=>{
			var n_obj = segment.time.length -1
			if(prev_segments_n_obj+n_obj>abs_position){
				return true
			}else{
				//next segment
				prev_segments_n_obj+=n_obj //last element of a segment is .
				segment_index++
				return false
			}
		})
		position = abs_position - prev_segments_n_obj
	}
	var current_segment_data=all_segment_data[segment_index]
	var isRange=null
	var time=current_segment_data.time[position]
	var note=current_segment_data.note[position]
	var diesis=current_segment_data.diesis[position]
	var N_NP=voice_data_mod.neopulse.N
	var D_NP=voice_data_mod.neopulse.D
	var current_compas=null
	var current_scale=null

	var fraction = current_segment_data.fraction.find(fraction=>{return fraction.stop>time.P})
	if (typeof(fraction) == "undefined")fraction = null

	if(getRange){
		if(position==0){
			isRange=true
		}else{
			if(position==current_segment_data.time.length-1){
				isRange=true
				//fraction null bc is the end of segment
			}else{
				isRange = current_segment_data.fraction.some(fraction=>{
					return fraction.start==time.P && time.F==0
				})
			}
		}
	}
	var Psg_segment_start=0
	for (let i = 0; i < segment_index; i++) {
		Psg_segment_start+=all_segment_data[i].time.slice(-1)[0].P
	}

	if(getCompass && N_NP==1 && N_NP==1){
		var compas_data = data.compas
		var last_compas= compas_data.slice(-1)[0].compas_values
		var compas_length_value = last_compas[0]+last_compas[1]*last_compas[2]
		//it int_p inside a compas
		//take into account that pulse is part of a segment un x position

		//No problem using Pa, no compas if neopulse!!! Psg===Pa
		var Pa_element = Psg_segment_start + current_segment_data.time[position].P

		if(Pa_element <compas_length_value && compas_length_value!=0){
			//element inside a module
			var compas_type = compas_data.filter(compas=>{
				return compas.compas_values[0] <= Pa_element
			})

			//var current_compas_type = compas_type.pop()
			var current_compas_type = compas_type.pop()

			var current_compas_number = compas_type.reduce((prop,item)=>{
				return item.compas_values[2]+prop
			},0)

			var dP = Pa_element - current_compas_type.compas_values[0]
			var mod = current_compas_type.compas_values[1]
			var dC = Math.floor(dP/mod)
			var resto = dP
			if(dC!=0) resto = dP%mod
			current_compas_number+= dC

			current_compas={"":resto,"c":current_compas_number}
		}else{
			//outside or no compasses
		}
	}

	if(getScale){
		//find correct scale
		//find Pa_equivalent and verify if inside a scale
		var frac_p = 1
		if(fraction==null){
			//probably end segment
			console.error("ERROR: search for scale on end segment??")
		}else{
			frac_p = fraction.N/fraction.D
		}

		var Pa_equivalent = (Psg_segment_start+time.P) * N_NP/D_NP + (time.F+iT)* frac_p * N_NP/D_NP

		var scale_sequence= data.scale.scale_sequence
		var all_scale_length_value = scale_sequence.reduce((p,scale)=>{return parseInt(scale.duration)+p},0)

		if(Pa_equivalent <all_scale_length_value && all_scale_length_value!=0){
			//element inside a scale module
			var scale_end_p=0
			current_scale = scale_sequence.find(scale=>{
				scale_end_p += parseInt(scale.duration)
				return scale_end_p > Pa_equivalent
			})
		}else {
			//outside range scale
		}
	}
	return {time:time,note:note,diesis:diesis,fraction:fraction,N_NP:N_NP,D_NP:D_NP,isRange:isRange,compas:current_compas,scale:current_scale,Psg_segment_start:Psg_segment_start}
}


//enter single pulse (no duration)
function DATA_enter_new_object(voice_index, segment_index, pulse, fraction , note, diesis){
	if(note==-3 && pulse==0 && fraction==0)return

	var data = DATA_get_current_state_point(true)

	var voice_data_mod = data.voice_data.find(item=>{
		return item.voice_index==voice_index
	})

	var current_segment_data=voice_data_mod.data.segment_data[segment_index]
	if(current_segment_data.time.slice(-1)[0].P<=pulse){
		//console.log("next segment if exist" + voice_data_mod.data.segment_data.length)
		segment_index++
		if(segment_index>voice_data_mod.data.segment_data.length-1){
			//reached end of voice
			console.log("end of voice")
			return
		}
		current_segment_data=voice_data_mod.data.segment_data[segment_index]
		pulse=0
		fraction=0
	}
	//verify if is a new entry or a modification and in what position insert the thing
	var new_time = {"P":pulse,"F":fraction}
	var index_found=current_segment_data.time.indexOf(current_segment_data.time.find(time=>{return time.P==pulse && time.F==fraction}))

	//console.log(pulse+"."+fraction+"  found? "+found)
	if(index_found!=-1){
		//modify element if it is not the last one
		if(current_segment_data.note[index_found]==-4){
			//console.log("last one")
			return
		}
		current_segment_data.note[index_found]=note
	}else{
		//add element
		var index_next=current_segment_data.time.indexOf(current_segment_data.time.find(time=>{return time.P>pulse || (time.P==pulse && time.F>fraction)}))
		//console.log(index_next)
		if(index_next==-1){
			console.error("pulse not localized in database: "+pulse+"."+fraction)
			return
		}
		current_segment_data.time.splice(index_next,0,new_time)
		current_segment_data.note.splice(index_next,0,note)
		current_segment_data.diesis.splice(index_next,0,diesis)
	}

	//midi
	voice_data_mod.data.midi_data=DATA_segment_data_to_midi_data(voice_data_mod.data.segment_data,voice_data_mod.neopulse)
	//load
	DATA_insert_new_state_point(data)
	var mom = (JSON.parse(JSON.stringify(current_position_PMCRE)))
	DATA_load_state_point_data(false,true)
	//data load corrupt current_position
	current_position_PMCRE=mom
}

function DATA_get_object_index_note_scale(voice_index_segment_index,abs_position){//XXX OBSOLETE????? XXX XXX
	var data = DATA_get_current_state_point(false)
	var position = abs_position
	var voice_data_mod = data.voice_data.find(item=>{
		return item.voice_index==voice_index
	})
	var all_segment_data= voice_data_mod.data.segment_data
	if(segment_index==null){
		//absolute position
		//need to calculate segment number and relative position
		segment_index=0
		var prev_segments_n_obj = 0
		all_segment_data.find(segment=>{
			var n_obj = segment.time.length -1
			if(prev_segments_n_obj+n_obj>abs_position){
				return true
			}else{
				//next segment
				prev_segments_n_obj+=n_obj //last element of a segment is .
				segment_index++
				return false
			}
		})
		position = abs_position - prev_segments_n_obj
	}

	var current_segment_data=all_segment_data[segment_index]
	var time = current_segment_data.time[position]
	var note = current_segment_data.note[position]

	//determine the scale
	//XXX

	return [note,scale]
}


//enter single pulse (no duration) in position index =   (btw position-1 and position of current data)
//at iT 1 from previous position if P and F == null
//if P anf F /! null control if correct values
function DATA_enter_new_object_index(voice_index, segment_index, abs_position , P=null,F=null,note, diesis){
	var data = DATA_get_current_state_point(true)
	var position = abs_position
	var voice_data_mod = data.voice_data.find(item=>{
		return item.voice_index==voice_index
	})
	var all_segment_data= voice_data_mod.data.segment_data
	if(segment_index==null){
		//absolute position
		//need to calculate segment number and relative position
		segment_index=0
		var prev_segments_n_obj = 0
		all_segment_data.find(segment=>{
			var n_obj = segment.time.length -1
			if(prev_segments_n_obj+n_obj>abs_position){
				return true
			}else{
				//next segment
				prev_segments_n_obj+=n_obj //last element of a segment is .
				segment_index++
				return false
			}
		})
		position = abs_position - prev_segments_n_obj
	}

	//find if there is space after position
	var current_segment_data=all_segment_data[segment_index]
	if(current_segment_data.time.length>position && position!=0){
		if(P!=null &&F!=null){
			//find correct fraction range
			var current_fraction_range = current_segment_data.fraction.find(range=>{
				return range.stop>P //i m creating a new pulse, i m sure it is not a fraction range
			})

			var float_prev= current_segment_data.time[position-1].P+current_segment_data.time[position-1].F/100
			var float_next= current_segment_data.time[position].P+current_segment_data.time[position].F/100
			var float_current= P+F/100

			if(float_prev<float_current && float_current<float_next){
				//respect fractioning
				var rest = (P-current_segment_data.time[position-1].P) % current_fraction_range.N
				if(rest!=0){
					return false
				}
				//control if fractioned part is allowed
				if(F<current_fraction_range.D){
					var new_time = {"P":P,"F":F}
					current_segment_data.time.splice(position,0,new_time)
					current_segment_data.note.splice(position,0,note)
					current_segment_data.diesis.splice(position,0,diesis)

					//midi
					voice_data_mod.data.midi_data=DATA_segment_data_to_midi_data(all_segment_data,voice_data_mod.neopulse)
					//load
					DATA_insert_new_state_point(data)
					var mom = (JSON.parse(JSON.stringify(current_position_PMCRE)))
					DATA_load_state_point_data(false,true)
					//data load corrupt current_position
					current_position_PMCRE=mom

					return true
				}else {
					return false
				}
			} else {
				//outside range
				return false
			}
		}else{
			//put at iT=1
			//verify if there is space (it>2)
			//find correct fraction range
			var current_fraction_range = current_segment_data.fraction.find(range=>{
				//fraction_range_index++
				return range.stop>current_segment_data.time[position-1].P
			})

			var iT=DATA_calculate_interval_PA_PB(current_segment_data.time[position-1],current_segment_data.time[position],current_fraction_range)

			if(iT>1){
				//var new_time = {"P":pulse,"F":fraction}
				var new_time = DATA_calculate_next_position_PA_iT(current_segment_data.time[position-1],1,current_fraction_range)
				current_segment_data.time.splice(position,0,new_time)
				current_segment_data.note.splice(position,0,note)
				current_segment_data.diesis.splice(position,0,diesis)

				//midi
				voice_data_mod.data.midi_data=DATA_segment_data_to_midi_data(all_segment_data,voice_data_mod.neopulse)
				//load
				DATA_insert_new_state_point(data)
				var mom = (JSON.parse(JSON.stringify(current_position_PMCRE)))
				DATA_load_state_point_data(false,true)
				//data load corrupt current_position
				current_position_PMCRE=mom
				return true
			}else{
				return false
			}
		}
	}else {
		return false
	}
}

//MODIFY / DELETE objects
//this family of functions modify or delete existing object(s) starting from a given position
//work with voice index , segment index ,position
//POSITION IS GIVEN WITH THE INDEX OF THE OBJECT instead of pulse and fraction

function DATA_delete_object_index(voice_index,segment_index,abs_position){
	var data = DATA_get_current_state_point(true)
	var position = abs_position
	var voice_data_mod = data.voice_data.find(item=>{
		return item.voice_index==voice_index
	})
	var all_segment_data= voice_data_mod.data.segment_data
	if(segment_index==null){
		//absolute position
		//need to calculate segment number and relative position
		segment_index=0
		var prev_segments_n_obj = 0
		all_segment_data.find(segment=>{
			var n_obj = segment.time.length -1
			if(prev_segments_n_obj+n_obj>abs_position){
				return true
			}else{
				//next segment
				prev_segments_n_obj+=n_obj //last element of a segment is .
				segment_index++
				return false
			}
		})
		position = abs_position - prev_segments_n_obj
	}
	var current_segment_data=all_segment_data[segment_index]

	if(position==0 ){
		//e
		//var obj_list = [...document.querySelector("#App_PMC_selected_voice_sequence_container").querySelectorAll(".App_PMC_selected_voice_sequence_object")]
		//obj_list[abs_position].value=previous_value
		if(current_segment_data.note[position]==-2){
			//nothing to do
			return false
		}
		current_segment_data.note[position]=-2
	}else{
		//check if is part of a fractioning range
		var found = current_segment_data.fraction.some(fraction=>{
			return fraction.start==current_segment_data.time[position].P && current_segment_data.time[position].F==0
		})

		if(found){
			if(current_segment_data.note[position]==-2){
				//nothing to do
				return false
			}
			current_segment_data.note[position]=-2
		}else{
			current_segment_data.note.splice(position,1)
			current_segment_data.time.splice(position,1)
			current_segment_data.diesis.splice(position,1)
		}
	}

	voice_data_mod.data.midi_data=DATA_segment_data_to_midi_data(all_segment_data,voice_data_mod.neopulse)
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
	return true
}

//modify starting point of an object
function DATA_modify_object_index_time(voice_index,segment_index, abs_position, new_P,new_F){
	var data = DATA_get_current_state_point(true)
	var position = abs_position
	var voice_data_mod = data.voice_data.find(item=>{
		return item.voice_index==voice_index
	})
	var all_segment_data= voice_data_mod.data.segment_data
	if(segment_index==null){
		//absolute position
		//need to calculate segment number and relative position
		segment_index=0
		var prev_segments_n_obj = 0
		all_segment_data.find(segment=>{
			var n_obj = segment.time.length -1
			if(prev_segments_n_obj+n_obj>abs_position){
				return true
			}else{
				//next segment
				prev_segments_n_obj+=n_obj //last element of a segment is .
				segment_index++
				return false
			}
		})
		position = abs_position - prev_segments_n_obj
	}
	if(position==0){
		return [false,0,null]
	}
	var current_segment_data=all_segment_data[segment_index]

	//verify if is a fractioning range
	var old_time_data=current_segment_data.time[position]
	var isRange = false
	var current_fraction_index = null
	var current_fraction = current_segment_data.fraction.find((fraction,index)=>{
		//return fraction.start<old_time_data.P && fraction.stop>old_time_data.P
		current_fraction_index=index
		return fraction.stop>old_time_data.P
	})

	//console.log(current_fraction_index)
	if(old_time_data.F==0 && current_fraction.start==old_time_data.P){
		isRange = true
	}

	if(isRange){
		//verify fraction ==0
		if(new_F==0){
			//change fractioning ranges and internal data structure
			//find previous fraction range
			var prev_fraction = current_segment_data.fraction[current_fraction_index-1]

			var resto = (new_P-prev_fraction.start)%prev_fraction.N
			if(new_P<=prev_fraction.start || resto!=0){
				var error_T_indexA=0
				current_segment_data.time.find((time,index)=>{
					if(time.P==prev_fraction.start){
						error_T_indexA=index
						return true
					}
				})
				var error_T_indexB=0
				current_segment_data.time.find((time,index)=>{
					if(time.P==prev_fraction.stop){
						error_T_indexB=index
						return true
					}
				})
				return [false,1,{"error_Tf_index":current_fraction_index-1,"error_T_indexA":error_T_indexA,"error_T_indexB":error_T_indexB}]
			}

			resto = (current_fraction.stop-new_P)%current_fraction.N
			if(new_P>=current_fraction.stop || resto!=0){
				var error_T_indexA=0
				current_segment_data.time.find((time,index)=>{
					if(time.P==current_fraction.start){
						error_T_indexA=index
						return true
					}
				})
				var error_T_indexB=0
				current_segment_data.time.find((time,index)=>{
					if(time.P==current_fraction.stop){
						error_T_indexB=index
						return true
					}
				})
				return [false,2,{"error_Tf_index":current_fraction_index,"error_T_indexA":error_T_indexA,"error_T_indexB":error_T_indexB}]
			}

			//trim ranges data
			current_segment_data.fraction[current_fraction_index-1].stop=new_P
			current_segment_data.fraction[current_fraction_index].start=new_P

			//find index A and B
			var range1_start_index = -1
			current_segment_data.time.find((time,index)=>{
				if(time.P==current_segment_data.fraction[current_fraction_index-1].start && time.F==0){
					range1_start_index=index
					return true
				}
			})
			var range2_stop_index = -1
			current_segment_data.time.find((time,index)=>{
				if(time.P==current_segment_data.fraction[current_fraction_index].stop && time.F==0){
					range2_stop_index=index
					return true
				}
			})
			var cutFROM=-1
			var cutTO=-1

			for (let i = range1_start_index+1; i < position; i++) {
				if(current_segment_data.time[i].P>=new_P){
					cutFROM=i
					cutTO=position-1
					i=position
				}
			}

			if(cutFROM==-1){
				cutFROM=position+1
				for (let i = position+1; i <= range2_stop_index; i++) {
					var time_aprox= current_segment_data.time[i].P+current_segment_data.time[i].F/100

					if(time_aprox>new_P){
						cutTO=i-1
						i=range2_stop_index
					}
				}
			}

			current_segment_data.time[position].P=new_P
			current_segment_data.time[position].F=0
			current_segment_data.time.splice(cutFROM,(cutTO-cutFROM+1))
			current_segment_data.note.splice(cutFROM,(cutTO-cutFROM+1))
			current_segment_data.diesis.splice(cutFROM,(cutTO-cutFROM+1))

			//console.log(current_segment_data.time)
			//XXX maybe control L
		}else{
			return [false,0,null]
		}
	}else{
		//verify if P and F value is legit
		var rest = (new_P-current_fraction.start) % current_fraction.N
		if(rest!=0){
			//return [false,1,asdfasdfasdf]
			return [false,0,null]
		}
		//control if fractioned part is allowed
		if(new_F>=current_fraction.D){
			//return [false,1,asdfasdfasdf]
			return [false,0,null]
		}

		var prev_time_data = current_segment_data.time[position-1]
		var next_time_data = current_segment_data.time[position+1]

			//pulses like float numbers
		var new_value_float = parseFloat(new_P+"."+new_F)
		var previous_value_float = parseFloat(prev_time_data.P+"."+prev_time_data.F)
		var next_value_float = parseFloat(next_time_data.P+"."+next_time_data.F)
		if(previous_value_float<new_value_float && new_value_float<next_value_float){
			current_segment_data.time[position].P=new_P
			current_segment_data.time[position].F=new_F
		}else{
			return [false,0,null]
		}
	}

	//overwrite midi_data
	voice_data_mod.data.midi_data=DATA_segment_data_to_midi_data(all_segment_data,voice_data_mod.neopulse)
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
	return [true,,]
}

//mod if segment index==null //position is absolute position
function DATA_modify_object_index_note(voice_index,segment_index, abs_position, note_Na,diesis=null){
	var data = DATA_get_current_state_point(true)

	var position = abs_position
	var voice_data_mod = data.voice_data.find(item=>{
		return item.voice_index==voice_index
	})
	var all_segment_data= voice_data_mod.data.segment_data
	if(segment_index==null){
		//absolute position
		//need to calculate segment number and relative position
		segment_index=0
		var prev_segments_n_obj = 0
		all_segment_data.find(segment=>{
			var n_obj = segment.time.length -1
			if(prev_segments_n_obj+n_obj>abs_position){
				return true
			}else{
				//next segment
				prev_segments_n_obj+=n_obj //last element of a segment is .
				segment_index++
				return false
			}
		})
		position = abs_position - prev_segments_n_obj
	}

	//traduction already done
	//var note_Na=note_abs

	if(position==0 && note_Na==-3){
		return false
	}
	var max_note_number = TET * 7-1
	if(note_Na>max_note_number)note_Na=max_note_number

	all_segment_data[segment_index].note[position]=note_Na
	if(diesis!=null){
		//change it
		all_segment_data[segment_index].diesis[position]=diesis
	}

	//overwrite midi_data
	voice_data_mod.data.midi_data=DATA_segment_data_to_midi_data(all_segment_data,voice_data_mod.neopulse)
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
	return true
}

//mod it = change iT of a object (if 0 delete it)
function DATA_modify_object_index_iT(voice_index,segment_index,abs_position,duration_iT){
	
	var data = DATA_get_current_state_point(true)
	var voice_data_mod = data.voice_data.find(item=>{
		return item.voice_index==voice_index
	})

	var position = abs_position
	var all_segment_data= voice_data_mod.data.segment_data
	if(segment_index==null){
		//ABSOLUTE position
		//need to calculate segment number and relative position
		segment_index=0
		var prev_segments_n_obj = 0
		all_segment_data.find(segment=>{
			var n_obj = segment.time.length -1
			if(prev_segments_n_obj+n_obj>abs_position){
				return true
			}else{
				//next segment
				prev_segments_n_obj+=n_obj //last element of a segment is .
				segment_index++
				return false
			}
		})
		position = abs_position - prev_segments_n_obj
	}

	var current_segment_data=all_segment_data[segment_index]

	//find pulse and fraction start
	
	var pulse_start = current_segment_data.time[position].P
	var fraction_start = current_segment_data.time[position].F
	//find correct fraction range
	var current_fraction_range_index=0
	var current_fraction_range = current_segment_data.fraction.find((range,index)=>{
		if(range.stop>pulse_start){
			current_fraction_range_index=index
			return true
		}
		return false
	})

	//find index start and stop change modification
	//var index_start_mod = current_segment_data.time.indexOf(current_segment_data.time.find(time=>{return time.P==pulse_start && time.F==fraction_start}))
	var index_start_mod = position
	//console.log(index_start_mod)
	var index_stop_mod = current_segment_data.time.indexOf(current_segment_data.time.find(time=>{return time.P==current_fraction_range.stop && time.F==0}))-1
	//console.log(index_stop_mod)
	var old_iT_value = DATA_calculate_interval_PA_PB(current_segment_data.time[index_start_mod],current_segment_data.time[index_start_mod+1],current_fraction_range)
	var delta_iT = duration_iT-old_iT_value
	//console.log(delta_iT)
	if(duration_iT==0){
		//delete range
		//if only element in the range or the last...
		//console.log(index_stop_mod+"  "+index_start_mod)//the last
		if(index_stop_mod==index_start_mod){
			//it is the last element
			if(fraction_start==0 && pulse_start==current_fraction_range.start){
				//it is the only element
				//console.log("ERROR: trying to delete entire range")
				return [false,0,]
			}else{
				//delete object
				current_segment_data.time.splice(index_start_mod,1)
				current_segment_data.note.splice(index_start_mod,1)
				current_segment_data.diesis.splice(index_start_mod,1)
			}
		}else{
			//delete element and slide other objects
			for (var i = index_start_mod+1; i<=index_stop_mod; i++){
				current_segment_data.time[i] = DATA_calculate_next_position_PA_iT(current_segment_data.time[i],delta_iT,current_fraction_range)
			}
			//delete object
			current_segment_data.time.splice(index_start_mod,1)
			current_segment_data.note.splice(index_start_mod,1)
			current_segment_data.diesis.splice(index_start_mod,1)
		}
	}else{
		//modify element
		//check if I am editing the last value OF THE RANGE
		if(index_stop_mod==index_start_mod){
			//modify last element
			if(duration_iT>old_iT_value){
				//there is no space

				//calculating if the delta_iT make integer pulses AND if this pulses are a multiple of min_Li
				var resto_Fr = delta_iT%current_fraction_range.D
				if(resto_Fr==0){
					var delta_Ps = delta_iT*current_fraction_range.N/current_fraction_range.D
					var min_Li = DATA_calculate_minimum_Li()
					var min_Ls = min_Li * (voice_data_mod.neopulse.D/voice_data_mod.neopulse.N)
					var resto = delta_Ps%min_Ls
					if(resto==0){
						//1 : enlarge the segment (and other voices!)
						var new_Ls = current_segment_data.time.slice(-1)[0].P+delta_Ps
						data = DATA_calculate_change_Ls_segment(data,voice_index,segment_index,new_Ls)
						if(data==false){//not necess
							return [false,3,]
						}
						//2 : change pulse sequence in current segment

						for (let i = index_start_mod+1; i < current_segment_data.time.length-1; i++) {
							current_segment_data.time[i].P+=delta_Ps
						}

						//change fraction range of segment
						current_segment_data.fraction.forEach((fraction,index,array)=>{
							if(fraction.start>pulse_start){
								array[index].start+=delta_Ps
							}
							if(fraction.stop>pulse_start){
								array[index].stop+=delta_Ps
							}
						})

						//last one was already corrected
						current_segment_data.fraction[current_segment_data.fraction.length-1].stop-=delta_Ps
					}else{
						//console.log("no i can't!")
						return [false,3,]
					}
				}else{
					//console.log("no i can't!")
					return [false,2,{"error_Tf_index":current_fraction_range_index}]
				}
			}else{
				//create new element at the end of the range
				var new_time = DATA_calculate_next_position_PA_iT(current_segment_data.time[index_start_mod],duration_iT,current_fraction_range)
				current_segment_data.time.splice(index_start_mod+1,0,new_time)
				current_segment_data.note.splice(index_start_mod+1,0,-2)
				current_segment_data.diesis.splice(index_start_mod+1,0,true)
			}
		}else{
			// verify if there is space for a new interval length
			var last_iT_value = DATA_calculate_interval_PA_PB(current_segment_data.time[index_stop_mod],current_segment_data.time[index_stop_mod+1],current_fraction_range)
			//console.log("hello"+last_iT_value+"   " +delta_iT+"   ")
			//console.log(current_segment_data.time[index_stop_mod])
			if(last_iT_value>delta_iT){
				//recalculating new P values inside the range (not the last one)
				for (var i = index_start_mod+1; i<=index_stop_mod; i++){
					//var new_time = DATA_calculate_next_position_PA_iT(current_segment_data.time[i],delta_iT,current_fraction_range)
					current_segment_data.time[i] = DATA_calculate_next_position_PA_iT(current_segment_data.time[i],delta_iT,current_fraction_range)
				}
			}else{
				//blink error
				return [false,1,{"error_iT_index":index_stop_mod}]
			}
		}
	}
	voice_data_mod.data.midi_data=DATA_segment_data_to_midi_data(all_segment_data,voice_data_mod.neopulse)
	//load
	DATA_insert_new_state_point(data)

	var mom = (JSON.parse(JSON.stringify(current_position_PMCRE)))
	DATA_load_state_point_data(false,true)
	//data load corrupt current_position
	current_position_PMCRE=mom

	return [true,,]
}

function DATA_modify_object_index_iN(voice_index,segment_index,abs_position,value,isanote=true,diesis=null){
	var data = DATA_get_current_state_point(true)
	var voice_data_mod = data.voice_data.find(item=>{
		return item.voice_index==voice_index
	})
	var position = abs_position
	var all_segment_data= voice_data_mod.data.segment_data
	if(segment_index==null){
		//ABSOLUTE position
		//need to calculate segment number and relative position
		segment_index=0
		var prev_segments_n_obj = 0
		all_segment_data.find(segment=>{
			var n_obj = segment.time.length -1
			if(prev_segments_n_obj+n_obj>abs_position){
				return true
			}else{
				//next segment
				prev_segments_n_obj+=n_obj //last element of a segment is .
				segment_index++
				return false
			}
		})
		position = abs_position - prev_segments_n_obj
	}

	var current_segment_data=all_segment_data[segment_index]
	var iNa
	if(isanote){
		//is a iNa
		iNa=value
	}else{
		//is e , L or endrange
		if(value==-1)iNa="s"
		if(value==-2)iNa="e"
		if(value==-3){
			iNa=tie_intervals
			if(position==0)return false
		}
		if(value==-4)iNa=end_range
	}
	//calculate iNa segment
	var iNa_list=DATA_calculate_iNa_from_Na_list(current_segment_data.note)
	iNa_list[position]=iNa
	var new_Na_list= DATA_calculate_Na_from_iNa_list(iNa_list)
	current_segment_data.note=new_Na_list

	//if diesis != null change diesis
	if(diesis==true || diesis==false)current_segment_data.diesis[position]=diesis

	voice_data_mod.data.midi_data=DATA_segment_data_to_midi_data(all_segment_data,voice_data_mod.neopulse)
	var mom = (JSON.parse(JSON.stringify(current_position_PMCRE)))
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
	//data load current_position
	current_position_PMCRE=mom
	return true
}

function DATA_enter_new_object_index_iN(voice_index,segment_index,abs_position,value,isanote=true,diesis=true){
	var data = DATA_get_current_state_point(true)
	var voice_data_mod = data.voice_data.find(item=>{
		return item.voice_index==voice_index
	})
	var position = abs_position
	var all_segment_data= voice_data_mod.data.segment_data
	if(segment_index==null){
		//ABSOLUTE position
		//need to calculate segment number and relative position
		segment_index=0
		var prev_segments_n_obj = 0
		all_segment_data.find(segment=>{
			var n_obj = segment.time.length -1
			if(prev_segments_n_obj+n_obj>abs_position){
				return true
			}else{
				//next segment
				prev_segments_n_obj+=n_obj //last element of a segment is .
				segment_index++
				return false
			}
		})
		position = abs_position - prev_segments_n_obj
	}

	var current_segment_data=all_segment_data[segment_index]
	//find if there is space after position
	if(current_segment_data.time.length>position && position!=0){
		//verify if there is space (it>2)
		//find correct fraction range
		var current_fraction_range = current_segment_data.fraction.find(range=>{
			//fraction_range_index++
			return range.stop>current_segment_data.time[position-1].P
		})
		var iT=DATA_calculate_interval_PA_PB(current_segment_data.time[position-1],current_segment_data.time[position],current_fraction_range)
		if(iT>1){
			//var new_time = {"P":pulse,"F":fraction}
			var new_time = DATA_calculate_next_position_PA_iT(current_segment_data.time[position-1],1,current_fraction_range)
			current_segment_data.time.splice(position,0,new_time)
			current_segment_data.diesis.splice(position,0,diesis)

			var iNa_list=DATA_calculate_iNa_from_Na_list(current_segment_data.note)
			var iNa
			if(isanote){
				//is a iNa
				iNa=value
			}else{
				//is e , L or endrange
				if(value==-1)iNa="s"
				if(value==-2)iNa="e"
				if(value==-3)iNa=tie_intervals
				if(value==-4)iNa=end_range
			}
			iNa_list.splice(position,0,iNa)
			var new_Na_list= DATA_calculate_Na_from_iNa_list(iNa_list)
			current_segment_data.note=new_Na_list
			//midi
			voice_data_mod.data.midi_data=DATA_segment_data_to_midi_data(all_segment_data,voice_data_mod.neopulse)
			//load
			DATA_insert_new_state_point(data)
			var mom = (JSON.parse(JSON.stringify(current_position_PMCRE)))
			DATA_load_state_point_data(false,true)
			//data load corrupt current_position
			current_position_PMCRE=mom
			return true
		}else{
			return false
		}
	}else {
		return false
	}
}

function DATA_delete_object_index_iN(voice_index,segment_index,abs_position){
	var data = DATA_get_current_state_point(true)
	var voice_data_mod = data.voice_data.find(item=>{
		return item.voice_index==voice_index
	})
	var position = abs_position
	var all_segment_data= voice_data_mod.data.segment_data
	if(segment_index==null){
		//ABSOLUTE position
		//need to calculate segment number and relative position
		segment_index=0
		var prev_segments_n_obj = 0
		all_segment_data.find(segment=>{
			var n_obj = segment.time.length -1
			if(prev_segments_n_obj+n_obj>abs_position){
				return true
			}else{
				//next segment
				prev_segments_n_obj+=n_obj //last element of a segment is .
				segment_index++
				return false
			}
		})
		position = abs_position - prev_segments_n_obj
	}

	var current_segment_data=all_segment_data[segment_index]
	var iNa_list=DATA_calculate_iNa_from_Na_list(current_segment_data.note)

	if(position==0 ){
		if(iNa_list[position]=="e")return false //nothing to do
		iNa_list[position]="e"
	}else{
		//check if is part of a fractioning range
		var found = current_segment_data.fraction.some(fraction=>{
			return fraction.start==current_segment_data.time[position].P && current_segment_data.time[position].F==0
		})

		if(found){
			if(iNa_list[position]=="e")return false //nothing to do
			iNa_list[position]="e"
		}else{
			iNa_list.splice(position,1)
			current_segment_data.time.splice(position,1)
			current_segment_data.diesis.splice(position,1)
		}
	}

	var new_Na_list= DATA_calculate_Na_from_iNa_list(iNa_list)
	current_segment_data.note=new_Na_list
	voice_data_mod.data.midi_data=DATA_segment_data_to_midi_data(all_segment_data,voice_data_mod.neopulse)
	var mom = (JSON.parse(JSON.stringify(current_position_PMCRE)))
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
	//data load current_position
	current_position_PMCRE=mom
	return true
}

//NOT YET USED ::::: N  PMC AND OPERATIONS BETTER USE THIS ONE !!!!! XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX
function DATA_calculate_D_iNa_from_to_object_index(D_iNa,from_position,to_position,current_segment_data){
	var max_note_number = TET * 7-1
	for (let i = position; i <= to_position; i++) {
		var current_note = current_segment_data.note[i]
		if(current_note>=0){
			current_note = DATA_calculate_inRange(current_note+D_iNa,0,max_note_number)[0]
			current_segment_data.note[i]=current_note
		}
	}
	return current_segment_data
}


function DATA_calculate_iNa_from_Na_list(Na_list){
	var first_note=true
	var prev_note=null
	var iNa_list=[]
	Na_list.forEach((current_note,index)=>{
		if(current_note==-1){
			iNa_list.push("s")
		}else if(current_note==-2){
			iNa_list.push("e")
		}else if(current_note==-3){
			iNa_list.push((index==0)?"e":tie_intervals)//last resource, normally cast an error
		}else if(current_note==-4){
			iNa_list.push((index==Na_list.length-1)?end_range:"e")
		}else{
			if(first_note){
				iNa_list.push(current_note)
				first_note=false
				prev_note=current_note
			}else{
				iNa_list.push(current_note-prev_note)
				prev_note=current_note
			}
		}
	})
	return iNa_list
}

//ATT!!! iNa list use letters  "e" "L" end_range
function DATA_calculate_Na_from_iNa_list(iNa_list){
	var Na_list = []
	var index=0
	var first_element= true
	var current_note = 0
	var max_note_number = TET * 7-1
	var adjust_iNa=false
	iNa_list.forEach((current_iNa,index)=>{
		if(current_iNa=="s"){
			Na_list.push(-1)
		}else if(current_iNa=="e"){
			Na_list.push(-2)
		}else if(current_iNa==tie_intervals){
			Na_list.push((index==0)?-2:-3)
		}else if(current_iNa==end_range){
			Na_list.push((index==iNa_list.length-1)?-4:-3)
		}else{
			if(first_element){
				//first element is a note
				current_note= DATA_calculate_inRange(current_iNa,0,max_note_number)[0]
				//the first note
				first_element=false
			} else {
				//current_note += Math.floor(current_iNa)
				//current_note += current_iNa
				var inRange
				[current_note,inRange] = DATA_calculate_inRange(current_note+current_iNa,0,max_note_number)
				if(!inRange)adjust_iNa=true
			}
			Na_list.push(current_note)
		}
	})
	if(adjust_iNa){
		console.warn("note interval outside note range")
	}
	return Na_list
}

//operations with grades
function DATA_modify_object_index_iNgrade(voice_index,segment_index,abs_position,value,isanote=true,diesis=null){
	var data = DATA_get_current_state_point(true)
	var voice_data_mod = data.voice_data.find(item=>{
		return item.voice_index==voice_index
	})
	var position = abs_position
	var all_segment_data= voice_data_mod.data.segment_data
	if(segment_index==null){
		//ABSOLUTE position
		//need to calculate segment number and relative position
		segment_index=0
		var prev_segments_n_obj = 0
		all_segment_data.find(segment=>{
			var n_obj = segment.time.length -1
			if(prev_segments_n_obj+n_obj>abs_position){
				return true
			}else{
				//next segment
				prev_segments_n_obj+=n_obj //last element of a segment is .
				segment_index++
				return false
			}
		})
		position = abs_position - prev_segments_n_obj
	}

	var Psg_segment_start = 0
	for (let i = 0; i < segment_index; i++) {
		Psg_segment_start+=all_segment_data[i].time.slice(-1)[0].P
	}

	var N_NP = voice_data_mod.neopulse.N
	var D_NP = voice_data_mod.neopulse.D

	var current_segment_data=all_segment_data[segment_index]

	var current_Na = current_segment_data.note[position]
	var current_diesis = current_segment_data.diesis[position]
	var current_scale = _current_voice_segment_position_scale(position)
	if(current_scale==null)return false

	//find previous note and modified delta grade
	var prev_position=position-1
	var prev_scale = "NaN"
	if(prev_position<0){
		//simply mod object
	}else{
		while (prev_position>=0 && current_segment_data.note[prev_position]<0){
			prev_position--
		}
		if(prev_position<0){
			//simply mod object
			console.error("iN grade outside segment")
			return false
		}else{
			prev_scale=_current_voice_segment_position_scale(prev_position)
		}
	}

	if(isanote){
		if(JSON.stringify(prev_scale) != JSON.stringify(current_scale)){
			//is start of a new scale
			if(current_Na>=0){
				//changing a note, i want to mantain serie iNgm
				var [current_grade,current_delta,current_reg]=BNAV_absolute_note_to_scale(current_Na,current_diesis,current_scale)
				var [target_grade,current_delta,target_reg]=BNAV_absolute_note_to_scale(value,current_diesis,current_scale)
				var scale_module = BNav_get_scale_from_acronym(current_scale.acronym).module
				var d_grade = target_grade-current_grade+(target_reg-current_reg)*scale_module
				current_segment_data = DATA_calculate_D_iNgm_from_object_index(d_grade,position,current_scale,current_segment_data,Psg_segment_start,N_NP,D_NP)
			}else{
				current_segment_data.note[position]=value
			}
		}else{
			//is a iNg
			var d_grade = value
			var prev_Na = current_segment_data.note[prev_position]

			if(current_Na>=0){
				//need to recalculate all intervals
				var prev_diesis = current_segment_data.diesis[prev_position]
				var [grade_A,delta_A,reg_A]=BNAV_absolute_note_to_scale(prev_Na,prev_diesis,prev_scale)
				var [current_grade,current_delta,current_reg]=BNAV_absolute_note_to_scale(current_Na,current_diesis,current_scale)
				var scale_module = BNav_get_scale_from_acronym(current_scale.acronym).module
				var old_d_grade = current_grade-grade_A+(current_reg-reg_A)*scale_module
				d_grade-=old_d_grade
			}else{
				current_segment_data.note[position]=prev_Na
			}

			current_segment_data = DATA_calculate_D_iNgm_from_object_index(d_grade,position,current_scale,current_segment_data,Psg_segment_start,N_NP,D_NP)
		}
	}else{
		//verify if is the position 0 no L
		if(position==0 && value==-3){
			return false
		}
		//find scale element and scale prev element
		if(JSON.stringify(prev_scale) != JSON.stringify(current_scale)){
			//first object scale , change no problem
			current_segment_data.note[position]=value
		}else{
			if(current_Na<0){
				//not a note , change no problem
				current_segment_data.note[position]=value
			}else{
				//need to recalculate all intervals
				var prev_Na = current_segment_data.note[prev_position]
				var prev_diesis = current_segment_data.diesis[prev_position]
				var [grade_A,delta_A,reg_A]=BNAV_absolute_note_to_scale(prev_Na,prev_diesis,prev_scale)
				var [current_grade,current_delta,current_reg]=BNAV_absolute_note_to_scale(current_Na,current_diesis,current_scale)
				var scale_module = BNav_get_scale_from_acronym(current_scale.acronym).module
				var d_grade = current_grade-grade_A+(current_reg-reg_A)*scale_module
				current_segment_data.note[position]=value
				current_segment_data = DATA_calculate_D_iNgm_from_object_index(-d_grade,position,current_scale,current_segment_data,Psg_segment_start,N_NP,D_NP)
			}
		}
	}
	voice_data_mod.data.midi_data=DATA_segment_data_to_midi_data(all_segment_data,voice_data_mod.neopulse)

	var mom = (JSON.parse(JSON.stringify(current_position_PMCRE)))
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
	//data load current_position
	current_position_PMCRE=mom
	return true

	function _current_voice_segment_position_scale(pos){
		let _P = current_segment_data.time[pos].P
		let _F = current_segment_data.time[pos].F
		//find correct scale
		//find Pa_equivalent and verify if inside a scale
		let _fraction = current_segment_data.fraction.find(fraction=>{return fraction.stop>_P})
		let _frac_p = _fraction.N/_fraction.D
		let _Pa_equivalent = (Psg_segment_start+_P) * N_NP/D_NP + _F* _frac_p * N_NP/D_NP
		//console.log(Pa_equivalent)
		return DATA_get_Pa_eq_scale(_Pa_equivalent)
	}
}

function DATA_enter_new_object_index_iNgrade(voice_index,segment_index,abs_position,value,isanote=true,diesis=true){
	var data = DATA_get_current_state_point(true)
	var voice_data_mod = data.voice_data.find(item=>{
		return item.voice_index==voice_index
	})
	var position = abs_position
	var all_segment_data= voice_data_mod.data.segment_data
	if(segment_index==null){
		//ABSOLUTE position
		//need to calculate segment number and relative position
		segment_index=0
		var prev_segments_n_obj = 0
		all_segment_data.find(segment=>{
			var n_obj = segment.time.length -1
			if(prev_segments_n_obj+n_obj>abs_position){
				return true
			}else{
				//next segment
				prev_segments_n_obj+=n_obj //last element of a segment is .
				segment_index++
				return false
			}
		})
		position = abs_position - prev_segments_n_obj
	}

	var Psg_segment_start = 0
	for (let i = 0; i < segment_index; i++) {
		Psg_segment_start+=all_segment_data[i].time.slice(-1)[0].P
	}

	var N_NP = voice_data_mod.neopulse.N
	var D_NP = voice_data_mod.neopulse.D

	var current_segment_data=all_segment_data[segment_index]


	//find if there is space after position
	if(current_segment_data.time.length>position && position!=0){
		//verify if there is space (it>2)
		//find correct fraction range
		var current_fraction_range = current_segment_data.fraction.find(range=>{
			//fraction_range_index++
			return range.stop>current_segment_data.time[position-1].P
		})
		var iT=DATA_calculate_interval_PA_PB(current_segment_data.time[position-1],current_segment_data.time[position],current_fraction_range)
		if(iT>1){
			var new_time = DATA_calculate_next_position_PA_iT(current_segment_data.time[position-1],1,current_fraction_range)
			current_segment_data.time.splice(position,0,new_time)
			current_segment_data.diesis.splice(position,0,diesis)

			//add prev note and apply D iNgm
			var prev_position=position-1
			var prev_scale = "NaN"
			if(prev_position<0){
				console.error("Start of a segment: operation not allowed")
				return false
			}else{
				while (prev_position>=0 && current_segment_data.note[prev_position]<0){
					prev_position--
				}
				if(prev_position<0){
					//console.error("prev position / note not found")
					// return false
				}else{
					prev_scale=_current_voice_segment_position_scale(prev_position)
				}
			}
			//provv
			current_segment_data.note.splice(position,0,-1)
			var current_scale = _current_voice_segment_position_scale(position)
			if(current_scale==null)return false

			if(isanote){
				if(JSON.stringify(prev_scale) != JSON.stringify(current_scale)){
					//is start of a new scale not a iNgm
					//inserting a note, i want to mantain serie iNgm
					//current_segment_data.note.splice(position,0,value)
					current_segment_data.note[position]=value
				}else{
					var prev_Na = current_segment_data.note[prev_position]
					//current_segment_data.note.splice(position,0,prev_Na)
					current_segment_data.note[position]=prev_Na
					current_segment_data = DATA_calculate_D_iNgm_from_object_index(value,position,current_scale,current_segment_data,Psg_segment_start,N_NP,D_NP)
				}
			}else{
				//verify if is the position 0 no L
				if(position==0 && value==-3){
					//impossible
					return false
				}
				//insertion of a no note will not change the sequence
				//current_segment_data.note.splice(position,0,value)
				current_segment_data.note[position]=value
			}
		}else{
			return false
		}
	}else {
		return false
	}

	//midi
	voice_data_mod.data.midi_data=DATA_segment_data_to_midi_data(all_segment_data,voice_data_mod.neopulse)
	//load
	DATA_insert_new_state_point(data)
	var mom = (JSON.parse(JSON.stringify(current_position_PMCRE)))
	DATA_load_state_point_data(false,true)
	//data load corrupt current_position
	current_position_PMCRE=mom

	return true

	function _current_voice_segment_position_scale(pos){
		let _P = current_segment_data.time[pos].P
		let _F = current_segment_data.time[pos].F
		//find correct scale
		//find Pa_equivalent and verify if inside a scale
		let _fraction = current_segment_data.fraction.find(fraction=>{return fraction.stop>_P})
		let _frac_p = _fraction.N/_fraction.D
		let _Pa_equivalent = (Psg_segment_start+_P) * N_NP/D_NP + _F* _frac_p * N_NP/D_NP
		//console.log(Pa_equivalent)
		return DATA_get_Pa_eq_scale(_Pa_equivalent)
	}
}

function DATA_delete_object_index_iNgrade(voice_index,segment_index,abs_position){
	var data = DATA_get_current_state_point(true)
	var voice_data_mod = data.voice_data.find(item=>{
		return item.voice_index==voice_index
	})
	var position = abs_position
	var all_segment_data= voice_data_mod.data.segment_data
	if(segment_index==null){
		//ABSOLUTE position
		//need to calculate segment number and relative position
		segment_index=0
		var prev_segments_n_obj = 0
		all_segment_data.find(segment=>{
			var n_obj = segment.time.length -1
			if(prev_segments_n_obj+n_obj>abs_position){
				return true
			}else{
				//next segment
				prev_segments_n_obj+=n_obj //last element of a segment is .
				segment_index++
				return false
			}
		})
		position = abs_position - prev_segments_n_obj
	}

	var Psg_segment_start = 0
	for (let i = 0; i < segment_index; i++) {
		Psg_segment_start+=all_segment_data[i].time.slice(-1)[0].P
	}

	var N_NP = voice_data_mod.neopulse.N
	var D_NP = voice_data_mod.neopulse.D

	var current_segment_data=all_segment_data[segment_index]

	var found = current_segment_data.fraction.some(fraction=>{
		return fraction.start==current_segment_data.time[position].P && current_segment_data.time[position].F==0
	})

	var current_Na = current_segment_data.note[position]
	var current_diesis = current_segment_data.diesis[position]
	var current_scale = _current_voice_segment_position_scale(position)
	if(current_scale==null)return false

	var d_grade = 0
	if(current_Na>=0){
		//find current interval and use it in order to modify other intervals
		var [current_grade,current_delta,current_reg]=BNAV_absolute_note_to_scale(current_Na,current_diesis,current_scale)
		var scale_module = BNav_get_scale_from_acronym(current_scale.acronym).module

		//find previous note and deleted delta grade
		var prev_position=position-1
		if(prev_position<0){
			//simply delete object (not necess, control already done? in RE!)
			console.error("simply try delete it")

		}else{
			while (prev_position>=0 && current_segment_data.note[prev_position]<0){
				prev_position--
			}
			if(prev_position<0){
				//simply delete object (not necess, control already done? in RE!)
				console.error("simply try delete it")
			}else{
				var prev_scale=_current_voice_segment_position_scale(prev_position)
				if(JSON.stringify(prev_scale) != JSON.stringify(current_scale)){
					//delete bc first note of the scale , second note will be new first note
					return DATA_delete_object_index(voice_index,segment_index,abs_position)
				}
				var prev_Na = current_segment_data.note[prev_position]
				var prev_diesis = current_segment_data.diesis[prev_position]
				var [grade_A,delta_A,reg_A]=BNAV_absolute_note_to_scale(prev_Na,prev_diesis,prev_scale)
				var d_grade = current_grade-grade_A+(current_reg-reg_A)*scale_module
			}
		}
	}

	if(found){
		if(current_segment_data.note[position]==-2){
			//nothing to do
			return false
		}
		current_segment_data = DATA_calculate_D_iNgm_from_object_index(-d_grade,position,current_scale,current_segment_data,Psg_segment_start,N_NP,D_NP)

		current_segment_data.note[position]=-2
	}else{
		current_segment_data = DATA_calculate_D_iNgm_from_object_index(-d_grade,position,current_scale,current_segment_data,Psg_segment_start,N_NP,D_NP)
		current_segment_data.note.splice(position,1)
		current_segment_data.time.splice(position,1)
		current_segment_data.diesis.splice(position,1)
	}
	//save
	voice_data_mod.data.midi_data=DATA_segment_data_to_midi_data(all_segment_data,voice_data_mod.neopulse)
	var mom = (JSON.parse(JSON.stringify(current_position_PMCRE)))
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
	//data load current_position
	current_position_PMCRE=mom
	return true

	function _current_voice_segment_position_scale(pos){
		let _P = current_segment_data.time[pos].P
		let _F = current_segment_data.time[pos].F
		//find correct scale
		//find Pa_equivalent and verify if inside a scale
		let _fraction = current_segment_data.fraction.find(fraction=>{return fraction.stop>_P})
		let _frac_p = _fraction.N/_fraction.D
		let _Pa_equivalent = (Psg_segment_start+_P) * N_NP/D_NP + _F* _frac_p * N_NP/D_NP
		//console.log(Pa_equivalent)
		return DATA_get_Pa_eq_scale(_Pa_equivalent)
	}
}

function DATA_calculate_D_iNgm_from_object_index(d_grade,position,current_scale,current_segment_data,Psg_segment_start,N_NP,D_NP){
	//var target_position = position+1 change FROM position
	var target_position = position
	var target_scale = _current_voice_segment_position_scale(target_position)
	var max_pos=current_segment_data.note.length-1
	while (JSON.stringify(target_scale) == JSON.stringify(current_scale)) {
		//calculating iNg
		var target_Na = current_segment_data.note[target_position]
		if(target_Na>=0){
			//need to recalc note
			var target_diesis = current_segment_data.diesis[target_position]//no change???
			var [target_grade,target_delta,target_reg]=BNAV_absolute_note_to_scale(target_Na,target_diesis,target_scale)
			target_grade+=d_grade
			var new_target_Na = BNAV_scale_note_to_absolute(target_grade,0,target_reg,target_scale)
			current_segment_data.note[target_position]=new_target_Na
			//current_segment_data.diesis[target_position]=true//no change??
		}
		//calculating new target note

		//find next target scale
		target_position++
		if(target_position>=max_pos){
			target_scale="EXITNOW"
		}else{
			target_scale= _current_voice_segment_position_scale(target_position)
		}
	}

	return current_segment_data

	function _current_voice_segment_position_scale(pos){
		let _P = current_segment_data.time[pos].P
		let _F = current_segment_data.time[pos].F
		//find correct scale
		//find Pa_equivalent and verify if inside a scale
		let _fraction = current_segment_data.fraction.find(fraction=>{return fraction.stop>_P})
		let _frac_p = _fraction.N/_fraction.D
		let _Pa_equivalent = (Psg_segment_start+_P) * N_NP/D_NP + _F* _frac_p * N_NP/D_NP
		//console.log(Pa_equivalent)
		return DATA_get_Pa_eq_scale(_Pa_equivalent)
	}
}

//mod iA XXX




//OVERWRITE
//this family of functions write over existing object(s) starting from a given position
//work with voice index , segment index , pulse position (s) and or it, notes

//overwrite note from -> to cutting whatever exist
function DATA_overwrite_new_object_PA_PB(voice_index,segment_index,pulse_start,fraction_start,pulse_end,fraction_end,note,diesis){
	//verify it is not L on 0.0
	if(note==-3 && pulse_start==0 && fraction_start==0)return

	var data = DATA_get_current_state_point(true)

	var voice_data_mod = data.voice_data.find(item=>{
		return item.voice_index==voice_index
	})

	var current_segment_data=voice_data_mod.data.segment_data[segment_index]

	/* XXX control it exist??? XXX   XXX
	if(current_segment_data.time.slice(-1)[0].P<=pulse){
		//console.log("next segment if exist" + voice_data_mod.data.segment_data.length)
		segment_index++
		if(segment_index>voice_data_mod.data.segment_data.length-1){
			//reached end of voice
			console.log("end of voice")
			return
		}
		current_segment_data=voice_data_mod.data.segment_data[segment_index]
		pulse=0
		fraction=0
	}
	*/

	//find how many fraction ranges are involved
	var fraction_changes = 0
	var starting_fraction_range_index=0
	current_segment_data.fraction.find((fraction_range,fraction_range_index)=>{
			if(fraction_range.start <= pulse_start)starting_fraction_range_index=fraction_range_index
			fraction_changes = fraction_range_index - starting_fraction_range_index
			return fraction_range.stop >= pulse_end+fraction_end/100//valid aproximation
			//return fraction_range.stop >= pulse_end+fraction_end//error
		}
	)

	var note_insert = []
	var time_insert = []
	var diesis_insert = []
	time_insert.push({P:pulse_start,F:fraction_start})
	note_insert.push(note)
	diesis_insert.push(diesis)
	for (let i = 0; i < fraction_changes; i++) {
		var fraction_range = current_segment_data.fraction[starting_fraction_range_index+i+1]
		time_insert.push({P:fraction_range.start,F:0})
		//console.log("insert pulse "+fraction_range.start)
		note_insert.push(-3)
		diesis_insert.push(diesis)
	}

	//find index position start
	var index_start = 0
	var index_insert = 0

	current_segment_data.time.find(time=>{
		if(time.P==pulse_start && time.F==fraction_start){
			//exact
			index_insert=index_start
			return true
		}
		if(time.P>pulse_start || (time.P==pulse_start && time.F>fraction_start)){
			//need to create a new object silence
			index_insert=index_start
			index_start--
			return true
		}
		index_start++
	})

	//find index position end: if exist nothing , if not add pulse + silence
	var index_end = 0
	current_segment_data.time.find(time=>{
		if(time.P==pulse_end && time.F==fraction_end){
			//exact
			//index_end--
			return true
		}
		if(time.P>pulse_end || (time.P==pulse_end && time.F>fraction_end)){
			//need to create a new object silence
			time_insert.push({P:pulse_end,F:fraction_end})
			note_insert.push(-1)
			diesis_insert.push(diesis)
			return true
		}
		index_end++
	})

	// console.log("index_start "+index_start+" , index_end "+index_end+" , index_insert "+index_insert)

	var array_cut =(index_end-index_insert)
	// console.log("cutting N "+array_cut+" elements")

	if(array_cut<0)array_cut=0
	//console.log(time_insert)
	//console.log(note_insert)

	current_segment_data.time.splice(index_insert,array_cut,...time_insert)
	current_segment_data.note.splice(index_insert,array_cut,...note_insert)
	current_segment_data.diesis.splice(index_insert,array_cut,...diesis_insert)
	// console.log(current_segment_data.time)
	// console.log(current_segment_data.note)

	voice_data_mod.data.midi_data=DATA_segment_data_to_midi_data(voice_data_mod.data.segment_data,voice_data_mod.neopulse)
	//load
	DATA_insert_new_state_point(data)

	var mom = (JSON.parse(JSON.stringify(current_position_PMCRE)))
	DATA_load_state_point_data(false,true)
	//data load corrupt current_position
	current_position_PMCRE=mom
}

////overwrite note from pulse to iT cutting whatever exist
function DATA_overwrite_new_object_PA_iT(voice_index,segment_index,pulse_start,fraction_start,duration_iT,note,diesis=true){
	var data = DATA_get_current_state_point(true)
	//console.log("insert in voice_index: "+voice_index+" segment "+segment_index+" position: "+pulse_start+"."+fraction_start+"  iT"+duration_iT)

	var voice_data_mod = data.voice_data.find(item=>{
		return item.voice_index==voice_index
	})
	
	var current_segment_data=voice_data_mod.data.segment_data[segment_index]
	//control if positioned in the last pulse of a segment
	if(current_segment_data.time.slice(-1)[0].P<=pulse_start){
		//console.log("next segment if exist" + voice_data_mod.data.segment_data.length)
		segment_index++
		if(segment_index>voice_data_mod.data.segment_data.length-1){
			//reached end of voice
			//console.log("end of voice")
			return
		}
		current_segment_data=voice_data_mod.data.segment_data[segment_index]
		pulse_start=0
		fraction_start=0
	}

	var pulse_end= pulse_start
	var fraction_end = fraction_start

	if(duration_iT==0){
		//console.log("to the next element")
		DATA_enter_new_object(voice_index, segment_index, pulse_start, fraction_start, note,diesis)
		//position accordingly
		// console.log(current_position_PMCRE)
		// console.log(pulse_start)
		current_segment_data.time.find(position=>{
			if(position.P>pulse_start || (position.P==pulse_start && position.F>fraction_start)){
				console.log(position)
				pulse_end = position.P
				fraction_end = position.F
				return true
			}
		})
		
	}else{
		//calculate pulse_end,fraction_end
		var fraction_range_index= -1
		var current_fraction_range = current_segment_data.fraction.find(range=>{
			fraction_range_index++
			return range.stop>pulse_start
		})

		//console.log(current_fraction_range)
		//look how many iT are left from current position
		var iT_available = calc_iT_available_fract(current_segment_data.fraction[fraction_range_index],pulse_start,fraction_start)//function

		var end_of_segment=false
		var fraction_start_provv = fraction_start
		while (iT_available<duration_iT) {
			pulse_end=current_fraction_range.stop
			fraction_end=0
			fraction_start_provv=0

			//recalc iT_available and duration_iT
			duration_iT-=iT_available
			if(fraction_range_index<current_segment_data.fraction.length-1){
				fraction_range_index++
			}else{
				//console.error("no more space")
				end_of_segment=true
				break
			}
			current_fraction_range=current_segment_data.fraction[fraction_range_index]
			iT_available=calc_iT_available_fract(current_fraction_range,pulse_end,fraction_end)
		}
		//console.log("end in "+pulse_end+"."+fraction_end)
		function calc_iT_available_fract(f_range,P,F){
			return (f_range.stop-P)*f_range.D/f_range.N - F
		}

		if(!end_of_segment){
			fraction_end=(fraction_start_provv+duration_iT)%current_fraction_range.D
			//console.log(fraction_start_provv)
			//console.log(current_fraction_range)
			//console.log((fraction_start_provv+duration_iT-fraction_end)*current_fraction_range.N/current_fraction_range.D)
			pulse_end+= (fraction_start_provv+duration_iT-fraction_end)*current_fraction_range.N/current_fraction_range.D
		}

		//console.log("end in "+pulse_end+"."+fraction_end)
		DATA_overwrite_new_object_PA_PB(voice_index,segment_index,pulse_start,fraction_start,pulse_end,fraction_end, note,diesis)
	}

	//position accordingly ///XXX NOT HERE !!!!!!!!!!!!
	var segment_end_P_abs=0
	var segment_start_P_abs=0
	var Psg_segment_end = 0
	var Psg_segment_start = 0
	var N_NP=voice_data_mod.neopulse.N
	var D_NP=voice_data_mod.neopulse.D

	var index=-1
	voice_data_mod.data.segment_data.find(segment=>{
		segment_start_P_abs=segment_end_P_abs
		//NEOPULSE correction
		segment_end_P_abs +=segment.time.slice(-1)[0].P * N_NP / D_NP
		Psg_segment_start=Psg_segment_end
		Psg_segment_end +=segment.time.slice(-1)[0].P
		index++
		return index==segment_index
	})
	//console.log(segment_start_P_abs)
	// var pulseM = segment_start_PM + pulse_end*N/D

	var fraction_range_index= -1
	var current_fraction_range = current_segment_data.fraction.find(range=>{
		fraction_range_index++
		return range.stop>pulse_end
	})

	//console.log(current_fraction_range)
	if(current_fraction_range==null){
		//end segment
		current_fraction_range= {N:1,D:1}
	}

	var pulse_abs=segment_start_P_abs + pulse_end*N_NP/D_NP
	var time = DATA_calculate_exact_time(Psg_segment_start,pulse_end,fraction_end,current_fraction_range.N,current_fraction_range.D,N_NP,D_NP)
	//console.log(voice_index+"  "+segment_index+" P "+pulse_end+" F "+fraction_end+" Pabs "+pulse_abs+" time "+time)
	APP_player_to_time(time, pulse_abs,pulse_end,fraction_end,segment_index,voice_index)
}

function DATA_overwrite_new_object_list(voice_index,segment_index,pulse_start,fraction_start,note_list,diesis_list){//XXX

}

//INSERT
//this family of functions create a new object in a given position PUSHING existing object
//work with voice index , segment index , pulse position (s) and or it, notes
function DATA_insert_new_object_index_iT(voice_index,segment_index,abs_position,duration_iT,note,diesis=true){
	var data = DATA_get_current_state_point(true)
	var voice_data_mod = data.voice_data.find(item=>{
		return item.voice_index==voice_index
	})

	var position = abs_position
	var all_segment_data= voice_data_mod.data.segment_data
	if(segment_index==null){
		//ABSOLUTE position
		//need to calculate segment number and relative position
		segment_index=0
		var prev_segments_n_obj = 0
		all_segment_data.find(segment=>{
			var n_obj = segment.time.length -1
			if(prev_segments_n_obj+n_obj>abs_position){
				return true
			}else{
				//next segment
				prev_segments_n_obj+=n_obj //last element of a segment is .
				segment_index++
				return false
			}
		})
		position = abs_position - prev_segments_n_obj
	}
	var current_segment_data=all_segment_data[segment_index]
	//verify position is not end of segment
	if(current_segment_data.time.length-1<=position){
		return [false,0,]
	}
	//find position time
	//var starting_time= current_segment_data.time[position]
	//a copy of the data
	var starting_time= {"P":current_segment_data.time[position].P,"F":current_segment_data.time[position].F}
	//find fractioning range
	var current_fraction_range = current_segment_data.fraction.find(range=>{
		return range.stop>starting_time.P
	})
	//find if there is space to place new object
	var last_iT_value = 0
	var last_iT_index = 0
	current_segment_data.time.find((time,index,array)=>{
		if(time.P ==current_fraction_range.stop && time.F == 0){
			last_iT_index=index
			last_iT_value = (time.P - array[index-1].P)*(current_fraction_range.D/current_fraction_range.N)-array[index-1].F//time.F=0 so not needed
			return true
		}
	})

	if(last_iT_value>duration_iT){
		//add new value and change others
		var position_time_aprox = starting_time.P+ starting_time.F/100
		var provv_next_time = DATA_calculate_next_position_PA_iT(starting_time,duration_iT,current_fraction_range)

		current_segment_data.time.find((time,index,array)=>{
			//if(time.P ==current_fraction_range.stop && time.F == 0){
			if(time.P ==current_fraction_range.stop){
				return true
			}
			var time_aprox = time.P+ time.F/100
			if(time_aprox>=position_time_aprox){
				var current_iT_value = DATA_calculate_interval_PA_PB(time,array[index+1],current_fraction_range)
				array[index].P=provv_next_time.P
				array[index].F=provv_next_time.F
				//recalculate new next P and F
				provv_next_time = DATA_calculate_next_position_PA_iT(array[index],current_iT_value,current_fraction_range)
			}
		})
		//insert element
		current_segment_data.time.splice(position,0,starting_time)
		current_segment_data.note.splice(position,0,note)
		current_segment_data.diesis.splice(position,0,diesis)
		voice_data_mod.data.midi_data=DATA_segment_data_to_midi_data(voice_data_mod.data.segment_data,voice_data_mod.neopulse)
		//load
		DATA_insert_new_state_point(data)

		var mom = (JSON.parse(JSON.stringify(current_position_PMCRE)))
		DATA_load_state_point_data(false,true)
		//data load corrupt current_position
		current_position_PMCRE=mom

		return [true,,]
	}else{
		return [false,1,{"error_iT_index":last_iT_index}]
	}
}

function DATA_calculate_next_position_PA_iT(timeA,iT,fraction){
	var next_F=timeA.F+iT
	var rest = next_F%fraction.D
	var delta_P = Math.floor(next_F/fraction.D)*fraction.N
	var next_P=timeA.P+delta_P
	next_F= (rest>=0)? rest : (fraction.D+rest)//this way work for negative iT
	//sometime next_F= -0 !!!
	next_F = Math.abs(next_F)
	//return [next_P,next_F]
	return {"P":next_P,"F":next_F}
}

function DATA_calculate_interval_PA_PB(timeA,timeB,fraction){
	return (timeB.P - timeA.P)  * fraction.D / fraction.N + (timeB.F - timeA.F)
}

function DATA_get_compas_sequence(){
	var data = DATA_get_current_state_point(false)
	//read new compas sequence
	return JSON.parse(JSON.stringify(data.compas))
}

function DATA_get_scale_data(){
	var data = DATA_get_current_state_point(false)
	//read new compas sequence
	return JSON.parse(JSON.stringify(data.scale))
}

function DATA_get_scale_sequence(){
	var data = DATA_get_current_state_point(false)
	//read new compas sequence
	return JSON.parse(JSON.stringify(data.scale.scale_sequence))
}

function DATA_get_idea_scale_list(){
	var data = DATA_get_current_state_point(false)
	return JSON.parse(JSON.stringify(data.scale.idea_scale_list))
}

function DATA_get_Pa_eq_scale(time_p){
	//time_p is the equivalent aprox ABSOLUTE PULSE
	var scale_sequence= DATA_get_scale_sequence()

	var all_scale_length_value = scale_sequence.reduce((p,scale)=>{return parseInt(scale.duration)+p},0)

	if(time_p <all_scale_length_value && all_scale_length_value!=0){
		//element inside a scale module
		var scale_end_p=0
		var current_scale = scale_sequence.find(scale=>{
			scale_end_p += parseInt(scale.duration)
			return scale_end_p > time_p
		})
		return current_scale
	}else {
		//outside range scale
		return null
	}
}

function DATA_list_voice_data_segment_change(voice_data){
	var time_segment_change= []
	var data = (JSON.parse(JSON.stringify(voice_data.data.segment_data)))
	var pre = 0
	time_segment_change = data.map(segment=>{
		if(segment==null)return
		var P = segment.time.pop().P
		var value = DATA_calculate_exact_time(pre,P,0,1,1,voice_data.neopulse.N,voice_data.neopulse.D)
		pre += P
		return value
	})
	time_segment_change.unshift(0)
	time_segment_change.pop()
	return time_segment_change
}

function DATA_list_voice_data_metro_fractioning(voice_data){
	//var fractioning = [{"time": "", "freq":"", "rep":""}]
	var fractioning = []
	var data = (JSON.parse(JSON.stringify(voice_data.data.segment_data)))
	var fractioning_data_raw = []
	data.forEach(segment=>{
		segment.fraction.forEach(F_range_raw=>{
			fractioning_data_raw.push(F_range_raw)
		})
	})

	var D_NP = voice_data.neopulse.D
	var N_NP = voice_data.neopulse.N
	var prev_time_range_end = 0
	fractioning = fractioning_data_raw.map(raw=>{
		//calcolate starting time
		var time = DATA_calculate_exact_time(prev_time_range_end,0,0,1,1,N_NP,D_NP)
		//actualize range and next fractioning
		var range = raw.stop-raw.start
		prev_time_range_end += range
		var time2 = DATA_calculate_exact_time(prev_time_range_end,0,0,1,1,N_NP,D_NP)
		var rep = range* raw.D/raw.N
		var freq = rep/(time2-time)
		return {"time":time, "freq":freq, "rep":rep}
	})

	return fractioning
}

















