function APP_download_frequency_to_percentage(frequency){
	let f_princ = 261.62
	let f_princ_reg = 4
	let f_first_note = f_princ/Math.pow(2,f_princ_reg)
	let f_last_note = Math.pow(2, (83-(f_princ_reg*12))/12) * f_princ
	let value_0 = Math.log(f_first_note)/Math.log(f_last_note)
	// var f_princ = 261.62
	// var f_princ_reg = 4
	// var f_first_note = f_princ/Math.pow(2,f_princ_reg)
	// var f_last_note = Math.pow(2, (83-(f_princ_reg*12))/12) * f_princ
	var value = Math.log(frequency)/Math.log(f_last_note)
	//var value_0 = Math.log(f_first_note)/Math.log(f_last_note)

	var linear = (value- value_0)/(1-value_0)
	return linear
}


function APP_download_minimap_jpg_for_web_component(datatest){
	
    //var canvas = document.getElementById("minimapWebCanvas")
	var canvas = document.createElement('canvas')
	var timeData = []
    var freqData = []
    var timeSig = 0
   
	if (datatest == null) datatest = DATA_get_current_state_point(false)

	console.log(datatest)
	timeSig=datatest.global_variables.Li/datatest.global_variables.PPM*60*18
	//refresh tab global scale if there is a new database
	datatest.voice_data.forEach(voice => {	
	   // voice.data.midi_data
	   voice.data.midi_data.time.push(60*datatest.global_variables.Li/datatest.global_variables.PPM)
	   timeData.push(voice.data.midi_data.time)
	   //console.log(voice.data.midi_data.note_freq)
	   freqData.push(voice.data.midi_data.note_freq.map(f=>{
		return APP_download_frequency_to_percentage(f)
		}))
	})
	
     canvas.height= 84
     canvas.width = timeSig
	
	 
     var ctx = canvas.getContext('2d')    
     for(i=0; i<freqData.length;i++){
      
         for(j=0;j<=freqData[i].length;j++){
            ctx.beginPath()         
            ctx.moveTo(Math.ceil(timeData[i][j]*18),Math.round((1-freqData[i][j])*canvas.height))
            ctx.lineTo(Math.ceil(timeData[i][j+1]*18),Math.round((1-freqData[i][j])*canvas.height)) 
            ctx.strokeStyle = '#43433B'
            ctx.stroke()}
     } 
	
	 var img = canvas.toDataURL('image/png')
	 return img
 }
