//OBSOLETE
// function SNAV_show_hide_RE_compas_modules_button(button){
// 	console.error("OBSOLETE FUNCTION SNAV_show_hide_RE_compas_modules_button")
// 	var state
// 	if(button.classList.contains("App_SNav_modules_button_pressed")){
		//remove
// 		button.classList.remove("App_SNav_modules_button_pressed")
// 		state=false
// 	}else{
		//add
// 		button.classList.add("App_SNav_modules_button_pressed")
// 		state=true
// 	}

// 	APP_refresh_module_lines()
// 	DATA_save_preferences_file()
// }

// function SNAV_show_hide_RE_scale_modules_button(button){
// console.error("OBSOLETE FUNCTION SNAV_show_hide_RE_scale_modules_button")
// 	var state
// 	if(button.classList.contains("App_SNav_modules_button_pressed")){
		//remove
// 		button.classList.remove("App_SNav_modules_button_pressed")
// 		state=false
// 	}else{
		//add
// 		button.classList.add("App_SNav_modules_button_pressed")
// 		state=true
// 	}
// 	APP_refresh_module_lines()
// 	DATA_save_preferences_file()
// }

function SNAV_set_default_icons(){
	var sideNavParent = document.querySelector('.App_SNav_button_container')
	var sideNavButtonsArr = [...sideNavParent.querySelectorAll('button')]
	sideNavButtonsArr.forEach(function(el){
		var img= el.getElementsByTagName('img')
		img[2].style.display='none'
		img[1].style.display='none'
		img[0].style.display='flex'
		var input = el.getElementsByTagName('input')
		if(input[0].checked){
			input[0].checked = false
		}
	})
}

function SNAV_display_menu_content(button){
	if(button.querySelector('input').checked){
		SNAV_hide_menu_content()
		SNAV_set_default_icons()
		var img= button.getElementsByTagName('img')
		//console.log(img)
		//img[0].src=iconsArr[el.value]
		img[2].style.display='none'
		img[0].style.display='none'
		img[1].style.display='flex'
		return
	}

	var sideBugButton = document.getElementById('bug_button')
	if(sideBugButton.value == 1){
		SNAV_show_bug_report(sideBugButton)
	}

	SNAV_set_default_icons()

	var menu_content = document.querySelector(".App_SNav_content")
	var voice_line_selector = document.querySelector('.App_SNav_line_selector')
	var parameters = document.querySelector('.App_SNav_parameters')
	var notebook = document.querySelector('.App_SNav_notebook')
	var settings = document.querySelector('.App_SNav_settings')
	var app_box = document.querySelector('.App_Box')

	//var bottom_Nav = document.getElementsByClassName('App_BNav')
	//var ESbottomdiv = document.getElementById('ES_bottom_content')
	//var bottom_content = document.getElementsByClassName('RE_bottom_content')

	//var tab1 = document.getElementById('Eye_input')
	var tab2 = document.getElementById('Parameter_input')
	var tab3 =  document.getElementById('Notes_input')
	var tab4 = document.getElementById('UserOptions')

	var buttonIcon = button.getElementsByTagName('img')

	buttonIcon[0].style.display='none'
	buttonIcon[1].style.display='none'
	buttonIcon[2].style.display='flex'


	/* buttonIcon_prev = buttonIcon[0]
	buttonIconSrc_prev = buttonIcon[0].src
	//buttonIcon[0].src='./Icons/ojo_active.svg'
	buttonIcon[0].src= `${iconsArrActive[button.value]}` */

	menu_content.style["display"] = "flex"
	//tab1.checked = false
	tab2.checked = false
	tab3.checked = false
	tab4.checked = false

	/* if(button.value == 0){
		tab1.checked = true
	} else  */if (button.value == 1){
		tab2.checked = true
	} else if (button.value == 2){
		tab3.checked = true
	} else if (button.value == 3){
		tab4.checked = true
	}

	/* if(tab1.checked){
		voice_line_selector.style["display"] = "block"
		parameters.style["display"] = "none"
		notebook.style["display"] = "none"
		settings.style["display"] = "none"
	} else  */if (tab2.checked) {
		//voice_line_selector.style["display"] = "none"
		parameters.style["display"] = "flex"
		notebook.style["display"] = "none"
		settings.style["display"] = "none"
	} else if (tab3.checked) {
		//voice_line_selector.style["display"] = "none"
		parameters.style["display"] = "none"
		notebook.style["display"] = "block"
		settings.style["display"] = "none"
	} else if (tab4.checked) {
		//voice_line_selector.style["display"] = "none"
		parameters.style["display"] = "none"
		notebook.style["display"] = "none"
		settings.style["display"] = "block"
	}

	//ESbottomdiv.style.width = 'calc(100% - var(--RE_block5) * 2)'
	//bottom_Nav.style.width = 'calc(100% - var(--RE_block5) * 2)'
	//bottom_content[2].style.width = 'calc(100% - var(--RE_block5) * 2)'
	app_box.style.width = 'calc(100% - var(--RE_block5) * 2)'
}

function SNAV_hide_menu_content(){
	SNAV_set_default_icons()
	var menu_content = document.querySelector(".App_SNav_content")
	//var menu_button_container = document.querySelector(".App_SNav_button_container")
	var app_box = document.querySelector('.App_Box')

	//var ESbottomdiv = document.getElementById('ES_bottom_content')
	//var bottom_Nav = document.getElementsByClassName('App_BNav')
	//var bottom_content = document.getElementsByClassName('RE_bottom_content')

	//var parentbox = document.querySelector('.RE_show_LS')

	//parentbox.style.width = 'var(--RE_block2)'
	//parentbox.style.justifyContent = 'space-between'

	menu_content.style["display"] = 'none'
	//menu_button_container.style["display"] = 'flex'

	//bottom_Nav.style.width = 'calc(100% - var(--RE_block2))'
	//ESbottomdiv.style.width = 'calc(100% - var(--RE_block2))'
	//bottom_content[2].style.width = 'calc(100% - var(--RE_block2))'
	app_box.style.width = 'calc(100% - var(--RE_block2))'
}

function SNAV_buttonHoverEnter(button){
	var buttonInput= button.getElementsByTagName('input')
	var buttonIcon = button.getElementsByTagName('img')

	if(buttonInput[0].checked==true){

		return
	}else{
		buttonIcon[0].style.display="none"
		buttonIcon[1].style.display="block"
		buttonIcon[2].style.display="none"
	}
	/* var iconsArr=['./Icons/side_ojo_hover.svg','./Icons/side_parametros_hover.svg','./Icons/side_notes_hover.svg','./Icons/side_settings_hover.svg']

	buttonIcon[0].src= `${iconsArr[button.value]}` */
	
}

function SNAV_buttonHoverExit(button){
	var buttonInput= button.getElementsByTagName('input')
	var buttonIcon = button.getElementsByTagName('img')
	if(buttonInput[0].checked==true){		
		return
	}else{
		buttonIcon[0].style.display="block"
		buttonIcon[1].style.display="none"
		buttonIcon[2].style.display="none"
	}

	
}

function SNAV_switch_play_new_note(){
	//reversed because it is not yet switched
	var pressed = SNAV_read_play_new_note()
	if(pressed){
		SNAV_set_play_new_note(false)
	}else{
		SNAV_set_play_new_note(true)
	}
	//Save state preferences
	DATA_save_preferences_file()
}

function SNAV_read_play_new_note() {
	var button = document.getElementById("activatePlayNewNoteButton")
	return (button.value === "true")
}

function SNAV_set_play_new_note(value) {
	var button = document.getElementById("activatePlayNewNoteButton")
	button.value=value
	if(value){
		button.innerHTML='On'
	}else{
		button.innerHTML='Off'
	}
}

function SNAV_switch_note_on_click(){
	//reversed because it is not yet switched
	var pressed = SNAV_read_note_on_click()
	if(pressed){
		SNAV_set_note_on_click(false)
	}else{
		SNAV_set_note_on_click(true)
	}
	//Save state preferences
	DATA_save_preferences_file()
}

function SNAV_read_note_on_click() {
	var button = document.getElementById("activatePlayClickNoteButton")
	return (button.value === "true")
}

function SNAV_set_note_on_click(value) {
	var button = document.getElementById("activatePlayClickNoteButton")
	button.value=value
	if(value){
		button.innerHTML='On'
	}else{
		button.innerHTML='Off'
	}
}

function SNAV_switch_note_over_grade(){
	//reversed because it is not yet switched
	var pressed = SNAV_read_note_over_grade()
	if(pressed){
		SNAV_set_note_over_grade(false)
	}else{
		SNAV_set_note_over_grade(true)
	}
	//Save state preferences
	DATA_save_preferences_file()
}

function SNAV_read_note_over_grade() {
	var button = document.getElementById("activateNoteOverGradeButton")
	return (button.value === "true")
}

function SNAV_set_note_over_grade(value) {
	var button = document.getElementById("activateNoteOverGradeButton")
	button.value=value
	if(value){
		button.innerHTML='On'
	}else{
		button.innerHTML='Off'
	}
}

function SNAV_switch_alert_overwrite_scale(){
	//reversed because it is not yet switched
	var pressed = SNAV_read_alert_overwrite_scale()
	if(pressed){
		SNAV_set_alert_overwrite_scale(false)
	}else{
		SNAV_set_alert_overwrite_scale(true)
	}
	//Save state preferences
	DATA_save_preferences_file()
}

function SNAV_read_alert_overwrite_scale() {
	var button = document.getElementById("showAlertOverwriteScaleButton")
	return (button.value === "true")
}

function SNAV_set_alert_overwrite_scale(value) {
	var button = document.getElementById("showAlertOverwriteScaleButton")
	button.value=value
	if(value){
		button.innerHTML='On'
	}else{
		button.innerHTML='Off'
	}
}

function SNAV_switch_export_audio_file_format() {
	var button = document.getElementById("exportAudioFileType")
	var value=button.value
	if(value=="mp3"){
		button.innerHTML='wav'
		button.value='wav'
	}else{
		button.innerHTML='mp3'
		button.value='mp3'
	}
}

function SNAV_read_export_audio_file_format() {
	var button = document.getElementById("exportAudioFileType")
	return value=button.value
}


async function SNAV_show_bug_report(button){
	var sideNavContent = document.querySelector('.App_SNav_content')
	var bugsDivContent = document.querySelector('.App_SNav_bug_content')
	var app_box = document.querySelector('.App_Box')
	//var bottomdiv = document.getElementsByClassName('RE_bottom_div')
	//var ESbottomdiv = document.getElementById('ES_bottom_content')
	//var bottom_content = document.getElementsByClassName('RE_bottom_content')
	if(sideNavContent.style.display != 'none'){
		SNAV_hide_menu_content()
	}

	if(button.value == 0){
		bugsDivContent.style.display="flex"
		app_box.style.width = 'calc(100% - var(--RE_block5) * 2)'
		button.classList.add('pressed')
		button.value ++
		if(typeof wp_Get_User_Permissions!='function' || DEBUG_MODE){
			var user = 0
			if(DEBUG_MODE){			
				user=10
			}else{
				return
			}
			if(user==0){				
				var bugSentTitle = document.querySelector('.App_SNav_bug_noUserTitle')
				var bugSentText = document.querySelector('.App_SNav_bug_noUserText')
				var sendBugButton = document.querySelector('.App_SNav_bug_sendButton')
				bugSentText.style.display='block'
				bugSentTitle.style.display='block'
				sendBugButton.classList.add('noUser')
				sendBugButton.disabled = true
			}
		}else{
			var user = await wp_Get_User_Permissions('','levels')
			if(user==0){
				var bugSentTitle = document.querySelector('.App_SNav_bug_noUserTitle')
				var bugSentText = document.querySelector('.App_SNav_bug_noUserText')
				var sendBugButton = document.querySelector('.App_SNav_bug_sendButton')
				bugSentText.style.display='block'
				bugSentTitle.style.display='block'
				sendBugButton.classList.add('noUser')
				sendBugButton.disabled = true
			}
		}

	} else {
		var bugSentTitle = document.querySelector('.App_SNav_bug_sendTitle')
		var bugParent= document.querySelector('.App_SNav_bug_content_fields')
		var bugSentText = document.querySelector('.App_SNav_bug_sendText')
		bugSentTitle.style.display='none'
		bugSentText.style.display='none'
		app_box.style.width = 'calc(100% - var(--RE_block2))'
		bugsDivContent.style.display="none"
		bugParent.style.maxHeight='calc(100% - 275px)'
		button.classList.remove('pressed')
		button.value = 0
	}
}

function SNAV_toggle_sendConsolePrint(bttn){
	if(bttn.value==1){
		bttn.value=0
	}else{
		bttn.value=1
	}
}

function SNAV_send_bug(){
	var bugTitle = document.getElementById('App_SNav_bug_title')
	var bugDescription = document.getElementById('App_SNav_bug_description')
	//var bugParent= document.querySelector('.App_SNav_bug_content_fields')
	if(bugTitle.innerHTML!=''&&bugDescription.innerHTML!=''){
		if(typeof Save_server_BUG!='function' || DEBUG_MODE){
			SNAV_user_feedback_on_send_bug()
			SNAV_test_send_bug()
		}else {
			Save_server_BUG()
			SNAV_test_send_bug()
		}
	}else{
		bugButton.style.backgroundColor='var(--Nuzic_red)'
	}
}
function SNAV_user_feedback_on_send_bug(error){
		var bugTitle = document.getElementById('App_SNav_bug_title')
		var bugDescription = document.getElementById('App_SNav_bug_description')
		var bugParent= document.querySelector('.App_SNav_bug_content_fields')
		var bugSentTitle = document.querySelector('.App_SNav_bug_sendTitle')
		var bugSentText = document.querySelector('.App_SNav_bug_sendText')
		bugParent.style.maxHeight='calc(100% - 357px)'
		bugTitle.innerHTML=''
		bugDescription.innerHTML=''
		bugSentTitle.style.display='block'
		bugSentText.style.display='block'
}


function SNAV_test_send_bug(){
	var bugTitle = document.getElementById('App_SNav_bug_title')
	var bugDescription = document.getElementById('App_SNav_bug_description')
	var bugButton = document.querySelector('.App_SNav_bug_sendButton')

	if(bugTitle.innerHTML==''||bugDescription.innerHTML==''){		
		bugButton.style.backgroundColor='var(--Nuzic_red_light)'
		bugButton.style.color='var(--Nuzic_light)'
		bugButton.disabled=true
		
	}else if(bugTitle.innerHTML!=''&&bugDescription.innerHTML!=''){
		bugButton.style.backgroundColor='var(--Nuzic_green)'
		bugButton.style.color='var(--Nuzic_dark)'
		bugButton.disabled=false
	}
}

function SNAV_select_zoom(selector) {
	var zoom = Number(selector.value)
	//console.log(zoom)
	//transform: scale(0.1);
	//transform-origin: 0% 0% 0px;
	var App_RE_data = document.querySelector(".App_RE")
	var App_PMC_data = document.querySelector(".App_PMC")

	var transformOrigin = [0,0]
	var p = ["webkit", "moz", "ms", "o"],
		s = "scale(" + zoom + ")",
		oString = (transformOrigin[0] * 100) + "% " + (transformOrigin[1] * 100) + "%"

	for (var i = 0; i < p.length; i++) {
		App_RE_data.style[p[i] + "Transform"] = s
		App_RE_data.style[p[i] + "TransformOrigin"] = oString
		App_PMC_data.style[p[i] + "Transform"] = s
		App_PMC_data.style[p[i] + "TransformOrigin"] = oString
	}

	App_RE_data.style["transform"] = s
	App_RE_data.style["transformOrigin"] = oString
	App_PMC_data.style["transform"] = s
	App_PMC_data.style["transformOrigin"] = oString

	var percent_string = 100/zoom+"%"
	//console.log(percent_string)
	//if tab open -378, if closed - 126
	/*
	var delta = 0

	if(BNav_content_opened()){
		//console.log("open")
		delta = (252+28) //size RE_bottom_div  + opened tab
	}else{
		delta = 28  //size RE_bottom_div  + opened tab
	}


	var minimapShowBttn = document.querySelector('.App_MMap_show_hide_button')
	if(minimapShowBttn.value == 0){
		//height of mini
		delta+= 18-2 // size minimap + play bar + margin
	}else{
		delta+= 98+2 // size minimap + play bar + margin
	}

	//correct for zoom factor
	delta=delta/zoom
	*/
	//var max_percent_string = `calc(${percent_string} - ${delta}px)`
	App_RE_data.style.maxHeight = percent_string
	App_RE_data.style.minHeight = percent_string
	App_RE_data.style.maxWidth = percent_string
	App_RE_data.style.minWidth = percent_string
	App_RE_data.style["width"]= percent_string
	App_RE_data.style["height"]= percent_string
	App_PMC_data.style.maxHeight = percent_string
	App_PMC_data.style.minHeight = percent_string
	App_PMC_data.style.maxWidth = percent_string
	App_PMC_data.style.minWidth = percent_string
	App_PMC_data.style["width"]= percent_string
	App_PMC_data.style["height"]= percent_string
}

