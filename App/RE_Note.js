//Implementation of N_line roules
function RE_enter_new_value_S(element,type){
	var string = element.value
	if(string==previous_value) return

	if(type=="Ngm" && string==RE_superscript_to_string(previous_value)){
		//need to rewrite
		element.value=previous_value
		return
	}

	APP_stop()

	//control value
	var segment_obj = element.closest(".App_RE_segment")
	var voice_obj = segment_obj.closest(".App_RE_voice")

	var voice_index = RE_read_voice_id(voice_obj)[1]
	var segment_list= [...voice_obj.querySelectorAll(".App_RE_segment")]
	var segment_index = segment_list.indexOf(segment_obj)
	var position
	var next=0

	//verify if is last cell
	//if(element.classList.contains("App_RE_segment_last_cell"))return //not really needed
	if(string==""){
		if(previous_value!=""){
			//delete values if not fraction range
			var [elementIndex,input_list]=RE_element_index(element,".App_RE_inp_box")
			position = 0
			input_list.find((inp,index)=>{
				if (index==elementIndex)return true
				if(inp.value!="")position++
			})
			var success = DATA_delete_object_index(voice_index,segment_index,position) //if fraction range note = "e"
			if(!success){
				element.value=previous_value
				return
			}
			next=0
		}else{
			return
		}
	}else{
		//control correctness new value of the note
		if(previous_value==""){
			//added new item
			//the note define a new iT
			var [note_number,isanote,diesis]= RE_read_Sound_Element(element,type)
			//console.log(note_number+"  "+isanote+"  "+diesis)
			if(note_number==null){
				//no need of continuing calculations.... is an error
				element.value=previous_value
				return
			}

			var [elementIndex,input_list]=RE_element_index(element,".App_RE_inp_box")
			position = 0
			input_list.find((inp,index)=>{
				if (index==elementIndex)return true
				if(inp.value!="")position++
			})

			if(note_number>=TET*7){
				element.value=previous_value
				APP_blink_error(element)
				return
			}
			diesis= (diesis==null)?true:diesis
			var success = DATA_enter_new_object_index(voice_index,segment_index,position,null,null,note_number,diesis)
			if(!success){
				element.value=previous_value
				APP_blink_error(element)
				var RE_iT = segment_obj.querySelector(".App_RE_iT")
				if(RE_iT!=null){
					var input_iT_list = [...RE_iT.querySelectorAll(".App_RE_inp_box:not(:placeholder-shown)")]
					APP_blink_error(input_iT_list[position-1])
				}
				return
			}
			//play note
			if(SNAV_read_play_new_note() && success){
				var instrument_index = DATA_which_voice_instrument(voice_index)
				APP_play_this_note_number(note_number,instrument_index)
			}
			next=1
		}else {
			//changed item
			var [note_number,isanote,diesis]= RE_read_Sound_Element(element,type)
			if(note_number==null){
				//no need of continuing calculations.... is an error
				element.value=previous_value
				return
			}
			//console.log(note_number+"  "+isanote+"  "+diesis)

			var [elementIndex,input_list]=RE_element_index(element,".App_RE_inp_box")
			position = 0
			input_list.find((inp,index)=>{
				if (index==elementIndex)return true
				if(inp.value!="")position++
			})

			//if ligadura and first element
			if(note_number==-3 && elementIndex==0){
				element.value=previous_value
				APP_blink_error(element)
				return
			} else {
				if(note_number>=TET*7){
					element.value=previous_value
					APP_blink_error(element)
					return
				}
				var success = DATA_modify_object_index_note(voice_index,segment_index, position, note_number,diesis)
				if(!success){
					element.value=previous_value
					APP_blink_error(element)
					return
				}

				//play note
				if(SNAV_read_play_new_note() && success){
					var instrument_index = DATA_which_voice_instrument(voice_index)
					APP_play_this_note_number(note_number,instrument_index)
				}
				next=1
			}
		}
	}
	RE_focus_on_object_index(voice_index,segment_index,type,position,next)
}

function RE_enter_new_value_dS(element,type){
	//if P is part of fraction range
	var string = element.value
	if(string==previous_value) return
	if(type=="iNgm" && string==RE_superscript_to_string(previous_value)){
		//need to rewrite
		element.value=previous_value
		return
	}

	if(previous_value==end_range){
		element.value=previous_value
		return
	}

	if(string=="-") {
		//no need of continuing calculations.... is an error???
		element.value=previous_value
		return
	}

	//control value ok
	if (string==end_range){
		//structural element
		element.value=previous_value
		return
	}

	APP_stop()

	if (string==tie_intervals){
		//tie 2 intervals
	}
	if (string==tie_intervals_m){
		//tie 2 intervals
		element.value=tie_intervals
	}

	var segment_obj = element.closest(".App_RE_segment")
	var voice_obj = segment_obj.closest(".App_RE_voice")

	var voice_index = RE_read_voice_id(voice_obj)[1]
	var segment_list= [...voice_obj.querySelectorAll(".App_RE_segment")]
	var segment_index = segment_list.indexOf(segment_obj)

	var [elementIndex,input_iN_list]=RE_element_index(element,".App_RE_inp_box")

	var position = 0
	var next=0
	input_iN_list.find((inp,index)=>{
		if (index==elementIndex)return true
		if(inp.value!="")position++
	})

	if(previous_value==""){
		//the iN define a new iT
		var [note_number,isanote,diesis]= RE_read_Sound_Element(element,type)
		//console.log(note_number+"  "+isanote+"  "+diesis)
		if(note_number==null){
			element.value=""
			APP_blink_error(element)
			return
		}
		diesis= (diesis==null)?true:diesis
		var success = false
		if(type!="iNgm"){
			success = DATA_enter_new_object_index_iN(voice_index,segment_index,position,note_number,isanote,diesis)
		}else {
			success = DATA_enter_new_object_index_iNgrade(voice_index,segment_index,position,note_number,isanote,diesis)
		}

		if(!success){
			//no space for new note
			element.value=""
			APP_blink_error(element)
			var RE_iT = segment_obj.querySelector(".App_RE_iT")
			if(RE_iT!=null){
				var input_iT_list = [...RE_iT.querySelectorAll(".App_RE_inp_box:not(:placeholder-shown)")]
				APP_blink_error(input_iT_list[position-1])
			}
			return
		}
		if(SNAV_read_play_new_note() && success){
			APP_stop()
			APP_play_this_note_RE_element(element)
		}
		next=1
	}else {
		//modify existing Note sequence
		if(element.value==""){
			//delete pulse value but if it is a fraction range put a "e"
			var success=false
			//if(!element.classList.contains("App_RE_note_bordered_cell") && type=="iNgm"){//control not necess.... inside next function
			if(type=="iNgm"){
				success = DATA_delete_object_index_iNgrade(voice_index,segment_index,position)
			}else{
				success = DATA_delete_object_index_iN(voice_index,segment_index,position)
			}

			if(!success){
				element.value=previous_value
				//APP_blink_error(element)
				return
			}
			next=0
		}else{
			//control correctness new value (range and type)
			var [note_number,isanote,diesis]= RE_read_Sound_Element(element,type)
			if(note_number==null){
				//no need of continuing calculations.... is an error
				element.value=previous_value
				APP_blink_error(element)
				return
			}
			//if diesis == null no change
			var success=false
			if(type!="iNgm"){
				success = DATA_modify_object_index_iN(voice_index,segment_index,position,note_number,isanote,diesis)
			}else{
				success = DATA_modify_object_index_iNgrade(voice_index,segment_index,position,note_number,isanote,diesis)
			}

			if(!success){
				element.value=previous_value
				APP_blink_error(element)
				return
			}
			
		if(SNAV_read_play_new_note() && success){
			APP_stop()
			APP_play_this_note_RE_element(element)
		}
			next=1
		}
	}
	RE_focus_on_object_index(voice_index,segment_index,type,position,next)
}

function RE_write_segment_Note_lines(segment_obj,line_list_states,index_columns,Psg_segment_start,segment_data,N_NP,D_NP){
	//writing single time lines
	if(line_list_states.Na){
		var Na_line = segment_obj.querySelector(".App_RE_Na")
		var input_Na_list = [...Na_line.querySelectorAll("input")]
		index_columns.forEach((column_number,index)=>{
			//note
			var current_note = segment_data.note[index]
			if(current_note<0){
				RE_write_a_no_note(current_note,input_Na_list[column_number])
			}else {
				input_Na_list[column_number].value= current_note
			}
		})
	}

	if(line_list_states.Nm){
		// Mod
		var Nm_line = segment_obj.querySelector(".App_RE_Nm")
		var input_list= [...Nm_line.querySelectorAll(".App_RE_inp_box")]
		var previous_reg=null
		index_columns.forEach((column_number,index)=>{
			var current_note=segment_data.note[index]
			if(current_note<0){
				RE_write_a_no_note(current_note,input_list[column_number])
			}else{
				var reg = Math.floor(current_note/TET)
				var resto = current_note%TET

				//warning first element
				//verify is has module, if not read previous value
				var showreg = true
				//here retrive button value
				if(previous_reg==reg)showreg=false

				if(showreg){
					stringN=resto+"r"+reg
				}else{
					stringN=resto
				}
				input_list[column_number].value= stringN
				previous_reg= reg
			}
		})
	}

	if(line_list_states.Ngm){
		// grade mod
		var Ngm_line = segment_obj.querySelector(".App_RE_Ngm")
		var input_list= [...Ngm_line.querySelectorAll(".App_RE_inp_box")]
		var previous_reg=null
		var prev_scale=null
		index_columns.forEach((column_number,index)=>{
			var current_note = segment_data.note[index]

			var current_P = segment_data.time[index].P
			var current_F = segment_data.time[index].F
			//find correct scale

			//find Pa_equivalent and verify if inside a scale
			//var time_p = int_p * N/D + AF*frac_p * N/D
			var current_fraction = segment_data.fraction.find(fraction=>{return fraction.stop>current_P})
			//console.log(current_fraction)
			//var frac_p = fractComplex/fractSimple
			var frac_p = current_fraction.N/current_fraction.D
			var Pa_equivalent = (Psg_segment_start+current_P) * N_NP/D_NP + current_F* frac_p * N_NP/D_NP

			var current_scale = DATA_get_Pa_eq_scale(Pa_equivalent)

			if(current_scale!=null){
				//element inside a scale module
				if(current_note<0){
					RE_write_a_no_note(current_note,input_list[column_number])
				}else{
					//determine if diesis or bemolle
					var [grade,delta,reg]=BNAV_absolute_note_to_scale(current_note,segment_data.diesis[index],current_scale)
					//warning first element
					//verify is has module, if not read previous value
					var showreg = true
					if(previous_reg==reg)showreg=false
					previous_reg= reg

					if(prev_scale!=null){
						if(prev_scale.acronym!=current_scale.acronym || prev_scale.rotation!=current_scale.rotation || prev_scale.starting_note!=current_scale.starting_note){
							// different scales
							input_list[column_number].classList.add("App_RE_note_bordered_cell")
							showreg=true
						}
					}else{
						input_list[column_number].classList.add("App_RE_note_bordered_cell")
					}
					prev_scale=current_scale

					var diesis = ""
					if(delta==1){
						diesis="+"
					}else if(delta>1){
						diesis="+"+(delta)
					}else if(delta==-1){
						diesis="-"
					}else if(delta<-1){
						diesis="-"+Math.abs(delta)
					}
					if(showreg){
						//stringN=grade+diesis+"r"+reg
						stringN=grade+RE_string_to_superscript(diesis)+"r"+reg
					}else{
						//stringN=grade+diesis
						stringN=grade+RE_string_to_superscript(diesis)
					}
					input_list[column_number].value= stringN
				}
			}else{
				//outside defined scales
				input_list[column_number].value = no_value
				input_list[column_number].classList.add("App_RE_note_empty_cell")
			}
		})
	}

	if(line_list_states.iNa){
		//first note is a bordered cell
		var iNa_line = segment_obj.querySelector(".App_RE_iNa")
		var input_list= [...iNa_line.querySelectorAll(".App_RE_inp_box")]
		var first_note=true
		var prev_note=null
		index_columns.forEach((column_number,index)=>{
			var current_note=segment_data.note[index]
			if(current_note<0){
				RE_write_a_no_note(current_note,input_list[column_number])
			}else{
				if(first_note){
					input_list[column_number].value=current_note
					input_list[column_number].classList.add("App_RE_note_bordered_cell")
					first_note=false
					prev_note=current_note
				}else{
					input_list[column_number].value=current_note-prev_note
					prev_note=current_note
				}
			}
		})
	}

	if(line_list_states.iNm){
		//first note is a bordered cell
		var iNm_line = segment_obj.querySelector(".App_RE_iNm")
		var input_list= [...iNm_line.querySelectorAll(".App_RE_inp_box")]
		var first_note=true
		var prev_note=null
		index_columns.forEach((column_number,index)=>{
			var current_note=segment_data.note[index]
			if(current_note<0){
				RE_write_a_no_note(current_note,input_list[column_number])
			}else{
				if(first_note){
					var reg = Math.floor(current_note/TET)
					var resto = current_note%TET
					input_list[column_number].value= resto+"r"+reg
					input_list[column_number].classList.add("App_RE_note_bordered_cell")
					first_note=false
					prev_note=current_note
				}else{
					var int_n = current_note-prev_note
					var reg = Math.floor(int_n/TET)
					var resto = int_n%TET
					var neg = false
					if(int_n<0){
						neg = true
						//console.log(int_n+"  resto "+resto)
						resto = Math.abs(resto)
						if(resto!=0){
							reg= Math.abs(reg)-1
						}else{
							reg= Math.abs(reg)
						}
					}
					var stringN=""
					if(neg){
						stringN="-"+resto//+"r"+reg
					}else{
						stringN=resto//+"r"+reg
					}
					if(reg!=0)stringN=stringN+"r"+reg
					input_list[column_number].value = stringN
					prev_note=current_note
				}
			}
		})
	}

	if(line_list_states.iNgm){
		var iNgm_line = segment_obj.querySelector(".App_RE_iNgm")
		var input_list= [...iNgm_line.querySelectorAll(".App_RE_inp_box")]
		var prev_note=null
		var prev_diesis=true
		var prev_scale=null
		index_columns.forEach((column_number,index)=>{
			var current_note = segment_data.note[index]
			var current_diesis = segment_data.diesis[index]

			var current_P = segment_data.time[index].P
			var current_F = segment_data.time[index].F
			//find correct scale

			//find Pa_equivalent and verify if inside a scale
			//var time_p = int_p * N/D + AF*frac_p * N/D
			var current_fraction = segment_data.fraction.find(fraction=>{return fraction.stop>current_P})
			//console.log(current_fraction)
			//var frac_p = fractComplex/fractSimple
			var frac_p = current_fraction.N/current_fraction.D
			var Pa_equivalent = (Psg_segment_start+current_P) * N_NP/D_NP + current_F* frac_p * N_NP/D_NP
			//console.log(Pa_equivalent)
			var current_scale = DATA_get_Pa_eq_scale(Pa_equivalent)

			if(current_scale!=null){
				//element inside a scale module
				if(current_note<0){
					RE_write_a_no_note(current_note,input_list[column_number])
				}else{
					var write_iN = true
					if(prev_scale!=null){
						if(prev_scale.acronym!=current_scale.acronym || prev_scale.rotation!=current_scale.rotation || prev_scale.starting_note!=current_scale.starting_note){
							// different scales
							input_list[column_number].classList.add("App_RE_note_bordered_cell")
							write_iN=false
						}
					}else{
						input_list[column_number].classList.add("App_RE_note_bordered_cell")
						write_iN=false
					}
					prev_scale=current_scale

					if(write_iN){
						//calculate note interval and make a traduction in current scale //PREV OR CURRENT SCALE??? XXX XXX
						input_list[column_number].value = BNAV_calculate_scale_interval_string(prev_note,prev_diesis,current_scale,current_note,current_diesis,current_scale)
					}else{
						//write note in current scale
						var [grade,delta,reg]=BNAV_absolute_note_to_scale(current_note,current_diesis,current_scale)
						var diesis = ""
						if(delta==1){
							diesis="+"
						}else if(delta>1){
							diesis="+"+(delta)
						}else if(delta==-1){
							diesis="-"
						}else if(delta<-1){
							diesis="-"+Math.abs(delta)
						}
						//stringN=grade+diesis+"r"+reg
						//grade+RE_string_to_superscript(diesis)+"r"+reg
						input_list[column_number].value= grade+RE_string_to_superscript(diesis)+"r"+reg
					}
					prev_note=current_note
					prev_diesis=current_diesis
				}
			}else{
				//outside defined scales
				input_list[column_number].value = no_value
				input_list[column_number].classList.add("App_RE_note_empty_cell")
			}
		})
	}

	if(line_list_states.Nabc){
		var Nabc_line = segment_obj.querySelector(".App_RE_Nabc")
		var input_list= [...Nabc_line.querySelectorAll(".App_RE_inp_box")]

		if(TET==12){
			index_columns.forEach((column_number,index)=>{
				//var reg = Math.floor(int_n/TET)
				var current_note=segment_data.note[index]
				if(current_note<0){
					RE_write_a_no_note(current_note,input_list[column_number])
				}else{
					var resto = current_note%TET
					if(segment_data.diesis[index]){
						input_list[column_number].value= ABC_note_list_diesis[resto]
					}else{
						input_list[column_number].value= ABC_note_list_bemolle[resto]
					}

				}
			})
		}else{
			index_columns.forEach((column_number,index)=>{
				input_list[column_number].value= no_value
			})
		}
	}
}

function RE_write_a_no_note(int_n,element){
	var stringN="Err"
	if (int_n ==-1){
		//silence
		stringN="s"
		element.setAttribute("data-value", "sHover")
	} else if (int_n ==-2){
		//un-determined element
		stringN="e"
		element.setAttribute("data-value", "eHover")
	} else if (int_n ==-3){
		//L
		//control if not first element of the segment
		//var [elementIndex,]=RE_element_index(element,".App_RE_inp_box")
		//if(elementIndex==0){
			//replace with e
			//console.error("L on start segment not allowed")
			//stringN="e"
			//element.setAttribute("data-value", "eHover")
		//}else{
		stringN=tie_intervals
			element.setAttribute("data-value", "LHover")
		//}
	} else if (int_n ==-4){
		//ending line
		stringN=end_range
		element.setAttribute("data-value", "dotHover")
	}
	element.value = stringN
}

function RE_read_Sound_Element(element, line_id=null){
	//translate element.value to N element absolute
	if(line_id==null){
		console.error("read_sound no line_id")
		return
	}

	var string = element.value
	if(string==="") return [null,false]

	var note_number = null
	var diesis = null

	//case not a note
	if (string=="s"){
		//silence
		note_number=-1
		return [note_number,false]
	}
	if (string=="e"){
		//un-determined element
		note_number=-2
		return [note_number,false]
	}
	if (string==tie_intervals){
		note_number=-3
		return [note_number,false]
	}
	if (string==tie_intervals_m){
		//console.log("ligadura l min")
		note_number=-3
		element.value=tie_intervals
		return [note_number,false]
	}
	if (string==end_range){
		//structural element
		note_number=-4
		return [note_number,false]
	}
	if (string=="."){
		//structural element
		note_number=-4
		element.value=end_range
		return [note_number,false]
	}

	//using database in order to find prev element note/scale/etc...
	var segment_obj = element.closest(".App_RE_segment")
	var voice_obj = element.closest(".App_RE_voice")
	var voice_index = RE_read_voice_id(voice_obj)[1]
	var segment_list= [...voice_obj.querySelectorAll(".App_RE_segment")]
	var segment_index = segment_list.indexOf(segment_obj)
	var [elementIndex,input_list]=RE_element_index(element,".App_RE_inp_box")
	var position = 0
	input_list.find((inp,index)=>{
		if (index==elementIndex)return true
		if(inp.value!="")position++
	})

	//case it is a note
	switch(line_id) {
	case "Na":
		// Abs
		note_number = Math.floor(string)
		if(isNaN(note_number)){
			note_number = null
			break
		}
		var max_note_number = TET * 7-1
		if(note_number>max_note_number){
			//if OUT OF NOTE RANGE
			note_number=max_note_number
		}
		break;
	case "Nm":
		// Mod
		//verify if has module, if not read previous value
		var split = element.value.split('r')
		var resto = Math.floor(split[0])
		var reg=3
		if(split[1]>=0 && split[1]!=""){
			reg=Math.floor(split[1])
		}else{
			var previous_element_position = DATA_get_object_prev_note_index(voice_index,segment_index,position)
			//console.log(previous_element_position)
			if(previous_element_position!= null){
				reg= Math.floor(DATA_get_object_index_info(voice_index,segment_index,previous_element_position).note/TET)
			}
		}
		//console.log(resto + "  "+ reg)
		if(resto>=TET){
			note_number = null
		}else{
			note_number = resto + reg*TET
			if(isNaN(note_number)){
				note_number = null
			}
		}
		break;
	case "Ngm":
		// grade mod
		//console.log(voice_index+" "+segment_index+" "+position)
		var iT=0
		var scale_position=position
		if(previous_value==""){
			iT=1
			scale_position--
		}
		var current_scale = DATA_get_object_index_info(voice_index,segment_index,scale_position,iT,false,false,true).scale

		if (current_scale==null){
			//no scale in this pulse
			console.error("Error reading RE input Ngm, scale not entered")
			return [null,null,null]
			break;
		}

		//ATT! reg can be -1
		var reg = 3
		var grade = 0
		var delta = 0

		if(string.includes("r")){
			var split = string.split("r")
			if(split[1]!=""){
				reg=Math.floor(split[1])
			}else{
				//no scale in this pulse
				console.error("no reg spec")
				note_number = null
				break;
			}
			var str_grade = split[0]
			if(split[0].includes("+")){
				var split2= split[0].split("+")
				str_grade=split2[0]
				delta=parseInt(split2[1])
				if(isNaN(delta))delta=1
				diesis=true
			}
			if(split[0].includes("-")){
				var split2= split[0].split("-")
				str_grade=split2[0]
				delta=parseInt(split2[1])*(-1)
				if(isNaN(delta))delta=-1
				diesis=false
			}
			var grade = parseInt(str_grade)
		}else{
			//find reg in previous OR reg==3
			var previous_element_position = DATA_get_object_prev_note_index(voice_index,segment_index,position)
			if(previous_element_position!= null){
				var prev_element_info = DATA_get_object_index_info(voice_index,segment_index,previous_element_position,0,false,false,true)
				var [,,c]=BNAV_absolute_note_to_scale(prev_element_info.note,true,prev_element_info.scale)
				reg= c
			}

			var str_grade = string
			if(string.includes("+")){
				var split2= string.split("+")
				str_grade=split2[0]
				delta=parseInt(split2[1])
				if(isNaN(delta))delta=1
				diesis=true
			}
			if(string.includes("-")){
				var split2= string.split("-")
				str_grade=split2[0]
				delta=parseInt(split2[1])*(-1)
				if(isNaN(delta))delta=-1
				diesis=false
			}
			var grade = parseInt(str_grade)
		}

		var scaleLength = idea_scale_list.scale_list.find(item=>{
			return item.TET == TET && item.letter==current_scale.acronym
		})
		//console.log(scaleLength)
	
		if(grade>=scaleLength.iS.length){
			//console.log(scale)
			element.value = previous_value
			console.log("ERROR : grade exceed scale length")
		} else {
			note_number = BNAV_scale_note_to_absolute(grade,delta,reg,current_scale)
		}

		if(isNaN(note_number)){
			note_number = null
		}
		break;
	case "iNa":
		note_number = Math.floor(string)
		//control if first element is a note
		//var [index,]=RE_element_index(element,".App_RE_inp_box")
		if(isNaN(note_number)){
			note_number = null
		}
		var max_note_number = TET * 7-1
		if(note_number>max_note_number){
			//un-determined element
			note_number=max_note_number
		}
		if(note_number<-max_note_number){
			//un-determined element
			note_number=-max_note_number
		}
		break;
	case "iNm":
		// distance modular
		//verify if is negative
		var i= 1
		if (string[0]=="-"){
			i=-1
		}
		//verify if has module, if not read previous value
		var split = element.value.split('r')
		var resto = Math.floor(split[0])
		var reg=0
		if(split[1]>=0 && split[1]!=""){
			reg=Math.floor(split[1])
		}

		if(!isNaN(resto)){
			note_number = resto + reg*TET*i
		}else{
			note_number = null
		}
		break;
	case "iNgm":
		//distance grade mod
		//find correct scale
		var iT=0
		var scale_position=position
		if(previous_value==""){
			iT=1
			scale_position--
		}
		var current_scale = DATA_get_object_index_info(voice_index,segment_index,scale_position,iT,false,false,true).scale

		if(current_scale==null){
			note_number = null
			break
		}
		//find previous note
		var previous_element_position = DATA_get_object_prev_note_index(voice_index,segment_index,position)
		var previous_scale = null
		//var previous_Na=0
		if(previous_element_position!= null){
			var prev_element_info = DATA_get_object_index_info(voice_index,segment_index,previous_element_position,0,false,false,true)
			//var [,,c]=BNAV_absolute_note_to_scale(prev_element_info.note,true,prev_element_info.scale)
			//reg= c
			//previous_Na = prev_element_info.note
			previous_scale = prev_element_info.scale
		}

		//verify if is negative
		var i= 1
		if (string[0]=="-"){
			i=-1
			string = string.substring(1)
		}

		var split = string.split('r')
		var reg=0
		if(JSON.stringify(current_scale)!=JSON.stringify(previous_scale))reg=3
		if(string.includes("r")){
			if(split[1]!=""){
				reg=parseInt(split[1])
			}
		}
		//reg cant be negative if is first element of a scale!!!
		// if(previous_element_position== null && (reg<0 || i<0)){
		// 	note_number = null
			//console.error("first number cant be neg")
		// 	break
		// }
		//calculating grade and dieresis
		//part with diesis
		var g = 0
		var delta = 0

		if(split[0].includes("+")){
			//positive dieresis
			var split2 = split[0].split('+')
			g= Math.floor(split2[0])
			if(split2[1]==""){
				delta=1
			}else{
				delta=Math.floor(split2[1])
			}
		}else if(split[0].includes("-")){
			//negatives
			var split2 = split[0].split('-')
			g= Math.floor(split2[0])
			if(split2[1]==""){
				delta=-1
			}else{
				delta=Math.floor(split2[1])*(-1)
			}
		}else{
			g= Math.floor(split[0])
		}

		//console.log("going "+i+ "  of grade "+g+" and diesis "+delta+" and reg "+reg)
		//changing this values in note_number

		if(!isNaN(i) && !isNaN(g) && !isNaN(delta) && !isNaN(reg)){
			//grade , delta , register
			var grade = g*i
			//console.log("going grade "+grade+" and diesis "+delta+" and reg "+reg)

			if(JSON.stringify(current_scale)==JSON.stringify(previous_scale)){
				//same scale
				/*/console.log("same scale")
				var [prev_grade,prev_delta,prev_rev]=BNAV_absolute_note_to_scale(prev_element_info.note,prev_element_info.diesis,prev_element_info.scale)

				var next_grade = prev_grade+grade
				var next_delta = prev_delta+delta
				var next_reg = prev_rev+reg

				if(grade<0){
					//correct the reg if negative grade
					var scale_module = BNAV_get_scale_module(current_scale)
					var reg_in_grade = Math.floor( Math.abs(grade)/scale_module)
					var resto_grade = Math.abs(grade)%scale_module
					grade = scale_module-resto_grade
				}

				var current_Na = BNAV_scale_note_to_absolute(next_grade,next_delta,next_reg,current_scale)

				////contorl neg
				if(current_Na<0){
					console.log("going grade "+next_grade+" and diesis "+next_delta+" and reg "+next_reg)
					console.error("first element of a new scale but negative : Na "+current_Na)
					note_number = null
					break
				}

				// console.log("prev Na "+previous_Na)
				// console.log("current Na "+current_Na)
				note_number=current_Na-previous_Na

				//DIESISSSSS STESSOOOOOOO
				if(next_delta>0)diesis=true
				if(next_delta<0)diesis=false
				//no change if delta = 0
*/
				//calculations made in DATA_function
				note_number=grade
				break
			}else{
				//scale changed INPUT INSERTED IS NEW PULSE
				console.log("scale changed")
				if(grade<0 || reg<0){ //XXX non é detto   RICORDA LE ROTAZIONIIIII XXX
					note_number = null
					console.log("going grade "+grade+" and diesis "+delta+" and reg "+reg)
					console.error("first number of a new scale cant be neg")
					break
				}

				var current_Na = BNAV_scale_note_to_absolute(grade,delta,reg,current_scale)

				if(current_Na<0){
					console.log("going grade "+grade+" and diesis "+delta+" and reg "+reg)
					console.error("first element of a new scale but negative : Na "+current_Na)
					note_number = null
					break
				}
				//note_number= current_Na-previous_Na//nono,scale changed
				note_number= current_Na
				if(delta>0)diesis=true
				if(delta<0)diesis=false
				//no change if delta = 0
				break
			}
		}else{
			note_number = null
		}
		break;
	default:
		console.error("Error reading case N writing roules")
		note_number=null
	}

	if(note_number=="X" || note_number==null)console.error("i don't understand what you wrote")
	return [note_number,true,diesis]
}


