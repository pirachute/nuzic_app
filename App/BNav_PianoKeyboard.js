function BNAV_set_virtual_piano_octaves(){
	var keyBaseValues = [1,3,6,8,10,0,2,4,5,7,9,11]
	var keysParent = document.querySelector('.App_BNav_keyboard_container')
	var keysParentList = [...document.querySelectorAll('.App_BNav_keyboard_container')]
	var parentIndex=0

	keysParentList.forEach(parent=>{
		var keysArr = [...parent.querySelectorAll('button')]
		keysArr.forEach(function(key, index){
			var tempValue = 12*parentIndex+keyBaseValues[index]
			key.value = tempValue
			key.innerHTML = tempValue
			key.setAttribute('onclick', 'BNAV_try_insert_note_duration(parseInt(this.value),keyboard_iT)')
			//key.setAttribute('onmousedown','APP_play_input_on_click(this,`Kb`)')
			key.setAttribute('onmousedown','BNAV_play_note(this.value,true,1)')
			key.setAttribute('onmouseenter','BNAV_enter_kbTile(this.value,true,2)')
			key.setAttribute('onmouseup','BNAV_play_note(this.value,false,1)')
			key.setAttribute('onmouseleave','BNAV_exit_kbTile(this.value,false,2)')
		})
		parentIndex++
	})

	BNAV_keyboard_keys_visualFeedback()
}

setTimeout(function () {
	BNAV_set_virtual_piano_octaves()
}, 50)

function BNAV_increase_virtual_piano_octave(){
	if(virtualPianoOctave == 6 || virtualPianoOctave == 5 && virtualPianoNote!=0){
		return
	}else{
	//console.log(virtualPianoOctave)
	var VPOctaveInput = document.getElementById('App_BNav_keyboard_octave')
	var modBttn = document.getElementById('App_BNav_keyboard_mod')
	BNAV_keyboard_clear_keys_visualFeedback()
	virtualPianoOctave++
	//console.log(virtualPianoOctave)
	VPOctaveInput.value = `${virtualPianoNote}r${virtualPianoOctave}`;
	//virtualPianoNote=0
	BNAV_keyboard_keys_visualFeedback()
	BNAV_keyboard_selectedKeysContainer_position()
	}
}

function BNAV_decrease_virtual_piano_octave(){
	if(virtualPianoOctave == 0){
		return
	}else{
	var VPOctaveInput = document.getElementById('App_BNav_keyboard_octave')
	var modBttn = document.getElementById('App_BNav_keyboard_mod')
	BNAV_keyboard_clear_keys_visualFeedback()
	virtualPianoOctave--
	VPOctaveInput.value = `${virtualPianoNote}r${virtualPianoOctave}`;
	BNAV_keyboard_keys_visualFeedback()
	BNAV_keyboard_selectedKeysContainer_position()
	}
}

function BNAV_pianoKeysToAbs(){
	//Keys to absolute mode
	var absBttn = document.getElementById('App_BNav_keyboard_abs')
	var modBttn = document.getElementById('App_BNav_keyboard_mod')
	/* absBttn.style.display= "block"
	modBttn.style.display= "none" */
	/* modBttn.value = 0 */

	if(TET==12){
		var keyBaseValues = [1,3,6,8,10,0,2,4,5,7,9,11]
		var keysParent = document.querySelector('.App_BNav_keyboard_container')
		var keysArr = [...keysParent.querySelectorAll('button')]
		keysArr.forEach(function(key, index){
			var tempValue = 12*virtualPianoOctave+keyBaseValues[index]
			key.innerHTML = tempValue
		})
	}else{
		BNAV_clear_piano()
		BNAV_TET_virtual_piano()
	}
}

function BNAV_pianoKeysToMod(){
	//keys to modular mode
	var absBttn = document.getElementById('App_BNav_keyboard_abs')
	var modBttn = document.getElementById('App_BNav_keyboard_mod')
	/* absBttn.style.display= "none"
	modBttn.style.display= "block" */
	/* modBttn.value = 1 */
	var keyboardOptionsParent= document.querySelector('.App_BNav_keyboard_optionsSelectKeyboard_container')
	var pianoKb = keyboardOptionsParent.childNodes[1]
	var kbModContainer = document.querySelector('.App_BNav_keyboard_optionsSelectKeyboard_container')

	if(kbModContainer.childNodes[1].classList.contains('App_BNav_keyboard_optionsSelectKeyboard_selectButtons_checked')){
		var keyBaseValues = [1,3,6,8,10,0,2,4,5,7,9,11]
		var keysParent = [...document.querySelectorAll('.App_BNav_keyboard_container')]
		var VPOctaveInput = document.getElementById('App_BNav_keyboard_octave')

		keysParent.forEach(function(keyBox,indexParent){
			var keysArr = [...keyBox.querySelectorAll('button')]
		keysArr.forEach(function(key, index){
			if(index == 5){
				key.innerHTML=`${keyBaseValues[index]}<sup>${indexParent}</sup>`
			} else {
				key.innerHTML = keyBaseValues[index]
			}
		})})
	}else{
		var container = document.querySelector('.App_BNav_keyboard_container')
		var blackContainer = container.querySelector('.App_BNav_keyboard_container_blackKeys')
		var whiteContainer =container.querySelector('.App_BNav_keyboard_container_whiteKeys')
		var supCount=0

		if(TET%2==0){
			whiteContainer.childNodes.forEach((key)=>{
				if(key.value==0){
					key.innerHTML=`${key.value}<sup>${supCount}</sup>`
					supCount++
				}
				if(key.value/TET>=1){
					if(key.value%TET==0){
						key.innerHTML=`${key.value%TET}<sup>${supCount}</sup>`
						supCount++
					}else{
						key.innerHTML=`${key.value%TET}`
					}
				}
			})
			blackContainer.childNodes.forEach((key)=>{
				if(key.value/TET>1){
					key.innerHTML=`${key.value%TET}`
				}
			})
		}else{
			whiteContainer.childNodes.forEach((key)=>{
				if(key.value==0){
					key.innerHTML=`${key.value}<sup>${supCount}</sup>`
					supCount+=2
				}
				if(key.value/TET>1){
					if(key.value%TET==0){
						key.innerHTML=`${key.value%TET}<sup>${supCount}</sup>`
						supCount+=2
					}else{
						key.innerHTML=`${key.value%TET}`
					}
				}
			})
			supCount=1
			blackContainer.childNodes.forEach((key)=>{
				if(key.value/TET>=1){
					if(key.value%TET==0){
						key.innerHTML=`${key.value%TET}<sup>${supCount}</sup>`
						supCount+=2
					}else{
						key.innerHTML=`${key.value%TET}`
					}
				}
			})
		}
	}
}

function BNAV_draw_virtual_piano(){
	if(TET ==12){
		BNAV_basic_virtual_piano()
	}else{
		BNAV_TET_virtual_piano()
	}
}

function BNAV_basic_virtual_piano(){
	//set basic piano
	var container = document.querySelector('.App_BNav_keyboard_containerPiano')

	container.innerHTML=`
		
	<div class="App_BNav_keyboard_container">
		<div class="App_BNav_keyboard_container_blackKeys">
			<button class="App_BNav_keyboard_hex_key_black"  value="1">1</button>
			<button class="App_BNav_keyboard_hex_key_black"  value="3">3</button>
			<div class="App_BNav_keyboard_invisible_key"></div>
			<button class="App_BNav_keyboard_hex_key_black"  value="6">6</button>
			<button class="App_BNav_keyboard_hex_key_black"  value="8">8</button>
			<button class="App_BNav_keyboard_hex_key_black"  value="10">10</button>
		</div>
		<div class="App_BNav_keyboard_container_whiteKeys">
			<button class="App_BNav_keyboard_hex_key_white"  value="0">0</button>
			<button class="App_BNav_keyboard_hex_key_white"  value="2">2</button>
			<button class="App_BNav_keyboard_hex_key_white"  value="4">4</button>
			<button class="App_BNav_keyboard_hex_key_white"  value="5">5</button>
			<button class="App_BNav_keyboard_hex_key_white"  value="7">7</button>
			<button class="App_BNav_keyboard_hex_key_white"  value="9">9</button>
			<button class="App_BNav_keyboard_hex_key_white"  value="11">11</button>
		</div>
	</div>
	<div class="App_BNav_keyboard_container">
		<div class="App_BNav_keyboard_container_blackKeys">
			<button class="App_BNav_keyboard_hex_key_black"  value="14">14</button>
			<button class="App_BNav_keyboard_hex_key_black"  value="16">16</button>
			<div class="App_BNav_keyboard_invisible_key"></div>
			<button class="App_BNav_keyboard_hex_key_black"  value="19">19</button>
			<button class="App_BNav_keyboard_hex_key_black"  value="21">21</button>
			<button class="App_BNav_keyboard_hex_key_black"  value="23">23</button>
		</div>
		<div class="App_BNav_keyboard_container_whiteKeys">
			<button class="App_BNav_keyboard_hex_key_white"  value="13">13</button>
			<button class="App_BNav_keyboard_hex_key_white"  value="15">15</button>
			<button class="App_BNav_keyboard_hex_key_white"  value="17">17</button>
			<button class="App_BNav_keyboard_hex_key_white"  value="18">18</button>
			<button class="App_BNav_keyboard_hex_key_white"  value="20">20</button>
			<button class="App_BNav_keyboard_hex_key_white"  value="22">22</button>
			<button class="App_BNav_keyboard_hex_key_white"  value="24">24</button>
		</div>
	</div>
	<div class="App_BNav_keyboard_container">
		<div class="App_BNav_keyboard_container_blackKeys">
			<button class="App_BNav_keyboard_hex_key_black"  value="1">1</button>
			<button class="App_BNav_keyboard_hex_key_black"  value="3">3</button>
			<div class="App_BNav_keyboard_invisible_key"></div>
			<button class="App_BNav_keyboard_hex_key_black"  value="6">6</button>
			<button class="App_BNav_keyboard_hex_key_black"  value="8">8</button>
			<button class="App_BNav_keyboard_hex_key_black"  value="10">10</button>
		</div>
		<div class="App_BNav_keyboard_container_whiteKeys">
			<button class="App_BNav_keyboard_hex_key_white"  value="0">0</button>
			<button class="App_BNav_keyboard_hex_key_white"  value="2">2</button>
			<button class="App_BNav_keyboard_hex_key_white"  value="4">4</button>
			<button class="App_BNav_keyboard_hex_key_white"  value="5">5</button>
			<button class="App_BNav_keyboard_hex_key_white"  value="7">7</button>
			<button class="App_BNav_keyboard_hex_key_white"  value="9">9</button>
			<button class="App_BNav_keyboard_hex_key_white"  value="11">11</button>
		</div>
	</div>
	<div class="App_BNav_keyboard_container">
		<div class="App_BNav_keyboard_container_blackKeys">
			<button class="App_BNav_keyboard_hex_key_black"  value="1">1</button>
			<button class="App_BNav_keyboard_hex_key_black"  value="3">3</button>
			<div class="App_BNav_keyboard_invisible_key"></div>
			<button class="App_BNav_keyboard_hex_key_black"  value="6">6</button>
			<button class="App_BNav_keyboard_hex_key_black"  value="8">8</button>
			<button class="App_BNav_keyboard_hex_key_black"  value="10">10</button>
		</div>
		<div class="App_BNav_keyboard_container_whiteKeys">
			<button class="App_BNav_keyboard_hex_key_white"  value="0">0</button>
			<button class="App_BNav_keyboard_hex_key_white"  value="2">2</button>
			<button class="App_BNav_keyboard_hex_key_white"  value="4">4</button>
			<button class="App_BNav_keyboard_hex_key_white"  value="5">5</button>
			<button class="App_BNav_keyboard_hex_key_white"  value="7">7</button>
			<button class="App_BNav_keyboard_hex_key_white"  value="9">9</button>
			<button class="App_BNav_keyboard_hex_key_white"  value="11">11</button>
		</div>
	</div>
	<div class="App_BNav_keyboard_container">
		<div class="App_BNav_keyboard_container_blackKeys">
			<button class="App_BNav_keyboard_hex_key_black"  value="1">1</button>
			<button class="App_BNav_keyboard_hex_key_black"  value="3">3</button>
			<div class="App_BNav_keyboard_invisible_key"></div>
			<button class="App_BNav_keyboard_hex_key_black"  value="6">6</button>
			<button class="App_BNav_keyboard_hex_key_black"  value="8">8</button>
			<button class="App_BNav_keyboard_hex_key_black"  value="10">10</button>
		</div>
		<div class="App_BNav_keyboard_container_whiteKeys">
			<button class="App_BNav_keyboard_hex_key_white"  value="0">0</button>
			<button class="App_BNav_keyboard_hex_key_white"  value="2">2</button>
			<button class="App_BNav_keyboard_hex_key_white"  value="4">4</button>
			<button class="App_BNav_keyboard_hex_key_white"  value="5">5</button>
			<button class="App_BNav_keyboard_hex_key_white"  value="7">7</button>
			<button class="App_BNav_keyboard_hex_key_white"  value="9">9</button>
			<button class="App_BNav_keyboard_hex_key_white"  value="11">11</button>
		</div>
	</div>
	<div class="App_BNav_keyboard_container">
		<div class="App_BNav_keyboard_container_blackKeys">
			<button class="App_BNav_keyboard_hex_key_black"  value="1">1</button>
			<button class="App_BNav_keyboard_hex_key_black"  value="3">3</button>
			<div class="App_BNav_keyboard_invisible_key"></div>
			<button class="App_BNav_keyboard_hex_key_black"  value="6">6</button>
			<button class="App_BNav_keyboard_hex_key_black"  value="8">8</button>
			<button class="App_BNav_keyboard_hex_key_black"  value="10">10</button>
		</div>
		<div class="App_BNav_keyboard_container_whiteKeys">
			<button class="App_BNav_keyboard_hex_key_white"  value="0">0</button>
			<button class="App_BNav_keyboard_hex_key_white"  value="2">2</button>
			<button class="App_BNav_keyboard_hex_key_white"  value="4">4</button>
			<button class="App_BNav_keyboard_hex_key_white"  value="5">5</button>
			<button class="App_BNav_keyboard_hex_key_white"  value="7">7</button>
			<button class="App_BNav_keyboard_hex_key_white"  value="9">9</button>
			<button class="App_BNav_keyboard_hex_key_white"  value="11">11</button>
		</div>
	</div>
	<div class="App_BNav_keyboard_container">
		<div class="App_BNav_keyboard_container_blackKeys">
			<button class="App_BNav_keyboard_hex_key_black"  value="1">1</button>
			<button class="App_BNav_keyboard_hex_key_black"  value="3">3</button>
			<div class="App_BNav_keyboard_invisible_key"></div>
			<button class="App_BNav_keyboard_hex_key_black"  value="6">6</button>
			<button class="App_BNav_keyboard_hex_key_black"  value="8">8</button>
			<button class="App_BNav_keyboard_hex_key_black"  value="10">10</button>
		</div>
		<div class="App_BNav_keyboard_container_whiteKeys">
			<button class="App_BNav_keyboard_hex_key_white"  value="0">0</button>
			<button class="App_BNav_keyboard_hex_key_white"  value="2">2</button>
			<button class="App_BNav_keyboard_hex_key_white"  value="4">4</button>
			<button class="App_BNav_keyboard_hex_key_white"  value="5">5</button>
			<button class="App_BNav_keyboard_hex_key_white"  value="7">7</button>
			<button class="App_BNav_keyboard_hex_key_white"  value="9">9</button>
			<button class="App_BNav_keyboard_hex_key_white"  value="11">11</button>
		</div>
	</div>
					
	`
	BNAV_set_virtual_piano_octaves()
}

function BNAV_clear_piano(){
	//clears the piano
	var container = document.querySelector('.App_BNav_keyboard_container')
	var blackContainer = container.querySelector('.App_BNav_keyboard_container_blackKeys')
	var whiteContainer = container.querySelector('.App_BNav_keyboard_container_whiteKeys')
	blackContainer.innerHTML=``
	whiteContainer.innerHTML=``
}

function BNAV_TET_virtual_piano(){
	//chromatic piano
	var container = document.querySelector('.App_BNav_keyboard_containerPiano')
	container.innerHTML=`
	<div class="App_BNav_keyboard_container">	
		<div class="App_BNav_keyboard_container_blackKeys">
		</div>
		<div class="App_BNav_keyboard_container_whiteKeys">
		</div>
	</div>
	
	`
	var kbContainers = document.querySelector('.App_BNav_keyboard_container')
	var blackArr =[]
	var whiteArr = []

	for(i=0; i<TET*7; i++){

		if(i%2 == 0){
			whiteArr.push(i)
		}else{
			blackArr.push(i)
		}
	}

	var blackContainer = kbContainers.querySelector('.App_BNav_keyboard_container_blackKeys')
	var whiteContainer = kbContainers.querySelector('.App_BNav_keyboard_container_whiteKeys')

	blackArr.forEach(el=>{
		var button = document.createElement('button')
		button.classList.add('App_BNav_keyboard_hex_key_black')
		button.value= el
		button.innerHTML=el
		button.setAttribute('onclick', 'BNAV_try_insert_note_duration(parseInt(this.value),keyboard_iT)')
		button.setAttribute('onmousedown','BNAV_play_note(this.value,true,1)')
		button.setAttribute('onmouseenter','BNAV_enter_kbTile(this.value,true,2)')
		button.setAttribute('onmouseup','BNAV_play_note(this.value,false,1)')
		button.setAttribute('onmouseleave','BNAV_exit_kbTile(this.value,false,2)')
		blackContainer.appendChild(button)
	})
	whiteArr.forEach(el=>{
		var button = document.createElement('button')
		button.classList.add('App_BNav_keyboard_hex_key_white')
		button.value= el
		button.innerHTML=el
		button.setAttribute('onclick', 'BNAV_try_insert_note_duration(parseInt(this.value),keyboard_iT)')
		button.setAttribute('onmousedown','BNAV_play_note(this.value,true,1)')
		button.setAttribute('onmouseenter','BNAV_enter_kbTile(this.value,true,2)')
		button.setAttribute('onmouseup','BNAV_play_note(this.value,false,1)')
		button.setAttribute('onmouseleave','BNAV_exit_kbTile(this.value,false,2)')
		whiteContainer.appendChild(button)
	})
}

function BNAV_keyboard_minimizeControls(bttn){
	//var midDiv = document.querySelector('.App_BNav_keyboard_optionsDivC')
	var rightDiv = document.querySelector('.App_BNav_keyboard_optionsDivR')
	var parent = rightDiv.parentNode
	
	if(bttn.value == 0){
		//midDiv.style.display='none'
		rightDiv.style.display='none'
		parent.style.width =`var(--RE_block)`
		bttn.classList.remove('App_BNav_tab_button_rotated')
		bttn.value =1
	} else{
		//midDiv.style.display='flex'
		rightDiv.style.display='flex'
		parent.style.width=`calc(var(--RE_block) * 12)`
		bttn.classList.add('App_BNav_tab_button_rotated')
		bttn.value =0
	}
}

function BNAV_keyboard_selectNoteTyping(bttn){
	var parent = bttn.parentNode
	var selected = parent.querySelector('.pressed')
	var showNote =document.querySelector('.App_BNav_keyboard_optionsDivL_showNote')
	showNote.innerHTML=bttn.innerHTML
	selected.classList.remove('pressed')
	bttn.classList.add('pressed')
	
	switch(bttn.value){
		case'mod':
			BNAV_pianoKeysToMod()
			BNAV_keyboard_keys_visualFeedback()
		break;
		case'abs':
			if(parent.parentNode.parentNode.querySelector('.App_BNav_keyboard_optionsSelectKeyboard_selectButtons_checked').innerHTML=='Piano'){
				
				BNAV_basic_virtual_piano()
				BNAV_keyboard_keys_visualFeedback()
			}else{							
				BNAV_TET_virtual_piano()
				BNAV_keyboard_keys_visualFeedback()
			}
		break;
	}
}

function BNAV_insRepSelect(bttn){
	//selects insert or replace
	var checked= document.querySelector('.App_BNav_keyboard_optionsHexInsRepChecked')
	checked.classList.remove('App_BNav_keyboard_optionsHexInsRepChecked')
	bttn.classList.add('App_BNav_keyboard_optionsHexInsRepChecked')
}

function BNAV_showKeyboardTypes(bttn){
	var keyboardDropdown=document.querySelector('.App_BNav_keyboard_optionsSelectKeyboard_container')

	if(bttn.value==0){
		keyboardDropdown.style.display='flex'
		bttn.classList.remove('App_BNav_tab_button_rotated')
		bttn.value=1
	}else{
		keyboardDropdown.style.display='none'
		bttn.classList.add('App_BNav_tab_button_rotated')
		bttn.value=0
	}
}

function BNAV_selectPiano(bttn){
	//select  regular piano
	cromaticKb=false
	var parent = bttn.parentNode
	parent.querySelector('.App_BNav_keyboard_optionsSelectKeyboard_selectButtons_checked').classList.remove('App_BNav_keyboard_optionsSelectKeyboard_selectButtons_checked')
	bttn.classList.add('App_BNav_keyboard_optionsSelectKeyboard_selectButtons_checked')
	BNAV_basic_virtual_piano()
	BNAV_showKeyboardTypes(parent.parentNode.querySelector('.App_BNav_keyboard_optionsSelectKeyboard_Button'))
	parent.parentNode.querySelector('.App_BNav_keyboard_optionsSelectKeyboard_P').innerHTML=bttn.innerHTML
	var absModParent = document.querySelector('.App_BNav_keyboard_optionsConfigDiv')
	var keyboardVisualFeedback= document.querySelector('.App_BNav_keyboard_container_selected_keys')
	keyboardVisualFeedback.style.width='388px'
	BNAV_keyboard_selectedKeysContainer_position()
	if(absModParent.querySelector('.App_button_mod ').classList.contains('pressed')){
		BNAV_pianoKeysToMod()
	}
}

function BNAV_selectCromatic(bttn){
	//select chromatic piano
	cromaticKb=true
	var parent = bttn.parentNode
	parent.querySelector('.App_BNav_keyboard_optionsSelectKeyboard_selectButtons_checked').classList.remove('App_BNav_keyboard_optionsSelectKeyboard_selectButtons_checked')
	bttn.classList.add('App_BNav_keyboard_optionsSelectKeyboard_selectButtons_checked')
	BNAV_TET_virtual_piano()
	BNAV_showKeyboardTypes(parent.parentNode.querySelector('.App_BNav_keyboard_optionsSelectKeyboard_Button'))
	parent.parentNode.querySelector('.App_BNav_keyboard_optionsSelectKeyboard_P').innerHTML=bttn.innerHTML
	var absModParent = document.querySelector('.App_BNav_keyboard_optionsConfigDiv')

	if(absModParent.querySelector('.App_button_mod ').classList.contains('pressed')){
		BNAV_pianoKeysToMod()
	}
	var keyboardVisualFeedback= document.querySelector('.App_BNav_keyboard_container_selected_keys')
	keyboardVisualFeedback.style.width='339.5px'
	BNAV_keyboard_selectedKeysContainer_position()
	BNAV_keyboard_keys_visualFeedback()
}

function BNAV_keyboard_selectiT(bttn){
	var parent = bttn.parentNode
	parent.querySelector('.App_BNav_keyboard_hex_key_iT_checked').classList.remove('App_BNav_keyboard_hex_key_iT_checked')
	bttn.classList.add('App_BNav_keyboard_hex_key_iT_checked')
	keyboard_iT=parseInt(bttn.value)
}
function BNAV_keyboard_lockiT(bttn){
	var noteBttn = document.querySelector('.App_BNav_keyboard_hex_key_note_lock_activated')
	var iTHexes = [...bttn.parentNode.getElementsByClassName('App_BNav_keyboard_hex_key_iT')]	
	if(noteBttn!=null){		
		noteBttn.value=0
		keyboard_notes_lock=false
		noteBttn.classList.remove('App_BNav_keyboard_hex_key_note_lock_activated')
		noteBttn.childNodes[0].src='./Icons/lock_open_icon.svg'
		var noteParent=document.querySelector('.App_BNav_keyboard_containerPiano')
		var lockedHexes = [...noteParent.getElementsByClassName('App_BNav_keyboard_hex_key_noteLocked')]
		lockedHexes.forEach((hex)=>{			
			hex.classList.remove('App_BNav_keyboard_hex_key_noteLocked')	
		})
		iTHexes.forEach((hex,index)=>{
			if(index!=0){				
				hex.setAttribute('onClick',`BNAV_keyboard_selectiT(this)`)	
			}		
		})
	}
	if(bttn.value==0){
		bttn.value=1
		keyboard_iT_lock=true
		bttn.classList.add('App_BNav_keyboard_hex_key_iT_lock_activated')
		bttn.childNodes[0].src='./Icons/lock_open_closed_icon.svg'			
		iTHexes.forEach((hex,index)=>{
			if(index!=0){
				hex.classList.add('App_BNav_keyboard_hex_key_iT_locked')				
			}			
		})
	}else{
		bttn.value=0
		keyboard_iT_lock=false
		bttn.classList.remove('App_BNav_keyboard_hex_key_iT_lock_activated')
		bttn.childNodes[0].src='./Icons/lock_open_icon.svg'			
		iTHexes.forEach((hex,index)=>{
			if(index!=0){
				hex.classList.remove('App_BNav_keyboard_hex_key_iT_locked')				
			}			
		})
	}
	
}
function BNAV_keyboard_lockNotes(bttn){
	var iTbttn = document.querySelector('.App_BNav_keyboard_hex_key_iT_lock_activated')
	var iTParent= document.querySelector('.App_BNav_keyboard_container_iTKeys')		
	var iTHexes = [...iTParent.getElementsByClassName('App_BNav_keyboard_hex_key_iT')]	
	if(iTbttn!=null){		
		iTbttn.value=0
		keyboard_iT_lock=false
		iTbttn.classList.remove('App_BNav_keyboard_hex_key_iT_lock_activated')
		iTbttn.childNodes[0].src='./Icons/lock_open_icon.svg'					
		iTHexes.forEach((hex,index)=>{
			if(index!=0){
				hex.classList.remove('App_BNav_keyboard_hex_key_iT_locked')		
				
			}			
		})
	}
	var noteParent=document.querySelector('.App_BNav_keyboard_containerPiano')
	if(bttn.value==0){
		bttn.value=1
		keyboard_notes_lock=true
		bttn.classList.add('App_BNav_keyboard_hex_key_note_lock_activated')
		bttn.childNodes[0].src='./Icons/lock_open_closed_icon.svg'
		var whiteHexes = [...noteParent.getElementsByClassName('App_BNav_keyboard_hex_key_white')]
		var blackHexes = [...noteParent.getElementsByClassName('App_BNav_keyboard_hex_key_black')]
		whiteHexes.forEach((hex)=>{			
				hex.classList.add('App_BNav_keyboard_hex_key_noteLocked')	
		})
		blackHexes.forEach((hex)=>{			
				hex.classList.add('App_BNav_keyboard_hex_key_noteLocked')	
		})
		iTHexes.forEach((hex,index)=>{
			if(index!=0){					
				hex.setAttribute('onClick',`BNAV_try_insert_note_duration(0,${hex.value})`)
				/* hex.onclick = function(){
					BNAV_try_insert_note_duration(0,hex.value)
				} */
				
				//console.log(hex.onclick)				
			}			
		})
	}else{
		bttn.value=0
		keyboard_notes_lock=false
		bttn.classList.remove('App_BNav_keyboard_hex_key_note_lock_activated')
		bttn.childNodes[0].src='./Icons/lock_open_icon.svg'
		var lockedHexes = [...noteParent.getElementsByClassName('App_BNav_keyboard_hex_key_noteLocked')]
		lockedHexes.forEach((hex)=>{
			hex.classList.remove('App_BNav_keyboard_hex_key_noteLocked')
		})
		iTHexes.forEach((hex,index)=>{
			if(index!=0){
				hex.setAttribute('onClick',`BNAV_keyboard_selectiT(this)`)
				
			}
		})
	}
	
}

function BNAV_keyboard_inputiT(bttn){
	var parent=bttn.parentNode
	parent.value=bttn.value
	keyboard_iT=parseInt(bttn.value)
}

function BNAV_try_insert_note_duration(note,duration_iT){
	if(keyboard_iT_lock){
		duration_iT=0
	}	
	//console.log(current_position_PMCRE)
	//find voice_index pulse start and fraction start FIRE
	if(current_position_PMCRE.segment_index==null)return
	
	//console.log(datatest)
	if (tabPMCbutton.checked){
		//PMC
		//verify current_position_PMCRE is the one showed
		if(current_position_PMCRE.voice_data.selected){
			DATA_overwrite_new_object_PA_iT(current_position_PMCRE.voice_data.voice_index,current_position_PMCRE.segment_index,current_position_PMCRE.pulse,current_position_PMCRE.fraction,duration_iT,note)
		}
	}else{
		// RE
		console.log(current_position_PMCRE.pulse)
		if(current_position_PMCRE.pulse==null){
			//console.error("blink")
			var elements_list=[...document.querySelectorAll(".animate_play_green_light")]
			elements_list.forEach(item=>{APP_blink_error(item)})
			return
		}
		var focus_column = false
		var focusGreen = document.querySelector(".animate_play_green_light")
		if(focusGreen==null)return
		if(focusGreen.value==""){
			focus_column=true
		}
		
		
		if(keyboard_notes_lock==false){
			current_position_PMCRE.note=note
			DATA_overwrite_new_object_PA_iT(current_position_PMCRE.voice_data.voice_index,current_position_PMCRE.segment_index,current_position_PMCRE.pulse,current_position_PMCRE.fraction,duration_iT,note)
			if(focus_column){
				var elements_list=[...document.querySelectorAll(".animate_play_green_light")]
				elements_list.forEach(item=>{
					var [previous_item,,]=RE_find_prev_filled_element(item,".App_RE_inp_box")
					APP_blink_good(previous_item)
				})
			}
		}else{
			var timeArr =timeIndex =  current_position_PMCRE.voice_data.data.segment_data[current_position_PMCRE.segment_index].time
		
			var timeIndex =  timeArr.findIndex(function(thePulse){
				return thePulse.P==current_position_PMCRE.pulse
			})
			var prevNote = current_position_PMCRE.voice_data.data.segment_data[current_position_PMCRE.segment_index].note[timeIndex]
			current_position_PMCRE.note=prevNote
			var RE_voiceData = [...document.getElementsByClassName('App_RE_voice')]
			//var theIndex = RE_voiceData.length - 1 - current_position_PMCRE.voice_data.voice_index
			//console.log(current_position_PMCRE.voice_data.name)
			//RE_voiceData.forEach(voice => console.log(voice.querySelector('.App_RE_voice_name')))
			var selectedVoice = RE_voiceData.filter(voice => voice.querySelector('.App_RE_voice_name').value == current_position_PMCRE.voice_data.name)[0]
			var iTinput = selectedVoice.querySelector('.App_RE_iT').querySelector('.animate_play_green_light')
			if(focus_column){
				var elements_list=[...document.querySelectorAll(".animate_play_green_light")]
				elements_list.forEach(item=>{
					var [previous_item,,]=RE_find_prev_filled_element(item,".App_RE_inp_box")
					APP_blink_good(previous_item)
				})
			}
			previous_value=iTinput.value		
			iTinput.value=duration_iT
			RE_enter_new_value_dT(iTinput,'iT')
			
			
			
			
			//console.log(RE_voiceData.childNodes[theIndex])
			//RE_voiceData[RE_voiceData.length-1-current_position_PMCRE.voice_data.voice_index].style.display='none'
			//DATA_modify_object_index_iT(current_position_PMCRE.voice_data.voice_index,current_position_PMCRE.segment_index,current_position_PMCRE.pulse-1,duration_iT)
			//DATA_overwrite_new_object_PA_iT(current_position_PMCRE.voice_data.voice_index,current_position_PMCRE.segment_index,current_position_PMCRE.pulse,current_position_PMCRE.fraction,duration_iT,current_position_PMCRE.note)
		}
		//console.log(current_position_PMCRE.note)
		
		

		/* if(focus_column){
			var elements_list=[...document.querySelectorAll(".animate_play_green_light")]
			elements_list.forEach(item=>{
				var [previous_item,,]=RE_find_prev_filled_element(item,".App_RE_inp_box")
				APP_blink_good(previous_item)
			})
		} */
	}
}


function BNAV_Refresh_Keyboard_On_TET_Change(){
	var bttnParent = document.querySelector('.App_BNav_keyboard_optionsSelectKeyboard_container')
	var menuParent = document.querySelector('.App_BNav_keyboard_optionsSelectKeyboard')

	if(TET!=12){
		BNAV_showKeyboardTypes(menuParent.querySelector('.App_BNav_keyboard_optionsSelectKeyboard_Button '))
		BNAV_selectCromatic(bttnParent.childNodes[3])
		bttnParent.childNodes[1].setAttribute('disabled','')
	}else{
		BNAV_selectPiano(bttnParent.childNodes[1])
		BNAV_showKeyboardTypes(menuParent.querySelector('.App_BNav_keyboard_optionsSelectKeyboard_Button '))

		if(bttnParent.childNodes[1].disabled){
			bttnParent.childNodes[1].removeAttribute('disabled')
		}
	}
}

let mouseDown = false;
document.body.onmousedown = () => {
	mouseDown = true;
};
document.body.onmouseup = () => {
	mouseDown = false;
};

function BNAV_play_note( nzc_note , down , volume){
	APP_connect_first_instrument()
	volume= 0.35
	var freq_note = APP_note_to_Hz(nzc_note,TET)
	var quantization = false
	var instrument_index = 0
	var windowTabPMC = document.getElementById('App_BNav_tab_PMC')
	
	if(windowTabPMC.checked){
		var  selectedData = DATA_get_selected_voice_data()		
		instrument_index=selectedData.instrument
	}else{
		var focused_elem = document.querySelector('.animate_play_green_light')
		if(focused_elem!=null ){			
			var voice = focused_elem.closest(".App_RE_voice")
			instrument_index = RE_which_voice_instrument(voice)				
			}		
		}

	if(down){
		//play note
		var now = Tone.immediate()
		//if not drum XXX
		if(instrument_index!=1){
			instrument[instrument_index].triggerAttack(freq_note, now, volume)
		}else{
			if(nzc_note>=23 && nzc_note<= 69)instrument[instrument_index].player(nzc_note).start(now, 0, 1, 0, volume)
		}
	}else{
		var now = Tone.now()
		//if not drum XXX
		if(instrument_index!=1){
			instrument[instrument_index].triggerRelease(freq_note, now)
		}
	}
}

function BNAV_enter_kbTile(nzc_note, down, volume){
	if(mouseDown){
		BNAV_play_note( nzc_note , down , volume)
	}
}

function BNAV_exit_kbTile(nzc_note, down, volume){
	if(mouseDown){
		BNAV_play_note( nzc_note , down , volume)
	}
}

function BNAV_keyboard_increase_note(bttn){
	if(virtualPianoOctave==6){
		return
	}else if(virtualPianoNote==TET-1){
		BNAV_keyboard_clear_keys_visualFeedback()
		var VPOctaveInput = document.getElementById('App_BNav_keyboard_octave')
		virtualPianoNote=0
		virtualPianoOctave++
		VPOctaveInput.value=`${virtualPianoNote}r${virtualPianoOctave}`
		BNAV_keyboard_keys_visualFeedback()
		BNAV_keyboard_selectedKeysContainer_position()
	}else{
		BNAV_keyboard_clear_keys_visualFeedback()
		var VPOctaveInput = document.getElementById('App_BNav_keyboard_octave')
		virtualPianoNote++
		VPOctaveInput.value=`${virtualPianoNote}r${virtualPianoOctave}`
		BNAV_keyboard_keys_visualFeedback()
		BNAV_keyboard_selectedKeysContainer_position()
	}
}

function BNAV_keyboard_decrease_note(bttn){
	if(virtualPianoOctave==0 && virtualPianoNote==0){
		return
	}else if(virtualPianoNote==0){
		BNAV_keyboard_clear_keys_visualFeedback()
		var VPOctaveInput = document.getElementById('App_BNav_keyboard_octave')
		virtualPianoNote=TET-1
		virtualPianoOctave--
		VPOctaveInput.value=`${virtualPianoNote}r${virtualPianoOctave}`
		BNAV_keyboard_keys_visualFeedback()
		BNAV_keyboard_selectedKeysContainer_position()
	}else{
		var VPOctaveInput = document.getElementById('App_BNav_keyboard_octave')
		BNAV_keyboard_clear_keys_visualFeedback()
		virtualPianoNote--
		VPOctaveInput.value=`${virtualPianoNote}r${virtualPianoOctave}`
		BNAV_keyboard_keys_visualFeedback()
		BNAV_keyboard_selectedKeysContainer_position()
	}
}
	


function BNAV_keyboard_keys_visualFeedback(){
	var buttonMidi = document.getElementById("compKeyBttn")
	
	
	if(buttonMidi.getAttribute('data-value')==true){
	var keysParent = [...document.querySelectorAll('.App_BNav_keyboard_container')]

	if(cromaticKb==false){
		var whites = [...keysParent[virtualPianoOctave].querySelector('.App_BNav_keyboard_container_whiteKeys').querySelectorAll('.App_BNav_keyboard_hex_key_white')]
		var blacks =[...keysParent[virtualPianoOctave].querySelector('.App_BNav_keyboard_container_blackKeys').querySelectorAll('.App_BNav_keyboard_hex_key_black')]		
		if(virtualPianoOctave!=6){
			var whites2 = [...keysParent[virtualPianoOctave+1].querySelector('.App_BNav_keyboard_container_whiteKeys').querySelectorAll('.App_BNav_keyboard_hex_key_white')]
			var blacks2 = [...keysParent[virtualPianoOctave+1].querySelector('.App_BNav_keyboard_container_blackKeys').querySelectorAll('.App_BNav_keyboard_hex_key_black')]
		}
		var whiteArrKey=['a','s','d','f','g','h','j','k','l','ñ']
		var blackArrKey=['q','w','e','r','t','y','u','i','o','p']

		if(virtualPianoNote==0){
			blackArrKey=['w','r','t','y','u']
			if(virtualPianoOctave!=6){
				whites.push(whites2[0])	
			}	
		}else if(virtualPianoNote==1){
			whites.shift()
			whites.push(whites2[0])
			blacks.push(blacks2[0])
			blackArrKey=['q','w','r','t','y','i']
		}else if(virtualPianoNote==2){
			whites.shift()
			whites.push(whites2[0],whites2[1])
			blackArrKey=['w','r','t','y','i']
			blacks.shift()
			blacks.push(blacks2[0])
		}else if(virtualPianoNote==3){
			whites.splice(0,2)
			whites.push(whites2[0],whites2[1])
			blackArrKey=['q','e','r','t','u','i']
			blacks.shift()
			blacks.push(blacks2[0],blacks2[1])
		}else if(virtualPianoNote==4){
			whites.splice(0,2)
			whites.push(whites2[0],whites2[1],whites2[2])
			console.log(whites)
			blackArrKey=['e','r','t','u','i']
			blacks.splice(0,2)
			blacks.push(blacks2[0],blacks2[1])
		}else if(virtualPianoNote==5){
			whites.splice(0,3)
			whites.push(whites2[0],whites2[1],whites2[2],whites2[3])
			blackArrKey=['w','e','r','y','u']
			blacks.splice(0,2)
			blacks.push(blacks2[0],blacks2[1])
		}else if(virtualPianoNote==6){
			whites.splice(0,4)
			whites.push(whites2[0],whites2[1],whites2[2],whites2[3])
			blackArrKey=['q','w','e','t','y','i']
			blacks.splice(0,2)
			blacks.push(blacks2[0],blacks2[1],blacks2[2])
		}else if(virtualPianoNote==7){
			whites.splice(0,4)
			whites.push(whites2[0],whites2[1],whites2[2],whites2[3],whites2[4])
			blackArrKey=['w','e','t','y','i']
			blacks.splice(0,3)
			blacks.push(blacks2[0],blacks2[1],blacks2[2])
		}else if(virtualPianoNote==8){
			whites.splice(0,5)
			whites.push(whites2[0],whites2[1],whites2[2],whites2[3],whites2[4])	
			blackArrKey=['q','w','r','t','u','i']
			blacks.splice(0,3)
			blacks.push(blacks2[0],blacks2[1],blacks2[2],blacks2[3])
		}else if(virtualPianoNote==9){
			whites.splice(0,5)
			whites.push(whites2[0],whites2[1],whites2[2],whites2[3],whites2[4],whites2[5])
			blackArrKey=['w','r','t','u','i']
			blacks.splice(0,4)
			blacks.push(blacks2[0],blacks2[1],blacks2[2],blacks2[3])
		}else if(virtualPianoNote==10){
			whites.splice(0,6)
			whites.push(whites2[0],whites2[1],whites2[2],whites2[3],whites2[4],whites2[5])	
			blackArrKey=['q','e','r','y','u','i']
			blacks.splice(0,4)
			blacks.push(blacks2[0],blacks2[1],blacks2[2],blacks2[3],blacks2[4])
		}else{
			whites.splice(0,6)
			whites.push(whites2[0],whites2[1],whites2[2],whites2[3],whites2[4],whites2[5],whites2[6])
			blackArrKey=['e','r','y','u','i']
			blacks=blacks2
		}

		whites.forEach((key,index)=>{
			key.innerHTML+=`<p>${whiteArrKey[index]}</p>`
		})
		blacks.forEach((key,index)=>{
			key.innerHTML+=`<p>${blackArrKey[index]}</p>`
		})
	} else{
		var whiteArrKey=['a','s','d','f','g','h','j','k','l','ñ']
		var blackArrKey=['q','w','e','r','t','y','u','i','o','p']
		if(virtualPianoOctave!=6){
			if(virtualPianoNote%2==0){
				for(i=1;i<=6;i++){
					//keysParent[0].querySelector('.App_BNav_keyboard_container_whiteKeys').childNodes[virtualPianoOctave*6+virtualPianoNote/2+i].innerHTML+=`<p>${whiteArrKey[i-1]}</p>`
					keysParent[0].querySelector('.App_BNav_keyboard_container_blackKeys').childNodes[virtualPianoOctave*6+virtualPianoNote/2+i].innerHTML+=`<p>${blackArrKey[i]}</p>`
				}
				for(i=1;i<=7;i++){
					keysParent[0].querySelector('.App_BNav_keyboard_container_whiteKeys').childNodes[virtualPianoOctave*6+virtualPianoNote/2+i].innerHTML+=`<p>${whiteArrKey[i-1]}</p>`
					//keysParent[0].querySelector('.App_BNav_keyboard_container_blackKeys').childNodes[virtualPianoOctave*6+virtualPianoNote/2+i].innerHTML+=`<p>${blackArrKey[i]}</p>`
				}
			}else {
				for(i=0;i<=5;i++){
					keysParent[0].querySelector('.App_BNav_keyboard_container_whiteKeys').childNodes[virtualPianoOctave*6+virtualPianoNote/2+0.5+i+1].innerHTML+=`<p>${whiteArrKey[i]}</p>`
					//keysParent[0].querySelector('.App_BNav_keyboard_container_blackKeys').childNodes[virtualPianoOctave*6+virtualPianoNote/2+0.5+i].innerHTML+=`<p>${blackArrKey[i]}</p>`
				}
				for(i=0;i<=6;i++){
					//keysParent[0].querySelector('.App_BNav_keyboard_container_whiteKeys').childNodes[virtualPianoOctave*6+virtualPianoNote/2+0.5+i+1].innerHTML+=`<p>${whiteArrKey[i]}</p>`
					keysParent[0].querySelector('.App_BNav_keyboard_container_blackKeys').childNodes[virtualPianoOctave*6+virtualPianoNote/2+0.5+i].innerHTML+=`<p>${blackArrKey[i]}</p>`
				}
			}
		}else{
			for(i=1;i<=6;i++){
				keysParent[0].querySelector('.App_BNav_keyboard_container_whiteKeys').childNodes[virtualPianoOctave*6+virtualPianoNote/2+i].innerHTML+=`<p>${whiteArrKey[i-1]}</p>`
				keysParent[0].querySelector('.App_BNav_keyboard_container_blackKeys').childNodes[virtualPianoOctave*6+virtualPianoNote/2+i].innerHTML+=`<p>${blackArrKey[i]}</p>`
			}
		}
	}
	}else{	
		return
	}
}

function BNAV_keyboard_clear_keys_visualFeedback(){
	var keysParent = [...document.querySelectorAll('.App_BNav_keyboard_container')]
	var buttonMidi = document.getElementById("compKeyBttn")
	
	
	if(buttonMidi.getAttribute('data-value')==true){
	if(cromaticKb==false){
		var whites = [...keysParent[virtualPianoOctave].querySelector('.App_BNav_keyboard_container_whiteKeys').querySelectorAll('.App_BNav_keyboard_hex_key_white')]
		var blacks = [...keysParent[virtualPianoOctave].querySelector('.App_BNav_keyboard_container_blackKeys').querySelectorAll('.App_BNav_keyboard_hex_key_black')]
		if(virtualPianoOctave!=6){
			var whites2 = [...keysParent[virtualPianoOctave+1].querySelector('.App_BNav_keyboard_container_whiteKeys').querySelectorAll('.App_BNav_keyboard_hex_key_white')]
			var blacks2 = [...keysParent[virtualPianoOctave+1].querySelector('.App_BNav_keyboard_container_blackKeys').querySelectorAll('.App_BNav_keyboard_hex_key_black')]
		}
		if(virtualPianoNote==0){
			if(virtualPianoOctave!=6){
			whites.push(whites2[0])
			}
		}else if(virtualPianoNote==1){
			blacks.push(blacks2[0])
			whites.shift()
			whites.push(whites2[0])
		}else if(virtualPianoNote==2){
			whites.shift()
			whites.push(whites2[0],whites2[1])
			blacks.shift()
			blacks.push(blacks2[0])
		}else if(virtualPianoNote==3){
			whites.splice(0,2)
			whites.push(whites2[0],whites2[1])
			blacks.shift()
			blacks.push(blacks2[0],blacks2[1])			
		}else if(virtualPianoNote==4){
			whites.splice(0,2)
			whites.push(whites2[0],whites2[1],whites2[2])
			blacks.splice(0,2)
			blacks.push(blacks2[0],blacks2[1])
		}else if(virtualPianoNote==5){
			whites.splice(0,3)
			whites.push(whites2[0],whites2[1],whites2[2],whites2[3])
			blacks.splice(0,2)
			blacks.push(blacks2[0],blacks2[1])
		}else if(virtualPianoNote==6){
			whites.splice(0,4)
			whites.push(whites2[0],whites2[1],whites2[2],whites2[3])
			blacks.splice(0,2)
			blacks.push(blacks2[0],blacks2[1],blacks2[2])
		}else if(virtualPianoNote==7){
			whites.splice(0,4)
			whites.push(whites2[0],whites2[1],whites2[2],whites2[3],whites2[4])
			blacks.splice(0,3)
			blacks.push(blacks2[0],blacks2[1],blacks2[2])
		}else if(virtualPianoNote==8){
			whites.splice(0,5)
			whites.push(whites2[0],whites2[1],whites2[2],whites2[3],whites2[4])	
			blacks.splice(0,3)
			blacks.push(blacks2[0],blacks2[1],blacks2[2],blacks2[3])
		}else if(virtualPianoNote==9){
			whites.splice(0,5)
			whites.push(whites2[0],whites2[1],whites2[2],whites2[3],whites2[4],whites2[5])	
			blacks.splice(0,4)
			blacks.push(blacks2[0],blacks2[1],blacks2[2],blacks2[3])
		}else if(virtualPianoNote==10){
			whites.splice(0,6)
			whites.push(whites2[0],whites2[1],whites2[2],whites2[3],whites2[4],whites2[5])
			blacks.splice(0,4)
			blacks.push(blacks2[0],blacks2[1],blacks2[2],blacks2[3],blacks2[4])
		}else if(virtualPianoNote==11){
			whites.splice(0,6)
			whites.push(whites2[0],whites2[1],whites2[2],whites2[3],whites2[4],whites2[5],whites2[6])
			blacks=blacks2
		}
		
		whites.forEach((key,index)=>{
			var newStr = key.innerHTML.slice(0,-8)
			key.innerHTML= newStr
		})
		blacks.forEach((key,index)=>{
			var newStr = key.innerHTML.slice(0,-8)
			key.innerHTML= newStr
		})
	}else{
		if(virtualPianoOctave!=6){
			if(virtualPianoNote%2==0){
			for(i=1;i<=6;i++){				
				let blackKey = keysParent[0].querySelector('.App_BNav_keyboard_container_blackKeys').childNodes[virtualPianoOctave*6+virtualPianoNote/2+i]
				let newBlackKey = blackKey.innerHTML.slice(0,-8)
				blackKey.innerHTML= newBlackKey
				}
			for(i=1;i<=7;i++){
				let whiteKey = keysParent[0].querySelector('.App_BNav_keyboard_container_whiteKeys').childNodes[virtualPianoOctave*6+virtualPianoNote/2+i]
				let newWhiteStr = whiteKey.innerHTML.slice(0,-8)
				whiteKey.innerHTML= newWhiteStr			
				}
			}else{
				for(i=1;i<=6;i++){
					let whiteKey = keysParent[0].querySelector('.App_BNav_keyboard_container_whiteKeys').childNodes[virtualPianoOctave*6+virtualPianoNote/2+0.5+i]
					let newWhiteStr = whiteKey.innerHTML.slice(0,-8)
					whiteKey.innerHTML= newWhiteStr				
					}
				for(i=1;i<=7;i++){				
					let blackKey = keysParent[0].querySelector('.App_BNav_keyboard_container_blackKeys').childNodes[virtualPianoOctave*6+virtualPianoNote/2+0.5+i-1]
					let newBlackKey = blackKey.innerHTML.slice(0,-8)
					blackKey.innerHTML= newBlackKey
					}
			}
		}else{
			for(i=1;i<=6;i++){
				let whiteKey = keysParent[0].querySelector('.App_BNav_keyboard_container_whiteKeys').childNodes[virtualPianoOctave*6+virtualPianoNote/2+i]
				let newWhiteStr = whiteKey.innerHTML.slice(0,-8)
				whiteKey.innerHTML= newWhiteStr
				let blackKey = keysParent[0].querySelector('.App_BNav_keyboard_container_blackKeys').childNodes[virtualPianoOctave*6+virtualPianoNote/2+i]
				let newBlackKey = blackKey.innerHTML.slice(0,-8)
				blackKey.innerHTML= newBlackKey
				}
		}
	}
	}else{
		console.log('nada 2')
		return
	}
}

function BNAV_keyboard_selectedKeysContainer_position(){
	let container = document.querySelector('.App_BNav_keyboard_container_selected_keys')
	let hexwidth = 48.5
	let desviation = 0
	var position = 0
	if(cromaticKb==false){
		if(virtualPianoNote>0&&virtualPianoNote<5){
			desviation=0
		}else if(virtualPianoNote>4){
			desviation=1
		}
		
		position = hexwidth*7*virtualPianoOctave+hexwidth/2*(virtualPianoNote+desviation)
		
	}else{		
		position =hexwidth*virtualPianoOctave*6+hexwidth/2*virtualPianoNote
	}
	container.style.left=`${position}px`
}

function BNAV_keyboard_lock_functions_TET_change(){
	var visualizationDiv = document.querySelector('.App_BNav_keyboard_container_selected_keys')
	var kbButton = document.getElementById('compKeyBttn')
	var octaveButtons = [...document.getElementsByClassName('App_Bnav_keyboard_options_IncDecOctave')]
	var noteButtons = [...document.getElementsByClassName('App_Bnav_keyboard_options_IncDecNote')]
	
	visualizationDiv.style.display='none'
	if(kbButton.getAttribute('data-value') == 1){
		Switch_keyboard_as_MIDI_controller()
	}
	kbButton.disabled = true
	octaveButtons.forEach(element => {
		element.disbaled=true
	});
	noteButtons.forEach(element => {
		element.disbaled=true
	});
}
function BNAV_keyboard_unlock_functions_TET_change(){
	var visualizationDiv = document.querySelector('.App_BNav_keyboard_container_selected_keys')
	var kbButton = document.getElementById('compKeyBttn')
	var octaveButtons = [...document.getElementsByClassName('App_Bnav_keyboard_options_IncDecOctave')]
	var noteButtons = [...document.getElementsByClassName('App_Bnav_keyboard_options_IncDecNote')]	
	visualizationDiv.style.display='flex'	
	kbButton.disabled = false
	octaveButtons.forEach(element => {
		element.disbaled=false
	});
	noteButtons.forEach(element => {
		element.disbaled=false
	});
}
