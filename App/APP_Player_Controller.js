//Player Controller

function APP_updateTime() {
	requestAnimationFrame(APP_updateTime)
	//the time elapsed in seconds

	if(Tone.Transport.state=="stopped"){
		//output_timer_total.value = `00' 00"`
		output_timer_total.value = APP_convertToTime(Tone.Transport.seconds.toFixed(2))
	} else {
		var total_time = Tone.Transport.seconds.toFixed(2)
		if(total_time>=0){
			// var timeSignature = Tone.Transport.timeSignature
			// var loop_n = Math.floor(total_time/(timeSignature*(60/PPM)))
			// console.log(loop_n)
			// var time = total_time - loop_n * (timeSignature*(60/PPM))
			var time = total_time //BC using transport loop !
			APP_draw_cursor_minimap_canvas_onPLay(time)
			PMC_draw_progress_line_time(time)
			output_timer_total.value = APP_convertToTime(time)

			if(Tone.Transport.state=="started"){
				//calculate PMC best position
				PMC_calculate_best_scrollbar_position(time)
			}
		}
	}
}

APP_updateTime()
function APP_convertToTime(value){
	var min = Math.floor(value/60)
	var sec = Math.floor(value)-min*60
	if(sec<10)sec="0"+sec
	if(min<10)min="0"+min
	return min+"' "+sec+'"'
}

function APP_player_to_time(time,pulse_abs=null,pulse=null,fraction=null,segment_index=null,voice_index=null){
	//progress bar only in pulse
	APP_move_progress_bar(pulse_abs)

	//RE position

	if(tabREbutton.checked){
		RE_highlight_columns_at_pulse(pulse_abs,time,voice_index)
	}else{
		//PMC_position??? XXX check if current voice is selected???
		PMC_draw_progress_line_time(time)
	}

	//RE and PMC focus
	//current_position_PMCRE.X_start = null
	//current_position_PMCRE.Y_start = null
	current_position_PMCRE.segment_index = segment_index
	current_position_PMCRE.pulse = pulse
	current_position_PMCRE.fraction = fraction
	//current_position_PMCRE.nextREobj = false //XXX

	//current_position_PMCRE.note = null  //no  handy for drag drop PMC
	//console.log(current_position_PMCRE)


	//transport
	Tone.Transport.seconds = time

	//minimap
	//APP_draw_cursor_minimap_canvas(pulse_abs)
	//better
	APP_draw_cursor_minimap_canvas_onPLay(time)
}

function APP_sequencer(){
	//in freq
	hz=PPM/60
	pulsationFrequency=hz+"hz"

	//change to database
	//a copy of database
	var current_data = DATA_get_current_state_point(false)

	var voice_solo = current_data.voice_data.filter(voice=>{
		return voice.solo==true
	})

	var voice_list_unmuted_time_data = []

	if(voice_solo!=null&&voice_solo.length!=0){
		//there is a solo
		voice_solo.forEach(element=>{
			if(element.volume!="0"){
				voice_list_unmuted_time_data.push(element)
			}
		})

	}else{
		//find other not muted voices
		voice_list_unmuted_time_data = current_data.voice_data.filter(voice=>{
			return voice.volume!="0"
		})
	}

	var compas_sequence = BNAV_read_compas_accents_sequence()

	stopper.dispose()
	stopper = new Tone.Part(function(time, value){
		if(value.end){
			//stop the reproduction
			APP_stop()
			var [now,start,end,max]=APP_return_progress_bar_values()
			APP_move_progress_bar(start)
			APP_reset_blink_play()
		}else{
			//what time is it? Time for you to shut up
			APP_reset_blink_play()
			//clear all the PMC notes
			PMC_reset_playing_notes()
		}
	})

	//use of Tone.Part (more complete) or Tone.Sequence(simpler, no metronome)
	//defined in global variables
	part.dispose()
	part = new Tone.Part(function(time, value){
		//the notes given as the second element in the array
		//will be passed in as the second argument
		//value.note
		//0->4 fractioning
		//>4 hz note
		//value.durationHz

		//value.inst
		//0 to 4

		//animations and actions
		if(value.action == "move_progress_bar"){
			//animation start
			Tone.Draw.schedule((time) => {
				//do drawing or DOM manipulation here
	 			APP_move_progress_bar(value.pulse)
				return
			}, time)
			return
		}

		if(value.action == "animation_on_RE"){
			//animation start
			Tone.Draw.schedule((time) => {
				//do drawing or DOM manipulation here
				RE_hightlight_column_on_play_on(value.segment_obj,value.column_index)
			}, time)
			return
		}

		if(value.action == "animation_off_RE"){
			Tone.Draw.schedule((time) => {
				//do drawing or DOM manipulation here
				RE_hightlight_column_on_play_off(value.segment_obj,value.column_index)
			}, time)
			return
		}

		if(value.action == "animation_off_PMC"){
			Tone.Draw.schedule((time) => {
				//do drawing or DOM manipulation here
				PMC_remove_note_from_playingNotesArr(value.note_data)
			}, time)
			return
		}

		if(value.action == "animation_on_PMC"){
			//animation start
			Tone.Draw.schedule((time) => {
				//do drawing or DOM manipulation here
				/* var noteAbs = APP_Hz_to_note(value.note,TET)
				var note_data = {note:noteAbs,time:value.time,duration:value.duration,voice:'Voice1', color:'green'} */
				PMC_add_note_to_playingNoteArr(value.note_data)
			}, time)
			return
		}

		if(value.action == "RE_movement"){
			Tone.Draw.schedule((time) => {
				//do drawing or DOM manipulation here
	 			RE_calculate_best_scrollbar_position(value.index_column)
			}, time)
			return
		}

		//sounds
		var vel = Math.random() * 0.5 + 0.5;
		//suona la nota nel gruppo di note keys al pulso "time" calcolato mediante una frequenza di pulsazione
		if(value.note>0 && value.note<=5){
			//fractioned metronome
			//volume
			var vol = -1
			//adjust volume for Claves
			if (value.note == 1){
				vol += 2
			}
			//adjust volume for Wood-Block-Med
			if (value.note == 4){
				vol += 5
			}
			keysFractionedMetronome.player(value.note).volume.value = vol;
			//console.log(vol)
			keysFractionedMetronome.player(value.note).start(time, 0, value.durationHz, 0, vel)
		}else{
			//main metronome (compass, accents) 0.1 is duration
			//vel is due to main volume and value.volume
			if(value.note=="Accent"){
				keysMainMetronome.player("Accent").start(time, 0, 0.1, 0, 1)
				return
			}
			if(value.note=="Accent_Weak"){
				keysMainMetronome.player("Accent_Weak").start(time, 0, 0.1, 0, 1)
				return
			}
			if(value.note=="Tic"){
				keysMainMetronome.player("Tic").start(time, 0, 0.1, 0, -1)
				return
			}

			if(value.note==-2){
				//pulse
				//with players
				if(keysPulse.player(value.inst).state=="stopped"){
					keysPulse.player(value.inst).start(time, 0, value.duration, 0, 1)
				}else{
					//time + little delay in order to not give sound pulse error
					keysPulse.player(value.inst).start(time+0.001, 0, value.duration, 0, 1)
				}
			}else{
				//NOTES INSTRUMENTS
				//if instrument sync not good
				if(value.inst!=1){
					//console.log(noteAbs)
					//console.log(value.note)
					instrument[value.inst].triggerAttackRelease(value.note, value.duration, time, value.volume/8)
				}else{
					let nzc_note = value.note
					if(nzc_note>=23 && nzc_note<= 69){
						var volume = APP_volume_value_to_dB(value.volume/3)
						//console.log(volume)
						instrument[value.inst].player(nzc_note).volume.value = volume -12//dB adjust
						instrument[value.inst].player(nzc_note).start(time, 0, value.duration, 0, 1)
					}
				}
			}
		}
	})

	//ADDING SEQUENCES OF SOUNDS LOOP BETTER IN TRANSPORT
	var [now,start,end,max]=APP_return_progress_bar_values()
	Tone.Transport.timeSignature = end

	var now_seconds = DATA_calculate_exact_time(now,0,0,1,1,1,1)
	var start_seconds = DATA_calculate_exact_time(start,0,0,1,1,1,1)
	var end_seconds = DATA_calculate_exact_time(end,0,0,1,1,1,1)
	var end_progress_bar = max

	//var time_to_stop = 1 * end * (60/PPM) -0.01
	var outside_initial_playing_range=false
	if(end_seconds<=now_seconds){
		outside_initial_playing_range=true
		end_seconds=DATA_calculate_exact_time(end_progress_bar,0,0,1,1,1,1)
			if(end_seconds<now_seconds){
				console.error("REPOSITIONING STARTING PLAY AT: "+now_seconds)
				//at least at the end
				end_seconds=now_seconds
			}
	}

	//a little bit before the true end in order to reset animations
	var time_to_stop=end_seconds-0.01

	if(outputLoop.value==="true"){
		if(outside_initial_playing_range){
			stopper.add({time: time_to_stop, end: true})
		}else{
			stopper.add({time: time_to_stop, end: false})
		}
	}else{
		stopper.add({time: time_to_stop, end: true})
	}

	//CLASSIC METRONOME NEED LOVE
	//var main_metro_vol = 0
	//dB
	//keysMainMetronome.volume.value=-80
	if(outputMainMetronome.value === "true"){
		//main_metro_vol = 1
		//keysMainMetronome.volume.value=0
		keysMainMetronome.mute=false
		//see if Li bigger than main metronome
		compas_sequence.forEach(item=>{
			//part.add("0:"+i,noteNames[1])
			var pulseN = Math.round(item[0]*(PPM/60))
			//console.log(item[0])
			part.add({ time: item[0], note: item[1], durationHz: 10, pulse: pulseN})
			// part.add({ time: item[0], note: item[1], durationHz: 10 , volume: main_metro_vol})
		})
	}else{
		keysMainMetronome.mute=true
	}

	//advance progress bar //all the possible pulses, even if not used
	//for ( var pulseN = 0 ; pulseN < end; pulseN++){
	for ( var pulseN = 0 ; pulseN < end_progress_bar; pulseN++){
		//var pulseN = Math.round(i*(PPM/60))
		var time_sec = pulseN*(60/PPM)
		part.add({ time: time_sec, action : "move_progress_bar", pulse: pulseN})
	}

	//METRONOME VOICE (FRACTIONING)
	voice_list_unmuted_time_data.forEach(voice_data=>{
		//check of i want to hear a metronome for this voice

		//from database
		var metro_sound = voice_data.metro

		//console.log(metro_sound)
		if(metro_sound!=0){
			//Metro sounds
			var metro_fractioning= DATA_list_voice_data_metro_fractioning(voice_data)
			metro_fractioning.forEach(change_fraction_data=>{
				for (var i =0; i<change_fraction_data.rep; i++){
					var time = change_fraction_data.time + i/change_fraction_data.freq
					var freq = change_fraction_data.freq+"hz"
					//part.add(time,metro_sound, freq)
					//console.log("time : "+ time)
					part.add({ time: time, note: metro_sound, durationHz: freq})
				}
			})
		}
	})
	var note_id= 0
	//NOTES VOICE
	voice_list_unmuted_time_data.forEach(voice_data=>{
		var midi_data = (JSON.parse(JSON.stringify(voice_data.data.midi_data)))
		var index_position = 0

		//changing instruments
		var voice_instrument = voice_data.instrument

		var nzc_note = midi_data.note_freq.map(freq=>{return APP_Hz_to_note(freq,TET)})//.reverse()
		var freq_note = midi_data.note_freq//.reverse()
		var note_list = []//array nuzic_note and frequence
		for (var i = 0 ; i<nzc_note.length;i++){
			note_list.push({"nzc" : nzc_note[i], "freq" : freq_note[i]})
		}

		var time_list = midi_data.time//.reverse()
		//duration iT_freq
		var duration_list = midi_data.duration//.reverse()

		var Dduration = 0
		//creating segment_obj_list, column_index_list
		var segment_obj_list = []
		var index_position_list=[]//n column in segment
		var segment_index_list=[]
		var index_abs_position_list=[]//n column from the start
		if(tabREbutton.checked && voice_data.RE_visible){
			//find voice
			var voice_RE_obj = RE_find_voice_obj_by_index(voice_data.voice_index)
			if(voice_RE_obj == null){
				console.error("voice not found")
				return
			}
			segment_obj_list = [...voice_RE_obj.querySelectorAll(".App_RE_segment")]
			//XXX XXX XXX if i want here i can force loading RE segments XXX XXX XXX
			var current_segment_change_index=1//when change first time (0->1)

			var segment_change_position=0
			var time_current_voice_segment_start=voice_data.data.segment_data.map(segment=>{
				var n_obj=segment.time.length-1
				segment_change_position+=n_obj
				return time_list[segment_change_position-n_obj]
			})

			var provv=0
			var ind_time_list=0
			var ind_abs_segment_start=0
			var ind_seg=0
			time_current_voice_segment_start.push("end")
			var time_list_size=time_list.length
			RE_global_time_all_changes.some(time=>{
				if(time==RE_global_time_segment_change[current_segment_change_index]){
					//plus a pair of columns
					provv+=2
					current_segment_change_index++
				}
				if(time==time_list[ind_time_list]){
					index_abs_position_list.push(provv)
					if(time==time_current_voice_segment_start[ind_seg+1]){
						ind_seg++
						ind_abs_segment_start=provv
					}
					segment_index_list.push(ind_seg)
					index_position_list.push(provv-ind_abs_segment_start)
					ind_time_list++
					if(ind_time_list>=time_list_size){
						return true
					}
				}
				provv+=2
				return false
			})
		}
		//reverse if not L not working
		nzc_note = nzc_note.reverse()
		freq_note = freq_note.reverse()
		note_list = note_list.reverse()
		time_list = time_list.reverse()
		duration_list = duration_list.reverse()
		index_position_list=index_position_list.reverse()
		segment_index_list=segment_index_list.reverse()
		index_abs_position_list= index_abs_position_list.reverse()
		//draw objects in PMC need to find last note (bc array is reversed !!!)
		var last_note = nzc_note.find(object=>{
			return object>-1
		})
		var last_pos=voice_data.e_sound
		if(last_note!=null){
			last_pos = last_note
		}
		note_list.forEach((note,index)=>{
			//OPTIMIZED
			if(tabREbutton.checked && voice_data.RE_visible){
				index_column_blink=2
				//add movement to the bar
				//var index_position = index_position_current_segment+index_column_blink
				part.add({ time: time_list[index], action: "RE_movement", index_column: index_abs_position_list[index]})
			}
			//dont sequence things outside
			if(time_list[index]<end_seconds){
				if(note.nzc<0){
					if(note.nzc==-3){
						//ligadura
						var duration = 1/duration_list[index]
						Dduration+=(duration)
						/// animation ligadura
						if(tabREbutton.checked && voice_data.RE_visible){
							var segment_obj = segment_obj_list[segment_index_list[index]]
							var index_column_blink = index_position_list[index]
							part.add({ time: time_list[index], action: "animation_on_RE",segment_obj: segment_obj,column_index:index_column_blink})
							part.add({ time: time_list[index]+duration, action: "animation_off_RE",segment_obj: segment_obj,column_index:index_column_blink})
						}
					}else{
						//silence or element
						//Dduration=0

						//animation silence and element
						var duration = 1/duration_list[index]
						var start_note_time=time_list[index]

						//animation
						if(tabREbutton.checked && voice_data.RE_visible){
							var segment_obj = segment_obj_list[segment_index_list[index]]
							var index_column_blink = index_position_list[index]
							part.add({ time: start_note_time, action: "animation_on_RE",segment_obj: segment_obj,column_index:index_column_blink})
							part.add({ time: start_note_time+duration, action: "animation_off_RE",segment_obj: segment_obj,column_index:index_column_blink})
						}

						//modify duration for PMC only
						if(Dduration!=0){
							duration += Dduration
							//reset
							Dduration=0
						}

						//PMC animation
						if(tabPMCbutton.checked && note.nzc==-2){
							//animation of "e"
							note_id++
							var note_data = {note:note.nzc,y_position:last_pos,pulsation:start_note_time*PPM/60,duration:duration*PPM/60,note_id:note_id, color:voice_data.color}
							part.add({ time: start_note_time, action: "animation_on_PMC", note_data:note_data})
							part.add({ time: start_note_time+duration, action: "animation_off_PMC", note_data:note_data})
						}

						//element
						//XXX attention if i call same sound twice XXX error same time XXX
						if(note.nzc==-2){
							part.add({ time: time_list[index], note: note.nzc, duration: 0.3, inst: "Pulse1"})
						}
					}
				}else{
					//it is a note
					var duration = 1/duration_list[index]
					if(Dduration!=0){
						duration += Dduration
						//reset
						Dduration=0
					}
					last_pos=note.nzc

					var start_note_time = time_list[index]
					var end_note_time = start_note_time+duration
					//control loop start/end
					//sequence everything
					//if(end_note_time>start_seconds && start_note_time<end_seconds){
						//note must be played
						//control loop start
						//sequence everything XXX this will prevent play a note in a loop if that start before the loop
						/*
						if(start_note_time<start_seconds){
							start_note_time= start_seconds
							duration = end_note_time-start_note_time
						}*/

						//control loop end
						//*/if commented (and sequence everything) this will prevent cutting a note in a loop if that end after the loop
						if(end_note_time>end_seconds && start_note_time<end_seconds){
							duration= end_seconds-start_note_time
						}
//*/

						//if instrument sync directly with transport
						//instrument[voice_instrument].triggerAttackRelease(note, duration, start_note_time, voice_data.volume/10)
						//if trigger with a part
						if(voice_instrument==1){
							//drum
							part.add({ time: start_note_time, note: note.nzc, duration: duration, inst: voice_instrument, volume: voice_data.volume})
						}else{
							part.add({ time: start_note_time, note: note.freq, duration: duration, inst: voice_instrument, volume: voice_data.volume})
						}

						//RE animation
						if(tabREbutton.checked && voice_data.RE_visible){
							//part.add({ time: start_note_time, action: "animation_on_RE", element_list: column_list})
							//part.add({ time: start_note_time+duration, action: "animation_off_RE", element_list: column_list})
							var segment_obj = segment_obj_list[segment_index_list[index]]
							var index_column_blink = index_position_list[index]
							part.add({ time: start_note_time, action: "animation_on_RE", segment_obj: segment_obj,column_index:index_column_blink})
							part.add({ time: start_note_time+duration, action: "animation_off_RE", segment_obj: segment_obj,column_index:index_column_blink})
						}

						if(isNaN(note.nzc)){
							console.error("note is NaN")
						}

						//PMC animation
						note_id++
						if(tabPMCbutton.checked){
							var note_data = {note:note.nzc,y_position:note.nzc,pulsation:start_note_time*PPM/60,duration:duration*PPM/60,note_id:note_id, color:voice_data.color}
							part.add({ time: start_note_time, action: "animation_on_PMC", note_data:note_data})
							part.add({ time: start_note_time+duration, action: "animation_off_PMC", note_data:note_data})
						}
					//}//sequence everything
				}
			}
		})
	})
}

function APP_play(){
	var play_label= document.querySelector('.App_Nav_play_button')
	var pause_label= document.querySelector('.App_Nav_pause_button')
	PMC_redraw_selected_voice_svg(false,DATA_get_selected_voice_data())
	var [now,start,end,max]=APP_return_progress_bar_values()
	var buffer = 0.3

	//not using bar to define transport position
	//var now_seconds = Tone.Transport.seconds
	//using bar to define transport position (USED ONLY IF TRANSPORT IS STOPPED)
	var now_seconds = DATA_calculate_exact_time(now,0,0,1,1,1,1)
	var start_seconds = DATA_calculate_exact_time(start,0,0,1,1,1,1)
	var end_seconds = DATA_calculate_exact_time(end,0,0,1,1,1,1)

	//no good, part will continue playin last note, need stopper to stop playing without stopping the loop
	// Tone.Transport.loop=true
	// Tone.Transport.loopStart = start*(60/PPM)
	// Tone.Transport.loopEnd = end*(60/PPM)

	play_label.style.display = 'none'
	pause_label.style.display = 'flex'
	//chrome and safari need action to start context
	if(Tone.context.state!=='running'){
		Tone.context.resume()
		Tone.start()
	}

	Tone.Transport.bpm.value = PPM

	//here because ppm change everything in sequencer
	if(outputLoop.value==="true"){
		if(now_seconds<end_seconds){
			Tone.Transport.loop=true
			//avoid rounding errors
			Tone.Transport.loopStart = start_seconds
			Tone.Transport.loopEnd = end_seconds
		}else{
			Tone.Transport.loop=false
		}
	}else{
		Tone.Transport.loop=false
	}

	if(Tone.Transport.state=="stopped"){
		//from the start
		APP_sequencer()
		//connect instrument if disconnected
		APP_connect_first_instrument()

		//countin
		keysMainMetronome.mute = false
		var j = TNAV_read_countIn()
		for (var i=0; i<j; i++){
			var countIn_time = i *1000 * 60 / PPM
			setTimeout(function () {
				var now = Tone.now()
				keysMainMetronome.player("Accent_Weak").start(now, 0, 0.1, 0, 1)
			}, countIn_time)
		}
		//keysMainMetronome.mute = true

		var countIn_end = j * 1000 * 60 / PPM - buffer * 1000//buffering
		setTimeout(function () {
			part.start(0)
			stopper.start(0)
			if(now_seconds==0){
				Tone.Transport.seconds = now_seconds-buffer
			}else{
				//smaller buffer
				Tone.Transport.seconds = now_seconds-0.05
			}

			Tone.Transport.start()
		}, countIn_end)

	} else if(Tone.Transport.state=="paused"){
		//paused: restart
		//connect instrument if disconnected
		APP_connect_first_instrument()
		Tone.Transport.start()
		part.start(0)
		stopper.start(0)

		//restart notes that were playing at the time
		APP_restart_paused_notes()
	} else {
		//NOTHING TO DO
	}
}

function APP_restart_paused_notes(time){
	var time = Tone.Transport.seconds
	//PMC_reset_playing_notes() //no need
	//PMC_draw_elements_on_play() //no need OBSOLETE??
	//look at every event in part and extrapolate what instrument was active in that moment
	//console.log(part.entries)
	var now = Tone.now()
	part._events.forEach(item =>{
		//console.log(item)
		//if(item.value.time < time && item.value.note>0){
		if(item.value.time < time && item.value.duration+item.value.time>time){
			//console.log(item)
			//replay this item

			if(item.value.inst!=1){
				//console.log(value.volume)
				var new_duration = item.value.duration - (time-item.value.time)
				instrument[item.value.inst].triggerAttackRelease(item.value.note, new_duration, now, item.value.volume/10)
			}
		}
	})
}

function APP_stop(){
	APP_reset_blink_play()
	//var PMC_progress_line_svg = document.getElementById('App_PMC_progress_svg')
	//var PMC_playing_notes_svg = document.getElementById('App_PMC_playing_notes_svg')
	//PMC_progress_line_svg.style.zIndex='0'
	//PMC_playing_notes_svg.style.zIndex='0'
	PMC_reset_progress_line()
	PMC_reset_action_pmc()
	PMC_reset_playing_notes()

	//document.getElementById('App_PMC_selected_voice').style.display='flex'
	PMC_redraw_selected_voice_svg(true,DATA_get_selected_voice_data())

	var play_label= document.querySelector('.App_Nav_play_button')
	var pause_label= document.querySelector('.App_Nav_pause_button')

	play_label.style.display = 'flex'
	pause_label.style.display = 'none'

	if(Tone.Transport.state!="stopped"){
		//need to stop tone event beeng played
		APP_stop_all_notes()

		Tone.Transport.stop()

		//part.stop(1000)
		//part.stop(0)
		part.stop(Tone.now())
		stopper.start(0)

		//redraw with delay NEED LOVE XXX

		var stopbutton = document.getElementById("stop_button")
		stopbutton.checked = true

	}
	APP_check_minimap_scroll()
}

function APP_pause(){
	var play_label= document.querySelector('.App_Nav_play_button')
	var pause_label= document.querySelector('.App_Nav_pause_button')

	play_label.style.display = 'flex'
	pause_label.style.display = 'none'

	if(Tone.Transport.state=="started"){
		//need to stop tone event beeng played
		APP_stop_all_notes()
		Tone.Transport.pause()
		//console.log("pause")
	}
}

let note_on_click_silenced=false
function APP_play_this_note_RE_element(element){
	if(note_on_click_silenced)return
	//APP_stop()
	APP_connect_first_instrument()
	var pos = RE_input_to_position(element)
	var element_info = DATA_get_object_index_info(pos.voice_index,pos.segment_index,pos.position)
	var nzc_note=element_info.note
	if(nzc_note>=0){
		//play note
		var voice = element.closest(".App_RE_voice")
		var instrument_index = RE_which_voice_instrument(voice)
		var freq_note = APP_note_to_Hz(nzc_note,TET)
		var now = Tone.now()
		//instrument[inst_index].triggerAttackRelease(freq_note, 0.5, now, RE_read_voice_volume(voice)/4) //changed from /3
		if(instrument_index!=1){
			instrument[instrument_index].triggerAttackRelease(freq_note, 0.5, now, RE_read_voice_volume(voice)/8)
		}else{
			//if drum
			//if(nzc_note>=23 && nzc_note<= 69)instrument[instrument_index].player(nzc_note).start(now, 0, 1, 0, RE_read_voice_volume(voice)/10)
			//the last 1 is the velocity, not volume
			if(nzc_note>=23 && nzc_note<= 69){
				var volume = APP_volume_value_to_dB(RE_read_voice_volume(voice)/3)
				//console.log(volume)
				instrument[instrument_index].player(nzc_note).volume.value = volume -12//dB adjust
				instrument[instrument_index].player(nzc_note).start(now, 0, 1, 0, 1)
				//instrument[1].player(44).volume.value
				//instrument[1].player(44).start(Tone.now(), 0, 1, 0, 1)
			}
		}
	}
}
function APP_play_notes_on_dbclick_RE_element(element){
	APP_stop()
	APP_connect_first_instrument()

	/* var pos = RE_input_to_position(element)
	var element_info = DATA_get_object_index_info(pos.voice_index,pos.segment_index,pos.position)
	var nzc_note=element_info.note */	
		//play note
		/* var voice = element.closest(".App_RE_voice")
		var instrument_index = RE_which_voice_instrument(voice) */
		var instrument_index=2
		//var freq_note = APP_note_to_Hz(nzc_note,TET)
		var now = Tone.now()
		//instrument[inst_index].triggerAttackRelease(freq_note, 0.5, now, RE_read_voice_volume(voice)/4) //changed from /3
		/* if(instrument_index!=1){ */
		console.log(element)
			instrument[instrument_index].triggerAttackRelease(APP_note_to_Hz(element[0],TET), 0.5, now, 0.2)
			instrument[instrument_index].triggerAttackRelease(APP_note_to_Hz(element[1],TET), 0.5, now+0.001, 0.2)
		/* }else{
			//if drum
			//if(nzc_note>=23 && nzc_note<= 69)instrument[instrument_index].player(nzc_note).start(now, 0, 1, 0, RE_read_voice_volume(voice)/10)
			//the last 1 is the velocity, not volume
			if(nzc_note>=23 && nzc_note<= 69){
				var volume = APP_volume_value_to_dB(RE_read_voice_volume(voice)/3)
				//console.log(volume)
				instrument[instrument_index].player(nzc_note).volume.value = volume -15//dB adjust
				instrument[instrument_index].player(nzc_note).start(now, 0, 1, 0, 1)
				//instrument[1].player(44).volume.value
				//instrument[1].player(44).start(Tone.now(), 0, 1, 0, 1)
			}
		} */
	console.log('1')
}

function APP_play_input_on_click(element, type){
	//if it is playng something
	//console.log('??')
	if(type=='Kb'){
		//console.log('si')
		APP_play_this_keyboard_note(element)
	}else if(type!="iA"){
		if(SNAV_read_note_on_click() == true && element.value!=""){
			APP_stop()
			setTimeout(function () {
				APP_play_this_note_RE_element(element)
			}, 400)
		}
	}else{
		//is a iA
		if(SNAV_read_note_on_click() == true){
			RE_play_iA(element)
		}
	}
}

function APP_play_all_notes_at_time(time){
	var data = DATA_get_current_state_point(true)
	var note_to_play= []
	data.voice_data.forEach(voice=>{
		//if not visible end
		if(!voice.RE_visible || voice.volume==0){
			return
		}
		voice.data.midi_data.time.find((midi_time,index) =>{
			if(midi_time==time){
				note_to_play.push({"note_freq":voice.data.midi_data.note_freq[index],"instrument":voice.instrument,"volume":voice.volume})
				return true
			}
			return false
		})
	})

	var now = Tone.now()
	note_to_play.forEach(note=>{
		if(note.note_freq<=0)return
		if(note.instrument==1){
			//drum
			let nzc_note = APP_Hz_to_note(note.note_freq,data.global_variables.TET)
			if(nzc_note>=23 && nzc_note<= 69){
				var volume = APP_volume_value_to_dB(note.volume/3)
				instrument[note.instrument].player(nzc_note).volume.value = volume -12//dB adjust
				instrument[note.instrument].player(nzc_note).start(now, 0, 1 , 0, 1)
			}
		}else{
			instrument[note.instrument].triggerAttackRelease(note.note_freq, 0.5, now, note.volume/8)
		}
	})
	//avoid play of single click
	note_on_click_silenced=true
	setTimeout(function () {
		note_on_click_silenced=false
	}, 500)
}

function APP_play_this_keyboard_note(element){	//XXX OBSOLETE??????
	APP_stop()
	APP_connect_first_instrument()
	var nzc_note = element.value
	var instrument_index = 0
	var freq_note = APP_note_to_Hz(nzc_note,TET)
	var now = Tone.now()
	instrument[instrument_index].triggerAttackRelease(freq_note, 0.5, now, 3/10)
}

//async function
function APP_play_this_note_number(nzc_note,instrument_index){
	if(nzc_note<0)return
	setTimeout(function(){
		//APP_stop()
		APP_connect_first_instrument()
		if(instrument[instrument_index]==null)return

		//play note
		var freq_note = APP_note_to_Hz(nzc_note,TET)
		var now = Tone.now()

		//instrument[instrument_index].triggerAttackRelease(freq_note, 0.5, now, 1/4) //changed from /3
		if(instrument_index!=1){
			instrument[instrument_index].triggerAttackRelease(freq_note, 0.5, now, 3/10)
		}else{
			//if drum
			if(nzc_note>=23 && nzc_note<= 69){
				instrument[instrument_index].player(nzc_note).volume.value = -15
				instrument[instrument_index].player(nzc_note).start(now, 0, 1, 0, 3/10)
			}
		}
	}, 0)
}

function APP_play_this_accents(){
	APP_stop()
	APP_connect_first_instrument()

	var tabla_compases = document.querySelector(".App_BNav_mod_T_top_sequence")
	var selected = tabla_compases.querySelector(".checked")
	var compas_value = JSON.parse(selected.value)

	var accent_list = compas_value.compas_values[3]
	var Lc = compas_value.compas_values[1]
	if(Lc ==0)return

	var now = Tone.now()
	var dT = 0.1
	var Ti = (60/PPM)

	//sounds
	for (var i=0 ; i<Lc ; i++){
		if(i==0){
			//first accent
			keysMainMetronome.player("Accent").start(now, 0, dT, 0, 1)
		}else{
			if(accent_list.includes(i)){
				keysMainMetronome.player("Accent_Weak").start(now+Ti*i, 0, dT, 0, 1)
			}else{
				keysMainMetronome.player("Tic").start(now+Ti*i, 0, dT, 0, -1)
			}
		}
	}
}

let _scale_playing=false
function APP_play_this_scale(data,starting_note=0,rotation=0){
	if(_scale_playing)return
	_scale_playing=true
	APP_stop()
	APP_connect_first_instrument()
	//play note from reg 3
	//starting note only for editor!!!
	var current_note = 3*data.TET + starting_note

	var inst_index = 0
	var freq_note = APP_note_to_Hz(current_note,data.TET)
	var now = Tone.now()
	//first note
	instrument[inst_index].triggerAttackRelease(freq_note, 0.5, now, 1)
	//other notes
	//iS+rotation
	var temp_iS = Array.from(data.iS)
	
	for (var y=0 ; y<rotation; y++){
		var iS = temp_iS.shift()
		temp_iS.push(iS)
		/* var iS = data.iS.shift()
		data.iS.push(iS) */
	}
	
	/* console.log(`data is${data.iS}`)
	
	console.log(`temp is${temp_iS}`) */
	for (var i=0 ; i<data.module; i++){
		//current_note += data.iS[i]
		current_note += temp_iS[i]
		freq_note = APP_note_to_Hz(current_note,data.TET)
		instrument[inst_index].triggerAttackRelease(freq_note, 0.5, now+(1+i)*0.5, 1)
	}
	//console.log(data.iS)
	var waiting_time = data.module*0.5*1000
	setTimeout(function () {
		_scale_playing=false
	}, waiting_time)
}

function APP_play_this_scale_note(number){
	var current_note = 3*editor_scale.scale.TET + number
	var freq_note = APP_note_to_Hz(current_note,editor_scale.scale.TET)
	var now = Tone.now()
	var inst_index = 0
	instrument[inst_index].triggerAttackRelease(freq_note, 0.5, now, 1)
}

function APP_sequencer_for_audio_export(current_data,start,end,play_metronome){
	//in freq
	hz=current_data.global_variables.PPM/60
	pulsationFrequency=hz+"hz"

	//creating instruments and sounds

	//offline masterVolume,mainMetronomeVolume
	const _offline_masterVolume = new Tone.Volume(0).toDestination()
	const _offline_mainMetronomeVolume = new Tone.Volume(0).connect(_offline_masterVolume)

	//value in dB
	var master_vol = APP_volume_value_to_dB(APP_read_master_volume())
	_offline_masterVolume.volume.value = master_vol
	if(master_vol==-20){
		_offline_masterVolume.mute=true
	}else{
		_offline_masterVolume.mute=false
	}
	var mainMetronome_vol = APP_volume_value_to_dB(APP_read_main_metronome_volume())
	_offline_mainMetronomeVolume.volume.value = mainMetronome_vol
	if(mainMetronome_vol==-20){
		_offline_mainMetronomeVolume.mute = true
	}else{
		_offline_mainMetronomeVolume.mute = false
	}

	//recreate all the instruments and sounds!!!
	const _offline_keysPulse = APP_sound_keysPulse().connect(_offline_masterVolume)
	const _offline_keysMainMetronome = APP_sound_keysMainMetronome().connect(_offline_mainMetronomeVolume)
	const _offline_keysFractionedMetronome=APP_sound_keysFractionedMetronome().connect(_offline_masterVolume)

	//create an instrument for every voice, avoid conflicts

	const _offline_instrument_voice = []
	/*
	_offline_instrument[0] = APP_sound_instrument_0().connect(_offline_masterVolume)
	_offline_instrument[1] = APP_sound_instrument_1().connect(_offline_masterVolume)
	_offline_instrument[2] = APP_sound_instrument_2().connect(_offline_masterVolume)
	_offline_instrument[3] = APP_sound_instrument_3().connect(_offline_masterVolume)
	_offline_instrument[4] = APP_sound_instrument_4().connect(_offline_masterVolume)
	_offline_instrument[5] = APP_sound_instrument_5().connect(_offline_masterVolume)
	*/

	/*/TEST part
	//const synth = new Tone.Synth().toDestination();
	var _offline_part = new Tone.Part(((time, note) => {
		//synth.triggerAttackRelease(note, "8n", time)
		_offline_keysMainMetronome.player("Tic").start(time, 0, 0.1, 0, -1)
		//sampler
		_offline_instrument[0].triggerAttackRelease(note, "8n", time)
		//inst 1 player
		//_offline_instrument[1].player(44).volume.value = 3 -12//dB adjust
		//_offline_instrument[1].player(44).start(time, 0, "8n", 0, 1)
		// _offline_instrument[2].triggerAttackRelease(note, "8n", time)
		// _offline_instrument[3].triggerAttackRelease(note, "8n", time)
		// _offline_instrument[4].triggerAttackRelease(note, "8n", time)
		_offline_instrument[5].triggerAttackRelease(note, "8n", time)
		//sampler

	}), [[0, "C2"], ["0:2", "C3"], ["0:3:2", "G2"]])
	return _offline_part
	//*/

	//building time sequences

	var voice_solo = current_data.voice_data.filter(voice=>{
		return voice.solo==true
	})

	var voice_list_unmuted_time_data = []
	if(voice_solo!=null&&voice_solo.length!=0){
		//there is a solo
		voice_solo.forEach(element=>{
			if(element.volume!="0"){
				voice_list_unmuted_time_data.push(element)
			}
		})
	}else{
		//find other not muted voices
		voice_list_unmuted_time_data = current_data.voice_data.filter(voice=>{
			return voice.volume!="0"
		})
	}

	var _offline_part = new Tone.Part(function(time, value){
		//sounds
		var vel = Math.random() * 0.5 + 0.5;
		//suona la nota nel gruppo di note keys al pulso "time" calcolato mediante una frequenza di pulsazione
		if(value.note>0 && value.note<=5){
			//fractioned metronome
			//volume
			var vol = -1
			//adjust volume for Claves
			if (value.note == 1){
				vol += 2
			}
			//adjust volume for Wood-Block-Med
			if (value.note == 4){
				vol += 5
			}
			_offline_keysFractionedMetronome.player(value.note).volume.value = vol;
			//avoiding glitching at very hight PPM fractioning and neopulse
			if(_offline_keysFractionedMetronome.player(value.note).state=="stopped"){
				_offline_keysFractionedMetronome.player(value.note).start(time, 0, value.durationHz, 0, vel)
			}else{
				//sound is overlapping
			}
		}else{
			//main metronome (compass, accents) 0.1 is duration
			//vel is due to main volume and value.volume
			if(value.note=="Accent"){
				_offline_keysMainMetronome.player("Accent").start(time, 0, 0.1, 0, 1)
				return
			}
			if(value.note=="Accent_Weak"){
				_offline_keysMainMetronome.player("Accent_Weak").start(time, 0, 0.1, 0, 1)
				return
			}
			if(value.note=="Tic"){
				_offline_keysMainMetronome.player("Tic").start(time, 0, 0.1, 0, -1)
				return
			}

			if(value.note==-2){
				//pulse
				//with players
				if(_offline_keysPulse.player(value.inst).state=="stopped"){
					_offline_keysPulse.player(value.inst).start(time, 0, value.duration, 0, 1)
				}else{
					//NO NEED TO DUPLICATE pulse
					//sound is overlapping
					//time + little delay in order to not give sound pulse error
					//_offline_keysPulse.player(value.inst).start(time+0.001, 0, value.duration, 0, 1)
				}
			}else{
				//NOTES INSTRUMENTS
				//if instrument sync not good
				if(value.inst!=1){
					//console.log(noteAbs)
					//console.log(value.note)
					//_offline_instrument[value.inst].triggerAttackRelease(value.note, value.duration, time, value.volume/8)
					_offline_instrument_voice[value.voice_index].triggerAttackRelease(value.note, value.duration, time, value.volume/8)
				}else{
					let nzc_note = value.note
					if(nzc_note>=23 && nzc_note<= 69){
						var volume = APP_volume_value_to_dB(value.volume/3)
						//console.log(volume)
						//_offline_instrument[value.inst].player(nzc_note).volume.value = volume -12//dB adjust
						_offline_instrument_voice[value.voice_index].player(nzc_note).volume.value = volume -12//dB adjust
						//_offline_instrument[value.inst].player(nzc_note).start(time, 0, value.duration, 0, 1)
						_offline_instrument_voice[value.voice_index].player(nzc_note).start(time, 0, value.duration, 0, 1)
					}
				}
			}
		}
	})

	//ADDING SEQUENCES OF SOUNDS

	//countin
	var count_in = current_data.global_variables.count_in
	//var count_in_duration = count_in * 1000 * 60 / current_data.global_variables.PPM //in m_sec
	var count_in_duration = DATA_calculate_exact_time(count_in,0,0,1,1,1,1) //in sec
	if(count_in!=0){
		keysMainMetronome.mute = false
		for (var i=0; i<count_in; i++){
			var time_count_in = DATA_calculate_exact_time((start+i),0,0,1,1,1,1)//not really needed
			_offline_part.add({ time: time_count_in, note: "Accent_Weak"})
		}
		// for (var i=0; i<j; i++){
		// 	var countIn_time = i *1000 * 60 / current_data.global_variables.PPM
		// 	setTimeout(function () {
		// 		var now = Tone.now()
		// 		keysMainMetronome.player("Accent_Weak").start(now, 0, 0.1, 0, 1)
		// 	}, countIn_time)
		// }
	}

	var compas_values_array = current_data.compas
	var Ti = 60/current_data.global_variables.PPM //time no need to be exact
	var metro_compas_sequence = [[count_in_duration,"Accent"]]
	if(compas_values_array.length==1 && compas_values_array[0].compas_values[1]==0){
		//no compases
		//play an accent at the start and pulsacion next
		for (var i=1; i<Li ; i++){
			metro_compas_sequence[i]=[(i+count_in)*Ti,"Tic"]
		}
	}else{
		//compasses
		var index_P = 0
		compas_values_array.forEach(compas =>{
			var accent_sequence=compas.compas_values[3]
			if(compas.compas_values[4]!=0){
				//recalculate rotated sequence
				accent_sequence.push(compas.compas_values[1])
				var iT_sequence = []
				for (let i = 0; i < accent_sequence.length-1; i++) {
					iT_sequence.push(accent_sequence[i+1]-accent_sequence[i])
				}
				//rotate iT sequence
				var rotation_N = compas.compas_values[4]
				while (rotation_N!=0) {
					iT_sequence.unshift(iT_sequence.pop())
					rotation_N--
				}
				accent_sequence=[0]
				var i=0
				iT_sequence.forEach(iT=>{
					accent_sequence.push(accent_sequence[i]+iT)
					i++
				})
				accent_sequence.pop()
			}
			for (var rep = 0; rep<compas.compas_values[2]; rep++){
				for(var pulse =0;pulse<compas.compas_values[1];pulse++){
					if(pulse==0){
						//first accent
						metro_compas_sequence[index_P]=[(index_P+count_in)*Ti,"Accent"]
					}else{
						//if(compas.compas_values[3].includes(pulse)){
						if(accent_sequence.includes(pulse)){
							metro_compas_sequence[index_P]=[(index_P+count_in)*Ti,"Accent_Weak"]
						}else{
							metro_compas_sequence[index_P]=[(index_P+count_in)*Ti,"Tic"]
						}
					}
					index_P++
				}
			}
		})
	}

	//var now_seconds = now*(60/current_data.global_variables.PPM)
	//var start_seconds = start*(60/current_data.global_variables.PPM)+count_in_duration
	var start_seconds = DATA_calculate_exact_time((count_in+start),0,0,1,1,1,1,current_data.global_variables.PPM)
	//var end_seconds = end*(60/current_data.global_variables.PPM)+count_in_duration
	var end_seconds = DATA_calculate_exact_time((count_in+end),0,0,1,1,1,1,current_data.global_variables.PPM)
	//var time_to_stop = 1 * end * (60/current_data.global_variables.PPM) -0.01

	//CLASSIC METRONOME
	if(play_metronome){
		//main_metro_vol = 1
		//keysMainMetronome.volume.value=0
		keysMainMetronome.mute=false
		//see if Li bigger than main metronome
		metro_compas_sequence.forEach(item=>{
			var pulseN = Math.round(item[0]*(current_data.global_variables.PPM/60))//XXX needed if want to move bar
			_offline_part.add({ time: item[0], note: item[1], durationHz: 10, pulse: pulseN})
		})
	}else{
		if(count_in==0)keysMainMetronome.mute=true
	}

	//METRONOME VOICE (FRACTIONING)
	voice_list_unmuted_time_data.forEach(voice_data=>{
		//check of i want to hear a metronome for this voice
		//from database
		var metro_sound = voice_data.metro

		//console.log(metro_sound)
		if(metro_sound!=0){
			//Metro sounds
			var metro_fractioning= DATA_list_voice_data_metro_fractioning(voice_data)
			metro_fractioning.forEach(change_fraction_data=>{
				for (var i =0; i<change_fraction_data.rep; i++){
					var time = change_fraction_data.time + i/change_fraction_data.freq
					var freq = change_fraction_data.freq+"hz"
					//_offline_part.add(time,metro_sound, freq)
					_offline_part.add({ time: time, note: metro_sound, durationHz: freq})
				}
			})
		}
	})

	//create a instrument for every voice
	voice_list_unmuted_time_data.forEach(voice_data=>{
		_offline_instrument_voice[voice_data.voice_index]=APP_sound_instrument(voice_data.instrument).connect(_offline_masterVolume)
	})

	//NOTES VOICE
	voice_list_unmuted_time_data.forEach(voice_data=>{
		var midi_data = (JSON.parse(JSON.stringify(voice_data.data.midi_data)))
		var index_position = 0

		//changing instruments
		var voice_instrument = voice_data.instrument
		var voice_index = voice_data.voice_index

		var nzc_note = midi_data.note_freq.map(freq=>{return APP_Hz_to_note(freq,current_data.global_variables.TET)})//.reverse()
		var freq_note = midi_data.note_freq
		var note_list = []//array nuzic_note and frequence
		for (var i = 0 ; i<nzc_note.length;i++){
			note_list.push({"nzc" : nzc_note[i], "freq" : freq_note[i]})
		}

		var time_list = midi_data.time
		if(count_in!=0){
			// time_list.forEach((time,index,array)=>{
				//need to recalc all times ex novo
				//array[index]=time+count_in_duration //BAAAADDDDDDDDDD
			// })
			time_list = DATA_segment_data_to_midi_data(voice_data.data.segment_data,voice_data.neopulse,count_in,current_data.global_variables.PPM).time
		}

		//duration iT_freq
		var duration_list = midi_data.duration

		var Dduration = 0

		note_list.forEach((note,index)=>{
			if(note.nzc<0){
				if(note.nzc==-3){
					//ligadura
					Dduration+=(1/duration_list[index])
					// XXX
					/// animation ligadura
				}else{
					//silence or element
					Dduration=0

					//animation silence and element
					var duration = 1/duration_list[index]

					//element
					//XXX attention if i call same sound twice XXX error same time XXX
					if(note.nzc==-2){
						_offline_part.add({ time: time_list[index], note: note.nzc, duration: 0.3, inst: "Pulse1"})
					}
				}
			}else{
				//it is a note
				var duration = 1/duration_list[index]
				if(Dduration!=0){
					duration += Dduration
					//reset
					Dduration=0
				}
				if(time_list[index]<end_seconds){
					var start_note_time = time_list[index]
					var end_note_time = start_note_time+duration
					//control loop start/end
					if(end_note_time>start_seconds && start_note_time<end_seconds){
						//note must be played
						//control loop start
						if(start_note_time<start_seconds){
							start_note_time= start_seconds
							duration = end_note_time-start_note_time
						}

						//control loop end
						if(end_note_time>end_seconds){
							duration= end_seconds-start_note_time
						}

						//if instrument sync directly with transport
						//instrument[voice_instrument].triggerAttackRelease(note, duration, start_note_time, voice_data.volume/10)
						//if trigger with a _offline_part
						if(voice_instrument==1){
							//drum
							_offline_part.add({ time: start_note_time, note: note.nzc, duration: duration, inst: voice_instrument, voice_index: voice_index, volume: voice_data.volume})
						}else{
							_offline_part.add({ time: start_note_time, note: note.freq, duration: duration, inst: voice_instrument, voice_index: voice_index, volume: voice_data.volume})
						}

						if(isNaN(note.nzc)){
							console.error("note is NaN")
						}
					}
				}
			}
		})
	})
	return _offline_part
}

//play stop with spacebar
document.addEventListener('keyup', function(e){
	//console.log(e.keyCode)
	//spacebar
	if(e.keyCode == "32"){
		//play pause
		var currentElement_id = document.activeElement.id
		if(currentElement_id=="Notebook" || currentElement_id=="composition_name")return
		var current_class_list = document.activeElement.classList
		if(current_class_list.contains("App_BNav_mod_T_content_header_inp_name") || current_class_list.contains("App_BNav_mod_S_content_header_inp_name") ||
			current_class_list.contains("App_RE_voice_name") || current_class_list.contains("App_RE_column_selector_segment_name") || current_class_list.contains("App_RE_segment_name") ||
			current_class_list.contains("App_PMC_segment_name") || current_class_list.contains("App_PMC_voice_selector_voice_item_name") ||
			current_class_list.contains("App_SNav_bug_divForm"))return

		let state = Tone.Transport.state
		if(state=="paused" || state=="stopped"){
			APP_play()
		}else{
			APP_pause()
		}
	}
})

