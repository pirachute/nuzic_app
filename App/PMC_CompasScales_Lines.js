function PMC_write_C_line_values() {
	var C_line = document.querySelector(".App_PMC_C")
	C_line.innerHTML = ''
	//var compasArr = Read_compas_values()
	var compasArr= DATA_get_current_state_point(false).compas

	var compasArrWithReps = []
	var counterCompasReps = 0
	for(i=0;i<compasArr.length;i++){
		compasArr[i].compas_values[0] = counterCompasReps
		compasArrWithReps.push(compasArr[i])
		counterCompasReps+=compasArr[i].compas_values[1]
		if(compasArr[i].compas_values[2]!=1){
			for(j=1;j<compasArr[i].compas_values[2];j++){
				var tempCompas = {compas_values:[counterCompasReps,compasArr[i].compas_values[1],compasArr[i].compas_values[2],compasArr[i].compas_values[3],compasArr[i].compas_values[4],true]}
				compasArrWithReps.push(tempCompas)
				counterCompasReps+=tempCompas.compas_values[1]
			}
		}
	}

	var lastElementCounter = 0
	compasArrWithReps.forEach((element, index) => {
		var C_button = document.createElement('button')
		C_button.classList.add('App_PMC_C_button')
		//var C_button_left = PMC_zoom_x * nuzic_block * (element.compas_values[0] - index / PMC_zoom_x) + nuzic_block / 2
		var C_button_left = PMC_zoom_x*nuzic_block*element.compas_values[0]+nuzic_block/2
		C_button.style.left = `${C_button_left}px`
		C_button.value = `{"C_values":[${element.compas_values[0]},${index},${element.compas_values[1]},[0],true]}`
		if(element.compas_values.length == 6){
			C_button.value = `{"C_values":[${element.compas_values[0]},${index},${element.compas_values[1]},[0],false]}`
		}

		C_button.onclick= function(){BNAV_show_compas_input(C_button)}

		if(element.compas_values.length==5){
		C_button.innerHTML = `
							 <div class="App_PMC_C_inp_box">
								<div class="App_PMC_C_value">
									<p class="App_PMC_sub_C_value">${index}</p>
								</div>
								<div class="App_PMC_C_range">
									<p class="App_PMC_sub_C_range">${element.compas_values[1]}</p>
								</div>
							</div>
		`
		C_line.appendChild(C_button)

		if(PMC_zoom_x<1)C_button.querySelector('.App_PMC_C_range').style.display='none'
		}else {
			C_button.innerHTML = `
											<div class="App_PMC_C_inp_box">
												<div class="App_PMC_C_value">
													<p class="App_PMC_sub_C_value">${index}</p>
												</div>
											</div>
								`
			C_line.appendChild(C_button)
		}
		if(index == compasArrWithReps.length-1){
			lastElementCounter = C_button_left
		}
	})

	if(PMC_zoom_x>=1)lastElementCounter+=28

	var voice_data = DATA_get_selected_voice_data()
	var maxLength = PMC_x_length(voice_data)
	//var voiceLength = document.getElementById('App_PMC_selected_voice')

	var lastElement = document.createElement('div')
	lastElement.classList.add('PMC_scaleCompas_lastEl')

	lastElement.style.left= 0

	var width_string = "calc(var(--PMC_x_block) * "+(maxLength-0.5)+" + var(--RE_block))"
	//lastElement.style.left= `${lastElementCounter}px`
	lastElement.style.width= width_string
	//lastElement.style.width = `${maxLength*nuzic_block*PMC_zoom_x+nuzic_block/2 - lastElementCounter -nuzic_block*compasArrWithReps.length}px`
	C_line.appendChild(lastElement)
}

function PMC_write_S_line_values() {
	var S_line = document.querySelector(".App_PMC_S")
	S_line.innerHTML = ''

	var data= DATA_get_current_state_point(false)
	
	var scale_values = data.scale.scale_sequence
	var counter = 0
	//var lastElementCounter = 0
	scale_values.forEach((element, index) => {
		// console.log(element)
		var S_button = document.createElement('button')
		S_button.classList.add('App_PMC_S_button')
		// var S_button_left = PMC_zoom_x * nuzic_block * (counter - index / PMC_zoom_x) + nuzic_block / 2
		// var S_button_left = PMC_zoom_x*nuzic_block*(counter - index / PMC_zoom_x)+ nuzic_block/2
		var S_button_left= PMC_zoom_x*nuzic_block* counter +nuzic_block/2
		S_button.style.left = `${S_button_left}px`
		// console.log(element)
		S_button.value=JSON.stringify(element)
		S_button.onclick= function(){PMC_show_scale_input(S_button)}
		S_button.innerHTML = `
				<div class="App_PMC_S_inp_box" style="display: inline;">
					<div class="App_PMC_S_value">
						<p class="App_PMC_sub_S_value">${element.acronym}${element.starting_note}.${element.rotation}</p>
					</div>
					<div class="App_PMC_S_duration">
						<p class="App_PMC_sub_S_duration">${element.duration}</p>
					</div>
				</div>
		`
		S_line.appendChild(S_button)
		counter += element.duration
		/* if(index == scale_values.length-1){
			lastElementCounter = S_button_left

		} */
		if(PMC_zoom_x<1)S_button.querySelector('.App_PMC_S_duration').style.display='none'
	})

	//if(PMC_zoom_x>=1)lastElementCounter+=28
	var voice_data = DATA_get_selected_voice_data()
	var maxLength = PMC_x_length(voice_data)

	//var voiceLength = document.getElementById('App_PMC_selected_voice')
	var lastElement = document.createElement('div')
	lastElement.classList.add('PMC_scaleCompas_lastEl')
	lastElement.style.left= 0

	var width_string = "calc(var(--PMC_x_block) * "+(maxLength-0.5)+" + var(--RE_block))"
	//lastElement.style.left= `${lastElementCounter}px`
	lastElement.style.width= width_string
	//lastElement.style.width = `${maxLength*nuzic_block*PMC_zoom_x+nuzic_block/2 - lastElementCounter -nuzic_block*scale_values.length}px`
	S_line.appendChild(lastElement)
}

function PMC_show_scale_input(S_button=null){
	//use scale != null == button pressed to focus on correct scale
	//APP_stop()
	var selected = -1
	if(S_button==null){
		//select first scale
		selected=0
	}else{
		var [elementIndex,list]=RE_element_index(S_button,".App_PMC_S_button")
		selected=elementIndex
	}
	//verify if found something
	var scale_sequence= DATA_get_scale_sequence()
	if(selected<0 && selected>=scale_sequence.length)selected=0

	var ES_button = document.getElementById("App_BNav_tab_ES")
	ES_button.click()
	BNAV_select_scale_number_tab_list(selected)
}

function PMC_show_scale_number_S_line(index){
	var scale_line = document.querySelector('.App_PMC_S')
	var scale_button_list = [...scale_line.querySelectorAll(".App_PMC_S_button")]

	//find correct div and move horizontal bar there!
	var scale_button_target = scale_button_list[index]
	var str = scale_button_target.style.left
	var n_pixels_left=parseInt(str.substring(0, str.length - 2))
	//var tot_buttons = scale_button_list.length
	var value = n_pixels_left-nuzic_block
	PMC_checkScroll_H(value)
}


