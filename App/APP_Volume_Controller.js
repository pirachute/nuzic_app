//master volume
function APP_set_master_volume(value){
	if(value>1)value=1	//compatibility
	outputMasterVolume.value = value

	if (value == 0){
		Tone.Destination.mute = true
	} else {
		Tone.Destination.mute = false
		var dB = APP_volume_value_to_dB(value)
		APP_change_master_volume(dB)
	}
}

function APP_slider_master_volume(){
	let value = APP_read_master_volume()
	APP_set_master_volume(value)
	DATA_save_preferences_file()
}

function APP_read_master_volume(){
	return Number(outputMasterVolume.value)
}

function APP_change_master_volume(value){
	//value in dB
	masterVolume.volume.value=value
	if(value==-20){
		masterVolume.mute=true
	}else{
		masterVolume.mute=false
	}
}

//main metronome volume

function APP_set_main_metronome_volume(value){
	if(value>1)value=1	//compatibility
	outputMainMetronomeVolume.value = value

	// if (value == 0){
		//Tone.Destination.mute = true
	// } else {
		//Tone.Destination.mute = false
		var dB = APP_volume_value_to_dB(value)
		APP_change_main_metronome_volume(dB)
	// }
}

function APP_slider_main_metronome_volume(){
	let value = APP_read_main_metronome_volume()
	APP_set_main_metronome_volume(value)
	DATA_save_preferences_file()
}

function APP_read_main_metronome_volume(){
	return Number(outputMainMetronomeVolume.value)
}

function APP_change_main_metronome_volume(value){
	//value in dB
	mainMetronomeVolume.volume.value=value
	if(value==-20){
		mainMetronomeVolume.mute=true
	}else{
		mainMetronomeVolume.mute=false
	}
}


//dB

function APP_volume_value_to_dB(value){
	//transform volume value 0-1 to decibel value -20 a 0
	var dB = -20 + value * 20
	//console.log(dB)
	return dB
}
