var database_version = 8.0

function DATA_initialize_state_point(data){
	current_state_point_index = 0
	min_state_point_index=current_state_point_index
	max_state_point_index=current_state_point_index
	state_point_array[current_state_point_index]=data
}

function DATA_insert_new_state_point(data_in){
	//control version and save
	data = DATA_control_database_version(data_in)
	current_state_point_index++
	if(min_state_point_index!=0)min_state_point_index=current_state_point_index+1
	if(min_state_point_index==state_point_array.length)min_state_point_index=0
	if(current_state_point_index==state_point_array.length) {
		current_state_point_index=0
		min_state_point_index=1
	}
	max_state_point_index=current_state_point_index

	//console.log("inserting state point in "+current_state_point_index+"  min "+min_state_point_index+" max "+max_state_point_index)
	state_point_array[current_state_point_index]=data
	//console.error("saved")
	//console.log(data)
	flag_DATA_updated.PMC=false
	flag_DATA_updated.RE=false
}

function DATA_get_previous_state_point(){
	if(current_state_point_index==min_state_point_index){
		console.log("Ended number of saved sates")
		return null
	} else {
		var previous_state_point_index = current_state_point_index -1
		if (previous_state_point_index<0)previous_state_point_index=state_point_array.length-1
		current_state_point_index=previous_state_point_index
		return JSON.parse(JSON.stringify(state_point_array[previous_state_point_index]))
	}
}

function DATA_get_current_state_point(integrate_current_preferences=true){
	//return A COPY of current index state point
	var data_copy=JSON.parse(JSON.stringify(state_point_array[current_state_point_index]))

	if(integrate_current_preferences){
		//PMC POSITION , ZOOM, AND VISIBILITY
		data_copy.global_variables.app_containers=DATA_read_app_containers_button_states()
		data_copy.global_variables.PMC=DATA_read_PMC_global_variables()
		data_copy.global_variables.RE=DATA_read_RE_global_variables()
		//NOTEBOOK
		data_copy.notebook = document.getElementById('Notebook').innerHTML
		//count in
		data_copy.global_variables.count_in=TNAV_read_countIn()
	}

	data_copy.database_version = database_version
	return data_copy
}

function DATA_get_next_state_point(){
	if(current_state_point_index==max_state_point_index){
		console.log("No more next saved sates")
		return null
	} else {
		var previous_state_point_index = current_state_point_index +1
		if (previous_state_point_index==state_point_array.length)previous_state_point_index=0
		current_state_point_index=previous_state_point_index
		return JSON.parse(JSON.stringify(state_point_array[previous_state_point_index]))
	}
}

function DATA_get_selected_voice_data(){
	var data = DATA_get_current_state_point(false)
	// find current voice database v6
	var selected_voice_data= data.voice_data.find(item=>{
		return item.selected
	})
	return selected_voice_data
}

function DATA_get_PMC_visible_voice_data(){
	var data = DATA_get_current_state_point(false)
	// find current voice database v6
	var visible_voices_data = data.voice_data.filter(item=>{
		//return !item.selected and visible
		return item.PMC_visible && !item.selected
	})

	//return JSON.parse(JSON.stringify(data.voice_data[0]))
	return visible_voices_data
}

function DATA_control_database_version(data_in){
	//crerate a copy of the data
	var data = JSON.parse(JSON.stringify(data_in))

	if (data.database_version==database_version){//
		//control y CORRECTION DIESIS for database ==7
		//control BUG scale sequence not strings for database ==7

		if(DEBUG_MODE){
			//verify midi data
			// data.voice_data.forEach(voice=>{
			// 	voice.data.segment_data.forEach(segment=>{
			// 		var old=-1
			// 		segment.time.forEach((obj,index)=>{
			// 			var now=obj.P+obj.F/100
			// 			if(now<=old)console.error("ERROR TIME voice "+voice.name+" v."+voice.voice_index+" s."+index+" P."+obj.P+" F."+obj.F)
			// 		})
			// 	})
			// 	if(voice.name=="5 kik")console.log(voice.data.midi_data)
			// })
		}

		return data
	}




	if (data.database_version==null){
		console.error("no database_version found")
		data.database_version=4
	}
	//change of version
	if (data.database_version<3){
		console.info("Old Database , changing compas data")
		//changed .compas
		//from array of array[3]
		//to  array of .compas_values	{int , int, int , [int ,int ],int}
		//								 start, Lc , rep , accents
		var mom = data.compas
		//transform into new version
		data.compas = 0
		var index=0
		mom.forEach(item => {
			//var data = {global_variables:[], compas:[], sound_quantization:[], voice_data:[]}
			var compas_values = {compas_values:[parseInt(item[0]),parseInt(item[1]),parseInt(item[2]),[0],0]}//XXX no rotation???
			mom[index]=compas_values
			//console.log(compas_values)
			//compas_value=item
			//datatest.compas[index].compas_value[3]=[0]
			index++
		})
		data.compas = mom
	}

	//change of version

	// tet,name,iS,letter,module,tags
	idea_scale_list.scale_list = []
	idea_scale_list.TET_list = []
	//letter,start,rotation,duration
	var scale_sequence =[]
	//change of version
	if (data.database_version<4){
		console.info("Old Database , changing scale list data")
		//added scale.scale_list and scale.scale_sequence
		var scale = JSON.parse(JSON.stringify({idea_scale_list: idea_scale_list ,scale_sequence:scale_sequence}))
		data.scale = scale
	}

	//change of version
	//voice number / voice name, Minimap/PMC/RE shown
	//change of version
	if (data.database_version<5){
		console.info("Old Database, changing data")
		data.global_variables.app_containers= {"RE":true, "PMC":false,"MMap":true, "ES_tab": false, "ET_tab":false, "Key_tab":true, "tabs_opened":false}

		var voice_index= 0
		data.voice_data.forEach(item=>{
			item.voice_index=voice_index
			voice_index++
		})

		//verify midi_data note
		//bug midi_data.note to note_freq
		data.voice_data.forEach(item=>{
			if(item.data.midi_data.note!=null){
				item.data.midi_data.note_freq = item.data.midi_data.note
				delete item.data.midi_data.note
			}
		})
	}


	if (data.database_version<6){
		//count in
		//PMC
		//zoom(s)
		//scrollbar position
		//axis type (Na,Pa...)
		//RE_preview
		//elements shown (Na iS O iT in PMC and type)
		//PMC module button states
		//VOICES
		//voice selected
		//voice PMC visible
		//voice color
		//segment_name

		//string to int
		//volume
		//instrument
		//metro
		//fraction
		//time
		//note s-1  e-2  L-3!!!  ·-4!!!
		//scale duration

		console.info("Old Database, changing data")
		//count in
		data.global_variables.count_in = 0

		//PMC
		data.global_variables.PMC={}
		//zoom(s)
		data.global_variables.PMC.zoom_x=1
		data.global_variables.PMC.zoom_y=1
		//scrollbar position
		data.global_variables.PMC.scroll_x=0
		data.global_variables.PMC.scroll_y=0
		//RE_preview
		data.global_variables.PMC.vsb_RE=false
		//elements shown (Na iS O iT in PMC and type)
		data.global_variables.PMC.vsb_N={"show":true, "type":"Na"}
		data.global_variables.PMC.vsb_iN={"show":true, "type":"iNa"}
		data.global_variables.PMC.vsb_T={"show":true, "type":"Ta"}
		data.global_variables.PMC.vsb_iTa={"show":true, "type":"iTa"}
		//PMC module button states NEW
		//console.log(data.global_variables)
		data.global_variables.module_button_states={"ES_PMC":true,"CET_PMC":true,"ES_RE":true,"CET_RE":true}

		//VOICES
		var index=0
		data.voice_data.forEach(voice=>{
			//voice selected
			if(index==0){
				voice.selected=true
			}else{
				voice.selected=false
			}
			//voice PMC visible
			voice.PMC_visible=true
			//voice color
			voice.color="nuzic_green"

			//str to int
			voice.volume=parseInt(voice.volume)
			voice.instrument=parseInt(voice.instrument)
			voice.metro=parseInt(voice.metro)

			index++
			voice.data.segment_data.forEach(segment=>{
				segment.segment_name=""
				//str to int
				var index_element=0
				segment.note.forEach(note=>{
					if(note=="s"){
						segment.note[index_element]=-1
					}else if(note=="e"){
						segment.note[index_element]=-2
					}else if(note=="L"){
						segment.note[index_element]=-3
					}else if(note==end_range){
						segment.note[index_element]=-4
					}else{
						segment.note[index_element]=parseInt(note)
					}
					index_element++
				})

				index_element=0
				var current_pulse=0
				segment.time.forEach(time=>{
					var split = time.split('.')
					var P=Math.floor(split[0])
					var F=Math.floor(split[1])
					if(split[1]==null)F=0
					if(P==0)P=current_pulse
					current_pulse=P
					segment.time[index_element]={P,F}
					index_element++
				})

				index_element=0
				segment.fraction.forEach(fraction=>{
					var split = fraction[0].split('/')
					var N=Math.floor(split[0])
					var D=Math.floor(split[1])
					segment.fraction[index_element]={"N":N,"D":D,"start":parseInt(fraction[1]),"stop":parseInt(fraction[2])}
					index_element++
				})
			})
			//data.midi_data correcting bug truncate NO NEED , done in next check
			/*
			var old_PPM = PPM
			PPM=data.global_variables.PPM
			voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
			PPM=old_PPM
			*/
		})

		//SCALE duration is a number
		data.scale.scale_sequence.forEach(sequence=>{
			sequence.duration=parseInt(sequence.duration)
		})
	}
	//XXX add some other validations
	if (data.database_version<7){
		//ADDING//RENAMING LINES
		// PA
		// PS
		// ALFABET NOTATION

		//VOICE VISIBLE== VOICE RE_visible

		//no possible change on C and S line visibility

		console.info("Old Database version " +data.database_version+ " , changing data")

		data.voice_data.forEach(voice=>{
			voice.RE_visible=voice.visible
			delete voice.visible
		})

		//data.global_variables.line_button_states -> data.global_variables.RE
		//add new key
		data.global_variables.RE = {}
		data.global_variables.RE.scroll_x=0
		data.global_variables.RE.scroll_y=0

		data.global_variables.RE.Na = data.global_variables.line_button_states.Na
		data.global_variables.RE.Nm = data.global_variables.line_button_states.Nm
		data.global_variables.RE.Ngm = data.global_variables.line_button_states.Ngm
		data.global_variables.RE.Nabc = false
		data.global_variables.RE.iNa = data.global_variables.line_button_states.iNa
		data.global_variables.RE.iNm = data.global_variables.line_button_states.iNm
		data.global_variables.RE.iNgm = data.global_variables.line_button_states.iNgm

		data.global_variables.RE.Ta = false
		data.global_variables.RE.Tf = data.global_variables.line_button_states.Tf
		data.global_variables.RE.Ts = data.global_variables.line_button_states.Ta
		data.global_variables.RE.Tm = data.global_variables.line_button_states.Tm
		data.global_variables.RE.iT = data.global_variables.line_button_states.iTa

		data.global_variables.PMC.vsb_Nabc = true
		data.global_variables.PMC.vsb_iT=data.global_variables.line_button_states.iTa
		delete data.global_variables.line_button_states.iTa

		//console.log(data.global_variables.line_button_states)
		//console.log(data.global_variables.RE)
		delete data.global_variables.line_button_states

		//no module button states anymore
		//detete data.global_variables.module_button_states
		data.global_variables.module_button_states.ES_RE=true
		data.global_variables.module_button_states.CET_RE=true
		data.global_variables.module_button_states.ES_PMC=true
		data.global_variables.module_button_states.CET_PMC=true

		//delete data.global_variables.module_button_states
		if(data.global_variables.hasOwnProperty("iA_button_states")){
			data.global_variables.RE.iAa=data.global_variables.iA_button_states.iAa
			data.global_variables.RE.iAgm=data.global_variables.iA_button_states.iAgm
			data.global_variables.RE.iAm=data.global_variables.iA_button_states.iAm
			delete data.global_variables.iA_button_states
		}else{
			data.global_variables.RE.iAa=false
			data.global_variables.RE.iAgm=false
			data.global_variables.RE.iAm=true
		}


		//BUG THOMAS , control no null values in data compas
		data.compas.forEach((compas,index,array)=>{
		//console.log(compas.compas_values)
			if (compas.compas_values[4]==null){
				//console.error("Compas rotation null value")
				//console.log(array)
				//console.log(index)
				array[index].compas_values[4]=0
				return
			}
		})
		//console.log(data.compas)

		data.voice_data.forEach(voice=>{
			voice.data.segment_data.forEach(segment=>{
				//every note is # or +
				segment.diesis = segment.note.map(index=>{
					return true
				})
			})

			//neopulse=[1,1]  => neopulse.N    neopulse.D
			voice.neopulse={N:voice.neopulse[0],D:voice.neopulse[1]}
			//new truncate
			//data.midi_data correcting bug truncate

			var old_PPM = PPM
			PPM=data.global_variables.PPM
			voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
			PPM=old_PPM
		})
	}

	if (data.database_version<8){
		//control y CORRECTION DIESIS //for database ==7
		console.info("Old Database version " +data.database_version+ " , changing data")
		var diesis_correction=false
		data.voice_data.forEach(voice=>{
			voice.data.segment_data.forEach(segment=>{
				//every note is # or +

				if(segment.diesis==undefined){
					segment.diesis=[true,true]
					diesis_correction=true
				}
				var d_length = segment.diesis.length
				var t_length = segment.time.length

				if(d_length>t_length){
					for (let i = 0; i < d_length-t_length; i++) {
						segment.diesis.pop()
					}
					diesis_correction=true
				}

				if(d_length<t_length){
					for (let i = 0; i < t_length-d_length; i++) {
						segment.diesis.push(true)
					}
					diesis_correction=true
				}
			})
		})

		if(diesis_correction){
			console.error("DIESIS NOT SAME LENGTH OF TIME - NOTE - DIESIS")
			APP_error_popup("ERROR GENERATING DATABASE: note diesis array\n\nError patched")
		}

		//control BUG scale sequence not strings
		data.scale.scale_sequence.forEach(item=>{
			item.duration=parseInt(item.duration)
			item.starting_note=parseInt(item.starting_note)
			item.rotation=parseInt(item.rotation)
		})

		//add CALC tab
		data.global_variables.app_containers.Calc_tab=false
		// var app_containers=data.global_variables.app_containers
		// var cipolla = JSON.parse(JSON.stringify({"RE":app_containers.RE, "PMC":app_containers.PMC,
		// 		"MMap":app_containers.MMap, "ES_tab": app_containers.ES_tab, "ET_tab":app_containers.ET_tab, "Key_tab":app_containers.Key_tab,
		// 		"CALC_tab": false,"tabs_opened":app_containers.tabs_opened}))
		// 		data.global_variables.app_containers={}
		// 		data.global_variables.app_containers=cipolla

		//add sound definition for pulse
		//delete key polipulse
		data.voice_data.forEach(voice=>{
			voice.e_sound=22
			delete voice.polipulse
		})


		//BUG delete voice from RE create multiple selection
		//verify single voice selected
		var selection= data.voice_data.filter(voice=>{return voice.selected})
		if(selection.length!=1){
			//clear selection and select first voice
			data.voice_data.forEach(voice=>{voice.selected=false})
			data.voice_data[0].selected=true
		}
	}

	if (data.database_version>database_version){
		console.error("incompatible database")
		data="error"
	}

	data.database_version=database_version

	return data
}

function DATA_load_state_point_data(new_file=true,reload_extra=true){
	//new_file reload values meant to be loaded only with new files and not in undo redo (values change doesn't trigger save new state)
	if(new_file)reload_extra=true

	//select and clean current objects
	var use_current_visualization_options = !reload_extra
	var data = DATA_get_current_state_point(use_current_visualization_options)

	if(data=="error" || data==null){
		console.error("error in loading: database not found")
		return
	}

	//GLOBAL VARIABLES
	var global = data.global_variables

	//[Li,PPM,TET,line_button_states]
	old_Li = Li
	Li = global.Li
	PPM = global.PPM

	if(TET != global.TET){
		TET = global.TET
		BNAV_Refresh_Keyboard_On_TET_Change()
	}

	APP_populate_global_inputs(data)
	TNAV_convertLiToTime()
	if(reload_extra){
		TNAV_set_countIn(global.count_in)
	}

	//modules lines
	//APP_set_module_button_states(global.module_button_states) //OBSOLETE XXX
	//minimap PMC - RE - tabs opened/closed
	DATA_set_app_containers_button_states(global.app_containers)

	//COMPAS
	var compas_values = data.compas
	var current_compas_values = BNAV_read_current_compas_sequence()
	//compare arrays to detect if there is a change
	if(JSON.stringify(compas_values)===JSON.stringify(current_compas_values)){
		//nothing to do
	} else {
		BNAV_write_compas_sequence(compas_values)
	}
	//progress bar
	var end_loop_at_Li=false
	if(APP_return_progress_bar_values()[2]>=old_Li)end_loop_at_Li=true
	APP_redraw_Progress_bar_limits(new_file,end_loop_at_Li)

	//SOUND LINE
	var scale_values =JSON.parse(JSON.stringify(data.scale))
	//it contains 	.idea_scale_list that contains 	.scale_list
	//												.TET_list
	var current_scale_values = BNAV_read_current_scale_data()

	//compare arrays to detect if there is a change
	if(JSON.stringify(scale_values)===JSON.stringify(current_scale_values)){
		//nothing to do
	} else {
		BNAV_write_scale_values(scale_values)
	}
	//2 times (1 for data to reload)
	if(reload_extra){
		DATA_set_RE_global_variables(global.RE)
		DATA_set_PMC_global_variables(global.PMC)
	}

	//OPTIMIZED
	//redraw either PMC OR RE
	//-1 no start progress bar glitch
	current_position_PMCRE = {voice_data:DATA_get_selected_voice_data() ,segment_index:0 , pulse: 0, fraction: 0, note: null, X_start:-1, Y_start:null,REscrollbar_redraw:true}
	//0 no first click glitch
	//current_position_PMCRE = {voice_data:DATA_get_selected_voice_data() ,segment_index:0 , pulse: 0, fraction: 0, note: null, X_start:0, Y_start:null}

	DATA_smart_redraw()
	//2 times (1 for x y position)
	if(reload_extra){
		DATA_set_RE_global_variables(global.RE)
		DATA_set_PMC_global_variables(global.PMC)
	}

	//Notebook
	var notebook = document.getElementById('Notebook')
	if(data.notebook!=null){
		notebook.innerHTML = data.notebook
	}else{
		notebook.innerHTML = ""
	}
	//current_file_name = file_name
	APP_load_minimap()
	APP_refresh_minimap()
}

function DATA_smart_redraw(){
	//redraw eventually play line OR current position IF i'm about to change something
	//copy
	var  t1, t2,
	startTimer = function() {
		t1 = new Date().getTime();
		return t1;
	},
	stopTimer = function() {
		t2 = new Date().getTime();
		return t2 - t1;
	}
	var provv = JSON.parse(JSON.stringify(current_position_PMCRE))
	//FLAGS!!!!!
	if (tabPMCbutton.checked){
		if(flag_DATA_updated.PMC)return
		//Draw PMC
		//separated form global redraw PMC
		PMC_set_V_scrollbar_step_N()
		PMC_redraw_sound_line()
		PMC_populate_voice_list()

		//true redraw of PMC
		startTimer()

		PMC_redraw()
		console.log('Regular rendering of ALL PMC in ' + stopTimer() + 'ms')
		current_position_PMCRE=provv
		//if voice.selected != current_position_PMCRE.voice_data
		if(current_position_PMCRE!=null && !current_position_PMCRE.voice_data.selected){
			//console.error("cambio")
			PMC_reset_action_pmc()
			//clear eventually selected columns animate_play_green
			APP_reset_blink_play()
		}else{
			//draw
			if(current_position_PMCRE!=null)PMC_draw_progress_line_position(current_position_PMCRE.X_start)
		}
		flag_DATA_updated.PMC=true
	}else{
		if(flag_DATA_updated.RE)return
		//Draw RE
		startTimer()
		//not optimized
		//RE_redraw_old()//copy of wild dance 2592ms
		//RE_redraw_old_not_check_changes()
		RE_redraw()

		console.log('Regular rendering of ALL RE in ' + stopTimer() + 'ms')
		//XXX
		current_position_PMCRE=provv

		flag_DATA_updated.RE=true
	}
}

//searching which element of an array is equal to which and in which position or which one is changed XXX not used
function DATA_search_into_JSON_arrays(array,item){

	//look if there is MORE then 1 segment!!!
	var found_index_array = []
	var i=0
	array.forEach(item2=>{
		if(JSON.stringify(item)===JSON.stringify(item2)){
			found_index_array.push(i)
		}
		i++
	})

	return found_index_array
}

async function DATA_save_local(){
	var user = 0
	if(typeof wp_Get_User_Permissions!='function' || DEBUG_MODE){
		if(DEBUG_MODE){
			user=100
		}else{
			return
		}
	} else{
		user = await wp_Get_User_Permissions('','levels')
	}
	if(user==0){return}
	APP_stop()
	TNAV_read_composition_name()
	var current_state_point = DATA_get_current_state_point(true)
	//const result = await window.chooseFileSystemEntries({ type: "save-file" });

	/* var element = document.getElementsByClassName('files_div')

	element[0].style.display = 'none' */


	//console.log(current_state_point)
	//save


	const a = document.createElement('a')
	const file = new Blob([JSON.stringify(current_state_point)], {type: 'text/cvs'}) //text/plain

	//provv save preferences...
	//var data = DATA_read_current_state_preferences()
	//const file = new Blob([JSON.stringify(data)], {type: 'text/cvs'})

	a.href= URL.createObjectURL(file)
	a.download = current_file_name+extension
	a.click()

	URL.revokeObjectURL(a.href)
	TNAV_hide_main_menu()
	//get the filename inserted XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
}

async function DATA_load_local(event){
	var user = 0
	if(typeof wp_Get_User_Permissions!='function' || DEBUG_MODE){
		if(DEBUG_MODE){
			user=100
		}else{
			return
		}
	} else{
		user = await wp_Get_User_Permissions('','levels')
	}
	if(user==0){return}
	APP_stop()
	//load data file with FileIO

	let files = await FileIO.openDialog({
		contentTypes: '.nzc, .pmc, text/csv', //, image/*', // Optional. The accepted file types
		allowMultiple: false,    // Optional. Allows multiple file selection. Default is false
	})

	DATA_show_loader()

	//Do something with the selected files
	var file_name = generic_file_name
	files.forEach((file, index) => {
		console.log(`File #${index + 1}: ${file.name}`)
		file_name = file.name.split(".")[0]
	})


	let contents = await FileIO.read({
		data: files[0],                 // File/Blob
		encoding: 'UTF-8',     // ISO-8859-1   Optional. If reading as text, set the encoding. Default is UTF-8
		readAsTypedArray: false,    // Optional. Specifies to read the file as a Uint8Array. Default is false
		onProgressChange: (progressEvent) => { // Optional. Fires periodically as the file is read
			let percentLoaded = Number(((progressEvent.loaded / progressEvent.total) * 100).toFixed(2))
			console.log(`Percent Loaded: ${percentLoaded}`)
		}
	})


	//Do something with the file contents
	//verify the data version and load / convert to actual one
	//
	var data = JSON.parse(contents)

	editor_scale= false

	setTimeout(function(){
		//maybe faster???
		DATA_insert_new_state_point(data)
		DATA_load_state_point_data(true)
		//repositionate the scrollbar
		RE_checkScroll_H(0)
		APP_set_composition_name(file_name)
		TNAV_hide_main_menu()
		DATA_hide_loader(contents)
	}, 20)
}

async function DATA_load_server(){
	//contact server and get a list of user compositions
	var user = 0
	if(typeof wp_Get_User_Permissions!='function' || DEBUG_MODE){
		if(DEBUG_MODE){
			user=100
		}else{
			return
		}
	} else{
		user = await wp_Get_User_Permissions('','levels')
	}
	if(user==0){return}
	WEB_Open_APP_Sidebar('mis-compos',false)
	TNAV_hide_main_menu()
}

async function DATA_export_midi(){
	var user = 0
	if(typeof wp_Get_User_Permissions!='function' || DEBUG_MODE){
		if(DEBUG_MODE){
			user=100
		}else{
			return
		}
	} else{
		user = await wp_Get_User_Permissions('','levels')
	}
	if(user==0){return}
	APP_stop()

	var current_state_point = DATA_get_current_state_point(true)

	//save
	var file_data = Generate_midi_blob(current_state_point)
	if(file_data==null)return

	const a = document.createElement('a')

	var bytesU16 = file_data.toBytes() //returns a UTF-16 encoded string

	//attenzione !!!! UTF-8 is a variable length encoding scheme (Uint8Array and a Uint16Array) leading to data corruption
	//MIDI only use 8 bit encoding
	// Convert data to UTF-8 encoding
	var bytesU8 = new Uint8Array(bytesU16.length)
	for (var i=0; i<bytesU16.length; i++) {
		//only first part of data
		bytesU8[i] = bytesU16[i].charCodeAt(0)
	}

	var blob = new Blob([bytesU8], {type: "audio/midi"})
	a.href= URL.createObjectURL(blob)
	a.download = current_file_name+".mid"
	a.click()

	URL.revokeObjectURL(a.href)
	TNAV_hide_main_menu()
}

function DATA_new_composition_data(){
	//v6
	//var data_in = {"global_variables":{"Li":12,"PPM":120,"TET":12,"count_in":0,"PMC":{"zoom_x":1,"zoom_y":1,"scroll_x":0,"scroll_y":0,"vsb_RE":false,"vsb_N":{"show":true,"type":"Nm"},"vsb_iN":{"show":true,"type":"iNa"},"vsb_T":{"show":true,"type":"Ta"},"vsb_iT":{"show":true,"type":"iTa"}},"line_button_states":{"Na":true,"Nm":false,"Ngm":false,"iNa":true,"iNm":false,"iNgm":false,"Ta":true,"Tm":false,"iTa":true,"Tf":true},"iA_button_states":{"iAa":false,"iAm":true,"iAgm":false},"module_button_states":{"ES_RE":true,"CET_RE":true,"ES_PMC":true,"CET_PMC":true},"app_containers":{"RE":true,"PMC":false,"MMap":false,"ES_tab":true,"ET_tab":false,"Key_tab":false,"tabs_opened":false}},"compas":[{"compas_values":[0,0,0,[0],0]}],"scale":{"idea_scale_list":{"scale_list":[],"TET_list":[12]},"scale_sequence":[]},"voice_data":[{"name":"Voice 1","voice_index":0,"blocked":true,"visible":true,"volume":3,"solo":false,"instrument":0,"metro":0,"polipulse":"???","neopulse":[1,1],"selected":true,"PMC_visible":true,"color":"nuzic_blue","data":{"segment_data":[{"time":[{"P":0,"F":0},{"P":12,"F":0}],"fraction":[{"N":1,"D":1,"start":0,"stop":12}],"note":[-1,-4],"segment_name":""}],"midi_data":{"time":[0],"duration":[0.16666],"note_freq":[-1]}}}],"notebook":"","database_version":6}
	//v7
	//var data_in = {"global_variables":{"Li":12,"PPM":120,"TET":12,"count_in":0,"PMC":{"zoom_x":1,"zoom_y":1,"scroll_x":0,"scroll_y":0,"vsb_RE":true,"vsb_N":{"show":true,"type":"Nm"},"vsb_Nabc":true,"vsb_iN":{"show":true,"type":"iNm"},"vsb_T":{"show":true,"type":"Ts"},"vsb_iT":{"show":true,"type":"iT"}},"module_button_states":{"ES_RE":true,"CET_RE":true,"ES_PMC":true,"CET_PMC":true},"app_containers":{"RE":true,"PMC":false,"MMap":false,"ES_tab":true,"ET_tab":false,"Key_tab":false,"tabs_opened":false},"RE":{"Na":false,"Nm":true,"Ngm":false,"Nabc":false,"iNa":false,"iNm":true,"iNgm":false,"iAa":false,"iAm":false,"iAgm":false,"Ta":false,"Tm":false,"Ts":true,"iT":true,"Tf":true,"scroll_x":0,"scroll_y":0}},"compas":[{"compas_values":[0,0,0,[0],0]}],"scale":{"idea_scale_list":{"scale_list":[],"TET_list":[12]},"scale_sequence":[]},"voice_data":[{"name":"Voz 1","voice_index":0,"blocked":true,"volume":3,"solo":false,"instrument":0,"metro":0,"polipulse":"???","neopulse":{"N":1,"D":1},"selected":true,"PMC_visible":true,"color":"nuzic_blue","data":{"segment_data":[{"time":[{"P":0,"F":0},{"P":12,"F":0}],"fraction":[{"N":1,"D":1,"start":0,"stop":12}],"note":[-1,-4],"segment_name":"","diesis":[true,true]}],"midi_data":{"time":[0],"duration":[0.16666],"note_freq":[-1]}},"RE_visible":true}],"notebook":"","database_version":7}
	//v8
	var data_in = {"global_variables":{"Li":12,"PPM":120,"TET":12,"count_in":0,"PMC":{"zoom_x":1,"zoom_y":1,"scroll_x":0,"scroll_y":0,"vsb_RE":true,"vsb_N":{"show":true,"type":"Nm"},"vsb_Nabc":true,"vsb_iN":{"show":true,"type":"iNm"},"vsb_T":{"show":true,"type":"Ts"},"vsb_iT":{"show":true,"type":"iT"}},"module_button_states":{"ES_RE":true,"CET_RE":true,"ES_PMC":true,"CET_PMC":true},"app_containers":{"RE":true,"PMC":false,"MMap":false,"ES_tab":true,"ET_tab":false,"Key_tab":false,"Calc_tab":false,"tabs_opened":false},"RE":{"Na":false,"Nm":true,"Ngm":false,"Nabc":false,"iNa":false,"iNm":true,"iNgm":false,"iAa":false,"iAm":false,"iAgm":false,"Ta":false,"Tm":false,"Ts":true,"iT":true,"Tf":true,"scroll_x":0,"scroll_y":0}},"compas":[{"compas_values":[0,0,0,[0],0]}],"scale":{"idea_scale_list":{"scale_list":[],"TET_list":[12]},"scale_sequence":[]},"voice_data":[{"name":"Voz 1","voice_index":0,"blocked":true,"volume":3,"solo":false,"instrument":0,"metro":0,"e_sound":22,"neopulse":{"N":1,"D":1},"selected":true,"PMC_visible":true,"color":"nuzic_blue","data":{"segment_data":[{"time":[{"P":0,"F":0},{"P":12,"F":0}],"fraction":[{"N":1,"D":1,"start":0,"stop":12}],"note":[-1,-4],"segment_name":"","diesis":[true,true]}],"midi_data":{"time":[0],"duration":[0.16666],"note_freq":[-1]}},"RE_visible":true}],"notebook":"","database_version":8}

	var data = DATA_control_database_version(data_in)

	return data
}

async function DATA_new(){
	APP_stop()
	DATA_show_loader()
	setTimeout(function(){
		//make with pure DATA ??
		var data = DATA_new_composition_data()
		DATA_insert_new_state_point(data)
		DATA_load_state_point_data(true)

		TNAV_reset_composition_name()
		current_composition_id=""
		if(typeof Reset_current_composition_id === "function"){
			Reset_current_composition_id()
		}
		DATA_hide_loader()
		TNAV_hide_main_menu()
	}, 20)
}



//local HTML5 storage for preferences
function DATA_save_preferences_file(){
	var data = DATA_read_current_state_preferences()
	localStorage.setItem('nuzic_preferences', JSON.stringify(data))
}

function DATA_load_preferences_file(){
	var contents = localStorage.getItem('nuzic_preferences')
	if(contents!=null){
		var pref_data = JSON.parse(contents)
		if(pref_data.database_version==database_version ){
			//GLOBAL OPTIONS
			//show or not hover labels
			Set_hover_labels(pref_data.showHoverLabels)
			//Play naw note pressing enter or arrows
			SNAV_set_play_new_note(pref_data.playNewNote)
			SNAV_set_note_on_click(pref_data.playNoteOnClick)
			//XXX DISABLED
			//SNAV_set_note_over_grade(pref_data.noteOverGrade)
			SNAV_set_note_over_grade(true)
			//XXX DISABLED
			//SNAV_set_alert_overwrite_scale(pref_data.showAlertOverwriteScale)
			SNAV_set_alert_overwrite_scale(true)
			//main volume
			APP_set_master_volume(pref_data.mainVolume)
			//main metronome
			TNAV_set_main_metronome_OnOff(pref_data.mainMetronome)
			//old version of pref_data
			if(pref_data.mainMetronomeVolume==null)pref_data.mainMetronomeVolume=1
			//console.log(pref_data.mainMetronomeVolume)
			APP_set_main_metronome_volume(pref_data.mainMetronomeVolume)
			//main loop
			TNAV_set_loop_OnOff(pref_data.mainLoop)

			/*/COMPOSITION_DATA EXCLUDED
			data.global_variables.PMC=pref_data.PMC
			data.global_variables.RE=pref_data.RE

			//modules
			data.global_variables.module_button_states = pref_data.module_button_states
			//app containers
			if(pref_data.app_containers!=null){
				data.global_variables.app_containers = pref_data.app_containers
			}
			*/
		}else{
			console.error("App stored preferences version obsolete")
		}
	}
	return data
}

//load scale list from general database
function DATA_load_global_scale_list(){
	var contents = localStorage.getItem('nuzic_global_scales')

	if(contents!=null){
		//start loading this one
		console.log("Loading local file Global scales")
		global_scale_list=JSON.parse(contents)
		BNAV_generate_TET_list_from_scale_list(global_scale_list)
		BNAV_refresh_global_scale_tab()
	}else{
		//create a VERY basic database
		//TET 6
		//TET 12
		//TET 24
		global_scale_list={scale_list:[
			{
				TET:6,
				module:3,
				name:"Aldo",
				iS:[2, 2, 2],
				tags:[],
				info:""
			},
			{
				TET:12,
				module:3,
				name:"Giovanni",
				iS:[4, 4 ,4],
				tags:[],
				info:""
			},
				{
				TET:24,
				module:2,
				name:"Giacomo",
				iS:[8, 8, 8],
				tags:[],
				info:""
			}
		]}

		//save this database in nuzic_global_scales
		localStorage.setItem('nuzic_global_scales', JSON.stringify(global_scale_list))
		//refresh tab global scale
		BNAV_generate_TET_list_from_scale_list(global_scale_list)
		BNAV_refresh_global_scale_tab()
	}

	//try to retrive from server AND eventually use them in the global scale list

	var url = 'https://cdn.nuzic.org/Arch/Scale_gobal.dat'
	// read text from URL location
	var request = new XMLHttpRequest()
	request.open('GET', url, true)
	//request.setRequestHeader("Cache-Control", "no-cache, no-store, max-age=0")
	request.send(null)
	request.onreadystatechange = function () {
		if (request.readyState === 4 && request.status === 200) {
			var type = request.getResponseHeader('Content-Type')
			if (type.indexOf("text") !== 1) {
				var returnValue = request.responseText
				var datatest = JSON.parse(returnValue)
				var current_datatest = {scale_list: global_scale_list.scale_list}
				//refresh tab global scale if there is a new database
				if(JSON.stringify(datatest)!=JSON.stringify(current_datatest)){
					global_scale_list=datatest
					//save this database in nuzic_global_scales
					console.log("Loading NEW external file Global scales")

					//attention at ===scale BUT different tags!!!!!!!!!!! XXX

					localStorage.setItem('nuzic_global_scales', JSON.stringify(global_scale_list))
					BNAV_generate_TET_list_from_scale_list(global_scale_list)
					BNAV_refresh_global_scale_tab()
				}else{
					//nothing to do
				}

				return returnValue
			}
		}
	}

	//test
	// var requestTEST = new XMLHttpRequest();
	// requestTEST.open("GET",'test2.pmc', false)
	// requestTEST.send(null)
	// var returnValueTEST = requestTEST.responseText
	// var datatest = JSON.parse(returnValueTEST)
	// console.log("test2")
	// console.log(datatest)
}

function DATA_save_user_scale_list(){
	var data_to_save= {scale_list: user_scale_list.scale_list}
	//i dont save TET_list
	//clear letters!!! no need
	//data_to_save.scale_list.forEach(item=>{
	//	item.letter=""
	//})

	//save locally
	localStorage.setItem('nuzic_user_scales', JSON.stringify(data_to_save))

	//save on cloud XXX
}

//load user scale list
function DATA_load_user_scale_list(){
	var contents = localStorage.getItem('nuzic_user_scales')

	if(contents!=null){
		//start loading this one
		console.log("Loading local file User scales")
		user_scale_list=JSON.parse(contents)
		BNAV_generate_TET_list_from_scale_list(user_scale_list)
		BNAV_refresh_user_scale_tab()
	}else{
		//create a VERY basic database
		user_scale_list={scale_list:[
		]}

		BNAV_generate_TET_list_from_scale_list(user_scale_list)
		//save this database in nuzic_user_scales
		DATA_save_user_scale_list()
		//refresh tab global scale
		BNAV_refresh_user_scale_tab()
	}

	//try to retrive USER SCALE from server AND eventually merge them
	var url = 'https://cdn.nuzic.org/Arch/Scale_UID000000000.dat'
	// read text from URL location
	var request = new XMLHttpRequest()
	request.open('GET', url, true)
	request.send(null)
	request.onreadystatechange = function () {
		if (request.readyState === 4 && request.status === 200) {
			var type = request.getResponseHeader('Content-Type')
			if (type.indexOf("text") !== 1) {
				var returnValue = request.responseText
				var datatest = JSON.parse(returnValue)
				//refresh tab global scale if there is a new database
				if(JSON.stringify(datatest)!=JSON.stringify(user_scale_list)){
					console.log("Loading and merging external file User scales")
					//MERGE 2 data serts
					var union = [...new Set([...user_scale_list.scale_list ,...datatest.scale_list])]
					var merged = union.filter((thing, index) => {
						return index === union.findIndex(obj => {
							return JSON.stringify(obj) === JSON.stringify(thing)
						})
					})
					user_scale_list.scale_list = merged
					BNAV_generate_TET_list_from_scale_list(user_scale_list)
					DATA_save_user_scale_list()
					BNAV_refresh_user_scale_tab()
				}else{
					//nothing to do
				}

				return returnValue
			}
		}
	}
}

function DATA_save_scale_list_bookmarks(){
	//save locally
	localStorage.setItem('nuzic_user_scales_bookmarks', JSON.stringify(global_scale_list_bookmarks))

	//save on cloud XXX
}

function DATA_load_scale_list_bookmarks(){
	var contents = localStorage.getItem('nuzic_user_scales_bookmarks')
	if(contents!=null){
		//start loading this one
		console.log("Loading local file global scales bookmarks")
		global_scale_list_bookmarks=JSON.parse(contents)
	}

	//try to retrive USER SCALE from server AND eventually merge them			XXX
	var url = 'https://cdn.nuzic.org/Arch/Scale_pref_UID000000000.dat'
	// read text from URL location
	var request = new XMLHttpRequest()
	request.open('GET', url, true)
	request.send(null)
	request.onreadystatechange = function () {
		if (request.readyState === 4 && request.status === 200) {
			var type = request.getResponseHeader('Content-Type')
			if (type.indexOf("text") !== 1) {
				var returnValue = request.responseText
				var datatest = JSON.parse(returnValue)
				//refresh tab global scale if there is a new database
				if(JSON.stringify(datatest)!=JSON.stringify(global_scale_list_bookmarks)){
					console.log("Loading and merging external file scales preferences")
					//MERGE 2 data serts
					var union = [...new Set([...global_scale_list_bookmarks ,...datatest])]
					var merged = union.filter((thing, index) => {
						return index === union.findIndex(obj => {
							return JSON.stringify(obj) === JSON.stringify(thing)
						})
					})

					global_scale_list_bookmarks = merged
					//some kind of refresh
					//Meh
					BNAV_refresh_user_scale_tab()
					BNAV_refresh_global_scale_tab()
				}else{
					//nothing to do
				}
				return returnValue
			}
		}
	}
}

//
function DATA_read_current_state_preferences(){
	var data = {}
	data.database_version=database_version

	//show or not hover labels
	data.showHoverLabels = Read_hover_labels()
	//Play new note pressing enter or arrows
	data.playNewNote = SNAV_read_play_new_note()
	//play note on click
	data.playNoteOnClick = SNAV_read_note_on_click()
	//Note over grade XXX DISABLED BUTTON
	//data.noteOverGrade = SNAV_read_note_over_grade()
	data.noteOverGrade = true
	//Alert scale is overwritten XXX DISABLED BUTTON
	//data.showAlertOverwriteScale = SNAV_read_alert_overwrite_scale()
	data.showAlertOverwriteScale = true
	//main volume
	data.mainVolume = APP_read_master_volume()
	//main metronome
	data.mainMetronome = (outputMainMetronome.value === 'true')
	data.mainMetronomeVolume = APP_read_main_metronome_volume()
	//main loop
	data.mainLoop = (outputLoop.value === 'true')

	/*/visualization options lines
	var RE_global_variables  = DATA_read_RE_global_variables()
	RE_global_variables.scroll_x = 0
	RE_global_variables.scroll_y = 0
	data.RE = RE_global_variables

	var PMC_global_variables = DATA_read_PMC_global_variables()
	PMC_global_variables.scroll_x = 0
	PMC_global_variables.scroll_y = 0
	data.PMC = PMC_global_variables

	//module lines OBSOLETE
	//data.module_button_states = APP_read_module_button_states()
	data.module_button_states={ES_RE:true,CET_RE:true,ES_PMC:true,CET_PMC:true}

	data.app_containers = DATA_read_app_containers_button_states()
	*/
	return data
}


function DATA_show_loader(){
	var background = document.getElementById('App_loading_background')
	var bouncer = document.getElementById('App_loading_bouncer')
	//show background loader
	background.style.display = 'flex'
	bouncer.style.display = 'flex'
}

function DATA_hide_loader(){
	var background = document.getElementById('App_loading_background')
	var bouncer = document.getElementById('App_loading_bouncer')
	//hude background loader
	background.style.display = 'none'
	bouncer.style.display = 'none'
}

function DATA_show_wait(){ //XXX
	var background = document.getElementById('App_waiting_background')
	//show background loader
	background.style.display = 'flex'
	document.body.style.cursor = 'wait';
	console.error("OBSOLETE FUNCTION wait background")
}

function DATA_hide_wait(){ //XXX
	var background = document.getElementById('App_waiting_background')
	//hude background loader
	background.style.display = 'none'
	document.body.style.cursor = 'default';
	console.error("OBSOLETE FUNCTION wait background")
}

/*
function DATA_export_Ogg(){//OBSOLETE FUNCTION
	console.error("Obsolete function export Ogg")
	//control if all instruments loaded
	if(!APP_verify_sound_ready()){
		return
	}

	var recorder = new Tone.Recorder()
	DATA_show_loader()
	APP_stop()

	//connect instruments to recorder
	masterVolume.disconnect()
	masterVolume.connect(recorder)

	//calculate time registration
	var [now,start,end,max]=APP_return_progress_bar_values()
	//XXX count in not
	var countin = 0
	var time_ms = (Number(end) - Number(start) - countin) * 1000 * 60 / PPM + 50

	//force play bar


	var progressBar = document.querySelector('#progress_bar')
	progressBar.value=start
	//APP_check_minimap_scroll()

	setTimeout(async () => {

		// start recording
		recorder.start()

		//play note
		APP_play()
		// wait for the notes to end and stop the recording
		setTimeout(async () => {
			// the recorded audio is returned as a blob
			APP_stop()
			DATA_hide_loader()
			const recording = await recorder.stop()
			const a = document.createElement('a')

			a.href= URL.createObjectURL(recording)
			a.download = "recording.ogg"
			a.click()

			URL.revokeObjectURL(a.href)
			TNAV_hide_main_menu()

			recorder.dispose()
			setTimeout(async () => {
				// the recorded audio is returned as a blob
				console.log("end recording")
				masterVolume.toDestination()
			}, 200)
		}, time_ms)
	}, 100)
}
*/
/* NO WEB WORKER
function DATA_export_audio_ok(){
	//control if all instruments loaded
	var type=SNAV_read_export_audio_file_format()

	if(!APP_verify_sound_ready()){
		return
	}

	DATA_show_loader()
	APP_stop()

	//calculate time registration
	var [now,start,end,max]=APP_return_progress_bar_values()
	var current_data = DATA_get_current_state_point(true)
	var count_in = current_data.global_variables.count_in

	var play_metronome=false
	if(outputMainMetronome.value === "true"){
		play_metronome=true
	}else{
		//if metonome not active, make no sense rendering more than Li (compas or
		if(end>Li)end=Li
	}

	//var time_ms = (Number(end) - Number(start) - count_in) * 1000 * 60 / PPM + 50
	//var time_ms = (Number(end) - Number(now) + count_in) * 1000 * 60 / PPM +  0.3 * 1000 //buffer
	var time_seconds = (Number(end) - Number(start) + count_in) * 60 / current_data.global_variables.PPM // no buffer

	setTimeout(async () => {
		Tone.Offline(({ transport }) => {
			//not using bar to define transport position

			//var start_seconds = start*(60/current_data.global_variables.PPM)
			var start_seconds = DATA_calculate_exact_time((start),0,0,1,1,1,1,current_data.global_variables.PPM)
			// var end_seconds = end*(60/current_data.global_variables.PPM)

			//chrome and safari need action to start context
			if(Tone.context.state!=='running'){
				Tone.context.resume()
				Tone.start()
			}

			transport.bpm.value = current_data.global_variables.PPM
			transport.loop=false
			transport.seconds = start_seconds
			//transport.timeSignature = end_seconds//Li???


			//from the start
			var _offline_part = APP_sequencer_for_audio_export(current_data,start,end,play_metronome)
			_offline_part.start(0)
			transport.start(0)
		}, time_seconds).then((buffer) => {
			// do something with the output buffer
			//console.log(buffer)
			let c = buffer.getChannelData()
			let myArrayBuffer = buffer["_buffer"]

			//play with tone
			// Tone.context.resume()
			// let player = new Tone.Player({
			// 	'url':buffer,
			// 	'autostart':true
			// }).toDestination()
			// player.start()
			///

			//this play with audio context
			// var audioCtx = new (window.AudioContext || window.webkitAudioContext)()
			// var source = audioCtx.createBufferSource()
			// source.buffer = myArrayBuffer
			// source.connect(audioCtx.destination)
			// source.start()

			// Process Audio
			//const mp3 = await this.playerService.createMP3(this.composerState.currentVersion.notes)

			//var offlineAudioCtx = new OfflineAudioContext({
			var offlineAudioCtx = new (window.OfflineAudioContext || window.webkitOfflineAudioContext)({
				numberOfChannels: myArrayBuffer.numberOfChannels,
				length: myArrayBuffer.length,
				sampleRate: myArrayBuffer.sampleRate,
			})

			// Audio Buffer Source
			var soundSource = offlineAudioCtx.createBufferSource()
			soundSource.buffer = myArrayBuffer

			soundSource.connect(offlineAudioCtx.destination)
			soundSource.start(0)

			offlineAudioCtx.startRendering().then(function(renderedBuffer) {
				var duration = renderedBuffer.duration
				var rate = renderedBuffer.sampleRate
				var offset = 0

				const a = document.createElement('a')

				// Generate audio file and assign URL
				if(type=="mp3"){
					var mp3_blob = _bufferToMp3(renderedBuffer, offlineAudioCtx.length)
					//var mp3_blob = audioBufferToWav(renderedBuffer)
					var new_file = URL.createObjectURL(mp3_blob)
					a.href= new_file
					a.download = "recording.mp3"
				}else{
					var wave_blob = _bufferToWave(renderedBuffer, offlineAudioCtx.length)
					var new_file = URL.createObjectURL(wave_blob)
					//var new_file = URL.createObjectURL(_bufferToWave(renderedBuffer, offlineAudioCtx.length))
					a.href= new_file
					a.download = "recording.wav"
				}

				a.click()
				DATA_hide_loader()
				URL.revokeObjectURL(a.href)
			}).catch(function(err) {
				// Handle error
				APP_warning_popup("Error export audio file")
			})

			// Convert AudioBuffer to a Blob using WAVE representation
			function _bufferToWave(abuffer, len) {
				var numOfChan = abuffer.numberOfChannels,
				length = len * numOfChan * 2 + 44,
				buffer = new ArrayBuffer(length),
				view = new DataView(buffer),
				channels = [], i, sample,
				offset = 0,
				pos = 0;

				// write WAVE header
				setUint32(0x46464952)
				setUint32(length - 8)
				setUint32(0x45564157)
				setUint32(0x20746d66)
				setUint32(16)
				setUint16(1)
				setUint16(numOfChan)
				setUint32(abuffer.sampleRate)
				setUint32(abuffer.sampleRate * 2 * numOfChan)
				setUint16(numOfChan * 2)
				setUint16(16)
				setUint32(0x61746164)
				setUint32(length - pos - 4)

				// write interleaved data
				for(i = 0; i < abuffer.numberOfChannels; i++)
					channels.push(abuffer.getChannelData(i))

				while(pos < length) {
					for(i = 0; i < numOfChan; i++) {
						sample = Math.max(-1, Math.min(1, channels[i][offset]))
						sample = (0.5 + sample < 0 ? sample * 32768 : sample * 32767)|0
						view.setInt16(pos, sample, true)
						pos += 2
					}
					offset++
				}

				// create Blob
				return new Blob([buffer], {type: "audio/wav"})

				function setUint16(data) {
					view.setUint16(pos, data, true)
					pos += 2
				}

				function setUint32(data) {
					view.setUint32(pos, data, true)
					pos += 4
				}
			}

			function _bufferToMp3(abuffer, len) {
				var numOfChan = abuffer.numberOfChannels,
				length = len * numOfChan * 2 + 44,
				buffer = new ArrayBuffer(length),
				view = new DataView(buffer),
				channels = [], i, sample,
				offset = 0,
				pos = 0;

				// write WAVE header
				setUint32(0x46464952)
				setUint32(length - 8)
				setUint32(0x45564157)
				setUint32(0x20746d66)
				setUint32(16)
				setUint16(1)
				setUint16(numOfChan)
				setUint32(abuffer.sampleRate)
				setUint32(abuffer.sampleRate * 2 * numOfChan)
				setUint16(numOfChan * 2)
				setUint16(16)
				setUint32(0x61746164)
				setUint32(length - pos - 4)

				// write interleaved data
				for(i = 0; i < abuffer.numberOfChannels; i++)
					channels.push(abuffer.getChannelData(i))

				while(pos < length) {
					for(i = 0; i < numOfChan; i++) {
						sample = Math.max(-1, Math.min(1, channels[i][offset]))
						sample = (0.5 + sample < 0 ? sample * 32768 : sample * 32767)|0
						view.setInt16(pos, sample, true)
						pos += 2
					}
					offset++
				}

				// create Blob
				//return new Blob([buffer], {type: "audio/wav"})
				var wav = lamejs.WavHeader.readHeader(new DataView(buffer))

				//stereo
				var samples = new Int16Array(buffer, wav.dataOffset, wav.dataLen / 2)
				let leftData_samples = [];
				let rightData_samples = [];
				for (let i = 0; i < samples.length; i += 2) {
					leftData_samples.push(samples[i]);
					rightData_samples.push(samples[i + 1]);
				}

				var left_samples = new Int16Array(leftData_samples)
				var right_samples = new Int16Array(rightData_samples)
				var mp3_blob = _wavToMp3_stereo(wav.channels, wav.sampleRate, left_samples,right_samples)
				//var mp3_blob_mono = _wavToMp3_mono(wav.channels, wav.sampleRate, samples)
				return mp3_blob

				function _wavToMp3_mono(channels, sampleRate, samples) {
					var buffer = []
					var mp3enc = new lamejs.Mp3Encoder(channels, sampleRate, 128)
					var remaining = samples.length
					var samplesPerFrame = 1152
					for (var i = 0; remaining >= samplesPerFrame; i += samplesPerFrame) {
						var mono = samples.subarray(i, i + samplesPerFrame)
						var mp3buf = mp3enc.encodeBuffer(mono)//mono.buffer???
						if (mp3buf.length > 0) {
							buffer.push(new Int8Array(mp3buf))
						}
						remaining -= samplesPerFrame
					}
					var d = mp3enc.flush()
					if(d.length > 0){
						buffer.push(new Int8Array(d))
					}
					var mp3Blob = new Blob(buffer, {type: 'audio/mp3'})
					var bUrl = window.URL.createObjectURL(mp3Blob);
					 // send the download link to the console
					console.log('mp3 download:', bUrl);
					return mp3Blob
				}

				function _wavToMp3_stereo(channels, sampleRate, left, right) {
					var buffer = [];
					var mp3enc = new lamejs.Mp3Encoder(channels, sampleRate, 128);
					var remaining = left.length;
					var samplesPerFrame = 1152;
					for (var i = 0; remaining >= samplesPerFrame; i += samplesPerFrame) {
						var leftChunk = left.subarray(i, i + samplesPerFrame);
						var rightChunk = right.subarray(i, i + samplesPerFrame);
						var mp3buf = mp3enc.encodeBuffer(leftChunk,rightChunk);
						if (mp3buf.length > 0) {
							buffer.push(mp3buf);//new Int8Array(mp3buf));
						}
						remaining -= samplesPerFrame;
					}
					var d = mp3enc.flush();
					if(d.length > 0){
						buffer.push(new Int8Array(d));
					}

					var mp3Blob = new Blob(buffer, {type: 'audio/mp3'});
					//var bUrl = window.URL.createObjectURL(mp3Blob);

					// send the download link to the console
					//console.log('mp3 download:', bUrl);
					return mp3Blob;
				}

				function setUint16(data) {
					view.setUint16(pos, data, true)
					pos += 2
				}

				function setUint32(data) {
					view.setUint32(pos, data, true)
					pos += 4
				}
			}

			TNAV_hide_main_menu()
		})  //end tone offline
	}, 100)
}
*/
function DATA_export_audio(){
	//control if all instruments loaded
	var type=SNAV_read_export_audio_file_format()

	if(!APP_verify_sound_ready()){
		return
	}

	DATA_show_loader()
	APP_stop()

	//calculate time registration
	var [now,start,end,max]=APP_return_progress_bar_values()
	var current_data = DATA_get_current_state_point(true)
	var count_in = current_data.global_variables.count_in

	var play_metronome=false
	if(outputMainMetronome.value === "true"){
		play_metronome=true
	}else{
		//if metonome not active, make no sense rendering more than Li (compas or
		if(end>Li)end=Li
	}

	//var time_ms = (Number(end) - Number(start) - count_in) * 1000 * 60 / PPM + 50
	//var time_ms = (Number(end) - Number(now) + count_in) * 1000 * 60 / PPM +  0.3 * 1000 //buffer
	var time_seconds = (Number(end) - Number(start) + count_in) * 60 / current_data.global_variables.PPM // no buffer

	setTimeout(async () => {
		Tone.Offline(({ transport }) => {
			//not using bar to define transport position

			//var start_seconds = start*(60/current_data.global_variables.PPM)
			var start_seconds = DATA_calculate_exact_time((start),0,0,1,1,1,1,current_data.global_variables.PPM)
			// var end_seconds = end*(60/current_data.global_variables.PPM)

			//chrome and safari need action to start context
			if(Tone.context.state!=='running'){
				Tone.context.resume()
				Tone.start()
			}

			transport.bpm.value = current_data.global_variables.PPM
			transport.loop=false
			transport.seconds = start_seconds
			//transport.timeSignature = end_seconds//Li???


			//from the start
			var _offline_part = APP_sequencer_for_audio_export(current_data,start,end,play_metronome)
			_offline_part.start(0)
			transport.start(0)
		}, time_seconds).then((buffer) => {
			// do something with the output buffer
			//console.log(buffer)
			let c = buffer.getChannelData()
			let myArrayBuffer = buffer["_buffer"]

			/*/play with tone
			Tone.context.resume()
			let player = new Tone.Player({
				'url':buffer,
				'autostart':true
			}).toDestination()
			player.start()
			//*/
			//this play with audio context
			// var audioCtx = new (window.AudioContext || window.webkitAudioContext)()
			// var source = audioCtx.createBufferSource()
			// source.buffer = myArrayBuffer
			// source.connect(audioCtx.destination)
			// source.start()
			TNAV_hide_main_menu()


			// Process Audio
			//var offlineAudioCtx = new OfflineAudioContext({
			var offlineAudioCtx = new (window.OfflineAudioContext || window.webkitOfflineAudioContext)({
				numberOfChannels: myArrayBuffer.numberOfChannels,
				length: myArrayBuffer.length,
				sampleRate: myArrayBuffer.sampleRate,
			})

			// Audio Buffer Source
			var soundSource = offlineAudioCtx.createBufferSource()
			soundSource.buffer = myArrayBuffer

			soundSource.connect(offlineAudioCtx.destination)
			soundSource.start(0)

			const worker = new Worker("WORKER_Export_Audio.js")
			const a = document.createElement('a')

			offlineAudioCtx.startRendering().then(function(renderedBuffer) {
				//var duration = renderedBuffer.duration
				var sampleRate = renderedBuffer.sampleRate
				var numOfChan = renderedBuffer.numberOfChannels
				//var offset = 0
				var channels = []

				for(let i = 0; i < numOfChan; i++)channels.push(renderedBuffer.getChannelData(i))

				// Generate audio file and assign URL with a webworker
				// worker.postMessage({
				// 	command: 'test',
				// })

				worker.postMessage({
					command: 'exportAudio',
					data: {
						type:type,
						//buffer: renderedBuffer,//cannot pass renderedBuffer
						sampleRate:sampleRate,
						numOfChan:numOfChan,
						channels:channels,
						len: offlineAudioCtx.length,
					}
				})
			}).catch(function(err) {
				// Handle error
				APP_warning_popup("Error export audio file")
			})

			worker.onmessage = (event) => {
				if(type=="mp3"){
					var mp3_blob = event.data
					var new_file = URL.createObjectURL(mp3_blob)
					a.href= new_file
					a.download = "recording.mp3"
				}else{
					var wave_blob = event.data
					var new_file = URL.createObjectURL(wave_blob)
					a.href= new_file
					a.download = "recording.wav"
				}
				a.click()
				URL.revokeObjectURL(a.href)
				worker.terminate()
				DATA_hide_loader()
			}

		})  //end tone offline
	}, 100)
}

function DATA_read_app_containers_button_states(){
	//get buttons states
	var PMC_button = tabPMCbutton.checked
	var RE_button = tabREbutton.checked
	var show_minimap = (document.querySelector("#App_show_minimap").value == 1)
	var ES_button = document.getElementById("App_BNav_tab_ES").checked
	var ET_button = document.getElementById("App_BNav_tab_ET").checked
	var Calc_button = document.getElementById("App_BNav_tab_calc").checked
	var Key_button = document.getElementById("App_BNav_tab_keyboard").checked
	var tab_open = document.querySelector('.App_BNav_tab_button').classList.contains('App_BNav_tab_button_rotated')

	return {
		RE:RE_button,
		PMC:PMC_button,
		MMap: show_minimap,
		ES_tab: ES_button,
		ET_tab: ET_button,
		Calc_tab: Calc_button,
		Key_tab: Key_button,
		tabs_opened: tab_open
		}
}

function DATA_set_app_containers_button_states(app_containers_button_states){
	if(app_containers_button_states==null)return
	var ES_button = document.getElementById("App_BNav_tab_ES")
	var ET_button = document.getElementById("App_BNav_tab_ET")
	var calc_button = document.getElementById("App_BNav_tab_calc")
	var Key_button = document.getElementById("App_BNav_tab_keyboard")
	var tab_open_button = document.querySelector('.App_BNav_tab_button')

	APP_set_show_hide_minimap(app_containers_button_states.MMap)

	tabPMCbutton.checked = app_containers_button_states.PMC
	tabREbutton.checked = app_containers_button_states.RE
	BNAV_show_RE_PMC()
	ES_button.checked = app_containers_button_states.ES_tab
	ET_button.checked = app_containers_button_states.ET_tab
	calc_button.checked = app_containers_button_states.Calc_tab
	Key_button.checked = app_containers_button_states.Key_tab
	if(app_containers_button_states.tabs_opened!=tab_open_button.classList.contains('App_BNav_tab_button_rotated')){
		tab_open_button.click()
	}

	if(app_containers_button_states.tabs_opened){
		//verify if correct tab is shown
		BNAV_open_bottom_nav_content()
	}
}

function DATA_read_RE_global_variables(){
	var line_button_list = RE_list_all_option_lines_buttons()
	var RE_global_variables = {}

	Object.keys(line_button_list).forEach(id=>{
		var button = line_button_list[id]
		if(button.classList.contains("pressed")){
			return RE_global_variables[id]=true
		}else{
			return RE_global_variables[id]=false
		}
	})
	RE_global_variables.scroll_x=0 //XXX
	RE_global_variables.scroll_y=0 // XXX

	return RE_global_variables
}

function DATA_set_RE_global_variables(RE){
	//visualization options
	var line_button_list = RE_list_all_option_lines_buttons()
	Object.keys(line_button_list).forEach(id=>{
		var button = line_button_list[id]
		var state = RE[id]
		//state=true if is pressed , false if not
		//there is the class and i dont want it
		if(button.classList.contains("pressed") && !state){
			//remove class
			button.classList.remove("pressed")
		}
		//there is not the class and i want it
		if(!button.classList.contains("pressed") && state){
			//add class
			button.classList.add("pressed")
		}
	})
}

function DATA_read_PMC_global_variables(){
	var container = document.querySelector(".App_PMC")
	var scrollbar_V = container.querySelector('.App_PMC_main_scrollbar_V')

	var y_val = Number(document.querySelector("#App_PMC_select_zoom_sound").value)

	return {
		zoom_x:PMC_zoom_x,
		//zoom_y:PMC_zoom_y,  //0 is all
		zoom_y:y_val,
		scroll_x:PMC_main_scrollbar_H.scrollLeft,
		scroll_y:scrollbar_V.scrollTop,
		vsb_RE:PMC_read_vsb_RE(),
		vsb_N:PMC_read_vsb_N(),
		vsb_Nabc:PMC_read_vsb_Nabc(),
		vsb_iN:PMC_read_vsb_iN(),
		vsb_T:PMC_read_vsb_T(),
		vsb_iT:PMC_read_vsb_iT(),
		}
}

function DATA_set_PMC_global_variables(PMC){
	//zoom(s)
	PMC_set_zoom_sound(PMC.zoom_y)
	PMC_set_zoom_time(PMC.zoom_x)
	//scrollbar position //XXX HERE??? maybe later (when the alignment has drawn everything
	PMC_checkScroll_V(PMC.scroll_y,true)
	PMC_checkScroll_H(PMC.scroll_x,true)
	/*
	//RE_preview  XXX
	data.global_variables.PMC.RE_prevew
	*/
	//elements shown (Na iS O iT in PMC and type) {"show":true, "type":"iNa"}

	PMC_set_vsb_N(PMC.vsb_N)
	PMC_set_vsb_Nabc(PMC.vsb_Nabc)
	PMC_set_vsb_iN(PMC.vsb_iN)
	PMC_set_vsb_T(PMC.vsb_T)
	PMC_set_vsb_iT(PMC.vsb_iT)
	PMC_set_vsb_RE(PMC.vsb_RE)
}

