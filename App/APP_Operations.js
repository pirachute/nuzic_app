function APP_show_operation_dialog_box(options){
	var background = document.getElementById("App_OP_dialog_box_background")
	var option_box_list = [...background.querySelectorAll(".App_OP_option_box")]
	var option_box_string=""
	//PS attention to difference BW voice_number(SHOWN) and voice_index(DATABASE)

	switch (options.working_space) {
		case "RE":

		break;
		case "PMC":

		break;
	}

	switch (options.target) {
		case "voice":
			option_box_string=`[v${options.voice_number}]`
		break;
		case "segment":
			option_box_string=`[v${options.voice_number}s${options.segment_index}]`
		break;
		case "column":
			option_box_string=`[c${options.column}]`
		break;
	}
	option_box_list.forEach(box=>{
		box.innerHTML=option_box_string
	})

	var div_repetitions = background.querySelector(".App_OP_dialog_repetitions")
	var div_operations = background.querySelector(".App_OP_dialog_operations")
	var div_work_in_progress = background.querySelector(".App_OP_work_in_progress")//XXX
	switch (options.type) {
		case "operations":
			div_repetitions.style="display:none"
			div_operations.style="display:flex"
		break;
		case "repetitions":
			div_repetitions.style="display:flex"
			div_operations.style="display:none"

			//hide work in progress XXX
			div_work_in_progress.style="display:none"
		break;
	}
	background.style.display = 'flex'

	data_info = JSON.stringify(options)
	background.setAttribute("data_info",data_info)
	APP_change_operation()
	//kinda duplicate
	APP_verify_apply_operation()
}

function APP_hide_operation_dialog_box(){
	//hide div
	var background = document.getElementById('App_OP_dialog_box_background')
	background.style.display = 'none'
	//clear info operation in div XXXx

	//reset RE or PMC selection
	if (tabPMCbutton.checked){
		PMC_reset_selection()
	}else{
		RE_reset_selection()
	}
}

function APP_change_operation(){
	var dialog_box= document.querySelector(".App_OP_dialog_box")
	var div_operations= dialog_box.querySelector(".App_OP_dialog_operations")
	var container_type = dialog_box.querySelector(".App_OP_container_type")
	var container_operation = dialog_box.querySelector(".App_OP_container_operation")
	//var type_button_list = [...container_type.querySelectorAll("input:checked")]
	var type_button = container_type.querySelector("input:checked")

	//activate/deactivate buttons
	//make sure all buttons are activated
	var op_button_list= [...container_operation.querySelectorAll("input")]
	op_button_list.forEach(input=>{input.disabled=false})
	//make sure all options are hidden
	var op_option_container_list= [...div_operations.querySelectorAll(".App_OP_options")]
	op_option_container_list.forEach(container=>{container.style="display:none"})

	if(type_button.value=="iT"){
		//deactivate last operation
		var v_mirror_button = container_operation.querySelector("#App_OP_v_mirror")
		if(v_mirror_button.checked==true){
			var sum_button=container_operation.querySelector("#App_OP_sum")
			sum_button.checked=true
		}
		v_mirror_button.disabled=true
	}else if(type_button.value=="T"){
		//deactivate everything
		var op_button_list= [...container_operation.querySelectorAll("input")]
		op_button_list.forEach(input=>{input.disabled=true})
		//deactivate ok button
		var OP_apply = div_operations.querySelector(".App_OP_apply")
		OP_apply.disabled=true
		var OP_title = div_operations.querySelector(".App_OP_title")
		OP_title.innerHTML="NO OPERATION"
		return
	}

	//change current operation title
	var OP_title = div_operations.querySelector(".App_OP_title")
	var operation_button = container_operation.querySelector("input:checked")

	//maybe not needed XXX
	var background = document.getElementById("App_OP_dialog_box_background")//XXX
	var options = JSON.parse(background.getAttribute("data_info"))//XXX
	var div_work_in_progress = background.querySelector(".App_OP_work_in_progress")//XXX
	div_work_in_progress.style="display:none"
	// var options = JSON.parse(background.getAttribute("data_info"))

	switch (operation_button.value) {
		case "sum":
			OP_title.innerHTML="SUMA"
			var op_option_container= div_operations.querySelector(".App_OP_options_sum")
			op_option_container.style="display:block"
			var op_lines_list = [...op_option_container.querySelectorAll(".App_OP_option_line")]
			if(type_button.value=="iT"){
				_change_current_operation_options(op_lines_list,[false,true,true,true,true])
				div_work_in_progress.style="display:block"//XXX
				//add correct text controller
				var inp_box = op_lines_list[1].querySelector(".App_OP_inp_box")
				inp_box.setAttribute('onkeypress',"APP_onlyIntegers(event,this)")
			}
			if(type_button.value=="iN" || type_button.value=="iNgm"){
				_change_current_operation_options(op_lines_list,[true,true,false,false,false])
				//add correct text controller
				var inp_box = op_lines_list[1].querySelector(".App_OP_inp_box")
				inp_box.setAttribute('onkeypress',"APP_operations_int_mod(event,this)")
			}
			if(type_button.value=="N" || type_button.value=="Ngm"){
				_change_current_operation_options(op_lines_list,[false,true,false,false,false])
				//add correct text controller
				var inp_box = op_lines_list[1].querySelector(".App_OP_inp_box")
				inp_box.setAttribute('onkeypress',"APP_operations_int_mod(event,this)")
			}
		break;
		case "rot":
			OP_title.innerHTML="ROTACIÓN"
			var op_option_container= div_operations.querySelector(".App_OP_options_rot")
			op_option_container.style="display:block"
			var op_lines_list = [...op_option_container.querySelectorAll(".App_OP_option_line")]
			if(type_button.value=="iT"){
				_change_current_operation_options(op_lines_list,[true,true,false,options.target=="voice",true,true,true])
				div_work_in_progress.style="display:block"//XXX
			}
			if(type_button.value=="iN" || type_button.value=="iNgm"){
				_change_current_operation_options(op_lines_list,[true,false,true,options.target=="voice",false,false,false])
			}
			if(type_button.value=="N" || type_button.value=="Ngm"){
				_change_current_operation_options(op_lines_list,[true,false,true,options.target=="voice",false,false,false])
			}
		break;
		case "h_mirror":
			OP_title.innerHTML="REFLEJO HORIZONTAL"
			var op_option_container= div_operations.querySelector(".App_OP_options_h_mirror")
			op_option_container.style="display:block"
			var op_lines_list = [...op_option_container.querySelectorAll(".App_OP_option_line")]
			if(type_button.value=="iT"){
				_change_current_operation_options(op_lines_list,[true,true,false,options.target=="voice",true])
			}
			if(type_button.value=="iN" || type_button.value=="iNgm"){
				_change_current_operation_options(op_lines_list,[true,false,true,options.target=="voice",false])
			}
			if(type_button.value=="N" || type_button.value=="Ngm"){
				_change_current_operation_options(op_lines_list,[true,false,true,options.target=="voice",false])
			}
		break;
		case "v_mirror":
			OP_title.innerHTML="REFLEJO VERTICAL"
			var op_option_container= div_operations.querySelector(".App_OP_options_v_mirror")
			op_option_container.style="display:block"
			var op_lines_list = [...op_option_container.querySelectorAll(".App_OP_option_line")]
			if(type_button.value=="iN" || type_button.value=="iNgm"){_change_current_operation_options(op_lines_list,[false,false,true])}
			if(type_button.value=="N" || type_button.value=="Ngm"){_change_current_operation_options(op_lines_list,[true,true,true])}
		break;
	}

	//verify if ok button is good to go
	APP_verify_apply_operation()

	//change current operation options
	function _change_current_operation_options(op_line_list,active_array,menu){
		//array of true/false
		op_line_list.forEach((line,index)=>{
			if(active_array[index]==true){
				line.style="display:flex"
			}else{
				line.style="display:none"
			}
		})
	}
}

//OPERATIONS

function APP_verify_apply_operation(){
	//determine what operation is active
	var background = document.getElementById("App_OP_dialog_box_background")
	var options = JSON.parse(background.getAttribute("data_info"))
	var container_type = background.querySelector(".App_OP_container_type")
	var container_operation = background.querySelector(".App_OP_container_operation")
	var type_button = container_type.querySelector("input:checked")
	var operation_button = container_operation.querySelector("input:checked")
console.log(options)
	var apply_buttons = [...background.querySelectorAll(".App_OP_apply")]

	var success=false
	_disable()
	switch (options.type) {
		case "operations":
			//find inp_box
			var div_operations = background.querySelector(".App_OP_dialog_operations")
			switch (operation_button.value) {
				case "sum":
					var op_option_container= div_operations.querySelector(".App_OP_options_sum")
					var op_lines_list = [...op_option_container.querySelectorAll(".App_OP_option_line")]
					if(type_button.value=="iT"){
						//warning visibility
						if(op_lines_list[2].querySelector("#App_OP_block_Ls").checked || op_lines_list[3].querySelector("#App_OP_force_sum").checked){
							op_lines_list[4].style="display:flex"
						}else{
							op_lines_list[4].style="display:none"
						}
						var delta_value = op_lines_list[1].querySelector(".App_OP_inp_box").value
						if(parseInt(delta_value)!=0 && delta_value!="" && delta_value!="-")success=true
					}else{
						success=_check_note(op_lines_list[1].querySelector(".App_OP_inp_box"),false)
					}
					// if(type_button.value=="iN" || type_button.value=="iNgm"){}
					// if(type_button.value=="N" || type_button.value=="Ngm"){}
					//same for everyone
				break;
				case "rot":
					var op_option_container= div_operations.querySelector(".App_OP_options_rot")
					var op_lines_list = [...op_option_container.querySelectorAll(".App_OP_option_line")]
					if(type_button.value=="iT"){
						//warning visibility
						if(op_lines_list[4].querySelector("#App_OP_force_rot").checked){
							op_lines_list[5].style="display:flex"
						}else{
							op_lines_list[5].style="display:none"
						}
						//rotation voice global
						if(options.target=="voice" && !op_lines_list[3].querySelector("#App_OP_voice_rot").checked){
							//warning disconect
							op_lines_list[6].style="display:flex"
						}else{
							//warning disconect creation new elements
							if(op_lines_list[4].querySelector("#App_OP_force_rot").checked){
								op_lines_list[6].style="display:flex"
							}else{
								op_lines_list[6].style="display:none"
							}
						}
					}
					//same for everyone
					var delta_value = op_lines_list[0].querySelector(".App_OP_inp_box").value
					if(parseInt(delta_value)!=0 && delta_value!="" && delta_value!="-")success=true
				break;
				case "h_mirror":
					if(type_button.value=="iT"){
						var op_option_container= div_operations.querySelector(".App_OP_options_h_mirror")
						var op_lines_list = [...op_option_container.querySelectorAll(".App_OP_option_line")]
						//voice global
						if(options.target=="voice" && !op_lines_list[3].querySelector("#App_OP_voice_h_mirror").checked){
							//warning disconect
							op_lines_list[4].style="display:flex"
						}else{
							op_lines_list[4].style="display:none"
						}
					}
					//same for everyone
					success=true
				break;
				case "v_mirror":
					var op_option_container= div_operations.querySelector(".App_OP_options_v_mirror")
					var op_lines_list = [...op_option_container.querySelectorAll(".App_OP_option_line")]
					if(type_button.value=="N" || type_button.value=="Ngm"){
						//second line depend on first value
						var dropdown_value=op_lines_list[0].querySelector(".App_v_mirror_axis").value

						if(dropdown_value=="user_note"){
							op_lines_list[1].style="display:flex"
							op_lines_list[2].style="display:none"

							//check value
							success=_check_note(op_lines_list[1].querySelector(".App_OP_inp_box"),true)
						}else{
							op_lines_list[1].style="display:none"
							op_lines_list[2].style="display:flex"
							success=true
						}
					}
					if(type_button.value=="iN" || type_button.value=="iNgm")success=true
				break;
			}
		break;
		case "repetitions":
			//find inp_box
			var div_repetitions = background.querySelector(".App_OP_dialog_repetitions")
			var N_rep_inp_box = div_repetitions.querySelector(".App_OP_inp_box")
			var N_rep = parseInt(N_rep_inp_box.value)
			if(N_rep>0){
				success=true
			}

			var type_repetition = div_repetitions.querySelector("select").value
			var warning_div = div_repetitions.querySelector(".App_OP_option_line:last-child")
			if(type_repetition=="insert"){
				warning_div.style="display:none"
			}else{
				warning_div.style="display:flex"
			}
		break;
	}

	if(success)_enable()

	function _disable(){
		apply_buttons.forEach(button=>{button.disabled=true})
	}

	function _enable(){
		apply_buttons.forEach(button=>{button.disabled=false})
	}

	function _check_note(input,only_positive){
		//only_positive==false -> works for negative too
		var string_value=input.value
		if(string_value!=""){
			var Na_value=null
			var positive = 1
			if(string_value[0]=="-"){
				if(only_positive){
					return false
				}else{
					positive=-1
					string_value=string_value.slice(1)
					if(string_value=="")return false
				}
			}
			if(string_value.includes("r")){
				// Mod
				var split = string_value.split('r')
				var resto = Math.floor(split[0])
				//if no module r 3
				var reg=-10
				if(split[1]>=0 && split[1]!=""){
					reg=Math.floor(split[1])
					Na_value = resto + reg*TET
				}else{
					return false
				}
			}else{
				Na_value=parseInt(string_value)
			}
			Na_value=Na_value*positive
			if(!(isNaN(Na_value) || Na_value==null)){
				//pleonastico
				var max_note = TET*7 -1
				if(Na_value>0 && Na_value<max_note && only_positive){
					return true
				}
				if(Na_value>-max_note && Na_value<max_note && !only_positive){
					return true
				}
			}
		}
		return false
	}
}

function APP_apply_operation(){
	//determine what operation is active
	var background = document.getElementById("App_OP_dialog_box_background")
	var options = JSON.parse(background.getAttribute("data_info"))
	var container_type = background.querySelector(".App_OP_container_type")
	var container_operation = background.querySelector(".App_OP_container_operation")
	var type_button = container_type.querySelector("input:checked")
	var operation_button = container_operation.querySelector("input:checked")

	//do the operation
	switch (options.type) {
		case "operations":
			//find inp_box
			var div_operations = background.querySelector(".App_OP_dialog_operations")
			var voice_index=-1
			var segment_index =-1
			if(options.target=="segment"){
				voice_index=options.voice_index
				segment_index=options.segment_index
			}
			if(options.target=="column"){
				segment_index=options.column
			}
			if(options.target=="voice"){
				voice_index=options.voice_index
			}

			switch (operation_button.value) {
				case "sum":
					var op_option_container= div_operations.querySelector(".App_OP_options_sum")
					var op_lines_list = [...op_option_container.querySelectorAll(".App_OP_option_line")]
					if(type_button.value=="iT"){
						//XXX
						var value = parseInt(op_lines_list[1].querySelector(".App_OP_inp_box").value)
						//if(options.target=="segment")DATA_sum_iT_segment(voice_index,segment_index,value,block_Ls,force_sum)
						//if(options.target=="column")DATA_sum_iT_segment_column(segment_index,value,block_Ls,force_sum)
						//if(options.target=="voice")DATA_sum_iT_voice(voice_index,value,block_Ls,force_sum)
					}
					if(type_button.value=="iN"){
						var value=_value_note(op_lines_list[1].querySelector(".App_OP_inp_box"),false)
						if(isNaN(value) || value==null){
							console.error("Sum note not valid")
							return
						}
						var type_sum = op_lines_list[0].querySelector("select").value
						if(options.target=="segment")DATA_sum_iNa_segment(voice_index,segment_index,value,type_sum)
						if(options.target=="column")DATA_sum_iNa_segment_column(segment_index,value,type_sum)
						if(options.target=="voice")DATA_sum_iNa_voice(voice_index,value,type_sum)
					}
					if(type_button.value=="N"){
						var value=_value_note(op_lines_list[1].querySelector(".App_OP_inp_box"),false)
						if(isNaN(value) || value==null){
							console.error("Sum note not valid")
							return
						}
						if(options.target=="segment")DATA_sum_Na_segment(voice_index,segment_index,value)
						if(options.target=="column")DATA_sum_Na_segment_column(segment_index,value)
						if(options.target=="voice")DATA_sum_Na_voice(voice_index,value)
					}
					if(type_button.value=="iNgm"){//XXX
						//if(options.target=="segment")DATA_sum_iNgm_segment(voice_index,segment_index,value,option)
						//if(options.target=="column")DATA_sum_iNgm_segment_column(segment_index,value,option)
						//if(options.target=="voice")DATA_sum_iNgm_voice(voice_index,value,option)
					}
					if(type_button.value=="Ngm"){//XXX
						//if(options.target=="segment")DATA_sum_Ngm_segment(voice_index,segment_index,value)
						//if(options.target=="column")DATA_sum_Ngm_segment_column(segment_index,value)
						//if(options.target=="voice")DATA_sum_Ngm_voice(voice_index,value)
					}
				break;
				case "rot":
					var op_option_container= div_operations.querySelector(".App_OP_options_rot")
					var op_lines_list = [...op_option_container.querySelectorAll(".App_OP_option_line")]
					var value=parseInt(op_lines_list[0].querySelector(".App_OP_inp_box").value)
					var by_segment=op_lines_list[3].querySelector("#App_OP_voice_rot").checked
					var rot_se=op_lines_list[2].querySelector("#App_OP_rot_se").checked
					if(type_button.value=="iT"){//XXX
						var rot_N=op_lines_list[1].querySelector("#App_OP_rot_N").checked
						var force=op_lines_list[4].querySelector("#App_OP_force_rot").checked
						if(rot_N){
							// if(options.target=="segment")DATA_rotate_element_segment(voice_index,segment_index,value,force)
							// if(options.target=="column")DATA_rotate_element_segment_column(segment_index,value,force)
							// if(options.target=="voice")DATA_rotate_element_voice(voice_index,value,force,by_segment)//XXX global or by segment
						}else{
							// if(options.target=="segment")DATA_rotate_iT_segment(voice_index,segment_index,value,force)
							// if(options.target=="column")DATA_rotate_iT_segment_column(segment_index,value,force)
							// if(options.target=="voice")DATA_rotate_iT_voice(voice_index,value,force,by_segment)//XXX global or by segment
						}

					}
					if(type_button.value=="iN"){
						if(options.target=="segment")DATA_rotate_iNa_segment(voice_index,segment_index,value,rot_se)
						if(options.target=="column")DATA_rotate_iNa_segment_column(segment_index,value,rot_se)
						if(options.target=="voice")DATA_rotate_iNa_voice(voice_index,value,rot_se,by_segment)
					}
					if(type_button.value=="N"){
						if(options.target=="segment")DATA_rotate_Na_segment(voice_index,segment_index,value,rot_se)
						if(options.target=="column")DATA_rotate_Na_segment_column(segment_index,value,rot_se)
						if(options.target=="voice")DATA_rotate_Na_voice(voice_index,value,rot_se,by_segment)
					}
					if(type_button.value=="iNgm"){//XXX
						//if(options.target=="segment")DATA_rotate_iNgm_segment(voice_index,segment_index,value,rot_se)
						//if(options.target=="column")DATA_rotate_iNgm_segment_column(segment_index,value,rot_se)
						//if(options.target=="voice")DATA_rotate_iNgm_voice(voice_index,value,rot_se,by_segment)//XXX global or by segment
					}
					if(type_button.value=="Ngm"){//XXX
						//if(options.target=="segment")DATA_rotate_Ngm_segment(voice_index,segment_index,value,rot_se)
						//if(options.target=="column")DATA_rotate_Ngm_segment_column(segment_index,value,rot_se)
						//if(options.target=="voice")DATA_rotate_Ngm_voice(voice_index,value,rot_se,by_segment)//XXX global or by segment
					}
				break;
				case "h_mirror":
					var op_option_container= div_operations.querySelector(".App_OP_options_h_mirror")
					var op_lines_list = [...op_option_container.querySelectorAll(".App_OP_option_line")]
					var by_segment=op_lines_list[3].querySelector("#App_OP_voice_h_mirror").checked
					var mirror_se=op_lines_list[2].querySelector("#App_OP_h_mirror_se").checked
					if(type_button.value=="iT"){
						if(op_lines_list[1].querySelector("#App_OP_h_mirror_N").checked){
							if(options.target=="segment")DATA_h_mirror_element_segment(voice_index,segment_index)
							if(options.target=="column")DATA_h_mirror_element_segment_column(segment_index)
							if(options.target=="voice")DATA_h_mirror_element_voice(voice_index,by_segment)
						}else{
							if(options.target=="segment")DATA_h_mirror_iT_segment(voice_index,segment_index)
							if(options.target=="column")DATA_h_mirror_iT_segment_column(segment_index)
							if(options.target=="voice")DATA_h_mirror_iT_voice(voice_index,by_segment)
						}
					}
					if(type_button.value=="iN"){
						if(options.target=="segment")DATA_h_mirror_iNa_segment(voice_index,segment_index,mirror_se)
						if(options.target=="column")DATA_h_mirror_iNa_segment_column(segment_index,mirror_se)
						if(options.target=="voice")DATA_h_mirror_iNa_voice(voice_index,mirror_se,by_segment)
					}
					if(type_button.value=="N"){
						//deactivated Na+iT in N
						//if(options.target=="segment")DATA_h_mirror_element_segment(voice_index,segment_index)
						//if(options.target=="column")DATA_h_mirror_element_segment_column(segment_index)
						//if(options.target=="voice")DATA_h_mirror_element_voice(voice_index,by_segment)

						if(options.target=="segment")DATA_h_mirror_Na_segment(voice_index,segment_index,mirror_se)
						if(options.target=="column")DATA_h_mirror_Na_segment_column(segment_index,mirror_se)
						if(options.target=="voice")DATA_h_mirror_Na_voice(voice_index,mirror_se,by_segment)
					}
					if(type_button.value=="iNgm"){//XXX
						// if(options.target=="segment")DATA_h_mirror_iNgm_segment(voice_index,segment_index,mirror_se)
						// if(options.target=="column")DATA_h_mirror_iNgm_segment_column(segment_index,mirror_se)
						// if(options.target=="voice")DATA_h_mirror_iNgm_voice(voice_index,by_segment,mirror_se)
					}
					if(type_button.value=="Ngm"){//XXX
						// if(options.target=="segment")DATA_h_mirror_Ngm_segment(voice_index,segment_index,mirror_se)
						// if(options.target=="column")DATA_h_mirror_Ngm_segment_column(segment_index,mirror_se)
						// if(options.target=="voice")DATA_h_mirror_Ngm_voice(voice_index,by_segment,mirror_se)
					}
				break;
				case "v_mirror":
					var op_option_container= div_operations.querySelector(".App_OP_options_v_mirror")
					var op_lines_list = [...op_option_container.querySelectorAll(".App_OP_option_line")]
					//intervals has no options
					var max_note = TET*7 -1
					if(type_button.value=="iN"){
						if(options.target=="segment")DATA_v_mirror_iNa_segment(voice_index,segment_index)
						if(options.target=="column")DATA_v_mirror_iNa_segment_column(segment_index)
						if(options.target=="voice")DATA_v_mirror_iNa_voice(voice_index)
					}
					if(type_button.value=="N"){
						var option={axis_type:op_lines_list[0].querySelector(".App_v_mirror_axis").value}
						if(option.axis_type=="user_note"){
							var Na_value = _value_note(op_lines_list[1].querySelector(".App_OP_inp_box"),true)
							if(isNaN(Na_value) || Na_value==null){
								console.error("Axis note not valid")
								return
							}
							//extra control
							if(Na_value<=0 && Na_value>=max_note){
								console.error("Axis note not valid")
								return
							}
							option.value=Na_value
						}
						if(options.target=="segment")DATA_v_mirror_Na_segment(voice_index,segment_index,option)
						if(options.target=="column")DATA_v_mirror_Na_segment_column(segment_index,option)
						if(options.target=="voice")DATA_v_mirror_Na_voice(voice_index,option)
					}
					if(type_button.value=="iNgm"){//XXX
						//if(options.target=="segment")DATA_v_mirror_iNgm_segment(voice_index,segment_index)
						//if(options.target=="column")DATA_v_mirror_iNgm_segment_column(segment_index)
						//if(options.target=="voice")DATA_v_mirror_iNgm_voice(voice_index)
					}
					if(type_button.value=="Ngm"){//XXX
						var option={axis_type:op_lines_list[0].querySelector(".App_v_mirror_axis").value}
						if(option.axis_type=="user_note"){
							//XXX
							console.error("PENDING CHECK BASE NOTE IS OK XXX")
							option.value=parseInt(op_lines_list[1].querySelector(".App_OP_inp_box").value)
						}
						//if(options.target=="segment")DATA_v_mirror_Ngm_segment(voice_index,segment_index,option)
						//if(options.target=="column")DATA_v_mirror_Ngm_segment_column(segment_index,option)
						//if(options.target=="voice")DATA_v_mirror_Ngm_voice(voice_index,option)
					}
				break;
			}
		break;
		case "repetitions":
			//find inp_box
			var div_repetitions = background.querySelector(".App_OP_dialog_repetitions")
			var N_rep_inp_box = div_repetitions.querySelector(".App_OP_inp_box")
			var N_rep = parseInt(N_rep_inp_box.value)
			var type_repetition = div_repetitions.querySelector("select").value
			if(N_rep>0){
				var insert = false
				if(type_repetition=="insert")insert=true

				if(options.target=="voice") DATA_repeat_voice(options.voice_index,N_rep,insert)
				if(options.target=="segment") DATA_repeat_segment(options.voice_index,options.segment_index,N_rep,insert)
				if(options.target=="column") DATA_repeat_segment_column(options.column,N_rep,insert)
			}
		break;
	}

	//close operation box
	APP_hide_operation_dialog_box()

	function _value_note(input,only_positive){
		//only_positive==false -> works for negative too
		var string_value=input.value
		if(string_value!=""){
			var Na_value=null
			var positive = 1
			if(string_value[0]=="-"){
				if(only_positive){
					return null
				}else{
					positive=-1
					string_value=string_value.slice(1)
					if(string_value=="")return null
				}
			}
			if(string_value.includes("r")){
				// Mod
				var split = string_value.split('r')
				var resto = Math.floor(split[0])
				//if no module r 3
				var reg=-10
				if(split[1]>=0 && split[1]!=""){
					reg=Math.floor(split[1])
					Na_value = resto + reg*TET
				}else{
					return null
				}
			}else{
				Na_value=parseInt(string_value)
			}
			Na_value=Na_value*positive
			if(!(isNaN(Na_value) || Na_value==null)){
				//pleonastico
				var max_note = TET*7 -1
				if(Na_value>0 && Na_value<max_note && only_positive){
					return Na_value
				}
				if(Na_value>-max_note && Na_value<max_note && !only_positive){
					return Na_value
				}
			}
		}
		return null
	}
}

