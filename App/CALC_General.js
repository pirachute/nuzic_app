var calc_arr = [
    {
        content:['28','5','55','4','20'],
        name:'[00]',
        origin:'',
        compatibility:[],
        parent:[],
        children:[],
        partingNote:'',
    }
]

function CALC_add_line(){
    var parent = document.querySelector('.App_BNav_calc_lines_container')
    var number = parent.children.length
    var line = `
    <div class="App_BNav_calc_line_box" onmouseover="CALC_hover_show_delete_line(this)" onmouseleave="CALC_hover_hide_delete_line(this)">
            <div class="App_BNav_calc_line_box_L">
                <button class="App_BNav_calc_line_delete_button" onclick="CALC_delete_line(this)">x</button>
                <div class="App_BNav_calc_line_origin"><img src="./Icons/op_add.svg"></div>
            </div>
            <div class="App_BNav_calc_line_box_R">
                <div class="App_BNav_calc_line_type">N</div>
                <div class="App_BNav_calc_line_number">${CALC_new_voice_name()}</div>
            </div>
     </div>
    `
    var line_content_parent = document.querySelector('.App_BNav_calc_lines_content_container')
    var line_content = `
    <div class="App_BNav_calc_line_content" onclick='CALC_select_line(this)' >
    <input class="App_BNav_calc_line_input" value="29" maxlength="4" onkeypress='CALC_onlyIntegers(event)' onkeyup="CALC_enter_line_input(this,event)"  onfocusout="CALC_line_input_focus_out(this)">
    <button class="App_BNav_calc_line_add_input" onclick="CALC_add_line_input(this)" >+</button>
</div>
    `
    parent.insertAdjacentHTML('beforeend',line)
    line_content_parent.insertAdjacentHTML('beforeend',line_content)
    CALC_data_add_line(line_content_parent.children.length-1)
    var lineCounter = document.querySelector('.App_BNav_calc_box_menu_L_end').querySelector('p')
    lineCounter.innerHTML=`(${calc_arr.length})`
    var scroll = document.querySelector('.App_BNav_calc_box_content_R')
    scroll.scrollTop=20000000000000000000000
    
}

function CALC_delete_line(button){
    var parent = button.closest('.App_BNav_calc_lines_container')
    var line = button.closest('.App_BNav_calc_line_box')
    var line_content_parent = document.querySelector('.App_BNav_calc_lines_content_container')
    var index = [...parent.children].indexOf(line)    
    line.innerHTML=''
    line.remove()
    line_content_parent.children[index].innerHTML=''
    line_content_parent.children[index].remove()
    CALC_data_remove_line(index)
    var lineCounter = document.querySelector('.App_BNav_calc_box_menu_L_end').querySelector('p')
    lineCounter.innerHTML=`(${calc_arr.length})`

}

function CALC_add_line_input(bttn){
    var parent = bttn.parentElement   
    var newInput = `<input class="App_BNav_calc_line_input" value="29" onkeyup="CALC_enter_line_input(this,event)"  onfocusout="CALC_line_input_focus_out(this)">`
    var newBttn=` <button class="App_BNav_calc_line_add_input" onclick="CALC_add_line_input(this)" >+</button>`
    var newInput = document.createElement('input')
       newInput.classList.add('App_BNav_calc_line_input')
       newInput.setAttribute('onkeyup',"CALC_enter_line_input(this,event)")
       newInput.setAttribute('onfocusout',"CALC_line_input_focus_out(this)")
       newInput.setAttribute('onkeypress','CALC_onlyIntegers(event)')
       newInput.setAttribute('maxlength',"4")
       newInput.value=''
       parent.insertBefore(newInput,bttn) 
      newInput.focus()  
}

function CALC_line_input_focus_out(input){  
    var parent = input.parentNode      
    var parentIndex  = [...parent.parentNode.children].indexOf(parent)   
    if(input.value=='')input.remove()
   var tempArr = []
    var inputArr = [...parent.children]
    inputArr.forEach((el,index) => {
        if(index<parent.children.length-1){
            if(el.value!=''){                
            tempArr.push(el.value) 
            }
        }
    });
    calc_arr[parentIndex].content=tempArr
}

function CALC_enter_line_input(input,event){
    if (event.code === 'Enter') {
       var newInput = document.createElement('input')
       newInput.classList.add('App_BNav_calc_line_input')
       newInput.setAttribute('onkeyup',"CALC_enter_line_input(this,event)")
       newInput.setAttribute('onfocusout',"CALC_line_input_focus_out(this)")
       newInput.setAttribute('onkeypress','CALC_onlyIntegers(event)')
       newInput.setAttribute('maxlength',"4")
       newInput.value=''
       input.parentNode.insertBefore(newInput,input.nextSibling)
       newInput.focus()
    }
}

function CALC_onlyIntegers(event){  
    var ch = String.fromCharCode(event.which)
    var isEmpty = !!(event.target.value=="")
    if(isEmpty==true){
       if(!(/[0-9-]/.test(ch))){
       event.preventDefault()
        } 
    }else{
        if(!(/[0-9]/.test(ch))){
            event.preventDefault()  
    }
    
}
}
function CALC_onlyIntegers_operation(event){  
    var ch = String.fromCharCode(event.which)  
    if(ch==`[`){
        
    } else if(ch==`]`){

    } else if(!(/[0-9]/.test(ch))){
            event.preventDefault() 
        }
}
function CALC_new_voice_name(fromData){    
    
    if(calc_arr.length!=0){    
    var prevName = calc_arr[calc_arr.length-1].name
    console.log(prevName)
    
    prevName = prevName.slice(1,-1)  
        while(prevName.charAt(0)==='0'&&prevName.length>1){
            prevName =prevName.substring(1)
        }
    var indexName=parseInt(prevName)+1
    if(fromData==1)indexName-=1
    if(indexName<10){
        indexName=`[0${indexName}]`
    }else{
        indexName=`[${indexName}]`
    }
    return indexName
}else{
    console.log('??')
    return `[00]`
}
}
function CALC_data_add_line(index){
    
    var line = {
        content:[29],
        name:CALC_new_voice_name(),
        origin:'',
        compatibility:[],
        parent:[],
        children:[],
        partingNote:'',
    }
    calc_arr.push(line)
}

function CALC_data_remove_line(index){
    calc_arr.splice(index,1)
    counter = 0
    calc_arr.forEach(el=>{
        el.line=counter
        counter++
    })
}
function CALC_create_new_line_from_data(line_index){
    var parent = document.querySelector('.App_BNav_calc_lines_container')
   
    var line = `
    <div class="App_BNav_calc_line_box">
            <div class="App_BNav_calc_line_box_L" onmouseover="CALC_hover_show_delete_line(this)" onmouseleave="CALC_hover_hide_delete_line(this)">
                <button class="App_BNav_calc_line_delete_button" onclick="CALC_delete_line(this)">x</button>
                <div class="App_BNav_calc_line_origin"><img src=./Icons/${calc_arr[line_index-1].origin.operation}.svg></div>
            </div>
            <div class="App_BNav_calc_line_box_R">
                <div class="App_BNav_calc_line_type">N</div>
                <div class="App_BNav_calc_line_number">${CALC_new_voice_name(1)}</div>
            </div>
     </div>
    `
    var line_content_parent = document.querySelector('.App_BNav_calc_lines_content_container')
    var line_content = document.createElement('div')
    line_content.classList.add('App_BNav_calc_line_content')
    line_content.onclick= function(){CALC_select_line(line_content)}
    
     /*  
    
    <input class="App_BNav_calc_line_input" value="29" maxlength="2" onkeypress='CALC_onlyIntegers(event)' onkeyup="CALC_enter_line_input(this,event)"  onfocusout="CALC_line_input_focus_out(this)">
    <button class="App_BNav_calc_line_add_input" onclick="CALC_add_line_input(this)" >+</button>

     */
    parent.insertAdjacentHTML('beforeend',line)  
    
    calc_arr[line_index-1].origin.values.forEach(el=>{

       
        var elDiv = document.createElement('div')
        elDiv.classList.add('App_BNav_calc_line_origin_parent')
        elDiv.innerHTML=el
        //console.log([...parent.children][parent.length-1])
       parent.children[parent.children.length-1].querySelector('.App_BNav_calc_line_box_L').appendChild(elDiv)
        
    })

    line_content_parent.appendChild(line_content)  
 
    calc_arr[line_index-1].content.forEach((el,index)=>{
        var newInput = document.createElement('input')
        newInput.classList.add('App_BNav_calc_line_input')
        newInput.setAttribute('onkeyup',"CALC_enter_line_input(this,event)")
        newInput.setAttribute('onfocusout',"CALC_line_input_focus_out(this)")
        newInput.setAttribute('onkeypress','CALC_onlyIntegers(event)')
        newInput.setAttribute('maxlength',"4")
        newInput.value=el
       line_content.appendChild(newInput)        
    }) 
    var line_content_button = `<button class="App_BNav_calc_line_add_input" onclick="CALC_add_line_input(this)" >+</button>`
    line_content.insertAdjacentHTML('beforeend', line_content_button)
    var lineCounter = document.querySelector('.App_BNav_calc_box_menu_L_end').querySelector('p')
    lineCounter.innerHTML=`(${calc_arr.length})`
}

function CALC_two_lines_sum(index1,index2){
    var content1 = Array.from(calc_arr[index1].content)
    var content2 = Array.from(calc_arr[index2].content)


    if(content1.length<content2.length){
        for(i=0;content1.length<content2.length;i++){
            content1.push(0)
        }
    } 
    if(content2.length<content1.length){
        for(i=0;content2.length<content1.length;i++){
            content2.push(0)
        }
    }
    var content_sum = []
    content1.forEach((element,index)=>{
        content_sum.push(parseInt(element)+parseInt(content2[index]))
    })
    var new_line ={content:content_sum,name:CALC_new_voice_name(),origin:{operation:'op_suma',values:[calc_arr[index1].name,calc_arr[index2].name]},compatibility:[], parent:[calc_arr[index1].name,calc_arr[index2].name],children:[],partingNote:''}
    
    calc_arr.push(new_line)
    calc_arr[index1].children.push(calc_arr[calc_arr.length-1].name)
    calc_arr[index2].children.push(calc_arr[calc_arr.length-1].name)
    CALC_create_new_line_from_data(calc_arr.length)    
   
}

function CALC_two_lines_substraction(index1,index2){
    var content1 = Array.from(calc_arr[index1].content)
    var content2 = Array.from(calc_arr[index2].content)


    if(content1.length<content2.length){
        for(i=0;content1.length<content2.length;i++){
            content1.push(0)
        }
    } 
    if(content2.length<content1.length){
        for(i=0;content2.length<content1.length;i++){
            content2.push(0)
        }
    }
    var content_sum = []
    content1.forEach((element,index)=>{
        var result = parseInt(element)-parseInt(content2[index])
        if(result<0)result=0
        content_sum.push(result)
    })
    var new_line ={content:content_sum,name:CALC_new_voice_name(),origin:{operation:'op_resta',values:[calc_arr[index1].name,calc_arr[index2].name]},compatibility:[], parent:[calc_arr[index1].name,calc_arr[index2].name],children:[],partingNote:''}
    
    calc_arr.push(new_line)
    calc_arr[index1].children.push(calc_arr[calc_arr.length-1].name)
    calc_arr[index2].children.push(calc_arr[calc_arr.length-1].name)
    CALC_create_new_line_from_data(calc_arr.length)    
}

function CALC_lines_rotation(line_index,rotation){
    var content = Array.from(calc_arr[line_index].content)
    for(i=0;i<rotation;i++){
        var el = content[0]
        content.shift()
        content.push(el)
    }
    var new_line ={content:content,name:CALC_new_voice_name(),origin:{operation:'op_rot',values:[calc_arr[line_index].name,rotation]},compatibility:[], parent:[calc_arr[line_index].name],children:[],partingNote:''}
    calc_arr.push(new_line)  
    calc_arr[line_index].children.push(calc_arr[calc_arr.length-1].name)  
    CALC_create_new_line_from_data(calc_arr.length)  

}

function CALC_lines_horizontal_mirror(line_index){
    var content = Array.from(calc_arr[line_index].content)
    var new_content = []
    for(i=0;i<content.length;i++){
        new_content.unshift(content[i])
    }
    var new_line ={content:new_content,name:CALC_new_voice_name(),origin:{operation:'op_h_mirror',values:[calc_arr[line_index].name]},compatibility:[], parent:[calc_arr[line_index].name],children:[],partingNote:''}
    calc_arr.push(new_line)   
    calc_arr[line_index].children.push(calc_arr[calc_arr.length-1].name)   
    CALC_create_new_line_from_data(calc_arr.length)  


}

function CALC_lines_augment_content(line_index, number){   
    console.log(line_index)
    var content = Array.from(calc_arr[line_index].content)
    var new_content = []
   
    content.forEach(el=>{
        el=parseInt(el)+number        
        new_content.push(el)
    })
    
    var new_line ={content:new_content,name:CALC_new_voice_name(),origin:{operation:'op_suma',values:[calc_arr[line_index].name,number]},compatibility:[], parent:[calc_arr[line_index].name],children:[],partingNote:''}
    calc_arr.push(new_line) 
    calc_arr[line_index].children.push(calc_arr[calc_arr.length-1].name)     
    CALC_create_new_line_from_data(calc_arr.length)  
}
function CALC_lines_decrease_content(line_index, number){
    var content = Array.from(calc_arr[line_index].content)
    var new_content = []
    content.forEach(el=>{
        el=parseInt(el)-number        
        new_content.push(el)
    })
    var new_line ={content:new_content,name:CALC_new_voice_name(),origin:{operation:'op_resta',values:[calc_arr[line_index].name,number]},compatibility:[], parent:[calc_arr[line_index].name],children:[],partingNote:''}
    calc_arr.push(new_line)   
    calc_arr[line_index].children.push(calc_arr[calc_arr.length-1].name)   
    CALC_create_new_line_from_data(calc_arr.length)  
}
function CALC_lines_increase_absolute_content(line_index, number){
    var content = Array.from(calc_arr[line_index].content)
    var new_content = []
    content.forEach(el=>{
        if(Math.sign(el)==1){
            el=parseInt(el)+number        
            new_content.push(el)
        } else{
            el=parseInt(el)-number        
            new_content.push(el)
        }
        
    })
    var new_line ={content:new_content,name:CALC_new_voice_name(),origin:{operation:'op_suma',values:[calc_arr[line_index].name,number]},compatibility:[], parent:[calc_arr[line_index].name],children:[],partingNote:''}
    calc_arr.push(new_line)   
    calc_arr[line_index].children.push(calc_arr[calc_arr.length-1].name)   
    CALC_create_new_line_from_data(calc_arr.length)  
}
function CALC_lines_decrease_absolute_content(line_index, number){
    var content = Array.from(calc_arr[line_index].content)
    var new_content = []
    content.forEach(el=>{
        if(Math.sign(el)==1){
            el=parseInt(el)-number        
            new_content.push(el)
        } else{
            el=parseInt(el)+number        
            new_content.push(el)
        }
        
    })
    var new_line ={content:new_content,name:CALC_new_voice_name(),origin:{operation:'op_resta',values:[calc_arr[line_index].name,number]},compatibility:[], parent:[calc_arr[line_index].name],children:[],partingNote:''}
    calc_arr.push(new_line)    
    calc_arr[line_index].children.push(calc_arr[calc_arr.length-1].name)  
    CALC_create_new_line_from_data(calc_arr.length)  
}

function CALC_lines_vertical_mirror(line_index){
    var content = Array.from(calc_arr[line_index].content)
    var new_content = []
    content.forEach(el=>{
        el=parseInt(el)*-1     
        new_content.push(el)
    })
    var new_line ={content:new_content,name:CALC_new_voice_name(),origin:{operation:'op_v_mirror',values:[calc_arr[line_index].name]},compatibility:[], parent:[calc_arr[line_index].name],children:[],partingNote:''}
    calc_arr.push(new_line)   
    calc_arr[line_index].children.push(calc_arr[calc_arr.length-1].name)   
    CALC_create_new_line_from_data(calc_arr.length)  
}
function CALC_lines_vertical_notes_mirror(line_index, num){
    var content = Array.from(calc_arr[line_index].content)
    var new_content = []
    content.forEach(el=>{
        var temp = (parseInt(el)-num)*-1
           
        new_content.push(num+temp)
    })
    var new_line ={content:new_content,name:CALC_new_voice_name(),origin:{operation:'op_v_mirror',values:[calc_arr[line_index].name,num]},compatibility:[], parent:[calc_arr[line_index].name],children:[],partingNote:''}
    calc_arr.push(new_line)  
    calc_arr[line_index].children.push(calc_arr[calc_arr.length-1].name)    
    CALC_create_new_line_from_data(calc_arr.length)  
}
function CALC_select_line(line){    
    var line_selected =line.parentNode.querySelector('.selected')
    if(line_selected!=null){
        line_selected.classList.remove('selected')
    }
    line.classList.add('selected')
    var line_index = [...line.parentNode.children].indexOf(line)
    var lineBox = document.querySelector('.App_BNav_calc_box_menu_L_end').querySelector('div')
    lineBox.innerHTML=calc_arr[line_index].name
    if(calc_prev_inp!=null){
        var indexcontent = document.querySelector('.App_BNav_calc_lines_container').children[line_index].querySelector('.App_BNav_calc_line_number')
        
        calc_prev_inp.value=indexcontent.innerText
    }else{
        console.log('no')
    }
}

function CALC_close_operation_box(bttn){
    bttn.parentNode.parentNode.style.display = 'none'
}
function CALC_operation_box_test_ok(bttn){    
    var input1 = bttn.parentNode.parentNode.querySelector('.App_BNav_calc_operations_box_input1')
    var input2 = bttn.parentNode.parentNode.querySelector('.App_BNav_calc_operations_box_input2')
    
    var value1=input1.value

    var value2=input2.value
    value1=CALC_valid_line_test(input1)
    var select = bttn.parentNode.parentNode.querySelector('.App_BNav_calc_operations_box_options_select')
    if(select.value=='sum'){
        if(isNaN(value2)==false){
            CALC_lines_augment_content(value1, parseInt(value2))
        }else{
            value2 = CALC_valid_line_test(input2)
                       
            CALC_two_lines_sum(value1,value2)
            
        }
    }else if(select.value=='augment'){
        if(isNaN(value2)==false) {
            CALC_lines_increase_absolute_content(value1, parseInt(value2))
        }else{
            console.log('errorrrrrrrrrrrrrrrr')
        }
    }else if(select.value=='substraction'){
        if(isNaN(value2)==false){
            CALC_lines_decrease_content(value1, parseInt(value2))
        }else{
            value2 = CALC_valid_line_test(input2)            
            CALC_two_lines_substraction(value1,value2)            
        }
    }else if(select.value=='disminution'){
        if(isNaN(value2)==false) {
            CALC_lines_decrease_absolute_content(value1, parseInt(value2))
        }else{
            console.log('errorrrrrrrrrrrrrrrr')
        }
    }else if(select.value=='rotation'){
        if(isNaN(value2)==false) {
            CALC_lines_rotation(value1, parseInt(value2))
        }else{
            console.log('errorrrrrrrrrrrrrrrr')
        }
    }else if(select.value=='hmirror'){
        CALC_lines_horizontal_mirror(value1)
    }else if(select.value=='vmirror'){
        if(isNaN(value2)==false) {
            CALC_lines_vertical_notes_mirror(value1, parseInt(value2))
        }else{
            console.log('errorrrrrrrrrrrrrrrr')
        }
    }
    CALC_close_operation_box(bttn)
}

function CALC_valid_line_test(element){   
    var validEl = false
    if(element.value.charAt(0)!='['||element.value.charAt(element.value.length-1)!=']'){
        console.error('no []')
    }else{        
        var nameExist= calc_arr.filter(el=>el.name==element.value)
       
        if(nameExist[0]!=undefined){          
            var newIndex = calc_arr.findIndex(line=>{
                return line.name==nameExist[0].name
            })
            validEl=true
        }else{
            console.error('line no found')
        } 
        /* elValue=element.value.slice(1,-1)        
        while(elValue.charAt(0)==='0'&&elValue.length>1){
            elValue =elValue.substring(1)
        }
        var lines = document.querySelector('.App_BNav_calc_lines_content_container')
        if(elValue>=0&&elValue<lines.children.length){
            console.log(`its line ${elValue}`)
            validEl=true
        }else{
            console.error('line no found')
        } */
    }
    if(validEl==false){
        element.style.backgroundColor='red'
    } else{
        return newIndex
    }
}
    

function CALC_display_operation_box(bttn){
    var boxes = [...document.getElementsByClassName('App_BNav_calc_operations_box')]
    boxes.forEach(el=>{
        el.style.display='none'
    })
    var box = document.getElementById(bttn.value)
    
    box.style.display='flex'

}

function CALC_operation_input_focusout(inp){
    calc_prev_inp= inp
    setTimeout(function(){
        calc_prev_inp=null
    },200)
}

function CALC_check_line_validation(index){
    var line = calc_arr[0].content
    var tempArr = []
    var iSCounter=0
    var calc_Na= false
    var calc_iS= true
    var calc_P= true
    var calc_iT= false

    line.forEach(el=>{
       tempArr.push(parseInt(el))
       iSCounter+=parseInt(el)
       if(iSCounter<0||iSCounter>83){
        calc_iS=false      
     }
    })
    
   if(Math.min.apply(Math,tempArr)>=0&& Math.max.apply(Math,tempArr)<=83){
        calc_Na=true
   }
   if(tempArr[0]!=0){
    calc_P=false
   }else{    
    for(i=1;i<tempArr.length;i++){
        if(tempArr[i]<=tempArr[i-1]){
            calc_P=false
        }
    }    
   }
   if(tempArr.some(v => v <= 0) == false){
    calc_iT=true
   }   
   return [calc_Na,calc_iS,calc_P,calc_iT]
}

function CALC_div_scroll_y(thisScroll){
   var scrollL = document.querySelector('.App_BNav_calc_box_content_L')
   
    scrollL.scrollTop=thisScroll.scrollTop
   
}

function CALC_hover_show_delete_line(line){
   
    var bttn = line.querySelector('.App_BNav_calc_line_delete_button')
    
    bttn.style.color='var(--Nuzic_dark)'
}
function CALC_hover_hide_delete_line(line){
    var bttn = line.querySelector('.App_BNav_calc_line_delete_button')   
    bttn.style.color='transparent'
}
