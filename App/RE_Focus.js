//SEGMENT LINES
let tab_pressed=false
let shift_pressed=false
function RE_move_focus(evt,element){
	tab_pressed=false
	shift_pressed=false
	switch(evt.keyCode){
	case 37:
		// Key left.
		RE_focus_next_index(element,-1)
		evt.preventDefault()
		break
	case 38:
		// Key up.
		RE_focus_up(element)
		evt.preventDefault();
		break
	case 39:
		// Key right.
		RE_focus_next_index(element,1)
		evt.preventDefault();
		break
	case 40:
		// Key down.
		RE_focus_down(element)
		evt.preventDefault();
		break
	case 9:
		//tab
		evt.preventDefault()
		tab_pressed=true
		if(evt.shiftKey) {
			//shift was down when tab was pressed
			shift_pressed=true
			var [previous_element,,]=_find_prev_filled_element(element,".App_RE_inp_box")
			if(previous_element!=null){
				RE_focus_next_index(previous_element,0)
			}else{
				var point_at_element = element
				RE_focus_next_index(point_at_element,-1)
			}
		}else{
			//tab
			//find next filled element
			//if undefined, maybe is last one or maybe it has dot end segment XXX
			var [next_element,,]=RE_find_next_filled_element(element,".App_RE_inp_box")
			if(next_element!=null){
				RE_focus_next_index(next_element,0)
			}else{
				var [index,elements_list]=RE_element_index(element,".App_RE_inp_box")
				var last_segment_element = elements_list.pop()
				var point_at_element = last_segment_element
				if(last_segment_element==element){
					//jump next segment
					RE_focus_next_index(point_at_element,1)
				}else{
					if(element.classList.contains("RE_note_cell")){
						//if sound line jump next segment
						RE_focus_next_index(point_at_element,1)
					}else{
						//jump end line
						RE_focus_next_index(point_at_element,0)
					}
				}
			}
		}
		break
	case 13:
		//intro
		//if element == empty -> focus next index
		//else focus +1
		if(!element.value){
			var [next_element,,]=RE_find_next_filled_element(element,".App_RE_inp_box")
			if(next_element!=null){
				RE_focus_next_index(next_element,0)
			}else{
				var [index,elements_list]=RE_element_index(element,".App_RE_inp_box")
				var last_segment_element = elements_list.pop()
				var point_at_element = last_segment_element
				if(last_segment_element==element){
					RE_focus_next_index(point_at_element,1)
				}else{
					RE_focus_next_index(point_at_element,0)
				}
			}
		}else{
			RE_focus_next_index(element,1)
		}
		break
	}
	//preventDefault to avoid scrollbar to move automatically
	function _find_prev_filled_element(element, string="input"){
		var parent = element.parentNode
		var input_list = [...parent.querySelectorAll(string)]
		var elementIndex = input_list.indexOf(element)
		var index_previous = -1

		for (var i=elementIndex-1; i>=0;i--){
			var value = input_list[i].value
			if(value!="" && value!=end_range){
				//element not void
				index_previous=i
				i=-1
			}
		}

		if (index_previous>=0){
			var distance = elementIndex-index_previous
			return [input_list[index_previous], index_previous, distance]
		}else{
			return [null, -1,-1]
		}
	}
}

//jump focus to next element
function RE_focus_next_index(element,dindex=1){
	//in case element will be deleted...
	var input_element=element
	var [index_old,list_inp]=RE_element_index(element,".App_RE_inp_box")

	if(index_old+dindex<0){
		//out of boundary
		//jump previous segment
		var current_segment = element.closest(".App_RE_segment")
		var [index_current_segment,list_segment]=RE_element_index(current_segment,".App_RE_segment")
		var current_line_object = element.closest(".App_RE_segment_line")
		var [index_current_line,]= RE_element_index(current_line_object,".App_RE_segment_line")
		if(index_current_segment>0){
			var next_segment_obj =  list_segment[index_current_segment-1]

			if(next_segment_obj.querySelector(".App_RE_segment_loader").classList.contains("loading")){
				//move scrollbar in position
				document.querySelector('.App_RE_main_scrollbar_H').scrollLeft-=nuzic_block*2
				RE_checkScroll_H()
			}
			//better
			var next_line_object_list= [...next_segment_obj.querySelectorAll(".App_RE_segment_line")]

			if(next_line_object_list.length==0){
				console.error("Error: Position focus not found try later")
				// setTimeout(() => {
				// 	var next_line_object = next_line_object_list[index_current_line]
				// 	var next_focused_list = [...next_line_object.querySelectorAll(".App_RE_inp_box")]
				// 	var next_focused_element = next_focused_list[next_focused_list.length-1]
				// 	next_focused_element.focus()
				// 	next_focused_element.select()
				// }, 100)
				return
			}
			var next_line_object = next_line_object_list[index_current_line]
			var next_focused_list = [...next_line_object.querySelectorAll(".App_RE_inp_box")]
			var next_focused_element = next_focused_list[next_focused_list.length-1]
			next_focused_element.focus()
			next_focused_element.select()

		}else{
			//very first element
			document.querySelector('.App_RE_main_scrollbar_H').scrollLeft-=nuzic_block*2
		}
		return
	}

	if(index_old+dindex>=list_inp.length){
		//out of boundary
		//jump next segment
		var current_segment_obj = element.closest(".App_RE_segment")
		var [index_current_segment,list_segment]=RE_element_index(current_segment_obj,".App_RE_segment")
		var current_line_object = element.closest(".App_RE_segment_line")
		var [index_current_line,]= RE_element_index(current_line_object,".App_RE_segment_line")
		if(index_current_segment<list_segment.length-1){
			var next_segment_obj =  list_segment[index_current_segment+1]
			if(next_segment_obj.querySelector(".App_RE_segment_loader").classList.contains("loading")){
				//move scrollbar in position
				document.querySelector('.App_RE_main_scrollbar_H').scrollLeft+=nuzic_block*2
				RE_checkScroll_H()
			}
			//better BUT not perfect
			var next_line_object_list= [...next_segment_obj.querySelectorAll(".App_RE_segment_line")]
			if(next_line_object_list.length==0){
				console.error("Error: Position focus not found try later")
				// setTimeout(() => {
				// 	var next_line_object_list= [...next_segment_obj.querySelectorAll(".App_RE_segment_line")]
				// 	var next_line_object = next_line_object_list[index_current_line]
				// 	var next_focused_element = next_line_object.querySelector(".App_RE_inp_box:first-child")
				// 	next_focused_element.focus()
				// 	next_focused_element.select()
				// }, 50)
				return
			}
			var next_line_object = next_line_object_list[index_current_line]
			var next_focused_element = next_line_object.querySelector(".App_RE_inp_box:first-child")
			next_focused_element.focus()
			next_focused_element.select()
		}else{
			//very last element
			element.blur()
			element.focus()
			document.querySelector('.App_RE_main_scrollbar_H').scrollLeft+=nuzic_block*2
		}
		return
	}

	var parent= element.parentNode
	//element.blur()
	input_element.blur()
	//wait a moment (50 ms)
	//setTimeout(function () {
		var [index,list]=RE_element_index(element,".App_RE_inp_box")
		if(index==-1){
			index=index_old
			list = [...parent.querySelectorAll(".App_RE_inp_box")]
		}
		index+=dindex
		if(index<list.length) {
			list[index].focus()
			list[index].select()
		}
	//}, 50)
}

function RE_focus_down(element){
	var [elementIndex,elements_list]=RE_element_index(element,".App_RE_inp_box")
	var list_container = element.parentNode

	var [index_line,line_list]=RE_element_index(list_container,".App_RE_segment_line")
	//find wich is the next visible label
	if(index_line+1>=line_list.length){
		//maybe iA or other voice
		var success = RE_focus_iA_element_from_voice_element(element,false)
		if(!success){
			RE_focus_voice_to_voice(element,elementIndex,"down")
		}
	}else{
		var next_visible_line= line_list[index_line+1]
		if(next_visible_line.classList.contains("App_RE_Tf")){
			//calculate position_px
			//var global_zoom = Number(document.querySelector('.App_SNav_select_zoom').value)
			var RE_block = nuzic_block	//*global_zoom
			var position_px = RE_block*elementIndex

			var Fr_input_list = [...next_visible_line.querySelectorAll(".App_RE_inp_box")]
			var Fr_element_index
			Fr_input_list.find((item,index)=>{
				if(item.offsetLeft>position_px)return true
				Fr_element_index=index
				if(item.offsetLeft==position_px)return true
			})
			var Fr_element = Fr_input_list[Fr_element_index]
			Fr_element.focus()
			Fr_element.select()
		}else{
			RE_focus_on_element_index(elementIndex,next_visible_line)
		}
	}
}

function RE_focus_up(element){
	var [elementIndex,]=RE_element_index(element,".App_RE_inp_box")
	var list_container = element.parentNode
	var [index_line,line_list]=RE_element_index(list_container,".App_RE_segment_line")
	//find wich is the next visible label
	if(index_line==0){
		//maybe iA or other voice
		var success = RE_focus_iA_element_from_voice_element(element,true)
		if(!success){
			RE_focus_voice_to_voice(element,elementIndex,"up")
		}
	}else{
		var previous_visible_line= line_list[index_line-1]
		if(!list_container.classList.contains("App_RE_Tf")){
			RE_focus_on_element_index(elementIndex,previous_visible_line)
		}else{
			//going up from fractioning
			var indexPF1=Math.round(element.offsetLeft/nuzic_block)
			RE_focus_on_element_index(indexPF1,previous_visible_line)
		}
	}
}

function RE_focus_voice_to_voice(element,index,direction){
	var voice_list = [...document.querySelectorAll(".App_RE_voice")]
	var visible_voice_list = voice_list.filter((item)=>{return RE_voice_is_visible(item)})
	var current_voice = element.closest(".App_RE_voice")
	var current_segment = element.closest(".App_RE_segment")
	var current_voice_index = visible_voice_list.indexOf(current_voice)

	var target_voice = null
	if(direction=="up"){
		var target_voice=visible_voice_list[current_voice_index-1]
	}else if(direction=="down"){
		var target_voice=visible_voice_list[current_voice_index+1]
	}else{
		return
	}

	var current_voice_segment_list = [...current_voice.querySelectorAll(".App_RE_segment")]
	var current_segment_index = current_voice_segment_list.indexOf(current_segment)

	var current_segment_position = 0
	for (let i = 0; i < current_segment_index; i++) {
		//current_segment_position+= [...current_voice_segment_list[i].querySelector(".App_RE_segment_line").querySelectorAll(".App_RE_inp_box")].length+1
		current_segment_position+= Math.round(current_voice_segment_list[i].clientWidth/nuzic_block)+1
	}

	//find index column
	var index_total= index + current_segment_position

	//find first line next voice
	var target_voice_segment_list = [...target_voice.querySelectorAll(".App_RE_segment")]
	var target_segment_end = 0
	var target_segment_columns = 0
	var target_segment = target_voice_segment_list.find(segment=>{
		target_segment_columns = [...segment.querySelector(".App_RE_segment_line").querySelectorAll(".App_RE_inp_box")].length
		target_segment_end+=target_segment_columns+1
		return target_segment_end-1>index_total
	})

	var new_index = target_segment_columns - (target_segment_end - 1 - index_total)
	if(new_index<0)new_index=0

	//focus in last visible line
	var line_list = [...target_segment.querySelectorAll(".App_RE_segment_line")]
		var visible_line_list = line_list.filter(line=>{
		return line.style.display != "none"
	})
	var target_visible_line=null
	if(direction=="up"){
		target_visible_line = visible_line_list.pop()
		if(target_visible_line.classList.contains("App_RE_Tf")){
			target_visible_line = visible_line_list.pop()
		}
	}else if(direction=="down"){
		target_visible_line = visible_line_list.shift()
	}
	var next_list = [...target_visible_line.querySelectorAll(".App_RE_inp_box")]
	next_list[new_index].focus()
	next_list[new_index].select()
}

function RE_focus_iA_element_from_voice_element(element,up){
	//return true if no more action is necessary
	//return false if necessary to jump to a voice

	//used by move focus
	//finding voices
	var voice_matrix = element.closest(".App_RE_voice_matrix")
	var current_voice = element.closest(".App_RE_voice")
	var voice_list = [...document.querySelectorAll(".App_RE_voice")]
	var visible_voice_list = voice_list.filter((item)=>{return RE_voice_is_visible(item)})
	var voice_index = visible_voice_list.indexOf(current_voice)

	//find index
	var visible_iA_list = [...voice_matrix.querySelectorAll(".App_RE_iA:not(.hidden)")]
	//var iA_index = visible_iA_list.indexOf(iA_list)

	var iA_index = null

	//find voice index and voice index+1
	//var voice_up = visible_voice_list[iA_index]
	//var voice_down = visible_voice_list[iA_index+1]
	if(up){
		iA_index=voice_index-1
	}else{
		iA_index=voice_index
	}

	//no more voices
	if(iA_index<0 || iA_index>=visible_iA_list.length)return true
	//need to jump directly to a voice
	if(visible_iA_list[iA_index].style.display=="none")return false
	//need to jump to a iA (visible)
	var iA_element_list = [...visible_iA_list[iA_index].querySelectorAll(".App_RE_inp_box")]

	//determining index iA
	var current_line = element.closest(".App_RE_segment_line")
	var iA_index = Math.round(element.offsetLeft/nuzic_block)

	var segment_list= [...current_voice.querySelectorAll(".App_RE_segment")]
	var current_segment = element.closest(".App_RE_segment")
	var segment_index = segment_list.indexOf(current_segment)

	for (var i =0 ; i<segment_index; i++){
		//iA_index+=[...segment_list[i].querySelector(".App_RE_segment_line").querySelectorAll(".App_RE_inp_box")].length+1
		iA_index+=Math.round(segment_list[i].clientWidth/nuzic_block)+1
		//offsetWidth clientWidth
	}
	var iA_element = iA_element_list[iA_index]
	if(iA_element.disabled == true){
		for (var i = iA_index; i>=0; i--){
			if(iA_element_list[i].disabled != true){
				iA_element=iA_element_list[i]
				i=-1
			}
		}
	}

	if(iA_element!=null){
		iA_element.focus()
		iA_element.select()
		return true
	}else{
		return false
	}
}

function RE_focus_voice_element_from_iA_element(element,up){
	//used by move focus
	var iA_elementIndex = Math.round(element.offsetLeft/nuzic_block) //ATT! start at position 8 for some reason !!!
	var iA_line = element.closest(".App_RE_iA")
	var voice_obj=RE_find_voice_obj_from_iA_line(iA_line,up)
	var segment_list= [...voice_obj.querySelectorAll(".App_RE_segment")]

	var segment_index = 0
	var current_segment=segment_list.find(segment=>{
		var seg_end_position=+Math.round((segment.clientWidth+segment.offsetLeft)/nuzic_block)
		return seg_end_position>iA_elementIndex
	})

	var seg_start_position = Math.round(current_segment.offsetLeft/nuzic_block)
	var target_elementIndex = iA_elementIndex-seg_start_position
	var line_list = [...current_segment.querySelectorAll(".App_RE_segment_line")]

	if(up){
		//find the lower line element ( fr excluded )
		var bottom_visible_line = line_list.pop()
		if(bottom_visible_line.classList.contains("App_RE_Tf")){
			bottom_visible_line = line_list.pop()
		}
		var next_list = [...bottom_visible_line.querySelectorAll(".App_RE_inp_box")]
		var new_element = next_list[target_elementIndex]
		new_element.focus()
		new_element.select()
	}else{
		//find the higher line element
		var upper_visible_line = line_list[0]
		var next_list = [...upper_visible_line.querySelectorAll(".App_RE_inp_box")]
		var new_element = next_list[target_elementIndex]
		new_element.focus()
		new_element.select()
	}
}


function RE_focus_on_element_index(index,next_list_container){
	var next_list = [...next_list_container.querySelectorAll(".App_RE_inp_box")]
	next_list[index].focus()
	next_list[index].select()
}

//ATT!!! POSITION IS NOT ABSOLUTE
function RE_focus_on_object_index(voice_index,segment_index,type,position,next=0){
	//var voice_obj = RE_find_voice_obj_by_index(current_position_PMCRE.voice_data.voice_index)
	var voice_obj = RE_find_voice_obj_by_index(voice_index)
	var segment_obj_list = [...voice_obj.querySelectorAll(".App_RE_segment")]
	//var segment_obj = segment_obj_list[current_position_PMCRE.segment_index]
	if(segment_index>segment_obj_list.length-1){
		//focus last element
		RE_focus_on_object_index(voice_index,segment_obj_list.length-1,type,-1,0)
		return
	}
	var segment_obj = segment_obj_list[segment_index]

	if(segment_obj.querySelector(".App_RE_segment_loader").classList.contains("loading")){
		//move scrollbar in position
		//console.log(segment_obj.offsetLeft)
		document.querySelector('.App_RE_main_scrollbar_H').scrollLeft=segment_obj.offsetLeft
		RE_checkScroll_H()
	}
	var lineClass = ".App_RE_"+type
	var inp_line = segment_obj.querySelector(lineClass)
	var inp_list = [...inp_line.querySelectorAll(".App_RE_inp_box")]
	var index_focus = null
	if(position==-1){
		index_focus=inp_list.length-1
	}else{
		var index_no_empty = -1
		inp_list.find((input,index)=>{
			if(input.value!="")index_no_empty++
			if(index_no_empty==position){
			//if(index_no_empty==current_position_PMCRE.index_object){
				index_focus=index
				return true
			}
			return false
		})

		if(index_focus==null){
			console.error("Error: Position focus not found")
			return
		}
		index_focus+=next
	}
	if(index_focus>inp_list.length-1){
		//try next segment
		RE_focus_on_object_index(voice_index,segment_index+1,type,0,0)
		return
	}

	//change index_focus depending on tab or shift+tab
	if(tab_pressed){
		if(shift_pressed){
			var [,prev_filled_element_index,]=RE_find_prev_filled_element(inp_list[index_focus-1],".App_RE_inp_box")
			index_focus=prev_filled_element_index
			if(index_focus<0)index_focus=0
		}else{
			var [,next_filled_element_index,]=RE_find_next_filled_element(inp_list[index_focus],".App_RE_inp_box")
			index_focus=next_filled_element_index
			if(index_focus<0)index_focus=inp_list.length-1
		}
	}
	tab_pressed=false
	shift_pressed=false

	inp_list[index_focus].focus()
	inp_list[index_focus].select()
	//???RE_set_current_position
}

function RE_move_focus_iA(evt,element){  //XXX
	switch(evt.keyCode){
	case 37:
		// Key left.
		//focus previous not disabled element
		RE_focus_next_active_iA_element(element,-1)
		evt.preventDefault()
		break
	case 38:
		// Key up.
		//focus prev voice
		RE_focus_voice_element_from_iA_element(element,true)
		evt.preventDefault()
		break
	case 39:
		// Key right.
		//focus next not disabled element
		RE_focus_next_active_iA_element(element,1)
		evt.preventDefault()
		break
	case 40:
		// Key down.
		//focus next voice
		RE_focus_voice_element_from_iA_element(element,false)
		evt.preventDefault()
		break
	case 9:
		//tab
		//focus next not disabled element
		evt.preventDefault()

		if(evt.shiftKey) {
			//shift was down when tab was pressed
			RE_focus_next_active_iA_element(element,-1)
		}else{
			//tab
			//find next filled element
			RE_focus_next_active_iA_element(element,1)
		}
		break
	case 13:
		//intro
		//focus next not disabled element
		RE_focus_next_active_iA_element(element,1)
		break
	}
	//preventDefault to avoid scrollbar to move automatically
	//NO need to a function that eventually move the scrollbar if the imput is out of sight
}

function RE_focus_next_active_iA_element(element,direction){
	if(direction==1){
		//right
		var next_element = _find_next_active_element(element,".App_RE_inp_box")
		if(next_element!=null){
			next_element.focus()
			next_element.select()
			return
		}else{
			//very last element
			//element.blur()
			//element.focus()
			document.querySelector('.App_RE_main_scrollbar_H').scrollLeft+=nuzic_block*2
		}
	}
	if(direction==-1){
		//left
		var previous_element = _find_previous_active_element(element,".App_RE_inp_box")
		if(previous_element!=null){
			previous_element.focus()
			previous_element.select()
			return
		}else{
			//very first element
			document.querySelector('.App_RE_main_scrollbar_H').scrollLeft-=nuzic_block*2
		}
	}
	//stay
	element.focus()
	element.select()


	function _find_next_active_element(element,string="input"){
		var parent = element.parentNode
		var input_list = [...parent.querySelectorAll(string)]
		var elementIndex=input_list.indexOf(element)
		var arr = input_list.filter((item)=>{
			return input_list.indexOf(item)>input_list.indexOf(element)
		})
		var next_active_element= arr.find((item)=>{
			return item.disabled==false
		})

		return next_active_element
	}

	function _find_previous_active_element(element,string="input"){
		var parent = element.parentNode
		var input_list = [...parent.querySelectorAll(string)]
		var elementIndex = input_list.indexOf(element)
		var index_previous = -1

		for (var i=elementIndex-1; i>=0;i--){
			var value = input_list[i].value
			if(input_list[i].disabled==false){
				//element not void
				index_previous=i
				i=-1
			}
		}

		if (index_previous>=0){
			return input_list[index_previous]
		}else{
			return null
		}
	}
}

//ATT!!! PORISTION IS NOT ABSOLUTE
function RE_focus_on_object_index_iA(voice_index_down,position,next=0){
	//var voice_obj = RE_find_voice_obj_by_index(current_position_PMCRE.voice_data.voice_index)
	var voice_obj_down = RE_find_voice_obj_by_index(voice_index_down)

	//find index
	//var visible_iA_list = [...voice_matrix.querySelectorAll(".App_RE_iA:not(.hidden)")]
	//var iA_index = visible_iA_list.indexOf(iA_list)

	//var iA_container_obj = voice_obj_down.previousSibling
	var iA_container_obj = voice_obj_down.previousElementSibling.querySelector(".App_RE_iA")

	if(iA_container_obj!=null){
		var iA_element_list = [...iA_container_obj.querySelectorAll(".App_RE_inp_box:not(:disabled)")]

		var iA_element = iA_element_list[position+next]
		if(iA_element!=null){
			iA_element.focus()
			iA_element.select()
			return true
		}else{
			return false
		}
	}
}

