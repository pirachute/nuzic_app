function TNAV_convertLiToTime(){
	var min = Math.floor(Li/PPM)
	var sec = Math.floor(Li*(60/PPM))-min*60
	if(sec<10)sec="0"+sec
	if(min<10)min="0"+min

	output_timer_partial_min.value = min + "'"
	output_timer_partial_sec.value = sec + '"'
}

function TNAV_stop_button(){
	APP_stop()
	var [now,start,end,max]=APP_return_progress_bar_values()
	APP_move_progress_bar(start)
	APP_draw_cursor_minimap_canvas_onPLay(0)
}

function TNAV_set_loop_OnOff(value){
	//APP_stop()
	var pressed = (outputLoop.value === 'true')

	if(!(pressed ^ value)){
		//no need to change
	}else{
		var icon = outputLoop.firstElementChild
		outputLoop.value=value
		if(value){
			//loop
			icon.src="./Icons/main_repeat_on.svg"
		}else{
			icon.src="./Icons/main_repeat.svg"
		}
	}
}

function TNAV_switch_loop_OnOff(){
	APP_stop()

	//reversed because it is not yet switched
	var pressed = (outputLoop.value === 'true')
	if(pressed){
		TNAV_set_loop_OnOff(false)
	}else{
		TNAV_set_loop_OnOff(true)
	}
	//Save state preferences
	DATA_save_preferences_file()
}

function TNAV_set_main_metronome_OnOff(value){
	//APP_stop()
	var pressed = (outputMainMetronome.value === 'true')

	if(!(pressed ^ value)){
		//no need to change
	}else{
		var icon = outputMainMetronome.firstElementChild
		outputMainMetronome.value=value
		if(value){
			icon.src="./Icons/main_metronome_on.svg"
		}else{
			icon.src="./Icons/main_metronome.svg"
		}
	}
}

function TNAV_switch_main_metronome_OnOff(){
	APP_stop()
	//reversed because it is not yet switched
	var pressed = (outputMainMetronome.value === 'true')
	if(pressed){
		TNAV_set_main_metronome_OnOff(false)
	}else{
		TNAV_set_main_metronome_OnOff(true)
	}
	//Save state preferences
	DATA_save_preferences_file()
}

function TNAV_undo(){
	APP_stop()
	var state_point = DATA_get_previous_state_point()
	if(state_point==null){
		console.log("no state point to load")
		return
	} else {
		flag_DATA_updated.PMC=false
		flag_DATA_updated.RE=false
		DATA_show_loader()
		setTimeout(function () {
			DATA_load_state_point_data(false,false)
			DATA_hide_loader()
		}, 50)
		//console.log("current_state_point_index "+current_state_point_index+"  min "+min_state_point_index+" max "+max_state_point_index)
	}
}

function TNAV_redo(){
	APP_stop()
	var state_point = DATA_get_next_state_point()	
	if(state_point==null){
		return
	} else {
		flag_DATA_updated.PMC=false
		flag_DATA_updated.RE=false
		DATA_show_loader()
		setTimeout(function () {
			DATA_load_state_point_data(false,false)
			DATA_hide_loader()
		}, 50)
		//console.log("state point loaded")
		//console.log("current_state_point_index "+current_state_point_index+"  min "+min_state_point_index+" max "+max_state_point_index)
	}
}

function TNAV_reset_composition_name(){
//	var navname = document.getElementsByClassName('c_name_nav')
	var composition_name = document.getElementById('composition_name')
	current_file_name = generic_file_name
	composition_name.value = current_file_name
	//navname[0].innerHTML = composition_name.value
}

function TNAV_read_composition_name(){
	//var navname = document.getElementsByClassName('c_name_nav')
	var composition_name = document.getElementById('composition_name')
	current_file_name = composition_name.value
}

function TNAV_maximize(bttn){
	//close save menu in case is opened
	TNAV_hide_main_menu()
	if(bttn.value==1){
		//bttn.value = 0
	}else if(bttn.value==0){
		//bttn.value=1
	}

	SNAV_select_zoom(document.querySelector('.App_SNav_select_zoom'))
	var element_header = document.getElementById("mainHeaderNav")
	//console.log(bttn.value)
	//var element_app_body = document.getElementById("App_body")
	var app_container = document.getElementById("page")
	var percent_string = 100+"%"
	var delta = ""
	if(element_header==null){
		delta = "56"
	}else{
		if(bttn.value==1){
			bttn.value = 0
		}else if(bttn.value==0){
			bttn.value=1
		}
		if(element_header.classList.contains("closed")){
			//open
			element_header.classList.remove("closed")
			delta= 56
		}else{
			element_header.classList.add("closed")
			delta= 0
		}
		var height_string = `calc(${percent_string} - ${delta}px)`
		app_container.style.height= height_string
		app_container.style.marginTop= `${delta}px`
	}


	//element_app_body.style.height= height_string
}

// function toggleFullScreen() {
//bad if used with F11
// 	if (!document.fullscreenElement) {
// 		document.documentElement.requestFullscreen();
// 	} else {
// 		if (document.exitFullscreen) {
// 			document.exitFullscreen();
// 		}
// 	}
// }

function TNAV_countIn_button(element){
	switch (element.value) {
		case "0":
			TNAV_set_countIn(2)
		break;
		case "2":
			TNAV_set_countIn(3)
		break;
		case "3":
			TNAV_set_countIn(4)
		break;
		case "4":
			TNAV_set_countIn(0)
		break;
		default:
			TNAV_set_countIn(0)
		break;
	}
}

function TNAV_set_countIn(value){
	var button = document.getElementById("countIn_button")
	button.value=value
	button.innerText = button.value
}

function TNAV_read_countIn(){
	var button = document.getElementById("countIn_button")
	return Number(button.value)
}

function TNAV_show_main_menu(){
	TNAV_menu_user_validation()
	APP_stop()
	
	var parent = document.querySelector('.App_Nav_end_menu')
	var menu = parent.querySelector('.App_Nav_menu_list')
	var button = parent.querySelector('.App_Nav_button')
	button.setAttribute('onClick', 'TNAV_hide_main_menu()')
	menu.style.display = 'flex'
	var elPosTop = 42
	var navTNAV_maximizeBttn = document.getElementById('App_Nav_maximize')
	var header = document.getElementById("mainHeaderNav")

	if(header!=null && navTNAV_maximizeBttn.value == "0"){
		elPosTop+=56
	}
	menu.style.top = `${elPosTop}px`
}

function TNAV_hide_main_menu(){
	var parent = document.querySelector('.App_Nav_end_menu')
	var menu = document.querySelector('.App_Nav_menu_list')
	var button = parent.querySelector('.App_Nav_button')
	button.setAttribute('onClick', 'TNAV_show_main_menu()')

	menu.style.display = 'none'

	menu.childNodes[3].setAttribute('disabled','')
	menu.childNodes[5].setAttribute('disabled','')
	menu.childNodes[7].setAttribute('disabled','')
	menu.childNodes[9].setAttribute('disabled','')
	menu.childNodes[13].setAttribute('disabled','')
	menu.childNodes[15].setAttribute('disabled','')

	menu.childNodes[3].classList.add('App_Nav_menu_list_disabled')
	menu.childNodes[5].classList.add('App_Nav_menu_list_disabled')
	menu.childNodes[7].classList.add('App_Nav_menu_list_disabled')
	menu.childNodes[9].classList.add('App_Nav_menu_list_disabled')
	menu.childNodes[13].classList.add('App_Nav_menu_list_disabled')
	menu.childNodes[15].classList.add('App_Nav_menu_list_disabled')
}

async function TNAV_menu_user_validation(){
	if(typeof wp_Get_User_Permissions!='function' || DEBUG_MODE){
		var user = 0
		if(DEBUG_MODE){
			user=100
		}else{
			return
		}
		var menu = document.querySelector('.App_Nav_menu_list')

		if(user == 0){
			menu.childNodes[3].setAttribute('disabled','')
			menu.childNodes[5].setAttribute('disabled','')
			menu.childNodes[7].setAttribute('disabled','')
			menu.childNodes[9].setAttribute('disabled','')
			menu.childNodes[13].setAttribute('disabled','')
			menu.childNodes[15].setAttribute('disabled','')

			menu.childNodes[3].classList.add('App_Nav_menu_list_disabled')
			menu.childNodes[5].classList.add('App_Nav_menu_list_disabled')
			menu.childNodes[7].classList.add('App_Nav_menu_list_disabled')
			menu.childNodes[9].classList.add('App_Nav_menu_list_disabled')
			menu.childNodes[13].classList.add('App_Nav_menu_list_disabled')
			menu.childNodes[15].classList.add('App_Nav_menu_list_disabled')
		}else if (user == 1 || user == 2 || user == 99 || user == 100 || DEBUG_MODE){
			menu.childNodes[3].removeAttribute('disabled')
			menu.childNodes[5].removeAttribute('disabled')
			menu.childNodes[7].removeAttribute('disabled')
			menu.childNodes[9].removeAttribute('disabled')
			menu.childNodes[13].removeAttribute('disabled')
			menu.childNodes[15].removeAttribute('disabled')
			menu.querySelector('.App_Nav_menu_info').style.display='none'
			menu.childNodes[3].classList.remove('App_Nav_menu_list_disabled')
			menu.childNodes[5].classList.remove('App_Nav_menu_list_disabled')
			menu.childNodes[7].classList.remove('App_Nav_menu_list_disabled')
			menu.childNodes[9].classList.remove('App_Nav_menu_list_disabled')
			menu.childNodes[13].classList.remove('App_Nav_menu_list_disabled')
			menu.childNodes[15].classList.remove('App_Nav_menu_list_disabled')
		}
	} else{
		var user = await wp_Get_User_Permissions('','levels')
		var menu = document.querySelector('.App_Nav_menu_list')

		if(user == 0){
			menu.childNodes[3].setAttribute('disabled','')
			menu.childNodes[5].setAttribute('disabled','')
			menu.childNodes[7].setAttribute('disabled','')
			menu.childNodes[9].setAttribute('disabled','')
			menu.childNodes[13].setAttribute('disabled','')
			menu.childNodes[15].setAttribute('disabled','')

			menu.childNodes[3].classList.add('App_Nav_menu_list_disabled')
			menu.childNodes[5].classList.add('App_Nav_menu_list_disabled')
			menu.childNodes[7].classList.add('App_Nav_menu_list_disabled')
			menu.childNodes[9].classList.add('App_Nav_menu_list_disabled')
			menu.childNodes[13].classList.add('App_Nav_menu_list_disabled')
			menu.childNodes[15].classList.add('App_Nav_menu_list_disabled')
		}else if (user == 1 || user == 2 || user == 99 || user == 100 || DEBUG_MODE){
			menu.childNodes[3].removeAttribute('disabled')
			menu.childNodes[5].removeAttribute('disabled')
			menu.childNodes[7].removeAttribute('disabled')
			menu.childNodes[9].removeAttribute('disabled')
			menu.childNodes[13].removeAttribute('disabled')
			menu.childNodes[15].removeAttribute('disabled')

			menu.childNodes[3].classList.remove('App_Nav_menu_list_disabled')
			menu.childNodes[5].classList.remove('App_Nav_menu_list_disabled')
			menu.childNodes[7].classList.remove('App_Nav_menu_list_disabled')
			menu.childNodes[9].classList.remove('App_Nav_menu_list_disabled')
			menu.childNodes[13].classList.remove('App_Nav_menu_list_disabled')
			menu.childNodes[15].classList.remove('App_Nav_menu_list_disabled')
		}
	}
}


