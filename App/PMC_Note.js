function PMC_redraw_sound_line(){
	//need to be called every time TET change
	//redraw sound line
	var line_container = document.querySelector(".App_PMC_sound_line")
	var note_container = line_container.querySelector(".App_PMC_sound_line_note_container")
	var note_list = [...line_container.querySelectorAll(".App_PMC_sound_line_note")]

	var note_number = TET*7
	if(note_number!=note_list.length){
		//redraw
		note_container.innerHTML=""

		for (let i=0; i < note_number; i++) {
			//add a div
			var div = document.createElement('div')
			div.classList.add("App_PMC_sound_line_note")
			note_container.appendChild(div)
		}
		//add final spacer
		var spacer_div = document.createElement('div')
		spacer_div.classList.add("App_PMC_element_spacer_V")
		note_container.appendChild(spacer_div)

		//add 2 canvas
		// var div_canvas_note = document.createElement('canvas')
		// div_canvas_note.classList.add("App_PMC_sound_line_canvas")
		// div_canvas_note.id = "App_PMC_sound_line_canvas_note"
		// line_container.appendChild(div_canvas_note)
		// var div_canvas_scale = document.createElement('canvas')
		// div_canvas_scale.classList.add("App_PMC_sound_line_canvas")
		// div_canvas_scale.id = "App_PMC_sound_line_canvas_scale"
		// line_container.appendChild(div_canvas_scale)

		note_container.style.height="calc(var(--PMC_y_block) * "+(note_number-0.5)+" + var(--RE_block_half))"
	}
	//write values
	PMC_write_sound_line_values()
	//write_canvas
	PMC_write_sound_line_canvas_note()
	PMC_redraw_Nabc()
}

function PMC_redraw_Nabc(){
	var parent = document.querySelector('.App_PMC_voice_selector_letter_guide')
	parent.innerHTML=''

	if(PMC_zoom_y==1.5 || PMC_zoom_y==1.2 || PMC_zoom_y==1 || PMC_zoom_y==0.75 || PMC_zoom_y==0.5){
		var letterHeight = nuzic_block*PMC_zoom_y
		if(PMC_read_vsb_Nabc() && TET==12){
			//PINTO
			for(i=0;i<84;i++){
				var letterContainer = document.createElement('div')
				letterContainer.classList.add('App_PMC_voice_selector_letter_div')
				letterContainer.style.height=`${letterHeight}px`
				letterContainer.style.minHeight=`${letterHeight}px`
				parent.appendChild(letterContainer)
				let letter = document.createElement('p')
				letter.classList.add('App_PMC_voice_selector_letter_text')
				letter.innerHTML= ABC_note_list_diesis[11-(i%12)]
				/* letter.style.height=`${letterHeight}px`
				letter.style.minHeight=`${letterHeight}px` */
				letterContainer.appendChild(letter)
			}
		}else{
			return
		}
	}else{
		return
	}
}

function PMC_write_sound_line_values(){
	//read what tipe of values to write
	var vsb_N=PMC_read_vsb_N()
	var note_type = vsb_N.type

	//list objects
	var container = document.querySelector(".App_PMC_sound_line")
	//console.log(container)
	var note_list = [...container.querySelectorAll(".App_PMC_sound_line_note")]
	var note_number = TET*7
	if(note_number!=note_list.length){
		PMC_redraw_sound_line()
		return
	}
	note_list = note_list.reverse()
	//write values
	switch (note_type) {
	case "Na":
		var i = 0
		note_list.forEach(item=>{
			if(PMC_zoom_y<0.5){
				if(i%TET==0){
					item.innerHTML = i
				}else{
					item.innerHTML = ""
				}
			} else {
				item.innerHTML = i
			}
			i++
		})
	break;
	case "Nm":
		var index = 0
		note_list.forEach(item=>{
			var reg = Math.floor(index/TET)
			var note = index%TET
			if(PMC_zoom_y<0.5){
				if(note==0){
					item.innerHTML = `${note}r${reg}`
				}else{
					item.innerHTML = ""
				}
			} else {
				if(note==0){
					item.innerHTML = `${note}r${reg}`
				} else{
					item.innerHTML = `${note}`
				}
			}
			index++
		})
	break;
	case "Ngm":
		var index = 0
		note_list.forEach(item=>{
			var reg = Math.floor(index/TET)
			var note = index%TET
			if(PMC_zoom_y<0.5){
				if(note==0){
					item.innerHTML = `${note}r${reg}`
				}else{
					item.innerHTML = ""
				}
			} else {
				if(note==0){
					item.innerHTML = `${note}r${reg}`
				} else{
					item.innerHTML = `${note}`
				}
			}
			index++
		})
	break;
	}
	//XXX
	//add some other visual things (reg start , scale, fractioning??? , etc)
}

function PMC_write_sound_line_canvas_note(){
	var canvas = document.querySelector("#App_PMC_sound_line_canvas_note")
	//var parent = canvas_old.parentNode
	//delete old canvas
	// parent.removeChild(canvas_old)

	// var canvas = document.createElement('canvas')
	// canvas.classList.add("App_PMC_sound_line_canvas")
	// canvas.id="App_PMC_sound_line_canvas_note"
	// parent.appendChild(canvas)

	var delta = nuzic_block * PMC_zoom_y
	//var offset = (block + delta)/2 //2 half block
	var offset = delta/2

	var note_number = TET*7
	canvas.style.height="calc(var(--PMC_y_block) * "+(note_number-0.5)+" + var(--RE_block_half))"
	PMC_mouse_action_sound_svg.style.height="calc(var(--PMC_y_block) * "+(note_number-0.5)+" + var(--RE_block_half))"

	canvas.width= nuzic_block *1.5
	canvas.height = nuzic_block/2 + delta * (note_number-0.5)

	var ctx = canvas.getContext('2d')
	//clear canvas
	ctx.clearRect(0, 0, canvas.width, canvas.height)

	//note tics
	for (var i=0; i<note_number;i++){
		//left
		ctx.beginPath()
		var y = Math.round(offset+i*delta)-0.5
		ctx.moveTo(0,y)
		ctx.lineTo(7,y)
		ctx.strokeStyle = nuzic_dark
		ctx.lineWidth = 1
		ctx.stroke()
		//right
		ctx.beginPath()
		ctx.moveTo(nuzic_block *1.5-7,y)
		ctx.lineTo(nuzic_block *1.5,y)
		ctx.strokeStyle = nuzic_dark
		ctx.lineWidth = 1
		ctx.stroke()
	}

	//reg
	for (var i=0; i<7;i++){
		//left
		ctx.beginPath()
		var y = offset+delta*(TET-1)+i*delta*TET-0.5
		ctx.moveTo(0,y)
		ctx.lineTo(7,y)
		ctx.strokeStyle = nuzic_dark
		ctx.lineWidth = 3
		ctx.stroke()
		//right
		ctx.beginPath()
		ctx.moveTo(nuzic_block *1.5-7,y)
		ctx.lineTo(nuzic_block *1.5,y)
		ctx.strokeStyle = nuzic_dark
		ctx.lineWidth = 3
		ctx.stroke()
	}
}

