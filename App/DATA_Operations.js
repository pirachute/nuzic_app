/////////*****REPETITIONS*****/////////

function DATA_repeat_voice(voice_index,repetitions,insert){
	var data = DATA_get_current_state_point(true)

	//remove solo
	data.voice_data.forEach(voice=>{
		voice.solo=false
	})
	//duplicate current voice
	var voice_position = -1
	var voice_data = JSON.parse(JSON.stringify(data.voice_data.find(item=>{
		voice_position++
		return item.voice_index==voice_index
	})))

	if(voice_data==null)return

	if(insert){
		var voice_index_list = data.voice_data.map(item=>{
			var number = Number(item.voice_index)
			return number
		})

		var new_voice_index = Math.max(...voice_index_list)+1

		voice_data.solo= false
		voice_data.selected=false

		for (let i = 0; i < repetitions; i++) {
			var voice_data_copy=JSON.parse(JSON.stringify(voice_data))
			voice_data_copy.voice_index = new_voice_index
			new_voice_index++
			var string = voice_data_copy.name+"#"
			voice_data_copy.name= string.slice(0,8)
			data.voice_data.splice(voice_position,0,voice_data_copy)
		}

		DATA_insert_new_state_point(data)
		DATA_load_state_point_data(false,true)
	}else{
		//overwrite
		var voice_index_list = data.voice_data.map(item=>{
			var number = Number(item.voice_index)
			return number
		})
		var new_voice_index = Math.max(...voice_index_list)+1
		voice_data.solo= false
		voice_data.selected=false
		for (let i = 0; i < repetitions; i++) {
			var voice_data_copy=JSON.parse(JSON.stringify(voice_data))
			voice_data_copy.voice_index = new_voice_index
			new_voice_index++
			var string = voice_data_copy.name+"#"
			voice_data_copy.name= string.slice(0,8)
			voice_position--
			if(voice_position>=0){
				//change voice
				data.voice_data.splice(voice_position,1,voice_data_copy)
			}else{
				//add voice
				data.voice_data.splice(0,0,voice_data_copy)
			}
		}
		//verify at least oneselected exist
		var something_selected = data.voice_data.some(voice=>{
			return voice.selected
		})
		if(!something_selected){
			//select the copied voice
			data.voice_data.find(item=>{
				if(item.voice_index==voice_index)item.selected=true
				return item.voice_index==voice_index
			})
		}

		DATA_insert_new_state_point(data)
		DATA_load_state_point_data(false,true)
	}
}

function DATA_repeat_segment(voice_index,segment_index,repetitions,insert){
	var data = DATA_get_current_state_point(true)

	var current_voice_data = data.voice_data.find(item=>{
		return item.voice_index==voice_index
	})

	if(current_voice_data==null){
		console.error("Error repeating segment, voice index not found")
		return
	}

	if(current_voice_data.data.segment_data.length<segment_index || segment_index<0){
		console.error("Error repeating segment, segment index not found")
		return
	}

	var segment_Ls_absolte = current_voice_data.data.segment_data[segment_index].time.slice(-1)[0].P *current_voice_data.neopulse.N/current_voice_data.neopulse.D
	var segment_data=JSON.parse(JSON.stringify(current_voice_data.data.segment_data[segment_index]))

	if(insert){
		var new_Li_absolute=(segment_Ls_absolte*repetitions+data.global_variables.Li)
		//verify if compatible with max Li
		if(new_Li_absolute>max_Li){
			console.error("ERROR: max Li reached")
			return
		}

		if(current_voice_data.blocked){
			//voice is blocked
			data.voice_data.forEach(voice=>{
				var new_Ls=segment_Ls_absolte*voice.neopulse.D/voice.neopulse.N
				var fraction=[]
				fraction[0] = {"N":1,"D":1,"start":0,"stop":new_Ls}
				var empty_segment_data={"time":[{"P":0,"F":0},{"P":new_Ls,"F":0}],"fraction":fraction,"note":[-1,-4],diesis:[true,true],"segment_name":segment_data.segment_name}

				if(voice.blocked){
					if(voice.voice_index==voice_index){
						//add copy of segment in position
						for (let i = 0; i < repetitions; i++) {
							var segment_data_copy=JSON.parse(JSON.stringify(segment_data))
							voice.data.segment_data.splice(segment_index,0,segment_data_copy)
						}
					}else{
						//add empty segments in position
						for (let i = 0; i < repetitions; i++) {
							var empty_segment_data_copy=JSON.parse(JSON.stringify(empty_segment_data))
							voice.data.segment_data.splice(segment_index+1,0,empty_segment_data_copy)
						}
					}

					//midi
					voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
				}else{
					//add-extend segment in end
					DATA_calculate_force_voice_Li(voice,new_Li_absolute,false)
				}
			})
		}else{
			//voice is unblocked
			//verify if blocked voices can be extended
			var extend_blocked=DATA_verify_blocked_voices_extend_last_segment(data)

			data.voice_data.forEach(voice=>{
				var new_Ls=(segment_Ls_absolte*voice.neopulse.D/voice.neopulse.N)
				var fraction=[]
				fraction[0] = {"N":1,"D":1,"start":0,"stop":new_Ls}
				var new_segment_data={"time":[{"P":0,"F":0},{"P":new_Ls,"F":0}],"fraction":fraction,"note":[-1,-4],diesis:[true,true],"segment_name":""}
				if(voice.voice_index==voice_index){
					//add segment in position current voice
					for (let i = 0; i < repetitions; i++) {
						var segment_data_copy=JSON.parse(JSON.stringify(segment_data))
						voice.data.segment_data.splice(segment_index,0,segment_data_copy)
					}
					//midi
					//console.log(voice.data.segment_data)
					voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
				}else{
					//add-extend segment in end
					DATA_calculate_force_voice_Li(voice,new_Li_absolute,extend_blocked)
				}
			})
		}
		//recalculate Li
		data.global_variables.Li=new_Li_absolute
		DATA_insert_new_state_point(data)
		DATA_load_state_point_data(false,true)
	}else{
		//overwrite
		//verify if compatible with max Li
		var sum_Ls_to_segment_rep = 0
		var sum_Ls_from_segment_rep = 0
		current_voice_data.data.segment_data.forEach((segment,index)=>{
			if(index<=segment_index)sum_Ls_to_segment_rep+=segment.time.slice(-1)[0].P *current_voice_data.neopulse.N/current_voice_data.neopulse.D
			if(index>segment_index+repetitions)sum_Ls_from_segment_rep+=segment.time.slice(-1)[0].P *current_voice_data.neopulse.N/current_voice_data.neopulse.D
		})
		var new_Li_absolute=sum_Ls_to_segment_rep+(segment_Ls_absolte*repetitions)+sum_Ls_from_segment_rep

		if(new_Li_absolute>max_Li){
			console.error("ERROR: max Li reached")
			return
		}

		if(current_voice_data.blocked){
			//voice is blocked
			data.voice_data.forEach(voice=>{
				if(voice.voice_index==voice_index){
					//replace/create necessary structure in current voice
					var segment_position=segment_index
					var segment_length = voice.data.segment_data.length
					for (let i = 0; i < repetitions; i++) {
						var segment_data_copy=JSON.parse(JSON.stringify(segment_data))
						segment_position++
						if(segment_position<=segment_length){
							//change segment
							voice.data.segment_data.splice(segment_position,1,segment_data_copy)
						}else{
							//add segment
							voice.data.segment_data.splice(segment_length-1,0,segment_data_copy)
						}
					}
					//midi
					//console.log(voice.data.segment_data)
					voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
				}else{
					if(voice.blocked){
						//cut/expand/create segments un other blocked voices
						var segment_position=segment_index
						var segment_length = voice.data.segment_data.length
						var new_Ls = voice.data.segment_data[segment_position].time.slice(-1)[0].P
						var new_name = voice.data.segment_data[segment_position].segment_name
						for (let i = 0; i < repetitions; i++) {
							segment_position++
							if(segment_position<segment_length){
								//change segment Ls
								DATA_calculate_force_segment_data_Ls(voice.data.segment_data[segment_position],new_Ls)
								//change segment name
								voice.data.segment_data[segment_position].segment_name=new_name
							}else{
								//add empty segment
								var fraction=[]
								fraction[0] = {"N":1,"D":1,"start":0,"stop":new_Ls}
								var new_segment_data={"time":[{"P":0,"F":0},{"P":new_Ls,"F":0}],"fraction":fraction,"note":[-1,-4],diesis:[true,true],"segment_name":new_name}
								voice.data.segment_data.splice(segment_length,0,new_segment_data)
							}
						}
						//midi
						voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
					}else{
						//cut/expand unnblocked voices
						DATA_calculate_force_voice_Li(voice,new_Li_absolute,false)
					}
				}
			})
		}else{
			//voice is unblocked
			//verify if last segment blocked voices can be extended
			var extend_blocked=DATA_verify_blocked_voices_extend_last_segment(data)

			data.voice_data.forEach(voice=>{
				if(voice.voice_index==voice_index){
					//replace/create necessary structure in current voice
					var segment_position=segment_index
					var segment_length = voice.data.segment_data.length
					for (let i = 0; i < repetitions; i++) {
						var segment_data_copy=JSON.parse(JSON.stringify(segment_data))
						segment_position++
						if(segment_position<=segment_length){
							//change segment
							voice.data.segment_data.splice(segment_position,1,segment_data_copy)
						}else{
							//add segment
							voice.data.segment_data.splice(segment_length-1,0,segment_data_copy)
						}
					}
					//midi
					//console.log(voice.data.segment_data)
					voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
				}else{
					//cut/expand other voices
					DATA_calculate_force_voice_Li(voice,new_Li_absolute,extend_blocked)
				}
			})
		}
		//recalculate Li
		data.global_variables.Li=new_Li_absolute
		DATA_insert_new_state_point(data)
		DATA_load_state_point_data(false,true)
	}
}

function DATA_repeat_segment_column(segment_index,repetitions,insert){
	var data = DATA_get_current_state_point(true)

	var current_voice_data_list = data.voice_data.filter(item=>{
		return item.blocked
	})

	if(current_voice_data_list.length==0){
		console.error("Error repeating segment, voice index not found")
		return
	}

	if(current_voice_data_list[0].data.segment_data.length<segment_index || segment_index<0){
		console.error("Error repeating segment, segment index not found")
		return
	}

	var segment_Ls_absolte = current_voice_data_list[0].data.segment_data[segment_index].time.slice(-1)[0].P *current_voice_data_list[0].neopulse.N/current_voice_data_list[0].neopulse.D

	if(insert){
		var new_Li_absolute=(segment_Ls_absolte*repetitions+data.global_variables.Li)
		//verify if compatible with max Li
		if(new_Li_absolute>max_Li){
			console.error("ERROR: max Li reached")
			return
		}

		//clone if voice is blocked, add segment end if not
		data.voice_data.forEach(voice=>{
			if(voice.blocked){
				var segment_data=JSON.parse(JSON.stringify(voice.data.segment_data[segment_index]))

				//add copy of segment in position
				for (let i = 0; i < repetitions; i++) {
					var segment_data_copy=JSON.parse(JSON.stringify(segment_data))
					voice.data.segment_data.splice(segment_index,0,segment_data_copy)
				}
				//midi
				voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
			}else{
				//add-extend segment in end
				DATA_calculate_force_voice_Li(voice,new_Li_absolute,false)
			}
		})

		//recalculate Li
		data.global_variables.Li=new_Li_absolute
		DATA_insert_new_state_point(data)
		DATA_load_state_point_data(false,true)
	}else{
		//overwrite
		//verify if compatible with max Li
		var sum_Ls_to_segment_rep = 0
		var sum_Ls_from_segment_rep = 0
		current_voice_data_list[0].data.segment_data.forEach((segment,index)=>{
			if(index<=segment_index)sum_Ls_to_segment_rep+=segment.time.slice(-1)[0].P *current_voice_data_list[0].neopulse.N/current_voice_data_list[0].neopulse.D
			if(index>segment_index+repetitions)sum_Ls_from_segment_rep+=segment.time.slice(-1)[0].P *current_voice_data_list[0].neopulse.N/current_voice_data_list[0].neopulse.D
		})
		var new_Li_absolute=sum_Ls_to_segment_rep+(segment_Ls_absolte*repetitions)+sum_Ls_from_segment_rep

		if(new_Li_absolute>max_Li){
			console.error("ERROR: max Li reached")
			return
		}

		data.voice_data.forEach(voice=>{
			if(voice.blocked){
				//replace/create necessary structure in current voice
				var segment_data=JSON.parse(JSON.stringify(voice.data.segment_data[segment_index]))
				var segment_position=segment_index
				var segment_length = voice.data.segment_data.length
				for (let i = 0; i < repetitions; i++) {
					var segment_data_copy=JSON.parse(JSON.stringify(segment_data))
					segment_position++
					if(segment_position<=segment_length){
						//change segment
						voice.data.segment_data.splice(segment_position,1,segment_data_copy)
					}else{
						//add segment
						voice.data.segment_data.splice(segment_length-1,0,segment_data_copy)
					}
				}
				//midi
				voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
			}else{
				//cut/expand unnblocked voices
				DATA_calculate_force_voice_Li(voice,new_Li_absolute,false)
			}
		})

		//recalculate Li
		data.global_variables.Li=new_Li_absolute
		DATA_insert_new_state_point(data)
		DATA_load_state_point_data(false,true)
	}
}


/////////*****SUMS*****/////////

//iT
function DATA_sum_iT_segment(voice_index,segment_index,value,block_Ls,force_sum){//force sum adapt Fr
}
function DATA_sum_iT_segment_column(segment_index,value,block_Ls,force_sum){//force sum adapt Fr
}
function DATA_sum_iT_voice(voice_index,value,block_Ls,force_sum){//force sum adapt Fr
}

//iNa
function DATA_calculate_sum_iNa_segment(segment,value,option){
	var max_note = TET*7 -1
	var outside_range=false
	var sum_success=false

	var note_array_copy = JSON.parse(JSON.stringify(segment.note))
	var iNa_list=DATA_calculate_iNa_from_Na_list(note_array_copy)
	var note_array_copy = []
	var current_note=0
	//process iNa_list

	var is_starting_note=true
	iNa_list.forEach((current_iNa,current_iNa_index)=>{
		if(!isNaN(current_iNa)){
			if(is_starting_note){
				is_starting_note=false
				current_note= current_iNa
				note_array_copy.push(current_iNa)
			}else{
				//new iNa
				sum_success=true
				var provv = current_note
				switch (option) {
					case "sum":
						//pure sum of current value
						//iNa_list[current_iNa_index]=current_iNa+value
						provv += current_iNa+value
					break;
					case "abs":
						//mantain original iNa sign and sum absolute value (expansion or contraction)
						provv += Math.sign(current_iNa)*(Math.abs(current_iNa)+value)
					break;
				}
				current_note = DATA_calculate_inRange(provv,0,max_note)[0]
				if(provv!=current_note)outside_range=true
				note_array_copy.push(current_note)
			}
		}else{
			if(current_iNa=="s"){
				note_array_copy.push(-1)
			}else if(current_iNa=="e"){
				note_array_copy.push(-2)
			}else if(current_iNa==tie_intervals){
				note_array_copy.push((current_iNa_index==0)?-2:-3)
			}else if(current_iNa==end_range){
				note_array_copy.push((current_iNa_index==iNa_list.length-1)?-4:-3)
			}
		}
	})
	if(sum_success)segment.note=note_array_copy
	return [sum_success,outside_range]
}

function DATA_sum_iNa_segment(voice_index,segment_index,value,option){
	if(voice_index==-1 || segment_index==-1)return
	var data = DATA_get_current_state_point(true)
	var current_voice_data = data.voice_data.find(item=>{return item.voice_index==voice_index})
	if(current_voice_data==null){
		console.error("Operation error, data not found")
		return
	}
	if(current_voice_data.data.segment_data.length<=segment_index){
		console.error("Operation error, data not found")
		return
	}

	var [sum_success,outside_range]=DATA_calculate_sum_iNa_segment(current_voice_data.data.segment_data[segment_index],value,option)
	if(!sum_success){
		console.warn("Nothing to do")
		return
	}
	if(outside_range){
		console.warn("ATTENTION: sum operation maxed note range")
	}
	current_voice_data.data.midi_data=DATA_segment_data_to_midi_data(current_voice_data.data.segment_data,current_voice_data.neopulse)
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_sum_iNa_segment_column(segment_index,value,option){
	if(segment_index==-1)return
	var data = DATA_get_current_state_point(true)
	var find_data=false
	var outside_range=false
	var sum_success=false
	var max_note = TET*7 -1
	data.voice_data.forEach(voice=>{
		if(voice.blocked){
			find_data=true
			if(voice.data.segment_data.length<=segment_index){
				find_data=false
				return
			}

			var [success,out_r]=DATA_calculate_sum_iNa_segment(voice.data.segment_data[segment_index],value,option)
			if(!success){
				return
			}
			if(out_r)outside_range=true
			sum_success=true
			//calculate midi
			voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
		}
	})
	if(!find_data){
		console.error("Operation error, data not found")
		return
	}
	if(!sum_success){
		console.warn("Nothing to do")
		return
	}
	if(outside_range){
		console.warn("ATTENTION: sum operation maxed note range")
	}

	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_sum_iNa_voice(voice_index,value,option){
	if(voice_index==-1)return
	var data = DATA_get_current_state_point(true)
	var current_voice_data = data.voice_data.find(item=>{return item.voice_index==voice_index})
	if(current_voice_data==null){
		console.error("Operation error, data not found")
		return
	}

	var max_note = TET*7 -1
	var outside_range=false
	var sum_success=false
	current_voice_data.data.segment_data.forEach(segment=>{
		var [success,out_r]=DATA_calculate_sum_iNa_segment(segment,value,option)
		if(!success){
			return
		}
		if(out_r)outside_range=true
		sum_success=true
	})
	if(!sum_success){
		console.warn("Nothing to do")
		return
	}
	if(outside_range){
		console.warn("ATTENTION: sum operation maxed note range")
	}
	current_voice_data.data.midi_data=DATA_segment_data_to_midi_data(current_voice_data.data.segment_data,current_voice_data.neopulse)
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

//Na
function DATA_calculate_sum_Na_segment(segment,value){
	var max_note = TET*7 -1
	var outside_range=false
	var sum_success=false

	segment.note.forEach((note,note_index)=>{
		if(note>=0){
			sum_success=true
			note+=value
			if(note>max_note){
				note=max_note
				outside_range=true
			}
			if(note<0){
				note=0
				outside_range=true
			}
			segment.note[note_index]=note
		}
	})
	return [sum_success,outside_range]
}

function DATA_sum_Na_segment(voice_index,segment_index,value){
	if(voice_index==-1 || segment_index==-1)return
	var data = DATA_get_current_state_point(true)
	var current_voice_data = data.voice_data.find(item=>{return item.voice_index==voice_index})
	if(current_voice_data==null){
		console.error("Operation error, data not found")
		return
	}
	if(current_voice_data.data.segment_data.length<=segment_index){
		console.error("Operation error, data not found")
		return
	}

	var [sum_success,outside_range]=DATA_calculate_sum_Na_segment(current_voice_data.data.segment_data[segment_index],value)
	if(!sum_success){
		console.warn("Nothing to do")
		return
	}
	if(outside_range){
		console.warn("ATTENTION: sum operation maxed note range")
	}
	current_voice_data.data.midi_data=DATA_segment_data_to_midi_data(current_voice_data.data.segment_data,current_voice_data.neopulse)
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_sum_Na_segment_column(segment_index,value){
	if(segment_index==-1)return
	var data = DATA_get_current_state_point(true)
	var find_data=false
	var outside_range=false
	var sum_success=false
	var max_note = TET*7 -1
	data.voice_data.forEach(voice=>{
		if(voice.blocked){
			find_data=true
			if(voice.data.segment_data.length<=segment_index){
				find_data=false
				return
			}
			var [success,out_r]=DATA_calculate_sum_Na_segment(voice.data.segment_data[segment_index],value)
			if(!success){
				return
			}
			if(out_r)outside_range=true
			sum_success=true
			//calculate midi
			voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
		}
	})
	if(!find_data){
		console.error("Operation error, data not found")
		return
	}
	if(!sum_success){
		console.warn("Nothing to do")
		return
	}
	if(outside_range){
		console.warn("ATTENTION: sum operation maxed note range")
	}

	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_sum_Na_voice(voice_index,value){
	if(voice_index==-1)return
	var data = DATA_get_current_state_point(true)
	var current_voice_data = data.voice_data.find(item=>{return item.voice_index==voice_index})
	if(current_voice_data==null){
		console.error("Operation error, data not found")
		return
	}

	var max_note = TET*7 -1
	var outside_range=false
	var sum_success=false
	current_voice_data.data.segment_data.forEach(segment=>{
		var [success,out_r]=DATA_calculate_sum_Na_segment(segment,value)
		if(!success){
			return
		}
		if(out_r)outside_range=true
		sum_success=true
	})
	if(!sum_success){
		console.warn("Nothing to do")
		return
	}
	if(outside_range){
		console.warn("ATTENTION: sum operation maxed note range")
	}
	current_voice_data.data.midi_data=DATA_segment_data_to_midi_data(current_voice_data.data.segment_data,current_voice_data.neopulse)
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

//iNgm XXX
function DATA_sum_iNgm_segment(voice_index,segment_index,value,option){}
function DATA_sum_iNgm_segment_column(segment_index,value,option){}
function DATA_sum_iNgm_voice(voice_index,value,option){}
//Ngm XXX
function DATA_sum_Ngm_segment(voice_index,segment_index,value){}
function DATA_sum_Ngm_segment_column(segment_index,value){}
function DATA_sum_Ngm_voice(voice_index,value){}


/////////*****ROTATIONS*****/////////

//iT and N XXX
function DATA_rotate_element_segment(voice_index,segment_index,value,force){}
function DATA_rotate_element_segment_column(segment_index,value,force){}
function DATA_rotate_element_voice(voice_index,value,force,by_segment){
	if(voice_index==-1)return
	var data = DATA_get_current_state_point(true)
	var current_voice_data = data.voice_data.find(item=>{return item.voice_index==voice_index})
	if(current_voice_data==null){
		console.error("Operation error, data not found")
		return
	}

	//rotate only note, no s,e,L,end_note
	var from_n=-1
	if(rot_se){
		//rotate only note,s,e, no L,end_note
		from_n=-3
	}

	if(by_segment){
		var rot_success=false
		current_voice_data.data.segment_data.forEach((segment,segment_index)=>{
			//XXX XXX XXX
		})

		if(!rot_success){
			console.warn("Nothing to do")
			return
		}
return
		//calculate midi
		current_voice_data.data.midi_data=DATA_segment_data_to_midi_data(current_voice_data.data.segment_data,current_voice_data.neopulse)
		//save new database
		DATA_insert_new_state_point(data)
		DATA_load_state_point_data(false,true)
		return
	}else{
		//rotate everything XXX

	}
}
//iT XXX
function DATA_rotate_iT_segment(voice_index,segment_index,value,force){}
function DATA_rotate_iT_segment_column(segment_index,value,force){}
function DATA_rotate_iT_voice(voice_index,value,force,by_segment){
	if(voice_index==-1)return
	var data = DATA_get_current_state_point(true)
	var current_voice_data = data.voice_data.find(item=>{return item.voice_index==voice_index})
	if(current_voice_data==null){
		console.error("Operation error, data not found")
		return
	}

	//rotate only note, no s,e,L,end_note
	var from_n=-1
	if(rot_se){
		//rotate only note,s,e, no L,end_note
		from_n=-3
	}

	if(by_segment){
		var rot_success=false
		current_voice_data.data.segment_data.forEach((segment,segment_index)=>{
			//XXX XXX XXX
		})

		if(!rot_success){
			console.warn("Nothing to do")
			return
		}
return
		//calculate midi
		current_voice_data.data.midi_data=DATA_segment_data_to_midi_data(current_voice_data.data.segment_data,current_voice_data.neopulse)
		//save new database
		DATA_insert_new_state_point(data)
		DATA_load_state_point_data(false,true)
		return
	}else{
		//rotate everything XXX

	}
}

//iNa
function DATA_calculate_rotate_iNa_segment(segment,value,rot_se){
	var rot_success=false
	//rotate notes
	var note_array_copy = JSON.parse(JSON.stringify(segment.note))
	var diesis_array_copy = JSON.parse(JSON.stringify(segment.diesis))

	var iNa_list=DATA_calculate_iNa_from_Na_list(note_array_copy)
	//NO ROTATION OF L
	var iNa_list_rot=[]
	var diesis_array_rot=[]

	if(rot_se){
		//rotate only note,s,e, no L,end_note
		var from_n=-3
		note_array_copy.forEach((note,index_note)=>{
			if(note>from_n){
				iNa_list_rot.push(iNa_list[index_note])
				diesis_array_rot.push(diesis_array_copy[index_note])
			}
		})
		//verify if there is something to rotate
		if(iNa_list_rot.length>1){
			rot_success=true
			//rotate iNa
			for (let i = 0; i < value; i++) {
				var current_iNa=iNa_list_rot.shift()
				var current_diesis=diesis_array_rot.shift()
				if(isNaN(current_iNa)){
					iNa_list_rot.push(current_iNa)
					diesis_array_rot.push(current_diesis)
				}else{
					//it is starting note
					//find next iNa
					var next_index= _jump_to_next_note_index(iNa_list_rot)
					if(next_index==-1){
						//no other notes, put it last
						iNa_list_rot.push(current_iNa)
						diesis_array_rot.push(current_diesis)
					}else{
						var next_iNa=iNa_list_rot[next_index]
						var next_diesis=diesis_array_rot[next_index]
						//take iNa to last ans sunstitute with starting note
						iNa_list_rot.push(next_iNa)
						diesis_array_rot.push(next_diesis)
						iNa_list_rot[next_index]=current_iNa
						diesis_array_rot[next_index]=current_diesis
					}
				}
			}
			//reinsert
			var position=0
			note_array_copy.forEach((note,index_note)=>{
				if(note>from_n){
					iNa_list[index_note]=iNa_list_rot[position]
					diesis_array_copy[index_note]=diesis_array_rot[position]
					position++
				}
			})
			note_array_copy=DATA_calculate_Na_from_iNa_list(iNa_list)
		}
	}else{
		//rotate only note, no s,e,L,end_note
		var from_n=-1
		note_array_copy.forEach((note,index_note)=>{
			if(note>from_n){
				iNa_list_rot.push(iNa_list[index_note])
				diesis_array_rot.push(diesis_array_copy[index_note])
			}
		})
		var starting_note=iNa_list_rot.shift()
		var starting_diesis=diesis_array_rot.shift()
		if(iNa_list_rot.length>1){
			rot_success=true
			//rotate iNa
			for (let i = 0; i < value; i++) {
				iNa_list_rot.push(iNa_list_rot.shift())
				diesis_array_rot.push(diesis_array_rot.shift())
			}
			iNa_list_rot.unshift(starting_note)
			diesis_array_rot.unshift(starting_diesis)

			//reinsert
			var position=0
			note_array_copy.forEach((note,index_note)=>{
				if(note>from_n){
					iNa_list[index_note]=iNa_list_rot[position]
					diesis_array_copy[index_note]=diesis_array_rot[position]
					position++
				}
			})
			note_array_copy=DATA_calculate_Na_from_iNa_list(iNa_list)
		}
	}
	if(rot_success){
		segment.note=note_array_copy
		segment.diesis=diesis_array_copy
	}
	return rot_success

	function _jump_to_next_note_index(new_note_order){
		if(new_note_order.length==0){
			console.error("NOTE NOT FOUND")
			return -1
		}
		var next_index=-1
		new_note_order.find((iNa,index)=>{
			if(!isNaN(iNa)){
				next_index=index
				return true
			}
		})
		return next_index
	}
}

function DATA_rotate_iNa_segment(voice_index,segment_index,value,rot_se){
	if(voice_index==-1 || segment_index==-1)return
	var data = DATA_get_current_state_point(true)
	var current_voice_data = data.voice_data.find(item=>{return item.voice_index==voice_index})
	if(current_voice_data==null){
		console.error("Operation error, data not found")
		return
	}
	if(current_voice_data.data.segment_data.length<=segment_index){
		console.error("Operation error, data not found")
		return
	}

	var rot_success = DATA_calculate_rotate_iNa_segment(current_voice_data.data.segment_data[segment_index],value,rot_se)
	if(!rot_success){
		console.warn("Nothing to do")
		return
	}

	//calculate midi
	current_voice_data.data.midi_data=DATA_segment_data_to_midi_data(current_voice_data.data.segment_data,current_voice_data.neopulse)
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_rotate_iNa_segment_column(segment_index,value,rot_se){
	if(segment_index==-1)return
	var data = DATA_get_current_state_point(true)
	var find_data=false
	var rot_success=false
	data.voice_data.forEach(voice=>{
		if(voice.blocked){
			find_data=true
			if(voice.data.segment_data.length<=segment_index){
				find_data=false
				return
			}

			var success = DATA_calculate_rotate_iNa_segment(current_voice_data.data.segment_data[segment_index],value,rot_se)
			if(!success){
				return
			}
			rot_success=true
			//calculate midi
			voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
		}
	})
	if(!find_data){
		console.error("Operation error, data not found")
		return
	}
	if(!rot_success){
		console.warn("Nothing to do")
		return
	}

	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_rotate_iNa_voice(voice_index,value,rot_se,by_segment){
	if(voice_index==-1)return
	var data = DATA_get_current_state_point(true)
	var current_voice_data = data.voice_data.find(item=>{return item.voice_index==voice_index})
	if(current_voice_data==null){
		console.error("Operation error, data not found")
		return
	}

	if(by_segment){
		var rot_success=false
		current_voice_data.data.segment_data.forEach((segment,segment_index)=>{
			//rotate notes
			var success = DATA_calculate_rotate_iNa_segment(segment,value,rot_se)
			if(!success){
				return
			}
			rot_success=true
		})
	}else{
		//copy Note lines
		var provv_note_order = []
		var provv_diesis_order = []
		//no need to control end segment
		current_voice_data.data.segment_data.forEach((segment,segment_index)=>{
			provv_note_order = provv_note_order.concat(segment.note)
			provv_diesis_order =provv_diesis_order.concat(segment.diesis)
		})
		var provv_segment= {note:provv_note_order,diesis:provv_diesis_order}
		var success = DATA_calculate_rotate_Na_segment(provv_segment,value,rot_se)

		if(success){
			rot_success=true
			//overwrite new note/diesis list inside old structure
			//rotate only note, no s,e,L,end_note
			var from_n=-1
			if(rot_se){
				//rotate only note,s,e, no L,end_note
				from_n=-3
			}
			var index=0
			current_voice_data.data.segment_data.forEach((segment,segment_index)=>{
				segment.note.forEach((note,note_index)=>{
					if(note>from_n){
						//sub with old one
						segment.note[note_index]=provv_segment.note[index]
						segment.diesis[note_index]=provv_segment.diesis[index]
					}
					index++
				})
			})
		}
	}
	if(!rot_success){
		console.warn("Nothing to do")
		return
	}

	//calculate midi
	current_voice_data.data.midi_data=DATA_segment_data_to_midi_data(current_voice_data.data.segment_data,current_voice_data.neopulse)
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

//Na
function DATA_calculate_rotate_Na_segment(segment,value,rot_se){
	var rot_success=false
	//rotate notes
	var note_array_copy = JSON.parse(JSON.stringify(segment.note))
	var diesis_array_copy = JSON.parse(JSON.stringify(segment.diesis))

	//NO ROTATION OF L
	var note_array_rot=[]
	var diesis_array_rot=[]
	//rotate only note, no s,e,L,end_note
	var from_n=-1
	if(rot_se){
		//rotate only note,s,e, no L,end_note
		from_n=-3
	}

	note_array_copy.forEach((note,index_note)=>{
		if(note>from_n){
			note_array_rot.push(note)
			diesis_array_rot.push(diesis_array_copy[index_note])
		}
	})

	if(note_array_rot.length>1){
		rot_success=true
		for (let i = 0; i < value; i++) {
			note_array_rot.push(note_array_rot.shift())
			diesis_array_rot.push(diesis_array_rot.shift())
		}
		//reinsert
		var position=0
		note_array_copy.forEach((note,index_note)=>{
			if(note>from_n){
				note_array_copy[index_note]=note_array_rot[position]
				diesis_array_copy[index_note]=diesis_array_rot[position]
				position++
			}
		})
	}
	if(rot_success){
		segment.note=note_array_copy
		segment.diesis=diesis_array_copy
	}
	return rot_success
}

function DATA_rotate_Na_segment(voice_index,segment_index,value,rot_se){
	if(voice_index==-1 || segment_index==-1)return
	var data = DATA_get_current_state_point(true)
	var current_voice_data = data.voice_data.find(item=>{return item.voice_index==voice_index})
	if(current_voice_data==null){
		console.error("Operation error, data not found")
		return
	}
	if(current_voice_data.data.segment_data.length<=segment_index){
		console.error("Operation error, data not found")
		return
	}
	var rot_success = DATA_calculate_rotate_Na_segment(current_voice_data.data.segment_data[segment_index],value,rot_se)
	if(!rot_success){
		console.warn("Nothing to do")
		return
	}

	//calculate midi
	current_voice_data.data.midi_data=DATA_segment_data_to_midi_data(current_voice_data.data.segment_data,current_voice_data.neopulse)
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_rotate_Na_segment_column(segment_index,value,rot_se){
	if(segment_index==-1)return
	var data = DATA_get_current_state_point(true)
	var find_data=false
	var rot_success=false
	data.voice_data.forEach(voice=>{
		if(voice.blocked){
			find_data=true
			if(voice.data.segment_data.length<=segment_index){
				find_data=false
				return
			}
			var success = DATA_calculate_rotate_Na_segment(voice.data.segment_data[segment_index],value,rot_se)
			if(!success){
				return
			}
			rot_success=true

			//calculate midi
			voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
		}
	})
	if(!find_data){
		console.error("Operation error, data not found")
		return
	}
	if(!rot_success){
		console.warn("Nothing to do")
		return
	}

	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_rotate_Na_voice(voice_index,value,rot_se,by_segment){
	if(voice_index==-1)return
	var data = DATA_get_current_state_point(true)
	var current_voice_data = data.voice_data.find(item=>{return item.voice_index==voice_index})
	if(current_voice_data==null){
		console.error("Operation error, data not found")
		return
	}

	var rot_success=false
	if(by_segment){
		current_voice_data.data.segment_data.forEach((segment,segment_index)=>{
			var success = DATA_calculate_rotate_Na_segment(segment,value,rot_se)
			if(!success){
				return
			}
			rot_success=true
		})
	}else{
		//copy Note lines
		var provv_note_order = []
		var provv_diesis_order = []
		//no need to control end segment
		current_voice_data.data.segment_data.forEach((segment,segment_index)=>{
			provv_note_order = provv_note_order.concat(segment.note)
			provv_diesis_order =provv_diesis_order.concat(segment.diesis)
		})
		var provv_segment= {note:provv_note_order,diesis:provv_diesis_order}
		var success = DATA_calculate_rotate_Na_segment(provv_segment,value,rot_se)

		if(success){
			rot_success=true
			//overwrite new note/diesis list inside old structure
			//rotate only note, no s,e,L,end_note
			var from_n=-1
			if(rot_se){
				//rotate only note,s,e, no L,end_note
				from_n=-3
			}
			var index=0
			current_voice_data.data.segment_data.forEach((segment,segment_index)=>{
				segment.note.forEach((note,note_index)=>{
					if(note>from_n){
						//sub with old one
						segment.note[note_index]=provv_segment.note[index]
						segment.diesis[note_index]=provv_segment.diesis[index]
					}
					index++
				})
			})
		}
	}

	if(!rot_success){
		console.warn("Nothing to do")
		return
	}

	//calculate midi
	current_voice_data.data.midi_data=DATA_segment_data_to_midi_data(current_voice_data.data.segment_data,current_voice_data.neopulse)
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

//iNgm XXX
function DATA_rotate_iNgm_segment(voice_index,segment_index,value,rot_se){}
function DATA_rotate_iNgm_segment_column(segment_index,value,rot_se){}
function DATA_rotate_iNgm_voice(voice_index,value,rot_se,by_segment){}
//Ngm XXX
function DATA_rotate_Ngm_segment(voice_index,segment_index,value,rot_se){}
function DATA_rotate_Ngm_segment_column(segment_index,value,rot_se){}
function DATA_rotate_Ngm_voice(voice_index,value,rot_se,by_segment){}

/////////*****VERTICAL MIRROR (horizontal axis)*****/////////

//iNa
function DATA_v_mirror_iNa_segment(voice_index,segment_index){
	//same as v_mirror iN with option start note
	var option ={"axis_type": "starting_note"}
	DATA_v_mirror_Na_segment(voice_index,segment_index,option)
}

function DATA_v_mirror_iNa_segment_column(segment_index){
	//same as v_mirror iN with option start note
	var option ={"axis_type": "starting_note"}
	DATA_v_mirror_Na_segment_column(segment_index,option)
}

function DATA_v_mirror_iNa_voice(voice_index){
	//same as v_mirror iN with option start note
	var option ={"axis_type": "starting_note"}
	DATA_v_mirror_Na_voice(voice_index,option)
}

//Na
function DATA_calculate_v_mirror_Na_segment(segment,option){
	var max_note = TET*7 -1
	var outside_range=false
	var v_mirror_success=false
	var axis_defined=false

	var axis_note=null
	switch (option.axis_type) {
		case "user_note":
			axis_note=option.value
		break;
		case "starting_note":
			axis_note = segment.note.find(note=>{
				return note>=0
			})
		break;
		case "lowest_note":
			var provv=max_note+1
			segment.note.forEach(note=>{
				if(note>=0 && note<provv){
					provv=note
					axis_note=note
				}
			})
		break;
		case "highest_note":
			var provv=-1
			segment.note.forEach(note=>{
				if(note>=0 && note>provv){
					provv=note
					axis_note=note
				}
			})
		break;
	}
	if(axis_note!=null){
		axis_defined=true
		segment.note.forEach((note,note_index)=>{
			if(note>=0){
				v_mirror_success=true
				//calculating delta Na
				var dNa=(note-axis_note)*2
				note-=dNa
				if(note>max_note){
					note=max_note
					outside_range=true
				}
				if(note<0){
					note=0
					outside_range=true
				}
				segment.note[note_index]=note
			}
		})
	}

	return [axis_defined,v_mirror_success,outside_range]
}

function DATA_v_mirror_Na_segment(voice_index,segment_index,option){
	if(voice_index==-1 || segment_index==-1)return
	var data = DATA_get_current_state_point(true)
	var current_voice_data = data.voice_data.find(item=>{return item.voice_index==voice_index})
	if(current_voice_data==null){
		console.error("Operation error, data not found")
		return
	}
	if(current_voice_data.data.segment_data.length<=segment_index){
		console.error("Operation error, data not found")
		return
	}

	var [axis_defined,success,outside_range] = DATA_calculate_v_mirror_Na_segment(current_voice_data.data.segment_data[segment_index],option)
	if(!axis_defined){
		console.error("Operation error, axis not well defined")
		return
	}
	if(!success){
		console.warn("Nothing to do")
		return
	}

	if(outside_range){
		console.warn("ATTENTION: sum operation maxed note range")
	}
	current_voice_data.data.midi_data=DATA_segment_data_to_midi_data(current_voice_data.data.segment_data,current_voice_data.neopulse)
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_v_mirror_Na_segment_column(segment_index,option){
	if(segment_index==-1)return
	var data = DATA_get_current_state_point(true)
	var find_data=false
	var outside_range=false
	var v_mirror_success=false
	var max_note = TET*7 -1
	data.voice_data.forEach(voice=>{
		if(voice.blocked){
			find_data=true
			if(voice.data.segment_data.length<=segment_index){
				find_data=false
				return
			}
			var [axis_defined,success,out_r] = DATA_calculate_v_mirror_Na_segment(voice.data.segment_data[segment_index],option)
			if(!success){
				return
			}
			if(out_r)outside_range=true
			v_mirror_success=true
			//calculate midi
			voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
		}
	})
	if(!find_data){
		console.error("Operation error, data not found")
		return
	}
	if(!v_mirror_success){
		console.warn("Nothing to do")
		return
	}
	if(outside_range){
		console.warn("ATTENTION: sum operation maxed note range")
	}

	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_v_mirror_Na_voice(voice_index,option){
	if(voice_index==-1)return
	var data = DATA_get_current_state_point(true)
	var current_voice_data = data.voice_data.find(item=>{return item.voice_index==voice_index})
	if(current_voice_data==null){
		console.error("Operation error, data not found")
		return
	}

	//copy Note lines
	var provv_note_order = []
	var provv_diesis_order = []
	//no need to control end segment
	current_voice_data.data.segment_data.forEach((segment,segment_index)=>{
		provv_note_order = provv_note_order.concat(segment.note)
		provv_diesis_order =provv_diesis_order.concat(segment.diesis)
	})
	var v_mirror_success=false

	var provv_segment= {note:provv_note_order,diesis:provv_diesis_order}
	var [axis_defined,success,outside_range] = DATA_calculate_v_mirror_Na_segment(provv_segment,option)
	if(!axis_defined){
		console.error("Operation error, axis not well defined")
		return
	}
	if(success){
		v_mirror_success=true
		//overwrite new note/diesis list inside old structure
		var index=0
		current_voice_data.data.segment_data.forEach((segment,segment_index)=>{
			segment.note.forEach((note,note_index)=>{
				//not touching L,e,s,end_range
				if(note>-1){
					//sub with old one
					segment.note[note_index]=provv_segment.note[index]
					segment.diesis[note_index]=provv_segment.diesis[index]
				}
				index++
			})
		})
	}

	if(!v_mirror_success){
		console.warn("Nothing to do")
		return
	}
	if(outside_range){
		console.warn("ATTENTION: sum operation maxed note range")
	}
	current_voice_data.data.midi_data=DATA_segment_data_to_midi_data(current_voice_data.data.segment_data,current_voice_data.neopulse)
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

//iNgm XXX
function DATA_v_mirror_iNgm_segment(voice_index,segment_index){}
function DATA_v_mirror_iNgm_segment_column(segment_index){}
function DATA_v_mirror_iNgm_voice(voice_index){}
//Ngm XXX
function DATA_v_mirror_Ngm_segment(voice_index,segment_index,option){}
function DATA_v_mirror_Ngm_segment_column(segment_index,option){}
function DATA_v_mirror_Ngm_voice(voice_index,option){}

/////////*****HORIZONTAL MIRROR (vertical axis)*****/////////

//iT and N
function DATA_calculate_h_mirror_element_segment(segment){
	//reflect notes
	var note_array_copy = JSON.parse(JSON.stringify(segment.note))
	var diesis_array_copy = JSON.parse(JSON.stringify(segment.diesis))
	note_array_copy.pop()
	diesis_array_copy.pop()
	//check L
	note_array_copy.forEach((note,index)=>{
		if(note==-3){
			note_array_copy[index]=note_array_copy[index-1]
			note_array_copy[index-1]=-3
			diesis_array_copy[index]=diesis_array_copy[index-1]
			diesis_array_copy[index-1]=-3
		}
	})

	if(note_array_copy.length<2){
		return false
	}

	note_array_copy.reverse()
	note_array_copy.push(-4)
	diesis_array_copy.reverse()
	diesis_array_copy.push(true)
	segment.note=note_array_copy
	segment.diesis=diesis_array_copy
	//reflect iT and Fr
	//calculate iTs
	var iT_list = []

	var time_array_copy=JSON.parse(JSON.stringify(segment.time))
	var fraction_array_copy=JSON.parse(JSON.stringify(segment.fraction))
	for (let i = 0; i < time_array_copy.length-1 ; i++) {
		var pulse = time_array_copy[i].P
		var fraction = time_array_copy[i].F
		var next_pulse = time_array_copy[i+1].P
		var next_fraction = time_array_copy[i+1].F
		var current_fraction=fraction_array_copy.find(fraction=>{return fraction.stop>pulse})
		//calculate the dT value to be written
		var value = (next_pulse - pulse)  * current_fraction.D / current_fraction.N + (next_fraction - fraction)
		iT_list.push(value)
	}
	iT_list.reverse()
	fraction_array_copy.reverse()
	var start_pulse=0
	fraction_array_copy.forEach((fraction,index)=>{
		var Fr_range=fraction.stop-fraction.start
		fraction_array_copy[index].start=start_pulse
		fraction_array_copy[index].stop=start_pulse+Fr_range
		start_pulse+=Fr_range
	})

	time_array_copy=[]
	//prev_fraction = 0
	time_array_copy.push({"P":0,"F":0})
	iT_list.forEach((iT,index)=>{
		var current_pulse = time_array_copy[index].P
		var current_fraction = time_array_copy[index].F
		var current_Fr=fraction_array_copy.find(fraction=>{return fraction.stop>current_pulse})
		var d_pulse = (Math.floor((current_fraction+iT)/current_Fr.D))*current_Fr.N
		var next_pulse=current_pulse+d_pulse
		var next_fraction = (current_fraction+iT)%current_Fr.D
		time_array_copy.push({"P":next_pulse,"F":next_fraction})
	})
	segment.time=time_array_copy
	segment.fraction=fraction_array_copy
	return true
}

function DATA_h_mirror_element_segment(voice_index,segment_index){
	if(voice_index==-1 || segment_index==-1)return
	var data = DATA_get_current_state_point(true)

	var current_voice_data = data.voice_data.find(item=>{return item.voice_index==voice_index})
	if(current_voice_data==null){
		console.error("Operation error, data not found")
		return
	}

	if(current_voice_data.data.segment_data.length<=segment_index){
		console.error("Operation error, data not found")
		return
	}

	var segment=current_voice_data.data.segment_data[segment_index]
	var success = DATA_calculate_h_mirror_element_segment(segment)
	if(!success){
		console.warn("Nothing to do")
		return
	}

	//calculate midi
	current_voice_data.data.midi_data=DATA_segment_data_to_midi_data(current_voice_data.data.segment_data,current_voice_data.neopulse)
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_h_mirror_element_segment_column(segment_index){
	if(segment_index==-1)return
	var data = DATA_get_current_state_point(true)
	var find_data=false
	var h_mirror_success=false
	data.voice_data.forEach(voice=>{
		if(voice.blocked){
			find_data=true
			if(voice.data.segment_data.length<=segment_index){
				find_data=false
				return
			}
			var segment=voice.data.segment_data[segment_index]
			var success = DATA_calculate_h_mirror_element_segment(segment)
			if(!success){
				return
			}
			h_mirror_success=true
			//calculate midi
			voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
		}
	})
	if(!find_data){
		console.error("Operation error, data not found")
		return
	}
	if(!h_mirror_success){
		console.warn("Nothing to do")
		return
	}

	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_h_mirror_element_voice(voice_index,by_segment){
	if(voice_index==-1)return
	var data = DATA_get_current_state_point(true)
	var current_voice_data = data.voice_data.find(item=>{return item.voice_index==voice_index})
	if(current_voice_data==null){
		console.error("Operation error, data not found")
		return
	}

	var h_mirror_success=false
	//mirror x segment
	current_voice_data.data.segment_data.forEach((segment,segment_index)=>{
		var success = DATA_calculate_h_mirror_element_segment(segment)
		if(!success){
			return
		}
		h_mirror_success=true
	})
	if(by_segment){
		//nothing else to do
	}else{
		//disconnect voice
		current_voice_data.blocked=false
		//mirror segments order
		if(current_voice_data.data.segment_data.length>1){
			current_voice_data.data.segment_data.reverse()
			h_mirror_success=true
		}
	}
	if(!h_mirror_success){
		console.warn("Nothing to do")
		return
	}
	//calculate midi
	current_voice_data.data.midi_data=DATA_segment_data_to_midi_data(current_voice_data.data.segment_data,current_voice_data.neopulse)
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

//iT
function DATA_calculate_h_mirror_iT_segment(segment){
	//reflect iT and Fr
	//calculate iTs
	var iT_list = []

	var time_array_copy=JSON.parse(JSON.stringify(segment.time))
	if(time_array_copy.length<3){
		return false
	}
	var fraction_array_copy=JSON.parse(JSON.stringify(segment.fraction))
	for (let i = 0; i < time_array_copy.length-1 ; i++) {
		var pulse = time_array_copy[i].P
		var fraction = time_array_copy[i].F
		var next_pulse = time_array_copy[i+1].P
		var next_fraction = time_array_copy[i+1].F
		var current_fraction=fraction_array_copy.find(fraction=>{return fraction.stop>pulse})
		//calculate the dT value to be written
		var value = (next_pulse - pulse)  * current_fraction.D / current_fraction.N + (next_fraction - fraction)
		iT_list.push(value)
	}
	iT_list.reverse()
	fraction_array_copy.reverse()
	var start_pulse=0
	fraction_array_copy.forEach((fraction,index)=>{
		var Fr_range=fraction.stop-fraction.start
		fraction_array_copy[index].start=start_pulse
		fraction_array_copy[index].stop=start_pulse+Fr_range
		start_pulse+=Fr_range
	})

	time_array_copy=[]
	//prev_fraction = 0
	time_array_copy.push({"P":0,"F":0})
	iT_list.forEach((iT,index)=>{
		var current_pulse = time_array_copy[index].P
		var current_fraction = time_array_copy[index].F
		var current_Fr=fraction_array_copy.find(fraction=>{return fraction.stop>current_pulse})
		var d_pulse = (Math.floor((current_fraction+iT)/current_Fr.D))*current_Fr.N
		var next_pulse=current_pulse+d_pulse
		var next_fraction = (current_fraction+iT)%current_Fr.D
		time_array_copy.push({"P":next_pulse,"F":next_fraction})
	})
	segment.time=time_array_copy
	segment.fraction=fraction_array_copy

	//mirror L structure too
	var note_array_copy=JSON.parse(JSON.stringify(segment.note))
	var diesis_array_copy=JSON.parse(JSON.stringify(segment.diesis))
	var max_index = note_array_copy.length-1
	//indexing L
	var L_index_list = []
	note_array_copy.forEach((note,index_note)=>{
		if(note==-3){
			L_index_list.push(index_note)
		}
	})
	//deleting L
	var copy_L_index_list=JSON.parse(JSON.stringify(L_index_list))
	copy_L_index_list.reverse()
	var copy_L_index_diesis=[]
	copy_L_index_list.forEach(index_note=>{
		note_array_copy.splice(index_note,1)
		copy_L_index_diesis.push(diesis_array_copy[index_note])
		diesis_array_copy.splice(index_note,1)
	})
	//mirroring index L
	var new_L_index_list = L_index_list.map(item=>{return max_index-item})
	new_L_index_list.reverse()
	copy_L_index_diesis.reverse()
	new_L_index_list.forEach((L_index,i)=>{
		note_array_copy.splice(L_index,0,-3)
		diesis_array_copy.splice(L_index,0,copy_L_index_diesis[i])
	})

	segment.note=note_array_copy
	segment.diesis=diesis_array_copy

	return true
}

function DATA_h_mirror_iT_segment(voice_index,segment_index){
	if(voice_index==-1 || segment_index==-1)return
	var data = DATA_get_current_state_point(true)

	var current_voice_data = data.voice_data.find(item=>{return item.voice_index==voice_index})
	if(current_voice_data==null){
		console.error("Operation error, data not found")
		return
	}

	if(current_voice_data.data.segment_data.length<=segment_index){
		console.error("Operation error, data not found")
		return
	}

	var success = DATA_calculate_h_mirror_iT_segment(current_voice_data.data.segment_data[segment_index])
	if(!success){
		console.warn("Nothing to do")
		return
	}

	//calculate midi
	current_voice_data.data.midi_data=DATA_segment_data_to_midi_data(current_voice_data.data.segment_data,current_voice_data.neopulse)
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_h_mirror_iT_segment_column(segment_index){
	if(segment_index==-1)return
	var data = DATA_get_current_state_point(true)
	var find_data=false
	var h_mirror_success=false
	data.voice_data.forEach(voice=>{
		if(voice.blocked){
			find_data=true
			if(voice.data.segment_data.length<=segment_index){
				find_data=false
				return
			}

			var segment=voice.data.segment_data[segment_index]
			var success = DATA_calculate_h_mirror_iT_segment(segment)
			if(!success){
				return
			}
			h_mirror_success=true

			//calculate midi
			voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
		}
	})
	if(!find_data){
		console.error("Operation error, data not found")
		return
	}
	if(!h_mirror_success){
		console.warn("Nothing to do")
		return
	}

	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_h_mirror_iT_voice(voice_index,by_segment){
	if(voice_index==-1)return
	var data = DATA_get_current_state_point(true)
	var current_voice_data = data.voice_data.find(item=>{return item.voice_index==voice_index})
	if(current_voice_data==null){
		console.error("Operation error, data not found")
		return
	}

	var h_mirror_success=false
	//mirror x segment
	current_voice_data.data.segment_data.forEach((segment,segment_index)=>{
		var success = DATA_calculate_h_mirror_iT_segment(segment)
		if(!success){
			return
		}
		h_mirror_success=true
	})
	if(by_segment){
		//nothing else to do
	}else{
		//disconnect voice
		current_voice_data.blocked=false

		//copy Note lines
		var original_note_list = []
		var original_diesis_list = []
		current_voice_data.data.segment_data.forEach((segment,segment_index)=>{
			original_note_list=original_note_list.concat(JSON.parse(JSON.stringify(segment.note)))
			original_diesis_list=original_diesis_list.concat(JSON.parse(JSON.stringify(segment.diesis)))
		})

		//mirror segments order
		if(current_voice_data.data.segment_data.length>1){
			current_voice_data.data.segment_data.reverse()
			h_mirror_success=true
		}

		//overwrite note order with old one
		current_voice_data.data.segment_data.forEach((segment,segment_index)=>{
			segment.note.forEach((note,note_index)=>{
				//moving L with iT XXX ??? XXX XXX XXX
				//if(note>-3){
					//sub with old one
				//}
				//not moving L with iT
				if(note>-4){
					//sub with old one
					var current_note=original_note_list.shift()
					var current_diesis=original_diesis_list.shift()
					if(current_note==-4){
						current_note=original_note_list.shift()
						current_diesis=original_diesis_list.shift()
					}
					segment.note[note_index]=current_note
					segment.diesis[note_index]=current_diesis
				}
			})
		})
	}

	if(!h_mirror_success){
		console.warn("Nothing to do")
		return
	}
	//calculate midi
	current_voice_data.data.midi_data=DATA_segment_data_to_midi_data(current_voice_data.data.segment_data,current_voice_data.neopulse)
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

//iNa
function DATA_calculate_h_mirror_iNa_segment(segment,mirror_se){
	//reflect i notes
	var diesis_array_copy = JSON.parse(JSON.stringify(segment.diesis))
	var note_array_copy = JSON.parse(JSON.stringify(segment.note))

	//rotate only note, no s,e,L,end_note
	var from_n=-1
	if(mirror_se){
		//no mirror L
		from_n=-3
	}

	var iNa_list=DATA_calculate_iNa_from_Na_list(note_array_copy)
	var iNa_list_mir=[]
	var diesis_array_mir=[]
	note_array_copy.forEach((note,index_note)=>{
		if(note>from_n){
			iNa_list_mir.push(iNa_list[index_note])
			diesis_array_mir.push(diesis_array_copy[index_note])
		}
	})
	if(iNa_list_mir.length<2){
		return false
	}

	//mirror
	iNa_list_mir.reverse()
	diesis_array_mir.reverse()

	if(!mirror_se){
		//last element is starting note
		var starting_note=iNa_list_mir.pop()
		var starting_diesis=diesis_array_mir.pop()
		iNa_list_mir.unshift(starting_note)
		diesis_array_mir.unshift(starting_diesis)
	}else{
		//find starting note
		var last_diesis=true
		var last_iNa=null
		note_array_copy.find((note,index_note)=>{
			if(note>-1){
				last_iNa=note
				last_diesis=diesis_array_copy[index_note]
				return true
			}
		})
		if(last_iNa!=null){
			//no note to mirror, only s,e
			iNa_list_mir.forEach((iNa,index_iNa)=>{
				if(iNa!="e" && iNa!="s"){

					var provv_iNa=iNa
					var provv_diesis = diesis_array_mir[index_iNa]
					iNa_list_mir[index_iNa]=last_iNa
					diesis_array_mir[index_iNa]=last_diesis
					last_iNa=provv_iNa
					last_diesis=provv_diesis
				}
			})
		}
	}

	//reinsert
	var position=0
	note_array_copy.forEach((note,index_note)=>{
		if(note>from_n){
			iNa_list[index_note]=iNa_list_mir[position]
			diesis_array_copy[index_note]=diesis_array_mir[position]
			position++
		}
	})

	note_array_copy=DATA_calculate_Na_from_iNa_list(iNa_list)

	segment.note=note_array_copy
	segment.diesis=diesis_array_copy
	return true
}

function DATA_h_mirror_iNa_segment(voice_index,segment_index,mirror_se){
	if(voice_index==-1 || segment_index==-1)return
	var data = DATA_get_current_state_point(true)
	var current_voice_data = data.voice_data.find(item=>{return item.voice_index==voice_index})
	if(current_voice_data==null){
		console.error("Operation error, data not found")
		return
	}
	if(current_voice_data.data.segment_data.length<=segment_index){
		console.error("Operation error, data not found")
		return
	}

	var success = DATA_calculate_h_mirror_iNa_segment(current_voice_data.data.segment_data[segment_index],mirror_se)
	if(!success){
		console.warn("Nothing to do")
		return
	}

	//calculate midi
	current_voice_data.data.midi_data=DATA_segment_data_to_midi_data(current_voice_data.data.segment_data,current_voice_data.neopulse)
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_h_mirror_iNa_segment_column(segment_index,mirror_se){
	if(segment_index==-1)return
	var data = DATA_get_current_state_point(true)
	var find_data=false
	var outside_range=false
	var h_mirror_success=false
	var max_note = TET*7 -1
	data.voice_data.forEach(voice=>{
		if(voice.blocked){
			find_data=true
			if(voice.data.segment_data.length<=segment_index){
				find_data=false
				return
			}

			var success = DATA_calculate_h_mirror_iNa_segment(voice.data.segment_data[segment_index],mirror_se)
			if(!success){
				return
			}
			h_mirror_success=true

			//calculate midi
			voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
		}
	})
	if(!find_data){
		console.error("Operation error, data not found")
		return
	}
	if(!h_mirror_success){
		console.warn("Nothing to do")
		return
	}
	// if(outside_range){
	// 	console.warn("ATTENTION: h_mirror operation outside note range")//inside DATA_calculate_Na_from_iNa_list
	// }

	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_h_mirror_iNa_voice(voice_index,mirror_se,by_segment){
	if(voice_index==-1)return
	var data = DATA_get_current_state_point(true)
	var current_voice_data = data.voice_data.find(item=>{return item.voice_index==voice_index})
	if(current_voice_data==null){
		console.error("Operation error, data not found")
		return
	}

	var h_mirror_success=false

	if(by_segment){
		current_voice_data.data.segment_data.forEach((segment,segment_index)=>{
			var success = DATA_calculate_h_mirror_iNa_segment(segment,mirror_se)
			if(!success){
				return
			}
			h_mirror_success=true
		})
	}else{
		//copy Note lines
		var provv_note_order = []
		var provv_diesis_order = []
		current_voice_data.data.segment_data.forEach((segment,segment_index)=>{
			var provv_note = JSON.parse(JSON.stringify(segment.note))
			var provv_diesis = JSON.parse(JSON.stringify(segment.diesis))
			provv_note.pop()
			provv_diesis.pop()
			provv_note_order = provv_note_order.concat(provv_note)
			provv_diesis_order =provv_diesis_order.concat(provv_diesis)
		})
		//adding end
		provv_note_order.push(-4)
		provv_diesis_order.push(true)

		var provv_segment= {note:provv_note_order,diesis:provv_diesis_order}
		var success = DATA_calculate_h_mirror_iNa_segment(provv_segment,mirror_se)
		if(success){
			h_mirror_success=true
			var from_n=-1
			if(mirror_se){
				//no mirror L
				from_n=-3
			}
			//overwrite new note/diesis list inside old structure
			current_voice_data.data.segment_data.forEach((segment,segment_index)=>{
				segment.note.forEach((note,note_index)=>{
					//not moving L
					if(note>from_n){
						//sub with old one
						var [current_note, current_diesis]= _jump_to_next(provv_segment.note,provv_segment.diesis,from_n)
						segment.note[note_index]=current_note
						segment.diesis[note_index]=current_diesis
					}
				})
			})
		}
	}

	if(!h_mirror_success){
		console.warn("Nothing to do")
		return
	}
	//calculate midi
	current_voice_data.data.midi_data=DATA_segment_data_to_midi_data(current_voice_data.data.segment_data,current_voice_data.neopulse)
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)

	function _jump_to_next(new_note_order,new_diesis_order,from_n){
		if(new_note_order.length==0){
			console.error("NOTE NOT FOUND")
			return [-1,true]
		}
		var current_note=new_note_order.shift()
		var current_diesis=new_diesis_order.shift()
		if(current_note>from_n){
			return [current_note,current_diesis]
		}else{
			return _jump_to_next(new_note_order,new_diesis_order,from_n)
		}
	}
}

//Na
function DATA_calculate_h_mirror_Na_segment(segment,mirror_se){
	//reflect notes
	var note_array_copy = JSON.parse(JSON.stringify(segment.note))
	var diesis_array_copy = JSON.parse(JSON.stringify(segment.diesis))
	var note_array_mir=[]
	var diesis_array_mir=[]
	//mirror only note, no s,e,L,end_note
	var from_n=-1
	if(mirror_se){
		//no mirror L
		from_n=-3
	}

	note_array_copy.forEach((note,index_note)=>{
		if(note>from_n){
			note_array_mir.push(note)
			diesis_array_mir.push(diesis_array_copy[index_note])
		}
	})

	if(note_array_mir.length<2){
		return false
	}

	//mirror
	note_array_mir.reverse()
	diesis_array_mir.reverse()

	//reinsert
	var position=0
	note_array_copy.forEach((note,index_note)=>{
		if(note>from_n){
			note_array_copy[index_note]=note_array_mir[position]
			diesis_array_copy[index_note]=diesis_array_mir[position]
			position++
		}
	})

	segment.note=note_array_copy
	segment.diesis=diesis_array_copy
	return true
}

function DATA_h_mirror_Na_segment(voice_index,segment_index,mirror_se){
	if(voice_index==-1 || segment_index==-1)return
	var data = DATA_get_current_state_point(true)
	var current_voice_data = data.voice_data.find(item=>{return item.voice_index==voice_index})
	if(current_voice_data==null){
		console.error("Operation error, data not found")
		return
	}
	if(current_voice_data.data.segment_data.length<=segment_index){
		console.error("Operation error, data not found")
		return
	}
	var success = DATA_calculate_h_mirror_Na_segment(current_voice_data.data.segment_data[segment_index],mirror_se)
	if(!success){
		console.warn("Nothing to do")
		return
	}

	//calculate midi
	current_voice_data.data.midi_data=DATA_segment_data_to_midi_data(current_voice_data.data.segment_data,current_voice_data.neopulse)
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_h_mirror_Na_segment_column(segment_index,mirror_se){
	if(segment_index==-1)return
	var data = DATA_get_current_state_point(true)
	var find_data=false
	var h_mirror_success=false
	data.voice_data.forEach(voice=>{
		if(voice.blocked){
			find_data=true
			if(voice.data.segment_data.length<=segment_index){
				find_data=false
				return
			}
			var success = DATA_calculate_h_mirror_Na_segment(voice.data.segment_data[segment_index],mirror_se)
			if(!success){
				return
			}
			h_mirror_success=true

			//calculate midi
			voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
		}
	})
	if(!find_data){
		console.error("Operation error, data not found")
		return
	}
	if(!h_mirror_success){
		console.warn("Nothing to do")
		return
	}
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_h_mirror_Na_voice(voice_index,mirror_se,by_segment){
	if(voice_index==-1)return
	var data = DATA_get_current_state_point(true)
	var current_voice_data = data.voice_data.find(item=>{return item.voice_index==voice_index})
	if(current_voice_data==null){
		console.error("Operation error, data not found")
		return
	}
	var h_mirror_success=false

	if(by_segment){
		current_voice_data.data.segment_data.forEach((segment,segment_index)=>{
			var success = DATA_calculate_h_mirror_Na_segment(voice.data.segment_data[segment_index],mirror_se)
			if(!success){
				return
			}
			h_mirror_success=true
		})
	}else{
		//copy Note lines
		var new_note_order = []
		var new_diesis_order = []
		current_voice_data.data.segment_data.forEach((segment,segment_index)=>{
			new_note_order = new_note_order.concat(JSON.parse(JSON.stringify(segment.note)))
			new_diesis_order =new_diesis_order.concat(JSON.parse(JSON.stringify(segment.diesis)))
		})

		//mirror all the notes
		if(new_note_order.length>2){
			new_note_order.reverse()
			new_diesis_order.reverse()
			h_mirror_success=true

			var from_n=-1
			if(mirror_se){
				//no mirror L
				from_n=-3
			}

			//overwrite note order with old one
			current_voice_data.data.segment_data.forEach((segment,segment_index)=>{
				segment.note.forEach((note,note_index)=>{
					//not moving L
					if(note>from_n){
						//sub with old one
						var [current_note, current_diesis]= _jump_to_next(new_note_order,new_diesis_order,from_n)
						segment.note[note_index]=current_note
						segment.diesis[note_index]=current_diesis
					}
				})
			})
		}
	}

	if(!h_mirror_success){
		console.warn("Nothing to do")
		return
	}
	//calculate midi
	current_voice_data.data.midi_data=DATA_segment_data_to_midi_data(current_voice_data.data.segment_data,current_voice_data.neopulse)
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)

	function _jump_to_next(new_note_order,new_diesis_order,from_n){
		if(new_note_order.length==0){
			console.error("NOTE NOT FOUND")
			return [-1,true]
		}
		var current_note=new_note_order.shift()
		var current_diesis=new_diesis_order.shift()
		if(current_note>from_n){
			return [current_note,current_diesis]
		}else{
			return _jump_to_next(new_note_order,new_diesis_order,from_n)
		}
	}
}

//iNgm XXX
function DATA_h_mirror_iNgm_segment(voice_index,segment_index){}
function DATA_h_mirror_iNgm_segment_column(segment_index){}
function DATA_h_mirror_iNgm_voice(voice_index,by_segment){}
//Ngm XXX
function DATA_h_mirror_Ngm_segment(voice_index,segment_index){}
function DATA_h_mirror_Ngm_segment_column(segment_index){}
function DATA_h_mirror_Ngm_voice(voice_index,by_segment){}


//PROVISIONAL  ----    TESTING

function DATA_PROVVISIONAL_sin_function_voice(voice_index,segment_index,low_Na,top_Na,D_x){
	var data = DATA_get_current_state_point(true)

	var current_voice_data = data.voice_data.find(item=>{
		return item.voice_index==voice_index
	})

	var current_segment_data=current_voice_data.data.segment_data[segment_index]

	current_segment_data.diesis=[]
	current_segment_data.note=[]
	var Ls = current_segment_data.time.pop().P
	current_segment_data.time=[]
	var fraction= current_segment_data.fraction[0]
	fraction.N=1
	fraction.D=1
	fraction.stop=Ls
	current_segment_data.fraction=[]
	current_segment_data.fraction[0]=fraction


	var pulse=0
	var current_Na=low_Na
	for (let i = 0; i < Ls; i++) {
		current_segment_data.time.push({"P":pulse,"F":0})
		current_segment_data.diesis.push(true)
		current_segment_data.note.push(current_Na)
		current_Na=low_Na+Math.floor((top_Na-low_Na)*Math.sin(i*Math.PI/(6*D_x)))
		pulse++
	}

	//last one
	current_segment_data.time.push({"P":Ls,"F":0})
	current_segment_data.diesis.push(true)
	current_segment_data.note.push(-4)

	console.log(current_segment_data)

	current_voice_data.data.midi_data=DATA_segment_data_to_midi_data(current_voice_data.data.segment_data,current_voice_data.neopulse)
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_PROVVISIONAL_ramp_function_voice(voice_index,segment_index,D_iNa){
	var data = DATA_get_current_state_point(true)

	var current_voice_data = data.voice_data.find(item=>{
		return item.voice_index==voice_index
	})

	var current_segment_data=current_voice_data.data.segment_data[segment_index]

	var iNa=0
	for (let i = 0; i < current_segment_data.note.length-1; i++) {
		var current_Na=current_segment_data.note[i]
		if(current_Na>=0){
			current_Na+=iNa
			iNa+=D_iNa
			if(current_Na>=TET*7){
				current_Na=TET*7-1
				//change direction
				D_iNa=-Math.abs(D_iNa)
			}
			if(current_Na<0){
				current_Na=0
				//change direction
				D_iNa=Math.abs(D_iNa)
			}
			current_segment_data.note[i]=current_Na
		}
	}
	current_voice_data.data.midi_data=DATA_segment_data_to_midi_data(current_voice_data.data.segment_data,current_voice_data.neopulse)
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

