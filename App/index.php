<?php
if (session_status() === PHP_SESSION_NONE) {
	session_start();
}
$_SESSION["current_composition_id"] = "";
unset($_SESSION["current_composition_id"]);

//banzai cache
// header("Cache-Control: no-store, no-cache, must-revalidate"); // HTTP/1.1
// header("Cache-Control: post-check=0, pre-check=0", false);
// header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
// header("Pragma: no-cache"); // HTTP/1.0
// header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");

require('../wp-load.php' );
$wp_user = wp_get_current_user();
$wp_user_id = $wp_user->ID;

get_header();

$data = file_get_contents('index.html');
$parts = explode('<!--php-->', $data);
echo $parts[1];//css
echo $parts[3];//body + scripts
//echo $parts[4];//scripts

get_footer();
?>

