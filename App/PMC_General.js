function PMC_select_zoom_sound(selector){
	var r = document.querySelector(':root')
	if(selector.value!="0"){
		PMC_zoom_y = Number(selector.value)
	}else{
		//calculate fit value
		var container=document.querySelector(".App_PMC_sound_line")
		var height = container.clientHeight
		var available_height = height-nuzic_block//spacer
		PMC_zoom_y = available_height/((TET*7-0.5)*nuzic_block)
		//console.log(PMC_zoom_y)
	}

	//setting a variable value
	r.style.setProperty('--PMC_y_block', 'calc(var(--RE_block) * '+PMC_zoom_y+')')

	PMC_redraw()//needed??? XXX probably
	PMC_redraw_sound_line()
	PMC_write_sound_line_values()
	PMC_redraw_Nabc()
}


let PMC_height=0
//CONTROL IF RESIZED PMC AND Y ZOOM ALL
const resize_PMC = new ResizeObserver(function(entries) {
	// since we are observing only a single element, so we access the first element in entries array
	let rect = entries[0].contentRect

	// current width & height
	//let width = rect.width
	let current_height = rect.height
	if(current_height!=PMC_height){
		PMC_height = current_height
		//verify if zoom == all an if so resize it
		if(PMC_read_zoom_sound()==0){
			//console.log("need to redraw")
			PMC_set_zoom_sound(0)
			PMC_redraw()//needed??? XXX probably
			PMC_redraw_sound_line()
			PMC_write_sound_line_values()
		}
	}
})

// start observing for resize
resize_PMC.observe(document.querySelector(".App_PMC"))

function PMC_select_zoom_time(selector){
	PMC_zoom_x = Number(selector.value)
	var r = document.querySelector(':root')
	//setting a variable value
	r.style.setProperty('--PMC_x_block', 'calc(var(--RE_block) * '+PMC_zoom_x+')')
	PMC_redraw()
}

function PMC_read_zoom_sound(){
	var selector= document.querySelector("#App_PMC_select_zoom_sound")
	return selector.value
}

function PMC_set_zoom_sound(value){
	var selector= document.querySelector("#App_PMC_select_zoom_sound")

	if(value==0){
		selector.value=0
		//var container=document.querySelector(".App_PMC_sound_line")
		//var height = container.clientHeight
		//DANGER XXX
		//App_Box - App_MMap - App_BNav - 28 (sequence )- 6 (borders)- 28 (C and S)* 2 - 13 (horiz scrollbar) - XXX (single RE voice) - 28 * 1.5 (time bar)
		//var height = document.querySelector(".App_Box").clientHeight - (document.querySelector(".App_MMap").clientHeight+document.querySelector(".App_BNav").clientHeight + nuzic_block * 4.5 + 6 + 13)

		var container=document.querySelector(".App_PMC_sound_line")
		var height = container.clientHeight
		var available_height = height-nuzic_block//spacer
		PMC_zoom_y = available_height/((TET*7-0.5)*nuzic_block)
		//console.log("hello "+height)
	}else{
		selector.value=value
		PMC_zoom_y = value
	}
	//ATT if PMC not shown...... it doesnt actualize All zoom!!!
	//console.log(PMC_zoom_y)

	//this doesn't trigger  PMC_select_zoom_sound
	var r = document.querySelector(':root')
	//setting a variable value
	r.style.setProperty('--PMC_y_block', 'calc(var(--RE_block) * '+PMC_zoom_y+')')
}

function PMC_set_zoom_time(value){
	var selector= document.querySelector("#App_PMC_select_zoom_time")
	selector.value=value
	PMC_zoom_x = value
	//this doesnt trigger  PMC_select_zoom_time
	var r = document.querySelector(':root')
	//setting a variable value
	r.style.setProperty('--PMC_x_block', 'calc(var(--RE_block) * '+PMC_zoom_x+')')
}

function PMC_redraw(){
	//PMC_redraw_sound_line()  //not here, only if zoom change
	var voice_data = DATA_get_selected_voice_data()

	PMC_load_selected_voice_header(voice_data)
	//if voice_datachanged....
	PMC_redraw_time_line(voice_data)

	//redraw svg
	PMC_redraw_all_voices_svg()
	//redraw selected voice
	PMC_redraw_selected_voice() //after painting all (visible) voices canvas bc it draw over that
	PMC_redraw_selected_voice_sequence() //non qui??? XXX
	//PMC_reset_action_pmc()after
	//redraw copy RE on PMC

	var n_box = PMC_x_length(voice_data)
	PMC_set_H_scrollbar_step_N(n_box) // here??? XXX

	PMC_write_S_line_values()
	PMC_write_C_line_values()

	PMC_checkScroll_H()
	PMC_checkScroll_V()
	
	PMC_reset_playing_notes()
	PMC_reset_progress_line()
	PMC_reset_action_pmc()
}


function PMC_redraw_all_voices_svg(){
	//draw all visible voices
	var voice_data = DATA_get_PMC_visible_voice_data() //every voice minus selected one
	var note_number = TET*7
	var height_string = 'calc('+TET+' * 7 * var(--PMC_y_block) )'
	//generic width of IDEA
	var width_string = "calc(var(--PMC_x_block) * "+Li+" + var(--RE_block))"

	var offset = nuzic_block
	var c_height = nuzic_block * PMC_zoom_y * TET * 7 + nuzic_block/2
	var delta_y = nuzic_block * PMC_zoom_y

	// using SVG
	PMC_svg_all_voices.style.height=height_string
	PMC_svg_all_voices.style.width=width_string
	PMC_svg_all_voices.innerHTML= ""
	var svg_string=""
	voice_data.forEach(item=>{
		//voice widths
		var D_NP = item.neopulse.D
		var N_NP = item.neopulse.N
		var neopulse_moltiplicator =  N_NP/D_NP
		var delta = nuzic_block * PMC_zoom_x * neopulse_moltiplicator
		var voice_color_light = eval(item.color+"_light")
		var array_voice_positions = PMC_segment_data_to_obj_data(item.data.segment_data,item.neopulse)

		//positioning and resizing main block
		var first_note = array_voice_positions.find(object=>{
				return object.nzc_note>-1
		})
		//draw objects
		var starting_pos = item.e_sound
		if(first_note!=null){
			starting_pos = first_note.nzc_note
		}

		var last_nzc_note=starting_pos
		var last_pos=starting_pos
		array_voice_positions.forEach(object=>{
			var nzc_note = object.nzc_note
			if (nzc_note ==-1){
				//silence == nothing
				last_nzc_note=nzc_note
			}else if(nzc_note ==-2){
				//element box positioned in line last note
				var note = last_pos
				var x1 = offset+object.pulse_float*delta
				var x2 = x1 + object.duration *delta
				var y = delta_y*(note_number-note)-delta_y+1
				if(x2>2)x2-=1//little space
				svg_string+=`
					<rect x="${x1}" y="${y}" width="${x2-x1}" height="${delta_y-2}" fill="var(--Nuzic_white)" stroke="${voice_color_light}" stroke-width="2"></rect>
				`
				last_nzc_note=nzc_note
			}else if(nzc_note ==-4){
				//nothing
				last_nzc_note=nzc_note
			} else if(nzc_note ==-3){
				//ligadura, need to print something
				if(last_nzc_note!=-1 && last_nzc_note!=-4){
					var note = last_pos
					var x1 = offset+object.pulse_float*delta
					var x2 = x1 + object.duration *delta
					var y = delta_y*(note_number-note)-delta_y/2
					if(x2>2)x2-=1//little space
					x1-=1 //connect with previous

					if(last_nzc_note==-2){
						//modify last
						svg_string+=`
							<line x1="${x1-1}" y1="${y}" x2="${x2}" y2="${y}" stroke="${voice_color_light}" stroke-width="${delta_y}"></line>
							<line x1="${x1-1}" y1="${y}" x2="${x2}" y2="${y}" stroke="var(--Nuzic_white)" stroke-width="${delta_y-4}"></line>
							<line x1="${x2}" y1="${y-delta_y/2}" x2="${x2}" y2="${y+delta_y/2}" stroke="${voice_color_light}" stroke-width="2"></line>
						`
					}else{
						svg_string+=`
							<line x1="${x1}" y1="${y}" x2="${x2}" y2="${y}" stroke="${voice_color_light}" stroke-width="${delta_y}"></line>
						`
					}
				}
			} else {
				var note = nzc_note
				var x1 = offset+object.pulse_float*delta
				var x2 = x1 + object.duration *delta
				var y = delta_y*(note_number-note)-delta_y/2
				if(x2>2)x2-=1//little space

				svg_string+=`
					<line x1="${x1}" y1="${y}" x2="${x2}" y2="${y}" stroke="${voice_color_light}" stroke-width="${delta_y}"></line>
				`
				last_nzc_note=nzc_note
				last_pos=nzc_note
			}
		})
	})
	PMC_svg_all_voices.innerHTML=svg_string
}

function PMC_redraw_selected_voice(){
	PMC_reset_selection()
	//Selected voice
	var voice_data = DATA_get_selected_voice_data()
	var D_NP = voice_data.neopulse.D
	var N_NP = voice_data.neopulse.N
	var neopulse_moltiplicator =  N_NP/D_NP
	var total_length = PMC_x_length(voice_data)

	var voice_color = eval(voice_data.color)
	var voice_color_light = eval(voice_data.color+"_light")

	var voice_instrument = voice_data.instrument
	var note_lines = TET * 7 -0.5
	var height_string = 'calc('+note_lines+' * var(--PMC_y_block) + var(--RE_block))'
	//give extra  PMC_x_bloks
	var width_string = "calc(var(--PMC_x_block) * "+(total_length-0.5)+" + var(--RE_block))"

	var delta = nuzic_block * PMC_zoom_x * neopulse_moltiplicator
	//var offset = (block + delta)/2 //2 half block
	var offset = nuzic_block
	var c_height = nuzic_block * PMC_zoom_y * note_lines + nuzic_block
	var c_width = nuzic_block + nuzic_block * PMC_zoom_x * total_length - 0.5 * nuzic_block * PMC_zoom_x


	//function timer -> Testing performance
	var  t1, t2,
	startTimer = function() {
		t1 = new Date().getTime()
		return t1
	}
	stopTimer = function() {
		t2 = new Date().getTime()
		return t2 - t1
	}

	//segment svg
	//startTimer()
	PMC_svg_segment.style.height=height_string
	PMC_svg_segment.style.width=width_string
	var segment_position = PMC_segment_data_to_segment_position(voice_data.data.segment_data)
	PMC_svg_segment.innerHTML=""
	var svg_string=""
	segment_position.forEach(item=>{
		var x = offset+item*delta//-0.5
		svg_string+=`
				<line x1="${x}" y1="0" x2="${x}" y2="${c_height}" stroke="${nuzic_dark}" stroke-width="2"></line>
		`
	})
	PMC_svg_segment.innerHTML=svg_string
	//console.log('Rendering of SVG segments in ' + stopTimer() + 'ms')

	//draw black points
	var note_number = TET*7
	var delta_y = nuzic_block * PMC_zoom_y

	var dots_N = PMC_x_dots(voice_data)

	//draw only if necessary
	var dots_attributes = total_length+"*"+delta+"*"+PMC_zoom_y
	if(dots_attributes==PMC_canvas_dots_container.getAttribute("value")){
		//no need to redraw
		console.log("canvas dots not redraw")
	}else{
		//tiling canvas with CSS
		startTimer()
		PMC_canvas_dots_container.style.height=height_string
		PMC_canvas_dots_container.style.width="calc(var(--PMC_x_block) * "+(total_length-5)+")"
		PMC_canvas_dots_container.style.backgroundSize=delta+"px"
		//PMC_canvas_dots_container.style.backgroundPositionX= "calc(var(--RE_block) - var(--PMC_x_block) * 0.5)"
		PMC_canvas_dots_container.style.backgroundPositionX= (delta*0.5)+"px"
		//PMC_canvas_dots_container.style.backgroundPositionX= "calc(var(--RE_block) - "+delta+" * 0.5px)"
		//create a canvas as a base tile
		//var PMC_canvas_dots_tile_line= document.createElement('canvas')
		PMC_canvas_dots.style.height=height_string
		PMC_canvas_dots.style.width=delta+"px"

		var ctx_p = PMC_canvas_dots.getContext('2d')
		PMC_canvas_dots.height= c_height
		PMC_canvas_dots.width = delta

		var x = 0.5*delta
		for (let j = 0; j < note_number; j++) {
			var y = delta_y/2+j*delta_y
			ctx_p.beginPath()
			//ctx_p.fillRect(x-1,y-1,2,2); //faaster
			ctx_p.fillRect(x-0.5,y-0.5,1,1);
			//ctx_p.arc(x, y, 1, 0, 2 * Math.PI, true); circle
			//ctx_p.strokeStyle = nuzic_red
			ctx_p.stroke()
		}


		PMC_canvas_dots_container.style.backgroundImage='url('+PMC_canvas_dots.toDataURL("image/png")+')'

		console.log('Rendering of TILED CANVAS DOTS CSS trick in ' + stopTimer() + 'ms')
	}

	//draw scale??? XXX
	//canvas resize/cleanup
	//PMC_canvas_scale.style.height=height_string
	//PMC_canvas_scale.style.width=width_string

	//Draw selected voice objects
	var div_selected_voice = PMC_canvas_container.querySelector("#App_PMC_selected_voice")
	var selected_voice_height_string = 'calc('+TET+' * 7 * var(--PMC_y_block) )'
	div_selected_voice.style.height=selected_voice_height_string
	div_selected_voice.style.width=width_string
	div_selected_voice.innerHTML=""
	var PMC_global_variables = DATA_read_PMC_global_variables()
	var array_voice_data = PMC_segment_data_to_obj_data(voice_data.data.segment_data,voice_data.neopulse)

	//faster with strings but not worth it
	//generate array texts and measures needed in PMC objects

	//draw objects
	startTimer()
	//create objects
	array_voice_data.forEach(object=>{
		var div_object_base = document.createElement('div') //XXX button maybe???
		div_object_base.classList.add('App_PMC_selected_voice_object')
		div_selected_voice.appendChild(div_object_base)
	})

	var objects_list = [...div_selected_voice.querySelectorAll(".App_PMC_selected_voice_object")]
	//add basic subdiv
	objects_list.forEach(object=>{
		var div = document.createElement('div')
		div.classList.add('App_PMC_selected_voice_object_N')
		object.appendChild(div)
	})
	var N_obj_list = [...div_selected_voice.querySelectorAll(".App_PMC_selected_voice_object_N")]

	//positioning and resizing main block
	var first_note = array_voice_data.find(object=>{
			return object.nzc_note>-1
	})
	var starting_pos = voice_data.e_sound
	if(first_note!=null){
		starting_pos = first_note.nzc_note
	}
	var prev_pos=starting_pos

	array_voice_data.forEach((object,index)=>{
		var nzc_note = object.nzc_note
		var note = 0
		if (nzc_note ==-1 || nzc_note ==-2 || nzc_note==-3){
			//previous position
			note = prev_pos
		}else{
			note = Number(nzc_note)
		}
		var y = delta_y*(note_number-note)-delta_y
		objects_list[index].style.width= object.duration *delta+"px"
		objects_list[index].style.marginTop= y+"px"
		index++
		prev_pos=note
	})


	//write pulse number
	if(PMC_global_variables.vsb_T.show){
		//Add green points show number

		//pulse
		var pulse_type = PMC_global_variables.vsb_T.type
		var time_text_list=[]

		switch (pulse_type) {
			case "Ta":
				array_voice_data.forEach(data=>{time_text_list.push(data.Ta_text)})
			break;
			case "Tm":
				var neopulse = N_NP!=1 || D_NP!=1

				if(neopulse){
					array_voice_data.forEach(data=>{time_text_list.push(no_value)})
				}else{
					var compas_data = DATA_get_compas_sequence()
					var last_compas= compas_data.slice(-1)[0].compas_values
					var compas_length_value = last_compas[0]+last_compas[1]*last_compas[2]
					var prev_comp = null
					var new_compas_shown = false
					var prev_pulse = null

					//it int_p inside a compas
					//take into account that pulse is part of a segment un x position
					//No problem using Pa, no compas if neopulse!!! Psg===Pa
					array_voice_data.forEach((data,index)=>{
						if(data.Tsa_data.P <compas_length_value && compas_length_value!=0){
							//element inside a module
							var compas_type = compas_data.filter(compas=>{
								return compas.compas_values[0] <= data.Tsa_data.P
							})

							var current_compas_type = compas_type.pop()

							var current_compas_number = compas_type.reduce((prop,item)=>{
								return item.compas_values[2]+prop
							},0)

							var dP = data.Tsa_data.P - current_compas_type.compas_values[0]
							var mod = current_compas_type.compas_values[1]
							var dC = Math.floor(dP/mod)
							var resto = dP
							if(dC!=0) resto = dP%mod
							current_compas_number+= dC
							//  <p>This text contains <sup>superscript</sup> text.</p>

							//warning first element
							//verify if it has module, if not read previous value
							var showcompas = false
							var showpulse = true
							//here retrive value
							if(prev_comp!=null){
								if(prev_pulse==resto)showpulse=false
								if(prev_comp==current_compas_number){
									if(!new_compas_shown){
										showcompas=true
										new_compas_shown=true
									}
								}else{
									new_compas_shown=false
									if(data.nzc_note>=0){
										showcompas=true
										new_compas_shown=true
									}
								}
							}else{
								if(data.nzc_note>=0){
									showcompas=true
									new_compas_shown=true
								}
							}
							var string=""

							if(showpulse || showcompas){
								string = resto
							}

							if(data.Tsa_data.F!=0){
								string += "."+data.Tsa_data.F
							}

							if(showcompas){
								//stringT += "<sup>"+current_compas_number+"</sup>"
								string += RE_string_to_superscript(current_compas_number.toString())
							}

							prev_comp = current_compas_number
							//if s , e , L doesnt register prev compas number???
							prev_pulse = resto
							time_text_list.push(string)
						}else{
							//outside or no compasses
							time_text_list.push(no_value)
						}
					})
				}
			break;
			case "Ts":
				array_voice_data.forEach(data=>{time_text_list.push(data.Ts_text)})
			break;
		}

		objects_list.forEach((object,index)=>{
			// var div_text = document.createElement('div')
			// div_text.classList.add("App_PMC_selected_voice_object_T")
			// object.appendChild(div_text)

			var div_1 = document.createElement('div')
			div_1.classList.add("App_PMC_selected_voice_object_dot_start_green")
			object.appendChild(div_1)

			var div_2 = document.createElement('div')
			div_2.classList.add("App_PMC_selected_voice_object_dot_end_green")
			object.appendChild(div_2)

			//write everithing
			var nzc_note = array_voice_data[index].nzc_note
			if (nzc_note ==-1 || nzc_note ==-2 || nzc_note==-3){
				//previous position
				//div_text.classList.add("hidden")
				div_1.classList.add("hidden")
				div_2.classList.add("hidden")
			}else{
				//div_1.innerHTML= array_voice_data[index].Time_text
				//Hiding roule
				div_1.innerHTML= time_text_list[index]
				if(time_text_list[index].length>3)div_1.classList.add("long_text")
			}
			index++
		})
	}else{
		//add little green points
		objects_list.forEach((object,index)=>{
			var div_1 = document.createElement('div')
			div_1.classList.add("App_PMC_selected_voice_object_dot_start_unselected")
			object.appendChild(div_1)

			var div_2 = document.createElement('div')
			div_2.classList.add("App_PMC_selected_voice_object_dot_end_unselected")
			object.appendChild(div_2)

			index++
		})
	}

	//add iT object and write iT number
	if(PMC_global_variables.vsb_iT.show){
		objects_list.forEach(object=>{
			var div = document.createElement('div')
			div.classList.add('App_PMC_selected_voice_object_iT')
			object.appendChild(div)
		})
		var iT_obj_list = [...div_selected_voice.querySelectorAll(".App_PMC_selected_voice_object_iT")]
		array_voice_data.forEach((object,index)=>{
			var nzc_note = object.nzc_note
			//PMC_global_variables.vsb_iT.type  ONLY 1 OPTION
			if (nzc_note ==-1){
				iT_obj_list[index].style.backgroundImage = "none"
			} else {
				iT_obj_list[index].innerHTML=object.iT
				//draw border in objects
				N_obj_list[index].style.borderBottom = "2px solid var(--Nuzic_yellow)"
			}
			index++
		})
	}else{
		//create for alignment od iS
		objects_list.forEach(object=>{
			var div = document.createElement('div')
			div.classList.add('App_PMC_selected_voice_object_iT')
			div.style.backgroundImage = "none"
			object.appendChild(div)
		})
	}
	//silence == -1
	var last_note = -1
	//write note number

	var prev_reg = 99
	if(PMC_global_variables.vsb_N.show){
		var string_N_list = PMC_calculate_object_N_text(array_voice_data,PMC_global_variables,voice_data.neopulse)

		array_voice_data.forEach((object,index)=>{
			var nzc_note = object.nzc_note
			if (nzc_note ==-1){
				//no color
				last_note=nzc_note
			}else if(nzc_note ==-2){
				N_obj_list[index].style.color = voice_color
				N_obj_list[index].innerHTML="e"
				last_note=nzc_note
			}else if(nzc_note ==-3){
				//follow last note
				N_obj_list[index].style.color = voice_color
				N_obj_list[index].innerHTML="L"
			} else {
				//note
				var note_type = PMC_global_variables.vsb_N.type
				N_obj_list[index].addEventListener("mousedown",()=>{PMC_play_this_note_number(nzc_note,voice_instrument)})
				N_obj_list[index].innerHTML=string_N_list[index]
				last_note=nzc_note
			}
		})
	}else{
		array_voice_data.forEach((object,index)=>{
			var nzc_note = object.nzc_note
			if (nzc_note ==-1){
				//no color
				last_note=nzc_note
			}else if(nzc_note ==-2){
				last_note=nzc_note
			}else if(object.nzc_note ==-3){
			} else {
				//note
				var note = nzc_note
				last_note=nzc_note
			}
		})
	}

	//write iN number
	if(PMC_global_variables.vsb_iN.show){
		objects_list.forEach(object=>{
			var div = document.createElement('div')
			div.classList.add('App_PMC_selected_voice_object_iN')
			object.appendChild(div)
		})
		var iN_obj_list = [...div_selected_voice.querySelectorAll(".App_PMC_selected_voice_object_iN")]
		//type traduction
		var prev_note = null
		var voice_started = false
		var value_array = []

		var iN_type = PMC_global_variables.vsb_iN.type
		switch(iN_type){
			case 'iNa':
				array_voice_data.forEach((item,index)=>{
					var note = item.nzc_note
					if (note ==-1 || note ==-2){
						value_array.push({"value":0,"string":""})
					}else if(note ==-3){
						value_array.push({"value":0,"string":""})
					} else {
						//note
						if(voice_started){
							var value = note-prev_note
							value_array.push({"value":value,"string":value})
						}else {
							voice_started = true
							value_array.push({"value":0,"string":""})
						}
						prev_note=note
					}
				})
			break;
			case 'iNm':
				array_voice_data.forEach((item,index)=>{
					var note = item.nzc_note
					if (note ==-1 || note ==-2){
						value_array.push({"value":0,"string":""})
					}else if(note ==-3){
						value_array.push({"value":0,"string":""})
					} else {
						//note
						if(voice_started){
							var value = note-prev_note
							var reg = Math.floor(value/TET)
							var resto = value%TET
							var neg = false
							if(value<0){
								neg = true
								//console.log(int_n+"  resto "+resto)
								resto = Math.abs(resto)
								if(resto!=0){
									reg= Math.abs(reg)-1
								}else{
									reg= Math.abs(reg)
								}
							}
							var string_iN=""
							if(neg){
								string_iN="-"+resto//+"r"+reg
							}else{
								string_iN=resto//+"r"+reg
							}
							if(reg!=0)string_iN=string_iN+"r"+reg
							value_array.push({"value":value,"string":string_iN})
						}else {
							voice_started = true
							value_array.push({"value":0,"string":""})
						}
						prev_note=note
					}
				})
			break;
			case 'iNgm':
				var prev_diesis=true
				var prev_scale=null
				array_voice_data.forEach((item,index)=>{
					var note = item.nzc_note
					var current_diesis = item.diesis

					var current_P_seg_global = item.Tsa_data.P
					var current_F = item.Tsa_data.F
					//find correct scale

					//find Pa_equivalent and verify if inside a scale
					// var current_fraction = segment_data.fraction.find(fraction=>{return fraction.stop>current_P})
					// var frac_p = current_fraction.N/current_fraction.D
					var frac_p = item.frac_moltiplicator
					var Pa_equivalent = current_P_seg_global * N_NP/D_NP + current_F* frac_p * N_NP/D_NP
					var current_scale = DATA_get_Pa_eq_scale(Pa_equivalent)

					if(current_scale!=null){
						//element inside a scale module
						if(note<0){
							//no note
							value_array.push({"value":0,"string":""})
						}else{
							var write_iN = true
							var value = note-prev_note
							if(prev_scale!=null){
								if(prev_scale.acronym!=current_scale.acronym || prev_scale.rotation!=current_scale.rotation || prev_scale.starting_note!=current_scale.starting_note){
									// different scales
									write_iN=false
								}
							}else{
								write_iN=false
							}
							prev_scale=current_scale
							if(write_iN){
								//calculate note interval and make a traduction in current scale //PREV OR CURRENT SCALE???
								var string_iN = BNAV_calculate_scale_interval_string(prev_note,prev_diesis,current_scale,note,current_diesis,current_scale)
								value_array.push({"value":value,"string":string_iN})
							}else{
								value_array.push({"value":value,"string":no_value})
							}
							prev_note=note
							prev_diesis=current_diesis
						}
					}else{
						//outside defined scales
						var value = 0
						if(note>=0){
							value = note-prev_note
							prev_note=note
						}

						value_array.push({"value":value,"string":no_value})
					}
				})
			break;
		}

		array_voice_data.forEach((item,index)=>{
			var note = item.nzc_note
			if (note ==-1){
				iN_obj_list[index].classList.add("dotted")
				iN_obj_list[index].style.borderTop = "2px dashed var(--Nuzic_pink)"
				iN_obj_list[index].style.height ="calc(var(--PMC_y_block) * 0.5)"
				iN_obj_list[index].style.width ="100%"
				iN_obj_list[index].style.backgroundImage ="-webkit-linear-gradient(right, transparent,transparent)"
				iN_obj_list[index].style.left="0px"
				iN_obj_list[index].style.top="calc(var(--PMC_y_block) * -0.5 - 35px - 2px)"
			}else if(note ==-2 ){
			}else if(note ==-3){
			}else{
				//note
				var value = value_array[index].value
				if(value!=0){
					iN_obj_list[index].innerHTML = value_array[index].string
					iN_obj_list[index].style.lineHeight = "calc(var(--PMC_y_block) * "+Math.abs(value)+")"
					iN_obj_list[index].style.height ="calc(var(--PMC_y_block) * "+Math.abs(value)+")"
					if(value<0){
						//repositioning
						iN_obj_list[index].style.top ="calc(-35px - var(--PMC_y_block) * "+(0.5 - value)+")"
					}
				}
			}
		})
	}

	//repaint selected voice in visible voices canvas
	PMC_redraw_selected_voice_svg(true,voice_data)

	console.log('Rendering of PMC selected voice objects in ' + stopTimer() + 'ms')
}

function PMC_calculate_object_N_text(object_list,PMC_global_variables,neopulse){
	if(!PMC_global_variables.vsb_N.show){
		return arr = Array(object_list.length).fill('')
	}
	var note_type = PMC_global_variables.vsb_N.type
	var last_note = -1
	//write note number
	var prev_reg = 99
	var string_list=[]
	var data= DATA_get_current_state_point(false)
	var scale_sequence = data.scale.scale_sequence
	object_list.forEach((object,index)=>{
		var nzc_note = object.nzc_note
		if (nzc_note ==-1){
			//nothing to do
			string_list.push("")
		}else if(object.nzc_note ==-2){
			string_list.push("e")
		}else if(object.nzc_note ==-3){
			string_list.push("L")
		} else {
			//note
			switch(note_type){
				case 'Na':
					string_list.push(nzc_note)
					//last_note=nzc_note
				break;
				case 'Nm':
					var mod = TET
					var reg = Math.floor(nzc_note/mod)
					var resto = nzc_note%mod

					if(reg != prev_reg){
						string_list.push(resto+"r"+reg)
					} else {
						string_list.push(resto)
					}
					prev_reg = reg
					last_note=nzc_note
				break;
				case'Ngm':
					var mod = TET
					//find scale
					var current_scale = null
					var pulse = object.pulse_float*neopulse.N/neopulse.D
					var scale_end = 0
					var current_scale = scale_sequence.find(scale=>{
						scale_end+=scale.duration
						return scale_end>pulse
					})

					if(current_scale==null){
						string_list.push(no_value)
					} else{
						//element inside a scale module

						//determine if diesis or bemolle
						//var positive = object.diesis
						var [grade,delta,reg] = BNAV_absolute_note_to_scale(object.nzc_note,object.diesis,current_scale)

						//warning first element
						//verify is has module, if not read previous value
						var showreg = true
						//here retrive prev element with a reg
						if(prev_reg==reg)showreg=false
						prev_reg=reg

						var diesis = ""
						if(delta==1){
							diesis="+"
						}else if(delta>1){
							diesis="+"+(delta)
						}else if(delta==-1){
							diesis="-"
						}else if(delta<-1){
							diesis="-"+Math.abs(delta)
						}
						if(showreg){
							stringN=grade+diesis+"r"+reg
						}else{
							stringN=grade+diesis
						}
						string_list.push(stringN)
					}
					last_note=nzc_note
				break;
			}
		}
	})
	return string_list
}

function PMC_redraw_selected_voice_svg(full_color,voice_data){
	//Selected voice
	var D_NP = voice_data.neopulse.D
	var N_NP = voice_data.neopulse.N
	var neopulse_moltiplicator =  N_NP/D_NP
	var total_length = PMC_x_length(voice_data)
	var voice_color = eval(voice_data.color)
	var voice_color_light = eval(voice_data.color+"_light")
	var voice_color_white = eval("nuzic_white")

	if(!full_color) voice_color = voice_color_light
	var offset = nuzic_block
	//voice widths
	var delta = nuzic_block * PMC_zoom_x * neopulse_moltiplicator
	var delta_y = nuzic_block * PMC_zoom_y
	var note_number = TET*7

	var array_voice_positions = PMC_segment_data_to_obj_data(voice_data.data.segment_data,voice_data.neopulse)

	//draw objects
	//positioning and resizing main block
	var first_note = array_voice_positions.find(object=>{
			return object.nzc_note>-1
	})
	//draw objects
	var starting_pos = voice_data.e_sound
	if(first_note!=null){
		starting_pos = first_note.nzc_note
	}

	var last_nzc_note=starting_pos
	var last_pos=starting_pos
	var svg_string=""
	array_voice_positions.forEach(object=>{
		var nzc_note = object.nzc_note
		if (nzc_note ==-1){
			//silence == nothing
			last_nzc_note=nzc_note
		}else if (nzc_note ==-2){
			//element box positioned in line last note
			var note = last_pos
			var x1 = offset+object.pulse_float*delta
			var x2 = x1 + object.duration *delta
			var y = delta_y*(note_number-note)-delta_y+1
			if(x2>2)x2-=1//little space
			svg_string+=`
				<rect x="${x1}" y="${y}" width="${x2-x1}" height="${delta_y-2}" fill="var(--Nuzic_white)" stroke="${voice_color}" stroke-width="2"></rect>
			`
			last_nzc_note=nzc_note
		}else if(nzc_note ==-4){
				//nothing
				last_nzc_note=nzc_note
		} else if(nzc_note ==-3){
			//ligadura, need to print something
			if(last_nzc_note!=-1 && last_nzc_note!=-4){
				var note = last_pos
				var x1 = offset+object.pulse_float*delta
				var x2 = x1 + object.duration *delta
				var y = delta_y*(note_number-note)-delta_y/2
				//if(x2>2)x2-=1//NO NEED little space FOR SELECTED VOICE
				//x1-=1 //connect with previous
				if(last_nzc_note==-2){
					//modify last
					var last_line = svg_string.trim().match(/.*$/)[0]
					//modify value x2 of the rect in last line
					var last_line_array = svg_string.split(' width="').pop().split('" height=')
					var width_text = last_line_array[0]
					var old_width=parseInt(width_text)
					var s_add=old_width+x2-x1
					last_line=last_line.replace(' width="'+width_text+'"',' width="'+s_add+'"')
					svg_string+=last_line
				}else{
					//it is a note
					svg_string+=`
						<line x1="${x1}" y1="${y}" x2="${x2}" y2="${y}" stroke="${voice_color}" stroke-width="${delta_y}"></line>
						<line x1="${x1}" y1="${y}" x2="${x2}" y2="${y}" stroke="${voice_color_light}" stroke-width="${delta_y-4}"></line>
						<line x1="${x2}" y1="${y-delta_y/2}" x2="${x2}" y2="${y+delta_y/2}" stroke="${voice_color}" stroke-width="2"></line>
					`
				}
			}else if(last_nzc_note==-1){
				//L of a silence (not visible in not selected voice BUT visible in current voice)
				console.log("attenzione ai pallini")
				var note = last_pos
				var x1 = offset+object.pulse_float*delta
				var x2 = x1 + object.duration *delta
				var y = delta_y*(note_number-note)-delta_y/2-1
				svg_string+=`
					<line x1="${x1}" y1="${y}" x2="${x2}" y2="${y}" stroke="var(--Nuzic_pink)" stroke-dasharray="4,4" stroke-width="2"></line>
				`
				// svg_string+=`
				// 	<rect x="${x1}" y="${y}" width="${x2-x1}" height="${delta_y-2}" fill="var(--Nuzic_white)" stroke="${voice_color}" stroke-width="2"></rect>
				// `
			}
		} else {
			var note = nzc_note
			var x1 = offset+object.pulse_float*delta
			var x2 = x1 + object.duration *delta
			var y = delta_y*(note_number-note)-delta_y/2
			//if(x2>2)x2-=1//little space
			svg_string+=`
					<line x1="${x1}" y1="${y}" x2="${x2}" y2="${y}" stroke="${voice_color}" stroke-width="${delta_y}"></line>
				`
			last_nzc_note=nzc_note
			last_pos=nzc_note
		}
	})
	PMC_svg_all_voices.innerHTML+=svg_string
}

function PMC_redraw_selected_voice_sequence(){
	var container = document.querySelector("#App_PMC_selected_voice_sequence_container")
	var voice_data = DATA_get_selected_voice_data()
	var D_NP = voice_data.neopulse.D
	var N_NP = voice_data.neopulse.N
	var neopulse_moltiplicator =  N_NP/D_NP
	var total_length = PMC_x_length(voice_data) /// needed in order to add ending spacers
	//console.log("resulted length "+total_length)
	var voice_color = eval(voice_data.color)
	var voice_color_light = eval(voice_data.color+"_light")
	var voice_instrument = voice_data.instrument
	//var voice_color = nuzic_blue
	//var voice_color_light = nuzic_blue_light

	var delta_x = nuzic_block * PMC_zoom_x * neopulse_moltiplicator
	//var offset = (block + delta)/2 //2 half block
	//console.log(deltax)
	var offset = nuzic_block

	//selected voices objects
	var array_voice_positions = PMC_segment_data_to_obj_data(voice_data.data.segment_data,voice_data.neopulse)

	//draw objects XXX HERE WILL CHANGE
	// var ctx_selected = canvas_selected_voice.getContext('2d')
	container.innerHTML = ""

	//first spacer
	var div = document.createElement('div')
	div.classList.add("App_PMC_selected_voice_sequence_spacer")
	div.style.width = "var(--RE_block)"
	div.style.minWidth = "var(--RE_block)"
	container.appendChild(div)

	var prev_reg = 99
	var previousRange = -1
	var positive = true

	array_voice_positions.forEach(object=>{
		var inp = document.createElement('input')
		if(delta_x*object.duration<2){
			//there is no space,
			inp.style.border="none"
			console.log(inp)
			console.error("error segment time sequence voice index "+voice_data.voice_index)
		}
		inp.classList.add("App_PMC_selected_voice_sequence_object")
		inp.style.width = "calc(var(--PMC_x_block) * "+object.duration+" * "+neopulse_moltiplicator+")"
		inp.style.minWidth = "calc(var(--PMC_x_block) * "+object.duration+" * "+neopulse_moltiplicator+")"
		//inp.value = object.nzc_note

		var vsb_N = PMC_read_vsb_N()
		var type = vsb_N.type
		
		if (object.nzc_note ==-1){
			inp.style.backgroundColor = "var(--Nuzic_white)"
			inp.style.color = voice_color
			inp.value = "s"
		}else if (object.nzc_note ==-2){
			inp.style.backgroundColor = "var(--Nuzic_white)"
			inp.style.color = voice_color
			inp.value = "e"	//borders
			inp.style.backgroundImage= `-webkit-linear-gradient(right, ${voice_color} 0%, ${voice_color} 2px, transparent 2px, transparent calc(100% - 2px), ${voice_color} calc(100% - 2px), ${voice_color} 100%),
										-webkit-linear-gradient(top, ${voice_color} 0%, ${voice_color} 2px, var(--Nuzic_white) 2px, var(--Nuzic_white) calc(100% - 2px), ${voice_color} calc(100% - 2px), ${voice_color} 100%)`
		}else if(object.nzc_note ==-3){
			inp.style.backgroundColor = "var(--Nuzic_white)"
			inp.style.color = voice_color
			inp.value = "L"
			//borders
			inp.style.backgroundImage= `-webkit-linear-gradient(right, ${voice_color} 0%, ${voice_color} 2px, transparent 2px, transparent calc(100% - 2px), ${voice_color} calc(100% - 2px), ${voice_color} 100%),
										-webkit-linear-gradient(top, ${voice_color} 0%, ${voice_color} 2px, var(--Nuzic_white) 2px, var(--Nuzic_white) calc(100% - 2px), ${voice_color} calc(100% - 2px), ${voice_color} 100%)`
		} else {
			inp.style.backgroundColor = voice_color
			inp.style.color = "var(--Nuzic_white)"
		}

		switch(type){
		case 'Na':
			if(object.nzc_note >=0)inp.value = object.nzc_note
			inp.addEventListener("keypress", (e)=>{APP_onlyIntegers_Na_line(e,inp)})
			inp.addEventListener("change", ()=>{PMC_change_selected_voice_sequence_object_note(inp,type)})
		break;
		case 'Nm':
			if(object.nzc_note >=0){
				var mod = TET
				var reg = Math.floor(object.nzc_note/mod)
				var resto = object.nzc_note%mod
				if(reg != prev_reg){
					inp.value=`${resto}r${reg}`
				} else {
					inp.value=resto
				}
				prev_reg = reg
			}

			inp.addEventListener("keypress", (e)=>{APP_onlyIntegers_Nm_line(e,inp)})
			inp.addEventListener("change", ()=>{PMC_change_selected_voice_sequence_object_note(inp,type)})
		break;
		case 'Ngm':
			var mod = TET
			var data= DATA_get_current_state_point(false)
			var scale_sequence = data.scale.scale_sequence
			//find scale
			var current_scale = null
			var pulse = object.pulse_float*voice_data.neopulse.N/voice_data.neopulse.D
			var scale_end = 0

			var current_scale = scale_sequence.find(scale=>{
															scale_end+=scale.duration
															return scale_end>pulse
														})

			if(current_scale==null){
				inp.value=no_value
			} else{
				//element inside a scale module
				if(object.nzc_note >=0){
					//determine if diesis or bemolle
					var [grade,delta,reg] = BNAV_absolute_note_to_scale(object.nzc_note,object.diesis,current_scale)

					//warning first element
					//verify is has module, if not read previous value
					var showreg = true
					//here retrive prev element with a reg
					if(prev_reg==reg)showreg=false
					prev_reg=reg

					var diesis = ""
					if(delta==1){
						diesis="+"
					}else if(delta>1){
						diesis="+"+(delta)
					}else if(delta==-1){
						diesis="-"
					}else if(delta<-1){
						diesis="-"+Math.abs(delta)
					}
					if(showreg){
						stringN=grade+diesis+"r"+reg
					}else{
						stringN=grade+diesis
					}
					inp.value=stringN
				}
			}

			inp.addEventListener("keypress", (e)=>{APP_onlyIntegers_Ngm_line(e,inp)})
			inp.addEventListener("change", ()=>{PMC_change_selected_voice_sequence_object_note(inp,type,current_scale)})
		break;
		}

		container.appendChild(inp)
		//add functions
		//change value

		//control typing depends on type
		//inp.addEventListener("keypress", (e)=>{APP_onlyIntegers_Na_line(e,inp)})
		inp.addEventListener("keydown", (e)=>{PMC_move_focus_voice_sequence(e,inp,"App_PMC_selected_voice_sequence_object")}) //will trigger after change
		//inp.addEventListener("focusin", (e)=>{APP_set_previous_value(inp);APP_play_input_on_click(inp,'Na')})//PLAY NEED LOVE XXX
		inp.addEventListener("focusin", ()=>{APP_set_previous_value(inp)})

		inp.addEventListener("focusin",()=>{PMC_play_this_note_number(object.nzc_note,voice_instrument)})

	})
	//second spacer
	var div2 = document.createElement('div')
	div2.classList.add("App_PMC_selected_voice_sequence_spacer")
	div2.style.width = "calc(var(--PMC_x_block) * "+(total_length-Li-0.5)+")"
	div2.style.minWidth = "calc(var(--PMC_x_block) * "+(total_length-Li-0.5)+")"
	container.appendChild(div2)

	//draw canvas segments
	var canvas_segment = document.createElement('canvas')
	canvas_segment.classList.add("App_PMC_selected_voice_sequence_canvas")
	canvas_segment.style.width = "calc(var(--PMC_x_block) * "+(total_length-0.5)+" + var(--RE_block))"
	container.appendChild(canvas_segment)
	canvas_segment.style.height="var(--RE_block)"

	//draw segments,
	//console.log(voice_data)
	var segment_position = PMC_segment_data_to_segment_position(voice_data.data.segment_data)
	var ctx_s = canvas_segment.getContext('2d')

	var c_height = nuzic_block * 1
	canvas_segment.height= c_height
	canvas_segment.width = nuzic_block + nuzic_block * PMC_zoom_x * total_length - 0.5 * nuzic_block * PMC_zoom_x

	segment_position.forEach(item=>{
		ctx_s.beginPath()
		var x = offset+item*delta_x-0.5
		ctx_s.moveTo(x,0)
		ctx_s.lineTo(x,c_height)
		ctx_s.strokeStyle = nuzic_dark
		ctx_s.lineWidth = 3;
		ctx_s.stroke()
	})
}

function PMC_redraw_compas_grid(voice_data){
	//Selected voice
	var total_length = PMC_x_length(voice_data)
	var note_lines = TET*7-0.5
	var height_string = 'calc('+note_lines+' * var(--PMC_y_block) + var(--RE_block))'
	var width_string = "calc(var(--PMC_x_block) * "+(total_length-0.5)+" + var(--RE_block))"

	PMC_svg_compas.style.height=height_string
	PMC_svg_compas.style.width=width_string

	var delta = nuzic_block * PMC_zoom_x
	//var offset = (block + delta)/2 //2 half block
	var offset = nuzic_block
	var compas_data = DATA_get_current_state_point(false).compas
	var c_height = nuzic_block * PMC_zoom_y * note_lines + nuzic_block
	PMC_svg_compas.innerHTML=""

	var time_compas_change = []
	var position = 0
	compas_data.forEach(item=>{
		var lg = item.compas_values[1]
		var rep = item.compas_values[2]
		for (var i=0; i<rep;i++){
			position+=lg
			time_compas_change.push(position)
		}
	})

	//compas grid
	var svg_string = ""
	if(time_compas_change.length!=0){
		time_compas_change.unshift(0)
		for (var i=0; i<time_compas_change.length;i++){
			var x = offset+time_compas_change[i]*delta
			svg_string+=`
					<line x1="${x}" y1="0" x2="${x}" y2="${c_height}" stroke="${nuzic_light}" stroke-width="3"></line>
				`
		}
	}
	PMC_svg_compas.innerHTML=svg_string
}

function PMC_clear_compas_grid(voice_data){
	//Selected voice
	var total_length = PMC_x_length(voice_data)
	var note_lines = TET*7-0.5
	var height_string = 'calc('+note_lines+' * var(--PMC_y_block) + var(--RE_block))'
	var width_string = "calc(var(--PMC_x_block) * "+(total_length-0.5)+" + var(--RE_block))"

	//svg
	PMC_svg_compas.style.height=height_string
	PMC_svg_compas.style.width=width_string
	PMC_svg_compas.innerHTML=""
}

function PMC_segment_data_to_obj_data(segment_data_in,neopulse){
	//given a segment_data_array return position of every object
	var segment_data = JSON.parse(JSON.stringify(segment_data_in))//FK COPY!!!
	var time = []
	var nzc_note_list=[]
	var diesis_list=[]
	var nzc_time_list = []
	var nzc_frac_list = []
	segment_data.forEach(segment=>{
		segment.note.pop()
		nzc_note_list = nzc_note_list.concat(segment.note)
		segment.diesis.pop()
		diesis_list = diesis_list.concat(segment.diesis)
		//create a list of times
		nzc_time_list = nzc_time_list.concat(segment.time)
		//create a list of fractioning ranges
		nzc_frac_list = nzc_frac_list.concat(segment.fraction)
	})

	//convert array time in pulse/fraction data
	var last_pulse = 0
	var segment_start = 0
	var time_PF_list = [[0,0]]

	nzc_time_list.forEach(value=> {
		if(value==="") {
			console.error("error database, pulse string conversion")
			time_PF_list.push( [null,null])
		}
		var pulse = value.P
		var fraction = value.F
		if(pulse == 0 && fraction==0) {
			//start of a segment
			segment_start=last_pulse
			pulse=last_pulse

		}else{
			pulse +=segment_start
			time_PF_list.push([pulse,fraction])
		}
		//console.log(value +" = " +pulse+"  "+fraction)
		//last_pulse_text =
		last_pulse = pulse
		//console.log(last_pulse+"   "+segment_start)
	})

	//convert array fraction ranges
	last_pulse = 0
	var fraction_range_list = []
	nzc_frac_list.forEach(fraction=>{
		var delta = fraction.stop-fraction.start
		var start = last_pulse
		var end = start+delta
		last_pulse = end
		fraction_range_list.push([[fraction.N,fraction.D],start,end,delta])
	})

	//calculate time in pulse
	var time_list = []
	var moltiplicator_list = []
	time_PF_list.forEach(item=>{
		var time_P = item[0]
		var time_F = 0
		if (item[1]!=0){
			var fraction_data = fraction_range_list.find((fraction)=>{
				return fraction[2]>item[0]
			})
			var time_F = ((item[1] * fraction_data[0][0])/(fraction_data[0][1]))
			moltiplicator_list.push(fraction_data[0][0]/fraction_data[0][1])
		}else{
			moltiplicator_list.push(1)//F==0 so doesn t matter
		}

		time_list.push(time_P+time_F)
	})

	//recalculate the duration or iTs
	var index=1
	var max_index = time_list.length
	var duration_list = time_list.map(item=>{
		if(index<=max_index){
			var duration= time_list[index] - item
			index++
			return duration
		}
	})

	duration_list.pop()

	//recalculate iT
	index=1
	var iT_list = time_PF_list.map(item=>{
		if(index<max_index){
			var time_P1 = item[0]
			var time_F1 = item[1]
			var time_P2 = time_PF_list[index][0]
			var time_F2 = time_PF_list[index][1]
			var fraction_data = fraction_range_list.find((fraction)=>{
				return fraction[2]>time_P1
			})
			var iT= (time_P2-time_P1) * (fraction_data[0][1]/fraction_data[0][0]) - time_F1 + time_F2
			// console.log(fraction_data)
			// console.log(time_P1+"  "+time_F1+"   "+ time_P2+"  "+time_F2 + "   = "+iT)
			index++
			return iT
		}
	})

	time_list.pop()//no end in midi_data only start and duration


	var seg_start_pulse=0
	var Tsa_list = [] //pulse + pulse start segm


	//calc pulse segments (pulse + segment start)
	Tsa_list.push({P:0,F:0})
	nzc_time_list.forEach(nzc_time=>{
		if(nzc_time.P==0 && nzc_time.F==0){
			//Ta already there
			seg_start_pulse=Tsa_list.slice(-1)[0].P
		}else{
			let current_Tsg_P=(seg_start_pulse+nzc_time.P)
			Tsa_list.push({P:current_Tsg_P,F:nzc_time.F})
		}
	})

	//calc pulse absolutre if possible
	var last_pulse=0
	var Ta_list_text = []
	if(neopulse.N==1 && neopulse.D==1){
		//Ta_list_text.push("0")
		Tsa_list.forEach(nzc_time=>{
			let current_Tsg_P=nzc_time.P
			if(nzc_time.F==0){
				last_pulse=nzc_time.P
				Ta_list_text.push(current_Tsg_P+"")
			}else if(nzc_time.P==last_pulse){
				Ta_list_text.push("."+nzc_time.F)
			}else{
				last_pulse=nzc_time.P
				Ta_list_text.push(current_Tsg_P+"."+nzc_time.F)
			}
		})
	}else{
		Tsa_list.forEach(nzc_time=>{
			Ta_list_text.push(no_value)
		})
	}

	last_pulse=0
	var Ts_list_text = []
	Ts_list_text.push("0")
	nzc_time_list.forEach(nzc_time=>{
		if(nzc_time.P==0 && nzc_time.F==0){
			//Ts -> modify last value to 0
			//Ts_list_text.slice(-1)[0]="0"//NO
			Ts_list_text.pop()
			Ts_list_text.push("0")
		}else{
			if(nzc_time.F==0){
				last_pulse=nzc_time.P
				Ts_list_text.push(nzc_time.P+"")
			}else if(nzc_time.P==last_pulse){
				Ts_list_text.push("."+nzc_time.F)
			}else{
				last_pulse=nzc_time.P
				Ts_list_text.push(nzc_time.P+"."+nzc_time.F)
			}
		}
	})

	//organize in a single beautiful array
	var array = []
	time_list.forEach((item,index)=>{
		//array.push({"pulse":item ,"duration":duration_list[index],"nzc_note":nzc_note_list[index],"diesis":diesis_list[index],"iT":iT_list[index],"Time_text":time_text_list[index]})
		array.push({"pulse_float":item ,"duration":duration_list[index],"nzc_note":nzc_note_list[index],
				"diesis":diesis_list[index],"iT":iT_list[index],"Ts_text":Ts_list_text[index],
				"Ta_text":Ta_list_text[index],"Tsa_data":Tsa_list[index],"frac_moltiplicator":moltiplicator_list[index]})
	})

	return array
}

function PMC_segment_data_to_segment_position(segment_data){
	//given a segment_data_array return position of every segment
	var position = 0
	var segment_position = segment_data.map(segment=>{
		position += segment.time[segment.time.length-1].P
		return position
	})
	segment_position.unshift(0)
	return segment_position
}

function PMC_set_H_scrollbar_step_N(n_boxes){
	var step = PMC_main_scrollbar_H.querySelector(".App_PMC_element_step_H")

	//change div width according to required boxes
	step.style.minWidth = "calc(var(--PMC_x_block) * "+(n_boxes-0.5)+" + var(--RE_block))"
}

function PMC_set_V_scrollbar_step_N(){
	//call every time TET change
	//changing V scrollbar elements
	var scrollbar_container = document.querySelector(".App_PMC_main_scrollbar_V")
	var step = scrollbar_container.querySelector(".App_PMC_element_step_V")

	//change div height according to required note
	var note_number = TET*7-0.5
	step.style.minHeight = "calc(var(--PMC_y_block) * "+note_number+")"
}

//scrollbars sync with different voices
var scrollbarPMCposition_x = 0
var scrollbarPMCposition_y = 0
function PMC_checkScroll_H(value=null,forceScrollbarCheck = false) {
	var container = document.querySelector(".App_PMC")
	var time_line = container.querySelector(".App_PMC_time_line")

	var voice_sequence = container.querySelector("#App_PMC_selected_voice_sequence_container")
	// App_PMC_scale/compas modules
	var compas_div = container.querySelector('.App_PMC_S')
	var scales_div = container.querySelector('.App_PMC_C')
	// App_PMC_RE_preview XXX

	if(!PMC_main_scrollbar_H || !time_line || !PMC_canvas_container) return;

	if(value!=null){
		//force scrollbar position
		PMC_main_scrollbar_H.scrollLeft=value
		forceScrollbarCheck=true
	}

	if(PMC_main_scrollbar_H.scrollLeft != scrollbarPMCposition_x) {
		//no in forceScrollbar
		PMC_close_dropdown_menu_segment()
	}

	if(PMC_main_scrollbar_H.scrollLeft != scrollbarPMCposition_x || forceScrollbarCheck) {
		scrollbarPMCposition_x = PMC_main_scrollbar_H.scrollLeft
		PMC_canvas_container.scrollLeft = scrollbarPMCposition_x
		time_line.scrollLeft=  scrollbarPMCposition_x
		voice_sequence.scrollLeft=  scrollbarPMCposition_x
		compas_div.scrollLeft = scrollbarPMCposition_x
		scales_div.scrollLeft = scrollbarPMCposition_x
	}
}

function PMC_checkScroll_V(value=null,forceScrollbarCheck = false) {
	var container = document.querySelector(".App_PMC")
	var sound_line = container.querySelector(".App_PMC_sound_line")
	var letter_line = document.querySelector('.App_PMC_voice_selector_letter_guide')
	var scrollbar = container.querySelector('.App_PMC_main_scrollbar_V')

	if(!scrollbar || !sound_line || !PMC_canvas_container) return;

	if(value!=null){
		//force scrollbar position
		scrollbar.scrollTop=value
		forceScrollbarCheck=true
	}

	if(scrollbar.scrollTop != scrollbarPMCposition_y || forceScrollbarCheck) {
		scrollbarPMCposition_y = scrollbar.scrollTop
		PMC_canvas_container.scrollTop = scrollbar.scrollTop
		sound_line.scrollTop=  scrollbar.scrollTop
		letter_line.scrollTop = scrollbar.scrollTop
	}
}

function PMC_x_length(voice_data){
	var max_length=PMC_x_dots(voice_data)
	//neopulse!!
	max_length=max_length*voice_data.neopulse.N/voice_data.neopulse.D
	//Add scale if necessary
	var scaleData= DATA_get_current_state_point(false)

	var scale_values = scaleData.scale.scale_sequence
	var scaleCounter = 0
	scale_values.forEach((element, index) => {
		scaleCounter += element.duration
	})
	//extra bit
	if(max_length>scaleCounter){
		max_length += PMC_extra_x_space
		return max_length
	} else {
		scaleCounter += PMC_extra_x_space
		return scaleCounter
	}
}

//ELEMENTS

function PMC_change_selected_voice_sequence_object_note(element,type,current_scale){
	APP_stop()
	var line_container = document.querySelector("#App_PMC_selected_voice_sequence_container")
	var object_list = [...line_container.querySelectorAll(".App_PMC_selected_voice_sequence_object")]
	var position = object_list.indexOf(element)
	var voice_data = DATA_get_selected_voice_data()
	
	if(element.value==""){
		//DELETE
		DATA_delete_object_index(voice_data.voice_index,null, position)
		return
	}

	switch(type){
	case 'Na':
		//re-check
		var note_Na = parseInt(element.value)
		if(element.value=="L" || element.value=="l")note_Na=-3
		if(element.value=="e")note_Na=-2
		if(element.value=="s")note_Na=-1

		//calc in DATABASE
		var success = DATA_modify_object_index_note(voice_data.voice_index,null, position, note_Na,null)
		if(!success){
			element.value=previous_value
		}else{
			if(SNAV_read_play_new_note())APP_play_this_note_number(note_Na,voice_data.instrument)
		}
	break;
	case 'Nm':
		var note_Na
		if(element.value=="L" || element.value=="l"){
			note_Na=-3
		}else if(element.value=="e"){
			note_Na=-2
		} else if(element.value=="s"){
			note_Na=-1
		}else {
			var split = element.value.split('r')
			var resto = Math.floor(split[0])
			var reg=3
			if(split[1]>=0 && split[1]!=""){
				reg=Math.floor(split[1])
			}else{
				//find previous element value
				if(position!= 0){
					var found = false
					var prev_position = position-1
					var note_array= []
					voice_data.data.segment_data.forEach(segment=>{
						var note_list = JSON.parse(JSON.stringify(segment.note))
						note_list.pop()
						note_array= note_array.concat(note_list)
					})
					while(found == false){
						//console.log(note_array[prev_position])
						if(note_array[prev_position] >= 0){
							found = true
							reg= Math.floor(note_array[prev_position]/TET)
						} else {
							prev_position--
							if(prev_position<0)found=true
						}
					}
				}
			}
			note_Na = resto + reg*TET
		}
		//calc in DATABASE
		var success = DATA_modify_object_index_note(voice_data.voice_index,null, position, note_Na,null)
		if(!success){
			element.value=previous_value
		}else{
			if(SNAV_read_play_new_note())APP_play_this_note_number(note_Na,voice_data.instrument)
		}
	break;
	case 'Ngm':
		var note_Na = null
		var diesis = null
		if(element.value=="L" || element.value=="l"){
			note_Na=-3
		}else if(element.value=="e"){
			note_Na=-2
		} else if(element.value=="s"){
			note_Na=-1
		}else {
			//current_scale
			if (current_scale==null){
				//no scale in this pulse
				//console.error("no scale in this position")
				//using a unidentified element or a note if exist
				element.value=previous_value
				return
			}

			//ATT! reg can be -1
			var reg = 3
			var grade = 0
			var delta = 0
			var string = element.value

			if(string.includes("r")){
				var split = string.split("r")
				if(split[1]!=""){
					reg=Math.floor(split[1])
				}else{
					//no scale in this pulse
					console.error("no reg spec")
					note_Na = null
					element.value=previous_value
					break;
				}
				var str_grade = split[0]
				if(split[0].includes("+")){
					var split2= split[0].split("+")
					str_grade=split2[0]
					delta=parseInt(split2[1])
					if(isNaN(delta))delta=1
					diesis=true
				}
				if(split[0].includes("-")){
					var split2= split[0].split("-")
					str_grade=split2[0]
					delta=parseInt(split2[1])*(-1)
					if(isNaN(delta))delta=-1
					diesis=false
				}
				var grade = parseInt(str_grade)
			}else{
				//find reg in previous OR reg==3
				if(position!= 0){
					var found = false
					var prev_position = position-1
					var note_array= []
					voice_data.data.segment_data.forEach(segment=>{
						var note_list = JSON.parse(JSON.stringify(segment.note))
						note_list.pop()
						note_array= note_array.concat(note_list)
					})
					while(found == false){
						//console.log(note_array[prev_position])
						if(note_array[prev_position] >= 0){
							found = true
							reg= Math.floor(note_array[prev_position]/TET)
						} else {
							prev_position--
							if(prev_position<0)found=true
						}
					}
				}

				var str_grade = string
				if(string.includes("+")){
					var split2= string.split("+")
					str_grade=split2[0]
					delta=parseInt(split2[1])
					if(isNaN(delta))delta=1
					diesis=true
				}
				if(string.includes("-")){
					var split2= string.split("-")
					str_grade=split2[0]
					delta=parseInt(split2[1])*(-1)
					if(isNaN(delta))delta=-1
					diesis=false
				}
				var grade = parseInt(str_grade)
			}

			var scaleLength = idea_scale_list.scale_list.find(item=>{
				return item.TET == TET && item.letter==current_scale.acronym
			})
			//console.log(scaleLength)

			if(grade>=scaleLength.iS.length){
				//console.log(scale)
				element.value = previous_value
				return
			} else {
				note_Na = BNAV_scale_note_to_absolute(grade,delta,reg,current_scale)
			}
			// console.log("=======================")
			// console.log(note_number)
			// console.log(grade+"   "+delta+"   "+reg)
			if(isNaN(note_Na)){
				note_Na = null
			}
		}
		if(note_Na==null){
			console.error("i dont understand what woy wrote")
			element.value=previous_value
			return
		}

		//calc in DATABASE
		var success = DATA_modify_object_index_note(voice_data.voice_index,null, position, note_Na,diesis)
		if(!success){
			element.value=previous_value
		}else{
			if(SNAV_read_play_new_note())APP_play_this_note_number(note_Na,voice_data.instrument)
		}
	break;
	}
}

function PMC_move_focus_voice_sequence(evt,element,class_type){
	switch(evt.keyCode){
	case 37:
		// Key left.
		PMC_focus_prev_index_voice_sequence(element,class_type)
		evt.preventDefault();
		break
	case 39:
		// Key right.
		PMC_focus_next_index_voice_sequence(element,class_type)
		evt.preventDefault();
		break
	case 9:
		//tab
		evt.preventDefault()

		if(evt.shiftKey) {
			//shift was down when tab was pressed
			PMC_focus_prev_index_voice_sequence(element,class_type)
		}else{
			//tab
			PMC_focus_next_index_voice_sequence(element,class_type)
		}
		break
	case 13:
		//intro
		PMC_focus_next_index_voice_sequence(element,class_type)
		break
	}
	//preventDefault to avoid scrollbar to move automatically
	//NO need to a function that eventually move the scrollbar if the imput is out of sight
}

function PMC_focus_next_index_voice_sequence(element,class_type){
	var parent = element.parentNode
	var obj_list = [...parent.querySelectorAll("."+class_type)]

	var index=obj_list.indexOf(element)
	if(index<obj_list.length-1){
		PMC_focus_on_index_voice_sequence(index+1,obj_list,class_type,parent)
	}else{
		//manually trigger event
		element.dispatchEvent(new Event("change"))
		PMC_focus_on_index_voice_sequence(index,obj_list,class_type,parent)
	}
}

function PMC_focus_prev_index_voice_sequence(element,class_type){
	var parent = element.parentNode
	var obj_list = [...parent.querySelectorAll("."+class_type)]
	var index=obj_list.indexOf(element)
	if(index!=0){
		PMC_focus_on_index_voice_sequence(index-1,obj_list,class_type,parent)
	}else{
		//manually trigger event
		element.dispatchEvent(new Event("change"))
		PMC_focus_on_index_voice_sequence(index,obj_list,class_type,parent)
	}
}

function PMC_focus_on_index_voice_sequence(index,obj_list,class_type,parent){
	//first focus trigger refresh
	obj_list[index].focus()
	obj_list[index].select()

	//second focus stay
	requestAnimationFrame(function(){
		var obj_list = [...parent.querySelectorAll("."+class_type)]
		if(index>obj_list.length-1)index=obj_list.length-1
		obj_list[index].focus()
		obj_list[index].select()
	})
}

function PMC_focus_on_element(element){
	element.focus()
	element.select()
}

function PMC_reset_action_pmc(canvas=null,event=null){
	//clear position
	//console.error("clear")
	if(event!=null){
		var hover_id = event.target.id
		if(hover_id!="App_PMC_selected_voice"){
			//console.log("not reset")
			return
		}
		//parent  .... App_PMC_selected_voice_object
		//simpler
		if(event.explicitOriginalTarget.nodeName=="#text"){
			//console.log("text not reset")
			return
		}
		var selected_voice_object_hover = event.explicitOriginalTarget.closest(".App_PMC_selected_voice_object")
		//console.log(selected_voice_object_hover)
		if(selected_voice_object_hover!=null){
			//console.log("not reset")
			return
		}
	}

	//console.error("reset")
	current_position_PMCRE = {voice_data:DATA_get_selected_voice_data() ,segment_index:null , pulse: null, fraction: null, note: null, X_start:null, Y_start:null, REscrollbar_redraw:true}
	//current_position_PMCRE = JSON.parse(JSON.stringify(current_position_PMCRE_base))

	var [now,start,end,max]=APP_return_progress_bar_values()
	var width_pulse = max + 4.5
	//SVG
	var note_lines = TET * 7 -0.5
	var height_string = 'calc('+note_lines+' * var(--PMC_y_block) + var(--RE_block))'
	var width_string = "calc(var(--PMC_x_block) * "+width_pulse+" + var(--RE_block))"

	PMC_svg_selection_background.style.height=height_string
	PMC_svg_selection_background.style.width=width_string
	PMC_svg_selection_elements.style.height=height_string
	PMC_svg_selection_elements.style.width=width_string
	PMC_mouse_action_svg_1.style.height=height_string
	PMC_mouse_action_svg_1.style.width=width_string
	PMC_mouse_action_svg_2.style.height=height_string
	PMC_mouse_action_svg_2.style.width=width_string

	//clear lines in axes
	PMC_mouse_action_x.style.stroke="transparent"
	PMC_mouse_action_y.style.stroke="transparent"
	PMC_mouse_action_y_text.innerHTML=""
	//clear lines
	// PMC_mouse_action_x_PMC.style.stroke="transparent" //OPTIONAL not implemented
	// PMC_mouse_action_y_PMC.style.stroke="transparent" //OPTIONAL not implemented
	//clear svg
	PMC_mouse_action_drag.style.fill="transparent"
	PMC_mouse_action_drag_element.style.fill="transparent"
	PMC_mouse_action_drag_element.style.stroke="transparent"
}

function PMC_calculate_mouse_position(X_pos,Y_pos,voice_data){
	//function calculate mouse position (segment_index , pulse, fraction, note, fraction_index, X(pixels), Y(pixels)

	//calc note
	var note = Math.round((TET*7-1)- (Y_pos-0.5*PMC_zoom_y*nuzic_block)/(PMC_zoom_y*nuzic_block))

	//calc pulse,fraction and segment
	//var position = ((X_pos)/(PMC_zoom_x*nuzic_block))-1
	var position = ((X_pos-nuzic_block)/(PMC_zoom_x*nuzic_block))
	if(position<0){
		return {segment_index:0 , pulse: 0, fraction: 0 , note: note, X:nuzic_block, Y:(TET*7- 0.5 - note) * PMC_zoom_y*nuzic_block, time: 0}
	}

	if(position>=Li){
		var segment_index= voice_data.data.segment_data.length-1
		var pulse = voice_data.data.segment_data[segment_index].time.slice(-1)[0].P
		var X_pos = nuzic_block + Li * (PMC_zoom_x*nuzic_block)
		var Y_pos = (TET*7- 0.5 - note) * PMC_zoom_y*nuzic_block
		var time = Li*60/PPM
		return {segment_index:segment_index, pulse: pulse, fraction: 0 , note: note, X:X_pos, Y:Y_pos, time: time}
	}

	var pulse_raw_voice = Math.floor(Math.floor(position)*voice_data.neopulse.D/voice_data.neopulse.N)
	//calculate pulse in segment
	var segment_index=0
	var segment_start_position=0
	var segment_end_position=0
	voice_data.data.segment_data.find(segment=>{
		var Ls = segment.time.slice(-1)[0].P
		segment_start_position=segment_end_position
		segment_end_position+=Ls
		if(segment_end_position>pulse_raw_voice)return true
		segment_index++
	})
	var segment_start_position_abs=segment_start_position*voice_data.neopulse.N/voice_data.neopulse.D
	var pulse_raw_segment = pulse_raw_voice-segment_start_position

	//calculate pulse in current fractioning range
	//console.log(segment_index)
	var current_fraction = voice_data.data.segment_data[segment_index].fraction.find(fraction=>{
		return fraction.stop>pulse_raw_segment
	})

	var current_fraction_start_abs=current_fraction.start*voice_data.neopulse.N/voice_data.neopulse.D
	var delta_pixel_start_fraction =X_pos - nuzic_block - ((segment_start_position_abs+current_fraction_start_abs)*(PMC_zoom_x*nuzic_block))
	var delta_pixel_pulse= (PMC_zoom_x*nuzic_block)*voice_data.neopulse.N/voice_data.neopulse.D
	var delta_pixel_iT= delta_pixel_pulse*current_fraction.N/current_fraction.D

	//NEW ROULE: add whatever is smaller: 75% or 14px XXX XXX XXX UI XXX XXX XXX
	var roule75 = 14
	if((delta_pixel_iT*0.25)<roule75)roule75=Math.round(delta_pixel_iT*0.25)
	var iT_from_start_fractioning_range = Math.floor((delta_pixel_start_fraction+roule75)/delta_pixel_iT)

	var pulse = current_fraction.start+Math.floor(iT_from_start_fractioning_range/current_fraction.D)*current_fraction.N
	var fraction = iT_from_start_fractioning_range-(pulse-current_fraction.start)*(current_fraction.D/current_fraction.N)
	if(pulse<0 || fraction<0){
		console.error("PMC enter_new_element_pulse_note failed calculation mouse position")
		//console.log(pulse+"."+fraction)
		return
	}

	//not exactly here
	var X_pos = nuzic_block + (segment_start_position_abs + current_fraction_start_abs) * (PMC_zoom_x*nuzic_block) + iT_from_start_fractioning_range * delta_pixel_iT
	var Y_pos = (TET*7- 0.5 - note) * PMC_zoom_y*nuzic_block

	var time = (X_pos-nuzic_block)/(PMC_zoom_x*nuzic_block)*60/PPM

	return {segment_index:segment_index , pulse: pulse, fraction: fraction , note: note, X:X_pos, Y:Y_pos, time: time}
}

function PMC_mouse_down(canvas,event){
	APP_stop()
	//register position
	var Y_pos = event.layerY //0 top
	var X_pos = event.layerX //0 left

	if(event.target!=canvas){
		console.log("mouse down outside canvas")
		//hover on div
		Y_pos += event.target.offsetTop
		X_pos += event.target.offsetLeft
		//return
	}

	var mouse_position_corrected = PMC_calculate_mouse_position(X_pos,Y_pos,current_position_PMCRE.voice_data)
	//console.log(mouse_position_corrected)

	current_position_PMCRE.X_start = mouse_position_corrected.X
	current_position_PMCRE.Y_start = mouse_position_corrected.Y
	current_position_PMCRE.segment_index = mouse_position_corrected.segment_index
	current_position_PMCRE.pulse = mouse_position_corrected.pulse
	current_position_PMCRE.fraction = mouse_position_corrected.fraction
	current_position_PMCRE.note = mouse_position_corrected.note

	//no need .... APP_player_to_time do that
	//PMC_draw_progress_line_position(mouse_position_corrected.X)

	var segment_start_PM=0
	var index=0

	current_position_PMCRE.voice_data.data.segment_data.find(item=>{
		if(index==current_position_PMCRE.segment_index){
			return true
		}
		segment_start_PM+=item.time.slice(-1)[0].P
		index++
		return false
	})

	var neopulse_moltiplicator = current_position_PMCRE.voice_data.neopulse.N/current_position_PMCRE.voice_data.neopulse.D
	var pulse_abs = segment_start_PM + mouse_position_corrected.pulse*neopulse_moltiplicator

	APP_player_to_time(mouse_position_corrected.time, pulse_abs , mouse_position_corrected.pulse,current_position_PMCRE.fraction,current_position_PMCRE.segment_index,current_position_PMCRE.voice_data.voice_index)

}

function PMC_mouse_move(canvas,event){
	//calc position
	var Y_pos = event.layerY //0 top
	var X_pos = event.layerX //0 left
	if(event.target!=canvas){
		//hover on div
		Y_pos += event.target.offsetTop
		X_pos += event.target.offsetLeft
	}

	if(current_position_PMCRE==null || current_position_PMCRE.voice_data==null){
		PMC_reset_action_pmc()
		// var voice_data_mod = DATA_get_selected_voice_data()
		// current_position_PMCRE.voice_data = voice_data_mod
	}

	var mouse_position_corrected = PMC_calculate_mouse_position(X_pos,Y_pos,current_position_PMCRE.voice_data)

	//axis
	if(mouse_position_corrected!=undefined){
		PMC_mouse_action_x.style.stroke=eval("nuzic_yellow")
		PMC_mouse_action_x.setAttribute('x1',mouse_position_corrected.X-0.5)
		PMC_mouse_action_x.setAttribute('y1','0')
		PMC_mouse_action_x.setAttribute('x2',mouse_position_corrected.X-0.5)
		PMC_mouse_action_x.setAttribute('y2','100%')

		PMC_mouse_action_y.style.stroke=eval("nuzic_pink")
		PMC_mouse_action_y.setAttribute('x1','0')
		PMC_mouse_action_y.setAttribute('y1',mouse_position_corrected.Y-0.5)
		PMC_mouse_action_y.setAttribute('x2','100%')
		PMC_mouse_action_y.setAttribute('y2',mouse_position_corrected.Y-0.5)

		if(PMC_read_zoom_sound()=="0"){
			//write note
			PMC_mouse_action_y_text.setAttribute('y',mouse_position_corrected.Y+nuzic_block_half*0.5)
			var string_note = ""
			var vsb_N=PMC_read_vsb_N()
			var note_type = vsb_N.type

			//write values
			switch (note_type) {
			case "Na":
				string_note = mouse_position_corrected.note
			break;
			case "Nm":
				var reg = Math.floor(mouse_position_corrected.note/TET)
				var note = mouse_position_corrected.note%TET

				if(note==0){
					//string_note = `${note}r${reg}`
				} else{
					string_note = `${note}`
				}
			break;
			case "Ngm":
				var reg = Math.floor(mouse_position_corrected.note/TET)
				var note = mouse_position_corrected.note%TET

				if(note==0){
					//string_note = `${note}r${reg}`
				} else{
					string_note = `${note}`
				}
			break;
			}
			PMC_mouse_action_y_text.innerHTML=string_note
		}else{
			// PMC_mouse_action_y_text.fill="transparent"
			// PMC_mouse_action_y_text.stroke="transparent"
			PMC_mouse_action_y_text.innerHTML=""
		}
	}else{
		PMC_mouse_action_x.style.stroke="transparent"
		PMC_mouse_action_y.style.stroke="transparent"
		// PMC_mouse_action_y_text.fill="transparent"
		// PMC_mouse_action_y_text.stroke="transparent"
		PMC_mouse_action_y_text.innerHTML=""
	}

	//drag a note
	if(mouse_position_corrected!=undefined && event.buttons==1){
		if(mouse_position_corrected.segment_index==current_position_PMCRE.segment_index){
			if(mouse_position_corrected.X>current_position_PMCRE.X_start){
				var width = mouse_position_corrected.X-current_position_PMCRE.X_start
				PMC_mouse_action_drag.style.fill=eval("nuzic_light")
				PMC_mouse_action_drag.setAttribute('x',current_position_PMCRE.X_start-0.5)
				PMC_mouse_action_drag.setAttribute('width',width)

				var fill_color=current_position_PMCRE.voice_data.color
				var fill_color_light=fill_color+"_light"
				var border_color=current_position_PMCRE.voice_data.color
				//is_silence
				fill_color= (currKey=="s" || currKey=="z")?   "nuzic_white" :fill_color
				border_color= (currKey=="s" || currKey=="z")?   "nuzic_white" :fill_color
				//is_element
				fill_color= (currKey=="e" || currKey=="x")?   "nuzic_white" :fill_color
				//is_tie_intervals
				fill_color= (currKey=="l" || currKey=="c")?   fill_color_light :fill_color

				PMC_mouse_action_drag_element.style.fill=eval(fill_color)
				PMC_mouse_action_drag_element.style.stroke=`${eval(border_color)}`
				PMC_mouse_action_drag_element.setAttribute('x',current_position_PMCRE.X_start-0.5)
				PMC_mouse_action_drag_element.setAttribute('y',current_position_PMCRE.Y_start-0.5*PMC_zoom_y*nuzic_block)
				PMC_mouse_action_drag_element.setAttribute('width',width)
				PMC_mouse_action_drag_element.setAttribute('height',PMC_zoom_y*nuzic_block)
			}else{
				//clear
				PMC_mouse_action_drag_element.style.fill="transparent"
				PMC_mouse_action_drag_element.style.stroke="transparent"
				PMC_mouse_action_drag.style.fill="transparent"
			}
		}else{
			//end of segment
			if(current_position_PMCRE.segment_index==null)return
			if(mouse_position_corrected.X>current_position_PMCRE.X_start){
				//console.log(mouse_position_corrected)
				var segment_data = current_position_PMCRE.voice_data.data.segment_data
				var index = -1
				var pulse_abs = 0
				segment_data.find(item=>{
					index++
					pulse_abs+=item.time.slice(-1)[0].P
					return index==current_position_PMCRE.segment_index
				})
				var X_end = pulse_abs * (current_position_PMCRE.voice_data.neopulse.N/current_position_PMCRE.voice_data.neopulse.D)* (PMC_zoom_x*nuzic_block) + nuzic_block
				var width = X_end-current_position_PMCRE.X_start
				PMC_mouse_action_drag.style.fill=eval("nuzic_light")
				PMC_mouse_action_drag.setAttribute('x',current_position_PMCRE.X_start-0.5)
				PMC_mouse_action_drag.setAttribute('width',width)

				var fill_color=current_position_PMCRE.voice_data.color
				var fill_color_light=fill_color+"_light"
				var border_color=current_position_PMCRE.voice_data.color
				//is_silence
				fill_color= (currKey=="s" || currKey=="z")?   "nuzic_white" :fill_color
				border_color= (currKey=="s" || currKey=="z")?   "nuzic_white" :fill_color
				//is_element
				fill_color= (currKey=="e" || currKey=="x")?   "nuzic_white" :fill_color
				//is_tie_intervals
				fill_color= (currKey=="l" || currKey=="c")?   fill_color_light :fill_color

				PMC_mouse_action_drag_element.style.fill=eval(fill_color)
				PMC_mouse_action_drag_element.style.stroke=`${eval(border_color)}`
				PMC_mouse_action_drag_element.setAttribute('x',current_position_PMCRE.X_start-0.5)
				PMC_mouse_action_drag_element.setAttribute('y',current_position_PMCRE.Y_start-0.5*PMC_zoom_y*nuzic_block)
				PMC_mouse_action_drag_element.setAttribute('width',width)
				PMC_mouse_action_drag_element.setAttribute('height',PMC_zoom_y*nuzic_block)
			}else{
				//clear
				PMC_mouse_action_drag_element.style.fill="transparent"
				PMC_mouse_action_drag_element.style.stroke="transparent"
				PMC_mouse_action_drag.style.fill="transparent"
			}
		}
	}
}

let currKey = null;
window.onkeydown = function(event) {
	currKey = event.key
}
window.onkeyup = function(event) {
	currKey = null
}
function PMC_mouse_up(canvas,event){
	//if mouse has moved create new element
	var Y_pos = event.layerY //0 top
	var X_pos = event.layerX //0 left
	if(event.target!=canvas){
		console.log("mouse up outside canvas")
		//clear svg
		// PMC_mouse_action_drag.style.fill="transparent"
		// PMC_mouse_action_drag_element.style.fill="transparent"
		// PMC_mouse_action_drag_element.style.stroke="transparent"
		//hover on div
		//hover on div
		Y_pos += event.target.offsetTop
		X_pos += event.target.offsetLeft
	}
	//console.log(current_position_PMCRE)
	var mouse_position_corrected = PMC_calculate_mouse_position(X_pos,Y_pos,current_position_PMCRE.voice_data)
	//console.log(mouse_position_corrected)
	if(mouse_position_corrected==null){
		return
	}

	//var isCtrlPressed = event.ctrlKey
	// console.log(isCtrlPressed)
	// console.log(event.keyCode)
	var is_silence = currKey=="s" || currKey=="z"
	var is_element = currKey=="e" || currKey=="x"
	var is_tie_intervals = currKey=="l" || currKey=="c"

	//change database
	if(mouse_position_corrected.X>current_position_PMCRE.X_start){
		var P_start = current_position_PMCRE.pulse
		var P_end = null
		var F_start = current_position_PMCRE.fraction
		var F_end = null
		var segment_data = current_position_PMCRE.voice_data.data.segment_data[current_position_PMCRE.segment_index]
		if(mouse_position_corrected.segment_index>current_position_PMCRE.segment_index){
			P_end = segment_data.time[segment_data.time.length-1].P
			F_end = 0
		}

		if(mouse_position_corrected.segment_index==current_position_PMCRE.segment_index){
			P_end = mouse_position_corrected.pulse
			F_end = mouse_position_corrected.fraction
		}
		if(P_start<P_end || F_start<F_end){
			//proceed
			//console.log(P_start+"."+F_start+"->"+P_end+"."+F_end)
			var copy_current_position_PMCRE = JSON.parse(JSON.stringify(current_position_PMCRE))
			APP_stop()
			current_position_PMCRE = JSON.parse(JSON.stringify(copy_current_position_PMCRE))
			//console.log(P_start+" . "+F_start+"  "+P_end+" . "+F_end)
			//define note
			var note_to_insert=current_position_PMCRE.note
			var proceed=true
			if(is_silence){
				note_to_insert=-1
			}
			if(is_element){
				note_to_insert=-2
			}
			if(is_tie_intervals){
				//control if is possible to write note
				note_to_insert=-3
				if(P_start==0 && F_start==0)proceed=false
			}
			if(proceed){
				//write note
				DATA_overwrite_new_object_PA_PB(current_position_PMCRE.voice_data.voice_index,current_position_PMCRE.segment_index,P_start,F_start,P_end,F_end,note_to_insert,true)
			}

			//play note
			if(SNAV_read_play_new_note() && note_to_insert>-1)APP_play_this_note_number(note_to_insert,current_position_PMCRE.voice_data.instrument)
		}
	}

	// RE / PMC / progress position
	var pulse_abs = (current_position_PMCRE.X_start-nuzic_block)/(PMC_zoom_x*nuzic_block)
	var time = (current_position_PMCRE.X_start-nuzic_block)/(PMC_zoom_x*nuzic_block)*60/PPM
	APP_player_to_time(time, pulse_abs , mouse_position_corrected.pulse,current_position_PMCRE.fraction,current_position_PMCRE.segment_index,current_position_PMCRE.voice_data.voice_index)

	PMC_mouse_action_x.style.stroke=eval("nuzic_yellow")
	PMC_mouse_action_y.style.stroke=eval("nuzic_pink")

	//clear svg
	PMC_mouse_action_drag.style.fill="transparent"
	PMC_mouse_action_drag_element.style.fill="transparent"
	PMC_mouse_action_drag_element.style.stroke="transparent"
}

//double click
function PMC_enter_new_element_pulse_note(canvas,event){
	//calc position
	APP_stop()
	var Y_pos = event.layerY //0 top
	var X_pos = event.layerX //0 left
	if(event.target!=canvas){
		console.log("click outside canvas")
		//console.log(event.target)
		//hover on div
		Y_pos += event.target.offsetTop
		X_pos += event.target.offsetLeft
		//return
	}
	var voice_data_mod = DATA_get_selected_voice_data()

	//calc voice index

	//calc note
	var note = Math.round((TET*7-1)- (Y_pos-0.5*PMC_zoom_y*nuzic_block)/(PMC_zoom_y*nuzic_block))
	//console.log(note)

	//calc pulse,fraction and segment
	//var position = ((X_pos)/(PMC_zoom_x*nuzic_block))-1
	var position = ((X_pos-nuzic_block)/(PMC_zoom_x*nuzic_block))
	if(position<0)return
	if(position>Li)return

	var mouse_position_corrected = PMC_calculate_mouse_position(X_pos,Y_pos,current_position_PMCRE.voice_data)

	//add element to data
	DATA_enter_new_object(voice_data_mod.voice_index,mouse_position_corrected.segment_index,mouse_position_corrected.pulse,mouse_position_corrected.fraction,mouse_position_corrected.note,true)

	//play note
	if(SNAV_read_play_new_note())APP_play_this_note_number(mouse_position_corrected.note,voice_data_mod.instrument)

	// RE / PMC / progress position
	var pulse_abs = (mouse_position_corrected.X-nuzic_block)/(PMC_zoom_x*nuzic_block)
	APP_player_to_time(mouse_position_corrected.time, pulse_abs , mouse_position_corrected.pulse , mouse_position_corrected.fraction, mouse_position_corrected.segment_index,voice_data_mod.voice_index)
}

function PMC_draw_progress_line_time(time){
	//if (tabPMCbutton.checked){
	//PMC
	var pulsation = time*PPM/60
	var position = pulsation*nuzic_block*PMC_zoom_x+nuzic_block

	PMC_draw_progress_line_position(position)
	current_position_PMCRE.X_start=position
}

function PMC_draw_progress_line_position(position){
	//SVG
	//PMC_progress_line.style.stroke=nuzic_dark
	//PMC_progress_line.style.stroke="violet"
	//console.error(position)
	PMC_progress_line.style.stroke=nuzic_blue
	PMC_progress_line.setAttribute('x1',position-0.5)
	PMC_progress_line.setAttribute('y1','0')
	PMC_progress_line.setAttribute('x2',position-0.5)
	PMC_progress_line.setAttribute('y2','100%')
}

function PMC_reset_progress_line(){
	var [now,start,end,max]=APP_return_progress_bar_values()
	var width_pulse = max + 4.5
	//SVG
	var note_lines = TET*7-0.5
	var height_string = 'calc('+note_lines+' * var(--PMC_y_block) + var(--RE_block))'
	var width_string = "calc(var(--PMC_x_block) * "+width_pulse+" + var(--RE_block))"

	PMC_progress_svg.style.height=height_string
	PMC_progress_svg.style.width=width_string

	//var svg_height = nuzic_block * PMC_zoom_y * TET * 7 + nuzic_block/2
	//var svg_width = nuzic_block + nuzic_block * PMC_zoom_x * width_pulse
	//svg.viewBox="0 0 "+svg_height+" "+svg_width

	//clear line
	PMC_progress_line.style.stroke="transparent"
	if(current_position_PMCRE!=null)current_position_PMCRE.X_start = null

	//PMC_reset_action_pmc()
}

function PMC_reset_playing_notes(){
	//SVG
	var [now,start,end,max]=APP_return_progress_bar_values()
	var width_pulse = max + 4.5
	//SVG
	var note_lines= TET*7-0.5
	var height_string = 'calc('+note_lines+' * var(--PMC_y_block) + var(--RE_block))'
	var width_string = "calc(var(--PMC_x_block) * "+width_pulse+" + var(--RE_block))"

	PMC_playing_notes_svg.style.height=height_string
	PMC_playing_notes_svg.style.width=width_string

	//var svg_height = nuzic_block * PMC_zoom_y * TET * 7 + nuzic_block/2
	//var svg_width = nuzic_block + nuzic_block * PMC_zoom_x * width_pulse
	//svg.viewBox="0 0 "+svg_height+" "+svg_width

	PMC_clear_playing_notes()
}

function PMC_clear_playing_notes(){
	//clear
	PMC_playing_notes_svg.innerHTML="" //this break everything
	// var line_array= [...PMC_playing_notes_svg.querySelectorAll("line")]
	// if(line_array.length==0)return
	// line_array.forEach(line=>{
	// 	PMC_playing_notes_svg.removeChild(line)
	// })
	// var rect_array= [...PMC_playing_notes_svg.querySelectorAll("rect")]
	// if(rect_array.length==0)return
	// rect_array.forEach(rect=>{
	// 	PMC_playing_notes_svg.removeChild(rect)
	// })
}

function PMC_add_note_to_playingNoteArr(note_data){
	var x1 = note_data.pulsation*nuzic_block*PMC_zoom_x+nuzic_block
	var x2 = x1+Math.round(note_data.duration*nuzic_block*PMC_zoom_x)
	var note_number = TET*7
	var delta_y = nuzic_block * PMC_zoom_y
	//note_data.note is not the position
	var y = delta_y*(note_number-note_data.y_position-0.5)
	if(note_data.note==-2){
		var svg_string=`
			<rect id="${'event'+note_data.note_id}" x="${x1}" y="${y-(delta_y/2)+1}" width="${x2-x1}" height="${delta_y-2}" fill="var(--Nuzic_white)" stroke="${eval(note_data.color)}" stroke-width="2"></rect>
		`
	}else{
		var svg_string=`
			<line id="${'event'+note_data.note_id}" x1="${x1}" y1="${y}" x2="${x2}" y2="${y}" stroke="${eval(note_data.color)}" stroke-width="${delta_y}"></line>
		`
	}
	PMC_playing_notes_svg.innerHTML+=svg_string
}

function PMC_remove_note_from_playingNotesArr(note){
	var line= PMC_playing_notes_svg.querySelector("#event"+note.note_id)
	if(line!= null)PMC_playing_notes_svg.removeChild(line)
}

function PMC_calculate_best_scrollbar_position(time){
	if (tabPMCbutton.checked){
		var overflow_w = PMC_canvas_container.clientWidth
		var position = nuzic_block + time/60 * PPM * (nuzic_block*PMC_zoom_x)
		if(time<0.5)position=0
		var overflow_start_position = PMC_canvas_container.scrollLeft
		var overflow_end_position = overflow_start_position+overflow_w

		if(position<overflow_start_position || position>(overflow_end_position-nuzic_block*PMC_zoom_x)){
			//move scrollbar to position
			PMC_main_scrollbar_H.scrollLeft=position
		}
	}
}

//play App_PMC_selected_voice_object_N and App_PMC_selected_voice_sequence_object note
function PMC_play_this_note_number(nzc_note, instrument_index){
	if(SNAV_read_note_on_click() == true){
		APP_play_this_note_number(nzc_note , instrument_index)
	}
}

