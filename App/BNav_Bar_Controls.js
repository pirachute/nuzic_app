function BNAV_open_bottom_nav_content(){
	APP_stop()
	var ES_button = document.getElementById("App_BNav_tab_ES")
	var ET_button = document.getElementById("App_BNav_tab_ET")
	var Key_button = document.getElementById("App_BNav_tab_keyboard")
	var calc_button = document.getElementById("App_BNav_tab_calc")

	var mod_S_content = document.getElementById("App_BNav_mod_S")
	var mod_T_content = document.getElementById("App_BNav_mod_T")
	var keyboard_content = document.getElementById("App_BNav_keyboard")
	var calc_content = document.getElementById("App_BNav_calc")

	mod_S_content.style["display"] = "none"
	mod_T_content.style["display"] = "none"
	keyboard_content.style["display"] = "none"
	calc_content.style['display'] = 'none'
	let keyboardBttn = document.getElementById('compKeyBttn')
	if(ES_button.checked){
		if(keyboardBttn.value=='true'){
			Switch_keyboard_as_MIDI_controller()
		}
		mod_S_content.style["display"] = "flex"
		BNAV_change_ES_tab()
	} else if (ET_button.checked){
		if(keyboardBttn.value=='true'){
			Switch_keyboard_as_MIDI_controller()
		}
		mod_T_content.style["display"] = "flex"
		BNAV_change_ET_tab()
	} else if (Key_button.checked){
		keyboard_content.style["display"] = "flex"
	}else if(calc_button.checked){
		if(keyboardBttn.value=='true'){
			Switch_keyboard_as_MIDI_controller()
		}
		calc_content.style["display"] = "flex"
	}

	BNAV_refresh_all_compas_button_values()
	BNAV_refresh_PcLi()
	BNAV_refresh_PsLi()

	var button = document.querySelector('.App_BNav_tab_button')
	if(!button.classList.contains('App_BNav_tab_button_rotated'))button.classList.add('App_BNav_tab_button_rotated')
	button.setAttribute('onclick','BNAV_close_bottom_nav_content()')

	SNAV_select_zoom(document.querySelector('.App_SNav_select_zoom'))
}

function BNAV_close_bottom_nav_content(){
	APP_stop()
	var ES_button = document.getElementById("App_BNav_tab_ES")
	var ET_button = document.getElementById("App_BNav_tab_ET")
	var Key_button = document.getElementById("App_BNav_tab_keyboard")
	var mod_S_content = document.getElementById("App_BNav_mod_S")
	var mod_T_content = document.getElementById("App_BNav_mod_T")
	var keyboard_content = document.getElementById("App_BNav_keyboard")
	var calc_content = document.getElementById("App_BNav_calc")

	mod_S_content.style["display"] = "none"
	mod_T_content.style["display"] = "none"
	keyboard_content.style["display"] = "none"
	calc_content.style["display"] = "none"


	var button = document.querySelector('.App_BNav_tab_button')
	button.classList.remove('App_BNav_tab_button_rotated')
	button.setAttribute('onclick','BNAV_open_bottom_nav_content()')

	//XXX verify if necessary
	//BNAV_refresh_all_compas_button_values()
	//BNAV_refresh_PcLi()

	SNAV_select_zoom(document.querySelector('.App_SNav_select_zoom'))
}

function BNAV_swap_RE_PMC_button(){
	APP_stop()
	BNAV_show_RE_PMC()

	DATA_save_preferences_file()
	DATA_smart_redraw()
}

function BNAV_show_RE_PMC(){
	var App_RE = document.querySelector(".App_RE")
	var App_PMC = document.querySelector(".App_PMC")
	if(tabREbutton.checked){
		//App_RE.style["display"] = "block"
		App_RE.style["display"] = "flex"
		App_PMC.style["display"] = "none"
	} else if (tabPMCbutton.checked){
		App_RE.style["display"] = "none"
		//App_PMC.style["display"] = "block"
		App_PMC.style["display"] = "flex"
	}
}

function BNav_content_opened(){
	var bottom_contents = [...document.querySelectorAll(".App_BNav_content")]
	return bottom_contents.some(content=>{return !(content.style.display == 'none')})
}

