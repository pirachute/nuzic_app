//Implementation of T_line roules

//SEGMENT

function RE_break_segment_calculate_position(element,event){
	var bttn = element.querySelector("button")
	var segment_obj = element.closest(".App_RE_segment")
	var random_line_obj= segment_obj.querySelector(".App_RE_segment_line")
	var input_list= [...random_line_obj.querySelectorAll(".App_RE_inp_box")]
	var rect = element.getBoundingClientRect()

	//console.log(bttn)
	var x = event.clientX;     // Get the horizontal coordinate
	var y = event.clientY;     // Get the vertical coordinate
	var position = x-rect.left
	var global_zoom = Number(document.querySelector('.App_SNav_select_zoom').value)
	var RE_block = nuzic_block*global_zoom
	var index = Math.floor(position/RE_block)

	var finalPosition = index*nuzic_block

	//if iT index+1
	var d_index=0
	if(random_line_obj.classList.contains("App_RE_iT"))d_index=1

	if(index == 0 || index == input_list.length-1 || input_list[index+d_index].value==''){
		bttn.style ='display:none'
	}else {

		var elementIndex = index+d_index
		var position = 0
		input_list.find((inp,index)=>{
		if (index==elementIndex)return true
			if(inp.value!="")position++
		})

		var voice_obj = segment_obj.closest(".App_RE_voice")
		var voice_index = RE_read_voice_id(voice_obj)[1]
		var segment_list= [...voice_obj.querySelectorAll(".App_RE_segment")]
		var segment_index = segment_list.indexOf(segment_obj)

		//it is a non fractioned pulse and not first or last value of a segment, now....
		//control if segment is breakable in this point
		if(bttn.style.display=='flex'){
			//console.log("already shown, stop calculations")
			return
		}
		//verify if is fractioned pulse first and then if is all ok breaking here
		if(DATA_calculate_segment_break_in_object_index(voice_index,segment_index,position)){
			bttn.style='display:flex'
		}else{
			bttn.style ='display:none'
		}
	}
	bttn.style.left = `${finalPosition}px`
	bttn.value=index
}

function RE_exit_break_segment(element){
	var bttn = element.querySelector('button')
	bttn.style ='display:none'
}

//FRACTIONS

function RE_enter_new_fraction(element){
	APP_stop()
	var segment_obj = element.closest(".App_RE_segment")
	var voice_obj = segment_obj.closest(".App_RE_voice")
	var voice_index = RE_read_voice_id(voice_obj)[1]
	var segment_list= [...voice_obj.querySelectorAll(".App_RE_segment")]
	var segment_index = segment_list.indexOf(segment_obj)

	if(element.value == previous_value){
		return
	}

	var split = element.value.split('/');
	var fractComplex = Math.floor(split[0])
	var fractSimple = Math.floor(split[1])

	var fract_list= [...segment_obj.querySelectorAll(".App_RE_Tf>.App_RE_inp_box")]
	var position = fract_list.indexOf(element)

	var result = DATA_modify_fraction_value(voice_index,segment_index,position,fractComplex,fractSimple)

	if(result==false){
		element.value = previous_value
		APP_blink_error(element)
		return
	}
	RE_focus_on_object_index(voice_index,segment_index,"Tf",position,1)
}

function RE_break_fraction_range(button){
	APP_stop()
	var img = button.querySelector("img")
	if(img.style.display=='block'){
		//valid index
		var segment_obj = button.closest('.App_RE_segment')
		var voice_index = RE_read_voice_id(segment_obj.closest(".App_RE_voice"))[1]
		var segment_index = [...segment_obj.closest(".App_RE_voice_data").querySelectorAll(".App_RE_segment")].indexOf(segment_obj)
		var visible_line_list = [...segment_obj.querySelectorAll(".App_RE_segment_line")]
		var first_input_list= [...visible_line_list[0].querySelectorAll(".App_RE_inp_box")]
		//all the calculation if breakable is done by the spawning of the button
		var target_elementIndex = Number(button.value)

		var position = 0
		first_input_list.find((inp,index)=>{
			if (index==target_elementIndex)return true
			if(inp.value!="")position++
		})

		var success=[false]
		if(isOdd(target_elementIndex)){
			//cutting a iT
			//if shown only iT no need to -1 position
			if(!visible_line_list[0].classList.contains("App_RE_iT"))position--
			success = DATA_calculate_fraction_break_in_object_index(voice_index,segment_index,position,false)
		}else{
			success = DATA_calculate_fraction_break_in_object_index(voice_index,segment_index,position,true)
		}
		if(success[0])DATA_break_fraction_range(voice_index,segment_index,success[1],success[2])
	}
}

function RE_break_fraction_range_calculate_position(element,event){
	var img = element.querySelector("img")
	var segment_obj = element.closest(".App_RE_segment")

	var visible_line_list = [...segment_obj.querySelectorAll(".App_RE_segment_line")]

	var show_iT_frag=visible_line_list.some(line=>{return line.classList.contains("App_RE_iT")})
	var show_T_frag=visible_line_list.some(line=>{return !line.classList.contains("App_RE_iT") && !line.classList.contains("App_RE_Tf")})

	var first_input_list= [...visible_line_list[0].querySelectorAll(".App_RE_inp_box")]

	//var PaLine= segment.querySelector(".App_RE_Ts").children
	var rect_segment = segment_obj.getBoundingClientRect()
	var rect_element = element.getBoundingClientRect()

	var x = event.clientX;     // Get the horizontal coordinate
	var y = event.clientY;     // Get the vertical coordinate
	var position_px = x-rect_segment.left

	var global_zoom = Number(document.querySelector('.App_SNav_select_zoom').value)
	var RE_block = nuzic_block*global_zoom
	var elementIndex = Math.floor(position_px/RE_block)

	var finalPosition = Math.floor((x-rect_element.left)/RE_block)*nuzic_block
	//console.log(index)

	//already shown, stop calculations
	var old_index= parseInt(element.value)
	if(img.style.display=='block' && old_index==elementIndex)return

	//calculate the position
	var is_A_iT = false
	var is_A_T = false
	if(show_iT_frag && !show_T_frag){
		//only it is shown
		is_A_iT=first_input_list[elementIndex].value!=""
	}else{
		if(show_iT_frag){
			is_A_iT= first_input_list[elementIndex-1].value!=""
		}
		is_A_T= first_input_list[elementIndex].value!=""
	}

	if(!is_A_iT && !is_A_T){
		img.style ='display:none'
		return
	}

	var voice_obj = segment_obj.closest(".App_RE_voice")
	var voice_index = RE_read_voice_id(voice_obj)[1]
	var segment_list= [...voice_obj.querySelectorAll(".App_RE_segment")]
	var segment_index = segment_list.indexOf(segment_obj)

	var position = 0
	var index_last
	first_input_list.find((inp,index)=>{
		if (index==elementIndex)return true
		if(inp.value!="")position++
	})

	//if is a iT position is the next one, need to be lowered
	// if(is_A_iT)position--
	if(is_A_iT && show_T_frag)position--
	var success = DATA_calculate_fraction_break_in_object_index(voice_index,segment_index,position,is_A_T)

	if(success[0]){
		img.style ='display:block'
		img.style.left = `${finalPosition}px`
		element.value=elementIndex
	}else{
		img.style ='display:none'
	}
}

function RE_exit_break_fraction_range(element){
	var img = element.querySelector("img")
	img.style ='display:none'
}

function RE_join_fraction_ranges(element){
	var segment = element.closest(".App_RE_segment")
	var segment_list = [...segment.closest(".App_RE_voice_data").querySelectorAll(".App_RE_segment")]
	var segment_index = segment_list.indexOf(segment)
	var RE_Tf = segment.querySelector(".App_RE_Tf")
	var join_F_list = [...RE_Tf.querySelectorAll("input.App_RE_frac_join_button")]
	var first_fraction_index=join_F_list.indexOf(element)//mo need -1 bc first fraction doesnt have a join button
	var voice_index= Number(segment.closest(".App_RE_voice").getAttribute("value"))

	//with new data function
	DATA_join_fraction_ranges(voice_index,segment_index,first_fraction_index)
}

//TIME

function RE_enter_new_value_T(element,type){
	var string = element.value
	if(string==previous_value)return

	if(type=="Tm" && string==RE_superscript_to_Tm_string(previous_value)){
		//need to rewrite
		element.value=previous_value
		return
	}

	if(type=="Tm"){
		//need to rewrite
		var [elementIndex,input_list]=RE_element_index(element,".App_RE_inp_box")
		if(input_list[0].getAttribute("tm_triade")==""){
			element.value=previous_value
			return
		}
	}

	APP_stop()

	var segment_obj = element.closest(".App_RE_segment")
	var voice_obj = segment_obj.closest(".App_RE_voice")

	var voice_index = RE_read_voice_id(voice_obj)[1]
	var segment_list= [...voice_obj.querySelectorAll(".App_RE_segment")]
	var segment_index = segment_list.indexOf(segment_obj)

	var position
	var next=0

	//verify if is last cell
	if(element.classList.contains("App_RE_last_cell_editable_input")){
		if (string!="") {
			//modify segment longitude
			var [P,F]=RE_read_Time_Element(element,type)

			if(F!=0 || P==null){
				element.value=previous_value
				APP_blink_error(element)
				return
			}
			//position=RE_modify_segment_Ls(voice_index,segment_index,P,element)
			var success = DATA_change_Ls_segment(voice_index,segment_index,P)
			if(!success){
				element.value=previous_value
				APP_blink_error(element)
				return
			}
			position=-1
			next=0
		}else {
			element.value=previous_value
			APP_blink_error(element)
			return
		}
	}else{
		if(string==""){
			if(previous_value!=""){
				//delete values
				var [elementIndex,input_P_list]=RE_element_index(element,".App_RE_inp_box")
				position = 0
				input_P_list.find((inp,index)=>{
					if (index==elementIndex)return true
					if(inp.value!="")position++
				})
				var success = DATA_delete_object_index(voice_index,segment_index,position) //if fraction range note = "e"
				if(!success){
					element.value=previous_value
				}
				next=0
			}else{
				return
			}
		}else{
			//control correctness new value (range and fraction)
			var [P,F]=RE_read_Time_Element(element,type)
			if(P==null){
				element.value=previous_value
				APP_blink_error(element)
				return
			}
			var [elementIndex,input_P_list]=RE_element_index(element,".App_RE_inp_box")
			position = 0
			input_P_list.find((inp,index)=>{
				if (index==elementIndex)return true
				if(inp.value!="")position++
			})
			if(previous_value==""){
				//added new item
				var success = DATA_enter_new_object_index(voice_index,segment_index,position,P,F,-2,true)
				if(!success){
					element.value=previous_value
					APP_blink_error(element)
					return
				}
				next=1
				//if(SNAV_read_play_new_note())APP_play_this_note_number(note,instrument)
			}else {
				//changed item
				var [success,error_type,error_data] = DATA_modify_object_index_time(voice_index,segment_index, position, P,F)
				next=1
				if(!success){
					if(error_type==0){
						//simple
						element.value=previous_value
						APP_blink_error(element)
						return
					}
					if(error_type==1){
						//error on position, blink on prev fractioning and start prev frac
						element.value=previous_value
						APP_blink_error(element)
						var line_inp_list = [...element.closest(".App_RE_segment_line").querySelectorAll(".App_RE_inp_box")]
						var A_PF1_index=null
						var current_index =-1
						line_inp_list.some((inp_box,index)=>{
							if(inp_box.value!=""){
								current_index++
							}
							if(current_index==error_data.error_T_indexA){
								APP_blink_error(inp_box)
								return true
							}
							return false
						})

						var RE_Tf = segment_obj.querySelector(".App_RE_Tf")
						if(RE_Tf!=null){
							var input_F_list = [...RE_Tf.querySelectorAll(".App_RE_inp_box")]
							APP_blink_error(input_F_list[error_data.error_Tf_index])
						}
						return
					}
					if(error_type==2){
						//error on position, blink on fractioning and stop frac
						element.value=previous_value
						APP_blink_error(element)
						var line_inp_list = [...element.closest(".App_RE_segment_line").querySelectorAll(".App_RE_inp_box")]
						var current_index =-1
						line_inp_list.some((inp_box,index)=>{
							if(inp_box.value!=""){
								current_index++
							}
							if(current_index==error_data.error_T_indexB){
								APP_blink_error(inp_box)
								return true
							}
							return false
						})

						var RE_Tf = segment_obj.querySelector(".App_RE_Tf")
						if(RE_Tf!=null){
							var input_F_list = [...RE_Tf.querySelectorAll(".App_RE_inp_box")]
							APP_blink_error(input_F_list[error_data.error_Tf_index])
						}
						return
					}
				}
			}
		}
	}
	RE_focus_on_object_index(voice_index,segment_index,type,position,next)
}

//INTERVAL

function RE_enter_new_value_dT(element,type){
	if(element.value==previous_value) return
	APP_stop()

	var segment_obj = element.closest(".App_RE_segment")
	var voice_obj = segment_obj.closest(".App_RE_voice")

	var voice_index = RE_read_voice_id(voice_obj)[1]
	var segment_list= [...voice_obj.querySelectorAll(".App_RE_segment")]
	var segment_index = segment_list.indexOf(segment_obj)

	var position=0
	var next=0

	//find position
	var [elementIndex,input_iT_list]=RE_element_index(element,".App_RE_inp_box")
	input_iT_list.find((inp,index)=>{
		if (index==elementIndex)return true
		if(inp.value!="")position++
	})
	var [duration_iT,]=RE_read_Time_Element(element,type)
	if(duration_iT==null)duration_iT=0

	//determine if
	if(previous_value==""){
		//creating new interval
		if(duration_iT==0){
			element.value=previous_value
			return
		}

		//using P and F start NONONO
		//var success = DATA_insert_new_object_PA_iT(voice_index,segment_index,pulse_start,fraction_start,duration_iT,-2,true)
		//using position
		
		var [success,error_type,error_data] = DATA_insert_new_object_index_iT(voice_index,segment_index,position,duration_iT,-2,true)
		if(!success){
			//no space for interval
			if(error_type==1){
				var current_index =-1
				input_iT_list.some((inp_box,index)=>{
					if(inp_box.value!="")current_index++
					if(current_index==error_data.error_iT_index){
						//found last_iT_range
						APP_blink_error(inp_box)
						return true
					}
					return false
				})
			}
			element.value=previous_value
			APP_blink_error(element)
			return
		}
		next=1
	}else{
		//modify or deleting existing iT
		
		var [success,error_type,error_data] = DATA_modify_object_index_iT(voice_index,segment_index,position,duration_iT)

		//various case of insuccess
		if(!success){
			//cant deleting iT
			element.value=previous_value
			APP_blink_error(element)
			//if(error_type==0)//is the only element on the range
			if(error_type==1){
				//no space for interval
				var current_index =-1
				input_iT_list.some((inp_box,index)=>{
					if(inp_box.value!="")current_index++
					if(current_index==error_data.error_iT_index){
						//found last_iT_range
						APP_blink_error(inp_box)
						return true
					}
					return false
				})
			}
			if(error_type==2){
				//fraction range incompatible with new iT
				var RE_Tf = segment_obj.querySelector(".App_RE_Tf")
				if(RE_Tf!=null){
					var input_F_list = [...RE_Tf.querySelectorAll(".App_RE_inp_box")]
					APP_blink_error(input_F_list[error_data.error_Tf_index])
				}
			}
			if(error_type==3){
				//voices neopulse doesn't permit change of the range
				//blink all the RE_neopulse_inp_box
				var neopulse_elements_list = [...document.querySelectorAll(".App_RE_neopulse_inp_box")]
				neopulse_elements_list.forEach(item=>{
					APP_blink_error(item)
				})
			}
			return
		}
		next=0
		if(duration_iT!=0)next=1
	}
	RE_focus_on_object_index(voice_index,segment_index,type,position,next)
}

function RE_enter_new_value_dT_keyboard(value,type){
	var element =''
	element.value=value
	var type='iT'
	console.log(element)
	console.log(type)
	if(element.value==previous_value) return
	APP_stop()

	var segment_obj = element.closest(".App_RE_segment")
	var voice_obj = segment_obj.closest(".App_RE_voice")

	var voice_index = RE_read_voice_id(voice_obj)[1]
	var segment_list= [...voice_obj.querySelectorAll(".App_RE_segment")]
	var segment_index = segment_list.indexOf(segment_obj)

	var position=0
	var next=0

	//find position
	var [elementIndex,input_iT_list]=RE_element_index(element,".App_RE_inp_box")
	input_iT_list.find((inp,index)=>{
		if (index==elementIndex)return true
		if(inp.value!="")position++
	})
	var [duration_iT,]=RE_read_Time_Element(element,type)
	if(duration_iT==null)duration_iT=0

	//determine if
	if(previous_value==""){
		//creating new interval
		if(duration_iT==0){
			element.value=previous_value
			return
		}

		//using P and F start NONONO
		//var success = DATA_insert_new_object_PA_iT(voice_index,segment_index,pulse_start,fraction_start,duration_iT,-2,true)
		//using position
		
		var [success,error_type,error_data] = DATA_insert_new_object_index_iT(voice_index,segment_index,position,duration_iT,-2,true)
		if(!success){
			//no space for interval
			if(error_type==1){
				var current_index =-1
				input_iT_list.some((inp_box,index)=>{
					if(inp_box.value!="")current_index++
					if(current_index==error_data.error_iT_index){
						//found last_iT_range
						APP_blink_error(inp_box)
						return true
					}
					return false
				})
			}
			element.value=previous_value
			APP_blink_error(element)
			return
		}
		next=1
	}else{
		//modify or deleting existing iT
		
		var [success,error_type,error_data] = DATA_modify_object_index_iT(voice_index,segment_index,position,duration_iT)

		//various case of insuccess
		if(!success){
			//cant deleting iT
			element.value=previous_value
			APP_blink_error(element)
			//if(error_type==0)//is the only element on the range
			if(error_type==1){
				//no space for interval
				var current_index =-1
				input_iT_list.some((inp_box,index)=>{
					if(inp_box.value!="")current_index++
					if(current_index==error_data.error_iT_index){
						//found last_iT_range
						APP_blink_error(inp_box)
						return true
					}
					return false
				})
			}
			if(error_type==2){
				//fraction range incompatible with new iT
				var RE_Tf = segment_obj.querySelector(".App_RE_Tf")
				if(RE_Tf!=null){
					var input_F_list = [...RE_Tf.querySelectorAll(".App_RE_inp_box")]
					APP_blink_error(input_F_list[error_data.error_Tf_index])
				}
			}
			if(error_type==3){
				//voices neopulse doesn't permit change of the range
				//blink all the RE_neopulse_inp_box
				var neopulse_elements_list = [...document.querySelectorAll(".App_RE_neopulse_inp_box")]
				neopulse_elements_list.forEach(item=>{
					APP_blink_error(item)
				})
			}
			return
		}
		next=0
		if(duration_iT!=0)next=1
	}
	RE_focus_on_object_index(voice_index,segment_index,type,position,next)
}

function RE_element_is_fraction_range(element){
	//return true if element is part of a range of fractioning
	var [elementIndex,input_P_list]=RE_element_index(element,".App_RE_inp_box")
	var position = 0
	input_P_list.find((inp,index)=>{
	if (index==elementIndex)return true
		if(inp.value!="")position++
	})

	if(position==0)return true
	if(position==input_P_list.length-1)return true

	var segment_obj = element.closest(".App_RE_segment")
	var voice_obj = segment_obj.closest(".App_RE_voice")
	var voice_index = RE_read_voice_id(voice_obj)[1]
	var segment_list= [...voice_obj.querySelectorAll(".App_RE_segment")]
	var segment_index = segment_list.indexOf(segment_obj)
	var result = DATA_get_object_index_info(voice_index,segment_index,position,0,true)
	return result.isRange
}

function RE_write_segment_Time_lines(segment_obj,line_list_states,index_columns,Psg_segment_start,segment_data,N_NP,D_NP){
	//writing single time lines
	var neopulse = N_NP!=1 || D_NP!=1

	if(line_list_states.Ts ){
		var RE_Ts = segment_obj.querySelector(".App_RE_Ts")
		var input_Ps_list = [...RE_Ts.querySelectorAll("input")]
		var prev_pulse=-1
		index_columns.forEach((column_number,index)=>{
			var string = ""
			if(segment_data.time[index].P!=prev_pulse){
				string+=segment_data.time[index].P
				prev_pulse=segment_data.time[index].P
			}
			if(segment_data.time[index].F!=0){
				string+="."+segment_data.time[index].F
			}
			if(input_Ps_list[column_number]==null)console.log(input_Ps_list)
			input_Ps_list[column_number].value= string
		})
	}

	if(line_list_states.Ta ){
		var Ta_line = segment_obj.querySelector(".App_RE_Ta")
		var input_list= [...Ta_line.querySelectorAll(".App_RE_inp_box")]
		if(neopulse){
			index_columns.forEach((column_number,index)=>{
				input_list[column_number].value= no_value
				//input_list[column_number].setAttribute("tm_triade","")
			})
		}else{
			var prev_pulse=-1
			index_columns.forEach((column_number,index)=>{
				var string = ""
				if(segment_data.time[index].P!=prev_pulse){
					string+=(segment_data.time[index].P+Psg_segment_start)
					prev_pulse=segment_data.time[index].P
				}
				if(segment_data.time[index].F!=0){
					string+="."+segment_data.time[index].F
				}
				input_list[column_number].value= string
			})
		}
	}

	// Tm
	if(line_list_states.Tm){
		var Tm_line = segment_obj.querySelector(".App_RE_Tm")
		var input_list= [...Tm_line.querySelectorAll(".App_RE_inp_box")]
		if(neopulse){
			index_columns.forEach((column_number,index)=>{
				input_list[column_number].value= no_value
			})
		}else{
			previous_element=null
			index_columns.forEach((column_number,index)=>{
				if(Psg_segment_start==null)console.error("Write time element ERROR: not entered segment_starting_point value")
				var string = ""
				//using DATABASE
				//var data = DATA_get_current_state_point(false)
				//var compas_data = data.compas
				//XXX DANGER!!! but faster!!!
				//var compas_data = state_point_array[current_state_point_index].compas
				var compas_data = DATA_get_compas_sequence()
				var last_compas= compas_data.slice(-1)[0].compas_values
				var compas_length_value = last_compas[0]+last_compas[1]*last_compas[2]
				//it int_p inside a compas
				//take into account that pulse is part of a segment un x position

				//No problem using Pa, no compas if neopulse!!! Psg===Pa
				var Pa_element = Psg_segment_start + segment_data.time[index].P

				if(Pa_element <compas_length_value && compas_length_value!=0){
					//element inside a module
					var compas_type = compas_data.filter(compas=>{
						return compas.compas_values[0] <= Pa_element
					})

					//var current_compas_type = compas_type.pop()
					var current_compas_type = compas_type.pop()

					var current_compas_number = compas_type.reduce((prop,item)=>{
						return item.compas_values[2]+prop
					},0)

					var dP = Pa_element - current_compas_type.compas_values[0]
					var mod = current_compas_type.compas_values[1]
					var dC = Math.floor(dP/mod)
					var resto = dP
					if(dC!=0) resto = dP%mod
					current_compas_number+= dC
					//  <p>This text contains <sup>superscript</sup> text.</p>

					//warning first element
					//verify if it has module, if not read previous value
					var showcompas = true
					var showpulse = true
					//here retrive value
					if(previous_element!=null){
						var arrayOfStrings = previous_element.getAttribute("tm_triade").split(",")
						var arrayOfNumbers = arrayOfStrings.map(Number)
						if(arrayOfNumbers[0]==resto)showpulse=false

						if(arrayOfNumbers[2]==current_compas_number)showcompas=false
					}

					if(showpulse || showcompas){
						string = resto
					}

					if(segment_data.time[index].F!=0){
						string += "."+segment_data.time[index].F
					}

					if(showcompas){
						//stringT += "<sup>"+current_compas_number+"</sup>"
						string += RE_string_to_superscript(current_compas_number.toString())
					}

					// element.value = [resto,int_f,current_compas_number]
					//now is a new habdy attribute
					input_list[column_number].setAttribute("tm_triade",[resto,segment_data.time[index].F,current_compas_number])
					input_list[column_number].classList.remove("RE_time_empty_cell")
				}else{
					//outside or no compasses
					//stringT = "-"
					string = no_value
					input_list[column_number].setAttribute("tm_triade","")
				}

				input_list[column_number].value= string
				previous_element=input_list[column_number]
			})
		}
	}

	// iT
	if(line_list_states.iT){
		var iT_line = segment_obj.querySelector(".App_RE_iT")
		var input_list= [...iT_line.querySelectorAll(".App_RE_inp_box")]
		var index_columns_iT = index_columns.map(i=>{return i+1})
		index_columns_iT.pop()
		index_columns_iT.forEach((column_number,index)=>{
			var A_element = segment_data.time[index]
			var B_element = segment_data.time[index+1]
			//find fraction
			var current_fraction = segment_data.fraction.find(fraction=>{return fraction.stop>A_element.P})
			//var value = (BP - AP)  * fractSimple / fractComplex + (BF - AF)
			//var value = (B_element.P - A_element.P)  * current_fraction.D / current_fraction.N + (B_element.F - A_element.F)
			//input_list[column_number].value= value
			input_list[column_number].value= DATA_calculate_interval_PA_PB(A_element,B_element,current_fraction)
		})
	}
}

function RE_read_Time_Element(element, line_id=null){
	//translate element.value to P element absolute
	if(line_id==null){
		console.error("read_time without line_id")
		return
	}

	var pulse = -1
	var fraction = -1
	switch(line_id) {
	case "Ts":
		var string = element.value
		if(string==="") return [null,null]

		// Abs
		//code verify if prev element has same int_p and eventually omit info
		var split = element.value.split('.')
		if(split[0].length==0){
			var previous_element = _find_prev_element(element)
			var [prev_P,]=RE_read_Time_Element(previous_element, "Ts")
			split[0]=prev_P
		}
		var pulse = Math.floor(split[0])
		var fraction = Math.floor(split[1]) //give 0
		if (split[1]==null) fraction=0 //never enter here , never null , is length ==0
		break;
	case "Tm":
		// mod
		var string = element.value
		var segment_obj = element.closest(".App_RE_segment")
		var voice_obj = element.closest(".App_RE_voice")

		if(string=="") {
			return [null,null]
		}else{
			//translate
			string = RE_superscript_to_Tm_string(string)
			var splitted = string.split('c')
			var compas_number = parseInt(splitted[1])
			var [pulse_m_string , fraction_m_string] = splitted[0].split('.')
			var pulse_m = parseInt(pulse_m_string)
			var fraction_m = parseInt(fraction_m_string)
			//case fraction undetermined = 0
			if(isNaN(fraction_m))fraction_m=0

			//case compas_number is undetermined == last one
			if(isNaN(compas_number)){
				//using last compas
				var previous_element = _find_prev_element(element)
				if (previous_element==null){
					pulse=0
					fraction = 0
					break
				}

				//special function
				compas_number=_find_previous_compas_number(element)

				function _find_previous_compas_number(element){
					var previous_element = _find_prev_element(element)
					//if previous_element is null...it never enter here

					if (previous_element==null){
						//console.error("Not found prev element compas")
						return null
					}

					var [previous_pulse_m_string,previous_fraction_m_string,previous_compas_number_string] = previous_element.getAttribute("tm_triade").split(',')
					previous_pulse_m = parseInt(previous_pulse_m_string)
					previous_fraction_m = parseInt(previous_fraction_m_string)
					previous_compas_number = parseInt(previous_compas_number_string)

					if(isNaN(previous_compas_number)){
						previous_compas_number=_find_previous_compas_number(previous_element)
					}
					return previous_compas_number
				}

				//if neither pulse was specified == last modular pulse
				if(isNaN(pulse_m)){
					//using last modular pulse
					pulse_m = _find_previous_compas_pulse_number(element)

					function _find_previous_compas_pulse_number(element){
						var previous_element = _find_prev_element(element)
						//if previous_element is null...it never enter here

						if (previous_element==null){
							console.error("Not found prev element pulse")
							return 0
						}

						var [previous_pulse_m_string,previous_fraction_m_string,previous_compas_number_string] = previous_element.getAttribute("tm_triade").split(',')
						previous_pulse_m = parseInt(previous_pulse_m_string)

						if(isNaN(previous_pulse_m)){
							previous_pulse_m=_find_previous_compas_pulse_number(previous_element)
						}
						return previous_pulse_m
					}
				}
			}else{
				//compas typed but not pulse, i suppose you wanted a 0 pulse
				if(isNaN(pulse_m))pulse_m=0
			}

			var arr = RE_calcolate_segment_time_from_compas_values(pulse_m,fraction_m,compas_number,segment_obj,voice_obj)
			pulse = arr[0]
			fraction = arr[1]
		}
		break;
	case "Ta":
		// grade abs
		console.error("Ta not implemented")
		pulse = null
		fraction = null
		//code XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
		break;
	case "iT":
		// distances abs
		var string = element.value
		if(string==="") return [null,null]
		var pulse = Math.floor(element.value)
		var fraction = null
		break;
	default:
		console.error("Error reading case Time writing roules")
	}
	//console.log("pulse: "+pulse+" fraction: "+fraction)
	return [pulse, fraction]

	function _find_prev_element(element, string="input"){
		var parent = element.parentNode
		var input_list = [...parent.querySelectorAll(string)]
		var elementIndex = input_list.indexOf(element)
		var index_previous = -1

		for (var i=elementIndex-1; i>=0;i--){
			var value = input_list[i].value
			if(value!=""){
				//element not void
				index_previous=i
				i=-1
			}
		}

		if (index_previous>=0){
			return input_list[index_previous]
		}else{
			return null
		}
	}
}

//function that calculate the current segment absolute pulse and fraction given a compas time coordinate
function RE_calcolate_segment_time_from_compas_values(pulse_m,fraction_m,compas_number,segment_obj,voice_obj){
	//find starting time of the segment
	if(compas_number==null){
		console.log("cannot determinate previous compas number")
		return [null,null]
	}

	var [segment_index,segment_list]=RE_element_index(segment_obj,".App_RE_segment")
	var [voice_index,voice_list]=RE_element_index(voice_obj,".App_RE_voice")
	var data = DATA_get_current_state_point(true)
	var voice_data_mod = data.voice_data.find(item=>{
		return item.voice_index==voice_index
	})
	var all_segment_data= voice_data_mod.data.segment_data

	var segment_starting_pulse = 0
	all_segment_data.find((segment_data,index)=>{
		if(index==segment_index){
			return true
		}else{
			segment_starting_pulse+=segment_data.time.slice(-1)[0].P
		}
	})

	//calculating pulse position
	//getting current compas Lc
	var [compas_start_at, current_Lc] = BNAV_compas_number_to_Properties(compas_number)

	if(compas_start_at==null){
		//compas doesnt exist
		return [null,null]
	}

	//verify if current_Lc!=-1 and pulse_m tyed is < Lc
	if(current_Lc<2 || pulse_m>=current_Lc){
		return [null,null]
	} else {
		if(compas_start_at+pulse_m-segment_starting_pulse>0 && fraction_m>=0){
			return [compas_start_at+pulse_m-segment_starting_pulse,fraction_m]
		}else{
			return [null,null]
		}
	}
}


