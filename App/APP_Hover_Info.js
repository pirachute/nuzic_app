//global variables in Global_variables.js

function Hover_area_enter(element){
	if(!Read_hover_labels())return

	if(!element.hasAttribute('data-value'))return

	// highlight the mouseenter target
	var position = element.getBoundingClientRect()
	// console.log(position)
	// console.log(element.getAttribute('labelTimeOut'))

	var hover_label = document.querySelector("#App_hover_label")
	hover_label.style.display = "none"

	//init label hover
	var hover_label_title = hover_label.querySelector("#App_hover_label_title")
	var hover_label_title_button = hover_label_title.querySelector("button")
	if(!hover_label_title_button.classList.contains("closed"))hover_label_title_button.classList.add("closed")
	hover_label_title_button.style="display:none"

	var hover_title_text = hover_label_title.querySelector("p")
	hover_title_text.innerHTML = ""

	var hover_label_body = hover_label.querySelector("#App_hover_label_body")
	hover_label_body.innerHTML = ""
	hover_label_body.style.display="none"

	//data labelArray in label hover
	var labelID = element.getAttribute('data-value')
	var labelIndex = labelsArray.findIndex(x => x.id === labelID)
	if(labelIndex==-1)return

	hover_title_text.innerHTML = labelsArray[labelIndex].title
	hover_label_body.innerHTML = labelsArray[labelIndex].text

	if(labelsArray[labelIndex].text != ""){
		hover_label_title_button.style=""
	}

	//position label hover
	var posx = position.x+position.width
	var posy = position.y
	hover_label.style.left = `${posx}px`
	hover_label.style.top = `${posy}px`

	//display label hover

	//hover_label.ready

	if(hover_label.ready){
		//wait 200ms to show
		var timeout_id1 =  setTimeout(function(){
			hover_label.ready = true
			hover_label.style.display = "block"
		}, 200)
		hover_label.timeout_id1 = timeout_id1
	}else{
		//wait 1000ms to show
		var timeout_id1 =  setTimeout(function(){
			hover_label.ready = true
			hover_label.style.display = "block"
		}, 1000)
		hover_label.timeout_id1 = timeout_id1
	}

	//avoid change label readiness if moving in after entering from other label
	var hover_label_timeout_id2 = hover_label.timeout_id2
	if(hover_label_timeout_id2!="")clearTimeout(hover_label_timeout_id2)
	hover_label.timeout_id2 = ""

	//avoid hiding label
	var hover_label_timeout_id3 = hover_label.timeout_id3
	if(hover_label_timeout_id3!="")clearTimeout(hover_label_timeout_id3)
	hover_label.timeout_id3 = ""
}

function Hover_area_exit(element){
	if(!Read_hover_labels())return

	if(!element.hasAttribute('data-value'))return

	var hover_label = document.querySelector("#App_hover_label")

	//avoid show/change label if moving in-out mouse fast
	var hover_label_timeout_id1 = hover_label.timeout_id1
	if(hover_label_timeout_id1!="")clearTimeout(hover_label_timeout_id1)
	hover_label.timeout_show=""

	//start timeout ready off
	var timeout_id2 = setTimeout(function(){
		hover_label.ready = false
	}, 500)
	hover_label.timeout_id2 = timeout_id2

	//start timeout hide label
	var timeout_id3 = setTimeout(function(){
		hover_label.style.display = "none"
	}, 100)
	hover_label.timeout_id3 = timeout_id3
}

function Hover_label_enter(){
	var hover_label = document.querySelector("#App_hover_label")

	//avoid change label readiness if moving in after entering from other label
	var hover_label_timeout_id2 = hover_label.timeout_id2
	if(hover_label_timeout_id2!="")clearTimeout(hover_label_timeout_id2)
	hover_label.timeout_id2 = ""

	//avoid hiding label
	var hover_label_timeout_id3 = hover_label.timeout_id3
	if(hover_label_timeout_id3!="")clearTimeout(hover_label_timeout_id3)
	hover_label.timeout_id3 = ""
}

function Hover_label_exit(){

	var hover_label = document.querySelector("#App_hover_label")

	//avoid show/change label if moving in-out mouse fast
	var hover_label_timeout_id1 = hover_label.timeout_id1
	if(hover_label_timeout_id1!="")clearTimeout(hover_label_timeout_id1)
	hover_label.timeout_show=""

	//start timeout ready off
	var timeout_id2 = setTimeout(function(){
		hover_label.ready = false
	}, 500)
	hover_label.timeout_id2 = timeout_id2

	//start timeout hide label
	var timeout_id3 = setTimeout(function(){
		hover_label.style.display = "none"
	}, 100)
	hover_label.timeout_id3 = timeout_id3
}

function Open_close_hover_label_body(){
	var hover_label = document.querySelector("#App_hover_label")
	var hover_label_title_button = hover_label.querySelector("button")
	var hover_label_body = hover_label.querySelector("#App_hover_label_body")

	if(hover_label_title_button.classList.contains("closed")){
		//open
		hover_label_title_button.classList.remove("closed")
		hover_label_body.style.display = "inline-block"
	}else{
		//close
		hover_label_title_button.classList.add("closed")
		hover_label_body.style.display = "none"
	}

	hover_label_title_button.style="none"
}

function Switch_hover_labels(){
	//reversed because it is not yet switched
	var pressed = Read_hover_labels()
	if(pressed){
		Set_hover_labels(false)
	}else{
		Set_hover_labels(true)
	}
	//Save state preferences
	DATA_save_preferences_file()
}

function Read_hover_labels() {
	var button = document.getElementById("activateHoverButton")
	return (button.value === "true")
}

function Set_hover_labels(value) {
	var button = document.getElementById("activateHoverButton")
	button.value=value
	if(value){
		button.innerHTML='On'
	}else{
		button.innerHTML='Off'
	}
}

