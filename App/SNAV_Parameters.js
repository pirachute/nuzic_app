function SNAV_enter_on_segment_parameters(element){
	var parameters = document.querySelector('.App_SNav_parameters')
	if(parameters.style.display == 'flex'){
		var voice = element.closest('.App_RE_voice')
		var [voice_name,voice_index] = RE_read_voice_id(voice)

		//copy of database voice
		var current_voice_data_selected=DATA_get_current_state_point(false).voice_data.find(voice=>{return voice.voice_index==voice_index})
		if(current_voice_data_selected==null)return

		var time_array=current_voice_data_selected.data.midi_data.time
		//add end idea
		time_array.push(Li*60/PPM)
		//var freq_array= current_voice_data_selected.data.midi_data.note_freq
		var time_segment_change = DATA_list_voice_data_segment_change(current_voice_data_selected)

		var voiceInp = document.getElementById('parVoice')
		var segmentInp = document.getElementById('parSegment')

		//find segment
		var segment = element.closest(".App_RE_segment")
		var voice_data = segment.closest(".App_RE_voice_data")

		//find segment index
		var segment_list = [...voice_data.querySelectorAll(".App_RE_segment")]
		var segment_index= segment_list.indexOf(segment)

		var data = current_voice_data_selected.data.segment_data[segment_index]

		voiceInp.value= voice_name
		segmentInp.value = `s.${segment_index}`

		var segmentTimeArr

		if(segment_index == time_segment_change.length-1){
			//last segment
			segmentTimeArr = time_array.slice(time_array.indexOf(time_segment_change[segment_index]),time_array.length)
		} else {
			segmentTimeArr = time_array.slice(time_array.indexOf(time_segment_change[segment_index]),time_array.indexOf(time_segment_change[segment_index+1])+1)
		}
		SNAV_parameters_length(data)
		SNAV_parameters_time_quantity(data)
		SNAV_parameters_diferentiated_quantity(data, segmentTimeArr)
		SNAV_parameters_diferentiated_quantity_fr(data)
		SNAV_parameters_temporal_variety(data,segmentTimeArr, segmentTimeArr)
		SNAV_parameters_temporal_density(data)
		SNAV_parameters_temporal_stdDeviation(data, segmentTimeArr)
		SNAV_parameters_sound_range(data)
		SNAV_parameters_sound_quantity(data)
		SNAV_parameters_sound_diferentiated_quantity(data)
		SNAV_parameters_sound_diferentiated_quantity_iS(data)
		SNAV_parameters_sound_resulting_interval(data)
		SNAV_parameters_sound_variety(data)
		SNAV_parameters_sound_variety_iS(data)
		SNAV_parameters_sound_density(data)
	}
}

function SNAV_parameters_length(array){
	var input = document.getElementById('parTLengthInput')
	input.value= array.time[array.time.length-1].P

	if(isNaN(array.time[array.time.length-1].P)){
		//console.log('?')
	}
	return array.time[array.time.length-1].P
}

function SNAV_parameters_time_quantity(array){
	var input = document.getElementById('parTQuantityInput')
	//console.log(array.time)
	input.value=array.time.length-1
	return array.time.length-1
}

function SNAV_parameters_diferentiated_quantity(array,timesArr){

	if(array.fraction.length == 1){
		var iTArray =[]
	for(i=array.time.length-1;i>0;i--){
		iTArray.push(array.time[i].P-array.time[i-1].P)
	}
	//console.log(iTArray)
	iTArray.sort((a, b) => a - b);

	counter = 1;
	for (i = 1; i < iTArray.length; i++) {
		if (iTArray[i] != iTArray[i - 1]) {
			counter++;
		}
	}
	var input = document.getElementById('parTDiferentiatedQInput')
	input.value=counter
	return counter
	} else {
		//Read voice values y quieres el primer y quinto parametro
		var iTArray =[]
		//console.log(timesArr)
		for(i=timesArr.length-1;i>0;i--){
			var num =timesArr[i]-timesArr[i-1]
			iTArray.push(num.toFixed(4))
		}
		//console.log(iTArray)
		iTArray.sort((a, b) => a - b);
		//console.log(iTArray)
		counter = 1;
	 	for (i = 1; i < iTArray.length; i++) {
			if (iTArray[i] != iTArray[i - 1]) {
				counter++;
			}
		}
		var input = document.getElementById('parTDiferentiatedQInput')
		input.value=counter
		return counter
	}

}
function SNAV_parameters_diferentiated_quantity_fr(array){

	var frArray=[]
	for(i=array.fraction.length-1;i>=0;i--){
		frArray.push(eval(array.fraction[i][0]))
	}
	//Separar en diferentes arrays en funcion del fraccionamiento y hacer por esparado
	frArray.sort((a, b) => a - b);

	counter = 1;
	for (i = 1; i < frArray.length; i++) {
		if (frArray[i] != frArray[i - 1]) {
			counter++;
		}
	}
	var input = document.getElementById('parTFrQuantityInput')
	input.value=counter
}

function SNAV_parameters_temporal_variety(array, timeArr){

	var differentiatedQuantity = SNAV_parameters_diferentiated_quantity(array,timeArr)
	var timeQuantity = SNAV_parameters_time_quantity(array)
	var input = document.getElementById('parTVarietyInput')

	input.value=(differentiatedQuantity/timeQuantity).toFixed(3)

}
function SNAV_parameters_temporal_density(array){

	var length = SNAV_parameters_length(array)

	var timeQuantity = SNAV_parameters_time_quantity(array)
	var input = document.getElementById('parTDensityInput')

	input.value=(timeQuantity/length).toFixed(3)

}

function SNAV_parameters_temporal_stdDeviation(array, timesArr){
	var iTArray =[]
	var median = 0

	var stdDev = 0

	for(i=timesArr.length-1;i>0;i--){
		var num =timesArr[i]-timesArr[i-1]
		iTArray.push(num.toFixed(4))
		median += parseFloat(num.toFixed(4))
	}
	median = median/iTArray.length

	for(i=0;i<iTArray.length;i++){
		stdDev += ((parseFloat(iTArray[i])-median)**2)
	}
	stdDev = Math.sqrt(stdDev/median)

	var input = document.getElementById('parTDeviationInput')
	input.value= stdDev.toFixed(3)
}

function SNAV_parameters_sound_range(array){
	//console.log(array.note)
	/* console.log(array)
	var noNegativeArr = array.note.filter(el => el>0)
	console.log(noNegativeArr) */
	var numberArr = array.note.filter(el => el>0)
	numberArr.sort((a, b) => a - b);
	//console.log(numberArr)
	//console.log(numberArr.length)
	var input = document.getElementById('parSRangeInput')
	/* if(numberArr.length == 0){
		input.value = 0
		return 0
	}else  */if(isNaN(numberArr[numberArr.length-1]-numberArr[0]) == false){
		input.value=numberArr[numberArr.length-1]-numberArr[0]
		return numberArr[numberArr.length-1]-numberArr[0]
	} else {
		input.value = 0
		return 0
	}
}


function SNAV_parameters_sound_quantity(array){
	//var noNegativeArr = array.note.filter(el => el>0)
	var numberArr = array.note.filter(el => el>0)
	var input = document.getElementById('parSQuantityInput')
	input.value = numberArr.length
	return numberArr.length
}

function SNAV_parameters_sound_diferentiated_quantity(array){
	//var noNegativeArr = array.note.filter(el => el>0)
	var numberArr = array.note.filter(el => el>0)
	var input = document.getElementById('parSDiferentiatedQInput')
	var counter = 1;
	//console.log(numberArr.length)
	if(numberArr.length ==0){

		input.value = 0
		return 0
	}else{

	numberArr.sort((a, b) => a - b);

	//counter = 1;
	for (i = 1; i < numberArr.length; i++) {
		if (numberArr[i] != numberArr[i - 1]) {
			counter++;
		}
	}
	//console.log(counter)
	input.value=counter
	return counter
}
}

function SNAV_parameters_sound_diferentiated_quantity_iS(array){
	var numberArr = array.note.filter(el => el>0)
	var input = document.getElementById('parSDiferentiatedQISInput')
	if(numberArr.length ==0){
		input.value = 0
		return 0
	} else{
	var isArr = []
	for(i=1; i<numberArr.length;i++){
		isArr.push(Math.abs(numberArr[i]-numberArr[i-1]))
	}
	//console.log(isArr)
	isArr.sort((a, b) => a - b);
	counter = 1;
	for (i = 1; i < isArr.length; i++) {
		if (isArr[i] != isArr[i - 1]) {
			counter++;
		}
	}
	input.value=counter
	return counter
}
}

function SNAV_parameters_sound_resulting_interval(array){
	var numberArr = array.note.filter(el => el>0)
	var input = document.getElementById('parSResultingIntervalInput')

	if(isNaN(numberArr[numberArr.length-1]-numberArr[0]) == false){
		input.value=numberArr[numberArr.length-1]-numberArr[0]
	} else {
		input.value = 0
		return
	}
}

function SNAV_parameters_sound_variety(array){
	var differentiatedQuantity = SNAV_parameters_sound_diferentiated_quantity(array)
	var soundQuantity = SNAV_parameters_sound_quantity(array)
	var input = document.getElementById('parSVarietyInput')
	if(soundQuantity != 0){

		input.value=(differentiatedQuantity/soundQuantity).toFixed(3)
	} else {
		input.value=0
		return
	}
}

function SNAV_parameters_sound_variety_iS(array){
	var differentiatedQuantity = SNAV_parameters_sound_diferentiated_quantity_iS(array)
	var soundQuantity = SNAV_parameters_sound_quantity(array)
	var input = document.getElementById('parSVarietyISInput')

	if(soundQuantity != 0){
		input.value=(differentiatedQuantity/soundQuantity).toFixed(3)
	} else {
		input.value = 0
		return
	}
}

function SNAV_parameters_sound_density(array){
	var range = SNAV_parameters_sound_range(array)
	var input = document.getElementById('parSDensityInput')
	if(range==0){
		input.value= 0
		return
	}else{
		var soundQuantity = SNAV_parameters_sound_quantity(array)
		var result=soundQuantity/range

		if(isNaN(result) == false){
			input.value= result.toFixed(3)
		} else {
			input.value= 0
			return
		}
	}
}

function Voice_values_to_parameters(voice){
	console.error("OBSOLETE. USE DATABASE")
}



