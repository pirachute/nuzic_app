function RE_write_C_line_values(){
	var C_line = document.querySelector(".App_RE_C")
	var compas_values = DATA_get_compas_sequence()
	var compas_valuesWithReps = []
	var counterCompasReps = 0
	for(i=0;i<compas_values.length;i++){
		compas_values[i].compas_values[0] = counterCompasReps
		compas_valuesWithReps.push(compas_values[i])
		counterCompasReps+=compas_values[i].compas_values[1]
		if(compas_values[i].compas_values[2]!=1){
			for(j=1;j<compas_values[i].compas_values[2];j++){
				var tempCompas = {compas_values:[counterCompasReps,compas_values[i].compas_values[1],compas_values[i].compas_values[2],compas_values[i].compas_values[3],compas_values[i].compas_values[4],true]}
				compas_valuesWithReps.push(tempCompas)
				counterCompasReps+=tempCompas.compas_values[1]
			}
		}
	}

	var wantedPositions = []
	//add final time
	var time = 0
	var compas_pulse = 0
	var current_time = 0
	compas_valuesWithReps.forEach((item) => {
		//console.log(item.compas_values)
		var duration = parseInt(item.compas_values[1])
		// console.log(duration)
		// time_compas_end = time + duration
		var time_compas_end = DATA_calculate_exact_time(compas_pulse,duration,0,1,1,1,1)
		var index_compas_global_time = RE_global_time_all_changes.indexOf(time)
		var copyRE_global_time_segment_change =(JSON.parse(JSON.stringify(RE_global_time_segment_change)))
		copyRE_global_time_segment_change.pop()
		var times_compas_segment_change = copyRE_global_time_segment_change.filter((stime) => {
			return stime <= time;
		})

		var wanted_position =index_compas_global_time * 2 + (times_compas_segment_change.length - 1) * 2
		wantedPositions.push(wanted_position)
		current_time = time
		time = time_compas_end
		compas_pulse += duration
	})

	C_line.innerHTML = ''

	var lastElementCounter = 0
	compas_valuesWithReps.forEach((element, index) => {
		var C_button = document.createElement('button')
		C_button.classList.add('App_RE_C_button')
		//var C_button_left =  nuzic_block * (element.compas_values[0] - index)
		var C_button_left = nuzic_block*(wantedPositions[index]+1)
		C_button.style.left = `${C_button_left}px`
		C_button.value = `{"C_values":[${element.compas_values[0]},${index},${element.compas_values[1]},[0],true]}`
		if(element.compas_values.length == 6){
			C_button.value = `{"C_values":[${element.compas_values[0]},${index},${element.compas_values[1]},[0],false]}`
		}
		C_button.onclick= function(){BNAV_show_compas_input(C_button)}
		//C_button.onclick= function(){console.log('hola')}
		if(element.compas_values.length==5){
			C_button.innerHTML = `
								 <div class="App_RE_C_inp_box">
									<div class="App_RE_C_value">
										<p class="App_RE_sub_C_value">${index}</p>
									</div>
									<div class="App_RE_C_range">
										<p class="App_RE_sub_C_range">${element.compas_values[1]}</p>
									</div>
								</div>
			`
			C_line.appendChild(C_button)
			}else {
				C_button.innerHTML = `
				<div class="App_RE_C_inp_box">
					<div class="App_RE_C_value">
						<p class="App_RE_sub_C_value">${index}</p>
					</div>
				</div>
				`
				C_line.appendChild(C_button)
			}
		if(index == compas_values.length-1){
			lastElementCounter = C_button_left
		}
	})
	var scrollDiv = document.createElement('div')
	scrollDiv.classList.add('App_RE_SC_scrollDiv')
	C_line.appendChild(scrollDiv)
	//resize and positioning in RE_redraw
}

function RE_clear_C_line_values(){
	var C_line=document.querySelector('.App_RE_C')
	C_line.innerHTML=''
}

//SCALE LINE

function RE_write_S_line_values() {
	//reading data in scale_sequence and add/delete buttons
	//console.log(global_time_all_changes)
	//console.log(global_time_segment_change)
	var S_line = document.querySelector(".App_RE_S")
	var data = DATA_get_current_state_point(false)

	var scale_sequence = data.scale.scale_sequence

	var counter = 0

	var wantedPositions = []
	//add final time

	var time = 0
	var scale_pulse = 0
	var current_time = 0

	scale_sequence.forEach((item) => {
		//var duration = parseInt(item.duration);
		var duration = item.duration
		//var time_scale_end = time + duration
		var time_scale_end = DATA_calculate_exact_time(scale_pulse,duration,0,1,1,1,1)

		var index_scale_global_time = RE_global_time_all_changes.indexOf(time)
		var copyRE_global_time_segment_change =(JSON.parse(JSON.stringify(RE_global_time_segment_change)))
		copyRE_global_time_segment_change.pop()
		var times_scale_segment_change = copyRE_global_time_segment_change.filter((stime) => {
			return stime <= time
		})

		var wanted_position = index_scale_global_time * 2 + (times_scale_segment_change.length - 1) * 2
		wantedPositions.push(wanted_position)
		current_time = time
		time = time_scale_end
		scale_pulse += duration
	})

	S_line.innerHTML = ""

	scale_sequence.forEach((element, index) => {
		var S_button = document.createElement("button")
		S_button.classList.add("App_RE_S_button")

		var S_button_left = nuzic_block * (wantedPositions[index] + 1)
		S_button.style.left = `${S_button_left}px`

		S_button.value = JSON.stringify(element)
		S_button.onclick = function () {
			RE_show_scale_input(S_button)
		}
		S_button.innerHTML = `

						<div class="App_RE_S_inp_box" style="display: inline;">
							<div class="App_RE_S_value">
								<p class="App_RE_sub_S_value">${element.acronym}${element.starting_note}.${element.rotation}</p>
							</div>
							<div class="App_RE_S_duration">
								<p class="App_RE_sub_S_duration">${element.duration}</p>
							</div>
						</div>
				`
		S_line.appendChild(S_button)
		counter += parseInt(element.duration)
	})

	var scrollDiv = document.createElement('div')
	scrollDiv.classList.add('App_RE_SC_scrollDiv')
	S_line.appendChild(scrollDiv)
	//resize in RE_redraw
}

function RE_clear_S_line_values(){
	var S_line = document.querySelector(".App_RE_S");
	S_line.innerHTML = "";
}

function RE_show_scale_input(S_button=null){
	//use scale != null == button pressed to focus on correct scale
	//APP_stop()
	var selected = -1
	if(S_button==null){
		//select first scale
		selected=0
	}else{
		var [elementIndex,list]=RE_element_index(S_button,".App_RE_S_button")
		selected=elementIndex
	}
	//verify if found something
	var scale_sequence= DATA_get_scale_sequence()
	if(selected<0 && selected>=scale_sequence.length)selected=0

	var ES_button = document.getElementById("App_BNav_tab_ES")
	ES_button.click()
	BNAV_select_scale_number_tab_list(selected)
}

function RE_show_scale_number_S_line(index){
	var scale_line = document.querySelector('.App_RE_S')
	var scale_button_list = [...scale_line.querySelectorAll(".App_RE_S_button")]

	//find correct div and move horizontal bar there!
	var scale_button_target = scale_button_list[index]
	var str = scale_button_target.style.left
	var n_pixels_left=parseInt(str.substring(0, str.length - 2))
	//var tot_buttons = scale_button_list.length
	var value = n_pixels_left-nuzic_block
	RE_checkScroll_H(value)
}

