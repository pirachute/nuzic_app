//general functions for RE
function RE_redraw(){
	var data = DATA_get_current_state_point(true)
	current_position_PMCRE.REscrollbar_redraw=false
	//console.error("REDRAW RE")
	//VOICES DATA
	var voice_data = data.voice_data

	var voices_container = document.querySelector(".App_RE_voice_matrix")
	// empty voice container
	voices_container.innerHTML=""

	var indexV=0

	//create voices
	//create void voices at the end
	for (var i = 0 ; i<voice_data.length; i++){
		var new_voice_obj = document.createElement('div')
		RE_draw_new_empty_voice_object(new_voice_obj)
		var new_voice_between_obj = document.createElement('div')
		RE_draw_new_empty_voice_between_object(new_voice_between_obj)

		voices_container.appendChild(new_voice_between_obj)
		voices_container.appendChild(new_voice_obj)
	}

	//regenerate voice_obj_list
	var voice_obj_list = [...voices_container.querySelectorAll(".App_RE_voice")]
	RE_calculate_alignment_global_parameters()

	//Modify voices
	voice_obj_list.forEach((voice_obj,index)=>{
		//console.log("writing voice number "+index)

		//change name
		RE_write_voice_name_ID(voice_obj,voice_data[index].name,voice_data[index].voice_index)

		//voice is blocked
		//being a bit more rude here  XXX set_voice_block?????
		var block_button = voice_obj.querySelector(".App_RE_voice_block")
		var class_string= block_button.className
		var RE_voice_block_icons = [...block_button.querySelectorAll(".App_RE_icon")]

		if(voice_data[index].blocked){
			//is blocked
			if (!class_string.includes("pressed")){
				block_button.classList.add("pressed")
			}
			voice_obj.classList.add("blocked")
			RE_voice_block_icons[0].style["display"] = 'flex'
			RE_voice_block_icons[1].style["display"] = 'none'
		}else{
			//unblocked
			if (class_string.includes("pressed")){
				block_button.classList.remove("pressed")
			}
			RE_voice_block_icons[0].style["display"] = 'none'
			RE_voice_block_icons[1].style["display"] = 'flex'
		}
		//volume
		RE_set_voice_volume(voice_obj, voice_data[index].volume)

		//solo
		RE_set_solo_voice(voice_obj,voice_data[index].solo)
		//instrument
		RE_set_voice_instrument(voice_obj,voice_data[index].instrument)
		//metronome
		RE_set_metro_sound(voice_obj,voice_data[index].metro)

		//Polipulse TO BE IMPLEMENTED index 7 .polipulse

		//neopulse
		var D_NP = voice_data[index].neopulse.D
		var N_NP = voice_data[index].neopulse.N
		var np_string = N_NP+"/"+D_NP
		voice_obj.querySelector(".App_RE_neopulse_inp_box").value=np_string

		//Voices values

		var segment_list = voice_data[index].data.segment_data

		//objects
		var voice_data_obj = voice_obj.querySelector(".App_RE_voice_data")

		var segment_spacer_end_obj = voice_data_obj.querySelector(".App_RE_segment_spacer_end")


		//add the first spacer
		var new_segment_spacer_obj = document.createElement('div')
		RE_draw_new_segment_spacer(new_segment_spacer_obj)
		segment_spacer_end_obj.insertAdjacentHTML( 'beforebegin', new_segment_spacer_obj.outerHTML)

		for (var i = 0 ; i<segment_list.length; i++){
			//create at the end!!!!
			var new_segment_obj = document.createElement('div')
			var new_segment_spacer_obj = document.createElement('div')
			RE_draw_new_empty_segment_object(new_segment_obj)
			RE_draw_new_segment_spacer(new_segment_spacer_obj)
			segment_spacer_end_obj.insertAdjacentHTML( 'beforebegin', new_segment_obj.outerHTML)
			segment_spacer_end_obj.insertAdjacentHTML( 'beforebegin', new_segment_spacer_obj.outerHTML)
		}

		//generate segment_list
		var segment_obj_list = [...voice_data_obj.querySelectorAll(".App_RE_segment")]
		var segment_spacer_obj_list = [...voice_data_obj.querySelectorAll(".App_RE_segment_spacer")]
		var Psg_=0
		//Modify segments content
		segment_obj_list.forEach((segment,indexS)=>{
			segment.querySelector(".App_RE_segment_loader").classList.add("loading")

			//console.log(segment_list[indexS])
			var Psg_segment_start = Psg_
			var Psg_segment_end = Psg_segment_start + segment_list[indexS].time.slice(-1)[0].P
			segment.setAttribute('starting',Psg_segment_start)
			Psg_=Psg_segment_end

			//calculate times
			var time_segment_start= DATA_calculate_exact_time(Psg_segment_start,0,0,1,1,N_NP,D_NP)
			var time_segment_end= DATA_calculate_exact_time(Psg_segment_end,0,0,1,1,N_NP,D_NP)


			var index_col_start = RE_find_time_column_index(time_segment_start)
			var index_col_end = RE_find_time_column_index(time_segment_end)
			var n_column = (index_col_end-index_col_start)-1

			//set width segment
			segment.style.width="calc(var(--RE_block)* "+n_column+")"
			segment.style.minWidth="calc(var(--RE_block)* "+n_column+")"
			//activate loades segment XXX

			//segment name
			var display_name = (n_column>5)? true:false

			RE_write_segment_name(segment,segment_list[indexS].segment_name,display_name)
			segment.addEventListener("mouseenter", function(){SNAV_enter_on_segment_parameters(this)})
		})
		//refresh segment-value
		RE_refresh_segments_number_values(voice_obj)
	})

	RE_redraw_column_selection(data)
	RE_refresh_voices_labels(data)
	RE_resize_voice_headers()

	//actualize voice visibility RE
	//better doing this when voices are actually written (if not we can generate an error on iA writing)
	voice_obj_list.forEach((voice_obj,index)=>{
		RE_show_hide_voice(voice_obj, voice_data[index].RE_visible)
	})

	//actualize ia , spaces and visibility no need, it is done by show hide voices
	RE_verify_segments_button_visibility()
	//make shure all new element created are draggable
	RE_refresh_draggable_segments()
	RE_refresh_draggable_voices()

	//scrollbar
	var final_seg_time = DATA_calculate_exact_time(Li,0,0,1,1,1,1)
	var final_time = RE_global_time_all_changes[RE_global_time_all_changes.length-1]
	var extra = 0
	if(final_seg_time!=final_time)extra=1 //give 1 more bc S and T line end with a div that cover the boxes

	var n_boxes = 1+(RE_global_time_all_changes.length-1) * 2 + (RE_global_time_segment_change.length-1)*2 +extra
	//current_position_PMCRE.REscrollbar_redraw=false

	RE_set_H_scrollbar_step_N(n_boxes)
	// RE_checkScroll_H(null,true)
	// current_position_PMCRE.REscrollbar_redraw=true

	var n = (RE_global_time_all_changes.length - 1 -RE_global_time_all_changes.indexOf(RE_global_time_segment_change.slice(-1)[0]))*2
	if(n<0) n=0
	//give 1 more bc S and T line end with a div that cover the boxes
	n = extra==1? n+1:n

	//RE_verify_segment_visibility()
	RE_write_S_line_values()
	RE_write_C_line_values()
	RE_check_iA_line_visibility()
	RE_refresh_iA_voices_labels(data.global_variables.RE)
	var iA_n_boxes=n_boxes-n-2 //-2 are the first and last vertical bar
	RE_write_all_iA_lines(data,iA_n_boxes)

	var segment_spacer_end_list= [...voices_container.querySelectorAll(".App_RE_segment_spacer_end")]
	//change spacers width at the end of voices if needed
	segment_spacer_end_list.forEach(spacer=>{
		spacer.style.minWidth="calc(var(--RE_block) * "+n+")"
	})

	//scroll controller of compas and scale
	var step_compas_scale = [...document.querySelectorAll(".App_RE_SC_scrollDiv")]
	step_compas_scale.forEach(item=>{
		item.style.width ="calc(var(--RE_block) * "+n_boxes+")"
		item.style.minWidth ="calc(var(--RE_block) * "+n_boxes+")"
	})
	RE_checkScroll_H(null,true)
	current_position_PMCRE.REscrollbar_redraw=true
	RE_verify_segment_visibility()
}

function RE_verify_segment_visibility(){
	//trigger when change in scrollbar, windows resizing, voice visibility (loading data???) PMCtoRE XXX
	// var  t1, t2,
	// startTimer = function() {
	// 	t1 = new Date().getTime();
	// 	return t1;
	// },
	// stopTimer = function() {
	// 	t2 = new Date().getTime();
	// 	return t2 - t1;
	// }

	// startTimer()
	var data = DATA_get_current_state_point(true)
	var line_list_states = data.global_variables.RE
	//list voices
	var voices_container = document.querySelector(".App_RE_voice_matrix")
	var voice_obj_list = [...voices_container.querySelectorAll(".App_RE_voice")]
	var voice_data_obj_list = [...voices_container.querySelectorAll(".App_RE_voice_data")]
	//list segments

	voice_obj_list.forEach((voice_data_obj,index)=>{
		var voice_index = RE_read_voice_id(voice_data_obj)[1] //function find voice index(voice_obj_list[index])
		var voice_data = data.voice_data.find(item=>{return item.voice_index==voice_index}) //function data [voice_index]
		var segment_obj_list = [...voice_data_obj.querySelectorAll(".App_RE_segment")]
		segment_obj_list.forEach((segment_obj,segment_index)=>{
			if(RE_segment_is_visible(segment_obj)){
				//console.log("voice "+voice_index+" segment "+segment_index+" is visible")
				var segment_data = voice_data.data.segment_data[segment_index]
				RE_redraw_segment_content(segment_obj,segment_data,voice_data.neopulse.N,voice_data.neopulse.D,line_list_states)
			}
		})
	})
	//console.error('RE write segments in ' + stopTimer() + 'ms')
}

function RE_segment_is_visible(segment) {
	const rect = segment.getBoundingClientRect()
	return (
		rect.width> 0 &&
		((rect.left >= 0 && rect.left<=(window.innerWidth || document.documentElement.clientWidth)) ||
		(rect.right >= 0 && rect.right <= (window.innerWidth || document.documentElement.clientWidth)) ||
		(rect.left <= 0 && rect.right >= (window.innerWidth || document.documentElement.clientWidth))//left left and right to the right of the border of the screen
		)
	)

	/*/ENTIRELY INSIDE VIEWPORT
	return (
		rect.top >= 0 &&
		rect.left >= 0 &&
		rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
		rect.right <= (window.innerWidth || document.documentElement.clientWidth)
	)
	*/
}

function RE_calculate_alignment_global_parameters(){
	var time_event_list = []
	var note_event_list = []
	// object of any event in notation (useful in order to get the string)
	var object_event_list = []
	// frequency of said event (or duration)
	var frequency_event_list = []
	//metronome
	var time_fraction_list = []
	var fraction_list = []

	//change of an segment
	var time_segment_change = []

	var iA_n_boxes = 0

	//a copy of database
	var current_data = DATA_get_current_state_point(false)

	current_data.voice_data.forEach((voice)=>{
		//find voice id
		if(voice.RE_visible){
			//add ending time
			voice.data.midi_data.time.push(DATA_calculate_exact_time(Li,0,0,1,1,1,1))
			time_event_list.push({"voice_index":voice.voice_index,"time":voice.data.midi_data.time})
			time_segment_change.push(DATA_list_voice_data_segment_change(voice))

			var note = voice.data.segment_data.map(note_seg=>{return note_seg.note})
			var note_list = []
			note.forEach((item)=> {
				note_list = note_list.concat(item)
			})
			note_event_list.push({"voice_index":voice.voice_index,"note":note_list})
		}
	})

	RE_global_note_voices_changes=note_event_list

	//calculate how many Global time intervals exist (how many iA are needed)
	var provv = []
	time_event_list.forEach((item)=> {
		provv = provv.concat(item.time)
	})
	RE_global_time_voices_changes=time_event_list
	provv.sort(function(a, b){return a-b});
	var global_time_event_list = [...new Set(provv)]

	//calculate globally when a new segment start (change of segments)
	provv = []
	time_segment_change.forEach((item)=> {
		provv = provv.concat(item)
	})
	provv.sort(function(a, b){return a-b});
	var global_time_segment_change = [...new Set(provv)]
	// XXX  why this global variable??? to draw compasses??????
	//console.log(global_time_segment_change)
	global_time_segment_change.push(DATA_calculate_exact_time(Li,0,0,1,1,1,1))
	RE_global_time_segment_change = global_time_segment_change

	//compas line
	//if compas button pressed ... ???????
	// IN A DIFFERENT POSITION    XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

	var compas_change = []

	current_data.compas.forEach(compas=>{
		for (let i = 0; i < compas.compas_values[2]; i++) {
			compas_change.push(compas.compas_values[1]*i+compas.compas_values[0])
		}
	})

	//var compas_change = BNAV_read_time_all_compas()

	var global_time_compas_change = compas_change.map(item=>{
		return DATA_calculate_exact_time(item,0,0,1,1,1,1)
	})

	//scale_line
	var scale_pulse = 0
	var global_time_scale_change = current_data.scale.scale_sequence.map(item=>{
		// var value = scale_time
		///avoid sum of truncated values
		var value = DATA_calculate_exact_time(scale_pulse,0,0,1,1,1,1)
		scale_pulse += item.duration
		return value
	})

	//add global_time_segment_change && global_time_scale_change + global_time_compas_change for voice alignment
	provv = []
	provv=  global_time_compas_change.concat(global_time_segment_change)
	provv.sort(function(a, b){return a-b});
	var global_time_compas_segment_changes = [...new Set(provv)]

	provv = []
	provv=  global_time_compas_segment_changes.concat(global_time_scale_change)
	provv.sort(function(a, b){return a-b});
	var global_time_compas_scale_segment_changes = [...new Set(provv)]

	//add all the time changing change
	provv = []
	provv=  global_time_compas_scale_segment_changes.concat(global_time_event_list)
	provv.sort(function(a, b){return a-b});
	var global_time_all_changes = [...new Set(provv)]
	// XXX  why this global variable??? to draw compasses??????
	RE_global_time_all_changes = global_time_all_changes

	return
}

function RE_draw_new_empty_voice_object(voice_obj){
	//take an segment and empty it
	voice_obj.setAttribute("value", "-1")//provv value
	voice_obj.classList.add("App_RE_voice")
	//<div class="App_RE_voice" value="0">
	voice_obj.innerHTML=`
						<div class="App_RE_voice_header">
							<div class="App_RE_voice_header_L">
								<input class="App_RE_voice_name" placeholder="Voice" maxlength="8" onchange="RE_change_voice_name(this)"></input>
								<button class="App_RE_voice_instrument" onclick="RE_change_voice_instrument(this)"
									value="0" onmouseenter="Hover_area_enter(this)"
									onmouseleave="Hover_area_exit(this)" data-value="instrumentoHover">
									<img class="App_RE_icon" src="./Icons/keyboard.svg" />
								</button>
								<div class="App_RE_voice_header_extra"></div>
								<div class="App_RE_voice_header_L_B">
									<button class="App_RE_voice_block pressed" onClick="RE_block_unblock_voice(this)">
										<img class="App_RE_icon" onmouseenter="Hover_area_enter(this)"
											onmouseleave="Hover_area_exit(this)" data-value="SyncVoiceHover"
											src="./Icons/channel-secure-symbolic.svg" />
										<img class="App_RE_icon" onmouseenter="Hover_area_enter(this)"
											onmouseleave="Hover_area_exit(this)" data-value="AsyncVoiceHover"
											src="./Icons/changes-allow-symbolic.svg" style="display: none;" />
									</button>
									<button class="App_RE_metro_sound App_RE_text_strikethrough"
										onclick="RE_change_metro_sound(this)" value="0"
										onmouseenter="Hover_area_enter(this)" onmouseleave="Hover_area_exit(this)"
										data-value="metronomoVozHover">
										M
									</button>
								</div>
							</div>
							<div class="App_RE_voice_header_C">
								<button class="App_RE_voice_volume" onClick="RE_change_voice_volume(this)"
									value="3" onmouseenter="Hover_area_enter(this)" onmouseleave="Hover_area_exit(this)"
									data-value="volumenVozHover">
									<img class="App_RE_icon" src="./Icons/audio-volume-high-symbolic.svg" />
								</button>
								<button class="App_RE_voice_mute" onClick="RE_mute_unmute_voice(this)"
									value="3" onmouseenter="Hover_area_enter(this)" onmouseleave="Hover_area_exit(this)"
									data-value="muteHover">
									M
								</button>
								<button class="App_RE_voice_solo" onClick="RE_change_solo_voice(this)"
									onmouseenter="Hover_area_enter(this)" onmouseleave="Hover_area_exit(this)"
									data-value="soloHover">S
								</button>
								<div class="App_RE_voice_header_extra"></div>
								<input class="App_RE_neopulse_inp_box"
									onkeypress="APP_onlyIntegers_enter_NP(event,this)"
									onfocusin="APP_set_previous_value(this)" onfocusout="RE_enter_new_NP(this)"
									maxlength="3" value="1/1" onmouseenter="Hover_area_enter(this)"
									onmouseleave="Hover_area_exit(this)" data-value="FrGeneralHover"></input>
							</div>
							<div class="App_RE_voice_header_R" onclick="RE_toggle_selection_voice(this)">
								<button class="App_RE_voice_menu_button show" >
									<img class="App_RE_voice_menu_button_img" src="./Icons/menu.svg">
								</button>
								<div class="App_RE_voice_dropdown_menu">
									<a onmouseenter="Hover_area_enter(this)" onmouseleave="Hover_area_exit(this)" onclick="RE_add_voice_button(this)"><img class="App_RE_icon" src="./Icons/op_menu_add.svg"></a>
									<!--a onmouseenter="Hover_area_enter(this)" onmouseleave="Hover_area_exit(this)" onclick="RE_clone_voice_button(this)">Clone</a-->
									<a onmouseenter="Hover_area_enter(this)" onmouseleave="Hover_area_exit(this)" onclick="RE_clear_voice_button(this)"><img class="App_RE_icon" src="./Icons/op_menu_clear.svg"></a>
									<a class="App_RE_delete_voice" onmouseenter="Hover_area_enter(this)" onmouseleave="Hover_area_exit(this)" onclick="RE_delete_voice_button(this)"><img class="App_RE_icon" src="./Icons/op_menu_delete.svg"></a>
									<a onmouseenter="Hover_area_enter(this)" onmouseleave="Hover_area_exit(this)" onclick="RE_repeat_voice_button(this,event)"><img class="App_RE_icon" src="./Icons/op_menu_repeat_up.svg"></a>
									<a onmouseenter="Hover_area_enter(this)" onmouseleave="Hover_area_exit(this)" onclick="RE_operations_voice_button(this,event)"><img class="App_RE_icon" src="./Icons/op_menu_operations.svg"></a>
									<a ></a>
								</div>
								<button class="App_RE_voice_visible show" onclick="RE_show_hide_voice_button(this)" value="0">◂</button>
								<button class="App_RE_voice_header_R_minimized_button" onclick="RE_minimized_voice_header_button(this)" style="display: none;" value="1">
									<p></p>
								</button>
							</div>
						</div>
						<div class="App_RE_voice_selector_bg"></div>
						<div class="App_RE_voice_draggable" draggable="true" onclick="RE_toggle_selection_voice(this)">
							<p>=</p>
						</div>
						<div class="App_RE_voice_labels">
							<label class="App_RE_segment_label">sg</label>
							<label onmouseenter="Hover_area_enter(this)" onmouseleave="Hover_area_exit(this)"
								data-value="NaHover" class="hoverLabel boxHover App_RE_label App_RE_Na_l">Na</label>
							<label onmouseenter="Hover_area_enter(this)" onmouseleave="Hover_area_exit(this)"
								data-value="NHover" class="hoverLabel boxHover App_RE_label App_RE_Nm_l">Nm</label>
							<label onmouseenter="Hover_area_enter(this)" onmouseleave="Hover_area_exit(this)"
								data-value="NºHover" class="hoverLabel App_RE_label App_RE_Ngm_l">N°</label>
							<label onmouseenter="Hover_area_enter(this)" onmouseleave="Hover_area_exit(this)"
								data-value="iSaHover" class="hoverLabel boxHover App_RE_label App_RE_iNa_l">iSa</label>
							<label onmouseenter="Hover_area_enter(this)" onmouseleave="Hover_area_exit(this)"
								data-value="iSHover" class="hoverLabel boxHover App_RE_label App_RE_iNm_l">iSm</label>
							<label onmouseenter="Hover_area_enter(this)" onmouseleave="Hover_area_exit(this)"
								data-value="iSºHover" class="hoverLabel App_RE_label App_RE_iNgm_l">iS°</label>
							<label onmouseenter="Hover_area_enter(this)" onmouseleave="Hover_area_exit(this)"
								data-value="NabcHover" class="hoverLabel App_RE_label App_RE_Nabc_l">#/♭</label>
							<label onmouseenter="Hover_area_enter(this)" onmouseleave="Hover_area_exit(this)"
								data-value="PaHover" class="hoverLabel boxHover App_RE_label App_RE_Ta_l">Pa</label>
							<label onmouseenter="Hover_area_enter(this)" onmouseleave="Hover_area_exit(this)"
								data-value="PHover" class="hoverLabel boxHover App_RE_label App_RE_Tm_l">Pm</label>
							<label onmouseenter="Hover_area_enter(this)" onmouseleave="Hover_area_exit(this)"
								data-value="PsHover" class="hoverLabel boxHover App_RE_label App_RE_Ts_l">Psg</label>
							<label onmouseenter="Hover_area_enter(this)" onmouseleave="Hover_area_exit(this)"
								data-value="iTHover" class="hoverLabel boxHover App_RE_label App_RE_iT_l">iTa</label>
							<label onmouseenter="Hover_area_enter(this)" onmouseleave="Hover_area_exit(this)"
								data-value="frTHover" class="hoverLabel boxHover App_RE_label App_RE_Tf_l">Fr</label>
						</div>
						<div class="App_RE_voice_data" onscroll="RE_voice_scrolling(this,event)">
							<div class="App_RE_segment_spacer_end">
							</div>
						</div>
						<div class="App_RE_white_space_voice_end hidden"></div>
				`
}

function RE_draw_new_empty_voice_between_object(voice_between_obj){
	voice_between_obj.classList.add("App_RE_voices_between")
	voice_between_obj.innerHTML=`
						<label class="App_RE_label hidden" onmouseenter="Hover_area_enter(this)" onmouseleave="Hover_area_exit(this)" data-value="iAaHover">iA</label>
						<div class="App_RE_iA hidden" onscroll="RE_voice_scrolling(this,event)">
						</div>
					`
}

function RE_draw_new_empty_segment_object(segment_obj){
	//take an segment and empty it
	//segment_obj.addEventListener("mouseenter", function(){SNAV_enter_on_segment_parameters(this)})
	segment_obj.classList.add("App_RE_segment")
	segment_obj.innerHTML=	`
							<div class="App_RE_segment_loader">Loading
								</div>
								<div class="App_RE_segment_draggable" draggable="true" onclick="RE_toggle_selection_segment(this)">
									<div class="App_RE_dropdown_segment">
										<button class="App_RE_dropdown_segment_button">
											<img class="App_RE_icon" src="./Icons/menu.svg" />
										</button>
										<div class="App_RE_dropdown_segment_content">
											<a onmouseenter="Hover_area_enter(this)" onmouseleave="Hover_area_exit(this)"
												data-value="NameSegHover" onclick="RE_change_segment_name_button(this)"
												href="#"><img class="App_RE_icon" src="./Icons/op_menu_rename.svg"></a>
											<a onmouseenter="Hover_area_enter(this)" onmouseleave="Hover_area_exit(this)"
												data-value="ClearSegHover" onclick="RE_clear_segment_button(this)"
												href="#"><img class="App_RE_icon" src="./Icons/op_menu_clear.svg"></a>
											<a onmouseenter="Hover_area_enter(this)" onmouseleave="Hover_area_exit(this)"
												data-value="PasteSegHover" onclick="RE_repeat_segment_button(this,event)"
												href="#"><img class="App_RE_icon" src="./Icons/op_menu_repeat_right.svg"></a>
											<a onmouseenter="Hover_area_enter(this)" onmouseleave="Hover_area_exit(this)"
												data-value="CopySegHover" onclick="RE_copy_segment_button(this)"
												href="#"><img class="App_RE_icon" src="./Icons/op_menu_copy.svg"></a>
											<a onmouseenter="Hover_area_enter(this)" onmouseleave="Hover_area_exit(this)"
												data-value="PasteSegHover" onclick="RE_paste_segment_button(this)"
												href="#"><img class="App_RE_icon" src="./Icons/op_menu_paste.svg"></a>
											<a onmouseenter="Hover_area_enter(this)" onmouseleave="Hover_area_exit(this)"
												data-value="PasteSegHover" onclick="RE_operations_segment_button(this,event)"
												href="#"><img class="App_RE_icon" src="./Icons/op_menu_operations.svg"></a>
										</div>
									</div>
									<div class="App_RE_segment_number">
										0
									</div>
									<input class="App_RE_segment_name" onchange="RE_change_segment_name(this)" onfocusout="RE_hide_segment_name(this)" value="" maxlength="8" placeholder="">
									</input>
									<button class="App_RE_delete_segment" onmouseenter="Hover_area_enter(this)"
										onmouseleave="Hover_area_exit(this)" data-value="DeleteSegHover"
										onclick="RE_delete_segment_button(this)">
										<img class="App_RE_icon" src="./Icons/delete.svg" />
									</button>
								</div>
								<div class="App_RE_segment_draggable App_RE_segment_draggable_bottom" onmouseleave="RE_exit_break_segment(this)" onmousemove="RE_break_segment_calculate_position(this,event)" draggable="true">
									<button class="App_RE_break_segment" onclick="RE_break_segment_button(this)" value="0">
										<img src="./Icons/bolt_blue.svg" />
									</button>
								</div>
						`
}

function RE_draw_new_segment_spacer(segment_spacer_obj){
	segment_spacer_obj.classList.add("App_RE_segment_spacer")
	//<div class="App_RE_segment_spacer">
	segment_spacer_obj.innerHTML = `
									<svg data-name="Trazado 10" viewBox="0 0 28 28" preserveAspectRatio="none">
										<line x1="14" y1="0" x2="14" y2="28" style="stroke-width:2"></line>
									</svg>
									<div class="App_RE_segment_spacer_buttons">
										<button class="App_RE_add_segment" onmouseenter="Hover_area_enter(this)"
											onmouseleave="Hover_area_exit(this)" data-value="AddSegHover"
											onclick="RE_add_segment_button(this)">
											<img src="./Icons/new-segment.svg" />
										</button>
										<button class="App_RE_merge_segment" onmouseenter="Hover_area_enter(this)"
											onmouseleave="Hover_area_exit(this)" data-value="MergeHover"
											onclick="RE_merge_segment_button(this)">
											<img src="./Icons/merge-segment.svg" />
										</button>
									</div>
									`
}

function RE_refresh_voices_labels(data){
	var line_list_states = data.global_variables.RE
	//all voices
	var voice_matrix = document.querySelector('.App_RE_voice_matrix')
	var voice_list = [...voice_matrix.querySelectorAll('.App_RE_voice')]
	voice_list.forEach(voice=>{
		var line_label_list = _list_all_voice_lines_labels(voice)
		Object.keys(line_label_list).forEach(id=>{
			//show - hide
			var label = line_label_list[id]
			if(!line_list_states[id]){
				//remove
				label.style="display: none;"
			}else{
				//add
				label.style=""
			}
		})
	})

	function _list_all_voice_lines_labels(voice){
		//list all lines in voice head
		var voice_labels = voice.querySelector(".App_RE_voice_labels")
		//if function for listing line BUTTONS in App_SNav_line_selector
		if (voice_labels == null) voice_labels=voice

		//note absolute
		var Na_line_l = voice_labels.querySelector(".App_RE_Na_l")
		//note_modular
		var Nm_line_l = voice_labels.querySelector(".App_RE_Nm_l")
		//grade note
		var Ngm_line_l = voice_labels.querySelector(".App_RE_Ngm_l")
		//alpha note
		var Nabc_line_l = voice_labels.querySelector(".App_RE_Nabc_l")
		//note intervals
		var iNa_line_l = voice_labels.querySelector(".App_RE_iNa_l")
		var iNm_line_l = voice_labels.querySelector(".App_RE_iNm_l")
		var iNgm_line_l = voice_labels.querySelector(".App_RE_iNgm_l")
		//pulses
		var Ta_line_l = voice_labels.querySelector(".App_RE_Ta_l")
		var Tm_line_l = voice_labels.querySelector(".App_RE_Tm_l")
		var Ts_line_l = voice_labels.querySelector(".App_RE_Ts_l")
		//time intervals
		var iT_line_l = voice_labels.querySelector(".App_RE_iT_l")
		//time fraction
		var Tf_line_l = voice_labels.querySelector(".App_RE_Tf_l")

		//better
		return {
				Na:Na_line_l,
				Nm:Nm_line_l,
				Ngm:Ngm_line_l,
				Nabc:Nabc_line_l,
				iNa:iNa_line_l,
				iNm:iNm_line_l,
				iNgm:iNgm_line_l,
				Ta:Ta_line_l,
				Tm:Tm_line_l,
				Ts:Ts_line_l,
				iT:iT_line_l,
				Tf:Tf_line_l}
	}
}

function RE_redraw_segment_content(segment_obj,segment_data,N_NP,D_NP,line_list_states){
	var seg_loader = segment_obj.querySelector(".App_RE_segment_loader")
	if(seg_loader.classList.contains("loading")){
		seg_loader.classList.remove("loading")
	}else{
		//console.log("already loaded")
		return
	}

	//calculate partial global time array
	var Psg_segment_start = Number(segment_obj.getAttribute('starting'))
	var Psg_segment_end = segment_data.time.slice(-1)[0].P+Psg_segment_start
	var time_segment_start= DATA_calculate_exact_time(Psg_segment_start,0,0,1,1,N_NP,D_NP)
	var time_segment_end= DATA_calculate_exact_time(Psg_segment_end,0,0,1,1,N_NP,D_NP)

	var index_start = RE_global_time_all_changes.indexOf(time_segment_start)
	var index_end= RE_global_time_all_changes.indexOf(time_segment_end)

	//find alternative array
	var partial_RE_global_time_all_changes = RE_global_time_all_changes.slice(index_start,index_end+1)

	var index_start_segment_changes = RE_global_time_segment_change.indexOf(time_segment_start)
	var index_end_segment_changes= RE_global_time_segment_change.indexOf(time_segment_end)
	var partial_RE_global_time_segment_change = RE_global_time_segment_change.slice(index_start_segment_changes,index_end_segment_changes+1)
	//generate index array columns

	var max_fraction_index = segment_data.fraction.length-1
	var index_columns = segment_data.time.map((time,time_index)=>{
		//change length  arrow
		//calculate times
		var time_sec
		if(time.F!=0){
			var fraction_data = segment_data.fraction.find((fraction)=>{
				return fraction.stop>time.P
			})
			time_sec=DATA_calculate_exact_time(Psg_segment_start,time.P,time.F,fraction_data.N,fraction_data.D,N_NP,D_NP)
		}else{
			time_sec=DATA_calculate_exact_time(Psg_segment_start,time.P,time.F,1,1,N_NP,D_NP)
		}
		//look how much elements there are in global time
		return RE_find_time_column_index(time_sec,partial_RE_global_time_all_changes,partial_RE_global_time_segment_change)
	})
	var last_value = index_columns.pop()-2//correct last value
	index_columns.push(last_value)


	//write only 3 basic lines and the ones that are active
	//draw empty segment lines
	var cell_number = Number(segment_obj.style.width.split("*")[1].split(")")[0])
	//var cell_number = partial_RE_global_time_all_changes

	//Na BASIC LINE
	if(line_list_states.Na){
		var new_Na_line = document.createElement('div')
		new_Na_line.classList.add("App_RE_segment_line")
		new_Na_line.classList.add("App_RE_Na")
		var inner_string = ""

		for (let i = 0; i < cell_number-1; i++) {
			inner_string+= `
										<input class="App_RE_inp_box App_RE_note_cell" ondragstart="RE_disableEvent(event)"
											onkeypress="APP_onlyIntegers_Na_line(event,this)" onkeydown="RE_move_focus(event,this)"
											onfocusin="APP_set_previous_value(this);RE_parent_no_draggable(this);APP_play_input_on_click(this,'N');RE_set_current_position(this)"
											onfocusout="RE_parent_draggable(this);RE_enter_new_value_S(this,'Na')"
											ondblclick="RE_dbclick_inp_box(this)" onmouseenter="Hover_area_enter(this)"
											onmouseleave="Hover_area_exit(this)" maxlength="3"
											placeholder=" "></input>
										`
		}
		inner_string+= `
									<input class="App_RE_inp_box App_RE_note_cell App_RE_segment_last_cell" ondragstart="RE_disableEvent(event)"
										onkeydown="RE_move_focus(event,this);APP_onlyIntegers_denied(event,this)"
										onfocusin="APP_set_previous_value(this);RE_parent_no_draggable(this);APP_play_input_on_click(this,'N');RE_set_current_position(this)"
										onfocusout="RE_parent_draggable(this);RE_enter_new_value_S(this,'Na')"
										ondblclick="RE_dbclick_inp_box(this)" onmouseenter="Hover_area_enter(this)"
										onmouseleave="Hover_area_exit(this)"
										value="${end_range}" data-value="dotHover"></input>
									`

		// if(!line_list_states.Na){
			//not visible
		// 	new_Na_line.classList.add("hidden")
		// }
		new_Na_line.innerHTML= inner_string
		// var last_cell= new_Na_line.lastElementChild
		// last_cell.classList.add("App_RE_segment_last_cell")
		// last_cell.disabled=true
		// last_cell.value=end_range
		segment_obj.appendChild(new_Na_line)
	}

	// Nm
	if(line_list_states.Nm){
		var new_Nm_line = document.createElement('div')
		new_Nm_line.classList.add("App_RE_segment_line")
		new_Nm_line.classList.add("App_RE_Nm")
		var inner_string = ""
		for (let i = 0; i < cell_number-1; i++) {
			//new_Nm_line.innerHTML+= `
			inner_string+= `
										<input class="App_RE_inp_box App_RE_note_cell" ondragstart="RE_disableEvent(event)"
										onkeypress="APP_onlyIntegers_Nm_line(event,this)" onkeydown="RE_move_focus(event,this)"
										onfocusin="APP_set_previous_value(this);RE_parent_no_draggable(this);APP_play_input_on_click(this,'N');RE_set_current_position(this)"
										onfocusout="RE_parent_draggable(this);RE_enter_new_value_S(this,'Nm')"
										ondblclick="RE_dbclick_inp_box(this)" onmouseenter="Hover_area_enter(this)"
										onmouseleave="Hover_area_exit(this)" maxlength="4" placeholder=" "></input>
										`
		}
		inner_string+= `
										<input class="App_RE_inp_box App_RE_note_cell App_RE_segment_last_cell" ondragstart="RE_disableEvent(event)"
										onkeypress="APP_onlyIntegers_Nm_line(event,this)" onkeydown="RE_move_focus(event,this)"
										onfocusin="APP_set_previous_value(this);RE_parent_no_draggable(this);APP_play_input_on_click(this,'N');RE_set_current_position(this)"
										onfocusout="RE_parent_draggable(this);RE_enter_new_value_S(this,'Nm')"
										ondblclick="RE_dbclick_inp_box(this)" onmouseenter="Hover_area_enter(this)"
										onmouseleave="Hover_area_exit(this)" value="${end_range}" data-value="dotHover"></input>
										`
		new_Nm_line.innerHTML=inner_string
		// var last_cell= new_Nm_line.lastElementChild
		// last_cell.classList.add("App_RE_segment_last_cell")
		// last_cell.value=end_range
		// last_cell.disabled=true
		// last_cell.setAttribute("data-value", "dotHover")
		segment_obj.appendChild(new_Nm_line)
	}

	// Ngm
	if(line_list_states.Ngm){
		var new_Ngm_line = document.createElement('div')
		new_Ngm_line.classList.add("App_RE_segment_line")
		new_Ngm_line.classList.add("App_RE_Ngm")
		var inner_string = ""

		for (let i = 0; i < cell_number-1; i++) {
			//new_Ngm_line.innerHTML+= `
			inner_string+= `
										<input class="App_RE_inp_box App_RE_note_cell" ondragstart="RE_disableEvent(event)"
										onkeypress="APP_onlyIntegers_Ngm_line(event,this)"
										onkeydown="RE_move_focus(event,this)"
										onfocusin="APP_set_previous_value(this);RE_parent_no_draggable(this);APP_play_input_on_click(this,'N');RE_set_current_position(this);RE_replace_inp_box_superscript(this)"
										onfocusout="RE_parent_draggable(this);RE_enter_new_value_S(this,'Ngm')"
										ondblclick="RE_dbclick_inp_box(this)" onmouseenter="Hover_area_enter(this)"
										onmouseleave="Hover_area_exit(this)" maxlength="7" placeholder=" "></input>
										`
		}
		inner_string+= `
										<input class="App_RE_inp_box App_RE_note_cell App_RE_segment_last_cell" ondragstart="RE_disableEvent(event)"
										onkeypress="APP_onlyIntegers_Ngm_line(event,this)"
										onkeydown="RE_move_focus(event,this);APP_onlyIntegers_denied(event,this)"
										onfocusin="APP_set_previous_value(this);RE_parent_no_draggable(this);APP_play_input_on_click(this,'N');RE_set_current_position(this);RE_replace_inp_box_superscript(this)"
										onfocusout="RE_parent_draggable(this);RE_enter_new_value_S(this,'Ngm')"
										ondblclick="RE_dbclick_inp_box(this)" onmouseenter="Hover_area_enter(this)"
										onmouseleave="Hover_area_exit(this)" value="${end_range}" data-value="dotHover"></input>
										`
		new_Ngm_line.innerHTML=inner_string
		// var last_cell= new_Ngm_line.lastElementChild
		// last_cell.classList.add("App_RE_segment_last_cell")
		// last_cell.value=end_range
		// last_cell.disabled=true
		// last_cell.setAttribute("data-value", "dotHover")
		segment_obj.appendChild(new_Ngm_line)
	}

	// iNa
	if(line_list_states.iNa){
		var new_iNa_line = document.createElement('div')
		new_iNa_line.classList.add("App_RE_segment_line")
		new_iNa_line.classList.add("App_RE_iNa")
		var inner_string = ""

		for (let i = 0; i < cell_number-1; i++) {
			//new_iNa_line.innerHTML+= `
			inner_string+= `
										<input class="App_RE_inp_box App_RE_note_cell" ondragstart="RE_disableEvent(event)"
										onkeypress="APP_onlyIntegers_iNa_line(event,this)"
										onkeydown="RE_move_focus(event,this)"
										onfocusin="APP_set_previous_value(this);RE_parent_no_draggable(this);APP_play_input_on_click(this,'N');RE_set_current_position(this)"
										onfocusout="RE_parent_draggable(this);RE_enter_new_value_dS(this,'iNa')"
										ondblclick="RE_dbclick_inp_box(this)" onmouseenter="Hover_area_enter(this)"
										onmouseleave="Hover_area_exit(this)" maxlength="4" placeholder=" "></input>
										`
		}
		inner_string+= `
										<input class="App_RE_inp_box App_RE_note_cell App_RE_segment_last_cell" ondragstart="RE_disableEvent(event)"
										onkeypress="APP_onlyIntegers_iNa_line(event,this)"
										onkeydown="RE_move_focus(event,this);APP_onlyIntegers_denied(event,this)"
										onfocusin="APP_set_previous_value(this);RE_parent_no_draggable(this);APP_play_input_on_click(this,'N');RE_set_current_position(this)"
										onfocusout="RE_parent_draggable(this);RE_enter_new_value_dS(this,'iNa')"
										ondblclick="RE_dbclick_inp_box(this)" onmouseenter="Hover_area_enter(this)"
										onmouseleave="Hover_area_exit(this)" value="${end_range}" data-value="dotHover"></input>
										`
		new_iNa_line.innerHTML=inner_string
		// var last_cell= new_iNa_line.lastElementChild
		// last_cell.classList.add("App_RE_segment_last_cell")
		// last_cell.value=end_range
		// last_cell.disabled=true
		// last_cell.setAttribute("data-value", "dotHover")
		segment_obj.appendChild(new_iNa_line)
	}

	//iNm
	if(line_list_states.iNm){
		var new_iNm_line = document.createElement('div')
		new_iNm_line.classList.add("App_RE_segment_line")
		new_iNm_line.classList.add("App_RE_iNm")
		var inner_string = ""

		for (let i = 0; i < cell_number-1; i++) {
			//new_iNm_line.innerHTML+= `
			inner_string+= `
										<input class="App_RE_inp_box App_RE_note_cell" ondragstart="RE_disableEvent(event)"
										onkeypress="APP_onlyIntegers_iNm_line(event,this)"
										onkeydown="RE_move_focus(event,this)"
										onfocusin="APP_set_previous_value(this);RE_parent_no_draggable(this);APP_play_input_on_click(this,'N');RE_set_current_position(this)"
										onfocusout="RE_parent_draggable(this);RE_enter_new_value_dS(this,'iNm')"
										ondblclick="RE_dbclick_inp_box(this)" onmouseenter="Hover_area_enter(this)"
										onmouseleave="Hover_area_exit(this)" maxlength="5" placeholder=" "></input>
										`
		}
		inner_string+= `
										<input class="App_RE_inp_box App_RE_note_cell App_RE_segment_last_cell" ondragstart="RE_disableEvent(event)"
										onkeypress="APP_onlyIntegers_iNm_line(event,this)"
										onkeydown="RE_move_focus(event,this);APP_onlyIntegers_denied(event,this)"
										onfocusin="APP_set_previous_value(this);RE_parent_no_draggable(this);APP_play_input_on_click(this,'N');RE_set_current_position(this)"
										onfocusout="RE_parent_draggable(this);RE_enter_new_value_dS(this,'iNm')"
										ondblclick="RE_dbclick_inp_box(this)" onmouseenter="Hover_area_enter(this)"
										onmouseleave="Hover_area_exit(this)" value="${end_range}" data-value="dotHover"></input>
										`
		new_iNm_line.innerHTML=inner_string
		var last_cell= new_iNm_line.lastElementChild
		// last_cell.classList.add("App_RE_segment_last_cell")
		// last_cell.value=end_range
		// last_cell.disabled=true
		// last_cell.setAttribute("data-value", "dotHover")
		segment_obj.appendChild(new_iNm_line)
	}

	// iNgm
	if(line_list_states.iNgm){
		var new_iNgm_line = document.createElement('div')
		new_iNgm_line.classList.add("App_RE_segment_line")
		new_iNgm_line.classList.add("App_RE_iNgm")
		var inner_string = ""

		for (let i = 0; i < cell_number-1; i++) {
			//new_iNgm_line.innerHTML+= `
			inner_string+= `
										<input class="App_RE_inp_box App_RE_note_cell" ondragstart="RE_disableEvent(event)"
										onkeypress="APP_onlyIntegers_iNgm_line(event,this)"
										onkeydown="RE_move_focus(event,this)"
										onfocusin="APP_set_previous_value(this);RE_parent_no_draggable(this);APP_play_input_on_click(this,'N');RE_set_current_position(this);RE_replace_inp_box_superscript(this)"
										onfocusout="RE_parent_draggable(this);RE_enter_new_value_dS(this,'iNgm')"
										ondblclick="RE_dbclick_inp_box(this)" onmouseenter="Hover_area_enter(this)"
										onmouseleave="Hover_area_exit(this)" maxlength="4" placeholder=" "></input>
										`
		}
		inner_string+= `
										<input class="App_RE_inp_box App_RE_note_cell App_RE_segment_last_cell" ondragstart="RE_disableEvent(event)"
										onkeydown="RE_move_focus(event,this);APP_onlyIntegers_denied(event,this)"
										onfocusin="APP_set_previous_value(this);RE_parent_no_draggable(this);APP_play_input_on_click(this,'N');RE_set_current_position(this);RE_replace_inp_box_superscript(this)"
										onfocusout="RE_parent_draggable(this);RE_enter_new_value_dS(this,'iNgm')"
										ondblclick="RE_dbclick_inp_box(this)" onmouseenter="Hover_area_enter(this)"
										onmouseleave="Hover_area_exit(this)" value="${end_range}" data-value="dotHover"></input>
										`
		new_iNgm_line.innerHTML=inner_string
		// var last_cell= new_iNgm_line.lastElementChild
		// last_cell.classList.add("App_RE_segment_last_cell")
		// last_cell.value=end_range
		// last_cell.disabled=true
		// last_cell.setAttribute("data-value", "dotHover")
		segment_obj.appendChild(new_iNgm_line)
	}

		// Nabc
	if(line_list_states.Nabc){
		var new_Nabc_line = document.createElement('div')
		new_Nabc_line.classList.add("App_RE_segment_line")
		new_Nabc_line.classList.add("App_RE_Nabc")
		var inner_string = ""

		for (let i = 0; i < cell_number-1; i++) {
			//new_Nabc_line.innerHTML+= `
			inner_string+= `
										<input class="App_RE_inp_box App_RE_note_cell" ondragstart="RE_disableEvent(event)"
											onkeydown="RE_move_focus(event,this);APP_onlyIntegers_denied(event,this)"
											onfocusin="APP_set_previous_value(this);RE_parent_no_draggable(this);APP_play_input_on_click(this,'N');RE_set_current_position(this)"
											onfocusout="RE_parent_draggable(this)"
											ondblclick="RE_dbclick_inp_box(this)" onmouseenter="Hover_area_enter(this)"
											onmouseleave="Hover_area_exit(this)"
											placeholder=" "></input>
										`
										// <input class="App_RE_inp_box App_RE_note_cell" ondragstart="RE_disableEvent(event)"
										// 	ondblclick="RE_dbclick_inp_box(this)" onmouseenter="Hover_area_enter(this)"
										// 	onmouseleave="Hover_area_exit(this)" onkeypress="APP_onlyIntegers_Nabc_line(event,this)" maxlength="3"
										// 	placeholder=" " disabled="true"></input>
										// `
		}
		inner_string+= `
										<input class="App_RE_inp_box App_RE_note_cell App_RE_segment_last_cell" ondragstart="RE_disableEvent(event)"
											onkeydown="RE_move_focus(event,this);APP_onlyIntegers_denied(event,this)"
											onfocusin="APP_set_previous_value(this);RE_parent_no_draggable(this);APP_play_input_on_click(this,'N');RE_set_current_position(this)"
											onfocusout="RE_parent_draggable(this)"
											ondblclick="RE_dbclick_inp_box(this)" onmouseenter="Hover_area_enter(this)"
											onmouseleave="Hover_area_exit(this)"
											value="${end_range}" data-value="dotHover"></input>
										`
		new_Nabc_line.innerHTML=inner_string
		// var last_cell= new_Nabc_line.lastElementChild
		// last_cell.classList.add("App_RE_segment_last_cell")
		// last_cell.value=end_range
		// last_cell.disabled=true
		// last_cell.setAttribute("data-value", "dotHover")
		segment_obj.appendChild(new_Nabc_line)
	}

	//hide central line if there is not time and sound in the same time
	var sound_lines = ["Na","Nabc","Nm","Ngm","iNa","iNm","iNgm"]
	var sound_present = sound_lines.some(id=>{
		return line_list_states[id]
	})
	var time_lines = ["Ta","Tm","Ts","iT","Tf"]
	var time_present = time_lines.some(id=>{
		return line_list_states[id]
	})
	var new_central_line = document.createElement('div')
	new_central_line.classList.add("App_RE_Matrix_Hline")
	if(!sound_present || !time_present){
		new_central_line.classList.add("hidden")
	}
	segment_obj.appendChild(new_central_line)

	// Ta
	if(line_list_states.Ta){
		var new_Ta_line = document.createElement('div')
		new_Ta_line.classList.add("App_RE_segment_line")
		new_Ta_line.classList.add("App_RE_Ta")
		//first column
		//new_Ta_line.innerHTML+= `
		var inner_string = `
										<input class="App_RE_inp_box App_RE_time_cell" ondragstart="RE_disableEvent(event)"
										onkeydown="RE_move_focus(event,this);APP_onlyIntegers_denied(event,this)"
										onfocusin="APP_set_previous_value(this);RE_parent_no_draggable(this),RE_set_current_position(this)"
										onfocusout="RE_parent_draggable(this)"
										ondblclick="RE_dbclick_inp_box(this)" onmouseenter="Hover_area_enter(this)"
										onmouseleave="Hover_area_exit(this)"
										maxlength="4" placeholder=" "></input>
										`

		for (let i = 1; i < cell_number-1; i++) {
			//new_Ta_line.innerHTML+= `
			inner_string+= `
										<input class="App_RE_inp_box App_RE_time_cell" ondragstart="RE_disableEvent(event)"
										onkeydown="RE_move_focus(event,this);APP_onlyIntegers_Ta_line(event,this)"
										onfocusin="APP_set_previous_value(this);RE_parent_no_draggable(this),RE_set_current_position(this)"
										onfocusout="RE_parent_draggable(this);"
										ondblclick="RE_dbclick_inp_box(this)" onmouseenter="Hover_area_enter(this)"
										onmouseleave="Hover_area_exit(this)"
										maxlength="4" placeholder=" "></input>
										`
		}
		inner_string+= `
										<input class="App_RE_inp_box App_RE_time_cell App_RE_segment_last_cell App_RE_last_cell_editable_input" ondragstart="RE_disableEvent(event)"
										onkeydown="RE_move_focus(event,this);APP_onlyIntegers_Ta_line(event,this)"
										onfocusin="APP_set_previous_value(this);RE_parent_no_draggable(this),RE_set_current_position(this)"
										onfocusout="RE_parent_draggable(this);"
										ondblclick="RE_dbclick_inp_box(this)" onmouseenter="Hover_area_enter(this)"
										onmouseleave="Hover_area_exit(this)"
										maxlength="4" placeholder=" "></input>
										`
		new_Ta_line.innerHTML= inner_string
		// var last_cell= new_Ta_line.lastElementChild
		// last_cell.classList.add("App_RE_segment_last_cell")
		// last_cell.classList.add("App_RE_last_cell_editable_input")
		segment_obj.appendChild(new_Ta_line)
	}

	// Tm
	if(line_list_states.Tm){
		var new_Tm_line = document.createElement('div')
		new_Tm_line.classList.add("App_RE_segment_line")
		new_Tm_line.classList.add("App_RE_Tm")
		//new_Tm_line.innerHTML+=`
		var inner_string = `
									<input class="App_RE_inp_box App_RE_time_cell" ondragstart="RE_disableEvent(event)"
										onkeydown="RE_move_focus(event,this);APP_onlyIntegers_denied(event,this)"
										onfocusin="APP_set_previous_value(this);RE_parent_no_draggable(this);RE_set_current_position(this);RE_replace_inp_box_superscript_Tm(this)"
										onfocusout="RE_parent_draggable(this);RE_replace_first_Tm_to_superscript(this);RE_resize_inp_box(this)"
										ondblclick="RE_dbclick_inp_box(this)" onmouseenter="Hover_area_enter(this)"
										onmouseleave="Hover_area_exit(this)" oninput="" maxlength="10"
										placeholder=" " Tm_triade=""></input>
									`

		for (let i = 1; i < cell_number-1; i++) {
			//new_Tm_line.innerHTML+=`
			inner_string+= `
									<input class="App_RE_inp_box App_RE_time_cell" ondragstart="RE_disableEvent(event)"
										onkeypress="APP_onlyIntegers_Tm_line(event,this)" onkeydown="RE_move_focus(event,this)"
										onfocusin="APP_set_previous_value(this);RE_parent_no_draggable(this);RE_replace_inp_box_superscript_Tm(this),RE_set_current_position(this)"
										onfocusout="RE_parent_draggable(this);RE_enter_new_value_T(this,'Tm');RE_resize_inp_box(this)"
										ondblclick="RE_dbclick_inp_box(this)" onmouseenter="Hover_area_enter(this)"
										onmouseleave="Hover_area_exit(this)" oninput="RE_resize_inp_box(this)" maxlength="10"
										placeholder=" " Tm_triade=""></input>
									`
		}
		inner_string+= `
									<input class="App_RE_inp_box App_RE_time_cell App_RE_segment_last_cell App_RE_last_cell_editable_input" ondragstart="RE_disableEvent(event)"
										onkeypress="APP_onlyIntegers_Tm_line(event,this)" onkeydown="RE_move_focus(event,this)"
										onfocusin="APP_set_previous_value(this);RE_parent_no_draggable(this);RE_replace_inp_box_superscript_Tm(this),RE_set_current_position(this)"
										onfocusout="RE_parent_draggable(this);RE_enter_new_value_T(this,'Tm');RE_resize_inp_box(this)"
										ondblclick="RE_dbclick_inp_box(this)" onmouseenter="Hover_area_enter(this)"
										onmouseleave="Hover_area_exit(this)" oninput="RE_resize_inp_box(this)" maxlength="10"
										placeholder=" " Tm_triade=""></input>
									`
		new_Tm_line.innerHTML= inner_string
		// var last_cell= new_Tm_line.lastElementChild
		// last_cell.classList.add("App_RE_segment_last_cell")
		// last_cell.classList.add("App_RE_last_cell_editable_input")
		segment_obj.appendChild(new_Tm_line)
	}

	// Ts BASIC
	if(line_list_states.Ts){
		var new_Ts_line = document.createElement('div')
		new_Ts_line.classList.add("App_RE_segment_line")
		new_Ts_line.classList.add("App_RE_Ts")
		//new_Ts_line.innerHTML+= `
		var inner_string= `
									<input class="App_RE_inp_box App_RE_time_cell" ondragstart="RE_disableEvent(event)"
											onkeydown="RE_move_focus(event,this);APP_onlyIntegers_denied(event,this)"
											onfocusin="APP_set_previous_value(this);RE_parent_no_draggable(this),RE_set_current_position(this)"
											onfocusout="RE_parent_draggable(this)"
											ondblclick="RE_dbclick_inp_box(this)" onmouseenter="Hover_area_enter(this)"
											onmouseleave="Hover_area_exit(this)" maxlength="4" placeholder=" "></input>
										`
		for (let i = 1; i < cell_number-1; i++) {
			//new_Ts_line.innerHTML+= `
			inner_string+= `
									<input class="App_RE_inp_box App_RE_time_cell" ondragstart="RE_disableEvent(event)"
											onkeypress="APP_onlyIntegers_Ts_line(event,this)" onkeydown="RE_move_focus(event,this)"
											onfocusin="APP_set_previous_value(this);RE_parent_no_draggable(this),RE_set_current_position(this)"
											onfocusout="RE_parent_draggable(this);RE_enter_new_value_T(this,'Ts')"
											ondblclick="RE_dbclick_inp_box(this)" onmouseenter="Hover_area_enter(this)"
											onmouseleave="Hover_area_exit(this)" maxlength="4" placeholder=" "></input>
										`
		}
		inner_string+= `
									<input class="App_RE_inp_box App_RE_time_cell App_RE_segment_last_cell App_RE_last_cell_editable_input" ondragstart="RE_disableEvent(event)"
											onkeypress="APP_onlyIntegers_Ts_line(event,this)" onkeydown="RE_move_focus(event,this)"
											onfocusin="APP_set_previous_value(this);RE_parent_no_draggable(this),RE_set_current_position(this)"
											onfocusout="RE_parent_draggable(this);RE_enter_new_value_T(this,'Ts')"
											ondblclick="RE_dbclick_inp_box(this)" onmouseenter="Hover_area_enter(this)"
											onmouseleave="Hover_area_exit(this)" maxlength="4" placeholder=" "></input>
										`
		new_Ts_line.innerHTML= inner_string
		//var last_cell= new_Ts_line.lastElementChild
		// last_cell.classList.add("App_RE_segment_last_cell")
		// last_cell.classList.add("App_RE_last_cell_editable_input")
		segment_obj.appendChild(new_Ts_line)
	}

	// iT
	if(line_list_states.iT){
		var new_iT_line = document.createElement('div')
		new_iT_line.classList.add("App_RE_segment_line")
		new_iT_line.classList.add("App_RE_iT")
		var inner_string=""
		for (let i = 0; i < cell_number-1; i++) {
			//new_iT_line.innerHTML+= `
			inner_string+= `
										<input class="App_RE_inp_box App_RE_time_cell" ondragstart="RE_disableEvent(event)"
										onkeypress="APP_onlyIntegers_iT_line(event,this)"
										onkeydown="RE_move_focus(event,this)"
										onfocusin="APP_set_previous_value(this);RE_parent_no_draggable(this),RE_set_current_position(this)"
										onfocusout="RE_parent_draggable(this);RE_enter_new_value_dT(this,'iT')"
										ondblclick="RE_dbclick_inp_box(this)" onmouseenter="Hover_area_enter(this)"
										onmouseleave="Hover_area_exit(this)" maxlength="2" placeholder=" "></input>
										`
		}
		//new_iT_line.innerHTML+= `
		inner_string+= `
										<input class="App_RE_inp_box App_RE_time_cell App_RE_segment_last_cell" ondragstart="RE_disableEvent(event)"
										onkeydown="RE_move_focus(event,this);APP_onlyIntegers_denied(event,this)"
										onfocusin="APP_set_previous_value(this);RE_parent_no_draggable(this),RE_set_current_position(this)"
										onfocusout="RE_parent_draggable(this)"
										ondblclick="RE_dbclick_inp_box(this)" onmouseenter="Hover_area_enter(this)"
										onmouseleave="Hover_area_exit(this)" maxlength="2" placeholder=" "></input>
										`
		//var last_cell= new_iT_line.lastElementChild
		new_iT_line.innerHTML+= inner_string
		//last_cell.classList.add("App_RE_segment_last_cell")
		segment_obj.appendChild(new_iT_line)
	}

	//Tf BASIC
	if(line_list_states.Tf){
		var new_Tf_line = document.createElement('div')
		new_Tf_line.classList.add("App_RE_segment_line")
		new_Tf_line.classList.add("App_RE_Tf")

		var max_fraction_index = segment_data.fraction.length-1
		var inner_string=""
		segment_data.fraction.forEach((fraction,fraction_index)=>{
			//change length  arrow
			//calculate times
			var time_fraction_start= DATA_calculate_exact_time(Psg_segment_start,fraction.start,0,1,1,N_NP,D_NP)
			var time_fraction_end= DATA_calculate_exact_time(Psg_segment_start,fraction.stop,0,1,1,N_NP,D_NP)

			//look how much elements there are in global time
			var index_start = RE_find_time_column_index(time_fraction_start,partial_RE_global_time_all_changes,partial_RE_global_time_segment_change)
			var index_end= RE_find_time_column_index(time_fraction_end,partial_RE_global_time_all_changes,partial_RE_global_time_segment_change)

			//set width arrow
			var arrow_length = (index_end-index_start)-1
			if(fraction_index==max_fraction_index){
				arrow_length--
			}

			//new_Tf_line.innerHTML+= `
			inner_string+= `
										<input class="App_RE_inp_box" ondragstart="RE_disableEvent(event)"
											onkeypress="APP_onlyIntegers_enter_fraction(event,this)"
											onkeydown="RE_move_focus(event,this)" onfocusin="APP_set_previous_value(this)"
											onfocusout="RE_enter_new_fraction(this)" oninput="APP_stop()" maxlength="3" value="${fraction.N}/${fraction.D}"></input>
										<button class="App_RE_frac_arrow_button" onclick="RE_break_fraction_range(this)" onmouseleave="RE_exit_break_fraction_range(this)" onmousemove="RE_break_fraction_range_calculate_position(this,event)"
											style="--index: ${arrow_length};">
											<img src="./Icons/bolt_yellow.svg" />
											<svg id="arrowline" data-name="Trazado 10" viewBox="0 0 28 28"
												preserveAspectRatio="none">
												<line x1="0" y1="14" x2="28" y2="14" style="stroke-width:2"></line>
											</svg>
											<svg id="arrowwings" data-name="Trazado 10" viewBox="0 0 28 28">
												<line x1="27" y1="14" x2="23" y2="10" style="stroke-width:2"></line>
												<line x1="27" y1="14" x2="23" y2="18" style="stroke-width:2"></line>
											</svg>
										</button>
										<input type="button" class="App_RE_frac_join_button" onclick="RE_join_fraction_ranges(this)"
							`
			if(fraction_index==max_fraction_index){
				inner_string+= `
											value="«" disabled="true">
										</input>
										`
			}else{
				inner_string+= `
					value="«">
										</input>
										`
			}
		})

		//disable last button merge fractions
		new_Tf_line.innerHTML=inner_string
		// var last_fraction_button= new_Tf_line.lastElementChild
		// last_fraction_button.disabled=true

		segment_obj.appendChild(new_Tf_line)
	}

	//ADD VALUES
	//write all lines
	RE_write_segment_Time_lines(segment_obj,line_list_states,index_columns,Psg_segment_start,segment_data,N_NP,D_NP)
	index_columns.pop()
	RE_write_segment_Note_lines(segment_obj,line_list_states,index_columns,Psg_segment_start,segment_data,N_NP,D_NP)//cut last index_columns
}

function RE_find_time_column_index(time,partial_RE_global_time_all_changes=null,partial_RE_global_time_segment_change=null){
	//Function give column index of a given time
	//if no time array is given it use the RE_global_time_all_changes AND RE_global_time_segment_change
	if (partial_RE_global_time_all_changes==null) {
		//look how much elements there are in global time and global time segment
		partial_RE_global_time_all_changes=RE_global_time_all_changes
		partial_RE_global_time_segment_change=RE_global_time_segment_change
	}
	var index_all = partial_RE_global_time_all_changes.indexOf(time)
	//var index_segment = partial_RE_global_time_segment_change.indexOf(time)
	var index_segment = 0
	partial_RE_global_time_segment_change.find((seconds,index)=>{
		if(seconds==time){
			index_segment=index
			return true
		}else if (seconds>time){
			index_segment=index-1
			return true
		}
		return false
	})
	if(index_segment<0)console.error("time not found SEGMENT")
	if(index_all<0){
		console.error("time not found")
		console.log(partial_RE_global_time_segment_change)
		console.log(time)
	}
	return (index_all+index_segment)*2
}

function RE_disableEvent(event) {
	event.preventDefault()
	event.stopPropagation()
	return false
}

function RE_dbclick_inp_box(element){
	//iA or element
	APP_stop()
	var time = 0
	var pulse_abs = 0
	var pulse = 0
	var fraction = 0
	var segment_index = 0
	var voice_index = 0
	if(element.classList.contains("App_RE_iA_cell")){
		//return
		var info_list=RE_find_voice_elements_info_from_iA_element(element)
		//sub with the last one
		//var position = info.up.abs_position
		var info_up = info_list.up.info
		pulse = info_up.time.P
		pulse_abs = (info_up.Psg_segment_start + pulse)*info_up.N_NP/info_up.D_NP
		fraction = info_up.time.F
		time=DATA_calculate_exact_time(info_up.Psg_segment_start,pulse,fraction,info_up.fraction.N,info_up.fraction.D,info_up.N_NP,info_up.D_NP)
		segment_index = info_list.up.segment_index
		voice_index = info_list.up.voice_index
	}else{
		var [elementIndex,element_list]=RE_element_index(element,".App_RE_inp_box")
		var position = 0
		element_list.find((inp,index)=>{
			if (index==elementIndex)return true
			if(inp.value!="")position++
		})

		var segment_obj = element.closest(".App_RE_segment")
		var [s_index,]=RE_element_index(segment_obj,".App_RE_segment")
		segment_index=s_index
		var voice_obj = segment_obj.closest(".App_RE_voice")
		voice_index = RE_read_voice_id(voice_obj)[1]
		var info = DATA_get_object_index_info(voice_index,segment_index,position)

		pulse = info.time.P
		pulse_abs = (info.Psg_segment_start + pulse)*info.N_NP/info.D_NP
		fraction = info.time.F
		time=DATA_calculate_exact_time(info.Psg_segment_start,pulse,fraction,info.fraction.N,info.fraction.D,info.N_NP,info.D_NP)

		/*
		var data = DATA_get_current_state_point(true)
		//Selected voice
		var selected_voice_data= data.voice_data.find(item=>{
			return item.voice_index==voice_index
		})

		var all_segment_data = selected_voice_data.data.segment_data
		var current_segment_data = all_segment_data[segment_index]

		var Psg_segment_start=0
		all_segment_data.some((segment_data,index)=>{
			if(index==segment_index){
				return true
			}else{
				Psg_segment_start+=segment_data.time.slice(-1)[0].P
			}
		})
		var pulse = current_segment_data.time[position].P
		var fraction = current_segment_data.time[position].F
		var current_fraction_range = current_segment_data.fraction.find(range=>{
			return range.stop>pulse
		})

		var N=1
		var D=1
		if(current_fraction_range!=null){
			//null in last segment pulse
			N=current_fraction_range.N
			D=current_fraction_range.D
		}

		var N_NP=selected_voice_data.neopulse.N
		var D_NP=selected_voice_data.neopulse.D
		var pulse_abs = (Psg_segment_start + pulse)*N_NP/D_NP

		var time=DATA_calculate_exact_time(Psg_segment_start,pulse,fraction,N,D,N_NP,D_NP)
		*/
	}

	APP_player_to_time(time,pulse_abs,pulse,fraction,segment_index,voice_index)
	APP_play_all_notes_at_time(time)
}

function RE_voice_scrolling(voice_obj,event){
	if(voice_obj.scrollLeft != scrollbarREposition){
		RE_checkScroll_H(voice_obj.scrollLeft)
	}
}

//find the index of an element, default type ="input"
//for arrows and join etc = "button"
function RE_element_index(element, type){
	var parent = element.parentNode
	//if parent is null (element being deleted....)
	if(parent==null)return[-1,-1]
	var input_list = [...parent.querySelectorAll(type)]
	var elementIndex=input_list.indexOf(element)
	return [elementIndex,input_list]
}

//find whatever previous cell is not empty
function RE_find_prev_filled_element(element, string="input"){  //XXX XXX XXX XXX XXX XXX
	var parent = element.parentNode
	var input_list = [...parent.querySelectorAll(string)]
	var elementIndex = input_list.indexOf(element)
	var index_previous = -1

	for (var i=elementIndex-1; i>=0;i--){
		var value = input_list[i].value
		if(value!="" && value!=end_range){
			//element not void
			index_previous=i
			i=-1
		}
	}

	if (index_previous>=0){
		var distance = elementIndex-index_previous
		return [input_list[index_previous], index_previous, distance]
	}else{
		return [null, -1,-1]
	}
}

function RE_find_next_filled_element(element,string="input"){  //XXX    //XXX XXX XXX XXX XXX XXX
console.error("OBSOLETE FUNCTION RE_find_next_filled_element used only in ADD MIDI RE AND FOCUS")
	var parent = element.parentNode
	var input_list = [...parent.querySelectorAll(string)]
	var elementIndex=input_list.indexOf(element)
	var arr = input_list.filter((item)=>{
		return input_list.indexOf(item)>input_list.indexOf(element)
	})
	var next_filled_element= arr.find((item)=>{
		//return item.value!="" && item.value!=end_range && item.value!="tie_intervals" ///With ""??? XXX
		return item.value!="" && item.value!=end_range
	})
	var next_filled_element_index =-1
	var next_filled_element_distance =-1
	if(next_filled_element!=null){
		next_filled_element_index = input_list.indexOf(next_filled_element)
		next_filled_element_distance = next_filled_element_index-elementIndex
	}
	return [next_filled_element,next_filled_element_index, next_filled_element_distance]
}

function RE_write_segment_name(segment, name, display_name){
	var input_segment_name = segment.querySelector(".App_RE_segment_name")
	input_segment_name.value = name

	if(name=="" || !display_name){
		input_segment_name.classList.add("hidden")
	}else{
		input_segment_name.classList.remove("hidden")
	}
}

function RE_read_segment_name(segment){
	return segment.querySelector(".App_RE_segment_name").value
}

function RE_change_segment_name(element){
	element.blur()
	APP_stop()
	var data = DATA_get_current_state_point(true)
	//Selected voice
	var voice_obj = element.closest(".App_RE_voice")
	var [,voice_index] = RE_read_voice_id(voice_obj)
	var selected_voice_data= data.voice_data.find(item=>{
		return item.voice_index==voice_index
	})
	//find the segment index
	var container = element.closest(".App_RE_segment_draggable")
	var segment_index= parseInt(container.querySelector(".App_RE_segment_number").innerHTML)

	selected_voice_data.data.segment_data[segment_index].segment_name = element.value

	if(element.value==""){
		element.classList.add("hidden")
	}else{
		element.classList.remove("hidden")
	}

	if(selected_voice_data.blocked){
		//change name all blocked voices
		data.voice_data.forEach(item=>{
			if(item.blocked)item.data.segment_data[segment_index].segment_name = element.value
		})
	}
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)//XXX NEED TO LOAD ????  FLAGS == TRUE/FALSE???
}

function RE_hide_segment_name(input) {
	if(input.classList.contains("washidden")){
		input.classList.add("hidden")
		input.classList.remove("washidden")
	}
}

function RE_parent_no_draggable(element){
	var segment = element.parentNode.parentNode
	segment.setAttribute("draggable", "false")
	var voice = segment.parentNode.parentNode
	voice.setAttribute("draggable", "false")
}

function RE_parent_draggable(element){
	var segment = element.closest(".App_RE_segment")
	segment.setAttribute("draggable", "true")
	//var voice = segment.parentNode.parentNode
	var voice = segment.closest(".App_RE_voice")
	voice.setAttribute("draggable", "true")
}

//segments management
function RE_add_segment_button(button){
	APP_stop()
	// define segment object
	var voice_data = button.closest(".App_RE_voice_data")
	var voice = button.closest(".App_RE_voice")
	var segment_list = [...voice_data.querySelectorAll(".App_RE_segment")]
	// define index of spacer
	var spacer = button.closest(".App_RE_segment_spacer")
	var [segment_index,] = RE_element_index(spacer, ".App_RE_segment_spacer")
	//console.log(parseInt(voice.getAttribute("value")))
	//console.log(RE_read_voice_id(voice))
	DATA_add_segment(RE_read_voice_id(voice)[1],segment_index)
}

function RE_delete_segment_button(button){
	var voice=button.closest(".App_RE_voice")
	var [segment_index, segment_list] = RE_element_index(button.closest(".App_RE_segment"),".App_RE_segment")
	DATA_delete_segment(RE_read_voice_id(voice)[1],segment_index)
}

function RE_merge_segment_button(button){
	APP_stop()
	// define segment object
	var voice_data = button.closest(".App_RE_voice_data")
	var voice = button.closest(".App_RE_voice")
	//var segment_list = [...voice_data.querySelectorAll(".App_RE_segment")]
	// define index of spacer
	var spacer = button.closest(".App_RE_segment_spacer")
	var [spacer_index,] = RE_element_index(spacer, ".App_RE_segment_spacer")
	var segment_index=spacer_index-1
	DATA_merge_segment(RE_read_voice_id(voice)[1],segment_index)
}

function RE_break_segment_button(button) {
	APP_stop()
	var segment = button.closest('.App_RE_segment')
	var voice_index = RE_read_voice_id(segment.closest(".App_RE_voice"))[1]
	var segment_index = [...segment.closest(".App_RE_voice_data").querySelectorAll(".App_RE_segment")].indexOf(segment)
	var input_P_list = segment.querySelectorAll(".App_RE_Ts>input")
	//all the calculation if breakable is done by the spawning of the button
	var P_target_index = Number(button.value)
	var pulse = parseInt(input_P_list[P_target_index].value)
	DATA_break_segment(voice_index,segment_index, pulse)
}

function RE_read_voice_id(voice_obj){
	var name = voice_obj.querySelector(".App_RE_voice_name").value
	var voice_index = Number(voice_obj.getAttribute("value"))
	return [name, voice_index]
}

function RE_find_voice_obj_by_index(voice_index){
	var data = document.querySelector(".App_RE")
	var voice_matrix = data.querySelector(".App_RE_voice_matrix")
	var voice_list = [...voice_matrix.querySelectorAll(".App_RE_voice")]

	if(voice_list.length==0)return null

	var voice = voice_list.find((voice)=>{
		//find voice id
		var [voice_name, voice_id] = RE_read_voice_id(voice)
		if(voice_index == voice_id) return voice
	})
	return voice
}

function RE_write_voice_name_ID(voice,name,index){
	var voice_name = voice.querySelector(".App_RE_voice_name")
	var voice_name_minimized = voice.querySelector(".App_RE_voice_header_R_minimized_button>p")
	voice_name.value=name
	voice.setAttribute("value",index)
	voice_name_minimized.innerHTML=name
}

function RE_set_H_scrollbar_step_N(n_boxes){
	var data = document.querySelector(".App_RE")
	var scrollbar_line = data.querySelector(".App_RE_main_scrollbar_H")
	var step = scrollbar_line.querySelector(".App_RE_element_step")

	//change div width according to required boxes
	step.style.minWidth = "calc(var(--RE_block) * "+n_boxes+")"
	step.style.width = "calc(var(--RE_block) * "+n_boxes+")"
}

//return Nom and Den of a voice polipulse fraction
function RE_Calculate_NP(voice){
	var NP_voice = voice.querySelector(".App_RE_neopulse_inp_box").value
	var split = NP_voice.split('/')
	var N = Math.floor(split[0])
	var D = Math.floor(split[1])
	return [N,D]
}

//scrollbars sync with different voices
let scrollbarREposition = 0   ////XXX XXX XXX XXX XXX XXX   ???? global variable????
function RE_checkScroll_H(value=null,forceScrollbarCheck = false) {
	//.App_RE_voice_data
	//console.error("forceScrollbarCheck = false")
	var data = document.querySelector('.App_RE')
	var voice_matrix = data.querySelector('.App_RE_voice_matrix')
	var C_line = data.querySelector('.App_RE_C')
	var S_line = data.querySelector('.App_RE_S')
	var column_selector = document.getElementById('App_RE_column_selector_container')
	// Single scrollbar (to be implemented .RE_no_scrollbar and .RE_main_scrollbar)
	var voice_list = [...voice_matrix.querySelectorAll('.App_RE_voice_data')]
	var scrollbar = data.querySelector('.App_RE_main_scrollbar_H')
	var iA_list = [...voice_matrix.querySelectorAll('.App_RE_iA:not(.hidden)')]
	//
	if(!scrollbar || !voice_list || !C_line || !S_line || !column_selector) return;

	if(value!=null){
		//force scrollbar position
		scrollbar.scrollLeft=value
		forceScrollbarCheck=true
	}

	if(scrollbar.scrollLeft != scrollbarREposition) {
		scrollbarREposition = scrollbar.scrollLeft
		voice_list.forEach((item)=>{item.scrollLeft = scrollbar.scrollLeft})
		iA_list.forEach((item)=>{item.scrollLeft =scrollbar.scrollLeft})
		C_line.scrollLeft = scrollbar.scrollLeft
		S_line.scrollLeft = scrollbar.scrollLeft
		column_selector.scrollLeft = scrollbar.scrollLeft
	}

	if(forceScrollbarCheck){
		scrollbarREposition = scrollbar.scrollLeft
		voice_list.forEach((item)=>{item.scrollLeft = scrollbar.scrollLeft})
		iA_list.forEach((item)=>{item.scrollLeft = scrollbar.scrollLeft})
		C_line.scrollLeft = scrollbar.scrollLeft
		S_line.scrollLeft = scrollbar.scrollLeft
		column_selector.scrollLeft = scrollbar.scrollLeft
	}
	if(current_position_PMCRE.REscrollbar_redraw){
		RE_verify_segment_visibility()
		//console.log("verify seg visib deactivated/activated")
	}
}

function RE_refresh_iA_voices_labels(RE){
	//find the one that is true
	var active = "none"
	var iA_button_states={"iAa":RE.iAa,"iAm":RE.iAm,"iAgm":RE.iAgm}
	Object.keys(iA_button_states).forEach(id=>{
		if(iA_button_states[id]==true){
			if(id=="iAa"){
				active=id
				return
			}
			if(id=="iAm"){
				active="iAm"
				return
			}
			if(id=="iAgm"){
				active="iA°"
				return
			}
		}
	})
	var iA_divs_list = [...document.querySelector(".App_RE").querySelector(".App_RE_voice_matrix").querySelectorAll(".App_RE_voices_between")]
	var iA_label_list = iA_divs_list.map(item=>{
		return item.querySelector(".App_RE_label")
	})
	var iA_line_list = iA_divs_list.map(item=>{
		return item.querySelector(".App_RE_iA")
	})
	//change labels and content RE_iA
	if(active!="none"){
		iA_label_list.forEach(label=>{
			label.innerHTML=active
			label.setAttribute('data-value',`${active}Hover`)
		})
		iA_line_list.forEach(line=>{
			line.style.display=null
		})
	}else{
		//hide lines
		iA_label_list.forEach(label=>{
			label.innerHTML=""
		})
		iA_line_list.forEach(line=>{
			line.style.display="none"
		})
	}
}

function RE_input_to_position(element){
	var voice_obj = element.closest(".App_RE_voice")
	var voice_index = RE_read_voice_id(voice_obj)[1]
	var segment_list= [...voice_obj.querySelectorAll(".App_RE_segment")]
	var segment_obj = element.closest(".App_RE_segment")
	//console.log(segment_obj)
	var segment_index = segment_list.indexOf(segment_obj)

	var [elementIndex,element_list]=RE_element_index(element,".App_RE_inp_box")
	var position= -1

	if(elementIndex==0){
		//iT can have elemenindex=0 and position -1...
		position=0
	}else{
		element_list.find((element,index)=>{
			if(element.value!="")position++
			return index==elementIndex
		})
	}

	return {position:position,segment_index:segment_index,voice_index,voice_index}
}

function RE_set_current_position(element){
	var pos = RE_input_to_position(element)
	var element_info = DATA_get_object_index_info(pos.voice_index,pos.segment_index,pos.position)

	//RE position
	APP_reset_blink_play()
	var [column_index,]=RE_element_index(element,".App_RE_inp_box")
	var segment_obj = element.closest(".App_RE_segment")
	RE_highlight_column_at_inp_box_index(column_index,segment_obj,"current")

	//find voicedata
	var current_voice_data=DATA_get_current_state_point(false).voice_data.find(voice=>{return voice.voice_index==pos.voice_index})

	//PMC_position if needed
	if(current_voice_data.selected){
		if(element_info.time.P==null){
			//clear PMC
			PMC_reset_progress_line()
		}else{
			var time = 0
			if(element_info.fraction==null){
				//console.log("end of segment")
				//time = (Psg_segment_start+pulse) *current_voice_data.neopulse.N/current_voice_data.neopulse.D * 60/PPM
				time= DATA_calculate_exact_time(element_info.Psg_segment_start,element_info.time.P,0,1,1,current_voice_data.neopulse.N,current_voice_data.neopulse.D)
			}else{
				//time = (Psg_segment_start+pulse+fraction *current_fraction.N/current_fraction.D) *current_voice_data.neopulse.N/current_voice_data.neopulse.D * 60/PPM
				time= DATA_calculate_exact_time(element_info.Psg_segment_start,element_info.time.P,element_info.time.F,element_info.fraction.N,element_info.fraction.N,current_voice_data.neopulse.N,current_voice_data.neopulse.D)
			}

			PMC_draw_progress_line_time(time)
		}
	}else{
		//clear PMC
		PMC_reset_progress_line()
	}

	current_position_PMCRE.voice_data= current_voice_data
	//current_position_PMCRE.X_start = null //PMC_draw_progress_line_time OR PMC_reset_progress_line  write it down
	current_position_PMCRE.Y_start = null
	current_position_PMCRE.segment_index = pos.segment_index
	current_position_PMCRE.pulse = element_info.time.P
	current_position_PMCRE.fraction = element_info.time.F
	current_position_PMCRE.note = null

}

function RE_highlight_column_at_inp_box_index(column_index,segment_obj,color="current"){

	var line_list = [...segment_obj.querySelectorAll(".App_RE_segment_line:not(.App_RE_Tf)")]

	line_list.forEach(line=>{
		var inp_box_list = [...line.querySelectorAll(".App_RE_inp_box")]
		if(line.classList.contains("App_RE_iT")){
			if(color=="current"){
				APP_blink_play_green_light(inp_box_list[column_index+1])
			}else{
				//APP_blink_play_blue(inp_box_list[column_index+1])
				APP_blink_play_green(inp_box_list[column_index+1])
			}

		}else{
			if(color=="current"){
				APP_blink_play_green_light(inp_box_list[column_index])
			}else{
				//APP_blink_play_blue(inp_box_list[column_index])
				APP_blink_play_green(inp_box_list[column_index])
			}
		}
	})
}

function RE_highlight_columns_at_pulse(pulse_abs ,time=null, voice_index=null){
	// function used only by time bar , never used fraction
	//pulse => Ps
	if(!flag_DATA_updated.RE)return
	if(!tabREbutton.checked)return
	//console.log(pulse_abs+"   "+time)
	APP_reset_blink_play()
	//find correct pulse
	//for every voice
	var voice_matrix_obj = document.querySelector('.App_RE_voice_matrix')
	var voice_obj_list = [...voice_matrix_obj.querySelectorAll('.App_RE_voice')]

	var index_scrollbar = null
	var move = false

	if(time==null){
		time = DATA_calculate_exact_time(pulse_abs,0,0,1,1,1,1)
	}

	var data = DATA_get_current_state_point(true)

	if(pulse_abs<Li){
		voice_obj_list.forEach(voice_obj=>{
			//use RE_global_time_voices_changes
			var current_voice_index = RE_read_voice_id(voice_obj)[1]

			//Selected voice
			var selected_voice_data= data.voice_data.find(item=>{
				return item.voice_index==current_voice_index
			})

			//if not visible end
			if(!selected_voice_data.RE_visible){
				return
			}

			var global_voice_index=null
			var found = selected_voice_data.data.midi_data.time.some((midi_time,index) =>{
				if(midi_time==time){
					global_voice_index=index
					return true
				}
				return false
			})

			if(found){
				var all_segment_data = selected_voice_data.data.segment_data
				var position = null
				var column_index = null
				var segment_index = null
				var i_end=0
				all_segment_data.some((segment,index_s)=>{
					segment.time.pop()
					var i_start=i_end
					i_end+=segment.time.length
					if(i_end>=global_voice_index){
						segment_index=index_s
						return segment.time.some((t,index_t)=>{
							if(i_start+index_t==global_voice_index){
								position=index_t
								return true
							}else{
								return false
							}
						})
					}else{
						return false
					}
				})

				var segment_obj_list= [...voice_obj.querySelectorAll(".App_RE_segment")]
				var segment_obj = segment_obj_list[segment_index]
				var segment_line_obj = segment_obj.querySelector(".App_RE_segment_line")//the first is fine
				var input_list = [...segment_line_obj.querySelectorAll(".App_RE_inp_box")]
				var i=-1
				input_list.find((input,index)=>{
					if (input.value!=""){
						i++
						if(i==position){
							column_index=index
							return true
						}else{
							return false
						}
					}
				})

				if(current_voice_index!=voice_index){
					RE_highlight_column_at_inp_box_index(column_index,segment_obj,"other")
				}else{
					RE_highlight_column_at_inp_box_index(column_index,segment_obj,"current")
				}

				if(index_scrollbar==null){
					var past_segments_columns = segment_obj_list.reduce((prop,item,index_s)=>{
						if(index_s<segment_index){
							return prop+1+item.clientWidth/nuzic_block
						}else{
							return prop
						}
					},0)
					index_scrollbar = column_index + past_segments_columns
					move=true
				}
			}
		})

		//move slider in best position if founs something or if bigger than Li (is a compas)
		if(move){
			//move slider to position
			RE_calculate_best_scrollbar_position(index_scrollbar)
		}
	}else{
		//calculate compas position
		var C_line = document.querySelector(".App_RE_C")
		var C_button_list =  [...C_line.querySelectorAll(".App_RE_C_button")]

		var found = C_button_list.find(button => {
			if(button.value!=0){
				var variables = JSON.parse(button.value)
				if (variables.C_values[0]>=pulse_abs){
					return button
				}
			}
		})
		//find index compas
		if(found===undefined)return
		var str = found.style.left
		var n_pixels_left=parseInt(str.substring(0, str.length - 2))
		//var tot_buttons = scale_button_list.length
		var index_compas = n_pixels_left/nuzic_block+1

		//move slider to position
		RE_calculate_best_scrollbar_position(index_compas)
	}
}

function RE_hightlight_column_on_play_on(segment_obj,column_index){
	//APP_blink_play_input_list(element_list)
	var loader = segment_obj.querySelector(".loading")
	if(loader==null){
		var lines=segment_obj.querySelectorAll(".App_RE_segment_line:not(.App_RE_Tf)")
		var child_n=column_index+1
		lines.forEach(line=>{
			var element=null
			if(line.classList.contains("App_RE_iT")){
				element = line.querySelector(":nth-child("+(child_n+1)+")")
			}else{
				element = line.querySelector(":nth-child("+child_n+")")
			}
			if(element!=null){
				APP_blink_play_blue(element)
			}else{
				console.error("failed to blink column "+column_index)
			}
		})
	}else{
		//wait??? TOO LATE????
		console.error("wait for segment to load")
		console.log(loader)
	}
}

function RE_hightlight_column_on_play_off(segment_obj,column_index){
	var loader = segment_obj.querySelector(".loading")
	if(loader==null){
		var lines=segment_obj.querySelectorAll(".App_RE_segment_line:not(.App_RE_Tf)")
		var child_n=column_index+1
		lines.forEach(line=>{
			var element=null
			if(line.classList.contains("App_RE_iT")){
				element = line.querySelector(":nth-child("+(child_n+1)+")")
			}else{
				element = line.querySelector(":nth-child("+child_n+")")
			}
			if(element!=null){
				APP_stop_blink_play_blue(element)
			}else{
				console.error("failed to blink column "+column_index)
			}
		})
	}else{
		//wait??? TOO LATE?????
		//console.error("wait for segment to load")
	}
}

let old_index_position_scrollbar = 0
function RE_calculate_best_scrollbar_position(index_position){
	if(!tabREbutton.checked){
		return
	}
	//
	index_position++
	var data = document.querySelector('.App_RE')
	var scrollbar = data.querySelector('.App_RE_main_scrollbar_H')

	//see how big is the scrollbar
	var width = scrollbar.clientWidth -196 -13 //padding
	//var width = scrollbar.scrollWidth-scrollbar.scrollLeftMax
	//see how many sqares is the visible space
	var Nsq = Math.round(width / 28)
	//how many squares in total
	var Nsq_max = Math.round(scrollbar.scrollWidth / 28) -1

	var range = scrollbar.scrollWidth-width

	//better steps
	if(Nsq>12){
		//defining the common part Nsq_common width
		//the better fit bw 6,8,10
		var buffer = [6,8,10]
		var sum = buffer.map( (val, i) => val + Nsq_max )
		var remainder = sum.map( (val, i) => val % Nsq )

		//take it bigger remainder
		var best = Math.max.apply(null,remainder)
		var best_index = remainder.indexOf(best)
		var Nsq_common = buffer[best_index]
		var mod = Nsq - Nsq_common
		var step = scrollbar.scrollWidth / Nsq_max

		if((index_position%mod)<Nsq_common-1 && index_position>Nsq_common){
			//inside common zone, deciding if necesary to jump page
			var mod_position = Math.round(index_position/mod)
			//determine if going right or left
			//going right
			if(index_position>old_index_position_scrollbar){
				//console.log("RIGHT")
				RE_checkScroll_H(mod_position*mod*step)
				old_index_position_scrollbar=index_position
				return
			}
			//going left
			if(index_position<old_index_position_scrollbar){
				//console.log("LEFT")
				RE_checkScroll_H((mod_position-1)*mod*step)
				old_index_position_scrollbar=index_position
				return
			}
		}else{
			//maybe simple control being in the correct place
			//case jump from fringe Nsq_common
			var init_position = Math.round(scrollbar.scrollLeft / step)
			var end_position = init_position + Nsq


			if(index_position < init_position || index_position > end_position){
				//console.log("need to move")
				var mod_position = Math.round(index_position/mod)
				RE_checkScroll_H((mod_position)*mod*step)
				old_index_position_scrollbar=index_position
				return
			}
		}
		old_index_position_scrollbar=index_position
	}else{
		//scroll 1 on 1
		var step = range / Nsq_max
		if(index_position==-1)index_position=Nsq_max
		RE_checkScroll_H(index_position*step)
		old_index_position_scrollbar=index_position
	}
}

function RE_verify_segments_button_visibility(){
	//switch on off segment buttons
	//off first and last merge
	//off delete if only one segment
	//list of voices
	var voice_matrix = document.querySelector(".App_RE_voice_matrix")
	var voice_list = [...voice_matrix.querySelectorAll(".App_RE_voice")]
	var delete_voice_button_list = voice_list.map(voice=>{
		return voice.querySelector(".App_RE_delete_voice")
	})
	//delete voice button visibility
	if(delete_voice_button_list.length==1){
		//disable
		//delete_voice_button_list[0].style.display = "none"
		//delete_voice_button_list[0].disabled=true //is a <a>
		delete_voice_button_list[0].classList.add("disabled")
	}else{
		delete_voice_button_list.forEach(button =>{
			//button.style.display = "inline"
			//button.disabled=false
		})
	}

	voice_list.forEach(voice=>{
		//list of segments
		var segment_list = [...voice.querySelectorAll(".App_RE_segment")]
		var delete_segment_button_list = segment_list.map(segment=>{
			return segment.querySelector(".App_RE_delete_segment")
		})
		if(delete_segment_button_list.length==1){
			delete_segment_button_list[0].style.display = "none"
		}else{
			delete_segment_button_list.forEach(button =>{
				button.style.display = "inline"
			})
		}

		var segment_spacer_list = [...voice.querySelectorAll(".App_RE_segment_spacer")]
		//App_RE_segment_spacer_buttons
		var merge_segment_button_list = segment_spacer_list.map(segment_spacer =>{
			return segment_spacer.querySelector(".App_RE_merge_segment")
		})
		var first_merge_segment_button = merge_segment_button_list.shift()
		var last_merge_segment_button = merge_segment_button_list.pop()
		first_merge_segment_button.style.display = "none"
		last_merge_segment_button.style.display = "none"
		//enable and disable
		merge_segment_button_list.forEach(button=>{
			button.style.display = "inline"
		})
	})
}

function RE_string_to_superscript(string){
	//return the current text in subscript format
	var chars = '+−-=()0123456789AaÆᴂɐɑɒBbcɕDdðEeƎəɛɜɜfGgɡɣhHɦIiɪɨᵻɩjJʝɟKklLʟᶅɭMmɱNnɴɲɳŋOoɔᴖᴗɵȢPpɸrRɹɻʁsʂʃTtƫUuᴜᴝʉɥɯɰʊvVʋʌwWxyzʐʑʒꝯᴥβγδθφχнნʕⵡ'
	var sup = '⁺⁻⁻⁼⁽⁾⁰¹²³⁴⁵⁶⁷⁸⁹ᴬᵃᴭᵆᵄᵅᶛᴮᵇᶜᶝᴰᵈᶞᴱᵉᴲᵊᵋᶟᵌᶠᴳᵍᶢˠʰᴴʱᴵⁱᶦᶤᶧᶥʲᴶᶨᶡᴷᵏˡᴸᶫᶪᶩᴹᵐᶬᴺⁿᶰᶮᶯᵑᴼᵒᵓᵔᵕᶱᴽᴾᵖᶲʳᴿʴʵʶˢᶳᶴᵀᵗᶵᵁᵘᶸᵙᶶᶣᵚᶭᶷᵛⱽᶹᶺʷᵂˣʸᶻᶼᶽᶾꝰᵜᵝᵞᵟᶿᵠᵡᵸჼˤⵯ'
	return sup_string = [...string].map(char => {
		//console.log(char)
		var index = chars.indexOf(char)
		return (index != -1 ? sup[index] : char)
	}).join("")
}

function RE_superscript_to_string(string){
	var chars = '+−-=()0123456789AaÆᴂɐɑɒBbcɕDdðEeƎəɛɜɜfGgɡɣhHɦIiɪɨᵻɩjJʝɟKklLʟᶅɭMmɱNnɴɲɳŋOoɔᴖᴗɵȢPpɸrRɹɻʁsʂʃTtƫUuᴜᴝʉɥɯɰʊvVʋʌwWxyzʐʑʒꝯᴥβγδθφχнნʕⵡ'
	var sup = '⁺⁻⁻⁼⁽⁾⁰¹²³⁴⁵⁶⁷⁸⁹ᴬᵃᴭᵆᵄᵅᶛᴮᵇᶜᶝᴰᵈᶞᴱᵉᴲᵊᵋᶟᵌᶠᴳᵍᶢˠʰᴴʱᴵⁱᶦᶤᶧᶥʲᴶᶨᶡᴷᵏˡᴸᶫᶪᶩᴹᵐᶬᴺⁿᶰᶮᶯᵑᴼᵒᵓᵔᵕᶱᴽᴾᵖᶲʳᴿʴʵʶˢᶳᶴᵀᵗᶵᵁᵘᶸᵙᶶᶣᵚᶭᶷᵛⱽᶹᶺʷᵂˣʸᶻᶼᶽᶾꝰᵜᵝᵞᵟᶿᵠᵡᵸჼˤⵯ'
	var normalized_string = [...string].map(char => {
		//console.log(char)
		var index = sup.indexOf(char)
		return (index != -1 ? chars[index] : char)
	}).join("")
	return normalized_string
}

function RE_superscript_to_Tm_string(string){
	var chars = '+−-=()0123456789AaÆᴂɐɑɒBbcɕDdðEeƎəɛɜɜfGgɡɣhHɦIiɪɨᵻɩjJʝɟKklLʟᶅɭMmɱNnɴɲɳŋOoɔᴖᴗɵȢPpɸrRɹɻʁsʂʃTtƫUuᴜᴝʉɥɯɰʊvVʋʌwWxyzʐʑʒꝯᴥβγδθφχнნʕⵡ'
	var sup = '⁺⁻⁻⁼⁽⁾⁰¹²³⁴⁵⁶⁷⁸⁹ᴬᵃᴭᵆᵄᵅᶛᴮᵇᶜᶝᴰᵈᶞᴱᵉᴲᵊᵋᶟᵌᶠᴳᵍᶢˠʰᴴʱᴵⁱᶦᶤᶧᶥʲᴶᶨᶡᴷᵏˡᴸᶫᶪᶩᴹᵐᶬᴺⁿᶰᶮᶯᵑᴼᵒᵓᵔᵕᶱᴽᴾᵖᶲʳᴿʴʵʶˢᶳᶴᵀᵗᶵᵁᵘᶸᵙᶶᶣᵚᶭᶷᵛⱽᶹᶺʷᵂˣʸᶻᶼᶽᶾꝰᵜᵝᵞᵟᶿᵠᵡᵸჼˤⵯ'

	var normalized_string = [...string].map(char => {
		//console.log(char)
		var index = sup.indexOf(char)
		return (index != -1 ? "c"+chars[index] : char)
	}).join("")

	//clear extra "c" char
	var first = true
	var output_string = [...normalized_string].filter(char =>{
		if(char=="c"){
			if(first){
				first=false
				return true
			}else{
				return false
			}
		}else{
			return true
		}
	}).join("")

	return output_string
}

function RE_replace_inp_box_superscript_Tm(element){
	var string = element.value
	element.value=RE_superscript_to_Tm_string(string)
	RE_resize_inp_box(element)
}

function RE_replace_first_Tm_to_superscript(element){
	//console.log(previous_value)
	element.value=previous_value
}

function RE_replace_inp_box_superscript(element){
	var string = element.value
	element.value=RE_superscript_to_string(string)
	//RE_resize_inp_box(element)
}

function RE_resize_inp_box(element){
	//onchange only at intro...
	//oninput is better
	var width = element.value.length*8

	var margin = 0
	if(width>28){
		margin = -(element.value.length*8-28)/2
		element.style.width = element.value.length*8 + "px"
	}else {
		element.style.width = "28px"
	}
	element.style.marginLeft = margin + "px"
	element.style.marginRight = margin + "px"
}

function RE_refresh_segments_number_values(voice){
	var segment_list = [...voice.querySelectorAll(".App_RE_segment")]

	var i=0
	segment_list.forEach(segment=>{
		var segment_number = segment.querySelector(".App_RE_segment_number")
		segment_number.innerHTML=i
		i++
	})
}

function RE_open_C_lines_visualization(bttn){
	var bttn = document.querySelector('.App_RE_C_show_visualization_options_button')
	var box = bttn.parentNode.querySelector('.App_RE_C_show_visualization_options_box')	
	var boxOpp = document.querySelector('.App_RE_S_show_visualization_options_box')
	var bttnOpp= boxOpp.parentNode.querySelector('.App_RE_S_show_visualization_options_button')
	box.style.display='block'
	bttn.style.position='relative'
}

function RE_close_C_lines_visualization(bttn){
	var bttn = document.querySelector('.App_RE_C_show_visualization_options_button')
	var box = bttn.parentNode.querySelector('.App_RE_C_show_visualization_options_box')
	var boxOpp = document.querySelector('.App_RE_S_show_visualization_options_box')
	var bttnOpp= boxOpp.parentNode.querySelector('.App_RE_S_show_visualization_options_button')
	box.style.display='none'
	bttn.style.position='static'
}

function RE_open_S_lines_visualization(bttn){
	var bttn = document.querySelector('.App_RE_S_show_visualization_options_button')
	var box = bttn.parentNode.querySelector('.App_RE_S_show_visualization_options_box')
	var boxOpp = document.querySelector('.App_RE_S_show_visualization_options_box')
	var bttnOpp= boxOpp.parentNode.querySelector('.App_RE_S_show_visualization_options_button')
	box.style.display='block'
	bttn.style.position='relative'
}

function RE_close_S_lines_visualization(bttn){
	var bttn = document.querySelector('.App_RE_S_show_visualization_options_button')
	var box = bttn.parentNode.querySelector('.App_RE_S_show_visualization_options_box')
	var boxOpp = document.querySelector('.App_RE_S_show_visualization_options_box')
	var bttnOpp= boxOpp.parentNode.querySelector('.App_RE_S_show_visualization_options_button')
	box.style.display='none'
	bttn.style.position='static'
}
