function BNAV_change_ES_tab(){
	var editor_button = document.getElementById('App_BNav_mod_S_editor')
	var idea_button = document.getElementById('App_BNav_mod_S_idea')
	var user_button = document.getElementById('App_BNav_mod_S_user')
	var global_button = document.getElementById('App_BNav_mod_S_global')
	var editor_content = document.getElementById('App_BNav_mod_S_content_editor')
	var idea_content = document.getElementById('App_BNav_mod_S_content_idea')
	var user_content = document.getElementById('App_BNav_mod_S_content_user')
	var global_content = document.getElementById('App_BNav_mod_S_content_global')

	editor_content.style["display"] = "none"
	idea_content.style["display"] = "none"
	user_content.style["display"] = "none"
	global_content.style["display"] = "none"

	var circle_data = {acronym:"",starting_note:0,rotation:0,duration:""}
	var add_scale_to_scale_sequence_button = document.querySelector(".App_BNav_mod_S_controller_add > button")

	if(editor_button.checked){
		editor_content.style["display"] = "flex"
		circle_data = {acronym:editor_scale.acronym,starting_note:editor_scale.starting_note,rotation:0,duration:""}
		//add scale button
		add_scale_to_scale_sequence_button.style["display"] = "none"
		BNAV_check_buttons_save_editor_scale()
	} else if (idea_button.checked){
		idea_content.style["display"] = "flex"
		var container = idea_content.querySelector(".App_BNav_mod_S_scale_list")
		var selected = container.querySelector('input:checked') //XXX verify
		if (selected!=null){
			var scale_data = JSON.parse(selected.closest(".App_BNav_mod_S_scale_radio").value)
			if(scale_data.TET==TET){
				circle_data = {acronym:scale_data.letter,starting_note:0,rotation:0,duration:""}
			}
			BNAV_check_buttons_save_body_top(scale_data)
		}
		//add scale button
		add_scale_to_scale_sequence_button.style["display"] = "block"
	} else if (user_button.checked){
		user_content.style["display"] = "flex"
		//add scale button
		add_scale_to_scale_sequence_button.style["display"] = "none"
	} else if (global_button.checked){
		global_content.style["display"] = "flex"
		var container = global_content.querySelector(".App_BNav_mod_S_scale_list")
		var selected = container.querySelector('input:checked') //XXX verify
		if (selected!=null){
			var scale_data = JSON.parse(selected.closest(".App_BNav_mod_S_scale_radio").value)
			BNAV_check_buttons_save_body_top(scale_data)
		}
		//add scale button
		add_scale_to_scale_sequence_button.style["display"] = "none"
	}
	BNAV_scale_circle_change_input_values(circle_data)
}

function BNAV_refresh_PsLi(){
	var scales_length = document.getElementById('ES_scales_sum')
	//database
	var scale_sequence = DATA_get_scale_sequence()
	//var last_compas= compas_data.pop()
	var sum = scale_sequence.reduce((prop,item)=>{
		return parseInt(item.duration)+prop
	},0)
	scales_length.innerHTML = sum

	var compasLiRep = document.getElementById('ES_idea_length')
	compasLiRep.innerText = Li
}

function BNAV_save_new_scale_data(){//XXX
	//read database
	var data = DATA_get_current_state_point(true)

	data.scale=BNAV_read_current_scale_data()//XXX

	//save new database
	DATA_insert_new_state_point(data)

	//redraw/re align S lines
	DATA_load_state_point_data(false,true) //is this  necessary??? MOMENTANEAMENTEEEE
}

function BNAV_read_current_scale_data(){
	//current situation
	var scale_sequence = BNAV_read_current_scale_sequence()
	var scale = JSON.parse(JSON.stringify({idea_scale_list: idea_scale_list, scale_sequence: scale_sequence}))
	return scale
}

function BNAV_read_current_scale_sequence(){
	var parent = document.querySelector('.App_BNav_mod_S_top_sequence')
	var scale_list_elements = [...parent.querySelectorAll(".App_BNav_mod_S_sequence_item")]
	var scale_sequence = scale_list_elements.map(item=>{
		//console.log(item.value)
		return data = JSON.parse(item.value)
	})
	return scale_sequence
}

/*CIRCLE WRITE VALUES*/
function BNAV_scale_circle_change_input_values(data){
	document.getElementById('App_BNav_mod_S_scale_duration').value = data.duration
	document.getElementById('App_BNav_mod_S_scale_acronym').value = data.acronym
	document.getElementById('App_BNav_mod_S_scale_starting_note').value = data.starting_note
	document.getElementById('App_BNav_mod_S_scale_rot').value = data.rotation
	//reset eventually iS_line
	BNAV_refresh_iS_after_edited_scale_rotation()
}

/*CIRCLE READ VALUES*/
function BNAV_scale_circle_read_input_values(){
	var acronym = document.getElementById('App_BNav_mod_S_scale_acronym').value
	//starting note
	var string = document.getElementById('App_BNav_mod_S_scale_starting_note').value
	if(string==""){
		document.getElementById('App_BNav_mod_S_scale_starting_note').value=0
	}
	var starting_note = parseInt(document.getElementById('App_BNav_mod_S_scale_starting_note').value)
	//rot
	string = document.getElementById('App_BNav_mod_S_scale_rot').value
	if(string==""){
		document.getElementById('App_BNav_mod_S_scale_rot').value=0
	}
	var rotation = parseInt(document.getElementById('App_BNav_mod_S_scale_rot').value)

	//duration
	string = document.getElementById('App_BNav_mod_S_scale_duration').value
	var duration = ""
	if(string!=""){
		duration = parseInt(document.getElementById('App_BNav_mod_S_scale_duration').value)
	}

	return {acronym:acronym,starting_note:starting_note,rotation:rotation,duration:duration}
}

//edition of circle elements
function BNAV_scale_circle_edit_duration(element){
	if(parseInt(element.value)==0)element.value=""
	//this does not matter at all!!!
	editor_scale.duration=parseInt(element.value)
}

function BNAV_scale_circle_edit_starting_note(element){
	if(element.value == previous_value) return
	var circle_data = BNAV_scale_circle_read_input_values()
	var [value,inRange] = DATA_calculate_inRange(circle_data.starting_note,0,TET-1)
	//if(value!=circle_data.starting_note){
	if(!inRange){
		circle_data.starting_note=value
		BNAV_scale_circle_change_input_values(circle_data)
	}

	//if editor opened
	if(document.getElementById('App_BNav_mod_S_editor').checked){
		editor_scale.starting_note=circle_data.starting_note
		BNAV_refresh_editor_scale_table()
		return
	}
	//if others NOTHING
}

function BNAV_refresh_iS_after_edited_scale_rotation(){
	var circle_data = BNAV_scale_circle_read_input_values()

	//if editor
	if(document.getElementById('App_BNav_mod_S_editor').checked){
		editor_scale.rotation=circle_data.rotation
		BNAV_refresh_editor_scale_table()
		return
	}

	//if others
	if(document.getElementById('App_BNav_mod_S_idea').checked){
		var iS_line = document.getElementById('App_BNav_iS_idea_scale')
		var container = document.querySelector('#App_BNav_mod_S_content_idea').querySelector(".App_BNav_mod_S_scale_list")
		var [iS_list,scale_TET] = Find_selected_scale_and_rotate(container)
		BNAV_refresh_iS_line(iS_line,iS_list,scale_TET)
		return
	}
	if(document.getElementById('App_BNav_mod_S_user').checked){
		var iS_line = document.getElementById('App_BNav_iS_user_scale')
		var container = document.querySelector('#App_BNav_mod_S_content_user').querySelector(".App_BNav_mod_S_scale_list")
		var [iS_list,scale_TET] = Find_selected_scale_and_rotate(container)
		BNAV_refresh_iS_line(iS_line,iS_list,scale_TET)
		return
	}
	if(document.getElementById('App_BNav_mod_S_global').checked){
		var iS_line = document.getElementById('App_BNav_iS_global_scale')
		var container = document.querySelector('#App_BNav_mod_S_content_global').querySelector(".App_BNav_mod_S_scale_list")
		var [iS_list,scale_TET] = Find_selected_scale_and_rotate(container)
		BNAV_refresh_iS_line(iS_line,iS_list,scale_TET)
		return
	}

	//find current selected scale , extract iS list and rotate it
	function Find_selected_scale_and_rotate(container){
		var selected = container.querySelector('.App_BNav_mod_S_scale_label_checked')
		if (selected!=null){
			var scale_data = JSON.parse(selected.closest(".App_BNav_mod_S_scale_radio").value)

			//a COPY, not a pointer
			var iS_list = JSON.parse(JSON.stringify(scale_data.iS))
			//apply rotation (first become last)
			for (var i = 0 ; i<circle_data.rotation; i++){
				iS_list.push(iS_list.shift())
			}
			return [iS_list,scale_data.TET]
		}else{
			return [null,null]
		}
	}
}

/*EDITOR MANAGEMENT*/

function BNAV_add_scale_to_scale_sequence(){
	//verify if all the values are written
	var data = BNAV_scale_circle_read_input_values()
	if(data.duration == ""){
		//INDICATE SOMETHING IS MISSING XXX
		//APP_blink_error
		// console.log("0")
		return
	} else
	if(data.acronym == ""){
		//INDICATE SOMETHING IS MISSING XXX
		//APP_blink_error
		// console.log("1")
		return
	} else if(data.starting_note ===''){
		//INDICATE SOMETHING IS MISSING XXX
		//APP_blink_error
		// console.log("2")
		return
	} else if(data.rotation ===''){
		//INDICATE SOMETHING IS MISSING XXX
		//APP_blink_error
		// console.log("3")
		return
	}
	//identify what sequence is selected
	var scale_sequence_container = document.querySelector(".App_BNav_mod_S_top_sequence")
	var button_selected = scale_sequence_container.querySelector(".checked")
	//no radio

	//var circle_data = {acronym:acronym,starting_note:0,rotation:0,duration:""}
	if(button_selected){
		button_selected.classList.remove('checked')
		//create the object, select it and place in position
		var div = BNAV_generate_scale_sequence_div(data)
		div.classList.add('checked')
		scale_sequence_container.insertBefore(div,button_selected.nextSibling)
	}else{
		//create the object, select it and place last
		var div = BNAV_generate_scale_sequence_div(data)
		div.classList.add('checked')
		scale_sequence_container.appendChild(div)
	}
	BNAV_save_new_scale_data()
}

function BNAV_generate_scale_sequence_div(data){
	//creation of the div
	var sequence_item = document.createElement('div')
	sequence_item.classList.add('App_BNav_mod_S_sequence_item')
	sequence_item.setAttribute('onclick', 'BNAV_select_scale_sequence_button(this,true)')
	var data_string = JSON.stringify(data)
	sequence_item.value= data_string
	var delete_button = document.createElement('button')
	delete_button.classList.add('App_BNav_mod_S_scale_radio_button_mini')
	delete_button.setAttribute('onclick','BNAV_delete_scale_sequence_button(this)')
	delete_button.innerHTML= '<img class="App_BNav_content_icon" src="./Icons/delete.svg">'
	var info_div = document.createElement('div')

	var circle_div = document.createElement('div')

	var acronym_p = document.createElement('p')
	var starting_input = document.createElement('input')
	var circle_p = document.createElement('p')
	var rot_input = document.createElement('input')
	var duration_input = document.createElement('input')
	acronym_p.classList.add('App_BNav_mod_S_sequence_item_acronym')
	acronym_p.innerHTML= `${data.acronym}`
	starting_input.classList.add('App_BNav_mod_S_sequence_item_starting_note')
	starting_input.value= `${data.starting_note}`
	circle_p.innerHTML= "."
	rot_input.classList.add('App_BNav_mod_S_sequence_item_rot')
	rot_input.value= `${data.rotation}`

	duration_input.classList.add('App_BNav_mod_S_sequence_item_duration')
	duration_input.value=data.duration
	duration_input.maxLength = 3;

	sequence_item.appendChild(delete_button)
	sequence_item.appendChild(info_div)
	info_div.appendChild(circle_div)
	info_div.appendChild(duration_input)
	circle_div.appendChild(acronym_p)
	circle_div.appendChild(starting_input)
	circle_div.appendChild(circle_p)
	circle_div.appendChild(rot_input)
	//durationDiv.appendChild(durationP)

	starting_input.maxLength=1
	//starting_input.readOnly="true"
	rot_input.maxLength=1
	//rot_input.readOnly="true"

	//durationInput.addEventListener("keypress", APP_onlyIntegers_pos(event,this))
	duration_input.addEventListener('change', function(){
		BNAV_edit_scale_duration_input(duration_input)
	})
	starting_input.addEventListener('change', function(){
		BNAV_edit_scale_starting_input(starting_input)
	})
	rot_input.addEventListener('change', function(){
		BNAV_edit_scale_rotation_input(rot_input)
	})

	duration_input.addEventListener('focusin', function(){
		APP_set_previous_value(duration_input)
	})
	starting_input.addEventListener('focusin', function(){
		APP_set_previous_value(starting_input)
	})
	rot_input.addEventListener('focusin', function(){
		APP_set_previous_value(rot_input)
	})

	duration_input.addEventListener("keypress", function(){
		APP_onlyIntegers_pos(event,duration_input)
	})
	starting_input.addEventListener("keypress", function(){
		APP_onlyIntegers_pos(event,starting_input)
	})
	rot_input.addEventListener("keypress", function(){
		APP_onlyIntegers_pos(event,rot_input)
	})

	sequence_item.setAttribute("draggable", "true")

	sequence_item.addEventListener('dragstart', (e) => {
		sequence_item.classList.add('dragging')
		sequence_item.style.border="5px solid var(--Nuzic_light)"
		var canvas = document.createElement('canvas')
		var context = canvas. getContext('2d')
		context. clearRect(0, 0, canvas. width, canvas. height)
		e.dataTransfer.setDragImage(canvas, 10, 10)
	})
	sequence_item.addEventListener('dragend', () => {
		sequence_item.classList.remove('dragging')
		sequence_item.style.border=""
		BNAV_save_new_scale_data()
	})

	var scale_sequence_container = document.querySelector(".App_BNav_mod_S_top_sequence")
	sequence_item.addEventListener('dragover', e => {
		e.preventDefault()
		var after_element = BNAV_get_scale_drag_after_element(scale_sequence_container, e.clientX)
		var dragover_scale_sequence_div = scale_sequence_container.querySelector('.dragging')
		if (after_element == null){
			scale_sequence_container.appendChild(dragover_scale_sequence_div)
		} else {
			//parent.insertBefore(input, after_element.previousSibling)
			scale_sequence_container.insertBefore(dragover_scale_sequence_div, after_element)
		}
	})
	return sequence_item
}

function BNAV_get_scale_drag_after_element (container, x) {
	var draggableElements = [...container.querySelectorAll('.App_BNav_mod_S_sequence_item:not(.dragging)')]

	return draggableElements.reduce((closest, child) => {
		var box = child.getBoundingClientRect()
		var offset = x -box.left - box.width / 2
		if(offset < 0 && offset > closest.offset){
			return { offset: offset, element: child }
		} else {
			return closest
		}
	},{offset: Number.NEGATIVE_INFINITY}).element
}

function BNAV_save_edited_scale_as_new_user_scale(){
	//save editor scale to user scale list
	var scale_to_save = JSON.parse(JSON.stringify(editor_scale.scale))

	//verify if this scale does not already exist
	var already_exist = user_scale_list.scale_list.some(item=>{
		return scale_to_save.name===item.name && scale_to_save.TET==item.TET
	})

	//compare arrays to detect if there is a change
	if(already_exist){
		//alert
		if(BNAV_open_alert_overwrite_scale("user",scale_to_save.name)){
			//modify an existing scale
			scale_to_save.letter=""
			//find scale to overwrite
			var target = user_scale_list.scale_list.find(item=>{
				return scale_to_save.name===item.name && scale_to_save.TET==item.TET
			})
			target.module=scale_to_save.module
			target.iS=scale_to_save.iS
			//verify tet list
			BNAV_generate_TET_list_from_scale_list(user_scale_list)
			//save new user file
			DATA_save_user_scale_list()
			BNAV_refresh_user_scale_tab()
			BNAV_check_buttons_save_editor_scale()
		}
	}else{
		//console.log("scale added")
		scale_to_save.letter=""
		//clear tags
		scale_to_save.tags = []
		user_scale_list.scale_list.push(scale_to_save)
		//verify tet list
		BNAV_generate_TET_list_from_scale_list(user_scale_list)
		//save new user file
		DATA_save_user_scale_list()
		BNAV_refresh_user_scale_tab()
		BNAV_check_buttons_save_editor_scale()
	}
}

function BNAV_save_edited_scale_as_new_idea_scale(){
	//save editor scale to idea scale list
	//search idea list if already exist said name
	var same_name = false
	var same_letter = false

	idea_scale_list.scale_list.forEach(item=>{
		if(item.TET==TET){
			if(item.name==editor_scale.scale.name){
				same_name = true
			}
			if(item.letter==editor_scale.acronym){
				same_letter = true
			}
		}
	})

	if(same_name){
		//alert
		//try to override so use this function
		BNAV_override_edited_scale_idea_name()
	}else{
		var new_scale = JSON.parse(JSON.stringify(editor_scale.scale))
		if(same_letter){
			new_scale.letter=""
			editor_scale.acronym=""
			BNAV_refresh_editor_scale_tab()
		}else{
			new_scale.letter = editor_scale.acronym
		}

		idea_scale_list.scale_list.push(new_scale)///IDEA_SCALE_LIST
		BNAV_generate_TET_list_from_scale_list(idea_scale_list)
		BNAV_refresh_idea_scale_tab()
		//minimap
		var iS_list = JSON.parse(JSON.stringify(editor_scale.scale.iS))
		BNAV_scale_editor_iS_miniDiv(iS_list)
		BNAV_save_new_scale_data()
	}
}

/*/override scale in idea that match same acronym OBSOLETE FROM EDITOR
function Override_edited_scale_idea_acronym(){
	var index=-1

	idea_scale_list.scale_list.forEach(item=>{
		index++
		if(item.TET==TET){
			if(item.letter==editor_scale.scale.letter && item.letter!=""){
				//need to change this one
				//wait confirmation
				if(BNAV_open_alert_overwrite_scale("idea",item.name)){
					editor_scale.scale.letter=editor_scale.acronym //in case letter changed
					var new_scale = JSON.parse(JSON.stringify(editor_scale.scale))
					//if i have a rotation change the iS_list
					var rotation = editor_scale.rotation

					if(rotation !=0){
						//copy no pointer
						var new_iS_list = JSON.parse(JSON.stringify(new_scale.iS))
						//apply rotation (first become last)
						for (var i = 0 ; i<rotation; i++){
							new_iS_list.push(new_iS_list.shift())
						}
						editor_scale.rotation=0
						//copy

						editor_scale.scale.iS = JSON.parse(JSON.stringify(new_iS_list))
						new_scale.iS = JSON.parse(JSON.stringify(new_iS_list))
					}

					idea_scale_list.scale_list[index]=new_scale
					BNAV_generate_TET_list_from_scale_list(idea_scale_list)
					BNAV_refresh_idea_scale_tab()
					//editor tab & minimap
					BNAV_refresh_editor_scale_tab()
					var iS_list = JSON.parse(JSON.stringify(editor_scale.scale.iS))
					BNAV_scale_editor_iS_miniDiv(iS_list)
					BNAV_save_new_scale_data()
					return
				}else{
					return
				}
			}
		}
	})
}
*/

//override scale in idea that match same name
function BNAV_override_edited_scale_idea_name(){
	var index=-1

	idea_scale_list.scale_list.forEach(item=>{
		index++
		if(item.TET==TET){
			if(item.name==editor_scale.scale.name){
				//need to change this one
				//wait confirmation
				if(BNAV_open_alert_overwrite_scale("idea",editor_scale.scale.name)){
					//use idea scale letter
					editor_scale.scale.letter=item.letter
					editor_scale.acronym=item.letter
					var new_scale = JSON.parse(JSON.stringify(editor_scale.scale))
					idea_scale_list.scale_list[index]=new_scale
					BNAV_generate_TET_list_from_scale_list(idea_scale_list)
					BNAV_refresh_idea_scale_tab()
					//minimap
					var iS_list = JSON.parse(JSON.stringify(editor_scale.scale.iS))
					BNAV_scale_editor_iS_miniDiv(iS_list)
					BNAV_refresh_editor_scale_tab()
					BNAV_save_new_scale_data()
					return
				}else{
					return
				}
			}
		}
	})
}

function BNAV_reset_scale_basic_values(){
	//if letter not founc change only rot and strting note
	var original_scale=false

	var there_it_is = idea_scale_list.scale_list.some(item=>{
		if(item.TET==TET){
			if(item.letter==editor_scale.acronym){
				//a COPY, not a pointer
				original_scale = JSON.parse(JSON.stringify(item))
				return true
			}
		}
	})
	if(there_it_is){
		editor_scale.scale=original_scale
	}
	editor_scale.rotation = 0
	editor_scale.duration = 0//not ""
	editor_scale.starting_note = 0

	BNAV_refresh_editor_scale_tab()
}

function BNAV_clear_edited_scale(){
	// editor_scale.rotation = 0
	// editor_scale.starting_note = 0
	// editor_scale.duration = ""
	// editor_scale.acronym = ""
	// editor_scale.scale.iS = [TET]
	// editor_scale.scale.module = 1
	// editor_scale.scale.tags = []
	// editor_scale.scale.letter = ""
	// editor_scale.scale.name = ""
	editor_scale = false

	BNAV_refresh_editor_scale_tab()
	var iS_list = JSON.parse(JSON.stringify([TET]))
	BNAV_scale_editor_iS_miniDiv(iS_list)
}

function BNAV_delete_scale_sequence_button(button){
	//var container = bttn.parentNode.parentNode
	var parent = button.parentNode
	parent.innerHTML = ""
	parent.remove(parent)

	BNAV_save_new_scale_data()
}

function BNAV_editor_scale_select_note(button){
	if (editor_scale.rotation != 0 || editor_scale.starting_note != 0){
		editor_scale.rotation = 0
		editor_scale.starting_note = 0
		var circle_data = {acronym:editor_scale.acronym,starting_note:editor_scale.starting_note,rotation:editor_scale.rotation,duration:editor_scale.duration}
		BNAV_scale_circle_change_input_values(circle_data)
	} else {
		if (button.classList.contains('pressed')) {
			button.classList.remove('pressed')
		} else {
			button.classList.add('pressed')
			//play note
			BNAV_play_scale_note_on_click(button)
		}

		//calcolate new iS_list
		var note_line = document.getElementById("App_BNav_mod_S_accents")
		var button_list = [...note_line.querySelectorAll("button")]
		var i = 0
		var new_iS_list = []
		button_list.forEach(item=>{
			if (item.classList.contains('pressed')) {
				new_iS_list.push(i)
				i=1
			} else {
				i++
			}
		})
		//eliminate first iS = 0
		new_iS_list.shift()
		//add last interval
		new_iS_list.push(TET-new_iS_list.reduce((prop,item)=>{return item+prop},0))
		editor_scale.scale.iS=new_iS_list
		editor_scale.scale.module=new_iS_list.length
		BNAV_refresh_editor_scale_tab()
	}
	BNAV_check_buttons_save_editor_scale()
}

function BNAV_change_iS_color(iS,current_TET=0){
	//change scale with TET
	if(current_TET==0){
		console.error("TET not passed , not possible to calculate color palette")
		return
	}

	//creating array
	var color_base_array = [{color:[231, 111, 104],position:1},
							{color:[242, 138, 173],position:2},
							{color:[255, 187, 51],position:3},
							{color:[124, 214, 179],position:4},
							{color:[123, 180, 205],position:5}
	]

	//modify array base TET
	var D_iS = Math.floor(current_TET/(2*5))
	if(D_iS>0){
		var position = 1
		for (var i = 0 ; i<5 ; i++){
			color_base_array[i].position=position
			position+=D_iS
		}
	}

	//add last item WHITE???
	color_base_array.push({color:[255, 255, 255],position:current_TET})
	var index=-1
	var next_color = color_base_array.find(color=>{
		index++
		return color.position>=iS
	})

	if(next_color.position==iS){
		//is a base color
		return (background = `rgb(${next_color.color[0]},${next_color.color[1]},${next_color.color[2]})`)
	}else{
		var prev_color=color_base_array[index-1]
		var a = prev_color.color[0]+Math.floor(Math.abs(next_color.color[0]-prev_color.color[0])/(next_color.position-prev_color.position))*(iS-prev_color.position)
		var b = prev_color.color[1]+Math.floor(Math.abs(next_color.color[1]-prev_color.color[1])/(next_color.position-prev_color.position))*(iS-prev_color.position)
		var c = prev_color.color[2]+Math.floor(Math.abs(next_color.color[2]-prev_color.color[2])/(next_color.position-prev_color.position))*(iS-prev_color.position)
		return (background = `rgb(${a},${b},${c})`)
	}
}

function BNAV_write_scale_values(scale){///XXX/// KINDA OK??
	idea_scale_list = scale.idea_scale_list
	var scale_sequence = scale.scale_sequence

	//change scale TET displayed
	var scale_tab = document.getElementById("App_BNav_mod_S_content_idea")
	var TET_inp_box = scale_tab.querySelector(".App_BNav_mod_S_TET_inp")
	TET_inp_box.value = TET
	//write scale list on the tab
	BNAV_refresh_idea_scale_tab()
	editor_scale = false //is that ok XXX ????
	//if undo this is BAD !!! XXX
	//NONONONONONONONONONONONONONONONONONONONONONONONONONONONONONONONO
	BNAV_refresh_editor_scale_tab()
	BNAV_write_scale_sequence(scale_sequence)
	BNAV_refresh_PsLi()
}

function BNAV_refresh_global_scale_tab(){
	//reload all the items on the global scale tab from the global_scale_list
	//find the scale TET displayed
	var global_scale_tab = document.getElementById("App_BNav_mod_S_content_global")
	var TET_inp_box = global_scale_tab.querySelector(".App_BNav_mod_S_TET_inp")
	var current_TET = parseInt(TET_inp_box.value)
	//find all the scale that match said TET

	var button_active = (current_TET==TET)

	var current_TET_scale_list = global_scale_list.scale_list.filter(item=>{
		return item.TET == current_TET
	})
	//show this sublist
	var div_container = global_scale_tab.querySelector(".App_BNav_mod_S_scale_list")
	div_container.innerHTML = ""
	var index=0

	//compas_checked.value = JSON.stringify(compas_value)

	current_TET_scale_list.forEach(item=>{
		var scale = document.createElement("div")
		var item_string = JSON.stringify(item)
		var starred = ""

		if(global_scale_list_bookmarks.some(bookmark=>{return item.name===bookmark.name && item.TET===bookmark.TET})){
			starred="starred"
		}

		scale.classList.add("App_BNav_mod_S_scale_radio")
		scale.value = item_string
		scale.innerHTML = 	`<input onclick="BNAV_select_scale(this)" name="App_BNav_mod_S_global_radio" type="radio" id="App_BNav_mod_S_global_radio`+index+`"></input>
							<label for="App_BNav_mod_S_global_radio`+index+`">
								<p>`+item.name+`</p>
								<div>
									<button class="App_BNav_mod_S_scale_radio_button_mini `+starred+`" onclick="BNAV_bookmark_scale_button(this,'global')"><img src="./Icons/star.svg"></button>
									<button class="App_BNav_mod_S_scale_radio_button" onclick="BNAV_use_unuse_scale_button(this)"><img src="./Icons/add.svg"></img><img src="./Icons/selected.svg" style="display:none"></img></button>
									<button class="App_BNav_mod_S_scale_radio_button_mini" onclick="BNAV_play_scale_button(this)"><img src="./Icons/play_mini.svg"></img></button>
								</div>
							</label>`
		BNAV_scale_iS_miniDiv(scale)
		div_container.appendChild(scale)
		index++
	})

	if(button_active){
		//enable edit_selected_scale_button_ID XXX

		//change some background ?? XXX
	}else{
		//disable edit_selected_scale_button_ID XXX

		//change some background ?? XXX
	}

	BNAV_control_used_scale_icons()
	BNAV_order_scale_bookmark_list()
}

function BNAV_refresh_user_scale_tab(){///XXX///
	//reload all the items on the user scale tab from the user_scale_list
	//find the scale TET displayed
	var user_scale_tab = document.getElementById("App_BNav_mod_S_content_user")
	var TET_inp_box = user_scale_tab.querySelector(".App_BNav_mod_S_TET_inp")
	var current_TET = parseInt(TET_inp_box.value)
	//find all the scale that match said TET

	var button_active = (current_TET==TET)

	var current_TET_scale_list = user_scale_list.scale_list.filter(item=>{
		return item.TET == current_TET
	})
	//show this sublist
	var div_container = user_scale_tab.querySelector(".App_BNav_mod_S_scale_list")
	div_container.innerHTML = ""

	if(current_TET_scale_list.length>0){
		var index=0
		current_TET_scale_list.forEach(item=>{
			var scale = document.createElement("div")
			var item_string = JSON.stringify(item)
			var starred = ""
			if(item.tags.includes("bookmark")){
				starred="starred"
			}

			scale.classList.add("App_BNav_mod_S_scale_radio")
			scale.value = item_string
			scale.innerHTML = 	`<input onclick="BNAV_select_scale(this)" name="App_BNav_mod_S_user_radio" type="radio" id="App_BNav_mod_S_user_radio`+index+`"></input>
								<label for="App_BNav_mod_S_user_radio`+index+`">
									<p>`+item.name+`</p>
									<div>
										<button class="App_BNav_mod_S_scale_radio_button_mini `+starred+`" onclick="BNAV_bookmark_scale_button(this,'user')"><img src="./Icons/star.svg"></button>
										<button class="App_BNav_mod_S_scale_radio_button" onclick="BNAV_use_unuse_scale_button(this)"><img src="./Icons/add.svg"></img><img src="./Icons/selected.svg" style="display:none"></img></button>
										<button class="App_BNav_mod_S_scale_radio_button_mini" onclick="BNAV_play_scale_button(this)"><img src="./Icons/play_mini.svg"></img></button>
									</div>
								</label>`


			BNAV_scale_iS_miniDiv(scale)
			div_container.appendChild(scale)
			index++
		})
	}else{
		div_container.innerHTML = `<div class="App_BNav_mod_S_scale_radio_placeholder">Add scales from COMMUNITY, scales used on the current IDEA or create and save a new one on the scale EDITOR
			</div>`
	}

	if(button_active){
		//enable edit_selected_scale_button_ID XXX

		//change some background ?? XXX
	}else{
		//disable edit_selected_scale_button_ID XXX

		//change some background ?? XXX
	}

	BNAV_control_used_scale_icons()
	BNAV_order_scale_bookmark_list()
}

function BNAV_refresh_idea_scale_tab(){
	//find the scale TET displayed
	var idea_scale_tab = document.getElementById("App_BNav_mod_S_content_idea")
	var TET_inp_box = idea_scale_tab.querySelector(".App_BNav_mod_S_TET_inp")
	var current_TET = parseInt(TET_inp_box.value)

	var button_active = (current_TET==TET)
	//find all the scale that match said TET

	var current_TET_scale_list = idea_scale_list.scale_list.filter(item=>{
		return item.TET == current_TET
	})
	//show this sublist
	var div_container = idea_scale_tab.querySelector(".App_BNav_mod_S_scale_list")
	div_container.innerHTML = ""

	if(current_TET_scale_list.length>0){
		var index=0
		current_TET_scale_list.forEach(item=>{
			var scale = document.createElement("div")
			var item_string = JSON.stringify(item)

			scale.classList.add("App_BNav_mod_S_scale_radio")
			scale.value = item_string
			scale.innerHTML = 	`<input onclick="BNAV_select_scale(this)" name="App_BNav_mod_S_idea_radio" type="radio" id="App_BNav_mod_S_idea_radio`+index+`"></input>
								<label for="App_BNav_mod_S_idea_radio`+index+`">
									<p>`+item.name+`</p>
									<div>
										<button class="App_BNav_mod_S_scale_radio_button_mini selected"><img src="./Icons/selected.svg"></button>
										<input class="App_BNav_mod_S_scale_radio_button" onfocusin="APP_set_previous_value(this),BNAV_select_scale_form_input(this)" onkeypress="APP_onlyLetters(event,this)" onfocusout="BNAV_assign_acronym_idea_scale_input(this)" value='`+item.letter+`' maxlength="1" placeholder="-"></input>
										<button class="App_BNav_mod_S_scale_radio_button_mini" onclick="BNAV_play_scale_button(this)"><img src="./Icons/play_mini.svg"></img></button>
									</div>
								</label>`
			BNAV_scale_iS_miniDiv(scale)
			div_container.appendChild(scale)
			index++
		})
	}else{
		div_container.innerHTML = `<div class="App_BNav_mod_S_scale_radio_placeholder">Add scales from COMMUNITY, scales saved on the USER profile or create and use a new one on the scale EDITOR
			</div>`
	}


	if(button_active){
		//enable edit_selected_scale_button_ID XXX

		//change some background ?? XXX
	}else{
		//disable edit_selected_scale_button_ID XXX

		//change some background ?? XXX
	}

	BNAV_control_used_scale_icons()
}

function BNAV_refresh_editor_scale_tab(){
	//if no data is entered, set empty scale named scale1 or similia
	if(editor_scale==false){
		//Populate with single iS
		editor_scale={scale:
				{
					TET:TET,
					module:1,
					name:"",
					iS:[TET],
					tags:[],
					info:""
				},
				acronym:"",
				rotation: 0,
				starting_note:0,
				duration: ""
			}
	}

	//name
	if(editor_scale.scale.name==""){
		//if no name, find one suitable XXX
		editor_scale.scale.name = BNAV_new_scale_name()
	}
	document.querySelector(".App_BNav_mod_S_content_header_inp_name").value=editor_scale.scale.name

	BNAV_scale_circle_change_input_values(editor_scale)

	//acronym
	document.querySelector(".App_BNav_mod_S_content_header_acronym").querySelector("input").value=editor_scale.acronym

	//Editor table
	BNAV_refresh_editor_scale_table()
	BNAV_check_buttons_save_editor_scale()
}

/* create accents */
function BNAV_refresh_editor_scale_table(){
	//write every values
	var accent_line = document.getElementById('App_BNav_mod_S_accents')
	var grade_line = document.getElementById('App_BNav_mod_S_gS')
	var iS_line= document.getElementById('App_BNav_mod_S_iS')

	var last_accent = document.createElement('button')
	last_accent.innerHTML = '0'
	last_accent.setAttribute('onclick','BNAV_play_scale_note_on_click(this)')
	var last_accent_note = TET+editor_scale.starting_note
	last_accent.value = last_accent_note


	var last_grade = document.createElement('div')
	last_grade.innerHTML = '0'

	//define values arrays
	//a COPY, not a pointer
	var iS_list = JSON.parse(JSON.stringify(editor_scale.scale.iS))

	//apply rotation (first become last)
	for (var i = 0 ; i<editor_scale.rotation; i++){
		iS_list.push(iS_list.shift())
	}

	//calculate note
	var current_note = editor_scale.starting_note
	var selected_note_list = iS_list.map(iS=>{
		current_note+=iS
		return current_note
	})
	//add first element
	selected_note_list.unshift(editor_scale.starting_note)

	//cleaning the lines
	accent_line.innerHTML = ''
	grade_line.innerHTML = ''

	var grade_number=0
	for (i=0;i<TET;i++){  //XXX if i want to edit different TET???
		//elements
		var note_button = document.createElement('button')
		//note_button.classList.add("ES_note_button")
		if(i==0){
			//note_button.setAttribute('disabled', 'true')
			note_button.setAttribute('onclick','BNAV_play_scale_note_on_click(this)')
		}else{
			note_button.setAttribute('onclick','BNAV_editor_scale_select_note(this)')
		}

		var grade_div = document.createElement('div')

		var note = i+editor_scale.starting_note
		if(note >= TET){
			note_button.innerHTML=note-TET
		} else{
			note_button.innerHTML=note
		}
		note_button.value = note

		if(selected_note_list.includes(note)) {
			note_button.classList.add('pressed')
			grade_div.innerHTML=grade_number
			grade_number++
		}


		accent_line.appendChild(note_button)
		grade_line.appendChild(grade_div)
	}

	last_accent.innerHTML = editor_scale.starting_note
	accent_line.appendChild(last_accent)
	grade_line.appendChild(last_grade)

	//building the iS line
	BNAV_refresh_iS_line(iS_line,iS_list,TET)
}

function BNAV_refresh_iS_line(iS_line,iS_list=null,current_TET=12){
	iS_line.innerHTML = ''
	if(iS_list==null){
		//clear
	}else{
		iS_list.forEach(iS=>{
			//create element
			var div = document.createElement('div')
				div.innerHTML=iS
				divWidth = 28 * iS
				div.style.width=` ${divWidth}px`
				BNAV_change_iS_color(iS,current_TET)
				div.style.backgroundColor = background
			iS_line.appendChild(div)
		})
	}
}

function BNAV_new_scale_name(){
	var new_name = "Escala "
	var index = 0
	//search idea list if already exist said name
	idea_scale_list.scale_list.forEach(item=>{
		if(item.TET==TET){
			if(item.name==new_name+index){
				index++
			}
		}
	})
	return new_name+index
}

function BNAV_write_scale_sequence(scale_sequence){
	var parent = document.querySelector('.App_BNav_mod_S_top_sequence')
	parent.innerHTML=""
	scale_sequence.forEach(item=>{
		var div = BNAV_generate_scale_sequence_div(item)
		//in last position
		parent.appendChild(div)
	})
}

function BNAV_control_used_scale_icons(){
	//create a list of all divs in global and user
	var global_scale_tab = document.getElementById("App_BNav_mod_S_content_global")
	var user_scale_tab = document.getElementById("App_BNav_mod_S_content_user")

	var global_scale_container = global_scale_tab.querySelector(".App_BNav_mod_S_scale_list")
	var user_scale_container = user_scale_tab.querySelector(".App_BNav_mod_S_scale_list")

	var scale_div_list = [...global_scale_container.querySelectorAll(".App_BNav_mod_S_scale_radio")].concat([...user_scale_container.querySelectorAll(".App_BNav_mod_S_scale_radio")])
	scale_div_list.forEach(div=>{
		var data = JSON.parse(div.value)
		var button = div.querySelector(".App_BNav_mod_S_scale_radio_button")
		button.classList.remove("selected")
		button.childNodes[0].style="display:flex"
		button.childNodes[1].style="display:none"
		var iS_string = JSON.stringify(data.iS)
		idea_scale_list.scale_list.forEach(item=>{
			//console.log(item.name+"==="+data.name+" && "+item.TET+"=="+data.TET+" && "+item.iS+"=="+data.iS)
			if(item.name===data.name && item.TET==data.TET && iS_string === JSON.stringify(item.iS)){
				//activate
				button.classList.add("selected")
				button.childNodes[0].style="display:none"
				button.childNodes[1].style="display:flex"
			}
		})
	})
}

function BNAV_scale_iS_miniDiv(scale){
	//function add miniature iS representation
	var scale_data = JSON.parse(scale.value)
	var data = scale_data.iS
	var dataSum = 0

	for(i=0; i<data.length;i++){
		dataSum += data[i]
	}

	var container = document.createElement('div')
	container.classList.add('App_BNav_mod_iS_miniDiv')
	scale.appendChild(container)

	for(i=0;i<data.length;i++){
		var div = document.createElement('div')
		div.style.height='100%'
		div.style.width = `calc(var(--RE_block3) * ${data[i]} / ${dataSum})`
		div.style.borderRight = "solid var(--Nuzic_white) 1px"
		//div.innerHTML= data[i]
		BNAV_change_iS_color(data[i],scale_data.TET)
		div.style.backgroundColor = background
		container.appendChild(div)
	}
}

function BNAV_scale_editor_iS_miniDiv(data){
	//function add miniature iS representation
	var editor_header = document.querySelector('#App_BNav_mod_S_content_editor > .App_BNav_mod_content_header')
	var container = editor_header.querySelector('.App_BNav_mod_iS_miniDiv')
	container.innerHTML=""
	var dataSum = 0

	for(i=0; i<data.length;i++){
		dataSum += data[i]
	}
	for(i=0;i<data.length;i++){
		var div = document.createElement('div')
		div.style.height='100%'
		var divWidth = 84*data[i]/dataSum
		div.style.width = `${divWidth}px`
		div.style.borderRight = "solid white 1px"
		//div.innerHTML= data[i]
		BNAV_change_iS_color(data[i],TET)
		div.style.backgroundColor = background
		container.appendChild(div)
	}
}

function BNAV_generate_TET_list_from_scale_list(data){
	var TET_list = [...new Set(data.scale_list.map((item)=>{
			return item.TET
		})
	)]
	//sort and record
	data.TET_list = TET_list.sort(function(a, b){return a-b})
}

function BNAV_next_TET_global_scale_list(){
	//find current TET listed
	var global_scale_tab = document.getElementById("App_BNav_mod_S_content_global")
	var TET_inp_box = global_scale_tab.querySelector(".App_BNav_mod_S_TET_inp")
	var current_TET = parseInt(TET_inp_box.value)

	//find current index in list
	var current_TET_index = global_scale_list.TET_list.indexOf(current_TET)
	if(current_TET_index+1<global_scale_list.TET_list.length){
		TET_inp_box.value = global_scale_list.TET_list[current_TET_index+1]
		//display new list
		BNAV_refresh_global_scale_tab()
	}
}

function BNAV_previous_TET_global_scale_list(){
	//find current TET listed
	var global_scale_tab = document.getElementById("App_BNav_mod_S_content_global")
	var TET_inp_box = global_scale_tab.querySelector(".App_BNav_mod_S_TET_inp")
	var current_TET = parseInt(TET_inp_box.value)

	//find current index in list

	var current_TET_index = global_scale_list.TET_list.indexOf(current_TET)
	if(current_TET_index-1>=0){
		TET_inp_box.value = global_scale_list.TET_list[current_TET_index-1]
		//display new list
		BNAV_refresh_global_scale_tab()
	}
}

function BNAV_next_TET_user_scale_list(){
	//find current TET listed
	var user_scale_tab = document.getElementById("App_BNav_mod_S_content_user")
	var TET_inp_box = user_scale_tab.querySelector(".App_BNav_mod_S_TET_inp")
	var current_TET = parseInt(TET_inp_box.value)

	//find current index in list
	var current_TET_index = user_scale_list.TET_list.indexOf(current_TET)
	if(current_TET_index+1<user_scale_list.TET_list.length){
		TET_inp_box.value = user_scale_list.TET_list[current_TET_index+1]
		//display new list
		BNAV_refresh_user_scale_tab()
	}
}

function BNAV_previous_TET_user_scale_list(){
	//find current TET listed
	var user_scale_tab = document.getElementById("App_BNav_mod_S_content_user")
	var TET_inp_box = user_scale_tab.querySelector(".App_BNav_mod_S_TET_inp")
	var current_TET = parseInt(TET_inp_box.value)

	//find current index in list

	var current_TET_index = user_scale_list.TET_list.indexOf(current_TET)
	if(current_TET_index-1>=0){
		TET_inp_box.value = user_scale_list.TET_list[current_TET_index-1]
		//display new list
		BNAV_refresh_user_scale_tab()
	}
}

function BNAV_next_TET_idea_scale_list(){
	//find current TET listed
	var idea_scale_tab = document.getElementById("App_BNav_mod_S_content_idea")
	var TET_inp_box = idea_scale_tab.querySelector(".App_BNav_mod_S_TET_inp")
	var current_TET = parseInt(TET_inp_box.value)

	//find current index in list
	var current_TET_index = idea_scale_list.TET_list.indexOf(current_TET)
	if(current_TET_index+1<idea_scale_list.TET_list.length){
		TET_inp_box.value = idea_scale_list.TET_list[current_TET_index+1]
		//display new list
		BNAV_refresh_idea_scale_tab()
	}
}

function BNAV_previous_TET_idea_scale_list(){
	//find current TET listed
	var idea_scale_tab = document.getElementById("App_BNav_mod_S_content_idea")
	var TET_inp_box = idea_scale_tab.querySelector(".App_BNav_mod_S_TET_inp")
	var current_TET = parseInt(TET_inp_box.value)

	//find current index in list
	var current_TET_index = idea_scale_list.TET_list.indexOf(current_TET)
	if(current_TET_index-1>=0){
		TET_inp_box.value = idea_scale_list.TET_list[current_TET_index-1]
		//display new list
		BNAV_refresh_idea_scale_tab()
	}
}

function BNAV_select_scale(element){
	BNAV_stop_scale_name_interval()
	//find all other elements and uncheck them
	var container = element.closest(".App_BNav_mod_S_scale_list")
	var label_list = [...container.querySelectorAll("label")]

	//find label
	var parent = element.parentNode
	var label = parent.querySelector("label")

	BNAV_autoscroll_scale_name(label)

	var scale_data = JSON.parse(element.closest(".App_BNav_mod_S_scale_radio").value)

	var acronym = ""
	if(scale_data.letter === undefined){
	//acronym = =""
	}else{
		//verify TET is compatible
		if(TET==scale_data.TET){
			acronym=scale_data.letter
		}
	}
	//change values in circle
	var circle_data = {acronym:acronym,starting_note:0,rotation:0,duration:""}
	BNAV_scale_circle_change_input_values(circle_data)

	//actualize iS line
	var iS_line
	if(document.getElementById('App_BNav_mod_S_idea').checked){
		iS_line = document.getElementById('App_BNav_iS_idea_scale')
	}
	if(document.getElementById('App_BNav_mod_S_user').checked){
		iS_line = document.getElementById('App_BNav_iS_user_scale')
	}
	if(document.getElementById('App_BNav_mod_S_global').checked){
		iS_line = document.getElementById('App_BNav_iS_global_scale')
	}

	if(element.checked == "true"){console.log("ciao")
		BNAV_refresh_iS_line(iS_line,null,scale_data.TET)
	}else{
		BNAV_refresh_iS_line(iS_line,scale_data.iS,scale_data.TET)
	}
}

function BNAV_select_scale_form_input(input){
	var select_scale_div = input.closest('.App_BNav_mod_S_scale_radio').querySelector('input')
	select_scale_div.click()
}


function BNAV_select_scale_sequence_button(button,move_RE_H_scrollbar=false){
	//find all other buttons and uncheck them
	var container = button.closest(".App_BNav_mod_S_top_sequence")
	if(container==null){
		//eliminated button
		return
	}
	var checked_button_list = [...container.querySelectorAll(".checked")]
	//no radio

	var checked = checked_button_list.some(item=>{return item==button})
	checked_button_list.forEach(item=>{
		item.classList.remove('checked')
	})

	//var circle_data = {acronym:acronym,starting_note:0,rotation:0,duration:""}
	if(!checked){
		button.classList.add('checked')
		//move horizontal scrollbar to correct point
		if(move_RE_H_scrollbar){
			var button_list = [...container.querySelectorAll(".App_BNav_mod_S_sequence_item")]
			var index= button_list.indexOf(button)
			BNAV_move_scrollbar_to_selected_scale_number(index)
		}
	}
}

function BNAV_move_scrollbar_to_selected_scale_number(index){
	if(tabREbutton.checked){
		RE_show_scale_number_S_line(index)
	}else{
		PMC_show_scale_number_S_line(index)
	}
}

function BNAV_play_scale_button(button){
	//play this scale
	var parent = button.closest(".App_BNav_mod_S_scale_radio")
	var data = BNAV_scale_circle_read_input_values()
	//Change, play button play the original scale with 0 rotation BUT starting note
	APP_play_this_scale(JSON.parse(parent.value),data.starting_note,0)
}

function BNAV_play_selected_scale_button(button){
	//play selected scale
	var container = button.closest(".App_BNav_mod_content_body")
	var scale_list = container.querySelector(".App_BNav_mod_S_scale_list")
	var radio_selected = scale_list.querySelector("input:checked")

	if(radio_selected!=null){
		var parent = radio_selected.closest(".App_BNav_mod_S_scale_radio")
		var data = BNAV_scale_circle_read_input_values()
		APP_play_this_scale(JSON.parse(parent.value),data.starting_note,data.rotation)
	}
}

function BNAV_play_edited_scale_button(button){
	//play edited scale
	if(editor_scale!=undefined){
		//console.log(editor_scale)
		APP_play_this_scale(editor_scale.scale,editor_scale.starting_note,editor_scale.rotation)
	}
}

function BNAV_use_unuse_scale_button(element){
	var parent = element.closest(".App_BNav_mod_S_scale_radio")
	//HEI!!!! it is a div written from js, use getattribute
	//HEI!!!! it is a div.value = something from js, use .value
	var scale_selected = JSON.parse(parent.value)
	//verify if this scale does not already exist
	var index = -1

	var already_exist = idea_scale_list.scale_list.some(item=>{
		index++
		return scale_selected.name===item.name && scale_selected.TET==item.TET
	})
	if(element.classList.contains("selected")==true){
		//remove element from idea list
		if(already_exist){
			idea_scale_list.scale_list.splice(index,1)
			BNAV_generate_TET_list_from_scale_list(idea_scale_list)
			BNAV_refresh_idea_scale_tab()
			BNAV_save_new_scale_data()
		}else{
			console.error("Removing scale successfully failed: scale not found")
		}
	}else{
		//add element to idea list

		if(!already_exist){
			scale_selected.letter=""
			idea_scale_list.scale_list.push(scale_selected)
			BNAV_generate_TET_list_from_scale_list(idea_scale_list)
			BNAV_refresh_idea_scale_tab()
			BNAV_save_new_scale_data()
		}
	}
}

function BNAV_bookmark_scale_button(button,scale_case){
	//directly
	var parent = button.closest(".App_BNav_mod_S_scale_radio")
	var scale_selected = JSON.parse(parent.value)
	switch (scale_case) {
		case "global":
			if(!button.classList.contains("starred")){
				//add
				button.classList.add("starred")
				global_scale_list_bookmarks.push({name: scale_selected.name, TET:scale_selected.TET})
			}else{
				//remove
				button.classList.remove("starred")
				var index = -1
				if(global_scale_list_bookmarks.some(item=>{
					index++
					return item.name===scale_selected.name && item.TET===scale_selected.TET
				})){
					global_scale_list_bookmarks.splice(index,1)
				}
			}
			DATA_save_scale_list_bookmarks()
		break
		case "user":
			//find scale in the list
			var index = -1
			var scale_selected_string= JSON.stringify(scale_selected)
			var found = user_scale_list.scale_list.some(item=>{
				index++
				return JSON.stringify(item)==scale_selected_string
			})
			if(!found)return
			if(!button.classList.contains("starred")){
				//add tag
				button.classList.add("starred")
				user_scale_list.scale_list[index].tags.push("bookmark")
			}else{
				//remove tag
				button.classList.remove("starred")
				var index_bookmark_tag = user_scale_list.scale_list[index].tags.indexOf("bookmark")
				user_scale_list.scale_list[index].tags.splice(index_bookmark_tag,1)
			}
			parent.value=JSON.stringify(user_scale_list.scale_list[index])
			DATA_save_user_scale_list()
		break
	}
	BNAV_order_scale_bookmark_list()
}

function BNAV_bookmark_selected_scale_button(button,scale_case){
	//general button
	var container = button.closest(".App_BNav_mod_content_body")
	var scale_list = container.querySelector(".App_BNav_mod_S_scale_list")
	var radio_selected = scale_list.querySelector("input:checked")

	if(radio_selected!=null){
		var parent = radio_selected.closest(".App_BNav_mod_S_scale_radio")
		var bookmark_button = parent.querySelector("button")
		//console.log("virtual click")
		BNAV_bookmark_scale_button(bookmark_button,scale_case)
	}
	BNAV_order_scale_bookmark_list()
}

function BNAV_order_scale_bookmark_list(){
	//var scale_tab = document.getElementById("ES_bottom_content")
	var user_scale_subtab = document.getElementById("App_BNav_mod_S_content_user")
	var global_scale_subtab = document.getElementById("App_BNav_mod_S_content_global")

	Order(user_scale_subtab)
	Order(global_scale_subtab)

	function Order(element){
		var container = element.querySelector(".App_BNav_mod_S_scale_list")
		var scale_list = [...container.querySelectorAll(".App_BNav_mod_S_scale_radio")]
		//var last_bookmarked_element=0
		var index=0

		scale_list.forEach(item=>{
			if(item.querySelector("button").classList.contains("starred") == 1){
				//is bookmark
				if(index==0){
					//move at the very start
					if(scale_list.indexOf(item)!=0){
						//put it first
						container.insertBefore(item,container.firstChild)
					}
				}else{
					//after last_bookmarked_element
					//create strange things if the div is not shown
					//container.insertBefore(item,last_bookmarked_element.nextSibling)
					container.insertBefore(item,container.children[index])
				}
				//last_bookmarked_element=item
				index++
			}
		})
	}
}

function BNAV_edit_selected_scale_button(button){
	var container = button.closest(".App_BNav_mod_content_body")
	var scale_list = container.querySelector(".App_BNav_mod_S_scale_list")
	var radio_selected = scale_list.querySelector("input:checked")

	if(radio_selected!=null){
		var parent = radio_selected.closest(".App_BNav_mod_S_scale_radio")
		var scale_to_edit = JSON.parse(parent.value)
		//if tet not active does nothing
		if(scale_to_edit.TET==TET){
			var acronym = ""
			if(scale_to_edit.letter === undefined){
				//acronym = =""
			}else{acronym=scale_to_edit.letter
			}
			editor_scale={scale:scale_to_edit,
				acronym: acronym,
				rotation: 0,
				starting_note:0,
				duration: ""
			}
			BNAV_refresh_editor_scale_tab()
			//minimap
			var iS_list = JSON.parse(JSON.stringify(editor_scale.scale.iS))
			BNAV_scale_editor_iS_miniDiv(iS_list)
			//change to editor tab
			//BNAV_change_ES_tab(0)
			document.getElementById('App_BNav_mod_S_editor').click()
		}else{
			//maybe transform if scale is somewhat compatible XXX
			console.error("Need to check scale compatibility !!!!")
		}
	}
}

function BNAV_delete_selected_scale_button(button,scale_case){
	//var container = button.closest(".ES_scaleR")//XXX
	var container = button.closest(".App_BNav_mod_content_body")//XXX
	//var label_container = container.closest(".ES_scale_list")//XXX App_BNav_mod_S_scale_list
	var radio_container = container.querySelector(".App_BNav_mod_S_scale_list")//XXX App_BNav_mod_S_scale_list
	//var label_selected = container.querySelector(".ES_scale_label_checked")///XXX .App_BNav_mod_S_scale_radio > input[type="radio"]:checked
	var label_selected = radio_container.querySelector('.App_BNav_mod_S_scale_radio > input[type="radio"]:checked')

	if(label_selected!=null){
		switch (scale_case) {
			case "idea":
				//delete scale from idea list
				var radio_selected = label_selected.closest('.App_BNav_mod_S_scale_radio')
				var scale_to_delete = JSON.parse(radio_selected.value)
				//delete scale from user scale list
				var scale_is_used = false
				if(scale_to_delete.letter!=""){
					//verify if it is used
					var scale_sequence = DATA_get_scale_sequence()
					scale_is_used = scale_sequence.some(item=>{
						index++
						return scale_to_delete.TET==TET && scale_to_delete.letter==item.acronym
					})
				}

				if(scale_is_used==true){
					//deleting not possible
					return
				}else{
					//reset circle
					var circle_data = {acronym:"",starting_note:0,rotation:0,duration:""}
					BNAV_scale_circle_change_input_values(circle_data)

					var index = -1
					idea_scale_list.scale_list.find(item=>{
						index++
						return scale_to_delete.name===item.name  && scale_to_delete.TET==item.TET
					})

					if(index!=-1){
						idea_scale_list.scale_list.splice(index,1)
						//verify tet list
						BNAV_generate_TET_list_from_scale_list(idea_scale_list)
						BNAV_refresh_idea_scale_tab()
						BNAV_save_new_scale_data()
					}
				}
			break
			case "user":
				//delete scale from user scale list
				var radio_selected = label_selected.closest('.App_BNav_mod_S_scale_radio')
				//reset circle
				var circle_data = {acronym:"",starting_note:0,rotation:0,duration:""}
				BNAV_scale_circle_change_input_values(circle_data)
				var scale_to_delete = JSON.parse(radio_selected.value)

				//delete scale from user scale list
				var index = -1
				user_scale_list.scale_list.find(item=>{
					index++
					return scale_to_delete.name===item.name  && scale_to_delete.TET==item.TET
				})
				if(index!=-1){
					user_scale_list.scale_list.splice(index,1)
					//verify tet list
					BNAV_generate_TET_list_from_scale_list(user_scale_list)
					//save new user file
					DATA_save_user_scale_list()
					BNAV_refresh_user_scale_tab()
				}
			break
		}
	}
}

function BNAV_save_selected_scale_button(button){
	//save selected idea scale to user scale list
	var container = button.closest(".App_BNav_mod_content_body")
	var scale_list = container.querySelector(".App_BNav_mod_S_scale_list")
	var radio_selected = scale_list.querySelector("input:checked")
	if(radio_selected!=null){
		var parent = radio_selected.closest(".App_BNav_mod_S_scale_radio")
		var scale_to_save = JSON.parse(parent.value)
		//verify if this scale does not already exist
		var already_exist = user_scale_list.scale_list.some(item=>{
			return scale_to_save.name===item.name && scale_to_save.TET==item.TET
		})
		//compare arrays to detect if there is a change
		if(already_exist){
			//REWRITE
			//alert
			if(BNAV_open_alert_overwrite_scale("user",scale_to_save.name)){
				//modify an existing scale
				scale_to_save.letter=""
				//find scale to overwrite
				var target = user_scale_list.scale_list.find(item=>{
					return scale_to_save.name===item.name && scale_to_save.TET==item.TET
				})
				target.module=scale_to_save.module
				target.iS=scale_to_save.iS
				//verify tet list
				BNAV_generate_TET_list_from_scale_list(user_scale_list)
				//save new user file
				DATA_save_user_scale_list()
				BNAV_refresh_user_scale_tab()
			}
			BNAV_check_buttons_save_body_top(scale_to_save)
		} else {
			//console.log("scale added")
			scale_to_save.letter=""
			user_scale_list.scale_list.push(scale_to_save)
			//verify tet list
			BNAV_generate_TET_list_from_scale_list(user_scale_list)
			//save new user file
			DATA_save_user_scale_list()
			BNAV_refresh_user_scale_tab()
		}
	}
}

function BNAV_assign_name_editor_scale_input(element){
	var name = element.value
	if(name == previous_value) return

	if(name==""){
		//if no name, find one suitable
		name = BNAV_new_scale_name()
		element.value=name
	}
	editor_scale.scale.name=name
	BNAV_check_buttons_save_editor_scale()
}

function BNAV_assign_acronym_idea_scale_input(element){
	//Assign acronym letter on idea tab
	var acronym = element.value
	if(acronym == previous_value) return

	var scale_div = element.closest('.App_BNav_mod_S_scale_radio')
	var scale_to_modify_string = scale_div.value
	var scale_to_modify = JSON.parse(scale_to_modify_string)
	var scale_TET=scale_to_modify.TET

	//verify if scale is already used end i try to erease it
	if(acronym==""){
		var scale_is_used = false

		//verify if it is used
		var scale_sequence = DATA_get_scale_sequence()

		var scale_is_used = scale_sequence.some(item=>{
			index++
			return scale_to_modify.TET==TET && scale_to_modify.letter==item.acronym
		})
		if(scale_is_used){
			element.value=previous_value
			return
		}
	}

	//verify letter is used by other scales
	if(BNAV_verify_new_scale_acronym(acronym,scale_TET)){
		//find scale in list
		var index=-1
		var there_is = idea_scale_list.scale_list.some(item=>{index++
			return JSON.stringify(item)==scale_to_modify_string
		})

		//changing the letter
		if(there_is){
			idea_scale_list.scale_list[index].letter=acronym

			if(scale_to_modify.TET==TET){
				//change editor letter too if is the same!!!
				var scale_iS_string = JSON.stringify(scale_to_modify.iS)
				if(editor_scale.scale.TET== scale_to_modify.TET && editor_scale.scale.name== scale_to_modify.name && JSON.stringify(editor_scale.scale.iS)== scale_iS_string){
					//change letter in editor
					editor_scale.acronym = acronym
					document.querySelector(".App_BNav_mod_S_content_header_acronym > input").value=editor_scale.acronym
				}
				//change values in circle
				var circle_data = {acronym:acronym,starting_note:0,rotation:0,duration:""}
				BNAV_scale_circle_change_input_values(circle_data)

				if(previous_value!=""){
					//change all the sequence old letters if there are
					var scale_sequence = DATA_get_scale_sequence()
					scale_sequence.forEach(item=>{
						if(item.acronym==previous_value)item.acronym=acronym
					})
					BNAV_write_scale_sequence(scale_sequence)
				}
			}

			//change letter on element value
			scale_to_modify.letter=acronym
			scale_div.value=JSON.stringify(scale_to_modify)
			BNAV_save_new_scale_data()
		} else{
			console.error("ERROR : scale not found in database")
			element.value = previous_value
		}
	}else{
		element.value = previous_value
		console.error("ERROR : scale letter used in database")
	}
}

function BNAV_verify_new_scale_acronym(acronym, scale_TET){
	//true if there is no duplicate!
	var there_it_is = idea_scale_list.scale_list.filter(scale=>{return scale.TET==scale_TET}).some(item=>{return item.letter==acronym && acronym!=""})
	return !there_it_is
}

function BNAV_play_scale_note_on_click(element){
	var valueNumber = parseInt(element.value)

	if(Number.isInteger(valueNumber) && SNAV_read_note_on_click() == true){
		APP_play_this_scale_note(valueNumber)
	}
}

function BNAV_select_scale_number_tab_list(index){
	var scale_list_div = document.querySelector('.App_BNav_mod_S_top_sequence')
	var scale_list = [...scale_list_div.querySelectorAll(".App_BNav_mod_S_sequence_item")]
	var selected = scale_list_div.querySelector(".checked")
	if(selected!=null)	selected.classList.remove('checked')
	//no virtual click
	//scale_list[index].click()
	//call the function directly + false (not move the H bar)
	BNAV_select_scale_sequence_button(scale_list[index],false)
}

function BNAV_absolute_note_to_scale(note,positive=true,scale_data){
	var n_scale = note-scale_data.starting_note
	if(n_scale<0)n_scale=0-TET-n_scale
	var reg = Math.floor(n_scale/TET)
	var resto = Math.abs(n_scale)%TET
	var iS_list = []

	var scale = idea_scale_list.scale_list.find(item=>{
		return item.TET == TET && item.letter==scale_data.acronym
	})

	//changing the letter
	if(scale!=null){
		//a COPY, not a pointer
		var iS_list = JSON.parse(JSON.stringify(scale.iS))
	}else {
		//console.log("nothing")
		return ["",0,""]
	}

	for (var i = 0 ; i<scale_data.rotation; i++){
		iS_list.push(iS_list.shift())
	}

	var current_grade = 0
	var current_note = 0
	var current_iS = iS_list[0]
	iS_list.find(iS=>{
		if (current_note>=resto){
			return true
		}else{
			current_grade++
			current_note+=iS
			current_iS = iS
		}
	})

	var delta = 0
	if((current_note-resto)!=0){
		delta = current_iS-(current_note-resto)
	}

	if(positive){
		//if is not a grade take ++ diesis
		if(delta!=0){
			return [current_grade-1,delta,reg]
		}else{
			return [current_grade,delta,reg]
		}
	}else{
		//if is not a grade take a bemolle
		//console.log("calcolated delta "+delta)
		if(delta!=0){
			var negative_delta = delta-iS_list[current_grade-1]
			//avoid over shoot grade ( es grade 7- == 0-
			if(iS_list.length==current_grade) {
				current_grade=0
				reg++
			}
			return [current_grade,negative_delta,reg]
		}else{
			return [current_grade,delta,reg]
		}
	}
}

function BNav_get_scale_from_acronym(acronym){
	var scale = idea_scale_list.scale_list.find(item=>{
		return item.TET == TET && item.letter==acronym
	})

	//a COPY, not a pointer
	return scale
}

function BNav_test_scale(start,rot,isdiesis){
	for(let i = 0; i<(TET*7);i++){
		var [g,d,r]=BNAV_absolute_note_to_scale(i,{ acronym: "a",isdiesis, starting_note: start, rotation: rot, duration: 6 })
		var diesis=""
		if(d>0){
			for (let i = 0; i < d; i++) {
				diesis+="+"
			}
		}
		if(d<0){
			for (let i = 0; i > d; i--) {
				diesis+="-"
			}
		}
		console.log("Na : "+i+" == "+g+diesis+"r"+r)
	}
}

function BNAV_get_scale_module(scale_data){

	var scale = idea_scale_list.scale_list.find(item=>{
		return item.TET == TET && item.letter==scale_data.acronym
	})

	//changing the letter
	if(scale!=null){
		//return scale.iS.length
		return scale.module
	}else {
		console.log("nothing")
		return null
	}
}


function BNAV_scale_note_to_absolute(grade,delta,reg,scale_data){
	var starting_note = scale_data.starting_note

	var iS_list = []

	var scale = idea_scale_list.scale_list.find(item=>{
		return item.TET == TET && item.letter==scale_data.acronym
	})

	//changing the letter
	if(scale!=null){
		//a COPY, not a pointer
		var iS_list = JSON.parse(JSON.stringify(scale.iS))
		
	}else {
		return null
	}

	for (var i = 0 ; i<scale_data.rotation; i++){
		iS_list.push(iS_list.shift())
	}

	var grade_position = 0

	var y = 0

	if(grade<0){
		//case iNgm can generate neg grade
		//adjust reg
		var mod = scale.module
		var resto = (grade)%mod
		var new_grade = resto==0 ? 0 : mod+resto
		var d_reg = Math.floor(grade/mod)
		var new_reg=reg+d_reg
		//console.log(grade+"r"+reg+"  --->  "+new_grade+"r"+new_reg)
		reg=new_reg
		grade=new_grade
	}

	for (var i = 0 ; i<grade; i++){
		//grade CAN be > iS length
		grade_position += iS_list[y]
		y++
		if(y>=iS_list.length)y=0
	}

	//XXX TEST CONTROL IF DELTA IS POSSIBLE (POSITIVE OR NEGATIVE)
	//console.error("TEST INTRODUZIONE GRADO")
	if(delta>0){
		if(iS_list[y]<=delta)delta=iS_list[y]-1
	}
	if(delta<0){
		if(iS_list[y-1]+delta<=0)delta=1-iS_list[y-1]
	}
	//XXX END TEST

	var note_number = TET*reg + starting_note + grade_position + delta
	var max_note_number = TET * 7-1
	if(note_number>max_note_number)note_number=max_note_number
	if(note_number<0)note_number=0
	return note_number
}

function BNAV_edit_scale_duration_input(input){
	var sequence_item = input.closest(".App_BNav_mod_S_sequence_item")
	var scale_data = JSON.parse(sequence_item.value)
	if(input.value=="" || input.value=="0"){
		input.value=previous_value
		return
	}
	scale_data.duration = parseInt(input.value)
	sequence_item.value=JSON.stringify(scale_data)

	BNAV_save_new_scale_data()
}

function BNAV_edit_scale_starting_input(input){
	var sequence_item = input.closest(".App_BNav_mod_S_sequence_item")
	var scale_data = JSON.parse(sequence_item.value)
	if(input.value==""){
		input.value=previous_value
		return
	}
	scale_data.starting_note = parseInt(input.value)
	sequence_item.value=JSON.stringify(scale_data)
	BNAV_save_new_scale_data()
}

function BNAV_edit_scale_rotation_input(input){
	var sequence_item = input.closest(".App_BNav_mod_S_sequence_item")
	var scale_data = JSON.parse(sequence_item.value)
	if(input.value==""){
		input.value=previous_value
		return
	}
	scale_data.rotation = parseInt(input.value)
	sequence_item.value=JSON.stringify(scale_data)
	BNAV_save_new_scale_data()
}

//Accessory functions
function BNAV_stop_scale_name_interval(){
	for(i=0; i<100; i++){
		window.clearInterval(i);
	}
}

function BNAV_autoscroll_scale_name(nameLabel){
	var prev_val = 0
	//var labelInput = nameLabel.parentNode.childNodes[3]
	var nameP = nameLabel.querySelector('p')

	setInterval(function() {
		//console.log(nameP.scrollTop)
		nameP.scrollTo(0,nameP.scrollTop+10)
		if(nameP.scrollTop %10 !== 0){
			nameP.scrollTo(0,0)
		}
	}, 800)
}

function BNAV_open_alert_overwrite_scale(type="generic",name=""){
	var message = ""
	switch (type) {
		case "user":
			message = 'You are about to overwrite Scale "'+name+'" from your user library. \n\nAre you sure you want to proceed? \n\n'+RE_string_to_superscript("deactivate this window from the options")
		break;
		case "idea":
			message = 'You are about to overwrite Scale "'+name+'" from your idea library. \n\nAre you sure you want to proceed? \n\n'+RE_string_to_superscript("deactivate this window from the options")
		break;
		default:
			message = "You are about to overwrite a Scale from your library. \n\nAre you sure you want to proceed? \n\n"+RE_string_to_superscript("deactivate this window from the options")
	}
	if(SNAV_read_alert_overwrite_scale() == true){
		//string= "You are about to overwrite a Scale from your library. \n\nAre you sure you want to proceed? \n\n"+RE_string_to_superscript("deactivate this window from the options")
		//var retVal = confirm("You are about to overwrite a Scale from your library. \n\nAre you sure you want to proceed? \n\n"+"You can deactivate this window from the options")
		var retVal = confirm(message)
		return retVal
	}else{
		return true
	}
}

function BNAV_calculate_scale_interval_string(A_Na,A_diesis,A_Na_element_scale,B_Na,B_diesis,B_Na_element_scale){
	//calculate resto in grades
	var [grade_A,delta_A,reg_A]=BNAV_absolute_note_to_scale(A_Na,A_diesis,A_Na_element_scale)
	var [grade_B,delta_B,reg_B]=BNAV_absolute_note_to_scale(B_Na,B_diesis,B_Na_element_scale)
	//4 cases
	// console.log(grade_A+" "+delta_A+" "+reg_A)
	// console.log(grade_B+" "+delta_B+" "+reg_B)
	var scale_module = BNav_get_scale_from_acronym(A_Na_element_scale.acronym).module

	var stringiN=""
	var d_grad = grade_B-grade_A+(reg_B-reg_A)*scale_module
	stringiN+= d_grad

/* grade AND diesis
	var d_grad = grade_B-grade_A+(reg_B-reg_A)*scale_module
	var d_delta = delta_B

	stringiN+= d_grad
	//diesis true / false ???? XXX
	if(d_delta==1)stringiN+=RE_string_to_superscript("+")
	if(d_delta>1)stringiN+=RE_string_to_superscript("+"+d_delta)
	if(d_delta==-1)stringiN+=RE_string_to_superscript("-")
	if(d_delta<-1)stringiN+=RE_string_to_superscript(d_delta.toString())
*/
/* OBSOLETE grade diesis reg
	var d_reg = reg_B-reg_A
	var d_grad = grade_B-grade_A
	var d_delta = delta_B-delta_A

	//deltas
	stringiN+= d_grad
	if(d_delta==1)stringiN+=RE_string_to_superscript("+")
	if(d_delta>1)stringiN+=RE_string_to_superscript("+"+d_delta)
	if(d_delta==-1)stringiN+=RE_string_to_superscript("-")
	if(d_delta<-1)stringiN+=RE_string_to_superscript(d_delta.toString())
	if(d_reg!=0)stringiN+="r"+(d_reg.toString())
//*/
	return stringiN
}

function BNAV_check_buttons_save_editor_scale(){
	var editor_content = document.querySelector("#App_BNav_mod_S_content_editor")
	var button_container = editor_content.querySelector(".App_BNav_mod_content_header")
	var [save_to_user,save_to_idea] = [...button_container.querySelectorAll("button")]
	//var editor_scale_name = editor_content.querySelector('.App_BNav_mod_S_content_header_inp_name').value

	//create a copy
	var scale_to_save = JSON.parse(JSON.stringify(editor_scale.scale))

	//verify if this scale does not already exist
	var already_exist_in_user = user_scale_list.scale_list.find(item=>{
		return scale_to_save.name===item.name && scale_to_save.TET==item.TET
	})

	var already_exist_in_idea = idea_scale_list.scale_list.find(item=>{
		return scale_to_save.name===item.name && scale_to_save.TET==item.TET
	})

	if(already_exist_in_user!=null){
		if(JSON.stringify(scale_to_save.iS) == JSON.stringify(already_exist_in_user.iS)){
			//nothing changes GREEN
			save_to_user.style="background-color: var(--Nuzic_green);"
		}else{
			//change iS
			save_to_user.style="background-color: var(--Nuzic_yellow);"
		}
	}else{
		//scale not found
		save_to_user.style=""
	}

	if(already_exist_in_idea!=null){
		if(JSON.stringify(scale_to_save.iS) == JSON.stringify(already_exist_in_idea.iS)){
			//nothing changes GREEN
			save_to_idea.style="background-color: var(--Nuzic_green);"
		}else{
			//change iS
			save_to_idea.style="background-color: var(--Nuzic_yellow);"
		}
	}else{
		//scale not found
		save_to_idea.style=""
	}
}

function BNAV_check_buttons_save_body_top(scale){
	var container = document.querySelector(".App_BNav_mod_S_content")
	var [save_from_idea,save_from_global] = [...container.querySelectorAll(".App_BNav_mod_S_save_button")]
	//var editor_scale_name = editor_content.querySelector('.App_BNav_mod_S_content_header_inp_name').value

	//create a copy
	var scale_to_save = JSON.parse(JSON.stringify(scale))

	//verify if this scale does not already exist
	var already_exist_in_user = user_scale_list.scale_list.find(item=>{
		return scale_to_save.name===item.name && scale_to_save.TET==item.TET
	})

	if(already_exist_in_user!=null){
		if(JSON.stringify(scale_to_save.iS) == JSON.stringify(already_exist_in_user.iS)){
			//nothing changes GREEN
			save_from_idea.style="background-color: var(--Nuzic_green);"
			save_from_global.style="background-color: var(--Nuzic_green);"
		}else{
			//change iS
			save_from_idea.style="background-color: var(--Nuzic_yellow);"
			save_from_global.style="background-color: var(--Nuzic_yellow);"
		}
	}else{
		//scale not found
		save_from_idea.style=""
		save_from_global.style=""
	}
}

