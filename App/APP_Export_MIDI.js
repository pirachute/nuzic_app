function Generate_midi_blob(current_state_point){
	//console.log(current_state_point)
	//channel for main metronome???

	// current_state_point.voice_data[voice_index].data.midi_data
	// contain
	// .time
	// .note (freq)
	// .duration

	//with jsmidgen.js
	if(current_state_point.global_variables.TET!=12){
		alert("Can't convert to MIDI a non TET 12 sound (YET!)")  // display string message
		return null
	}else{

		var file = new Midi.File()
		// goog number (9*8*5)
		//file.ticks =2520
		//max number
		var tiks_N = 32767
		file.ticks = tiks_N
		var tick_L = (60/PPM)/tiks_N

		//define name XXX
		//var MetaEvent = new Midi.MetaEvent()
		//console.log(MetaEvent)
		//return

		//define Li   XXX

		//define ticks for beat XXX


		//exist midievent and util

		var track_list = []
		var track_index = 0
		//create a track for every voice or chord list
		current_state_point.voice_data.forEach(voice=>{
			track_list[track_index]=new Midi.Track()
			file.addTrack(track_list[track_index])
			track_list[track_index].setTempo(current_state_point.global_variables.PPM) //NEOPULSE!!!! XXX
			//track_list[track_index].setInstrument(1, voice.instrument)
			var number = 1
			switch (voice.instrument) {
				case "0":
					// 1 Acoustic Grand Piano
					number = 0
				break
				case "1":
					// 25 Acoustic Guitar (nylon)
					// 26 Acoustic Guitar (steel)
					number = 25
				break
				case "2":
					// 41 Violin
					number = 40
				break
				case "3":
					// 74 Flute
					number = 73
				break
				case "4":
					// 65 Soprano Sax
					number = 64
				break
			}

			let hexStr = "0x"+ number.toString(16)
			track_list[track_index].setInstrument(0, hexStr)

			//adding notes
			// voice.data.midi_data
			// .duration
			// .note
			// .time
			var current_d_time=0
			var index_event = 0
			voice.data.midi_data.note_freq.forEach(note_str =>{
				var note = +(note_str)
				var duration_freq = voice.data.midi_data.duration[index_event]
				//cange duration frequency in seconds
				var duration = 1/duration_freq  // OMG!!! XXX change it!
				if(note<0){
					//silence
					//current_d_time+=Tone.Time(duration).toTicks()
					current_d_time+= Math.round(duration/tick_L)
					//current_d_time+=64
				}else{
					//note

					var note_Midi = Tone.Midi(note+"hz").toMidi() //XXX
					//track_list[track_index].addNote(0, note, time, velocity)
					//in tiks XXX
					//duration = 64
					//aproximation min 1/128th note
					//var duration_converted = Tone.Time(duration).toNotation()
					//
					//maybe better in
					// console.log("note")
					// console.log(note_str)
					// console.log("duration")
					// console.log(duration)
					//console.log(Tone.Time(duration))
					//console.log(Tone.Time(duration).toMilliseconds())
					//var duration_converted = Tone.Time(duration).toTicks()
					var duration_converted = Math.round(duration/tick_L)
					//for tone there are 1000 ticks for pulse

					// console.log(duration_converted)
					track_list[track_index].addNoteOn(0, note_Midi, current_d_time)
					//track_list[track_index].addNoteOff(0, note_Midi, duration)
					track_list[track_index].addNoteOff(0, note_Midi, duration_converted)
					current_d_time=0
				}
				index_event++
			})


			//var note = Tone.Midi("445hz").toMidi()
			// var freq = track_index*100
			// var note = Tone.Midi(freq+"hz").toMidi()
			// var time = 64 //XXX
			// var velocity = voice.volume // XXX
			// track_list[track_index].addNote(0, note, time, velocity)

			track_index++
		})

		//var track = new Midi.Track()
		//file.addTrack(track)
		//track.setTempo(123)

		//console.log(Tone.Midi("445hz").toMidi()) //arrotonda a 69
		//console.log(Tone.Midi("440hz").toMidi())	//A4 nota midi 69
		// if the Transport is at 120bpm:
		//console.log(Tone.Time(2).toNotation())   // returns "1m"
// console.log(file)
//return
		return file
	}


/* TESTING POURPOSE
	track.addNote(0, 'c4', 64);
	track.addNote(0, 'd4', 64);
	track.addNote(0, 'e4', 64);
	track.addNote(0, 'f4', 64);
	track.addNote(0, 'g4', 64);
	track.addNote(0, 'a4', 64);
	track.addNote(0, 'b4', 64);
	track.addNote(0, 'c5', 64);



addNote(channel, pitch, duration[, time[, velocity]])

Add a new note with the given channel, pitch, and duration

    If time is given, delay that many ticks before starting the note
    If velocity is given, strike the note with that velocity

addNoteOn(channel, pitch[, time[, velocity]])

Start a new note with the given channel and pitch

    If time is given, delay that many ticks before starting the note
    If velocity is given, strike the note with that velocity

addNoteOff(channel, pitch[, time[, velocity]])

End a note with the given channel and pitch

    If time is given, delay that many ticks before ending the note
    If velocity is given, strike the note with that velocity



addChord(channel, chord[, velocity])

Add a chord with the given channel and pitches

    Accepts an array of pitches to play as a chord
    If velocity is given, strike the chord with that velocity

setInstrument(channel, instrument[, time])

Change the given channel to the given instrument

    If time is given, delay that many ticks before making the change

setTempo(bpm[, time])

Set the tempo to bpm beats per minute

    If time is given, delay that many ticks before making the change





*/





}

/*


var saveMIDI = function() {

        //Confirm there are notes to save, aborting if there are none
        var noteNum = 0;
        for (var i=0; i<Orbits.length; i++) {
            noteNum += Orbits[i].notes.length;
        }
        if (noteNum == 0) {return false;}

        // Create a new MIDI file with jsmidgen, and a track to store the note data
        var file = new Midi.File();
        var track = new Midi.Track();
        file.addTrack(track);
        track.setTempo(TEMPO);

        // Determine MIDI length (in measures) that guarentees all orbits loop and end aligned
        var len = 1;
        for (var i=0; i<Orbits.length; i++) {
            for (var j=i+1; j<Orbits.length; j++) {
                var a = Math.round(Orbits[i].radius/RADIUS_SNAP);
                var b = Math.round(Orbits[j].radius/RADIUS_SNAP);
                len = Math.max(len, Util.lcm(a, b));
            }
        }

        // Construct a timeline of events (each orbits' notes turning on and off)
        // [strangely, it seems jsmidgen's addNote method doesn't allow us to start a note while another is playing]
        var DURATION = 16; //default
        var timeline = [];
        for (var i=0; i<Orbits.length; i++) {
            var o = Orbits[i];

            // Smaller orbits need to repeat since bigger orbits take longer to complete
            var repeats = len/Math.round(o.radius/RADIUS_SNAP);
            for (var j=0; j<repeats; j++) {
                for (var k=0; k<o.notes.length; k++) {
                    // Record the time and type of each note on this orbit
                    var n = o.notes[k];
                    var angle = n.theta<-Math.PI/2 ? n.theta+2*Math.PI : n.theta; //returns a theta between (-Pi/2, 3Pi/2]
                    var fraction = (angle+Math.PI/2)/(2*Math.PI); //returns the fraction of the note's angle on the orbit (0 at top, increases clockwise up to 1)
                    var precise_time = (j+fraction) * (o.radius/RADIUS_SNAP) * file.ticks; //(ticks per beat=128, hardcoded in jsmidgen)
                    var time = Math.round(precise_time); //jsmidgen seems to always floor() 'time', which leads to midi misalignment. So, manually use round() instead.
                    var p = mapSampleToKey(n.sampler.index);
                    var on = {
                        on: true,
                        time: time,
                        pitch: p
                    };
                    var off = {
                        on: false,
                        time: time + DURATION,
                        pitch: p
                    };
                    timeline.push(on);
                    timeline.push(off);
                }
            }
        }

        // Sort the timeline chronologically
        timeline.sort(function(a,b) {
            return a.time-b.time;
        });

        // Add notes to MIDI track as different pitches
        var tPrev = 0;
        var tGlobalOffset = timeline[0].time;
        for (var i=0; i<timeline.length; i++) {
            var delay = timeline[i].time - tPrev - tGlobalOffset;
            delay = Math.max(delay, 0); //just in case, prevent delay < 0, which causes jsmidgen to crash
            if (timeline[i].on)
                track.addNoteOn(0, timeline[i].pitch, delay);
            else
                track.addNoteOff(0, timeline[i].pitch, delay);
            tPrev = timeline[i].time - tGlobalOffset;
        }

         // For testing purposes:
        // Create a new Track for the MIDI file
        var track = new Midi.Track();
        file.addTrack(track);

        // Populate track with a scale
        track.addNote(0, 'c4', 64);
        track.addNote(0, 'd4', 64);
        track.addNote(0, 'e4', 64);
        track.addNote(0, 'f4', 64);
        track.addNote(0, 'g4', 64);
        track.addNote(0, 'a4', 64);
        track.addNote(0, 'b4', 64);
        track.addNote(0, 'c5', 64);


        // Get MIDI data to be saved
        var bytesU16 = file.toBytes(); //returns a UTF-16 encoded string

        // Convert data to UTF-8 encoding
        var bytesU8 = new Uint8Array(bytesU16.length);
        for (var i=0; i<bytesU16.length; i++) {
            bytesU8[i] = bytesU16[i].charCodeAt(0);
        }

        // Populate a blob with the data
        var blob = new Blob([bytesU8], {type: "audio/midi"});


        // Export the file!
        saveAs(blob, "cyclic_drums.mid");

        return true;
    }*/




    /*

DRY WET MIDI

private void ExportToMIDI(){
		// Open file with filter
		var extensions = new [] {
			new ExtensionFilter("MIDI Files", "mid"),
			new ExtensionFilter("All Files", "*" ),
		};

		string path=StandaloneFileBrowser.SaveFilePanel("Export MIDI File", defaultPath, "save.mid", extensions);

		var midiFile = new MidiFile();
		TempoMap tempoMap = midiFile.GetTempoMap();


		float pulseLength=60f/scriptPlay.VsPlay;
		int microsecondsPulseLength = Mathf.FloorToInt(pulseLength*1000000f);

		//ripeto per numero di cicli (se ciclo == 0 == infinito lo faccio una volta - )
		int iterations = 1;
		if (scriptPlay.numberOfCyclesPlay!=0){
			iterations = scriptPlay.numberOfCyclesPlay;
		}

		eventList=scriptPlay.GiveMeEventList();

		var trackChunk = new TrackChunk(new SetTempoEvent(microsecondsPulseLength));

		for (int i = 0; i < iterations; i++){
			foreach(EventData target in eventList){
				if(target.isNote){
					int durationMicroSeconds = (int)(target.duration*microsecondsPulseLength);
					int positionMicroSeconds = Mathf.FloorToInt(target.pulseNumber*microsecondsPulseLength+i*scriptPlay.LsPlay*microsecondsPulseLength);

					//audioClipNoteNumber (le note inserite sono dei Do o C)
					//basato nel La i numeri sono quindi 3 15 27 ...
					//midi note = La number + 1
					//basato nel Do i numeri sono        0 12 24 ...
					//midi note = Do number + 4

					//ATTENZIONE LMMS DEVO FARE + 0?????
					//come se midi 0 = al nostro 0???? strano
					//in piu c'é un problema di registro: sembra che r0 ME == r1 Midi

					//stiamo usando Do ora!!
					//SevenBitNumber notename = SevenBitNumber.Parse("60");

					int notaMidi = target.noteNumber+24;
					//Debug.Log("Nota numero "+target.noteNumber+" equivalente MIDI "+notaMidi);
					string MidiNoteNumberEquivalent = (target.noteNumber+24).ToString();
					SevenBitNumber notename = SevenBitNumber.Parse(MidiNoteNumberEquivalent);

					long ticksFromMetric = TimeConverter.ConvertFrom(new MetricTimeSpan(durationMicroSeconds), tempoMap);


					using (var notesManager = trackChunk.ManageNotes()){
						NotesCollection notes = notesManager.Notes;
						notes.Add(new Note(
							notename,
							ticksFromMetric,
							LengthConverter.ConvertFrom(
								//new MetricTimeSpan(hours: 0, minutes: 0, seconds: 0),
								new MetricTimeSpan(positionMicroSeconds),
								0,
								tempoMap)
							)
						);
					}

				}
			}


		}

		midiFile.Chunks.Add(trackChunk);
		midiFile.Write(path,true,MidiFileFormat.MultiTrack);
	}
*/
