function PMC_redraw_time_line(voice_data=null){
	var line_container = document.querySelector(".App_PMC_time_line")
	//i can pass voice_data to draw (in case of drag/drop of segments)

	if(voice_data==null) {
		voice_data = DATA_get_selected_voice_data()
	}

	var D_NP = voice_data.neopulse.D
	var N_NP = voice_data.neopulse.N
	var neopulse_moltiplicator =  N_NP/D_NP
	//var mod_Li = Li / neopulse_moltiplicator
	//calculate virtual Li drag and drop
	var mod_Li = voice_data.data.segment_data.reduce((prop,segment)=>{return segment.time.slice(-1)[0].P + prop},0)

	//clear div container
	var pulse_container = line_container.querySelector(".App_PMC_time_line_pulse_container")
	pulse_container.innerHTML=""

	var div_string=""
	for (let i=0; i <= mod_Li; i++) {
		div_string+=	`
							<div class="App_PMC_time_line_pulse" style="width:calc(var(--PMC_x_block) * ${neopulse_moltiplicator}); min-width:calc(var(--PMC_x_block) * ${neopulse_moltiplicator})"
							onmouseenter="PMC_hover_time_line_pulse(this)" onmouseleave="PMC_leave_time_line_pulse(this)">
								<div class="App_PMC_time_line_pulse_top"></div>
								<div class="App_PMC_time_line_pulse_center"></div>
								<div class="App_PMC_time_line_pulse_bottom"></div>
							</div>
						`
	}
	pulse_container.innerHTML=div_string
	/*OBSOLETE
	for (let i=0; i <= mod_Li; i++) {
		//add a div
		var div = document.createElement('div')
		//line_container.appendChild(div)
		pulse_container.appendChild(div)

		div.outerHTML= 	`
							<div class="App_PMC_time_line_pulse" style="width:calc(var(--PMC_x_block) * ${neopulse_moltiplicator}); min-width:calc(var(--PMC_x_block) * ${neopulse_moltiplicator})"
							onmouseenter="PMC_hover_time_line_pulse(this)" onmouseleave="PMC_leave_time_line_pulse(this)">
								<div class="App_PMC_time_line_pulse_top"></div>
								<div class="App_PMC_time_line_pulse_center"></div>
								<div class="App_PMC_time_line_pulse_bottom"></div>
							</div>
						`
	}
	*/

	//change margin first element to align with start regardless of the current zoom
	var pulse_list = [...line_container.querySelectorAll(".App_PMC_time_line_pulse")]
	var delta = nuzic_block * PMC_zoom_x * neopulse_moltiplicator
	var offset = (nuzic_block - delta)/2
	pulse_list[0].style.marginLeft=`${offset}px`

	//write
	PMC_write_time_line_values(voice_data)
	//write 2or3 canvas
	PMC_write_time_line_canvas_pulse(voice_data)
	if(N_NP==1 && D_NP==1){
		PMC_write_time_line_compas(voice_data)
		PMC_redraw_compas_grid(voice_data)
	}else{
		PMC_clear_time_line_compas()
		PMC_clear_compas_grid(voice_data)
	}
	PMC_write_time_line_segments(voice_data)
	PMC_write_time_line_fraction_ranges(voice_data)
}

function PMC_write_time_line_values(voice_data){
	//read what type of values to write
	var pulse_type = PMC_read_vsb_T().type

	//list objects
	var container = document.querySelector(".App_PMC_time_line")
	var pulse_list = [...container.querySelectorAll(".App_PMC_time_line_pulse_center")]

	var D_NP = voice_data.neopulse.D
	var N_NP = voice_data.neopulse.N

	if((N_NP!=1 || D_NP!=1) && pulse_type!="Ts"){
		//compass not usable
		pulse_type="no_value"
	}

	var delta_step = nuzic_block * PMC_zoom_x * N_NP/D_NP
	var pulse_sequence = PMC_generate_current_voice_segment_pulse(voice_data)
	//adding functions
	var n = pulse_sequence.length
	pulse_list.forEach((item,index)=>{
		if(index<n) {
			if(pulse_sequence[index]==0 && index!=0){
				item.style.cursor="col-resize"
				//add drag segment LS function
				item.draggable=true
				item.addEventListener("dragstart", (e)=>{PMC_modify_segment_Ls_drag_start(e,item,delta_step)})
			}
		} else if(index==n) {
			item.innerHTML = `<p>${end_range}</p>`
			item.style.cursor="col-resize"
			//add drag segment LS function
			item.draggable=true
			item.addEventListener("dragstart", (e)=>{PMC_modify_segment_Ls_drag_start(e,item,delta_step)})
		}
	})

	//write values
	switch (pulse_type) {
	case "Ts":
		//write only segment numbers
		pulse_list.forEach((item,index)=>{
			if(index<n) {
				if(delta_step>=nuzic_block){
					item.innerHTML = `<p>${pulse_sequence[index]}</p>`
				}else if(delta_step>=nuzic_block/2){
					//every 2
					//if(!isOdd(pulse_sequence[i]))item.innerHTML = pulse_sequence[i]
					if(!isOdd(pulse_sequence[index]))item.innerHTML = `<p>${pulse_sequence[index]}</p>`
				}else if(delta_step>=nuzic_block/10){
					//every 10
					if(pulse_sequence[index]%10==0){
						//verify 5 away there is not another 0 or end
						var next_pulses = pulse_sequence.slice(index+1,index+6)
						if(!next_pulses.includes(0) && next_pulses.length==5)item.innerHTML = item.innerHTML = `<p>${pulse_sequence[index]}</p>`
					}
				}else{
					//every 50
					if(pulse_sequence[index]%50==0){
						//verify 25 away there is not another 0 or end
						var next_pulses = pulse_sequence.slice(index+1,index+26)
						if(!next_pulses.includes(0) && next_pulses.length==25)item.innerHTML = item.innerHTML = `<p>${pulse_sequence[index]}</p>`
					}
				}
			}
		})
	break;
	case "Tm":
		var pulse_sequence_mod = PMC_generate_current_voice_segment_pulse_mod(voice_data,n)
		pulse_list.forEach((item,index)=>{
			if(index<n) {
				if(delta_step>=nuzic_block){
					item.innerHTML = `<p>${pulse_sequence_mod[index].s}</p>`
				}else if(delta_step>=nuzic_block/2){
					//every 2
					if(!isOdd(pulse_sequence[index]))item.innerHTML = `<p>${pulse_sequence_mod[index].s_c}</p>`
				}else if(delta_step>=nuzic_block/10){
					//every 10
					if(pulse_sequence[index]%10==0){
						//verify 5 away there is not another 0 or end
						var next_pulses = pulse_sequence.slice(index+1,index+6)
						if(!next_pulses.includes(0) && next_pulses.length==5)item.innerHTML = item.innerHTML = `<p>${pulse_sequence_mod[index].s_c}</p>`
					}
				}else{
					//every 50
					if(pulse_sequence[index]%50==0){
						//verify 25 away there is not another 0 or end
						var next_pulses = pulse_sequence.slice(index+1,index+26)
						if(!next_pulses.includes(0) && next_pulses.length==25)item.innerHTML = item.innerHTML = `<p>${pulse_sequence_mod[index].s_c}</p>`
					}
				}
			}
		})
	break;
	case "Ta":
		pulse_list.forEach((item,index)=>{
			if(index<n) {
				if(delta_step>=nuzic_block){
					item.innerHTML = `<p>${index}</p>`
				}else if(delta_step>=nuzic_block/2){
					//every 2
					if(!isOdd(pulse_sequence[index]))item.innerHTML = `<p>${index}</p>`
				}else if(delta_step>=nuzic_block/10){
					//every 10
					if(pulse_sequence[index]%10==0){
						//verify 5 away there is not another 0 or end
						var next_pulses = pulse_sequence.slice(index+1,index+6)
						if(!next_pulses.includes(0) && next_pulses.length==5)item.innerHTML = item.innerHTML = `<p>${index}</p>`
					}
				}else{
					//every 50
					if(pulse_sequence[index]%50==0){
						//verify 25 away there is not another 0 or end
						var next_pulses = pulse_sequence.slice(index+1,index+26)
						if(!next_pulses.includes(0) && next_pulses.length==25)item.innerHTML = item.innerHTML = `<p>${index}</p>`
					}
				}
			}
		})
	break;
	case "no_value":
		pulse_list.forEach((item,index)=>{
			if(index<n) {
				if(delta_step>=nuzic_block){
					item.innerHTML = `<p>${no_value}</p>`
				}else if(delta_step>=nuzic_block/2){
					//every 2
					if(!isOdd(pulse_sequence[index]))item.innerHTML = `<p>${no_value}</p>`
				}else if(delta_step>=nuzic_block/10){
					//every 10
					if(pulse_sequence[index]%10==0){
						//verify 5 away there is not another 0 or end
						var next_pulses = pulse_sequence.slice(index+1,index+6)
						if(!next_pulses.includes(0) && next_pulses.length==5)item.innerHTML = item.innerHTML = `<p>${no_value}</p>`
					}
				}else{
					//every 50
					if(pulse_sequence[index]%50==0){
						//verify 25 away there is not another 0 or end
						var next_pulses = pulse_sequence.slice(index+1,index+26)
						if(!next_pulses.includes(0) && next_pulses.length==25)item.innerHTML = item.innerHTML = `<p>${no_value}</p>`
					}
				}
			}
		})
	break;
	case "seconds"://XXX not used
		var pulse_sequence = PMC_generate_current_voice_time_seconds(voice_data)
		var n = pulse_sequence.length
		var i = 0
		pulse_list.forEach(item=>{
			if(i<n) {
				item.innerHTML = pulse_sequence[i]
			} else if(i==n) {
				item.innerHTML = "||"
			}
			i++
		})
	break;
	}
}

function PMC_write_time_line_canvas_pulse(voice_data){
	// nepoulse
	var D_NP = voice_data.neopulse.D
	var N_NP = voice_data.neopulse.N
	var neopulse_moltiplicator =  N_NP/D_NP

	var canvas = document.querySelector("#App_PMC_time_line_canvas_pulse")
	var canvas_pulse_container = document.querySelector("#App_PMC_time_line_canvas_pulse_container")
	var total_length = PMC_x_length(voice_data)

	//CSS background repetition
	//canvas_pulse_container.style.width="calc(var(--PMC_x_block) * "+(total_length-4.5)+")"
	//XXX maybe smaller
	canvas_pulse_container.style.width="calc(var(--PMC_x_block) * "+Li+")"
	//canvas_pulse_container.style.height
	PMC_mouse_action_time_svg.style.width="calc(var(--PMC_x_block) * "+(total_length-0.5)+" + var(--RE_block))"
	PMC_selection_time_svg.style.width="calc(var(--PMC_x_block) * "+(total_length-0.5)+" + var(--RE_block))" //clear???XXX
	PMC_selection_time_svg.innerHTML=""

	var delta = nuzic_block * PMC_zoom_x * neopulse_moltiplicator
	var offset = nuzic_block
	var ctx = canvas.getContext('2d')
	canvas.height= nuzic_block *1.5
	canvas.width = delta
	//nuzic_block + nuzic_block * PMC_zoom_x * total_length - 0.5 * nuzic_block * PMC_zoom_x
	//pulse tic
	ctx.beginPath()
	var x = Math.round(delta*0.5)
	ctx.moveTo(x-0.5,0)
	ctx.lineTo(x-0.5,7)
	ctx.strokeStyle = nuzic_dark
	ctx.lineWidth = 1
	ctx.stroke()

	canvas_pulse_container.style.backgroundSize=delta+"px"
	canvas_pulse_container.style.backgroundPositionX= "calc("+delta+" * 0.5px)"
	canvas_pulse_container.style.backgroundImage='url('+canvas.toDataURL("image/png")+')'

	//segments tics XXX always invisible, covered by segment menu
}

function PMC_write_time_line_compas(voice_data){
	//SVG
	var svg = document.querySelector("#App_PMC_time_line_svg_compas")
	var total_length = PMC_x_length(voice_data)
	svg.style.width="calc(var(--PMC_x_block) * "+(total_length-0.5)+" + var(--RE_block))"
	svg.innerHTML=""

	var delta = nuzic_block * PMC_zoom_x
	var offset = nuzic_block

	var compas_data = DATA_get_current_state_point(false).compas

	var time_compas_change = []
	var position = 0
	compas_data.forEach(item=>{
		var lg = item.compas_values[1]
		var rep = item.compas_values[2]
		for (var i=0; i<rep;i++){
			position+=lg
			time_compas_change.push(position)
		}
	})

	//compas tics
	var svg_string=""
	if(time_compas_change.length!=0){
		time_compas_change.unshift(0)
		for (var i=0; i<time_compas_change.length;i++){
			var x = offset+time_compas_change[i]*delta-0.5
			svg_string+=`
					<line x1="${x}" y1="0" x2="${x}" y2="7" stroke="${nuzic_dark}" stroke-width="3"></line>
				`
		}
	}
	svg.innerHTML+=svg_string
}

function PMC_clear_time_line_compas(){
	//SVG
	var svg = document.querySelector("#App_PMC_time_line_svg_compas")
	svg.style.width="calc(var(--PMC_x_block) * "+(Li)+" + var(--RE_block))"
	svg.innerHTML=""
}

function PMC_write_time_line_fraction_ranges(voice_data){
	// nepoulse
	var D_NP = voice_data.neopulse.D
	var N_NP = voice_data.neopulse.N
	var neopulse_moltiplicator =  N_NP/D_NP

	var svg = document.querySelector("#App_PMC_time_line_svg_frac")
	svg.innerHTML=""
	//var total_length = PMC_x_length(voice_data)
	//svg.style.width="calc(var(--PMC_x_block) * "+(total_length-0.5)+" + var(--RE_block))"
	//XXX maybe smaller
	svg.style.width="calc(var(--PMC_x_block) * "+(Li+0.5)+" + var(--RE_block))"

	var delta = nuzic_block * PMC_zoom_x * neopulse_moltiplicator
	var offset = nuzic_block

	var positions = PMC_generate_current_voice_time_positions(voice_data)

	//yellow background
	var svg_string=""
	for (var i=0; i<positions.time_fraction_range_change.length-1;i++){
		if(positions.time_fraction_range_change[i][1]!=1 || positions.time_fraction_range_change[i][2]!=1){
			//paint a yellow line
			var x_from = offset+positions.time_fraction_range_change[i][0]*delta
			var x_to = offset+positions.time_fraction_range_change[i+1][0]*delta
			svg_string+=`
					<line x1="${x_from}" y1="${nuzic_block *1.5-7}" x2="${x_to}" y2="${nuzic_block *1.5-7}" stroke="${nuzic_yellow}" stroke-width="14"></line>
				`
		}
	}

	//fraction tics
	for (var i=0; i<positions.time_positions.length;i++){
		var x = offset+positions.time_positions[i]*delta
		svg_string+=`
					<line x1="${x-0.5}" y1="${nuzic_block *1.5-7}" x2="${x-0.5}" y2="100%" stroke="${nuzic_dark}" stroke-width="1"></line>
				`
		//100% new_line.setAttribute('y2',nuzic_block *1.5)

	}

	//white line fraction range
	for (var i=0; i<positions.time_fraction_range_change.length-1;i++){
		//paint a yellow line
		var x = offset+positions.time_fraction_range_change[i][0]*delta
		svg_string+=`
					<line x1="${x}" y1="${nuzic_block *1.5-14}" x2="${x}" y2="100%" stroke="${nuzic_yellow}" stroke-width="6"></line>
					<line x1="${x}" y1="${nuzic_block *1.5-14}" x2="${x}" y2="100%" stroke="${nuzic_white}" stroke-width="2"></line>
				`
	}

	//last pulse idea tic
	var last_pulse_idea_tic = Li / neopulse_moltiplicator
	var x = offset+last_pulse_idea_tic*delta
	svg_string+=`
					<line x1="${x}" y1="${nuzic_block *1.5-14}" x2="${x}" y2="100%" stroke="${nuzic_dark}" stroke-width="6"></line>
					<line x1="${x}" y1="${nuzic_block *1.5-14}" x2="${x}" y2="100%" stroke="${nuzic_white}" stroke-width="2"></line>
				`

	svg.innerHTML=svg_string
	//fraction input box
	//if(PMC_zoom_x>=1){
	var class_fraction_inp_box="App_PMC_fraction_inp_box"
	if(delta<nuzic_block){
		class_fraction_inp_box="App_PMC_fraction_inp_box hidden"
	}

	var line_container = svg.closest(".App_PMC_time_line")
	var pulse_bottom_list = [...line_container.querySelectorAll(".App_PMC_time_line_pulse_bottom")]
	var frac_data = PMC_generate_current_voice_fraction_data_position(voice_data)

	//new fraction button
	//calc Li because XXX
	var mod_Li = voice_data.data.segment_data.reduce((prop,segment)=>{return segment.time.slice(-1)[0].P + prop},0)
	//var mod_Li = Li / neopulse_moltiplicator
	var current_frac_index = 0
	for (let i = 0; i < mod_Li; i++) {
		if(frac_data.stop_position[current_frac_index]<=i){
			current_frac_index ++
		}

		//is fractionable?
		if((i-frac_data.start_position[current_frac_index])%frac_data.N[current_frac_index]==0){
			pulse_bottom_list[i].innerHTML=`
								<button class="App_PMC_fraction_add"
								onclick="PMC_break_fraction_range_button(this)" onmouseenter="PMC_hover_break_fraction_range_button(this)"
								>
								<img src="./Icons/bolt_yellow.svg" />
								</button>
							`
		}
	}

	var seg_position = 0
	var segment_start_position = []
	voice_data.data.segment_data.forEach(segment=>{
		segment_start_position.push(seg_position)
		seg_position += segment.time[segment.time.length-1].P
	})

	var index=0
	var segment_index=0
	frac_data.start_position.forEach(position=>{
		if(segment_start_position[segment_index]==position){
			//if start segment no button join
			pulse_bottom_list[position].innerHTML=`
														<input class="${class_fraction_inp_box}"
														onkeypress="APP_onlyIntegers_enter_fraction(event,this)" onfocusin="APP_set_previous_value(this)"
														onfocusout="PMC_enter_new_fraction(this)" onkeydown="PMC_move_focus_fraction(event,this)" oninput="APP_stop()" maxlength="3" value="${frac_data.string[index]}"
														>
													`
			segment_index++
		}else{
			pulse_bottom_list[position].innerHTML=`
														<button class="App_PMC_fraction_del" onclick="PMC_join_fraction_ranges(this)">
															«
														</button>
														<input class="${class_fraction_inp_box}"
														onkeypress="APP_onlyIntegers_enter_fraction(event,this)" onfocusin="APP_set_previous_value(this)"
														onfocusout="PMC_enter_new_fraction(this)" onkeydown="PMC_move_focus_fraction(event,this)" oninput="APP_stop()" maxlength="3" value="${frac_data.string[index]}"
														>
													`
		}
		index++
	})
}

function PMC_generate_current_voice_time_positions(voice_data){
	//current voice
	var s_data = voice_data.data.segment_data
	var array_frac_list = []
	s_data.forEach(segment=>{
		segment.fraction.forEach(array=>{
			array_frac_list.push(array)
		})
	})

	var time_positions = [0]
	var time_fraction_range_change=[]
	var frac_start = 0
	array_frac_list.forEach(fraction=>{
		var delta = fraction.stop-fraction.start
		//neopulse??? //NO NEED
		var fr = fraction.N/fraction.D
		var rep = delta/fr
		var position=0
		for (var i=1;i<=rep;i++){
			position=frac_start+fr*i
			time_positions.push(position)
		}
		time_fraction_range_change.push([frac_start,fraction.N,fraction.D])
		frac_start = Math.round(position) //to assure it is an int
	})
	time_fraction_range_change.push([frac_start,null,null])//the end
	return {"time_positions":time_positions,"time_fraction_range_change":time_fraction_range_change}
}

function PMC_hover_time_line_pulse(element){
	//show break fraction range
	if(PMC_segment_name_prevent_default)return

	var break_fraction_range_button = element.querySelector(".App_PMC_fraction_add")
	if(break_fraction_range_button!=null)break_fraction_range_button.style.display="flex"
	//show break fraction range
	var delete_fraction_range_button = element.querySelector(".App_PMC_fraction_del")
	if(delete_fraction_range_button!=null)delete_fraction_range_button.style.display="block"
	//show break segment
	var break_segment_range_button = element.querySelector(".App_PMC_break_segment")
	if(break_segment_range_button!=null)break_segment_range_button.style.display="flex"

	//show fraction inp box if exist and is invisible
	var fraction_inp_box = element.querySelector(".App_PMC_fraction_inp_box")
	if(fraction_inp_box!=null)fraction_inp_box.style.display="flex"
}

function PMC_leave_time_line_pulse(element){
	var break_fraction_range_button = element.querySelector(".App_PMC_fraction_add")
	if(break_fraction_range_button!=null)break_fraction_range_button.style.display="none"
	//show break fraction range
	var delete_fraction_range_button = element.querySelector(".App_PMC_fraction_del")
	if(delete_fraction_range_button!=null)delete_fraction_range_button.style.display="none"
	//show break segment
	var break_segment_range_button = element.querySelector(".App_PMC_break_segment")
	if(break_segment_range_button!=null)break_segment_range_button.style.display="none"
	//reset vertical yellow or blue line
	element.classList.remove("App_PMC_time_line_segment_blue")
	element.classList.remove("App_PMC_time_line_fraction_yellow")

	var fraction_inp_box = element.querySelector(".App_PMC_fraction_inp_box")
	if(fraction_inp_box!=null){
		//if(fraction_inp_box.is(":focus"))
		fraction_inp_box.style.display=""
	}
}


function PMC_generate_current_voice_segment_pulse(voice_data){
	//current voice
	var s_data = voice_data.data.segment_data
	var array_seg_list = []
	s_data.forEach(segment=>{
		array_seg_list.push(segment.time[segment.time.length-1].P)
	})

	var segment_pulses = []
	array_seg_list.forEach(val=>{
		for (let i = 0; i < val; i++) {
			segment_pulses.push(i)
		}
	})
	return segment_pulses
}

function PMC_generate_current_voice_segment_pulse_mod(voice_data,n_needed){
	//current voice
	var s_data = voice_data.data.segment_data

	//if i want a different way of writing on start of a segment
	//var array_seg_list_abs = []
	//var seg_start=0
	// s_data.forEach(segment=>{
	// 	var pos=seg_start+segment.time[segment.time.length-1].P
	// 	array_seg_list_abs.push(pos)
	// 	seg_start=pos
	// })
	//console.log(array_seg_list_abs)
	var segment_pulses = []
	var compas_data = DATA_get_compas_sequence()
	var last_compas= compas_data.slice(-1)[0].compas_values
	var compas_length_value = last_compas[0]+last_compas[1]*last_compas[2]

	//for (let i = 0; i < Li; i++) {
	for (let i = 0; i < n_needed; i++) {
		//maake calculation module
		if(i <compas_length_value && compas_length_value!=0){
			var compas_type = compas_data.filter(compas=>{
				return compas.compas_values[0] <= i
			})

			//var current_compas_type = compas_type.pop()
			var current_compas_type = compas_type.pop()

			var current_compas_number = compas_type.reduce((prop,item)=>{
				return item.compas_values[2]+prop
			},0)

			var dP = i - current_compas_type.compas_values[0]
			var mod = current_compas_type.compas_values[1]
			var dC = Math.floor(dP/mod)
			var resto = dP
			if(dC!=0) resto = dP%mod
			current_compas_number+= dC
			//  <p>This text contains <sup>superscript</sup> text.</p>

			//warning first element
			//verify if it has module, if not read previous value
			var showcompas = true
			var showpulse = true
			//here retrive value

			var string_1=resto
			var string_2=resto+""+RE_string_to_superscript(current_compas_number.toString())

			if(resto ==0){
				segment_pulses.push({s:string_2,s_c:string_2})
			}else{
				//if(i==XXX  array_seg_list_abs){
					//segment_pulses.push(resto+""+RE_string_to_superscript(current_compas_number.toString()))
				//}else{
					segment_pulses.push({s:string_1,s_c:string_2})
				//}
			}
		}else{
			//outside or no compasses
			segment_pulses.push({s:no_value,s_c:no_value})
		}
	}

	return segment_pulses
}

function PMC_generate_current_voice_time_seconds(voice_data){
	//current voice
	var s_data = voice_data.data.segment_data
	var array_frac_list = []
	s_data.forEach(segment=>{
		segment.fraction.forEach(array=>{
			array_frac_list.push(array)
		})
	})

	var time = [0]
	var position = 0
	array_frac_list.forEach(fraction=>{
		var delta = fraction.stop-fraction.start
		var fr = fraction.N/fraction.D
		var rep = delta/fr
		for (var i=0;i<rep;i++){
			position+=fr
			time.push(position)//need truncate!!!
		}
	})
	return time
}

function PMC_generate_current_voice_fraction_data_position(voice_data){
	//current voice
	var s_data = voice_data.data.segment_data//.fraction

	var array_frac_list = []
	s_data.forEach(segment=>{
		segment.fraction.forEach(array=>{
			array_frac_list.push(array)
		})
	})

	//console.log(array_frac_list)

	var start_position = [0]
	var stop_position = []
	var string=[]
	var N = []
	var D = []
	var current_position = 0
	array_frac_list.forEach(fraction=>{
		current_position += fraction.stop-fraction.start
		start_position.push(current_position)
		stop_position.push(current_position)
		N.push(fraction.N)
		D.push(fraction.D)
		string.push(fraction.N+"/"+fraction.D)
	})
	start_position.pop()
	return {start_position,stop_position,string,N,D}
}

function PMC_x_dots(voice_data){
	//how many dots there are in PMC
	var max_length=Li
	var last_compas = DATA_get_compas_sequence().pop()
	var max_compas=last_compas.compas_values[0]+last_compas.compas_values[1]*last_compas.compas_values[2]
	if(max_compas>Li)max_length=max_compas //XXX   from database li, compass and scale
	var D_NP = voice_data.neopulse.D
	var N_NP = voice_data.neopulse.N
	var neopulse_moltiplicator =  N_NP/D_NP
	if(N_NP!=1 || D_NP!=1){
		//compass not used
		max_length = Li/neopulse_moltiplicator
	}
	return max_length
}

function PMC_enter_new_fraction(element){
	if(element.value == previous_value){
		return
	}
	APP_stop()

	var split = element.value.split('/');
	var fractComplex = Math.floor(split[0])
	var fractSimple = Math.floor(split[1])

	var parent = element.closest(".App_PMC_time_line")

	var fract_list= [...parent.querySelectorAll(".App_PMC_fraction_inp_box")]

	var abs_position = fract_list.indexOf(element)
	var voice_index = DATA_get_selected_voice_data().voice_index
	var result = DATA_modify_fraction_value(voice_index,null,abs_position,fractComplex,fractSimple)

	if(result==false){
		element.value = previous_value
		APP_blink_error(element)
		return
	}
}

function PMC_move_focus_fraction(evt,element){
	switch(evt.keyCode){
	case 37:
		// Key left.
		PMC_fraction_focus_index(element,-1)
		evt.preventDefault();
		break
	case 39:
		// Key right.
		PMC_fraction_focus_index(element,1)
		evt.preventDefault();
		break
	case 9:
		//tab
		evt.preventDefault()

		if(evt.shiftKey) {
			//shift was down when tab was pressed
			PMC_fraction_focus_index(element,-1)
		}else{
			//tab
			PMC_fraction_focus_index(element,1)
		}
		break
	case 13:
		//intro
		PMC_fraction_focus_index(element,1)
		break
	}
}

function PMC_fraction_focus_index(element,move){
	var parent = element.closest(".App_PMC_time_line")
	var fract_list= [...parent.querySelectorAll(".App_PMC_fraction_inp_box")]
	var index = fract_list.indexOf(element)+move

	if(index>=0 && index<fract_list.length){
		//focus on index
		if(fract_list[index].classList.contains("hidden"))fract_list[index].classList.remove("hidden")
		fract_list[index].focus()
		fract_list[index].select()
	}else{
		//refocus
		element.blur()
		setTimeout(function(){
			element.focus()
			element.select()
		}, 10)
	}
}

function PMC_break_fraction_range_button(element){
	var pulse_div = element.closest(".App_PMC_time_line_pulse")
	var container = pulse_div.closest(".App_PMC_time_line")
	var pulse_div_list = [...container.querySelectorAll(".App_PMC_time_line_pulse")]
	var absolute_pulse_position = pulse_div_list.indexOf(pulse_div)
	//absolute position
	//need to calculate segment number and relative position
	var segment_index=0
	var prev_segments_n_obj = 0
	var voice_data = DATA_get_selected_voice_data()

	voice_data.data.segment_data.find(segment=>{
		var n_obj = segment.time[segment.time.length -1].P
		if(prev_segments_n_obj+n_obj>absolute_pulse_position){
			return true
		}else{
			//next segment
			prev_segments_n_obj+=n_obj //last element of a segment is .
			segment_index++
			return false
		}
	})
	var position = absolute_pulse_position-prev_segments_n_obj
	//verify if position is suitable for new fraction range
	//find current fraction
	var current_fraction_index = 0
	var current_fraction = voice_data.data.segment_data[segment_index].fraction.find(fraction=>{
		if(position>fraction.start && position<fraction.stop){
			//console.log(start+"<"+position+"<"+stop)
			return true
		}
		current_fraction_index++
	})

	//verify if fraccionable NO NEED: new fraction button is already well placed

	//apply new fractioning range
	var new_fraction = JSON.parse(JSON.stringify(current_fraction))
	current_fraction.stop=position
	new_fraction.start=position
	DATA_break_fraction_range(voice_data.voice_index,segment_index,current_fraction_index,[current_fraction,new_fraction])
}

function PMC_hover_break_fraction_range_button(button){
	var pulse_div = button.closest(".App_PMC_time_line_pulse")
	pulse_div.classList.remove("App_PMC_time_line_segment_blue")
	pulse_div.classList.add("App_PMC_time_line_fraction_yellow")
}

function PMC_join_fraction_ranges(element){
	//find index first fraction
	var parent = element.closest(".App_PMC_time_line")
	var fract_list= [...parent.querySelectorAll(".App_PMC_fraction_inp_box")]
	var fract_inp_box= element.parentNode.querySelector(".App_PMC_fraction_inp_box")
	var index_abs = fract_list.indexOf(fract_inp_box)

	//find segment and index fraction relative to segment
	var voice_data = DATA_get_selected_voice_data()
	var voice_index = voice_data.voice_index
	var segment_index=0
	var first_fraction_index=0
	var n_range=0
	voice_data.data.segment_data.find(segment=>{
		//console.log(segment)
		n_range+=segment.fraction.length
		if(n_range>index_abs){
			//fraction in this segment
			first_fraction_index=index_abs-(n_range-segment.fraction.length)-1
			return true
		}
		segment_index++
	})

	//console.log(segment_index+" f "+first_fraction_index)
	DATA_join_fraction_ranges(voice_index,segment_index,first_fraction_index)
}

//segments
function PMC_write_time_line_segments(voice_data){
	var D_NP = voice_data.neopulse.D
	var N_NP = voice_data.neopulse.N
	var neopulse_moltiplicator =  N_NP/D_NP
	var delta = nuzic_block * PMC_zoom_x * neopulse_moltiplicator

	var segment_data = PMC_generate_current_voice_segment_data_position(voice_data)

	//On start segment put a div with number, name(optional) ,menu , join button
	//list objects
	var line_container = document.querySelector(".App_PMC_time_line")
	//var pulse_list = [...line_container.querySelectorAll(".App_PMC_time_line_pulse")]
	var pulse_upper_list = [...line_container.querySelectorAll(".App_PMC_time_line_pulse_top")]

	var index_pulse=0
	var index_segment=0
	var index_breakable=0
	pulse_upper_list.forEach(pulse=>{
		if(segment_data.start_position[index_segment]==index_pulse){
			//menu at the start of a segment
			pulse_upper_list[index_pulse].innerHTML=`
										<button class="App_PMC_segment_header" end_segment_number="${index_segment-1}"
										onclick="PMC_toggle_dropdown_menu_segment(this,${index_segment})">
											${index_segment}
										</button>
									`
			//add title if there is space
			var title_space = delta*(segment_data.stop_position[index_segment]-segment_data.start_position[index_segment]-1)

			var input_string=""
			if(title_space>=84){
				if(!segment_data.segment_name[index_segment]==""){
					input_string+=`<input class="App_PMC_segment_name"`
				}else{
					input_string+=`<input class="App_PMC_segment_name hidden"`
				}
			}else{
				input_string+=`<input class="App_PMC_segment_name hidden short"`
			}
			input_string+=`
						value="${segment_data.segment_name[index_segment]}"
						disabled="true" maxlength="8" placeholder=""
						onfocusin="PMC_segment_name_prevent_default=true"
						onfocusout="PMC_segment_name_prevent_default=false"
						onchange="PMC_change_segment_name(this)"
					>
					</input>
				`
			pulse_upper_list[index_pulse].innerHTML+=input_string
			index_segment++;
		}

		if(segment_data.breakable_position_list[index_breakable]==index_pulse){
			index_breakable++

			pulse_upper_list[index_pulse].innerHTML=`
									<button class="App_PMC_break_segment"
									onclick="PMC_break_segment_button(this)" onmouseenter="PMC_hover_break_segment_button(this)"
									>
										<img src="./Icons/bolt_blue.svg" />
									</button>
								`
		}

		index_pulse++
	})

	//add last menu XXX
	pulse_upper_list[pulse_upper_list.length-1].innerHTML=`
										<button class="App_PMC_segment_header" end_segment_number="${index_segment-1}"
										onclick="PMC_add_segment_button(this)">
											+
										</button>
										`
}

function PMC_generate_current_voice_segment_data_position(voice_data){
	var segment_data = voice_data.data.segment_data
	var seg_position = 0
	var start_position = []
	var stop_position = []
	var segment_name = []
	segment_data.forEach(segment=>{
		start_position.push(seg_position)
		seg_position += segment.time[segment.time.length-1].P
		stop_position.push(seg_position)
		segment_name.push(segment.segment_name)
	})

	var breakable_position_list = []
	var mod_Li = voice_data.data.segment_data.reduce((prop,segment)=>{return segment.time.slice(-1)[0].P + prop},0)
	//calculate the pulses that are breakable
	var delta_Ls_absolute=DATA_calculate_minimum_Li()
	var delta_Ls_min= delta_Ls_absolute*voice_data.neopulse.D/voice_data.neopulse.N //XXX USE THIS X VERIFY BREAKABILITY

	//verify is voice is blocked,
	if(voice_data.blocked){
		//blocked, verify on every blocked voice
		var data = DATA_get_current_state_point(false)//here error on drag and drop???XXX not important
		var breakable_position_absolute_list = []

		//filter all blocked voices fractions and neopulse
		var frac_data_list = []
		data.voice_data.forEach(voice=> {
			if(voice.blocked){
				//calculate segment starting point
				var voice_start_position = []
				var voice_seg_position = 0
				voice.data.segment_data.forEach(segment=>{
					voice_start_position.push(voice_seg_position)
					voice_seg_position += segment.time[segment.time.length-1].P
				})
				frac_data_list.push({"frac_data": PMC_generate_current_voice_fraction_data_position(voice), "segment_start_position":voice_start_position,"neopulse":voice.neopulse})
			}
		})

		//for each blocked voice verify every pulse with fractioning
		frac_data_list.forEach(voice=>{
			var frac_data = voice.frac_data
			var D_NP = voice.neopulse.D
			var N_NP = voice.neopulse.N
			var neopulse_moltiplicator =  N_NP/D_NP
			var mod_Li_voice = Li / neopulse_moltiplicator //ATT for drag and drop
			var current_frac_index = 0
			var segment_index=0
			var breakable_position_list_voice=[]

			for (let i = 0; i < mod_Li_voice; i++) {
				if(frac_data.stop_position[current_frac_index]<=i){
					current_frac_index ++
				}
				if(voice.segment_start_position[segment_index]==i){
					segment_index++
				}else if((i-frac_data.start_position[current_frac_index])%frac_data.N[current_frac_index]==0){
					//is fractionable
					breakable_position_list_voice.push(i*neopulse_moltiplicator)
				}
			}
			breakable_position_absolute_list.push(breakable_position_list_voice)
		})
		//console.log(breakable_position_absolute_list)
		//collapse all arrays into one
		var breakable_position_absolute = breakable_position_absolute_list.pop()
		breakable_position_absolute_list.forEach(list=>{
			var mom = []
			var index_mom = 0
			var i=0
			var j=0
			while (i < breakable_position_absolute.length && j < list.length) {		// prevent running forever
				while (breakable_position_absolute[i] < list[j]) {					// check left side
					++i;															// increment index
				}
				while (list[j] < breakable_position_absolute[i]) {					// check right side
					++j;															// increment
				}
				if (breakable_position_absolute[i] === list[j]) {					// check equalness
					//console.log(a[i], b[j]);										// output or collect
					mom.push(breakable_position_absolute[i])
					++i;															// increment indices
					++j;
				}
			}
			breakable_position_absolute=mom
		})
		//console.log(breakable_position_absolute)
		//convert this array in current pulse
		var D_NP = voice_data.neopulse.D
		var N_NP = voice_data.neopulse.N
		var neopulse_moltiplicator =  N_NP/D_NP
		breakable_position_absolute.forEach(absolute_pulse=>{
			//breakable fraction AND min Ls
			if(absolute_pulse%delta_Ls_absolute==0)breakable_position_list.push(absolute_pulse/neopulse_moltiplicator)
		})
	}else{
		//voice not blocked
		var frac_data = PMC_generate_current_voice_fraction_data_position(voice_data)
		var current_frac_index = 0
		var segment_index=0
		for (let i = 0; i < mod_Li; i++) {
			if(frac_data.stop_position[current_frac_index]<=i){
				current_frac_index ++
			}
			if(start_position[segment_index]==i){
				segment_index++
			}else if((i-frac_data.start_position[current_frac_index])%frac_data.N[current_frac_index]==0 && i%delta_Ls_min==0){
				//is fractionable and module of min_Ls
				breakable_position_list.push(i)
			}
		}
	}

	//case drag and drop breakable_position_list is incorrect BUT doesnt matter
	var D_NP = voice_data.neopulse.D
	var N_NP = voice_data.neopulse.N
	var neopulse_moltiplicator =  N_NP/D_NP
	if(mod_Li != (Li / neopulse_moltiplicator))breakable_position_list=[]

	//console.log({start_position,stop_position,segment_name,breakable_position_list})
	return {start_position,stop_position,segment_name,breakable_position_list}
}

function PMC_break_segment_button(element){
	APP_stop()
	var pulse_div = element.closest(".App_PMC_time_line_pulse")
	var container = pulse_div.closest(".App_PMC_time_line")
	var pulse_div_list = [...container.querySelectorAll(".App_PMC_time_line_pulse")]
	var absolute_pulse_position = pulse_div_list.indexOf(pulse_div)
	//absolute position
	//need to calculate segment number and relative position
	var segment_index=0
	var prev_segments_n_obj = 0
	var voice_data = DATA_get_selected_voice_data()

	voice_data.data.segment_data.find(segment=>{
		var n_obj = segment.time[segment.time.length -1].P
		if(prev_segments_n_obj+n_obj>absolute_pulse_position){
			return true
		}else{
			//next segment
			prev_segments_n_obj+=n_obj //last element of a segment is .
			segment_index++
			return false
		}
	})
	var pulse = absolute_pulse_position-prev_segments_n_obj

	DATA_break_segment(voice_data.voice_index,segment_index, pulse)
}

function PMC_hover_break_segment_button(button){
	var pulse_div = button.closest(".App_PMC_time_line_pulse")
	pulse_div.classList.add("App_PMC_time_line_segment_blue")
	pulse_div.classList.remove("App_PMC_time_line_fraction_yellow")
}

function PMC_change_segment_name(element){
	element.blur()
	PMC_segment_name_prevent_default=false
	APP_stop()
	var data = DATA_get_current_state_point(true)
	//Selected voice
	var selected_voice_data= data.voice_data.find(item=>{
		return item.selected
	})
	//find the index
	var line_container = element.closest(".App_PMC_time_line")
	var segment_name_list = [...line_container.querySelectorAll(".App_PMC_segment_name")]
	var segment_index=segment_name_list.indexOf(element)
	selected_voice_data.data.segment_data[segment_index].segment_name = element.value

	if(element.value==""){
		element.classList.add("hidden")
	}else{
		element.classList.remove("hidden")
	}

	if(selected_voice_data.blocked){
		//change name all blocked voices
		data.voice_data.forEach(item=>{
			if(item.blocked)item.data.segment_data[segment_index].segment_name = element.value
		})
	}

	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function PMC_modify_segment_Ls_drag_start(event,element,delta_step){
	//calculate mouse position
	//var starting_position=event.offsetX//no
	var starting_position=event.clientX

	//find element segment_index
	//var segment_index= parseInt(element.parentNode.querySelector(".App_PMC_segment_header").innerHTML)-1
	var segment_index= parseInt(element.parentNode.querySelector(".App_PMC_segment_header").getAttribute("end_segment_number"))

	//document.addEventListener("dragover", (e)=>{PMC_preview_modify_segment_Ls_drag(e,element,starting_position,delta_step);APP_store_anonymous_event(arguments.callee, false, 'dragover');})
	document.addEventListener("dragover", function anon(e){PMC_preview_modify_segment_Ls_drag(e,element,starting_position,segment_index,delta_step);APP_store_anonymous_event(arguments.callee, false, 'dragover');})
	//document.addEventListener("dragover", function ciao() {console.log("ciao");APP_store_anonymous_event(arguments.callee, false, 'dragover');})
	element.addEventListener("dragend", (e)=>{PMC_modify_segment_Ls_drag_end(e,element,starting_position,segment_index,delta_step)})
}

function PMC_preview_modify_segment_Ls_drag(event,element,starting_position,segment_index,delta_step){
	//calculate mouse position
	var delta_drag =event.pageX-starting_position
	//calculate number x steps

	var x_steps = Math.round(delta_drag/delta_step)
	//if changed...
	if(x_steps!=PMC_drag_x_steps){
		PMC_drag_x_steps=x_steps
		//console.log("add "+PMC_drag_x_steps)
		//identify voice_index
		var voice_data_mod = DATA_get_selected_voice_data()
		//calc min Ls
		var delta_Ls_absolute=DATA_calculate_minimum_Li()
		var delta_Ls = delta_Ls_absolute*voice_data_mod.neopulse.D/voice_data_mod.neopulse.N

		var new_Ls = PMC_drag_x_steps+voice_data_mod.data.segment_data[segment_index].time.slice(-1)[0].P

		if(new_Ls<=0)return //< min Ls for this voice

		if(new_Ls%delta_Ls!=0){
			var resto = new_Ls%delta_Ls
			new_Ls-=resto
		}

		//verify if is ok to expand
		var old_Ls = voice_data_mod.data.segment_data[segment_index].time.slice(-1)[0].P
		var possible = Li+(new_Ls-old_Ls)*voice_data_mod.neopulse.N/voice_data_mod.neopulse.D<=max_Li
		//modify current voice x axis for display
		if(possible){
			//recalculate data segment
			DATA_calculate_force_segment_data_Ls(voice_data_mod.data.segment_data[segment_index],new_Ls)
			PMC_redraw_time_line(voice_data_mod)
		}
		//XXX CHROME GLITCH break fraction button is shown XXX
	}
}

function PMC_modify_segment_Ls_drag_end(e,element,starting_position,segment_index,delta_step){
	//calculate mouse position
	var delta_drag =event.pageX-starting_position
	//calculate number x steps
	var x_steps = Math.round(delta_drag/delta_step)

	//if changed...
	if(x_steps!=0){
		//recalculate data voice
		var data = DATA_get_current_state_point(false)
		var voice_data_mod = data.voice_data.find(voice=>{return voice.selected})
		//calc min Ls
		var delta_Ls_absolute=DATA_calculate_minimum_Li()
		var delta_Ls = delta_Ls_absolute*voice_data_mod.neopulse.D/voice_data_mod.neopulse.N


		//if(new_Ls<=0)return //< min Ls for this voice
		var new_Ls = PMC_drag_x_steps+voice_data_mod.data.segment_data[segment_index].time.slice(-1)[0].P
		if(new_Ls<=0){
			PMC_redraw_time_line(voice_data_mod)
			return //< min Ls for this voice
		}
		if(new_Ls%delta_Ls!=0){
			var resto = new_Ls%delta_Ls
			new_Ls-=resto
		}
		//recalculate data segment
		var success = DATA_change_Ls_segment(voice_data_mod.voice_index,segment_index,new_Ls)
		if(!success){
			//redraw old time
			PMC_redraw_time_line(voice_data_mod)
		}
	}
	APP_delete_stored_anonymous_event(document,"dragover")
	//reset global variable
	PMC_drag_x_steps=0
}

