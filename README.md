# Web and App Nuzic

Development of web apps and main app Nuzic

**License**

The software is provided to the user "as is".

The architecture and source code of the Nuzic app and the Nuzic.org website’s engine are original work and are intellectual property of Maumusic S.L.U, except where expressly specified.

They are released to the public under the GNU GPL v3 [https://www.gnu.org/licenses/gpl-3.0.html] license.

https://www.nuzic.org/terminos-y-condiciones/

The "Nuzic Team" takes no responsibility for any damage, which may occur as a result of using the Nuzic software.


**Sound Font License**


Salamander Grand Piano: Author Alexander Holm
License 	CC-BY-3.0

Other samples from a variety of public domain sources

Samples: CC-BY 3.0

bass        - Karoryfer
bassoon     - VSO2
cello       - Freesound - 12408__flcellogrl__real-cello-notes
contrabass  - VSO2
flute       - VSO2
french horn - VSO2
guitar ac   - Iowa
guitar el   - Karoryfer
guitar ny   - Freesound - 11573__quartertone__classicalguitar-multisampled
harmonium   - Freesound - 330410__donyaquick__harmonium-samples-all-keys-and-drones
harp        - VSO2
organ       - VSO2
piano       - VSO2
sax         - Karoryfer
trombone    - VSO2
trumpet     - VSO2
tuba        - VSO2
violin      - VSO2
xylophone   - VSO2


https://freesound.org
https://www.karoryfer.com/karoryfer-samples
http://vis.versilstudios.net/vsco-community.html
http://theremin.music.uiowa.edu/

Percussions
Fluid V3 Set
This set is based on Frank Wen's pro-quality GM/GS soundfont. 
http://openmetronome.sourceforge.net/samples.html

**Library**

Lame.js
https://github.com/zhuker/lamejs

Tone.js
https://tonejs.github.io/

jsmidgen.js
https://github.com/dingram/jsmidgen

FileIO.js
https://www.programmingnotes.org/5422/javascript-fileio-js-open-save-read-files-using-vanilla-javascript/
